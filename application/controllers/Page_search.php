<?php

class page_search extends CI_Controller
{

    function __construct()
    {

        parent :: __construct();

        $this->settings = $this->system_vars->get_settings();
    }

    function index()
    {

        $uri = $this->uri->segment('1');

        $sql = "SELECT 
                        members.username 
                FROM 
                        members
                JOIN member_profiles ON member_profiles.member_id = members.id AND members.active = 1
                WHERE members.username = '{$uri}'
                LIMIT 1
            ";
        $findReader = $this->db->query($sql)->row_array();

        if ($findReader)
        {
            redirect("/frontend/profile/{$findReader['username']}");
        } else
        {
            $getPage = $this->db->query("SELECT * FROM pages WHERE url = '{$uri}' LIMIT 1");
            /*9-15
              @pen
            */
            $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
            $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

            if ($getPage->num_rows() == 0)
            {
                $data = array(
                    'title' => 'Psychic Contact - Error 404: Page not found!',
                    'header' => $this->load->view('frontend/layouts/header', $param_header, TRUE),
                    'year' => range(1930, date('Y') + 50),
                    'slider' => $this->load->view('frontend/layouts/slider', '', TRUE),
                    'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE),
                    'content' => $this->load->view('/frontend/error_page', '', TRUE)
                );

                $this->load->library('parser');
                $this->parser->parse('frontend/layouts/page', $data);
            } else
            {
                $data = array(
                    'title' => "Psychic Contact",
                    'header' => $this->load->view('frontend/layouts/header', $param_header, TRUE),
                    'slider' => "",
                    'content' => $this->load->view('frontend/static_page', $getPage->row_array(), TRUE),
                    'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
                );
                $this->load->library('parser');        
                $this->parser->parse('frontend/layouts/page', $data);
            }
        }
    }

}
