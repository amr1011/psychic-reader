<?php

class blog extends CI_Controller
{

    function __construct()
    {
        parent :: __construct();

        $this->load->model('blog_model');
        $this->load->library('pagination');
    }

    function index()
    {
        $params['posts'] = $this->blog_model->getLatestPosts(5);

        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Blogs",
            'header' => $this->load->view('frontend/layouts/header_login', $param_header, TRUE),
            'slider' => "",
            'content' => $this->load->view('/frontend/blog/main', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');        
        $this->parser->parse('frontend/layouts/page', $data);
    }

    function login($blog_url)
    {

        $this->session->set_userdata('redirect', "/frontend/blog/{$blog_url}");
        redirect("/register/login");
    }

    function view($post_url)
    {

        $t = $this->blog_model->getPost($post_url);
        $t['comments'] = $this->blog_model->getComments($t['id']);

        $this->load->view('frontend/partial/header');
        $this->load->view('frontend/blog/view_post', $t);
        $this->load->view('frontend/partial/footer');
    }

    function submit_comment($id)
    {

        if ($this->input->post('comments') && isset($this->member->data['id']))
        {

            $post = $this->blog_model->getPost($id);

            $insert = array();
            $insert['blog_id'] = $id;
            $insert['member_id'] = $this->member->data['id'];
            $insert['datetime'] = date("Y-m-d H:i:s");
            $insert['comments'] = $this->input->post('comments');

            $this->db->insert('blog_comments', $insert);

            $this->session->set_flashdata('response', "Your comments have been saved and sent to the administrator for approval. Once they have been approved, they will appear on this blog post. Thank you for your submission!");

            redirect("/frontend/blog/{$post['url']}");
        }
    }

    function archive($page = 0)
    {

        $total_per_page = 5;

        // Pagination
        $config['base_url'] = "/frontend/blog/archive/";
        $config['uri_segment'] = '3';
        $config['total_rows'] = $this->blog_model->getTotalArchivedPosts();
        $config['per_page'] = $total_per_page;

        $this->pagination->initialize($config);
        $t['pagination'] = $this->pagination->create_links();

        // Get Posts
        $t['posts'] = $this->blog_model->getArchivedPosts($page, $total_per_page);

        $this->load->view('frontend/partial/header');
        $this->load->view('frontend/blog/main', $t);
        $this->load->view('frontend/partial/footer');
    }

}
