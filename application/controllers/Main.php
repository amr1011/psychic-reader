<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//--- Include Elephant.io
require_once( APPPATH . "models/ElephantIO/Client.php");


/*
 * This code needs optimization
 * Start date of code optimization: June 2, 2019
 * Revised By: EVP
*/
class Main extends CI_Controller
{

    function __construct()
    {
        parent :: __construct();
        $this->settings = $this->system_vars->get_settings();
        $this->load->library('session');
    }

    public function json()
    {
        $this->load->model('zendesk');
        $data = $this->zendesk->create_new_ticket("Rob", "ericvp2016@gmail.com", "Test Subject", "The message goes here... ONe more test goes here");
    }

    public function index()
    {
        if (isset($this->member->data['profile_id']) && $this->member->data['profile_id'])
        {
            redirect('my_account');
        }
    

      
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 AND add_to_homepage = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 AND add_to_homepage = 1 ORDER BY sort_footer ASC")->result_array();
        $data = array(
            'title' => 'Psychic Contact',
            'header' => $this->load->view('frontend/layouts/header', $param_header, TRUE),
            'banner' => $this->load->view('frontend/layouts/banner', '', TRUE),
            'content' => $this->load->view('frontend/layouts/home', '', TRUE),
            'reader_partial' => $this->load->view('frontend/layouts/reader_partial', '', TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $data["featured_readers"]   = $this->member->get_featured_readers();
        $data["online_readers"]     = $this->member->get_online_readers();
        $data['readers']            = $this->readers->getAll();


        // $this->hide_banner = true;
        // $t = $this->system_vars->get_page('homepage');
        // $getArticles = $this->db->query("SELECT * FROM articles ORDER BY id DESC LIMIT 5");
        // $t['articles'] = $getArticles->result_array();
       

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/app', $data);
    }

    function getTime()
    {
        $now = new DateTime();
        echo $now->format("M j, Y H:i:s");
    }

    function testObject()
    {

        $rampnode = new Client('http://psycon.rampnode.com', 'socket.io', 1, true, true, true);
        $rampnode->init();
        $rampnode->emit('subscribe', 'psycon.rampnode.com/readerStatuses');
        $rampnode->send(Client::TYPE_MESSAGE, null, null, "Test");
        $rampnode->close();
        // echo "Done?";
    }

    function logout()
    {

        if (isset($this->member->data['profile_id']))
        {
            $this->reader->set_status('offline');
        }

        $this->session->sess_destroy();
        redirect("/");
    }

    function banned()
    {

        if (isset($this->member->data['profile_id']))
        {
            $this->reader->set_status('offline');
        }

        $this->session->unset_userdata('member_logged');
        $this->session->set_flashdata("error", "You were banned from Psychic Contact.");
        redirect("/");
    }

    function disconnect_user($mem_id)
    {
        $this->reader->init($mem_id)->set_status('offline');
    }

    function favorite($profile_id)
    {

        if ($profile_id)
        {

            if ($this->session->userdata('member_logged'))
            {

                $checkFavs = $this->db->query("SELECT `id` FROM favorites WHERE profile_id = {$profile_id} AND member_id = {$this->session->userdata('member_logged')} LIMIT 1");

                if ($checkFavs->num_rows() == 0)
                {

                    $insert = array();
                    $insert['profile_id'] = $profile_id;
                    $insert['member_id'] = $this->session->userdata('member_logged');

                    $this->db->insert('favorites', $insert);

                    $array['message'] = "This profile has been saved to your favorites!";
                } else
                {

                    $array['message'] = "This expert's profiles has already been saved to your favorites";
                }
            } else
            {

                $array['message'] = "You must be logged in before you can add an experts profile as favorites";
            }
        } else
        {

            $array['message'] = "No profile id was specified";
        }

        echo json_encode($array);
    }

    public function check_multiple_reader_status()
    {
        $now = date("Y-m-d H:i:s");

        //--- Build a CSV list of all reader usernames
        $readerString = "";

        foreach ($this->input->post('usernameArray') as $i => $username) {
            $readerString .= "'{$username}',";
        }

        $readerString = substr($readerString, 0, -1);

        //--- Do one DB call to check ALL usernames
        $sql = "SELECT members.username,
                        members.id,
                        members.id as member_id,
                        member_profiles.last_chat_request,
                        member_profiles.last_pending_time,
                        member_profiles.break_time,
                        CASE WHEN member_profiles.status IS NULL OR member_profiles.status = ''
                             THEN 'offline'
                             ELSE member_profiles.status
                        END as status
                FROM    members,
                        member_profiles
                WHERE   members.username IN ({$readerString})
                        and members.id = member_profiles.member_id";
        $getProfile = $this->db->query($sql);

        $finalArray = array();
        foreach ($getProfile->result_array() as $user) {

            if ($this->session->userdata("member_logged"))
            {

                if ($this->member->checkBan($user['id']))
                {
                    $user['status'] = 'blocked';
                }

                if ($user['status'] == "online")
                {
                    if (strtotime($user['last_chat_request']) < strtotime($user['last_pending_time']))
                    {
                        // then just consider the pending time. 
                        if (strtotime($now) - strtotime($user['last_pending_time']) < CHAT_MAX_PENDING)
                        {
                            $user['status'] = "busy";
                        }
                    } else
                    {
                        if (strtotime($now) - strtotime($user['last_chat_request']) < CHAT_MAX_WAIT)
                        {
                            $user['status'] = "busy";
                        }
                    }

                    if (strtotime($now) < strtotime($user['break_time']))
                    {
                        $user['status'] = "break";
                    }
                }
            }

            $finalArray[$user['username']] = $user['status'];
        }

        //--- Return JSON formatted response
        echo json_encode($finalArray);
    }

    public function check_all_reader_statuses()
    {

        //--- Do one DB call to check ALL usernames
        $sql = "SELECT members.username,
                        members.id,
                        CASE WHEN member_profiles.status IS NULL OR member_profiles.status = ''
                             THEN 'offline'
                             ELSE member_profiles.status
                        END as status
                FROM members
                JOIN member_profiles ON member_profiles.member_id = members.id";
        $getProfile = $this->db->query($sql);

        $finalArray = array();
        foreach ($getProfile->result_array() as $user) {

            if ($this->session->userdata("member_logged"))
            {

                if ($this->member->checkBan($user['id']))
                {
                    $user['status'] = 'blocked';
                }
            }

            $finalArray[$user['username']] = $user['status'];
        }

        //--- Return JSON formatted response
        echo json_encode($finalArray);
    }
    
    function display_online_readers()
    {
        $data["online_readers"] = $this->member->get_online_readers();
        $html = $this->load->view('frontend/pages/online_readers', $data, TRUE);
        
        $return["status"] = "OK";
        $return["html"] = $html;
        
        echo json_encode($return);
        exit;
    }

    //thom ho8-6
    function get_chat_ended() {

        $chat_id = $this->input->post('data');
        $data['data'] = $this->db->query("SELECT * FROM chats WHERE id = {$chat_id} ")->row_array();
      
        echo json_encode($data);

    }

    function get_chat_ended2() {

        $chat_id = $this->input->post('data');
        $data['data'] = $this->db->query("SELECT * FROM chats WHERE id = {$chat_id} ")->row_array();
      
        echo json_encode($data);

    }

    function set_refused_reader_id() {

        $reader_id = $this->input->post('data');

        if ($this->session->userdata("reader_".$reader_id) !== null) {
            $this->session->unset_userdata("reader_".$reader_id);
        }
        
        $this->session->set_userdata("reader_".$reader_id, strtotime(date("Y-m-d H:i:s"),time()));

        $result["id"] = $this->session->userdata("reader_".$reader_id);

        echo json_encode($result);

    }

    function get_refused_result() {
        $reader_id = $this->input->post('data');
        if($this->session->userdata("reader_" . $reader_id) != null) {
            if((strtotime(date("Y-m-d H:i:s"),time())-$this->session->userdata("reader_" . $reader_id)) < 3600) {
                $result["status"] = "off";
                echo json_encode($result);
            } else {
                $this->session->unset_userdata("reader_".$reader_id);
                $result["status"] = "on";
                echo json_encode($result);
            }
        } else {
            $result["status"] = "on";
            echo json_encode($result);
        }
    }

    function no_reader_check($id)
    {
        $data = $this->db->query("SELECT * FROM messages WHERE `name`='no_reader' AND ricipient_id = '{$id}' AND `read` = 0")->result_array();
        $return['status'] = false;
        if(count($data)>0) {
            $return['status'] = true;
        }
        echo json_encode($return);
    }

    function check_online_readers()
    {
        $query = $this->db->query("
        
            SELECT members.`id`, member_profiles.status
            FROM members
            LEFT JOIN member_profiles ON members.`id` = member_profiles.`member_id`
            WHERE members.`type` = 'READER' AND (member_profiles.`status` = 'online' OR member_profiles.`status` = 'break' OR member_profiles.`status` = 'busy') AND members.deleted = 0 AND members.active = 1;
        ");

        $alert = $this->db->query("SELECT `message` FROM notification_messages WHERE subject='zero_reader'")->row_array();

        $all_online_readers = $query->result_array();

        $all = count($all_online_readers);

        $return['status'] = false;

        if($all == 0)
        {
            $return['status'] = true;
        }

        $return['alert'] = $alert;

        echo json_encode($return);
    }

    function check_disconnected()
    {
        $id = $this->input->post("id");
        $disconnect = $this->db->query("SELECT `disconnect` FROM member_profiles WHERE member_id='{$id}'")->row_array();
        $return['disconnect'] = $disconnect['disconnect'];

        echo json_encode($return);
    }

    function search_article()
    {
        $data = $this->input->post();
        $search_option = $data['search-option'];
        $result_array = [];
        $search_key = "";

        if(isset($this->member->data['profile_id']))
        {
            $this->db->from("articles");
            $this->db->join("categories", "categories.id = articles.category_id");
            $this->db->join("members", "members.id = articles.profile_id");
            $this->db->select("articles.*, categories.url as category_url, categories.title as category_title, members.username", false);
            $this->db->where("articles.url IS NOT NULL", null, false);
            $this->db->where("articles.approved", "1");
            $this->db->order_by('datetime', 'DESC');
            $getArticles = $this->db->get();
            $articleArray = $getArticles->result_array();
        }
        else
        {
            $this->db->from("articles");
            $this->db->join("categories", "categories.id = articles.category_id");
            $this->db->join("members", "members.id = articles.profile_id");
            $this->db->select("articles.*, categories.url as category_url, categories.title as category_title, members.username", false);
            $this->db->where("articles.url IS NOT NULL", null, false);
            $this->db->where("articles.approved", "1");
            $this->db->order_by('datetime', 'DESC');
            $this->db->limit(16);
            $getArticles = $this->db->get();
            $articleArray = $getArticles->result_array();
        }

        switch ($search_option)
        {
            case 'category':
                $search_key = $data['category'];
                foreach($articleArray as $article)
                {
                    if($search_key == $article['category_id'])
                    {
                        array_push($result_array, $article);
                    }
                }
                break;
            case 'author':
                $search_key = $data['author'];
                foreach($articleArray as $article)
                {
                    if($search_key == $article['profile_id'])
                    {
                        array_push($result_array, $article);
                    }
                }
                break;
            case 'title':
                $search_key = $data['title'];
                foreach($articleArray as $article)
                {
                    if(strpos($article['title'], $search_key) !== false)
                    {
                        array_push($result_array, $article);
                    }
                }
                break;
            case 'content':
                $search_key = $data['content'];
                foreach($articleArray as $article)
                {
                    if(strpos($article['content'], $search_key) !== false)
                    {
                        array_push($result_array, $article);
                    }
                }
                break;
            case 'date':
                $start_date = $data['start-date'];
                $end_date = $data['end-date'];
                foreach($articleArray as $article)
                {
                    if($this->check_in_range($start_date, $end_date, $article['datetime']) == true)
                    {
                        array_push($result_array, $article);
                    }
                }
                break;
        }

        $t['articles'] = $result_array;
        $t['count'] = count($result_array);
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Most Recent Articles",
            'header' => $this->load->view('frontend/layouts/header', $param_header, TRUE),
            'slider' => "",
            'content' => $this->load->view('/frontend/pages/article_search', $t, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/page', $data);
    }

    function check_in_range($start_date, $end_date, $date_from_user)
    {
        // Convert to timestamp
        $start_ts = strtotime($start_date);
        $end_ts = strtotime($end_date);
        $user_ts = strtotime($date_from_user);

        // Check that user date is between start & end
        return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
    }

}
