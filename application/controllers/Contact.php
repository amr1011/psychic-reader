<?php

class contact extends CI_Controller
{

    function __construct()
    {
        parent :: __construct();
        $this->load->helper('captcha');
        $this->settings = $this->system_vars->get_settings();
    }

    function index()
    {
        $getPage = $this->db->query("SELECT * FROM pages WHERE url = 'contact' LIMIT 1");
        $params = $getPage->row_array();

        $random_captcha = rand(10000, 99999);
        $this->session->set_userdata('captcha', $random_captcha);

        $vals = array
            (
            'word' => $random_captcha,
            'img_path' => './media/captcha/',
            'img_url' => site_url('/media/captcha/') . "/",
            'font_path' => './system/fonts/texb.ttf',
            'img_width' => '150',
            'img_height' => 30,
            'expiration' => 7200
        );

        $subjects = $this->db->query("SELECT * FROM contact_subject")->result_array();

        $cap = create_captcha($vals);
        $params['captcha'] = $cap['image'];
        $params['subjects'] = $subjects;
        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();
        
        $data = array(
            'title' => 'Psychic Contact',
            'header' => $this->load->view('frontend/layouts/header', $param_header, TRUE),
            'slider' => $this->load->view('frontend/layouts/slider', '', TRUE),
            'content' => $this->load->view('frontend/pages/contact', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );        
        
        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/page', $data);
    }

    function submit()
    {
        // $this->load->helper('form');
        // $this->load->library('form_validation');
        // $this->form_validation->set_rules('g-recaptcha-response', 'Captcha', 'required|trim|xss_clean');
        //
        // if (!$this->form_validation->run())
        // {
        //
        //     $this->index();
        // } else
        // {
            $name = $this->input->post("name");
            $email = $this->input->post("email");
            $phone = $this->input->post("phone");
            $subject = $this->input->post("subject");
            $comments = $this->input->post("comments");

            $now = date('Y-m-d H:i:s');

            $question_data = array(
                "name" => $name,
                "email" => $email,
                "phone" => $phone,
                "subject" => $subject,
                "comments" => $comments,
                "created_at" => $now
            );
            $insert = $this->db->insert("contact_question", $question_data);
            
            if($insert == 0){
                $this->session->set_flashdata('response_delete', "Error, Please try again");
            }else{
                $this->session->set_flashdata('response', "Thank you! Your message has been successfully sent");
            }
            redirect('/contact');
        // }
    }

    function send()
    { //david_v4
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $username = $this->input->post('username');
        $subject = $this->input->post('subject');
        $comments = $this->input->post('comments');


        $this->load->model('zendesk');
        $this->zendesk->create_new_ticket($name, $email, $subject, $comments, $phone, $username);
        //$this->session->set_flashdata('response', "Your contact inquiry has been submitted. Please allow up to 48 hours for someone to respond.");
        $data['response'] = "Your contact inquiry has been submitted. Please allow up to 48 hours for someone to respond.";
        $data['name'] = $name;
        $data['phone'] = $phone;
        $data['email'] = $email;
        $data['subject'] = $subject;
        $data['comments'] = $comments;
        $data['username'] = $username;
        echo json_encode($data);
    }

    function check_captcha($string = null)
    {

        if ($string)
        {

            $random_captcha = $this->session->userdata('captcha');

            if ($string == $random_captcha)
            {

                return true;
            } else
            {

                $this->form_validation->set_message('check_captcha', "The security code you entered is invalid");
                return false;
            }
        } else
        {

            return true;
        }
    }

}
