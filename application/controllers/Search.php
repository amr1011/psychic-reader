<?php

class search extends CI_Controller
{

    function __construct()
    {
        parent :: __construct();
    }

    function index()
    {

        $query = $this->input->post('query');

        $sql = "SELECT member_profiles.* 
                FROM members,
                     member_profiles 
                WHERE (first_name LIKE \"%{$query}%\" OR last_name LIKE \"%{$query}%\" OR members.id LIKE \"%{$query}%\") 
                AND expert = 1 AND member_profiles.member_id = members.id ";
        $getExperts = $this->db->query($sql);
        $t['experts'] = $getExperts->result_array();

        $this->load->view('frontend/partial/header');
        $this->load->view('frontend/pages/expert_list', $t);
        $this->load->view('frontend/partial/footer');
    }

}
