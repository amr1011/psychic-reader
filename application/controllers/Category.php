<?php

class category extends CI_Controller
{

    function __construct()
    {
        parent :: __construct();
    }

    function main($category_url)
    {

        // get all experts in this main category
        $featured_experts = array();
        $date = date("Y-m-d H:i:s");
        $sql = "SELECT p.* FROM member_profiles p,categories c,featured f WHERE (p.category_id = c.id AND c.url = '{$category_url}') AND (f.type = 'category' AND f.profile_id = p.member_id AND f.end_date >= '{$date}') GROUP BY p.member_id";
        $getTopExperts = $this->db->query($sql);
        $topExperts = $getTopExperts->result_array();

        foreach ($topExperts as $e) {
            $featured_experts[] = $e['id'];
        }
        foreach ($topExperts as $i => $e) {
            $topExperts[$i]['featured'] = 1;
        }

        $expert_string = implode(',', $featured_experts);

        // Get All other experts (exclude top experts)
        $sql = "SELECT p.* FROM member_profiles p, categories c WHERE p.category_id = c.id AND c.url = '{$category_url}' " . (trim($expert_string) ? "AND p.member_id NOT IN ({$expert_string})" : "") . " GROUP BY p.member_id ";
        $getStandardExperts = $this->db->query($sql);

        $t['experts'] = array_merge($topExperts, $getStandardExperts->result_array());

        $this->load->view('frontend/partial/header');
        $this->load->view('frontend/pages/expert_list', $t);
        $this->load->view('frontend/partial/footer');
    }

    function sub($category_url, $subcategory_url)
    {
        $date = date("Y-m-d H:i:s");

        // Get featured expertrs
        $sql = "SELECT
                        p.*
                FROM
                        member_profiles p,
                        subcategories c,
                        featured f
                WHERE
                        c.url = '{$subcategory_url}' AND 
                        EXISTS(SELECT * FROM profile_subcategories WHERE subcategory_id = c.id AND profile_id = p.member_id) AND 
                        (f.type = 'category' AND f.profile_id = p.member_id AND f.end_date >= '{$date}')
                GROUP BY
                        p.member_id";
        $getFeaturedExperts = $this->db->query($sql);

        $featured_experts = array();
        $topExperts = $getFeaturedExperts->result_array();

        foreach ($topExperts as $e) {
            $featured_experts[] = $e['id'];
        }
        foreach ($topExperts as $i => $e) {
            $topExperts[$i]['featured'] = 1;
        }

        $expert_string = implode(',', $featured_experts);

        // Get all other exprts MINUS top expets
        $sql = "SELECT
                        p.*
                FROM
                        member_profiles p,
                        subcategories c
                WHERE
                        " . (trim($expert_string) ? "p.member_id NOT IN ({$expert_string}) AND " : "") . "
                        c.url = '{$subcategory_url}' AND 
                        EXISTS(SELECT * FROM profile_subcategories WHERE subcategory_id = c.id AND profile_id = p.member_id)
                GROUP BY p.member_id";
        $getExperts = $this->db->query($sql);

        $t['experts'] = array_merge($topExperts, $getExperts->result_array());

        $this->load->view('frontend/partial/header');
        $this->load->view('frontend/pages/expert_list', $t);
        $this->load->view('frontend/partial/footer');
    }

}
