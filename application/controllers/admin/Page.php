<?php

class Page extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Page_model');
    } 

    /*
     * Listing of pages
     */
    function index()
    {
        $data['pages'] = $this->Page_model->get_all_pages();        
        $data['view_template'] = 'admin/page/index';   
        $this->load->view('admin/template',$data);
    }


    /*
     * Adding a new page
     */
    function add()
    {   
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title','Title','required|is_unique[title]');        
        $this->form_validation->set_rules('content','Content','required');
                
        if($this->form_validation->run())     
        {   
            $params = array(
                'title' => $this->input->post('title'),
                'url' => $this->input->post('url'),
                'add_to_footer' => $this->input->post('add_to_footer'),
                'add_to_homepage' => $this->input->post('add_to_homepage'),
                'sort' => $this->input->post('sort'),
                'content' => $this->input->post('content'),
            );
            
            $page_id = $this->Page_model->add_page($params);
            redirect('admin/page/index');
        }
        else
        {            
            $data['view_template'] = 'admin/page/add';
            $this->load->view('admin/template',$data);
        }
    }  



    
    /*
     * Editing a page
     */
    function edit($id)
    {   
        // check if the page exists before trying to edit it
        $data['page'] = $this->Page_model->get_page($id);
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title','Title','required|is_unique[title]');        
        $this->form_validation->set_rules('content','Content','required');
        
        if(isset($data['page']['id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
                    'title' => $this->input->post('title'),
                    'url' => $this->input->post('url'),
                    'add_to_footer' => $this->input->post('add_to_footer'),
                    'add_to_homepage' => $this->input->post('add_to_homepage'),
                    'sort' => $this->input->post('sort'),
                    'content' => $this->input->post('content'),
                );
                $this->Page_model->update_page($id,$params);            
                redirect('admin/page/index');
            }
            else
            {
                $data['view_template'] = 'admin/page/edit';
                $this->load->view('admin/template',$data);
            }
        }
        else
            show_error('The page you are trying to edit does not exist.');
    } 

    /*
     * Deleting page
     */
    function remove($id)
    {
        $page = $this->Page_model->get_page($id);

        // check if the page exists before trying to delete it
        if(isset($page['id']))
        {
            $this->Page_model->delete_page($id);
            redirect('admin/page/index');
        }
        else
            show_error('The page you are trying to delete does not exist.');
    }
    
}

