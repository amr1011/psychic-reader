<?php
/**
 * Created by PhpStorm.
 * User: Thom
 * Date: 4/8/2019
 * Time: 18:26
 */

class email_form extends CI_Controller
{
    function __construct()
    {

        parent::__construct();
        $this->settings = $this->system_vars->get_settings();

        $this->session->set_userdata('member_logged', 1);

        if (!$this->session->userdata('admin_is_logged')) {
            redirect('/admin/login');
            exit;
        }

        $this->error = null;
        $this->response = null;
        $this->open_nav = false;
        $this->admin = $this->session->userdata('admin_is_logged');

        $this->load->model("messages_model");
    }

    function index()
    {
        $params['title'] = "Email Forms";
        $params['data'] = $this->db->query("SELECT * FROM email_forms")->result_array();
        $page['view_template'] = $this->load->view('admin/email_form/index', $params, true);
        $this->load->view('admin/template', $page);
    }

    function add()
    {
        $page['view_template'] = $this->load->view('admin/email_form/add', null, true);
        $this->load->view('admin/template', $page);
    }

    function edit($id)
    {
        $params['form_id'] = $id;
        $params['form_data'] = $this->db->query("SELECT * FROM email_forms WHERE id={$id}")->row_array();
        $params['form_detail_data'] = $this->db->query("SELECT * FROM email_forms_detail WHERE form_id={$id}")->result_array();
        $page['view_template'] = $this->load->view('admin/email_form/edit', $params, true);
        $this->load->view('admin/template', $page);
    }

    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('email_forms');

        $this->db->where('form_id', $id);
        $this->db->delete('email_forms_detail');
        $this->session->set_flashdata("response_delete", "Email form has been deleted successfully.");
        redirect('/admin/email_form/index');
    }

    function upload_file()
    {
        if (!is_dir('media/responders')) {

            mkdir('media/responders', 0777, TRUE);
        }

        $config['upload_path'] = './media/responders/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png|iso|dmg|zip|rar|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|rtf|sxc|sxi|txt|exe|avi|mpeg|mp3|mp4|3gp';
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('responder-img')) {
            return $this->upload->data();
        } else {
            return $this->upload->display_errors();
        }
    }

    function add_new_form()
    {
        $response = $this->input->post();
        $file_data = $this->upload_file();
        $responder_image = null;
        if (!is_string($file_data)) {
            $responder_image = $file_data['file_name'];
        }

        $pages = $response['page'];
        $date = date('Y-m-d');
        $num_added_forms = count($pages);

        $existing_pages = array();
        $existing_data = $this->db->query("SELECT page FROM email_forms")->result_array();

        foreach ($existing_data as $existing_page) {
            array_push($existing_pages, $existing_page['page']);
        }

        foreach ($pages as $page) {

            if (!in_array($page, $existing_pages)) {
                $form_data = array(
                    'page' => $page,
                    'page_name' =>$response['name-page'],
                    'header' => $response['header'],
                    'footer' => $response['footer'],
                    'popup' => $response['popup'],
                    'responder_name' => $response['name-responder'],
                    'responder_content' => $response['responder-content'],
                    'responder_image' => $responder_image,
                    'created_at' => $date
                );
                $this->db->insert('email_forms', $form_data);
            } else {
                $num_added_forms = $num_added_forms - 1;
            }
        }


        $email_forms_ids = $this->db->query("(SELECT id FROM email_forms ORDER BY id DESC LIMIT {$num_added_forms}) ORDER BY id ASC")->result_array();

        foreach ($email_forms_ids as $email_forms_id) {

            $i = 0;
            for ($index = 0; $index < count($response['label']); $index++) {
                if ($response['type'][$index] == 'list') {

                    $form_detail_data = array(
                        'form_id' => $email_forms_id['id'],
                        'item_type' => $response['type'][$index],
                        'label' => $response['label'][$index],
                        'data' => $response['list'][$i],
                        'created_at' => $date
                    );
                    $i = $i + 1;

                } else {
                    $form_detail_data = array(
                        'form_id' => $email_forms_id['id'],
                        'item_type' => $response['type'][$index],
                        'label' => $response['label'][$index],
                        'data' => null,
                        'created_at' => $date
                    );
                }
                $this->db->insert('email_forms_detail', $form_detail_data);
            }
        }
        if ($num_added_forms > 0) {
            $this->session->set_flashdata("response_add", "Email form has been created successfully.");
        } else {
            $this->session->set_flashdata("response_exist", "Page is already existing.");
        }
        redirect('/admin/email_form/index');
    }

    function get_email_form_detail()
    {
        $form_id = $this->input->post('form_id');
        $form_detail = $this->db->query("SELECT * FROM email_forms_detail WHERE form_id={$form_id}")->result_array();

        $return["data"] = $form_detail;

        echo json_encode($return);

    }

    function save_edit_form($id)
    {
        $response = $this->input->post();

        $file_data = $this->upload_file();
        $responder_image = null;

        if (!is_string($file_data)) {
            $responder_image = $file_data['file_name'];
            $update_data = array(
                'page_name' => $response['name-page'],
                'header' => $response['header'],
                'footer' => $response['footer'],
                'popup' => $response['popup'],
                'responder_name' => $response['name-responder'],
                'responder_content' => $response['responder-content'],
                'responder_image' => $responder_image
            );
        } else {
            $update_data = array(
                'page_name' => $response['name-page'],
                'header' => $response['header'],
                'footer' => $response['footer'],
                'popup' => $response['popup'],
                'responder_name' => $response['name-responder'],
                'responder_content' => $response['responder-content']
            );
        }
        $this->db->where('id', $id);
        $this->db->update('email_forms', $update_data);

        // email_forms_detail update
        $date = date('Y-m-d');
        $i = 0;

        $this->db->where('form_id', $id);
        $this->db->delete('email_forms_detail');

        for ($index = 0; $index < count($response['label']); $index++) {
            if ($response['type'][$index] == 'list') {

                $form_detail_data = array(
                    'form_id' => $id,
                    'item_type' => $response['type'][$index],
                    'label' => $response['label'][$index],
                    'data' => $response['list'][$i],
                    'created_at' => $date
                );
                $i = $i + 1;
            } else {
                $form_detail_data = array(
                    'form_id' => $id,
                    'item_type' => $response['type'][$index],
                    'label' => $response['label'][$index],
                    'data' => null,
                    'created_at' => $date
                );
            }
            $this->db->insert('email_forms_detail', $form_detail_data);
        }

        $this->session->set_flashdata("response_update", "Email form has been updated successfully.");
        redirect('/admin/email_form/index');
    }
}