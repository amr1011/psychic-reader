<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pages extends CI_Controller
{

    function __construct()
    {

        parent::__construct();

        if (!$this->session->userdata('admin_is_logged'))
        {
            redirect('/admin/Login');
            exit;
        }
        $this->load->helper('inflector');
        $this->results_per_page = 100;
        $this->open_nav = null;
        $this->response = null;
        $this->error = null;
        $this->admin = $this->session->userdata('admin_is_logged');
        $this->member_data = '';
    }

    function getMember($table , $addlogic = null){
        $sql = "SELECT members.id, members.username, members.email FROM {$table} 
                LEFT JOIN member_profiles ON member_profiles.member_id = members.id
                WHERE member_profiles.member_id IS NOT NULL AND {$table}.deleted = 0 {$addlogic} ";
        $getData = $this->db->query($sql);
        return $this->member_data = $getData->result_array();
        
    }

    function getMemberJoinEmailReaders(){
        $sql = "SELECT members.id, members.username, err.first_q , err.second_q, err.third_q, err.fourth_q, err.fifth_q , err.special, err.is_active, err.date_created FROM members LEFT JOIN member_profiles ON member_profiles.member_id = members.id LEFT JOIN email_reading_readers as err ON err.member_id = members.id WHERE member_profiles.member_id IS NOT NULL AND members.deleted = 0";
        $getData = $this->db->query($sql);
        return $this->member_data = $getData->result_array();

    }

    function getTransaction(){
       $id = $this->input->post('id');
       
       $this->load->model('dashboard');
       $data = $this->dashboard->get_single_transaction($id);

        $response = array(
            "status" => true,
            "message" => "",
            "data" => $data
        );

        header("Content-Type:application/json;");
        echo json_encode($response);
    }
    

    function index($mainNavId = null, $subNavId = null)
    {
        // START Main Navigation Selection
        $subNavId = ($subNavId == '0' ? null : $subNavId);

        $sql = "SELECT * FROM vadmin_nav WHERE id = {$mainNavId} LIMIT 1";
        $getMainNav = $this->db->query($sql);
        $data['nav'] = $getMainNav->row_array();
        
        
        $this->open_nav = $data['nav']['id'];

        if (!$subNavId)
            $data['subnav'] = false;
        else
        {
            $sql = "SELECT * FROM vadmin_navsub WHERE id={$subNavId} LIMIT 1";
            $getSubNav = $this->db->query($sql);
            $data['subnav'] = $getSubNav->row_array();
            
        }
        
        // Get the right content
        if (!$subNavId)
        {
            // Get Straight From Table
            $table = $data['nav']['table'];

            $where = (isset($data['nav']['where']) && trim($data['nav']['where']) ? "WHERE " . $data['nav']['where'] : "");
            $sql = "SELECT id FROM {$table} {$where}";
            $totalRecords = $this->db->query($sql);
            $limiter = ($this->uri->segment('6') ? $this->uri->segment('6') : "0");
            print_r(__LINE__);
            // Paginate Results
            $config['base_url'] = "/'admin/main/pages/{$mainNavId}/" . (isset($data['subnav']['id']) ? $data['subnav']['id'] : '0') . "/";
            $data['total_results'] = $config['total_rows'] = $totalRecords->num_rows();
            $config['per_page'] = $this->results_per_page;
            $config['uri_segment'] = 6;
            $config['num_links'] = 5;

            $this->pagination->initialize($config);
            // $sql = "SELECT * FROM {$table} {$where} LIMIT {$limiter}, {$config['per_page']}";
            //thom
            if($table == "chats") {

                $where = "WHERE ended = 0";
                $sql = "SELECT * FROM {$table} {$where} ORDER BY start_datetime DESC LIMIT {$limiter}, 20";

            } else {

                $sql = "SELECT * FROM {$table}";
            }
            
            $getData = $this->db->query($sql);

            if ($getData->num_rows() == 0)
            {
                $data['data'] = false;
                $data['pagination'] = false;
            } else
            {
                $data['data'] = $getData->result_array();
                $data['pagination'] = $this->pagination->create_links();

            }
        } else
        {
            $table = ($data['subnav']['table'] ? $data['subnav']['table'] : $data['nav']['table']);
            $where = ($data['subnav']['where'] ? "WHERE " . $data['subnav']['where'] : "");
            
            // check for special tags
            $where = str_replace("%current_date%", date("Y-m-d H:i:s"), $where);
            $where = str_replace("%yesterdays_date%", date("Y-m-d H:i:s", strtotime("-1 day")), $where);
           
            $sql = "SELECT * FROM {$table} {$where}";
            
            $totalRecords = $this->db->query($sql);
            $limiter = ($this->uri->segment('6') ? $this->uri->segment('6') : "0");
            
            // Paginate Results
            $config['base_url'] = "/'admin/main/pages/{$mainNavId}/" . (isset($data['subnav']['id']) ? $data['subnav']['id'] : $data['nav']['id']) . "/";
            $data['total_results'] = $config['total_rows'] = $totalRecords->num_rows();
            $config['per_page'] = $this->results_per_page;
            $config['uri_segment'] = 6;
            $config['num_links'] = 5;
            
            $this->pagination->initialize($config);

            // Get data
            $sql = "SELECT * FROM {$table} {$where} LIMIT {$limiter}, {$config['per_page']}";

            $getData = $this->db->query($sql);
            
            if ($getData->num_rows() == 0)
            {
                $data['data'] = false;
                $data['pagination'] = false;
            } else
            {

                $data['data'] = $getData->result_array();
                $data['pagination'] = $this->pagination->create_links();

                if($mainNavId == 6 && $subNavId == 19)
                {
                    $sql = "SELECT members.id, members.username, members.first_name, members.last_name, members.dob,members.gender, members.email, members.country, members.paypal_email,
                                members.registration_date, billing_profiles.card_name, billing_profiles.card_number,
                                billing_profiles.customer_id, billing_profiles.payment_id, billing_profiles.address, billing_profiles.city,
                                billing_profiles.state, billing_profiles.zip
                        FROM members LEFT JOIN billing_profiles ON members.id = billing_profiles.member_id WHERE (members.`type` IS NULL OR members.`type` = 'CLIENT') AND members.deleted=0";

                    $getData = $this->db->query($sql);
                    $data['data'] = $getData->result_array();
                }
                if($mainNavId == 6 && $subNavId == 22)
                {
                    $sql = "SELECT members.id, members.username, members.first_name, members.last_name, members.dob,members.gender, members.email
                        FROM members WHERE members.`type` IS NULL AND members.deleted=0";

                    $members = $this->db->query("SELECT `banned` FROM members WHERE `type` IS NULL AND deleted = 0")->result_array();
                    $num_banned_user = 0;
                    $num_all_user = count($members);
                    foreach($members as $member)
                    {
                        if($member['banned'] == 1)
                        {
                            $num_banned_user ++;
                        }
                    }

                    if($num_all_user == $num_banned_user)
                    {
                        $data['all_ban'] = 1;
                    } else
                    {
                        $data['all_ban'] = 0;
                    }

                    $getData = $this->db->query($sql);
                    $data['data'] = $getData->result_array();
                }

                if($mainNavId == 37 && $subNavId == 46)
                {
                    $sql = "SELECT profile_categories.id, members.username, categories.title FROM profile_categories 
                        LEFT JOIN members ON members.id=profile_categories.profile_id
                        LEFT JOIN categories ON categories.id=profile_categories.category_id
                        ORDER BY profile_categories.id
                        ";

                    $getData = $this->db->query($sql);
                    $data['data'] = $getData->result_array();

                    $member_username = $this->db->query("SELECT DISTINCT members.username FROM profile_categories 
                                                    LEFT JOIN members ON members.id=profile_categories.profile_id
                                                    LEFT JOIN categories ON categories.id=profile_categories.category_id
                                                    ORDER BY profile_categories.id
                                                    ")->result_array();
                    $categories = $this->db->query("SELECT title FROM categories")->result_array();
                    $data['member_username'] = $member_username;
                    $data['categories'] = $categories;
                }

                if($mainNavId == 36 && $subNavId == 55)
                {
                    $sql = "SELECT * FROM message_center_notifications";
                    $pending_messages = $this->db->query($sql)->result_array();
                    $new_pending_messages = [];

                    foreach($pending_messages as $pending_message)
                    {
                        $sender_id = $pending_message['from'];
                        $recipient_id = $pending_message['to'];
                        $sender_name = $this->db->query("SELECT username FROM members WHERE `id` = {$sender_id}")->row_array();
                        $sender_name = $sender_name['username'];
                        $recipient_name = $this->db->query("SELECT username FROM members WHERE `id` = {$recipient_id}")->row_array();
                        $recipient_name = $recipient_name['username'];

                        $temp_message = [];

                        foreach($pending_message as $key => $item)
                        {
                            $temp_message[$key] = $item;
                        }
                        $temp_message['sender_name'] = $sender_name;
                        $temp_message['recipient_name'] = $recipient_name;

                        array_push($new_pending_messages, $temp_message);

                    }

                    $data['data1'] = $new_pending_messages;

                }

                if($mainNavId == 36 && $subNavId == 56)
                {
                    $sql = "SELECT * FROM message_center_group_notifications";
                    $pending_messages = $this->db->query($sql)->result_array();
                    $new_pending_messages = [];

                    foreach($pending_messages as $pending_message)
                    {
                        $sender_id = $pending_message['from'];
                        $recipients = explode(",", $pending_message['to']);

                        $sender_name = $this->db->query("SELECT username FROM members WHERE `id` = {$sender_id}")->row_array();
                        $sender_name = $sender_name['username'];

                        $to = "";

                        foreach($recipients as $recipient)
                        {
                            $recipient_name = $this->db->query("SELECT username FROM members WHERE `id` = {$recipient}")->row_array();
                            $recipient_name = $recipient_name['username'];

                            $to =$to . $recipient_name . ", ";
                        }

                        $temp_message = [];

                        foreach($pending_message as $key => $item)
                        {
                            $temp_message[$key] = $item;
                        }
                        $temp_message['sender_name'] = $sender_name;
                        $temp_message['recipient_name'] = $to;

                        array_push($new_pending_messages, $temp_message);

                    }

                    $data['data1'] = $new_pending_messages;

                }

                if($mainNavId == 12 && $subNavId == 57)
                {
                    $data['subnav']['new_title'] = 'Sites';

                }

                if($mainNavId == 13 && $subNavId == 60)
                {
                    $members = $this->db->query("SELECT * FROM members WHERE (`type` <> 'READER' AND `type` <> 'ADMIN') OR `type` IS NULL ")->result_array();
                    $data['members'] = $members;
                }
// start new menu
                if($mainNavId == 12 && $subNavId == 65)
                {
                    $appointments =  $this->db->query("SELECT * FROM appointment")->result_array();
                    $new_appointments_array = [];

                    foreach($appointments as $appointment)
                    {
                        $new_array = [];
                        $reader_username = $this->db->query("SELECT members.username FROM members WHERE id = {$appointment['reader_id']}")->row_array();
                        $client_username = $this->db->query("SELECT members.username FROM members WHERE id = {$appointment['client_id']}")->row_array();
                        foreach($appointment as $key=>$item)
                        {
                            $new_array[$key] = $item;
                        }

                        $new_array['reader_username'] = $reader_username['username'];
                        $new_array['client_username'] = $client_username['username'];

                        array_push($new_appointments_array, $new_array);
                    }

                    $data['data'] = $new_appointments_array;
                }

                if($mainNavId == 6 && $subNavId == 69)
                {
                    $time_backs = $this->db->query("SELECT * FROM reader_giveback")->result_array();
                    $new_backtime_array = [];

                    foreach($time_backs as $time_back)
                    {
                        $new_array = [];
                        $reader_username = $this->db->query("SELECT members.username FROM members WHERE id = '{$time_back['reader_id']}'")->row_array();
                        $client_username = $this->db->query("SELECT members.username FROM members WHERE id = '{$time_back['client_id']}'")->row_array();
                        foreach($time_back as $key=>$item)
                        {
                            $new_array[$key] = $item;
                        }

                        $new_array['reader_username'] = $reader_username['username'];
                        $new_array['client_username'] = $client_username['username'];

                        array_push($new_backtime_array, $new_array);
                    }

                    $data['data'] = $new_backtime_array;
                }
                if($mainNavId == 6 && $subNavId == 68)
                {
                    $sql = "SELECT transactions.*, members.username, members.country FROM transactions 
                            LEFT JOIN members 
                            ON members.id = transactions.member_id 
                            WHERE members.country='CA' 
                            AND transactions.currency='USD'
                            AND transactions.payment_type IS NOT NULL
                            ORDER BY transactions.id DESC";
                    $transactions = $this->db->query($sql)->result_array();
                    $data['data'] = $transactions;
                }

                if($mainNavId == 12 && $subNavId == 87)
                {
                    $sql = "SELECT chats.id, reader_id, client_id, topic, length, create_datetime FROM chats LEFT JOIN members ON members.id = chats.reader_id WHERE members.active=1 AND members.deleted=0";
                    $chats = $this->db->query($sql)->result_array();

                    $new_chat_array = [];
                    foreach($chats as $chat)
                    {
                        $temp_data = [];
                        $reader_name = $this->db->query("SELECT username from members where id={$chat['reader_id']}")->row_array();
                        $reader_name = $reader_name['username'];
                        $client_name = $this->db->query("SELECT username from members where id={$chat['client_id']}")->row_array();
                        $client_name = $client_name['username'];

                        foreach($chat as $key => $item)
                        {
                            $temp_data[$key] = $item;
                        }
                        $temp_data['reader_name'] = $reader_name;
                        $temp_data['client_name'] = $client_name;
                        $logout_time  = strtotime($chat['create_datetime']) + $chat['length'];
                        $temp_data['logout_time'] = date('Y-m-d H:i:s', $logout_time);

                        array_push($new_chat_array, $temp_data);
                    }
                    $readers_chat_room = $this->db->query("SELECT username FROM members WHERE type='READER' AND active=1 AND deleted=0")->result_array();

                    $data['data'] = $new_chat_array;
                    $data['readers_chat_room'] = $readers_chat_room;
                }

                if($mainNavId == 20 && $subNavId == 77)
                {
                    $sql = "SELECT * FROM chats";
                    $chats = $this->db->query($sql)->result_array();

                    $new_chat_array = [];
                    foreach($chats as $chat)
                    {
                        $temp_data = [];
                        $reader_name = $this->db->query("SELECT username from members where id={$chat['reader_id']}")->row_array();
                        $reader_name = $reader_name['username'];
                        $client_name = $this->db->query("SELECT username from members where id={$chat['client_id']}")->row_array();
                        $client_name = $client_name['username'];

                        $chat_id = $chat['id'];
                        $transaction = $this->db->query("SELECT amount,currency FROM transactions WHERE `chat_id`={$chat_id}")->row_array();

                        foreach($chat as $key => $item)
                        {
                            $temp_data[$key] = $item;
                        }
                        $temp_data['reader_name'] = $reader_name;
                        $temp_data['client_name'] = $client_name;
                        $temp_data['transaction_summary'] = $transaction;

                        array_push($new_chat_array, $temp_data);
                    }
                    $readers_chat_room = $this->db->query("SELECT username FROM members WHERE type='READER' AND active=1 AND deleted=0")->result_array();

                    $data['data'] = $new_chat_array;
                    $data['readers_chat_room'] = $readers_chat_room;
                }
            }
        }
        
        // Get Fields
        $customFields = $this->vadmin->get_field_spec($table, 'show_overview_fields');


        if ($customFields)
        {
            if($mainNavId == 6 && $subNavId == 19)
            {
                $fields = "id,username,first_name,last_name,dob,gender,email,card_number,customer_id,address,city,country,paypal_email,registration_date";
                $fieldArray = explode(',', trim($fields));
            } else if($mainNavId == 6 && $subNavId == 22)
            {
                $fields = "id,username,first_name,last_name,dob,gender,email";
                $fieldArray = explode(',', trim($fields));
            } else if($mainNavId == 6 && $subNavId == 66)
            {
                $fields = "id,username,registration_date,last_login_date ";
                $fieldArray = explode(',', trim($fields));
            } else if($mainNavId == 6 && $subNavId == 67)
            {
                $fields = "id,username,first_name,last_name,registration_date,last_login_date,deleted_date";
                $fieldArray = explode(',', trim($fields));
            } else if($mainNavId == 37 && $subNavId == 46)
            {
                $fields = "id,username,title";
                $fieldArray = explode(',', trim($fields));
            }
            else
            {
                $fieldArray = explode(',', trim($customFields['value']));
            }
        }
        else
        {
            $fieldArray = $this->db->list_fields($table);

            if($mainNavId == 12 && $subNavId == 65){
                $fields = "id,reader_username,client_username,appointment_date,checked,admin_checked,confirm,decline,decline_checked,admin_decline_checked,status";
                $fieldArray = explode(',', trim($fields));
            }
        }

        $data['fields'] = $fieldArray;
        $data['specs'] = $this->vadmin->get_table_specs($table);
        $data['countries'] = $this->db->query("SELECT `code`,`name` FROM countries")->result_array();

        //Readers Visiblity tool
        if($mainNavId == 12 && $subNavId == 57)
        {
            $datas = $data['data'];
            $data['data'] = $this->readers_visibility_tool($datas);
        }
        //end of Readers Visibility Tool

        //Email Reading Readers
        else if($mainNavId == 12 && $subNavId == 58){
            $getData = $this->getMemberJoinEmailReaders();
            $data['data'] = $getData;
            $data['fields'] = array('is_active','first_q', 'first_q', 'first_q','first_q','first_q','first_q');
        }
        //End of Email Reading Readers


        # Sort Records
        if (isset($data['specs']['default_sort']['value']))
        {
            if (strpos($data['specs']['default_sort']['value'], '||') === false)
            {
                $sorting_var = trim($data['specs']['default_sort']['value']);
                $ascordesc = "";
            } else
            {
                list($sorting_var, $ascordesc) = explode("||", $data['specs']['default_sort']['value']);
                $ascordesc = trim(strtolower($ascordesc));
            }

            $data['data'] = $this->system_vars->subval_sort($data['data'], trim($sorting_var));
            if ($ascordesc == 'desc')
                $data['data'] = array_reverse($data['data']);
        }

        $data["add_link"] = "/admin/pages/add/{$data["nav"]['id']}/". ($data["subnav"]['id'] ? $data["subnav"]['id'] : '0');

        if ($data['data'])
        {
            foreach($data['data'] as $table)
            {
                $data["edit_link"][$table['id']] = "/admin/pages/edit/{$data["nav"]['id']}/" . ($data["subnav"]['id'] ? $data["subnav"]['id'] : '0') . "/{$table['id']}";
            }
        }

        // Load views
        if($mainNavId == 12 && $subNavId == 58){
            $page["view_template"] = $this->load->view('admin/readers/index', $data, true);
        }else if($mainNavId == 20 && $subNavId == 77){
            $page["view_template"] = $this->load->view('admin/admin_chat/index', $data, true);
        }else{
            $page["view_template"] = $this->load->view('admin/pages/index', $data, true);
        }
        $this->load->view('admin/template', $page);
    }

    public function show_content($main_id = null, $sub_id = null)
    {
        $this->load->model('model_name');
        $this->model_name->get_list();
    }

    function add($mainNavId = null, $subNavId = null)
    {
        // START Main Navigation Selection
        $subNavId = ($subNavId == '0' ? null : $subNavId);

        $sql = "SELECT * FROM vadmin_nav WHERE id = {$mainNavId} LIMIT 1";
        $getMainNav = $this->db->query($sql);
        $data['nav'] = $getMainNav->row_array();

        $this->open_nav = $data['nav']['id'];

        if (!$subNavId)
            $data['subnav'] = false;
        else
        {
            $sql = "SELECT * FROM vadmin_navsub WHERE id={$subNavId} LIMIT 1";
            $getSubNav = $this->db->query($sql);
            $data['subnav'] = $getSubNav->row_array();
        }

        // Get Table name
        $table = ($data['subnav']['table'] ? $data['subnav']['table'] : $data['nav']['table']);

        // Get Record Data
        $data['data'] = false;

        // Get Table Fields
        $data['fields'] = $fields = $this->db->list_fields($table);
        $data['specs'] = $this->vadmin->get_table_specs($table);
        $data['sidemodules'] = false;

        $data["form_action"] = "/admin/pages/submit/{$data['nav']['id']}/" . ($data['subnav']['id'] ? $data['subnav']['id'] : '0');
        // Load views

        //Readers Visiblity tool
        if($mainNavId == 12 && $subNavId == 57)
        {
            $table = 'members';

            //get the readers
            $getData = $this->getMember($table);          

            //checkbox
            $newval = 'CB||diff_val||';
            $newval_sb = 'SB||diff_val||';
            foreach($getData as $value){
                $newval.= $value['id'].','.$value['username'].'||';
                $newval_sb .= $value['id'].','.$value['username'].'||';
            }
            $data['specs']['spec']['reader_ids']['value'] = $newval;
            $data['specs']['spec']['main_reader']['value'] = $newval_sb;

        }
        //end of Readers Visibility Tool
        else if($mainNavId == 7 && $subNavId == 14){
            $data['data']['type'] = 'Readers';
        }

        else if($mainNavId == 7 && $subNavId == 15){
            $data['data']['type'] = 'Clients';
        }
        

        $page['view_template'] = $this->load->view('admin/pages/add', $data, true);
        $this->load->view('admin/template', $page);
    }

    function ban_member($mainNavId = null, $subNavId = null, $userId = '', $isBanned)
    {
        $this->db->where("id", $userId);
        $this->db->update("members", ["banned" => $isBanned]);

        $this->db->where("member_id", $userId);
        $this->db->delete('member_bans');
        redirect("/admin/pages/index/{$mainNavId}/{$subNavId}");
    }

    function submit($mainNavId = null, $subNavId = null)
    {
        $subNavId = ($subNavId == '0' ? null : $subNavId);

        // Get Table Info
        $sql = "SELECT * FROM vadmin_nav WHERE id = {$mainNavId} LIMIT 1";
        $getMainNav = $this->db->query($sql);
        $nav = $getMainNav->row_array();

        // Subnav
        if (!$subNavId)
            $data['subnav'] = false;
        else
        {
            $sql = "SELECT * FROM vadmin_navsub WHERE id={$subNavId} LIMIT 1";
            $getSubNav = $this->db->query($sql);
            $subnav = $getSubNav->row_array();
        }

        $table = (isset($subnav['table']) ? $subnav['table'] : $nav['table']);
        $title = (isset($subnav['title']) ? $subnav['title'] : $nav['title']);

        // Get Fields
        $fieldArray = $this->db->list_fields($table);
        $specs = $this->vadmin->get_table_specs($table);
        $updateArray = array();



        // Get Hidden Fields
        $hidden_fields = array();

        if (isset($specs['hide_fields']))
        {
            $hidden_fields = explode(',', $specs['hide_fields']['value']);
        }

        $this->load->library('form_validation');



        foreach ($fieldArray as $f) {
            
            if (in_array($f, $hidden_fields))
            {

            } else
            {
                // Get Field Based On SPEC Data
                $spec_type = strtolower((isset($specs['spec'][$f]) ? $specs['spec'][$f]['value'] : "TB||50"));
                $spec_array = explode("||", $spec_type);

                

                // Load & Configure The Module
                $specMod = trim($spec_array[0]);
                if ($f == 'id')
                    $specMod = 'lb';

                // Load Field
                $this->$specMod->config($f, $this->input->post($f), $spec_array);
                $fieldValue = $this->$specMod->process_form();
                $isRequired = isset($this->$specMod->isRequired) ? $this->$specMod->isRequired : false;

                

                // Create Update Array
                if ($fieldValue != '[%skip%]')
                {
                    $updateArray[$f] = $fieldValue;
                }

                $this->form_validation->set_rules($f, $f, 'trim');
                if ($isRequired)
                {
                    $this->form_validation->set_rules($f, $this->lang->line($f, FALSE), 'required');
                }
            }
        }

        if ($this->form_validation->run())
        {   
           

            if($updateArray["title"] != "Register" && $updateArray["title"] != "Our Psychics" && $updateArray["title"] != "Phone Readings" && $updateArray["title"] != "Email Readings")
            {
                $this->db->insert($table, $updateArray);
                $recordId = $this->db->insert_id();

                $this->session->set_flashdata('response', "Record #{$recordId} has been added to {$title}");
                redirect("/admin/pages/index/{$mainNavId}/{$subNavId}");
            } else
            {
                redirect("/admin/pages/add/{$mainNavId}/{$subNavId}");
            }

        }

        redirect("/admin/pages/add/{$mainNavId}/{$subNavId}");
    }

    function edit($mainNavId = null, $subNavId = null, $recordId = null)
    {
        // START Main Navigation Selection
        $subNavId = ($subNavId == '0' ? null : $subNavId);

        $sql = "SELECT * FROM vadmin_nav WHERE id = {$mainNavId} LIMIT 1";
        $getMainNav = $this->db->query($sql);
        $nav = $getMainNav->row_array();

        $this->open_nav = $nav['id'];

        if (!$subNavId)
            $subnav = false;
        else
        {
            $sql = "SELECT * FROM vadmin_navsub WHERE id={$subNavId} LIMIT 1";
            $getSubNav = $this->db->query($sql);
            $subnav = $getSubNav->row_array();
        }

        $data['nav'] = $nav;
        $data['subnav'] = $subnav;
        // Get Table name
        $table = ($data['subnav']['table'] ? $data['subnav']['table'] : $data['nav']['table']);

        // Get Record Data
        $sql = "SELECT * FROM {$table} WHERE id = {$recordId} LIMIT 1";
        $getData = $this->db->query($sql);
        $row = $getData->row_array();
        $data['data'] = $row;

        // Get Table Fields
        $data['fields'] = $fields = $this->db->list_fields($table);
        $specs = $this->vadmin->get_table_specs($table);
        $data['specs'] = $specs;
        $data['sidemodules'] = false;

        if($mainNavId == 20)
        {
            $chat_id = $row['id'];
            $transaction = $this->db->query("SELECT transaction_id FROM transactions WHERE `chat_id`={$chat_id}")->row_array();
            $transaction_id = $transaction['transaction_id'];
            $data['transaction_id'] = $transaction_id;

        }

        // Check for any sideModules
        if (isset($data['specs']['sidemodule']))
        {
            $moduleArray = explode('**', trim($data['specs']['sidemodule']['value']));
            $data['sidemodules'] = $moduleArray;
        }


        $hidden_fields = array();

        if (isset($specs['hide_fields']))
        {
            $hidden_fields = explode(',', $specs['hide_fields']['value']);
        }

        //Readers Visiblity tool
        if($mainNavId == 12 && $subNavId == 57)
        {
            $table = 'members';

            //get the readers
            $getData = $this->getMember($table);          

            //checkbox
            $newval = 'CB||diff_val||';
            $newval_sb = 'SB||diff_val||';
            foreach($getData as $value){
                $newval.= $value['id'].','.$value['username'].'||';
                $newval_sb .= $value['id'].','.$value['username'].'||';
            }
            $data['specs']['spec']['reader_ids']['value'] = $newval;
            $data['specs']['spec']['main_reader']['value'] = $newval_sb;

            

        }
        //end of Readers Visibility Tool

        // Check if this page is a direct edit or edit through navigation
        // Simpy by checking to existence of $nav['id']
        if (isset($nav['id']))
        {
            // Through Nav
            $form_action = "/admin/pages/save/{$nav['id']}/" . ($subnav['id'] ? $subnav['id'] : '0') . "/{$row['id']}";

            // Buttons
            $cancel_button = "/admin/pages/index/{$nav['id']}/" . ($subnav['id'] ? $subnav['id'] : '0');
            $clone_button = "/admin/pages/clone_record/{$nav['id']}/" . ($subnav['id'] ? $subnav['id'] : '0') . "/{$row['id']}";
            $delete_button = "/admin/pages/delete/{$nav['id']}/" . ($subnav['id'] ? $subnav['id'] : '0') . "/{$row['id']}";
        } else
        {
            // Direct Edit
            $form_action = "/admin/pages/direct_save_record/{$nav['table']}/{$row['id']}";

            // Buttons
            $cancel_button = "/admin/pages/cancel_direct_action";
            $clone_button = "/admin/pages/direct_clone/{$nav['table']}/{$row['id']}";
            $delete_button = "/admin/pages/direct_delete/{$nav['table']}/{$row['id']}";
        }


        $data['form_action'] = $form_action;
        $data['hidden_fields'] = $hidden_fields;
        $data['cancel_button'] = $cancel_button;
        $data['clone_button'] = $clone_button;
        $data['delete_button'] = $delete_button;

        // Load views
        $page['view_template'] = $this->load->view('admin/pages/edit', $data, true);
        $this->load->view('admin/template', $page);
    }

    function save($mainNavId = null, $subNavId = null, $recordId = null)
    {
        $subNavId = ($subNavId == '0' ? null : $subNavId);

        // Get Table Info
        $getMainNav = $this->db->query("SELECT * FROM vadmin_nav WHERE id = {$mainNavId} LIMIT 1");
        $nav = $getMainNav->row_array();

        // Subnav
        if (!$subNavId)
            $data['subnav'] = false;
        else
        {

            $getSubNav = $this->db->query("SELECT * FROM vadmin_navsub WHERE id={$subNavId} LIMIT 1");
            $subnav = $getSubNav->row_array();
        }

        $table = (isset($subnav['table']) ? $subnav['table'] : $nav['table']);
        $title = (isset($subnav['title']) ? $subnav['title'] : $nav['title']);

        // Get Fields
        $fieldArray = $this->db->list_fields($table);
        $specs = $this->vadmin->get_table_specs($table);
        $updateArray = array();

        // Get Hidden Fields
        $hidden_fields = array();

        if (isset($specs['hide_fields']))
        {

            $hidden_fields = explode(',', $specs['hide_fields']['value']);
        }

        $this->load->library('form_validation');
        
        foreach ($fieldArray as $f) {

            if (in_array($f, $hidden_fields))
            {
                
            } else
            {

                // Get Field Based On SPEC Data
                $spec_type = strtolower((isset($specs['spec'][$f]) ? $specs['spec'][$f]['value'] : "TB||50"));
                $spec_array = explode("||", $spec_type);

                // Load & Configure The Module
                $specMod = trim($spec_array[0]);
                if ($f == 'id')
                    $specMod = 'lb';

                // Load Field
                $this->$specMod->config($f, $this->input->post($f), $spec_array);
                $fieldValue = $this->$specMod->process_form();
                $isRequired = isset($this->$specMod->isRequired) ? $this->$specMod->isRequired : false;

                // Create Update Array
                if ($fieldValue != '[%skip%]')
                {
                    $updateArray[$f] = $fieldValue;
                }
                
                $this->form_validation->set_rules($f, $f, 'trim');
                if ($isRequired)
                {
                    $this->form_validation->set_rules($f, $this->lang->line($f, FALSE), 'required');
                }
            }
        }

        if ($this->form_validation->run())
        {
            if($mainNavId == 6 && ($subNavId == 19 || $subNavId == 22))
            {
                $memberData = $this->db->query("SELECT * FROM members WHERE id = '{$recordId}'")->row_array();
                if($memberData["member_fund_type"] != $updateArray["member_fund_type"])
                {
                    // m_omail()
                    $memberData['type'] = 'email';
                    $memberData['fund_type'] = $updateArray["member_fund_type"];
                    switch ($updateArray['member_fund_type'])
                    {
                        case 'preferred':
                            $memberData['description'] = "<p>Congratulations.</p><p>Your charge amount has no limit.</p>";
                            $this->system_vars->m_omail($memberData['id'], 'fund_type_changed', $memberData);
                            break;
                        case 'limited':
                            $memberData['description'] = "<p>Your charge amount is limited to $" .$updateArray['fund_amount_day'] . "/day or $" . $updateArray['fund_amount_month'] . "/month</p>";
                            $this->system_vars->m_omail($memberData['id'], 'fund_type_changed', $memberData);
                            break;
                        case 'banned':
                            $updateArray['banned'] = 1;
                            break;
                    }
                }
            }
            if($mainNavId == 37 && $subNavId == 41)
            {
                $this->db->where("type", NULL);
                $this->db->update("members", ["fund_amount_day" => $updateArray["fund_amount_day"], "fund_amount_month" => $updateArray["fund_amount_month"]]);
            }

            if($mainNavId == 7 && $subNavId == 9){
                $updateArray["date_updated"] = date("Y-m-d H:i:s");
            }

            $this->db->where('id', $recordId);
            $stat= $this->db->update($table, $updateArray);

            $this->session->set_flashdata('response_save', "Record #{$recordId} has been saved in {$title}");


            redirect("/admin/pages/index/{$mainNavId}/{$subNavId}");
        }
        
        redirect("/admin/pages/edit/{$mainNavId}/{$subNavId}/{$recordId}");
    }

    // ===
    // These function are "DIRECT" editing functions for use outside main navigation
    // ===

    function add_direct_record($tableName = null, $mainNavId = null, $subNavId = null, $recordId = null)
    {

        //
        // START Main Navigation Selection
        $subNavId = ($subNavId == '0' ? null : $subNavId);

        $getMainNav = $this->db->query("SELECT * FROM vadmin_nav WHERE id = {$mainNavId} LIMIT 1");
        $data['nav'] = $getMainNav->row_array();

        $this->open_nav = $data['nav']['id'];

        if (!$subNavId)
            $data['subnav'] = false;
        else
        {

            $getSubNav = $this->db->query("SELECT * FROM vadmin_navsub WHERE id={$subNavId} LIMIT 1");
            $data['subnav'] = $getSubNav->row_array();
        }

        // END Main Navigation Selection
        //
            
            // Get Table name
        $table = ($data['subnav']['table'] ? $data['subnav']['table'] : $data['nav']['table']);
        $specs = $this->vadmin->get_table_specs($table);

        // Check for any sideModules
        if (isset($specs['sidemodule']))
        {

            $moduleArray = explode('**', trim($specs['sidemodule']['value']));
            $sidemodules = $moduleArray;

            foreach ($sidemodules as $m) {

                list($m_tableName, $m_bridgeField) = explode("||", $m);

                if (trim($tableName) == $m_tableName)
                {
                                    
                    $this->db->insert($m_tableName, array($m_bridgeField => $recordId));
                    $newRecordId = $this->db->insert_id();

                    // Set return URL and redirect to edit module
                    $this->session->set_userdata('redirect_url', "/'admin/main/edit_record/{$mainNavId}/{$subNavId}/{$recordId}");
                    redirect("/'admin/main/edit_record_directly/{$m_tableName}/{$newRecordId}/" . urlencode(base64_encode("/'admin/main/edit_record/{$data['nav']['id']}/" . ($data['subnav']['id'] ? $data['subnav']['id'] : '0') . "/{$recordId}")));

                    break;
                }
            }
        } else
        {

            //
            // Sidemodules were not defined
            //
                
                die('Message #348: Adding data without the use of sidemodules is not currently implemented. But it shouldn\'t be that hard to do.');
        }
    }

    function edit_record_directly($table = null, $recordId = null, $set_redirect = null)
    {

        // REDIRECT URL MUST BE SET IN SESSION
        // redirect_url

        if ($set_redirect)
        {
            $this->session->set_userdata('redirect_url', base64_decode(urldecode($set_redirect)));
        }

        if (!$this->session->userdata('redirect_url'))
            die('To use this function you need to set a session variable called redirect_url and give it a url to redirect back to when the user is done with the saving.');

        $specs = $this->vadmin->get_table_specs($table);

        $data['nav']['title'] = (isset($specs['module_name']['value']) ? $specs['module_name']['value'] : $table);
        $data['nav']['table'] = $table;
        $data['subnav'] = false;
       
        //
        // Get Record Data
        $getData = $this->db->query("SELECT * FROM {$table} WHERE id = {$recordId} LIMIT 1");
        $data['data'] = $getData->row_array();

        //
        // Get Table Fields
        $data['fields'] = $fields = $this->db->list_fields($table);
        $data['specs'] = $this->vadmin->get_table_specs($table);
        $data['sidemodules'] = false;

        // Check for any sideModules
        if (isset($data['specs']['sidemodule']))
        {

            $moduleArray = explode('**', trim($data['specs']['sidemodule']['value']));
            $data['sidemodules'] = $moduleArray;
        }

        //
        // Load views
        $this->load->view('admin/partial/header');
        $this->load->view('admin/record_form', $t);
        $this->load->view('admin/partial/footer');
    }

    function direct_save_record($table = null, $recordId = null)
    {

        // Get Fields
        $fieldArray = $this->db->list_fields($table);
        $specs = $this->vadmin->get_table_specs($table);
        $updateArray = array();

        foreach ($fieldArray as $f) {

            // Get Field Based On SPEC Data
            $spec_type = strtolower((isset($specs['spec'][$f]) ? $specs['spec'][$f]['value'] : "TB||50"));
            $spec_array = explode("||", $spec_type);

            // Load & Configure The Module
            $specMod = trim($spec_array[0]);
            if ($f == 'id')
                $specMod = 'lb';

            // Load Field
            $this->$specMod->config($f, $this->input->post($f), $spec_array);
            $fieldValue = $this->$specMod->process_form();

            // Create Update Array
            if ($fieldValue != '[%skip%]')
                $updateArray[$f] = $fieldValue;
        }

        $this->db->where('id', $recordId);
        $this->db->update($table, $updateArray);

        $this->session->set_flashdata('response_save', "Record #{$recordId} has been saved");

        // Redirect http location after delete, and remove the session variable
        $redirect_url = $this->session->userdata('redirect_url');
        $this->session->unset_userdata('redirect_url');
        redirect($redirect_url);
    }

    function direct_delete($table = null, $recordId = null, $set_redirect = null)
    {

        if ($set_redirect)
        {
            $this->session->set_userdata('redirect_url', base64_decode(urldecode($set_redirect)));
        }

        // Delete record
        $this->db->where('id', $recordId);
        $this->db->delete($table);

        // Save a message to screen
        if ($this->db->affected_rows() >= 1)
            $this->session->set_flashdata('response_delete', "Record #{$recordId} has been deleted.");
        else
            $this->session->set_flashdata('response_delete', "That record does not seem to exist, so nothing was deleted");

        // Redirect http location after delete, and remove the session variable
        $redirect_url = $this->session->userdata('redirect_url');
        $this->session->unset_userdata('redirect_url');
        redirect($redirect_url);
    }

    function direct_clone($table = null, $recordId = null)
    {

        $getRecord = $this->db->query("SELECT * FROM {$table} WHERE id = {$recordId} LIMIT 1");
        $record = $getRecord->row_array();
        $record['id'] = '';

        $this->db->insert($table, $record);
        $recordId = $this->db->insert_id();

        $this->session->set_flashdata('response', "Record #{$recordId} has been duplicated.");
        redirect("/'admin/main/edit_record_directly/{$table}/{$recordId}");
    }

    function cancel_direct_action()
    {

        // Redirect http location after delete, and remove the session variable
        $redirect_url = $this->session->userdata('redirect_url');
        $this->session->unset_userdata('redirect_url');
        redirect($redirect_url);
    }

    // ==
    // End direct editing functionality
    // ===

    function delete($mainNavId = null, $subNavId = null, $recordId = null)
    {
        $getMainNav = $this->db->query("SELECT * FROM vadmin_nav WHERE id = {$mainNavId} LIMIT 1");
        $nav = $getMainNav->row_array();
        
        if (!$subNavId)
            $data['subnav'] = false;
        else
        {

            $getSubNav = $this->db->query("SELECT * FROM vadmin_navsub WHERE id={$subNavId} LIMIT 1");
            $subnav = $getSubNav->row_array();
        }

        $table = (isset($subnav['table']) ? $subnav['table'] : $nav['table']);

        if(($mainNavId == 37 && $subNavId == 45) || ($mainNavId == 7 && $subNavId == 30))
        {
            $deleted_category_id = $recordId;
            $this->db->where('category_id', (int)$deleted_category_id);
            $this->db->delete('profile_categories');
            $this->session->set_userdata('response_delete', "Record #{$recordId} has been deleted");
        }

        // Readers soft delete
        if ($mainNavId == 6)
        {
            if($table == 'members' || $table  == 'deleted_unused_members') {

                $this->db->where('id', $recordId);
                $this->db->update($table, ["deleted" => 1]);
            } else {
                $this->db->where('id', $recordId);
                $this->db->delete($table);
            }
            // redirect("/admin/reader_management/all");
            $this->session->set_userdata('response_delete', "Record #{$recordId} has been deleted");
            redirect("/admin/pages/index/{$mainNavId}/{$subNavId}");
        }
        else
        {
            
            if($mainNavId == 12) {

                if($subNavId == 35) {

                    $this->db->where('id', $recordId);
                    $this->db->update('members', array('deleted'=>1));
                    $this->session->set_userdata('response_delete', "Record #{$recordId} has been deleted");

                    redirect("/admin/reader_management/all");

                } elseif ($subNavId == 36) {

                    $this->db->where('id', $recordId);
                    $this->db->update('members', array('deleted'=>1));
                    $this->session->set_userdata('response_delete', "Record #{$recordId} has been deleted");

                    redirect("/admin/reader_management/featured");

                } elseif ($subNavId == 57){
                    $this->db->where('id', $recordId);
                    $this->db->delete($table);
                }else {

                    $this->db->where('id', $recordId);
                    $this->db->update('members', array('deleted'=>1));
                }

            } else {
                
                $this->db->where('id', $recordId);
                $this->db->delete($table);
            }
        }

        $this->session->set_userdata('response_delete', "Record #{$recordId} has been deleted");

        // if ($this->db->affected_rows() >= 1)
        // {
        //     $this->session->set_flashdata('response_delete', "Record #{$recordId} has been deleted");
        // }
        //
        // else
        // {
        //     $this->session->set_flashdata('response_delete', "That record does not seem to exist, so nothing was deleted");
        // }

        redirect("/admin/pages/index/{$mainNavId}/{$subNavId}");
    }

    function delete_old_account($mainNavId = null, $subNavId = null, $recordId = null)
    {

        $this->db->where('id', $recordId);
        $this->db->update('members', ['deleted'=>1]);

        $member = $this->db->query("SELECT username, first_name, last_name, registration_date, last_login_date FROM members WHERE id={$recordId}")->row_array();

        $deleted_member = array(
            "username" => $member['username'],
            "first_name" => $member['first_name'],
            "last_name" => $member['last_name'],
            "registration_date" => $member['registration_date'],
            "last_login_date" => $member['last_login_date'],
            "deleted_date" => date('Y-m-d H:i:s'),
        );

        $this->db->insert('deleted_unused_members', $deleted_member);

        $this->session->set_flashdata('response_delete', "Record #{$recordId} has been deleted");
        redirect("/admin/pages/index/{$mainNavId}/{$subNavId}");
    }

    function delete_unused_log($mainNavId = null, $subNavId = null, $recordId = null)
    {
        $this->db->where('id', $recordId);
        $this->db->delete('deleted_unused_members');

        $this->session->set_flashdata('response_delete', "Record #{$recordId} has been deleted");
        redirect("/admin/pages/index/{$mainNavId}/{$subNavId}");
    }
    function delete_category($category_id)
    {
        $this->db->where('id',(int)$category_id);
        $this->db->delete('profile_categories');

        $this->session->set_userdata('response_delete', "Record #{$category_id} has been deleted");
        redirect("/admin/pages/index/37/46");
    }

    function clear_all_records($mainNavId = null, $subNavId = null)
    {

        $getMainNav = $this->db->query("SELECT * FROM vadmin_nav WHERE id = {$mainNavId} LIMIT 1");
        $nav = $getMainNav->row_array();

        $this->db->query("DELETE FROM {$nav['table']} ");

        if ($this->db->affected_rows() >= 1)
            $this->session->set_flashdata('response', $this->db->affected_rows() . " records have been deleted from {$nav['title']}");
        else
            $this->session->set_flashdata('response', "Nothing was deleted from {$nav['title']}");

        redirect("/'admin/main/overview/{$mainNavId}/{$subNavId}");
    }

    function export_data($mainNavId = null, $subNavId = null)
    {

        $getMainNav = $this->db->query("SELECT * FROM vadmin_nav WHERE id = {$mainNavId} LIMIT 1");
        $nav = $getMainNav->row_array();

        // Get Fields
        $fieldArray[] = $this->db->list_fields($nav['table']);

        // Get Data
        $getData = $this->db->query("SELECT * FROM {$nav['table']} ");
        $dataArray = $getData->result_array();

        // Combine 2 Arrays
        $data = array_merge($fieldArray, $dataArray);

        // Header Data
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename={$nav['table']}.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        // Compule Into CSV
        $fp = fopen('php://output', 'w');

        foreach ($data as $fields) {
            fputcsv($fp, $fields);
        }

        fclose($fp);
    }

    function clone_record($mainNavId = null, $subNavId = null, $recordId = null)
    {

        $getMainNav = $this->db->query("SELECT * FROM vadmin_nav WHERE id = {$mainNavId} LIMIT 1");
        $nav = $getMainNav->row_array();

        $getRecord = $this->db->query("SELECT * FROM {$nav['table']} WHERE id = {$recordId} LIMIT 1");
        $record = $getRecord->row_array();
        $record['id'] = '';

        $this->db->insert($nav['table'], $record);
        $recordId = $this->db->insert_id();

        $this->session->set_flashdata('response', "Record #{$recordId} has been duplicated. ");
        redirect("/'admin/main/edit_record/{$mainNavId}/{$subNavId}/{$recordId}");
    }

    function logout()
    {

        $this->session->sess_destroy();
        redirect('/admin/Login');
    }

    public function purge_old()
    {

        $getMembers = $this->db->query("
                SELECT *
                FROM members
                WHERE last_login_date <= DATE_SUB(NOW(), INTERVAL 1 YEAR)
            ");

        $total = 0;
        foreach ($getMembers->result() as $member) {
            $this->db->where('id', $member->id);
            $this->db->delete('members');
            $total++;
        }

        $this->session->set_flashdata('response', "{$total} members have been deleted");
        redirect("/'admin/main/overview/6/21");
    }

    function finalize_transaction($type = '', $transaction_id = null)
    {

        $this->load->model('member_billing');

        if ($type == 'settle')
        {
            $this->member_billing->settle_transaction($transaction_id);
            $this->session->set_flashdata('response', "Transaction has been settled");
        } else
        {
            $this->member_billing->void_transaction($transaction_id);
            $this->session->set_flashdata('response', "Transaction has been voided");
        }

        redirect("/'admin/main/overview/13/24");
    }

    function transactions($member_id = null)
    {

        $this->load->model('nrr_model');
        $params = array();

        //--- Member balance records
        $sql = "SELECT member_balance.*
                FROM member_balance
                WHERE member_id = {$member_id}";
        $params['balance'] = $this->db->query($sql)->result();

        //--- Transaction Log
        $sql = "SELECT transactions.*
                FROM transactions
                WHERE transactions.member_id = {$member_id}";
        $params['transactions'] = $this->db->query($sql)->result();

        //--- Chat Transcripts
        $sql = "SELECT chats.*, members.username as reader_username
                FROM chats
                JOIN members ON members.id = chats.reader_id
                WHERE client_id = {$member_id}";
        $params['transcripts'] = $this->db->query($sql)->result();

        $this->load->view('admin/partial/header');
        $this->load->view('admin/transaction_log', $params);
        $this->load->view('admin/partial/footer');
    }

    function delete_balance_record($record_id)
    {

        $record = $this->db->query("SELECT * FROM member_balance WHERE id = {$record_id}")->row();

        $this->db->where('id', $record_id);
        $this->db->delete('member_balance');

        $this->session->set_flashdata('response', "Balance record has been deleted");
        redirect("/admin/main/transactions/{$record->member_id}");
    }

    function delete_transaction($transaction_id)
    {

        $record = $this->db->query("SELECT * FROM transactions WHERE id = {$transaction_id}")->row();

        $this->db->where('id', $transaction_id);
        $this->db->delete('transactions');

        $this->session->set_flashdata('response', "Transaction record has been deleted");
        redirect("/admin/main/transactions/{$record->member_id}");
    }

    function transcripts($chat_id = null)
    {

        $params = $this->db->query("SELECT * FROM chats WHERE id = {$chat_id} ")->row_array();
        $params['transcripts'] = $this->db->query("

                SELECT
                  chat_transcripts.*,
                  members.username
                FROM chat_transcripts
                JOIN members ON members.id = chat_transcripts.member_id
                WHERE chat_id = $chat_id
                ORDER BY id

            ")->result();

        $this->load->model('nrr_model');
        $params['hasNRR'] = $hasNRR = $this->nrr_model->check_nrr_for_chat($chat_id);

        $page['view_template'] = $this->load->view('admin/chat_transcripts/details', $params, true);
        $this->load->view('admin/template', $page);

        //$this->load->view('admin/partial/header');
        //$this->load->view('admin/partial/footer');
    }

    function process_nrr($chat_id)
    {

        $this->load->model('nrr_model');

        //--- Create NRR
        $array = $this->nrr_model->create($chat_id, 1);

        //--- Process NRR
        $this->nrr_model->process($array['nrr_id'], 'paid', $array['amount']);

        //--- Response
        $this->session->set_flashdata('response', "NRR has been processed");

        //--- Redirect
        redirect("/admin/main/transactions/{$array['client_id']}");
    }

    function ban_user($member_id)
    {
        $this->load->model('member');
        $this->member->set_full_ban($member_id);

        $redirect_url = "/admin/pages/index/6/22";

        redirect($redirect_url);
    }

    function un_ban_user($member_id)
    {
        $this->load->model('member');
        $this->member->un_set_full_ban($member_id);

        $redirect_url = "/admin/pages/index/6/22";
        redirect($redirect_url);
    }

    function ban_all()
    {
        $this->load->model('member');
        $this->member->ban_all();

        $redirect_url = "/admin/pages/index/6/22";
        redirect($redirect_url);
    }

    function un_ban_all()
    {
        $this->load->model('member');
        $this->member->un_ban_all();

        $redirect_url = "/admin/pages/index/6/22";
        redirect($redirect_url);
    }

    function approve_pending_message($message_id)
    {
        $pending_message_data = $this->db->query("SELECT * FROM message_center_notifications WHERE id = {$message_id}")->row_array();

        $CI = & get_instance();
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.taroflash.com',
            'smtp_port' => 465,
            'smtp_crypto' => 'ssl',
            'smtp_timeout' => 7,
            'smtp_user' => 'norreply@psychic-contact.com',
            'smtp_pass' => 'NoRep2017*1245Klm1P32124',
            'mailtype'  => 'html',
            'charset'   => 'iso-8859-1'
        );
        $CI->load->library('email', $config);

        // Parse system email template
        $subject = $pending_message_data['subject'];
        $pre_content = html_entity_decode(nl2br($pending_message_data['message']));

        $content = "
                        <div style='background:#b9b9b9;padding:20px;'>
                            <div style='margin-bottom:10px;font-size:20px;font-weight:bold;font-family:Arial;'>Psychic-Contact.com</div>
                            <div style='background:#FFF;padding:20px;'>{$pre_content}</div>
                        </div>
                        ";

        // Configure Email Options
        $config['mailtype'] = 'html';
        $CI->email->initialize($config);

        // Send Email
        $from[1] = $CI->config->item('email_from_email_address');
        $from[2] = $CI->config->item('email_from_name');

        // IF the TO is a number
        // Get the member and send the message to their local inbox
        // as well as their email address
        $to = $pending_message_data['to'];

        if (is_numeric($to))
        {

            $member = $this->db->query("SELECT email FROM members WHERE `id`='{$to}'")->row_array();

            $to = $member['email'];
        }

        $CI->email->from($from[1], $from[2]);
        $CI->email->to($to);
        $CI->email->subject($subject);
        $CI->email->message($content);

        if($pending_message_data['attached_file'] != null)
        {
            $CI->email->attach($pending_message_data['attached_file']);
        } else {

        }
        $log = $CI->email->send();
        log_message('debug', $log);

        $this->member->notify($pending_message_data['to'], $pending_message_data['from'], $pending_message_data['subject'], nl2br($pending_message_data['message']));

        $this->db->where('id', $message_id);
        $this->db->update('message_center_notifications', array('message_status'=>'APPROVED'));

        $this->session->set_userdata('response', "Record #{$message_id} has been approved");
        $redirect_url = "/admin/pages/index/36/55";
        redirect($redirect_url);
    }

    function deny_pending_message($message_id)
    {

        $pending_message_data = $this->db->query("SELECT * FROM message_center_notifications WHERE id = {$message_id}")->row_array();

        $CI = & get_instance();
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.taroflash.com',
            'smtp_port' => 465,
            'smtp_crypto' => 'ssl',
            'smtp_timeout' => 7,
            'smtp_user' => 'norreply@psychic-contact.com',
            'smtp_pass' => 'NoRep2017*1245Klm1P32124',
            'mailtype'  => 'html',
            'charset'   => 'iso-8859-1'
        );
        $CI->load->library('email', $config);

        // Parse system email template
        $subject = $pending_message_data['subject'];
        $pre_content = html_entity_decode(nl2br($pending_message_data['message']));
        $pre_content_reason = html_entity_decode(nl2br($pending_message_data['reason']));

        $content = "
                        <div style='background:#b9b9b9;padding:20px;'>
                            <div style='margin-bottom:10px;font-size:20px;font-weight:bold;font-family:Arial;'>Psychic-Contact.com</div>
                            <div style='background:#FFF;padding:20px;'>{$pre_content}</div>
                            <div style='background:#FFF;padding:20px;'>{$pre_content_reason}</div>
                        </div>
                        ";

        // Configure Email Options
        $config['mailtype'] = 'html';
        $CI->email->initialize($config);

        // Send Email
        $from[1] = $CI->config->item('email_from_email_address');
        $from[2] = $CI->config->item('email_from_name');

        // IF the TO is a number
        // Get the member and send the message to their local inbox
        // as well as their email address
        $to = $pending_message_data['from'];

        if (is_numeric($to))
        {

            $member = $this->db->query("SELECT email FROM members WHERE `id`='{$to}'")->row_array();

            $to = $member['email'];
        }

        $CI->email->from($from[1], $from[2]);
        $CI->email->to($to);
        $CI->email->subject($subject);
        $CI->email->message($content);

        $log = $CI->email->send();

        $this->db->where('id', $message_id);
        $this->db->update('message_center_notifications', array('message_status'=>'DENNY'));

        $this->session->set_userdata('response_delete', "Record #{$message_id} has been denied");
        $redirect_url = "/admin/pages/index/36/55";
        redirect($redirect_url);
    }

    function approve_profile($change_profile_id)
    {
        //merge from changed_member_profiles to member_profiles and members
        $request_change_profile = $this->db->query("SELECT * FROM changed_member_profiles WHERE id={$change_profile_id}")->row_array();
        if($request_change_profile['profile_image'] != null)
        {
            $members_update_data = array(
                'gender' => $request_change_profile['gender'],
                'dob' => $request_change_profile['dob'],
                'country' => $request_change_profile['country'],
                'paypal_email' => $request_change_profile['paypal_email'],
                'profile_image' => $request_change_profile['profile_image']
            );
        } else {
            $members_update_data = array(
                'gender' => $request_change_profile['gender'],
                'dob' => $request_change_profile['dob'],
                'country' => $request_change_profile['country'],
                'paypal_email' => $request_change_profile['paypal_email'],
            );
        }

        $this->db->where('id', $request_change_profile['member_id']);
        $this->db->update('members', $members_update_data);

        $member_profiles_update_data = array(
            'title' => $request_change_profile['title'],
            'biography' => $request_change_profile['biography'],
            'area_of_expertise' => $request_change_profile['area_of_expertise'],
            'enable_email' => $request_change_profile['enable_email'],
            'email_total_days' => $request_change_profile['email_total_days']
        );
        $this->db->where('member_id', $request_change_profile['member_id']);
        $this->db->update('member_profiles', $member_profiles_update_data);

        $changed_profile_status = array(
            'status' => 1
        );
        $this->db->where('id', $change_profile_id);
        $this->db->update('changed_member_profiles', $changed_profile_status);


        // NoBo to Reader
        $member = $this->member->get($request_change_profile['member_id']);
        $member['type'] = 'email';
        $member['description'] = "<p>Your request has been approved.</p>";
        $this->system_vars->m_omail($member['id'], 'profile_change_request', $member);

        // NoBo to Admin
        $member = $this->member->get(1);
        $member['type'] = 'email';
        $member['description'] = $request_change_profile['username']."'s" . "<span> request has been approved.</span>";
        $this->system_vars->m_omail($member['id'], 'profile_change_request', $member);

        $this->session->set_flashdata('response', "Profile has been approved successfully.");

        $redirect_url = "/admin/pages/index/34/47";
        redirect($redirect_url);
    }

    function disapprove_profile($change_profile_id)
    {
        $request_change_profile = $this->db->query("SELECT * FROM changed_member_profiles WHERE id={$change_profile_id}")->row_array();

        $changed_profile_status = array(
            'status' => 2
        );
        $this->db->where('id', $change_profile_id);
        $this->db->update('changed_member_profiles', $changed_profile_status);

        // NoBo to Reader
        $member = $this->member->get($request_change_profile['member_id']);
        $member['type'] = 'email';
        $member['description'] = "<p>Your request has been declined.</p>";
        $this->system_vars->m_omail($member['id'], 'profile_change_request', $member);

        // NoBo to Admin
        $member = $this->member->get(1);
        $member['type'] = 'email';
        $member['description'] = $request_change_profile['username']."'s" . "<span> request has been declined.</span>";
        $this->system_vars->m_omail($member['id'], 'profile_change_request', $member);

        $this->session->set_flashdata('response_delete', "Profile has been disapproved.");

        $redirect_url = "/admin/pages/index/34/47";
        redirect($redirect_url);
    }

    function approve_promotion($promotion_request_id)
    {
        $data = array(
            'status' => 1,
            'updated_at' => date("Y-m-d H:i:s")
        );
        $this->db->where('id', $promotion_request_id);
        $this->db->update('promotion', $data);

        $redirect_url = "/admin/pages/index/35/50";
        redirect($redirect_url);
    }

    function approve_blog($blog_id)
    {
        $data = array(
            'approved' => 1,
            'date' => date("Y-m-d H:i:s")
        );
        $this->db->where('id', $blog_id);
        $this->db->update('blogs', $data);

        // Blog Api
        $WP_USERS =  array("summer"    => "bQVnMO#JM62M^UpQS8NmLStx");
        $password = $WP_USERS['summer'];

        $curl = curl_init( 'https://www.psychic-contact.net/oauth/token' );
        curl_setopt( $curl, CURLOPT_POST, true );
        curl_setopt( $curl, CURLOPT_POSTFIELDS, array(
            'client_id' => 'SrHuOEpzECpAlKrwtAa2AC4aAOsvdz',
            'client_secret' => 'q508zONZosBN3Xyut25D5Fg9PRXhma',
            'grant_type' => 'password',
            'username' => Ucfirst('summer'),
            'password' => $password,
        ) );
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl,  CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl,  CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl,  CURLOPT_SSL_VERIFYHOST, false);
        $auth = curl_exec(  $curl );

        $auth = json_decode($auth);
        $access_key = $auth->access_token;
        curl_close($curl);

        //IMAGE UPLOAD
        $featured_media = "";
        $blog = $this->db->query("SELECT * FROM blogs WHERE `id`={$blog_id}")->row_array();

        if (!empty($blog['image'])) {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: image/jpeg",
                "content-disposition: attachment; filename="."blog_img_1549449669.jpg",
                'Authorization: Bearer ' . $access_key));
            curl_setopt($curl, CURLOPT_URL, 'https://www.psychic-contact.net/wp-json/wp/v2/media');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_TIMEOUT, 60);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_POST, 1);
            $path = '/media/assets/'.$blog['image'];
            $fileinfo = file_get_contents($path);
            $data = $fileinfo;
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            $post = curl_exec( $curl );
            $status = json_decode($post);
            curl_close($curl);

            $featured_media = $status->id;
        }
        //call api wp rest api

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Bearer ' . $access_key));
        curl_setopt($curl, CURLOPT_URL, 'https://www.psychic-contact.net/wp-json/wp/v2/posts');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, 1);

        $string = html_entity_decode($blog['content']);
        $string = preg_replace("/\s/",' ',$string);
        $data = array(
            'title' => $blog['title'],
            'categories'=>$blog["category"],
            'status'=>'publish',
            'content' =>$string,
            'featured_media'=>  $featured_media
        );
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        $post = curl_exec( $curl );
        $status = json_decode($post);
        curl_close($curl);

        $redirect_url = "/admin/pages/index/7/32";
        redirect($redirect_url);
    }

    function disapprove_promotion($promotion_request_id)
    {
        $data = array(
            'status' => 2,
            'updated_at' => date("Y-m-d H:i:s")
        );
        $this->db->where('id', $promotion_request_id);
        $this->db->update('promotion', $data);

        $redirect_url = "/admin/pages/index/35/50";
        redirect($redirect_url);
    }

    // function disapprove_blog($blog_id)
    // {
    //     $data = array(
    //         'approved' => 0,
    //         'date' => date("Y-m-d H:i:s")
    //     );
    //     $this->db->where('id', $blog_id);
    //     $this->db->update('blogs', $data);
    //
    //     $redirect_url = "/admin/pages/index/18/32";
    //     redirect($redirect_url);
    // }

    function approve_article($article_id)
    {
        $data = array(
            'approved' => 1,
        );
        $this->db->where('id', $article_id);
        $this->db->update('articles', $data);

        $this->session->set_flashdata('response', "Article has been approved successfully.");

        $redirect_url = "/admin/pages/index/7/17";
        redirect($redirect_url);
    }
    function disapprove_article($article_id)
    {
        $data = array(
            'approved' => 2,
        );
        $this->db->where('id', $article_id);
        $this->db->update('articles', $data);

        $article = $this->db->query("SELECT profile_id FROM articles WHERE `id`={$article_id}")->row_array();
        $member_id = $article['profile_id'];

        /*notification of reject*/
        $member = $this->member->get($member_id);
        $member['type'] = 'email';
        $member['description'] = "<p>Your article has been rejected.</p>";
        $this->system_vars->m_omail($member['id'], 'article_rejected', $member);

        $this->session->set_flashdata('response', "Article has been disapproved.");

        $redirect_url = "/admin/pages/index/7/18";
        redirect($redirect_url);
    }

    function approve_group_pending_message($message_id)
    {
        $pending_message_data = $this->db->query("SELECT * FROM message_center_group_notifications WHERE id = {$message_id}")->row_array();

        $CI = & get_instance();
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.taroflash.com',
            'smtp_port' => 465,
            'smtp_crypto' => 'ssl',
            'smtp_timeout' => 7,
            'smtp_user' => 'norreply@psychic-contact.com',
            'smtp_pass' => 'NoRep2017*1245Klm1P32124',
            'mailtype'  => 'html',
            'charset'   => 'iso-8859-1'
        );
        $CI->load->library('email', $config);

        // Parse system email template
        $subject = $pending_message_data['subject'];
        $pre_content = html_entity_decode(nl2br($pending_message_data['message']));

        $content = "
                        <div style='background:#b9b9b9;padding:20px;'>
                            <div style='margin-bottom:10px;font-size:20px;font-weight:bold;font-family:Arial;'>Psychic-Contact.com</div>
                            <div style='background:#FFF;padding:20px;'>{$pre_content}</div>
                        </div>
                        ";

        // Configure Email Options
        $config['mailtype'] = 'html';
        $CI->email->initialize($config);

        // Send Email
        $from[1] = $CI->config->item('email_from_email_address');
        $from[2] = $CI->config->item('email_from_name');

        // IF the TO is a number
        // Get the member and send the message to their local inbox
        // as well as their email address

        $recipients = explode(",", $pending_message_data['to']);

        foreach($recipients as $recipient_id)
        {
            $CI->email->clear(true);
            $to = '';
            if (is_numeric($recipient_id))
            {

                $member = $this->db->query("SELECT email FROM members WHERE `id`='{$recipient_id}'")->row_array();

                $to = $member['email'];
            }

            $CI->email->from($from[1], $from[2]);
            $CI->email->to($to);
            $CI->email->subject($subject);
            $CI->email->message($content);

            if($pending_message_data['attached_file'] != null)
            {
                $CI->email->attach($pending_message_data['attached_file']);
            } else {

            }
            $log = $CI->email->send();
            log_message('debug', $log);

            $this->member->notify($pending_message_data['to'], $pending_message_data['from'], $pending_message_data['subject'], nl2br($pending_message_data['message']));
        }

        $this->db->where('id', $message_id);
        $this->db->update('message_center_group_notifications', array('message_status'=>'APPROVED'));

        $redirect_url = "/admin/pages/index/36/56";
        redirect($redirect_url);
    }

    function deny_group_pending_message($message_id)
    {

        $pending_message_data = $this->db->query("SELECT * FROM message_center_group_notifications WHERE id = {$message_id}")->row_array();

        $CI = & get_instance();
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.taroflash.com',
            'smtp_port' => 465,
            'smtp_crypto' => 'ssl',
            'smtp_timeout' => 7,
            'smtp_user' => 'norreply@psychic-contact.com',
            'smtp_pass' => 'NoRep2017*1245Klm1P32124',
            'mailtype'  => 'html',
            'charset'   => 'iso-8859-1'
        );
        $CI->load->library('email', $config);

        // Parse system email template
        $subject = $pending_message_data['subject'];
        $pre_content = html_entity_decode(nl2br($pending_message_data['message']));
        $pre_content_reason = html_entity_decode(nl2br($pending_message_data['reason']));

        $content = "
                        <div style='background:#b9b9b9;padding:20px;'>
                            <div style='margin-bottom:10px;font-size:20px;font-weight:bold;font-family:Arial;'>Psychic-Contact.com</div>
                            <div style='background:#FFF;padding:20px;'>{$pre_content}</div>
                            <div style='background:#FFF;padding:20px;'>{$pre_content_reason}</div>
                        </div>
                        ";

        // Configure Email Options
        $config['mailtype'] = 'html';
        $CI->email->initialize($config);

        // Send Email
        $from[1] = $CI->config->item('email_from_email_address');
        $from[2] = $CI->config->item('email_from_name');

        // IF the TO is a number
        // Get the member and send the message to their local inbox
        // as well as their email address
        $recipients = explode(",", $pending_message_data['to']);

        foreach($recipients as $recipient_id)
        {
            $to = '';
            if (is_numeric($recipient_id))
            {
                $member = $this->db->query("SELECT email FROM members WHERE `id`='{$recipient_id}'")->row_array();
                $to = $member['email'];
            }

            $CI->email->from($from[1], $from[2]);
            $CI->email->to($to);
            $CI->email->subject($subject);
            $CI->email->message($content);

            $log = $CI->email->send();
        }



        $this->db->where('id', $message_id);
        $this->db->update('message_center_group_notifications', array('message_status'=>'DENNY'));
        $redirect_url = "/admin/pages/index/36/56";
        redirect($redirect_url);
    }

    function impersonate($member_id)
    {

        $this->session->set_userdata('member_logged', $member_id);
        redirect('/my_account');

    }

    function reset_password($member_id, $nav_id, $subnav_id)
    {
        $get_user = $this->db->query("SELECT * FROM members WHERE `id` = {$member_id} LIMIT 1");
        $user = $get_user->row_array();

        $this->load->library('encrypt');
        $encryptedId = base64_encode($this->encrypt->encode($user['id']));

        $user['pin'] = mt_rand(10000, 99999);
        $encryptedPin = base64_encode($this->encrypt->encode($user['pin']));

        $user['link'] = $this->config->item('site_url') . "/register/confirm_pin_code/{$encryptedId}/{$encryptedPin}";

        $user['type'] = "email";

        $this->system_vars->omail($user['id'], 'reset_password', $user);

        if($nav_id == 12) {
            $this->session->set_flashdata('response', "Password has been reset successfully");
            redirect("/admin/reader_management/all");
        }

        $this->session->set_flashdata('response', "Password has been reset successfully");
        redirect("/admin/pages/index/".$nav_id."/".$subnav_id);
    }

    function approve_testimonial($testimonial_id)
    {
        $this->db->where('id', $testimonial_id);
        $this->db->update('testimonials', array('admin_approved'=> 1));

        $this->session->set_flashdata('response', "#" . $testimonial_id . "Testimonial has been approved successfully");

        redirect('/admin/pages/index/6/71');
    }

    function readers_visibility_tool($datas = null){

        if($datas){
            $temp_arr = array();

            //get the member username to display in the table
            foreach ($datas as $data_new) {
                $main_reader = $data_new['main_reader'];
                $table = 'members';
                $addlogic = "AND members.id = {$main_reader}";
                $getData = $this->getMember($table , $addlogic);

                if($getData){
                    $data_new['main_reader'] = $getData[0]['username'];
                }else{
                    $data_new['main_reader'] = 'No Reader';
                }

                array_push($temp_arr,$data_new); 
            }
            return $temp_arr; 
        }
        
    }

    function submitreaders(){

        $post_data = $this->input->post();
        $count = sizeof($post_data['is_active']);

        for ($x = 0; $x < $count; $x++) {
           
            $query = $this->db->where('member_id', $post_data['id'][$x])->get('email_reading_readers');
            $num_rows = $query->num_rows();
            
            $update_data = array(
                'is_active' => $post_data['is_active'][$x],
                'first_q' => $post_data['first_q'][$x],
                'second_q' => $post_data['second_q'][$x],
                'third_q' => $post_data['third_q'][$x],
                'fourth_q' => $post_data['fourth_q'][$x],
                'fifth_q' => $post_data['fifth_q'][$x],
                'special' => $post_data['special'][$x] 
                );

            if($num_rows < 1){ 
                $update_data['member_id'] = $post_data['id'][$x];
                $val = $this->db->insert('email_reading_readers', $update_data); 
                   
            }else{
                $this->db->where('member_id', $post_data['id'][$x]);
                $this->db->update('email_reading_readers', $update_data);  
            }

        
       } //for

       $this->session->set_flashdata('response', "Record has been successfully saved!");
       redirect("/admin/pages/index/12/58");

    }
    function reject_testimonial($testimonial_id)
    {
        $this->db->where('id', $testimonial_id);
        $this->db->update('testimonials', array('admin_approved'=> 2));

        $this->session->set_flashdata('response', "#" . $testimonial_id . "Testimonial has been rejected.");

        redirect('/admin/pages/index/6/71');
    }

    function remove_session_data()
    {
        $this->session->unset_userdata('response_delete');
        $this->session->unset_userdata('response');
    }
    function chat_filter_data($params)
    {
        if($params == 'ALL'){ 
            $sql = "SELECT chats.*, transactions.amount, transactions.currency FROM chats left join transactions ON chats.id = transactions.chat_id";
        } else{
            $sql = "SELECT chats.*, transactions.amount, transactions.currency FROM chats left join transactions ON chats.id = transactions.chat_id  WHERE currency = '".$params."' ";
        }
        
        //print_r($sql);
        $getData_ = $this->db->query($sql);
        $data =  $getData_->result_array();

        $new_chat_array = [];
        foreach( $data as $chat)
        {
            //print_r($chat);
            $temp_data = [];
            $temp_data['reader_name'] = '';
            $temp_data['client_name'] ='';

            if($chat['reader_id']){
                $reader_name = $this->db->query("SELECT username from members where id={$chat['reader_id']}")->row_array();
                $reader_name = $reader_name['username'];
            }

            if($chat['client_id']){
                $client_name = $this->db->query("SELECT username from members where id={$chat['client_id']}")->row_array();
                $client_name = $client_name['username'];
            }

            foreach($chat as $key => $item)
            {
                $temp_data[$key] = $item;
            }
            $temp_data['reader_name'] = $reader_name;
            $temp_data['client_name'] = $client_name;

            array_push($new_chat_array, $temp_data);
        }
        
        echo json_encode( ["datas"=> $new_chat_array] );
    }

    function chat_transcripts($params)
    {
        $sql = "SELECT * FROM chat_transcripts WHERE chat_id = $params";
        $getData = $this->db->query($sql);
        $data = $getData->result_array();
        echo json_encode($data);
    }

    function chat_details($params)
    {
        $sql = "SELECT * FROM chats WHERE id = $params";
        $getData = $this->db->query($sql);
        $data = $getData->result_array();
        
        $reader_name = $this->db->query("SELECT username from members where id={$data[0]['reader_id']}")->row_array();
        $data[0]['reader_name'] = $reader_name['username'];
        $client_name = $this->db->query("SELECT username from members where id={$data[0]['client_id']}")->row_array();
        $data[0]['client_name'] = $client_name['username'];
    

        $sql = "SELECT * FROM member_balance WHERE member_id = {$data[0]['client_id']} order by id desc limit 1";
        $getData = $this->db->query($sql);
        $client_datas = $getData->result_array();
 
        
        $datas = ["chat" => $data , "client_details" => $client_datas]; 
        // echo $reader_name;
        //die();
        echo json_encode($datas);
    }

    function add_free_time(){
        $id =  $this->input->post('id');
        $free_time = $this->input->post('data');


        $sql_select = "SELECT * from chats where id = $id";
        $data = $this->db->query($sql_select);
        $chat_data = $data->result_array();
        $update_free_minute = $chat_data[0]['free_minutes'] + $free_time;

        $sql = "UPDATE chats SET free_minutes = '".$update_free_minute."' WHERE id = $id ";
        $this->db->query($sql);
        echo $update_free_minute;
    }

    function add_paid_time(){
        $id =  $this->input->post('id');
        $free_time = $this->input->post('data');

        $sql_select = "SELECT total_free_length from chats where id = $id";
        $data = $this->db->query($sql_select);
        $chat_data = $data->result_array();
        $update_free_minute = $chat_data[0]['total_free_length'] + $free_time;

        $sql = "UPDATE chats SET total_free_length = '".$update_free_minute."' WHERE id = $id ";
        $this->db->query($sql);
        echo $update_free_minute;
    }

    

}
