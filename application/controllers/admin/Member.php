<?php

class Mage extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Member_model');
    } 

    /*
     * Listing of active members or members in chat session
     */
    function index()
    {
        $data['pages'] = $this->Page_model->get_all_pages();        
        $data['view_template'] = 'admin/members/index';   
        $this->load->view('admin/template',$data);
    }

}

