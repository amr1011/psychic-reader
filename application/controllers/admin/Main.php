<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Main extends CI_Controller
{

    function __construct()
    {

        parent::__construct();

        $this->session->set_userdata('member_logged', 1);

        if (!$this->session->userdata('admin_is_logged'))
        {
            redirect('/admin/Login');
            exit;
        }
        $this->load->model("chatmodel");
        $this->load->model("readers");
        $this->load->model("messages_model");
        $this->load->model("reader");
        $this->results_per_page = 100;
        $this->open_nav = null;
        $this->response = null;
        $this->error = null;
        $this->admin = $this->session->userdata('admin_is_logged');
    }

    function index()
    {
        //$this->load->view('admin/partial/header', null, true);
        //$this->load->view('admin/partial/footer', null, true);        
        $page['content'] = '';
        $this->load->view('admin/template', $page);
    }

    function check_unread_message()
    {
        $unreadMessage = count($this->db->query("SELECT id FROM messages WHERE ricipient_id=1 AND sender_id IS NOT NULL AND `read`=0")->result_array());
        $return["unread"] = $unreadMessage;
        echo json_encode($return);
    }

    /**

    */
    public function show_content($main_id = null, $sub_id = null) 
    {

        $this->load->model('model_name');
        $this->model_name->get_list();
    }


    public function overview($mainNavId = null, $subNavId = null)
    {

        // START Main Navigation Selection
        $subNavId = ($subNavId == '0' ? null : $subNavId);

        $getMainNav = $this->db->query("SELECT * FROM vadmin_nav WHERE id = {$mainNavId} LIMIT 1");
        $t['nav'] = $getMainNav->row_array();

        $this->open_nav = $t['nav']['id'];


        if (!$subNavId)
            $t['subnav'] = false;
        else
        {

            $getSubNav = $this->db->query("SELECT * FROM vadmin_navsub WHERE id={$subNavId} LIMIT 1");
            $t['subnav'] = $getSubNav->row_array();
        }

        
        // Get the right content
        if (!$subNavId)
        {
            // Get Straight From Table
            $table = $t['nav']['table'];

            //
            // WHERE
            $where = (isset($t['nav']['where']) && trim($t['nav']['where']) ? "WHERE " . $t['nav']['where'] : "");

            $totalRecords = $this->db->query("SELECT id FROM {$table} {$where}");
            $limiter = ($this->uri->segment('6') ? $this->uri->segment('6') : "0");

            // Paginate Results
            $config['base_url'] = "/'admin/main/pages/{$mainNavId}/" . (isset($t['subnav']['id']) ? $t['subnav']['id'] : '0') . "/";
            $t['total_results'] = $config['total_rows'] = $totalRecords->num_rows();
            $config['per_page'] = $this->results_per_page;
            $config['uri_segment'] = 6;
            $config['num_links'] = 5;

            $this->pagination->initialize($config);

            $getData = $this->db->query("SELECT * FROM {$table} {$where} LIMIT {$limiter}, {$config['per_page']}");

            if ($getData->num_rows() == 0)
            {

                $t['data'] = false;
                $t['pagination'] = false;
            } else
            {

                $t['data'] = $getData->result_array();
                $t['pagination'] = $this->pagination->create_links();
            }
        } else
        {

            //
            // Get From Subnavigation

            $table = ($t['subnav']['table'] ? $t['subnav']['table'] : $t['nav']['table']);
            $where = ($t['subnav']['where'] ? "WHERE " . $t['subnav']['where'] : "");

            // check for special tags
            $where = str_replace("%current_date%", date("Y-m-d H:i:s"), $where);
            $where = str_replace("%yesterdays_date%", date("Y-m-d H:i:s", strtotime("-1 day")), $where);

            $totalRecords = $this->db->query("SELECT * FROM {$table} {$where}");
            $limiter = ($this->uri->segment('6') ? $this->uri->segment('6') : "0");

            // Paginate Results
            $config['base_url'] = "/'admin/main/pages/{$mainNavId}/" . (isset($t['subnav']['id']) ? $t['subnav']['id'] : $t['nav']['id']) . "/";
            $t['total_results'] = $config['total_rows'] = $totalRecords->num_rows();
            $config['per_page'] = $this->results_per_page;
            $config['uri_segment'] = 6;
            $config['num_links'] = 5;


            $this->pagination->initialize($config);


            // Get data
            $getData = $this->db->query("SELECT * FROM {$table} {$where} LIMIT {$limiter}, {$config['per_page']}");

            if ($getData->num_rows() == 0)
            {

                $t['data'] = false;
                $t['pagination'] = false;
            } else
            {

                $t['data'] = $getData->result_array();
                $t['pagination'] = $this->pagination->create_links();
            }
        }

        // 
        // Get Fields
        $customFields = $this->vadmin->get_field_spec($table, 'show_overview_fields');

        if ($customFields)
            $fieldArray = explode(',', trim($customFields['value']));
        else
            $fieldArray = $this->db->list_fields($table);

        $t['fields'] = $fieldArray;
        $t['specs'] = $this->vadmin->get_table_specs($table);

        # Sort Records
        if (isset($t['specs']['default_sort']['value']))
        {

            # Subval sort data
            # $t['data']

            if (strpos($t['specs']['default_sort']['value'], '||') === false)
            {

                $sorting_var = trim($t['specs']['default_sort']['value']);
                $ascordesc = "";
            } else
            {

                list($sorting_var, $ascordesc) = explode("||", $t['specs']['default_sort']['value']);
                $ascordesc = trim(strtolower($ascordesc));
            }

            $t['data'] = $this->system_vars->subval_sort($t['data'], trim($sorting_var));
            if ($ascordesc == 'desc')
                $t['data'] = array_reverse($t['data']);
        }

        // Load views
        $page["view_template"] = $this->load->view('admin/pages', $t, true);        
        $this->load->view('admin/template', $page);        
    }

    function add_record($mainNavId = null, $subNavId = null)
    {
        // START Main Navigation Selection
        $subNavId = ($subNavId == '0' ? null : $subNavId);

        $getMainNav = $this->db->query("SELECT * FROM vadmin_nav WHERE id = {$mainNavId} LIMIT 1");
        $t['nav'] = $getMainNav->row_array();

        $this->open_nav = $t['nav']['id'];

        if (!$subNavId)
            $t['subnav'] = false;
        else
        {

            $getSubNav = $this->db->query("SELECT * FROM vadmin_navsub WHERE id={$subNavId} LIMIT 1");
            $t['subnav'] = $getSubNav->row_array();
        }

        // END Main Navigation Selection
        //
			
			// Get Table name
        $table = ($t['subnav']['table'] ? $t['subnav']['table'] : $t['nav']['table']);

        //
        // Get Record Data
        $t['data'] = false;

        //
        // Get Table Fields
        $t['fields'] = $fields = $this->db->list_fields($table);
        $t['specs'] = $this->vadmin->get_table_specs($table);
        $t['sidemodules'] = false;

        // Load views
        $page['content'] = $this->load->view('admin/add_record_form', $t, true);        
        $this->load->view('admin/template', $page);
        
        //$this->load->view('admin/partial/header');
        //$this->load->view('admin/partial/footer');
    }

    function add_record_submit($mainNavId = null, $subNavId = null)
    {

        $subNavId = ($subNavId == '0' ? null : $subNavId);

        // Get Table Info
        $getMainNav = $this->db->query("SELECT * FROM vadmin_nav WHERE id = {$mainNavId} LIMIT 1");
        $nav = $getMainNav->row_array();

        // Subnav
        if (!$subNavId)
            $t['subnav'] = false;
        else
        {

            $getSubNav = $this->db->query("SELECT * FROM vadmin_navsub WHERE id={$subNavId} LIMIT 1");
            $subnav = $getSubNav->row_array();
        }

        $table = (isset($subnav['table']) ? $subnav['table'] : $nav['table']);
        $title = (isset($subnav['title']) ? $subnav['title'] : $nav['title']);

        // Get Fields
        $fieldArray = $this->db->list_fields($table);
        $specs = $this->vadmin->get_table_specs($table);
        $updateArray = array();

        // Get Hidden Fields
        $hidden_fields = array();

        if (isset($specs['hide_fields']))
        {

            $hidden_fields = explode(',', $specs['hide_fields']['value']);
        }

        foreach ($fieldArray as $f) {

            if (in_array($f, $hidden_fields))
            {
                
            } else
            {

                // Get Field Based On SPEC Data
                $spec_type = strtolower((isset($specs['spec'][$f]) ? $specs['spec'][$f]['value'] : "TB||50"));
                $spec_array = explode("||", $spec_type);

                // Load & Configure The Module
                $specMod = trim($spec_array[0]);
                if ($f == 'id')
                    $specMod = 'lb';

                // Load Field
                $this->$specMod->config($f, $this->input->post($f), $spec_array);
                $fieldValue = $this->$specMod->process_form();

                // Create Update Array
                if ($fieldValue != '[%skip%]')
                    $updateArray[$f] = $fieldValue;
            }
        }

        $this->db->insert($table, $updateArray);
        $recordId = $this->db->insert_id();

        $this->session->set_flashdata('response', "Record #{$recordId} has been added to {$title}");
        redirect("/'admin/main/overview/{$mainNavId}/{$subNavId}");
    }

    function edit_record($mainNavId = null, $subNavId = null, $recordId = null)
    {
        // START Main Navigation Selection
        $subNavId = ($subNavId == '0' ? null : $subNavId);

        $getMainNav = $this->db->query("SELECT * FROM vadmin_nav WHERE id = {$mainNavId} LIMIT 1");
        $t['nav'] = $getMainNav->row_array();

        $this->open_nav = $t['nav']['id'];

        if (!$subNavId)
            $t['subnav'] = false;
        else
        {

            $getSubNav = $this->db->query("SELECT * FROM vadmin_navsub WHERE id={$subNavId} LIMIT 1");
            $t['subnav'] = $getSubNav->row_array();
        }

        // END Main Navigation Selection
			
        // Get Table name
        $table = ($t['subnav']['table'] ? $t['subnav']['table'] : $t['nav']['table']);

        //
        // Get Record Data
        $getData = $this->db->query("SELECT * FROM {$table} WHERE id = {$recordId} LIMIT 1");
        $t['data'] = $getData->row_array();

        //
        // Get Table Fields
        $t['fields'] = $fields = $this->db->list_fields($table);
        $t['specs'] = $this->vadmin->get_table_specs($table);
        $t['sidemodules'] = false;

        // Check for any sideModules
        if (isset($t['specs']['sidemodule']))
        {

            $moduleArray = explode('**', trim($t['specs']['sidemodule']['value']));
            $t['sidemodules'] = $moduleArray;
        }

        // Load views
        $page['content'] = $this->load->view('admin/record_form', $t, true);
        $this->load->view('admin/template', $page);
        //$this->load->view('admin/partial/header');
        //$this->load->view('admin/partial/footer');
    }

    function save_record($mainNavId = null, $subNavId = null, $recordId = null)
    {

        $subNavId = ($subNavId == '0' ? null : $subNavId);

        // Get Table Info
        $getMainNav = $this->db->query("SELECT * FROM vadmin_nav WHERE id = {$mainNavId} LIMIT 1");
        $nav = $getMainNav->row_array();

        // Subnav
        if (!$subNavId)
            $t['subnav'] = false;
        else
        {

            $getSubNav = $this->db->query("SELECT * FROM vadmin_navsub WHERE id={$subNavId} LIMIT 1");
            $subnav = $getSubNav->row_array();
        }

        $table = (isset($subnav['table']) ? $subnav['table'] : $nav['table']);
        $title = (isset($subnav['title']) ? $subnav['title'] : $nav['title']);

        // Get Fields
        $fieldArray = $this->db->list_fields($table);
        $specs = $this->vadmin->get_table_specs($table);
        $updateArray = array();

        // Get Hidden Fields
        $hidden_fields = array();

        if (isset($specs['hide_fields']))
        {

            $hidden_fields = explode(',', $specs['hide_fields']['value']);
        }

        foreach ($fieldArray as $f) {

            if (in_array($f, $hidden_fields))
            {
                
            } else
            {

                // Get Field Based On SPEC Data
                $spec_type = strtolower((isset($specs['spec'][$f]) ? $specs['spec'][$f]['value'] : "TB||50"));
                $spec_array = explode("||", $spec_type);

                // Load & Configure The Module
                $specMod = trim($spec_array[0]);
                if ($f == 'id')
                    $specMod = 'lb';

                // Load Field
                $this->$specMod->config($f, $this->input->post($f), $spec_array);
                $fieldValue = $this->$specMod->process_form();

                // Create Update Array
                if ($fieldValue != '[%skip%]')
                    $updateArray[$f] = $fieldValue;
            }
        }

        $this->db->where('id', $recordId);
        $this->db->update($table, $updateArray);

        $this->session->set_flashdata('response', "Record #{$recordId} has been saved in {$title}");
        redirect("/admin/main/overview/{$mainNavId}/{$subNavId}");
    }

    // ===
    // These function are "DIRECT" editing functions for use outside main navigation
    // ===

    function add_direct_record($tableName = null, $mainNavId = null, $subNavId = null, $recordId = null)
    {

        //
        // START Main Navigation Selection
        $subNavId = ($subNavId == '0' ? null : $subNavId);

        $getMainNav = $this->db->query("SELECT * FROM vadmin_nav WHERE id = {$mainNavId} LIMIT 1");
        $t['nav'] = $getMainNav->row_array();

        $this->open_nav = $t['nav']['id'];

        if (!$subNavId)
            $t['subnav'] = false;
        else
        {

            $getSubNav = $this->db->query("SELECT * FROM vadmin_navsub WHERE id={$subNavId} LIMIT 1");
            $t['subnav'] = $getSubNav->row_array();
        }

        // END Main Navigation Selection
        //
			
			// Get Table name
        $table = ($t['subnav']['table'] ? $t['subnav']['table'] : $t['nav']['table']);
        $specs = $this->vadmin->get_table_specs($table);

        // Check for any sideModules
        if (isset($specs['sidemodule']))
        {

            $moduleArray = explode('**', trim($specs['sidemodule']['value']));
            $sidemodules = $moduleArray;

            foreach ($sidemodules as $m) {

                list($m_tableName, $m_bridgeField) = explode("||", $m);

                if (trim($tableName) == $m_tableName)
                {

                    $this->db->insert($m_tableName, array($m_bridgeField => $recordId));
                    $newRecordId = $this->db->insert_id();

                    // Set return URL and redirect to edit module
                    $this->session->set_userdata('redirect_url', "/'admin/main/edit_record/{$mainNavId}/{$subNavId}/{$recordId}");
                    redirect("/admin/main/edit_record_directly/{$m_tableName}/{$newRecordId}/" . urlencode(base64_encode("/'admin/main/edit_record/{$t['nav']['id']}/" . ($t['subnav']['id'] ? $t['subnav']['id'] : '0') . "/{$recordId}")));

                    break;
                }
            }
        } else
        {

            //
            // Sidemodules were not defined
            //
				
				die('Message #348: Adding data without the use of sidemodules is not currently implemented. But it shouldn\'t be that hard to do.');
        }
    }

    function edit_record_directly($table = null, $recordId = null, $set_redirect = null)
    {

        // REDIRECT URL MUST BE SET IN SESSION
        // redirect_url

        if ($set_redirect)
        {
            $this->session->set_userdata('redirect_url', base64_decode(urldecode($set_redirect)));
        }

        if (!$this->session->userdata('redirect_url'))
            die('To use this function you need to set a session variable called redirect_url and give it a url to redirect back to when the user is done with the saving.');

        $specs = $this->vadmin->get_table_specs($table);

        $t['nav']['title'] = (isset($specs['module_name']['value']) ? $specs['module_name']['value'] : $table);
        $t['nav']['table'] = $table;
        $t['subnav'] = false;

        //
        // Get Record Data
        $getData = $this->db->query("SELECT * FROM {$table} WHERE id = {$recordId} LIMIT 1");
        $t['data'] = $getData->row_array();

        //
        // Get Table Fields
        $t['fields'] = $fields = $this->db->list_fields($table);
        $t['specs'] = $this->vadmin->get_table_specs($table);
        $t['sidemodules'] = false;

        // Check for any sideModules
        if (isset($t['specs']['sidemodule']))
        {

            $moduleArray = explode('**', trim($t['specs']['sidemodule']['value']));
            $t['sidemodules'] = $moduleArray;
        }

        //
        // Load views
        $this->load->view('admin/partial/header');
        $this->load->view('admin/record_form', $t);
        $this->load->view('admin/partial/footer');
    }

    function direct_save_record($table = null, $recordId = null)
    {

        // Get Fields
        $fieldArray = $this->db->list_fields($table);
        $specs = $this->vadmin->get_table_specs($table);
        $updateArray = array();

        foreach ($fieldArray as $f) {

            // Get Field Based On SPEC Data
            $spec_type = strtolower((isset($specs['spec'][$f]) ? $specs['spec'][$f]['value'] : "TB||50"));
            $spec_array = explode("||", $spec_type);

            // Load & Configure The Module
            $specMod = trim($spec_array[0]);
            if ($f == 'id')
                $specMod = 'lb';

            // Load Field
            $this->$specMod->config($f, $this->input->post($f), $spec_array);
            $fieldValue = $this->$specMod->process_form();

            // Create Update Array
            if ($fieldValue != '[%skip%]')
                $updateArray[$f] = $fieldValue;
        }

        $this->db->where('id', $recordId);
        $this->db->update($table, $updateArray);

        $this->session->set_flashdata('response', "Record #{$recordId} has been saved");

        // Redirect http location after delete, and remove the session variable
        $redirect_url = $this->session->userdata('redirect_url');
        $this->session->unset_userdata('redirect_url');
        redirect($redirect_url);
    }

    function direct_delete($table = null, $recordId = null, $set_redirect = null)
    {

        if ($set_redirect)
        {
            $this->session->set_userdata('redirect_url', base64_decode(urldecode($set_redirect)));
        }

        // Delete record
        $this->db->where('id', $recordId);
        $this->db->delete($table);

        // Save a message to screen
        if ($this->db->affected_rows() >= 1)
            $this->session->set_flashdata('response', "Record #{$recordId} has been deleted.");
        else
            $this->session->set_flashdata('response', "That record does not seem to exist, so nothing was deleted");

        // Redirect http location after delete, and remove the session variable
        $redirect_url = $this->session->userdata('redirect_url');
        $this->session->unset_userdata('redirect_url');
        redirect($redirect_url);
    }

    function direct_clone($table = null, $recordId = null)
    {

        $getRecord = $this->db->query("SELECT * FROM {$table} WHERE id = {$recordId} LIMIT 1");
        $record = $getRecord->row_array();
        $record['id'] = '';

        $this->db->insert($table, $record);
        $recordId = $this->db->insert_id();

        $this->session->set_flashdata('response', "Record #{$recordId} has been duplicated.");
        redirect("/admin/main/edit_record_directly/{$table}/{$recordId}");
    }

    function cancel_direct_action()
    {

        // Redirect http location after delete, and remove the session variable
        $redirect_url = $this->session->userdata('redirect_url');
        $this->session->unset_userdata('redirect_url');
        redirect($redirect_url);
    }

    // ==
    // End direct editing functionality
    // ===

    function delete_record($mainNavId = null, $subNavId = null, $recordId = null)
    {

        $getMainNav = $this->db->query("SELECT * FROM vadmin_nav WHERE id = {$mainNavId} LIMIT 1");
        $nav = $getMainNav->row_array();

        if (!$subNavId)
            $t['subnav'] = false;
        else
        {

            $getSubNav = $this->db->query("SELECT * FROM vadmin_navsub WHERE id={$subNavId} LIMIT 1");
            $subnav = $getSubNav->row_array();
        }

        $table = (isset($subnav['table']) ? $subnav['table'] : $nav['table']);

        $this->db->where('id', $recordId);
        $this->db->delete($table);

        if ($this->db->affected_rows() >= 1)
            $this->session->set_flashdata('response', "Record #{$recordId} has been deleted");
        else
            $this->session->set_flashdata('response', "That record does not seem to exist, so nothing was deleted");

        redirect("/admin/main/overview/{$mainNavId}/{$subNavId}");
    }

    function clear_all_records($mainNavId = null, $subNavId = null)
    {

        $getMainNav = $this->db->query("SELECT * FROM vadmin_nav WHERE id = {$mainNavId} LIMIT 1");
        $nav = $getMainNav->row_array();

        $this->db->query("DELETE FROM {$nav['table']} ");

        if ($this->db->affected_rows() >= 1)
            $this->session->set_flashdata('response', $this->db->affected_rows() . " records have been deleted from {$nav['title']}");
        else
            $this->session->set_flashdata('response', "Nothing was deleted from {$nav['title']}");

        redirect("/admin/main/overview/{$mainNavId}/{$subNavId}");
    }

    function export_data($mainNavId = null, $subNavId = null)
    {

        $getMainNav = $this->db->query("SELECT * FROM vadmin_nav WHERE id = {$mainNavId} LIMIT 1");
        $nav = $getMainNav->row_array();

        // Get Fields
        $fieldArray[] = $this->db->list_fields($nav['table']);

        // Get Data
        $getData = $this->db->query("SELECT * FROM {$nav['table']} ");
        $dataArray = $getData->result_array();

        // Combine 2 Arrays
        $data = array_merge($fieldArray, $dataArray);

        // Header Data
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename={$nav['table']}.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        // Compule Into CSV
        $fp = fopen('php://output', 'w');

        foreach ($data as $fields) {
            fputcsv($fp, $fields);
        }

        fclose($fp);
    }

    function clone_record($mainNavId = null, $subNavId = null, $recordId = null)
    {

        $getMainNav = $this->db->query("SELECT * FROM vadmin_nav WHERE id = {$mainNavId} LIMIT 1");
        $nav = $getMainNav->row_array();

        $getRecord = $this->db->query("SELECT * FROM {$nav['table']} WHERE id = {$recordId} LIMIT 1");
        $record = $getRecord->row_array();
        $record['id'] = '';

        $this->db->insert($nav['table'], $record);
        $recordId = $this->db->insert_id();

        $this->session->set_flashdata('response', "Record #{$recordId} has been duplicated. ");
        redirect("/admin/main/edit_record/{$mainNavId}/{$subNavId}/{$recordId}");
    }

    function logout()
    {

        $this->session->sess_destroy();
        redirect('/admin/Login');
    }

    public function purge_old()
    {

        $getMembers = $this->db->query("
                SELECT *
                FROM members
                WHERE last_login_date <= DATE_SUB(NOW(), INTERVAL 1 YEAR)
            ");

        $total = 0;
        foreach ($getMembers->result() as $member) {
            $this->db->where('id', $member->id);
            $this->db->delete('members');
            $total++;
        }

        $this->session->set_flashdata('response', "{$total} members have been deleted");
        redirect("/admin/main/overview/6/21");
    }

    function finalize_transaction($type = '', $transaction_id = null)
    {

        $this->load->model('member_billing');

        if ($type == 'settle')
        {
            $this->member_billing->settle_transaction($transaction_id);
            $this->session->set_flashdata('response', "Transaction has been settled");
        } else
        {
            $this->member_billing->void_transaction($transaction_id);
            $this->session->set_flashdata('response', "Transaction has been voided");
        }

        redirect("/admin/main/overview/13/24");
    }

    function transactions($member_id = null)
    {

        $this->load->model('nrr_model');
        $params = array();

        //--- Member balance records
        $sql = "SELECT member_balance.*
                FROM member_balance
                WHERE member_id = {$member_id}";
        $params['balance'] = $this->db->query($sql)->result();

        //--- Transaction Log
        $sql = "SELECT transactions.*
                FROM transactions
                WHERE transactions.member_id = {$member_id}";
        $params['transactions'] = $this->db->query($sql)->result();

        //--- Chat Transcripts
        $sql = "SELECT chats.*, members.username as reader_username
                FROM chats
                JOIN members ON members.id = chats.reader_id
                WHERE client_id = {$member_id}";
        $params['transcripts'] = $this->db->query($sql)->result();

        $page['view_template'] = $this->load->view('admin/transaction_log', $params, true);
        $this->load->view('admin/template', $page);
    }

    function delete_balance_record($record_id)
    {

        $record = $this->db->query("SELECT * FROM member_balance WHERE id = {$record_id}")->row();

        $this->db->where('id', $record_id);
        $this->db->delete('member_balance');

        $this->session->set_flashdata('response', "Balance record has been deleted");
        redirect("/admin/main/transactions/{$record->member_id}");
    }

    function delete_transaction($transaction_id)
    {

        $record = $this->db->query("SELECT * FROM transactions WHERE id = {$transaction_id}")->row();

        $this->db->where('id', $transaction_id);
        $this->db->delete('transactions');

        $this->session->set_flashdata('response', "Transaction record has been deleted");
        redirect("/admin/main/transactions/{$record->member_id}");
    }

    function transcripts($chat_id = null)
    {

        $params = $this->db->query("SELECT * FROM chats WHERE id = {$chat_id} ")->row_array();
        $params['transcripts'] = $this->db->query("

                SELECT
                  chat_transcripts.*,
                  members.username
                FROM chat_transcripts
                JOIN members ON members.id = chat_transcripts.member_id
                WHERE chat_id = $chat_id
                ORDER BY id

            ")->result();

        $this->load->model('nrr_model');
        $params['hasNRR'] = $hasNRR = $this->nrr_model->check_nrr_for_chat($chat_id);

        $page['content'] = $this->load->view('admin/chat_transcripts/details', $params, true);        
        $this->load->view('admin/template', $page);
        
        //$this->load->view('admin/partial/header');
        //$this->load->view('admin/partial/footer');
    }

    function process_nrr($chat_id)
    {

        $this->load->model('nrr_model');

        //--- Create NRR
        $array = $this->nrr_model->create($chat_id, 1);

        //--- Process NRR
        $this->nrr_model->process($array['nrr_id'], 'paid', $array['amount']);

        //--- Response
        $this->session->set_flashdata('response', "NRR has been processed");

        //--- Redirect
        redirect("/admin/main/transactions/{$array['client_id']}");
    }

    function zero_out()
    {
        $user_id = $this->input->post("user_id");
        $this->db->where("member_id", $user_id);
        $this->db->update("member_balance", ["balance" => 0]);
        
        $return["status"] = "OK";
        echo json_encode($return);
    }

    /*9-16*/
    function admin_chat()
    {

        $t = array();
        $t['readers'] = $this->readers->getAll();

        $page['view_template'] = $this->load->view('admin/admin_chat/main', $t, true);
        $this->load->view('admin/template', $page);

    }
    /*9-18*/
    function admin_interrupt()
    {
        $t = array();
        $chats_array = $this->db->query("SELECT * FROM chats WHERE ended = 0 AND start_datetime IS NOT NULL AND client_id <> 1 AND start_datetime > NOW() - INTERVAL 24 HOUR")->result_array();

        $t['chats'] = [];

        foreach ($chats_array as $item) {

            $client_id = $item["client_id"];
            $client = $this->db->query("SELECT * FROM members WHERE id = '{$client_id}'")->row_array();
            $client_array = array("client_username" => $client["username"], "client_first_name" => $client["first_name"], "client_last_name" => $client["last_name"]);

            $reader_id = $item["reader_id"];
            $reader = $this->db->query("SELECT * FROM members WHERE id = '{$reader_id}'")->row_array();
            $reader_array = array("reader_username" => $reader["username"], "reader_first_name" => $reader["first_name"], "reader_last_name" => $reader["last_name"]);

            $new_array = [];

            foreach ($item as $key => $value) {

                $new_array[$key] = $value;
            }
            foreach ($client_array as $key => $value) {

                $new_array[$key] = $value;
            }
            foreach ($reader_array as $key => $value) {

                $new_array[$key] = $value;
            }

            array_push($t['chats'], $new_array);
        }

        $page['view_template'] = $this->load->view('admin/admin_interrupt/main', $t, true);
        $this->load->view('admin/template', $page);
    }

    function admin_interrupt_message($chat_session_id, $interrupt_type)
    {
        $t = array();
        $t['chat_session_id'] = $chat_session_id;
        $t['interrupt_type'] = $interrupt_type;

        $page['view_template'] = $this->load->view('admin/admin_interrupt/message', $t, true);
        $this->load->view('admin/template', $page);
    }

    function save_admin_interrupt_message()
    {
        $chat_session_id = $this->input->post("chat_session_id");
        $interrupt_type = $this->input->post("interrupt_type");
        $message = $this->input->post("message");

        $data = array(

            'chat_session_id' => $chat_session_id,
            'interrupt_type' => $interrupt_type,
            'message' => $message,
        );

        $this->db->insert('admin_interrupt', $data);
    }

    /*9-23*/
    function get_interrupt_reply_message()
    {
        $chat_session_id = $this->input->post("chat_session_id");
        $reply_messages = $this->db->query("SELECT * FROM admin_interrupt_reply WHERE chat_session_id = '{$chat_session_id}' AND admin_check = 0")->result_array();

        $messages = [];

        foreach ($reply_messages as $item) {

            $member_id = $item["member_id"];
            $member = $this->db->query("SELECT * FROM members WHERE id = '{$member_id}'")->row_array();
            $member_array = array("member_type" => $member["type"], "username" => $member["username"], "first_name" => $member["first_name"], "last_name" => $member["last_name"]);

            $new_array = [];

            foreach ($item as $key => $value) {

                $new_array[$key] = $value;
            }

            foreach ($member_array as $key => $value) {

                $new_array[$key] = $value;
            }

            array_push($messages, $new_array);
        }

        $return['messages'] = $messages;
        echo json_encode($return);
    }

    function admin_check_reply_message()
    {
        $id = $this->input->post("id");
        $this->db->where('id', $id);
        $this->db->update('admin_interrupt_reply', ['admin_check'=>1]);
    }

    /*10-17*/
    function chat_in_session()
    {
        $t = array();
        $chats_array = $this->db->query("SELECT * FROM chats WHERE ended = 0 AND start_datetime IS NOT NULL AND client_id <> 1 AND start_datetime > NOW() - INTERVAL 24 HOUR")->result_array();

        $t['data'] = [];

        foreach ($chats_array as $item) {

            $client_id = $item["client_id"];
            $client = $this->db->query("SELECT * FROM members WHERE id = '{$client_id}'")->row_array();
            $client_array = array("client_username" => $client["username"], "client_first_name" => $client["first_name"], "client_last_name" => $client["last_name"]);

            $reader_id = $item["reader_id"];
            $reader = $this->db->query("SELECT * FROM members WHERE id = '{$reader_id}'")->row_array();
            $reader_array = array("reader_username" => $reader["username"], "reader_first_name" => $reader["first_name"], "reader_last_name" => $reader["last_name"]);

            $new_array = [];

            foreach ($item as $key => $value) {

                $new_array[$key] = $value;
            }
            foreach ($client_array as $key => $value) {

                $new_array[$key] = $value;
            }
            foreach ($reader_array as $key => $value) {

                $new_array[$key] = $value;
            }

            array_push($t['data'], $new_array);
        }

        $page['view_template'] = $this->load->view('admin/end_chat/index', $t, true);
        $this->load->view('admin/template', $page);
    }

    function delete_chat_session()
    {
        $delete_chats = $this->input->post("chat_session");

        foreach ($delete_chats as $chat)
        {
            $this->chatmodel->delete_chat_session($chat);

            $reader_id = $this->db->query("SELECT reader_id FROM chats WHERE chat_session_id='{$chat}'")->row_array();
            $this->db->where('member_id', $reader_id['reader_id']);
            $update['status'] = 'online';
            $this->db->update('member_profiles', $update);
        }
        $this->session->set_flashdata('response_end_chat', "Selected chats have been disconnected.");
    }

    function urgent_message()
    {
        $readers = $this->readers->get_online_readers();
        $t = array();
        $t['readers'] = $readers;
        $page['view_template'] = $this->load->view('admin/urgent_message/index', $t, true);
        $this->load->view('admin/template', $page);
    }
    function get_online_readers()
    {
        $readers = $this->readers->get_online_readers();
        $return["readers"] = $readers;

        echo json_encode($return);
    }

    function send_urgent_message()
    {
        $readers = $this->input->post("readers");
        $message = $this->input->post("message");
        $subject = $this->input->post("subject");

        foreach($readers as $reader)
        {
            $this->messages_model->sendAdminMessage($reader,$subject,$message);
        }

        $return["status"] = "ok";
        echo json_encode($return);

    }

    function get_interrupt_reply_message_all()
    {

        $reply_messages = $this->db->query("SELECT * FROM admin_interrupt_reply WHERE admin_check = 0")->result_array();

        $messages = [];

        foreach ($reply_messages as $item) {

            $member_id = $item["member_id"];
            $member = $this->db->query("SELECT * FROM members WHERE id = '{$member_id}'")->row_array();
            $member_array = array("member_type" => $member["type"], "username" => $member["username"], "first_name" => $member["first_name"], "last_name" => $member["last_name"]);

            $new_array = [];

            foreach ($item as $key => $value) {

                $new_array[$key] = $value;
            }

            foreach ($member_array as $key => $value) {

                $new_array[$key] = $value;
            }

            array_push($messages, $new_array);
        }

        $return['messages'] = $messages;
        echo json_encode($return);
    }

    function page_disconnect_reader()
    {
        $readers = $this->readers->get_all_readers();
        $t['data'] = $readers;
        $page['view_template'] = $this->load->view('admin/disconnect_readers/index', $t, true);
        $this->load->view('admin/template', $page);

    }

    function disconnect_reader()
    {
        $reader_id = $this->input->post("id");
        $this->db->where("member_id", $reader_id);
        $this->db->update("member_profiles", ["status" => "offline", "disconnect" => 1]);
        $this->session->set_flashdata('response_disconnect', "Reader #{$reader_id} has been disconnected.");
    }

    function reader_whitelist()
    {
        $whitelist_readers = $this->readers->get_whitelist_readers();
        $t['data'] = $whitelist_readers;
        $page['view_template'] = $this->load->view('admin/reader_whitelist/index', $t, true);
        $this->load->view('admin/template', $page);
    }

    function delete_whitelist($whitelist_id)
    {

        $this->db->where('id', $whitelist_id);
        $this->db->delete('reader_whitelist');
        $this->session->set_flashdata('response_delete', "White List #{$whitelist_id} has been deleted.");
        redirect('/admin/main/reader_whitelist');

    }

    function add_whitelist()
    {
        $clients = $this->db->query("SELECT id, username FROM members WHERE type IS NULL AND active=1 AND deleted=0")->result_array();
        $readers = $this->db->query("SELECT members.id, members.username FROM members LEFT JOIN member_profiles ON member_profiles.member_id = members.id WHERE members.`type`='READER' AND members.active=1 AND members.deleted=0 and member_profiles.status in ('online','busy','break','blocked','offline')")->result_array();

        $t['clients'] = $clients;
        $t['readers'] = $readers;
        $page['view_template'] = $this->load->view('admin/reader_whitelist/add', $t, true);
        $this->load->view('admin/template', $page);
    }

    function add_new_whitelist()
    {
        $client_id = $this->input->post('client_id');
        $readers = $this->input->post('readers');

        foreach($readers as $reader)
        {
            $query = $this->db->get_where('reader_whitelist',array('client_id'=>$client_id,'reader_id'=>$reader));
            if($query->num_rows() == 0)
            {
                $data = array(
                    'client_id' => $client_id,
                    'reader_id' => $reader,
                    'created_at' => date("Y-m-d")
                );
                $this->db->insert('reader_whitelist', $data);
            }
        }
        $this->session->set_flashdata('response_save', "White List has been added.");
        $return['status'] = 'success';
        echo json_encode($return);
    }

    /*Copyright Api*/
    function copyscape_api_call($operation, $params = array(), $xmlspec = null, $postdata = null)
    {
        define('COPYSCAPE_USERNAME', 'ericvp2016');
        define('COPYSCAPE_API_KEY', 'ghl6juziz9rfuykc');

        define('COPYSCAPE_API_URL', 'http://www.copyscape.com/api/');
        $url = COPYSCAPE_API_URL . '?u=' . urlencode(COPYSCAPE_USERNAME) .
            '&k=' . urlencode(COPYSCAPE_API_KEY) . '&o=' . urlencode($operation);

        foreach ($params as $name => $value)
            $url .= '&' . urlencode($name) . '=' . urlencode($value);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, isset($postdata));

        if (isset($postdata))
            curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

        $response = curl_exec($curl);
        curl_close($curl);
        $arr = simplexml_load_string($response);
        if (strlen($response))
            return $arr; //$this->copyscape_read_xml($response, $xmlspec);
        else
            return false;
    }

    function copyscape_api_text_search($text, $encoding, $full = null, $operation = 'csearch')
    {
        $params['e'] = $encoding;

        if (isset($full))
            $params['c'] = $full;

        return $this->copyscape_api_call($operation, $params, array(2 => array('result' => 'array')), $text);
    }

    function test_copy()
    {
        $exampletext = strip_tags($this->input->post('content'));
        $variable =$this->copyscape_api_text_search($exampletext, 'ISO-8859-1', null, 'csearch');

        $return['data'] = $variable;
        echo json_encode($return);
    }

    function check_duplicate_block_word()
    {
        $block_word = $this->input->post('block_word');
        $get_all_block_words = $this->db->query("SELECT block_words from word_block")->result_array();
        foreach($get_all_block_words as $word)
        {
            if($block_word == $word['block_words'])
            {
                $return['status'] = 'exist';
                echo json_encode($return);
                return;
            }
        }

        $return['status'] = 'new';
        echo json_encode($return);

    }
}
