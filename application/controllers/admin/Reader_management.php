<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reader_Management extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('admin_is_logged'))
        {
            redirect('/admin/login');
            exit;
        }

        $this->load->helper('inflector');
        $this->error = null;
        $this->response = null;
        $this->open_nav = false;
        $this->admin = $this->session->userdata('admin_is_logged');
        $this->results_per_page = 100;
    }

    function index()
    {

        $t = array();
        $t['readers'] = $this->readers->getAll();

        $page['view_template'] = $this->load->view('admin/readers/main', $t, true);
        $this->load->view('admin/template', $page);
    }

    function payment_report()
    {

        $t = array();
        $t['readers'] = $this->readers->getAll();

        $page['view_template'] = $this->load->view('admin/readers/main', $t, true);
        $this->load->view('admin/template', $page);
    }

    function all()
    {
        $mainNavId = 12;
        $subNavId = 35;
        // START Main Navigation Selection
        $subNavId = ($subNavId == '0' ? null : $subNavId);

        $sql = "SELECT * FROM vadmin_nav WHERE id = {$mainNavId} LIMIT 1";
        $getMainNav = $this->db->query($sql);
        $data['nav'] = $getMainNav->row_array();
        $data['nav']['title'] = "Reader Management";

        $this->open_nav = $data['nav']['id'];

        if (!$subNavId)
            $data['subnav'] = false;
        else
        {
            $sql = "SELECT * FROM vadmin_navsub WHERE id={$subNavId} LIMIT 1";
            $getSubNav = $this->db->query($sql);
            $data['subnav'] = $getSubNav->row_array();
        }

        $data['subnav']['title'] = 'Readers';
        // Get the right content
        $table = ($data['subnav']['table'] ? $data['subnav']['table'] : $data['nav']['table']);

        $sql = "SELECT members.id, members.username, members.email FROM {$table} 
                LEFT JOIN member_profiles ON member_profiles.member_id = {$table}.id
                WHERE member_profiles.member_id IS NOT NULL AND {$table}.deleted = 0 AND {$table}.type = 'READER'";

        $totalRecords = $this->db->query($sql);
        $limiter = ($this->uri->segment('6') ? $this->uri->segment('6') : "0");

        // Paginate Results
        $config['base_url'] = "/'admin/main/pages/{$mainNavId}/" . (isset($data['subnav']['id']) ? $data['subnav']['id'] : $data['nav']['id']) . "/";
        $data['total_results'] = $config['total_rows'] = $totalRecords->num_rows();
        $config['per_page'] = $this->results_per_page;
        $config['uri_segment'] = 6;
        $config['num_links'] = 5;

        $this->pagination->initialize($config);

        // Get data
        $sql = $sql . " LIMIT {$limiter}, {$config['per_page']}";
        $getData = $this->db->query($sql);

        if ($getData->num_rows() == 0)
        {
            $data['data'] = false;
            $data['pagination'] = false;
        } else
        {
            $data['data'] = $getData->result_array();
            $data['pagination'] = $this->pagination->create_links();
        }

        // Get Fields
        $customFields = $this->vadmin->get_field_spec($table, 'show_overview_fields');

        if ($customFields)
            $fieldArray = explode(',', trim($customFields['value']));
        else
            $fieldArray = $this->db->list_fields($table);

        $data['fields'] = $fieldArray;
        $data['specs'] = $this->vadmin->get_table_specs($table);

        $data["add_link"] = "/admin/reader_management/add";
        $data['block_link'] = "/admin/reader_management/block";
        $data['un_block_link'] = "/admin/reader_management/un_block";

        $new_reader_data = [];

        foreach ($data['data'] as $table) {
            $data["edit_link"][$table['id']] = "/admin/reader_management/edit/{$data["nav"]['id']}/" . ($data["subnav"]['id'] ? $data["subnav"]['id'] : '0') . "/{$table['id']}";

            $reader_block_check = $this->db->query("SELECT id FROM reader_block WHERE reader_id={$table['id']}")->result_array();
            $reader_block_check = count($reader_block_check);
            $new_array = [];
            foreach($table as $key => $field)
            {
                $new_array[$key] = $field;
            }
            $new_array['block_reader']  = $reader_block_check > 0 ? true:false;
            array_push($new_reader_data, $new_array);
        }
        $data['data'] = $new_reader_data;
        // Load views
        $page["view_template"] = $this->load->view('admin/pages/index', $data, true);
        $this->load->view('admin/template', $page);
    }

    function block($reader_id)
    {
        $data = array('reader_id' => $reader_id);
        $this->db->insert('reader_block', $data);
        $redirect_url = "/admin/reader_management/all";
        redirect($redirect_url);
    }
    function un_block($reader_id)
    {
        $this->db->where('reader_id', $reader_id);
        $this->db->delete('reader_block');
        $redirect_url = "/admin/reader_management/all";
        redirect($redirect_url);
    }

    function add()
    {
        $mainNavId = 12;
        $subNavId = 35;
        // START Main Navigation Selection
        $subNavId = ($subNavId == '0' ? null : $subNavId);

        $sql = "SELECT * FROM vadmin_nav WHERE id = {$mainNavId} LIMIT 1";
        $getMainNav = $this->db->query($sql);
        $data['nav'] = $getMainNav->row_array();

        $this->open_nav = $data['nav']['id'];

        if (!$subNavId)
            $data['subnav'] = false;
        else
        {
            $sql = "SELECT * FROM vadmin_navsub WHERE id={$subNavId} LIMIT 1";
            $getSubNav = $this->db->query($sql);
            $data['subnav'] = $getSubNav->row_array();
        }

        // Get Table name
        $table = ($data['subnav']['table'] ? $data['subnav']['table'] : $data['nav']['table']);

        // Get Record Data
        $data['data'] = false;

        $data["form_action"] = "/admin/reader_management/submit";
        // Get Table Fields
        $data['fields'] = $fields = $this->db->list_fields($table);
        $data['specs'] = $this->vadmin->get_table_specs($table);
        $data['sidemodules'] = false;

        // Load views
        $page['view_template'] = $this->load->view('admin/pages/add', $data, true);
        $this->load->view('admin/template', $page);
    }

    function edit($mainNavId = null, $subNavId = null, $recordId = null)
    {
        
        // START Main Navigation Selection
        $subNavId = ($subNavId == '0' ? null : $subNavId);

        $sql = "SELECT * FROM vadmin_nav WHERE id = {$mainNavId} LIMIT 1";
        $getMainNav = $this->db->query($sql);
        $nav = $getMainNav->row_array();

        $this->open_nav = $nav['id'];

        if (!$subNavId)
            $subnav = false;
        else
        {
            $sql = "SELECT * FROM vadmin_navsub WHERE id={$subNavId} LIMIT 1";
            $getSubNav = $this->db->query($sql);
            $subnav = $getSubNav->row_array();
        }

        $data['nav'] = $nav;
        $data['subnav'] = $subnav;
        // Get Table name
        $table = ($data['subnav']['table'] ? $data['subnav']['table'] : $data['nav']['table']);

        // Get Record Data
        $sql = "SELECT * FROM {$table} WHERE id = {$recordId} LIMIT 1";
        $getData = $this->db->query($sql);
        $row = $getData->row_array();
        $data['data'] = $row;

        // Get Table Fields
        $data['fields'] = $fields = $this->db->list_fields($table);
        $specs = $this->vadmin->get_table_specs($table);
        $data['specs'] = $specs;
        $data['sidemodules'] = false;

        // Check for any sideModules
        if (isset($data['specs']['sidemodule']))
        {
            $moduleArray = explode('**', trim($data['specs']['sidemodule']['value']));
            $data['sidemodules'] = $moduleArray;
        }


        $hidden_fields = array();

        if (isset($specs['hide_fields']))
        {
            $hidden_fields = explode(',', $specs['hide_fields']['value']);
        }

        // Check if this page is a direct edit or edit through navigation
        // Simpy by checking to existence of $nav['id']
        if (isset($nav['id']))
        {
            // Through Nav
            $form_action = "/admin/reader_management/save/{$nav['id']}/" . ($subnav['id'] ? $subnav['id'] : '0') . "/{$row['id']}";
            // Buttons
            $cancel_button = "/admin/reader_management/all";
        }

        $delete_button = "/admin/pages/delete/{$nav['id']}/" . ($subnav['id'] ? $subnav['id'] : '0') . "/{$row['id']}";

        $data['form_action'] = $form_action;
        $data['hidden_fields'] = $hidden_fields;
        $data['cancel_button'] = $cancel_button;
        $data['delete_button'] = $delete_button;

        // Load views
        $page['view_template'] = $this->load->view('admin/pages/edit', $data, true);
        $this->load->view('admin/template', $page);
    }

    function save($mainNavId = null, $subNavId = null, $recordId = null)
    {

        $subNavId = ($subNavId == '0' ? null : $subNavId);

        // Get Table Info
        $getMainNav = $this->db->query("SELECT * FROM vadmin_nav WHERE id = {$mainNavId} LIMIT 1");
        $nav = $getMainNav->row_array();

        // Subnav
        if (!$subNavId)
            $data['subnav'] = false;
        else
        {

            $getSubNav = $this->db->query("SELECT * FROM vadmin_navsub WHERE id={$subNavId} LIMIT 1");
            $subnav = $getSubNav->row_array();
        }

        $table = (isset($subnav['table']) ? $subnav['table'] : $nav['table']);
        $title = (isset($subnav['title']) ? $subnav['title'] : $nav['title']);

        // Get Fields
        $fieldArray = $this->db->list_fields($table);
        $specs = $this->vadmin->get_table_specs($table);
        $updateArray = array();

        // Get Hidden Fields
        $hidden_fields = array();

        if (isset($specs['hide_fields']))
        {

            $hidden_fields = explode(',', $specs['hide_fields']['value']);
        }

        $this->load->library('form_validation');

        foreach ($fieldArray as $f) {

            if (in_array($f, $hidden_fields))
            {
                
            } else
            {

                // Get Field Based On SPEC Data
                $spec_type = strtolower((isset($specs['spec'][$f]) ? $specs['spec'][$f]['value'] : "TB||50"));
                $spec_array = explode("||", $spec_type);

                // Load & Configure The Module
                $specMod = trim($spec_array[0]);
                if ($f == 'id')
                    $specMod = 'lb';

                // Load Field
                $this->$specMod->config($f, $this->input->post($f), $spec_array);
                $fieldValue = $this->$specMod->process_form();
                $isRequired = isset($this->$specMod->isRequired) ? $this->$specMod->isRequired : false;

                // Create Update Array
                if ($fieldValue != '[%skip%]')
                {
                    $updateArray[$f] = $fieldValue;
                }

                $this->form_validation->set_rules($f, $f, 'trim');
                if ($isRequired)
                {
                    $this->form_validation->set_rules($f, $this->lang->line($f, FALSE), 'required');
                }
            }
        }

        if ($this->form_validation->run())
        {
            $this->db->where('id', $recordId);
            $this->db->update($table, $updateArray);

            $this->session->set_flashdata('response', "Record #{$recordId} has been saved in {$title}");
            redirect("/admin/reader_management/all");
        }

        redirect("/admin/reader_management/edit/{$mainNavId}/{$subNavId}/{$recordId}");
    }

    function submit()
    {
        $mainNavId = 12;
        $subNavId = 35;

        $subNavId = ($subNavId == '0' ? null : $subNavId);

        // Get Table Info
        $sql = "SELECT * FROM vadmin_nav WHERE id = {$mainNavId} LIMIT 1";
        $getMainNav = $this->db->query($sql);
        $nav = $getMainNav->row_array();

        // Subnav
        if (!$subNavId)
            $data['subnav'] = false;
        else
        {
            $sql = "SELECT * FROM vadmin_navsub WHERE id={$subNavId} LIMIT 1";
            $getSubNav = $this->db->query($sql);
            $subnav = $getSubNav->row_array();
        }

        $table = (isset($subnav['table']) ? $subnav['table'] : $nav['table']);
        $title = (isset($subnav['title']) ? $subnav['title'] : $nav['title']);

        // Get Fields
        $fieldArray = $this->db->list_fields($table);
        $specs = $this->vadmin->get_table_specs($table);
        $updateArray = array();

        // Get Hidden Fields
        $hidden_fields = array();

        if (isset($specs['hide_fields']))
        {
            $hidden_fields = explode(',', $specs['hide_fields']['value']);
        }

        $this->load->library('form_validation');

        foreach ($fieldArray as $f) {

            if (in_array($f, $hidden_fields))
            {
                
            } else
            {
                // Get Field Based On SPEC Data
                $spec_type = strtolower((isset($specs['spec'][$f]) ? $specs['spec'][$f]['value'] : "TB||50"));
                $spec_array = explode("||", $spec_type);

                // Load & Configure The Module
                $specMod = trim($spec_array[0]);
                if ($f == 'id')
                    $specMod = 'lb';

                // Load Field
                $this->$specMod->config($f, $this->input->post($f), $spec_array);
                $fieldValue = $this->$specMod->process_form();
                $isRequired = isset($this->$specMod->isRequired) ? $this->$specMod->isRequired : false;

                // Create Update Array
                if ($fieldValue != '[%skip%]')
                {
                    $updateArray[$f] = $fieldValue;
                }

                $this->form_validation->set_rules($f, $f, 'trim');
                if ($isRequired)
                {
                    $this->form_validation->set_rules($f, $this->lang->line($f, FALSE), 'required');
                }
            }
        }

        if ($this->form_validation->run())
        {
            $this->db->insert($table, $updateArray);
            $recordId = $this->db->insert_id();

            $this->session->set_flashdata('response', "Record #{$recordId} has been added to {$title}");

            $insert = array();
            $insert['id'] = $recordId;
            $insert['member_id'] = $recordId;
            $insert['active'] = 1;

            $this->db->insert('member_profiles', $insert);
            redirect("/admin/reader_management/all");
        }

        redirect("/admin/reader_management/add");
    }

    function featured()
    {
        $mainNavId = 12;
        $subNavId = 35;
        // START Main Navigation Selection
        $subNavId = ($subNavId == '0' ? null : $subNavId);

        $sql = "SELECT * FROM vadmin_nav WHERE id = {$mainNavId} LIMIT 1";
        $getMainNav = $this->db->query($sql);
        $data['nav'] = $getMainNav->row_array();
        $data['nav']['title'] = "Reader Management";

        $this->open_nav = $data['nav']['id'];

        if (!$subNavId)
            $data['subnav'] = false;
        else
        {
            $sql = "SELECT * FROM vadmin_navsub WHERE id={$subNavId} LIMIT 1";
            $getSubNav = $this->db->query($sql);
            $data['subnav'] = $getSubNav->row_array();
        }

        $data['subnav']['title'] = 'Featured Readers';
        // Get the right content
        $table = ($data['subnav']['table'] ? $data['subnav']['table'] : $data['nav']['table']);

        $sql = "SELECT * FROM {$table} 
                LEFT JOIN member_profiles ON member_profiles.member_id = {$table}.id
                WHERE member_profiles.member_id IS NOT NULL AND members.deleted = 0  
                AND {$table}.type = 'READER'
                AND member_profiles.featured=1";

        $totalRecords = $this->db->query($sql);
        $limiter = ($this->uri->segment('6') ? $this->uri->segment('6') : "0");

        // Paginate Results
        $config['base_url'] = "/'admin/main/pages/{$mainNavId}/" . (isset($data['subnav']['id']) ? $data['subnav']['id'] : $data['nav']['id']) . "/";
        $data['total_results'] = $config['total_rows'] = $totalRecords->num_rows();
        $config['per_page'] = $this->results_per_page;
        $config['uri_segment'] = 6;
        $config['num_links'] = 5;

        $this->pagination->initialize($config);

        // Get data
        $sql = $sql . " LIMIT {$limiter}, {$config['per_page']}";
        $getData = $this->db->query($sql);

        if ($getData->num_rows() == 0)
        {
            $data['data'] = false;
            $data['pagination'] = false;
        } else
        {
            $data['data'] = $getData->result_array();
            $data['pagination'] = $this->pagination->create_links();
        }
        // Get Fields
        $customFields = $this->vadmin->get_field_spec($table, 'show_overview_fields');

        if ($customFields)
            $fieldArray = explode(',', trim($customFields['value']));
        else
            $fieldArray = $this->db->list_fields($table);

        $data['fields'] = $fieldArray;
        $data['specs'] = $this->vadmin->get_table_specs($table);

        $data["add_link"] = "/admin/reader_management/add";
        $data['block_link'] = "/admin/reader_management/block";
        $data['un_block_link'] = "/admin/reader_management/un_block";

        $new_reader_data = [];

        foreach ($data['data'] as $table) {
            $data["edit_link"][$table['id']] = "/admin/reader_management/edit/{$data["nav"]['id']}/" . ($data["subnav"]['id'] ? $data["subnav"]['id'] : '0') . "/{$table['id']}";

            $reader_block_check = $this->db->query("SELECT id FROM reader_block WHERE reader_id={$table['id']}")->result_array();
            $reader_block_check = count($reader_block_check);
            $new_array = [];
            foreach($table as $key => $field)
            {
                $new_array[$key] = $field;
            }
            $new_array['block_reader']  = $reader_block_check > 0 ? true:false;
            array_push($new_reader_data, $new_array);
        }
        $data['data'] = $new_reader_data;

        if ($data['data'])
        {
            foreach ($data['data'] as $table) {
                $data["edit_link"][$table['id']] = "/admin/reader_management/edit/{$data["nav"]['id']}/" . ($data["subnav"]['id'] ? $data["subnav"]['id'] : '0') . "/{$table['id']}";
            }
        }
        // Load views
        $page["view_template"] = $this->load->view('admin/pages/index', $data, true);
        $this->load->view('admin/template', $page);
    }

    public function new_payment()
    {
        if ($this->input->post("paypal") == "Pay with paypal")
        {
            // Set paypal variables
            $m = $this->member->get_member_data($this->input->post('readerid'));
            $t['item'] = "Paypal payment";
            $t['item_number'] = null;
            $t['custom'] = $this->input->post('readerid') . "*" . "payment";
            $t['return_url'] = site_url("'admin/reader_management");
            $t['cancel_url'] = site_url("'admin/reader_management/paypal_cancel");
            $t['notify_url'] = site_url("paypal_ipn/deposit");
            $t['amount'] = $this->input->post('amount');
            $t['email'] = $m['paypal_email'];
            // Load paypal module
            $this->load->view('admin/paypal', $t);
        } else
        {
            $readerid = $this->input->post('readerid');
            $region = $this->input->post('region');
            $amount = $this->input->post('amount');
            $notes = $this->input->post('notes');


            $this->member->data['id'] = $readerid;
            $this->member_funds->insert_transaction('payment', $amount, $region, $notes);

            redirect("/admin/reader_management");
        }
    }

    public function paypal_cancal()
    {
        $this->session->set_flashdata('error', "Your PayPal payment has been canceled. ");

        $redirect = $this->session->userdata('redirect');
        if ($redirect)
        {
            redirect($redirect);
            exit;
        }

        redirect("/my_account");
    }

    public function download_transactions()
    {

        set_time_limit(0);
        ini_set('memory_limit', '-1');

        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=reader_transactions.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        $readerid = $this->input->post('readerid');
        $from = date("Y-m-d H:i:s", strtotime($this->input->post('from')));
        $to = date("Y-m-d H:i:s", strtotime($this->input->post('to')));
        $region = $this->input->post('region');
        $service = $this->input->post('service');

        $get = $this->db->query("
            SELECT
                members.id as reader_id,
                members.username as reader,
                profile_balance.datetime,
                profile_balance.tier,
                profile_balance.type,
                profile_balance.region,
                profile_balance.amount,
                profile_balance.commission as payment

            FROM profile_balance
            LEFT JOIN members ON members.id = profile_balance.reader_id
            WHERE
                " . ($readerid ? "reader_id = {$readerid} AND " : "") . "
                (datetime BETWEEN '{$from}' AND '{$to}') AND
                " . ($region ? "region = '{$region}' AND " : "") . "
                " . ($service ? "type = '" . ($service == 'chat' ? "reading" : "email") . "' AND " : "") . "
                profile_balance.id IS NOT NULL

        ");

        echo $this->array2csv($get->result_array());
    }

    public function array2csv(array &$array)
    {
        if (count($array) == 0)
        {
            return null;
        }
        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, array_keys(reset($array)));
        foreach ($array as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }

    public function new_reader()
    {
        $memberid = $this->input->post('memberid');
        $legacy = $this->input->post('legacy');

        $insert = array();
        $insert['member_id'] = $memberid;
        $insert['active'] = 1;
        if ($legacy)
        {
            $insert['legacy_member'] = 1;
        }

        $this->db->insert('member_profiles', $insert);

        redirect("/admin/reader_management");
    }

    public function toggleFeatured($readerId = null)
    {
        $this->reader->init($readerId);
        $reader = $this->reader->data;

        $update = array();
        $update['featured'] = ($reader['featured'] ? 0 : 1);

        $this->db->where('member_id', $readerId);
        $this->db->update('member_profiles', $update);

        redirect("/admin/reader_management/all");
    }

}
