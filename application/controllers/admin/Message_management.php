<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Message_Management extends CI_Controller
{

    function __construct()
    {

        parent::__construct();
        $this->session->set_userdata('member_logged', 1);

        if (!$this->session->userdata('admin_is_logged')) {
            redirect('/admin/login');
            exit;
        }

        $this->error = null;
        $this->response = null;
        $this->open_nav = false;
        $this->admin = $this->session->userdata('admin_is_logged');

        $this->load->model("messages_model");

    }

    function index()
    {

        $params['title'] = "Messages (Inbox)";

        $messages = [];
        $senders = [];
        $sender = [];

        $senders_array = $this->db->query("SELECT DISTINCT sender_id, members.first_name, members.username FROM messages 
                                      LEFT JOIN members ON messages.sender_id=members.id
                                      WHERE ricipient_id =1  AND sender_id IS NOT NULL AND (messages.`type`<>'email' OR messages.`type` IS NULL) ORDER BY messages.id DESC")->result_array();
        foreach ($senders_array as $item) {
            $unread = $this->db->query("SELECT COUNT(id) as unread FROM messages WHERE  ricipient_id ={$this->member->data['id']} AND sender_id='{$item['sender_id']}' AND `read` = 0")->row_array();
            $sender['first_name'] = $item['first_name'];
            $sender['username'] = $item['username'];
            $sender['sender_id'] = $item['sender_id'];
            $sender['unread'] = $unread['unread'];

            array_push($senders, $sender);
            $messages_sub = $this->db->query("SELECT * FROM messages WHERE ricipient_id = {$this->member->data['id']} AND (`type`<>'email' or `type` IS NULL) AND sender_id = '{$sender['sender_id']}' ORDER BY id DESC")->result_array();
            $messages[$sender['first_name']] = $messages_sub;
        }
        $unread_admin = $this->db->query("SELECT COUNT(id) as unread_admin FROM messages WHERE  ricipient_id ={$this->member->data['id']} AND `type`='admin' AND `read` = 0")->row_array();

        $params['messages'] = $messages;
        $params['senders'] = $senders;
        $params['unread_admin'] = $unread_admin;
        $params['v_from'] = "";

        $page['view_template'] = $this->load->view('admin/messages/messages', $params, true);
        $this->load->view('admin/template', $page);
    }

    function outbox()
    {

        $params['title'] = "Messages(Outbox)";

        $messages = [];
        $receivers = [];
        $receiver = [];

        $receiver_array = $this->db->query("SELECT DISTINCT ricipient_id, members.first_name, members.username FROM messages 
                                      LEFT JOIN members ON messages.ricipient_id=members.id
                                      WHERE sender_id =1  AND ricipient_id IS NOT NULL AND ricipient_id <> '{$this->member->data['id']}' AND (messages.`type`<>'email' OR messages.`type` IS NULL) ORDER BY messages.id DESC")->result_array();
        foreach ($receiver_array as $item) {
            $receiver['first_name'] = $item['first_name'];
            $receiver['username'] = $item['username'];
            $receiver['receiver_id'] = $item['ricipient_id'];

            if ($this->member->data['type'] == 'READER') {
                $receiver['banned'] = 0;
                $check_ban = $this->db->query("SELECT id FROM member_bans WHERE reader_id='{$this->member->data['id']}' AND member_id='{$receiver['receiver_id']}' AND `type`='personal'")->result_array();
                if (count($check_ban) > 0) {
                    $receiver['banned'] = 1;
                }
            }

            array_push($receivers, $receiver);
            $messages_sub = $this->db->query("SELECT * FROM messages WHERE sender_id = {$this->member->data['id']} AND (`type`<>'email' or `type` IS NULL) AND ricipient_id = '{$receiver['receiver_id']}' ORDER BY id DESC")->result_array();
            $messages[$receiver['first_name']] = $messages_sub;
        }

        $params['messages'] = $messages;
        $params['receivers'] = $receivers;
        $params['v_from'] = "";

        $page['view_template'] = $this->load->view('admin/messages/messages_outbox', $params, true);
        $this->load->view('admin/template', $page);

    }

    function compose($message_reply_id = null, $type = "reply")
    {
        $sql = "SELECT
                    members.id,
                    members.username,
                    members.first_name
                FROM members
                WHERE members.active =1 AND members.deleted=0 AND id <> 1
                ORDER BY members.username";
        $getAllMembers = $this->db->query($sql);
        $params['users'] = $getAllMembers->result_array();

        $params['to'] = "";
        $params['subject'] = "";
        $params['message'] = "";

        // If a reply id was passed… Then
        // prepopulate the fields
        if ($message_reply_id && $type == "reply") {
            $sql = "SELECT * FROM messages WHERE id = {$message_reply_id} AND (ricipient_id = {$this->member->data['id']} OR sender_id = {$this->member->data['id']}) LIMIT 1";
            $getMessage = $this->db->query($sql);

            if ($getMessage->num_rows() == 1) {
                $message = $getMessage->row_array();

                $sender = $this->system_vars->get_member($message['sender_id']);

                $params['to'] = $message['sender_id'];
                $params['to_user'] = $sender;
                $params['subject'] = "Re: " . $message['subject'];
                $params['message'] = "\n\n\n+++ Original Message: {$sender['first_name']} {$sender['last_name']} - {$sender['username']} on " . date("m/d/y h:i:s", strtotime($message['datetime'])) . " +++\n\n" . html_entity_decode($message['message']);
            }
        } else if ($message_reply_id && $type != "reply") {
            $params['to'] = $message_reply_id;
            $params['to_user'] = $this->system_vars->get_member($message_reply_id);
            $params['subject'] = "";
            $params['message'] = "";
        }

        $page['view_template'] = $this->load->view('admin/messages/messages_compose', $params, true);
        $this->load->view('admin/template', $page);

    }

    function upload_file()
    {
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png|iso|dmg|zip|rar|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|rtf|sxc|sxi|txt|exe|avi|mpeg|mp3|mp4|3gp';
        $config['max_size'] = 204800;
        $config['max_height'] = 768;
        $config['max_width'] = 1024;
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('attach')) {
            return $this->upload->data();
        } else {
            return $this->upload->display_errors();
        }
    }

    function compose_submit()
    {
        $this->form_validation->set_rules("to", "Recipient", "trim|required");
        $this->form_validation->set_rules("subject", "Subject", "trim|required");
        $this->form_validation->set_rules("message", "Message", "trim|required");

        if (!$this->form_validation->run()) {

            $this->compose();

        } else {
            //send mail
            if (set_value('send-private') == '1') {
                //send mail
                $CI = &get_instance();
                $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'mail.taroflash.com',
                    'smtp_port' => 465,
                    'smtp_crypto' => 'ssl',
                    'smtp_timeout' => 7,
                    'smtp_user' => 'norreply@psychic-contact.com',
                    'smtp_pass' => 'NoRep2017*1245Klm1P32124',
                    'mailtype' => 'html',
                    'charset' => 'iso-8859-1'
                );
                $CI->load->library('email', $config);

                // Parse system email template
                $subject = set_value('subject');
                $pre_content = html_entity_decode(nl2br(set_value('message')));

                $content = "
                        <div style='background:#b9b9b9;padding:20px;'>
                            <div style='margin-bottom:10px;font-size:20px;font-weight:bold;font-family:Arial;'>Psychic-Contact.com</div>
                            <div style='background:#FFF;padding:20px;'>{$pre_content}</div>
                        </div>
                        ";

                $file_data = $this->upload_file();

                // Configure Email Options
                $config['mailtype'] = 'html';
                $CI->email->initialize($config);

                // Send Email
                $from[1] = $CI->config->item('email_from_email_address');
                $from[2] = $CI->config->item('email_from_name');

                // IF the TO is a number
                // Get the member and send the message to their local inbox
                // as well as their email address
                $to = set_value('to');

                if (is_numeric($to)) {

                    $member = $this->db->query("SELECT email FROM members WHERE `id`='{$to}'")->row_array();

                    $to = $member['email'];
                }

                $CI->email->from($from[1], $from[2]);
                $CI->email->to($to);
                $CI->email->subject($subject);
                $CI->email->message($content);

                if (!is_string($file_data)) {
                    $CI->email->attach($file_data['full_path']);
                } else {

                }
                $log = $CI->email->send();
                log_message('debug', $log);

            }

            $this->member->notify(set_value('to'), $this->member->data['id'], set_value('subject'), nl2br(set_value('message')));

            $this->session->set_flashdata('response', "Your message has been sent");

            redirect('/admin/message_management');
        }

    }

    function view($message_id)
    {

        $getMessages = $this->db->query("SELECT *
			                                 FROM   messages
			                                 WHERE  id = {$message_id}
			                                        AND (ricipient_id = {$this->member->data['id']}
			                                        OR sender_id = {$this->member->data['id']}) LIMIT 1");
        $t = $getMessages->row_array();

        if ($t['name'] == 'no_reader') {
            $t['from'] = ['first_name' => 'System', 'last_name' => ''];
        } else {
            if ($t['type'] == 'email') {
                $t['from'] = ['first_name' => 'System', 'last_name' => ''];
            } else {
                $t['from'] = $this->system_vars->get_member($t['sender_id']);
            }
        }


        $t['to'] = $this->system_vars->get_member($t['ricipient_id']);


        $this->messages_model->markMessageRead($t['id']);

        $page['view_template'] = $this->load->view('admin/messages/messages_view', $t, true);
        $this->load->view('admin/template', $page);

    }

    function delete($message_id, $page = null)
    {

        $this->db->where('id', $message_id);
        $this->db->delete('messages');

        redirect('/admin/message_management/' . $page);
    }
}
