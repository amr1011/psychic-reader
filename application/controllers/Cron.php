<?php

    class Cron extends CI_Controller{

        public function __construct(){
            parent::__construct();
        }

        //--- VOID transactions that are 7 days old + have not been used
        public function checkToVoidTransactions(){

            /*
             *
             * Zero out member_balance
             * Add refund transaction to transactions table
             * Email user
             *
             * */

            $getTransactions = $this->db->query("

                SELECT
                    transactions.*

                FROM transactions

                WHERE
                    transactions.type = 'purchase' AND
                    transactions.payment_type = 'cc' AND
                    transactions.datetime <= DATE_SUB(NOW(), INTERVAL 7 DAY) AND
                    transactions.time_used = 0 AND
                    transactions.settled IS NULL

            ");

            if($getTransactions->num_rows() > 0){

                $this->load->model('member_billing');
                foreach($getTransactions->result() as $t){
                    $this->member_billing->void_transaction($t->id);
                }

            }

        }

        // 10-15
        function check_member_fund_type()
        {
            $now = date("Y-m-d H:i:s");
            $clients = $this->db->query("SELECT * FROM members WHERE `type` IS NULL")->result_array();
            $fund_setting_data = $this->db->query("SELECT * FROM fund_limit_setting")->result_array();
            $fund_type_update_time_frame = (int)$fund_setting_data[0]["time_frame"];

            foreach($clients as $client)
            {
                $client_registration_date = $client["registration_date"];
                $client_id = $client["id"];
                if((strtotime($now) - strtotime($client_registration_date)) > $fund_type_update_time_frame*30*3600 && $client["auto_updated"] == 0)
                {
                    $this->db->where("id", $client_id);
                    $this->db->update("members", array("member_fund_type" => "preferred", "auto_updated" => 1));

                    $client['type'] = 'email';
                    $client['description'] = "<p>Congratulations.</p><p>Your charge amount has no limit.</p>";
                    $this->system_vars->m_omail($client['id'], 'fund_type_changed', $client);
                }
            }
        }

        function check_online_readers()
        {
            $online_readers = $this->db->query("
      
                SELECT members.`id`, member_profiles.status
                FROM members
                LEFT JOIN member_profiles ON members.`id` = member_profiles.`member_id`
                WHERE members.`type` = 'READER' AND member_profiles.`status` = 'online' AND members.deleted = 0 AND members.active = 1;
            ")->result_array();

            $break_readers = $this->db->query("
      
                SELECT members.`id`, member_profiles.status
                FROM members
                LEFT JOIN member_profiles ON members.`id` = member_profiles.`member_id`
                WHERE members.`type` = 'READER' AND member_profiles.`status` = 'break' AND members.deleted = 0 AND members.active = 1;
            ")->result_array();

            $session_readers = $this->db->query("
      
                SELECT members.`id`, member_profiles.status
                FROM members
                LEFT JOIN member_profiles ON members.`id` = member_profiles.`member_id`
                WHERE members.`type` = 'READER' AND member_profiles.`status` = 'busy' AND members.deleted = 0 AND members.active = 1;
            ")->result_array();

            $online = count($online_readers);
            $break = count($break_readers);
            $session = count($session_readers);
            $all = $online + $break + $session;

            if($all <= 3)
            {
                if(($online==1 && $break==2) || ($online==1 && $break==1 && $session==0) || ($online==2 && $break==0 && $session==0) || ($session==2 && $online==0 && $break==0) || $all<2 || ($break==2 && $all==2))
                {
                    $all_readers = $this->db->query("SELECT * FROM members WHERE `type`='READER' AND `deleted`=0 AND `active`=1")->result_array();
                    $admin = $this->db->query("SELECT * FROM members WHERE `id`=1")->row_array();
                // stop until DevPsy is live because of tons of mail.
                //     // to MeBo
                //     $admin['type'] = NULL;
                //     $this->system_vars->m_omail($admin['id'], 'no_reader', $admin);
                //     foreach($all_readers as $reader)
                //     {
                //         $reader['type'] = NULL;
                //         $this->system_vars->m_omail($reader['id'], 'no_reader', $reader);
                //     }
                //
                //     // to NoBo
                //     $admin['type'] = 'email';
                //     $this->system_vars->m_omail($admin['id'], 'no_reader', $admin);
                //     foreach($all_readers as $reader)
                //     {
                //         $reader['type'] = 'email';
                //         $this->system_vars->m_omail($reader['id'], 'no_reader', $reader);
                //     }
                }
            }

        }

        function log_chat_history()
        {
            $this->load->helper('file');

            if (!is_dir('chat_logs')) {

                mkdir('chat_logs', 0777, TRUE);
            }

            $get_all_chats = $this->db->query("SELECT chats.*, members.username FROM chats LEFT JOIN members ON members.id=chats.reader_id WHERE create_datetime > DATE_SUB(NOW(), INTERVAL 6 MONTH)")->result_array();

            foreach($get_all_chats as $chat)
            {
                $check = $this->db->query("SELECT * FROM chat_transcripts WHERE chat_id = {$chat['id']} ")->result_array();

                if(count($check) > 0)
                {
                    $dir_name = $chat['username'];
                    $file_name_time = str_replace(':', '-', $chat['create_datetime']);
                    $file_name_time = str_replace(' ', '-', $file_name_time);
                    $file_name = $dir_name . "-" . $file_name_time;

                    if (!is_dir('chat_logs/'.$dir_name)) {

                        mkdir('chat_logs/'.$dir_name, 0777, TRUE);
                    }

                    if(!file_exists('chat_logs/'.$dir_name.'/'.$file_name.'.txt' ))
                    {
                        $file=fopen('chat_logs/'.$dir_name.'/'.$file_name.'.txt',"a+");
                    }
                    else {
                        $file=fopen('chat_logs/'.$dir_name.'/'.$file_name.'.txt', "a");
                    }

                    $getTranscripts = $this->db->query("SELECT chat_transcripts.*, members.username FROM chat_transcripts LEFT JOIN members ON members.id = chat_transcripts.member_id WHERE chat_id = {$chat['id']} ");

                    $ta =  $getTranscripts->result_array();

                    foreach($ta as $transcript)
                    {
                        $chat_history = $transcript['datetime'] . "    " . $transcript['username'] . "  :   " . strip_tags($transcript['message']) . "\n";
                        fwrite($file, $chat_history);
                    }
                }
            }
        }

        function create_upload_folder()
        {
            $this->load->helper('file');

            if (!is_dir('media/articles')) {

                mkdir('media/articles', 0777, TRUE);
            }
            echo('successful');exit;
        }

    }