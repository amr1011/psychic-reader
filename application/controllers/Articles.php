<?php

class articles extends CI_Controller
{

    function __construct()
    {

        parent :: __construct();

        $this->settings = $this->system_vars->get_settings();

        $this->load->model('responder_model');
        $this->load->model('reader');
    }

    function index()
    {
        $this->db->from("articles");
        $this->db->join("categories", "categories.id = articles.category_id");
        $this->db->join("members", "members.id = articles.profile_id");
        $this->db->select("articles.*, categories.url as category_url, categories.title as category_title, members.username", false);
        $this->db->where("articles.url IS NOT NULL", null, false);
        $this->db->where("articles.approved", "1");
        $this->db->order_by('datetime', 'DESC');
        $this->db->limit(8);
        $getArticles = $this->db->get();        
        $articleArray = $getArticles->result_array();

        $params['title'] = "Most Recent Articles";
        $params['previous_title'] = "Previous Articles";
        $params['archive'] = 0;
        $params['articles'] = $articleArray;

        $this->db->from("articles");
        $this->db->join("categories", "categories.id = articles.category_id");
        $this->db->join("members", "members.id = articles.profile_id");
        $this->db->select("articles.*, categories.url as category_url, categories.title as category_title, members.username", false);
        $this->db->where("articles.url IS NOT NULL", null, false);
        $this->db->where("articles.approved", "1");
        $this->db->order_by('datetime', 'DESC');
        $previousArticles = $this->db->get();
        $previousArticles = $previousArticles->result_array();

        for($i=0; $i<8; $i++)
        {
            unset($previousArticles[$i]);
        }

        $previousArticles = array_values ($previousArticles);
        $newPreviousArticles = [];
        $limit_article = $this->db->query("SELECT limit_articles FROM admin_article_limit LIMIT 1")->result_array();
        if(count($limit_article) > 0)
        {
            $limit_article = $limit_article[0]['limit_articles'];
        } else {
            $limit_article = 12;
        }

        for($j=0; $j<$limit_article-8; $j++)
        {
            if(isset($previousArticles[$j]))
            {
                array_push($newPreviousArticles, $previousArticles[$j]);
            }
        }

        $email_form_data = $this->responder_model->get_email_form('article');
        $email_form_detail_data = array();
        if (count($email_form_data) > 0) {
            $email_form_detail_data_pre = $this->responder_model->get_email_form_detail($email_form_data[0]['id']);
            foreach ($email_form_detail_data_pre as $email_form_detail_datum) {
                if ($email_form_detail_datum['item_type'] == 'list') {
                    $list_data_name = $email_form_detail_datum['data'];
                    $list_data = $this->get_list_menu_data($list_data_name);
                    $email_form_detail_datum['data'] = $list_data;
                    array_push($email_form_detail_data, $email_form_detail_datum);
                } else {
                    array_push($email_form_detail_data, $email_form_detail_datum);
                }
            }

            $params['email_form_data'] = $email_form_data[0];
            $params['email_form_detail_data'] = $email_form_detail_data;

        } else {
            $params['email_form_data'] = null;
            $params['email_form_detail_data'] = null;
        }

        // var_dump($email_form_data);
        // var_dump($email_form_detail_data);
        // exit;

        $params['previous_articles'] = $newPreviousArticles;

        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('/frontend/pages/article_categories', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/sub_page', $data);
    }

    function lst($category_url)
    {

        $category = $this->site->get_category($category_url);
        $sql = "SELECT 
                        articles.*,
                        categories.url as category_url,
                        categories.title as category_title,
                        members.username
                FROM 
                        articles 
                JOIN categories ON categories.id = articles.category_id
                JOIN members ON members.id=articles.profile_id

                WHERE 
                        categories.url = '{$category_url}' AND 
                        articles.url IS NOT NULL AND
                        articles.approved = 1

                ORDER BY 
                        articles.datetime DESC 

                LIMIT 8";
        $getArticles = $this->db->query($sql);

        $articleArray = $getArticles->result_array();

        $t['category'] = $category;
        $t['title'] = "Most Recent \"{$category['title']}\" Articles";
        $t['previous_title'] = "Previous \"{$category['title']}\" Articles";
        $t['archive'] = 0;
        $t['articles'] = $articleArray;
        $t['category_title'] = $category['title'];
        $t['url'] = $category['url'];

        $sql_prev = "SELECT 
                        articles.*,
                        categories.url as category_url,
                        categories.title as category_title,
                        members.username
                FROM 
                        articles 
                JOIN categories ON categories.id = articles.category_id
                JOIN members ON members.id=articles.profile_id

                WHERE 
                        categories.url = '{$category_url}' AND 
                        articles.url IS NOT NULL AND
                        articles.approved = 1

                ORDER BY 
                        articles.datetime DESC";
        $getPrevArticles = $this->db->query($sql_prev);
        $getPrevArticles = $getPrevArticles->result_array();

        for($i=0; $i<8; $i++)
        {
            unset($getPrevArticles[$i]);
        }

        $previousArticles = array_values ($getPrevArticles);
        $newPreviousArticles = [];
        $limit_article = $this->db->query("SELECT limit_articles FROM admin_article_limit LIMIT 1")->result_array();
        $limit_article = $limit_article[0]['limit_articles'];

        if(count($previousArticles) > 0)
        {
            $length = min(count($previousArticles), $limit_article);

            for($j=0; $j< $length-8; $j++)
            {
                array_push($newPreviousArticles, $previousArticles[$j]);
            }
        }

        $t['previous_articles'] = $newPreviousArticles;

        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Most Recent Articles",
            'header' => $this->load->view('frontend/layouts/header', $param_header, TRUE),
            'slider' => "",
            'content' => $this->load->view('/frontend/pages/article_category', $t, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/page', $data);
    }

    function archive($category_url, $pageno = 1)
    {

        $per_page = 2;

        $category = $this->site->get_category($category_url);

        $sql = "
                SELECT 
                        articles.*,
                        categories.url as category_url

                FROM 
                        articles 

                JOIN categories ON categories.id = articles.category_id

                WHERE 
                        categories.url = '{$category_url}' AND 
                        articles.url IS NOT NULL AND
                        articles.approved = 1

                ORDER BY
                        articles.datetime DESC ";

        $getAllArticles = $this->db->query($sql);
        $getArticles = $this->db->query($sql . " LIMIT {$pageno}, {$per_page}");

        $articleArray = $getArticles->result_array();

        // Pagination
        $this->load->library('pagination');

        $config['base_url'] = "/articles/{$category_url}/archive/";
        $config['uri_segment'] = '4';
        $config['total_rows'] = $getAllArticles->num_rows();
        $config['per_page'] = $per_page;

        $this->pagination->initialize($config);

        $t['pagination'] = $this->pagination->create_links();

        $t['category'] = $category;
        $t['title'] = "\"{$category['title']}\" Article Archive";
        $t['archive'] = 1;
        $t['articles'] = $articleArray;

        $this->load->view('frontend/partial/header');
        $this->load->view('frontend/pages/article_categories', $t);
        $this->load->view('frontend/partial/footer');
    }

    function view($category_url, $category_id)
    {

        $getArticle = $this->db->query("SELECT * FROM articles WHERE `id` = '{$category_id}' LIMIT 1");
        $t = $getArticle->row_array();

        $category = $this->db->query("SELECT * FROM categories WHERE id='{$t['category_id']}'")->row_array();
        $t['url'] = $category['url'];
        $t['category_title'] = $category['title'];

        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Most Recent Articles",
            'header' => $this->load->view('frontend/layouts/header', $param_header, TRUE),
            'slider' => "",
            'content' => $this->load->view('/frontend/pages/view_article', $t, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/sub_page', $data);
    }

    function get_list_menu_data($name)
    {
        if($name == 'reader') {
            $data = $this->readers->get_all_readers();
            return $data;
        }
    }

    function submit()
    {
        $form_data = $this->input->post();

        $message = "<div>";
        foreach($form_data as $key => $form_datum) {
            $message = $message . "<p>{$key}: {$form_datum}</p>";
        }
        $message = $message . "</div>";

        if($this->session->userdata('member_logged')) {

            $message_data = array(
                'datetime' => date('Y-m-d H:i:s'),
                'sender_id' => $this->member->data['id'],
                'ricipient_id' => 1,
                'name' => null,
                'subject' => 'Email Form Submit',
                'message' => $message,
                'read' => 0,
                'type' => NULL
            );

            $message_data_user = array(
                'datetime' => date('Y-m-d H:i:s'),
                'sender_id' => $this->member->data['id'],
                'ricipient_id' => $this->member->data['id'],
                'name' => null,
                'subject' => 'Email Form Submit',
                'message' => $message,
                'read' => 0,
                'type' => NULL
            );

            $this->db->insert('messages', $message_data);
            $this->db->insert('messages', $message_data_user);

        } else {
            $message_data = array(
                'datetime' => date('Y-m-d H:i:s'),
                'sender_id' => 1,
                'ricipient_id' => 1,
                'name' => null,
                'subject' => 'Email Form Submit',
                'message' => $message,
                'read' => 0,
                'type' => NULL
            );
            $this->db->insert('messages', $message_data);

            // send mail
            $CI = & get_instance();
            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'mail.taroflash.com',
                'smtp_port' => 465,
                'smtp_crypto' => 'ssl',
                'smtp_timeout' => 7,
                'smtp_user' => 'norreply@psychic-contact.com',
                'smtp_pass' => 'NoRep2017*1245Klm1P32124',
                'mailtype'  => 'html',
                'charset'   => 'iso-8859-1'
            );
            $CI->load->library('email', $config);

            // Parse system email template
            $subject = "Email Form Submit";

            $content = "
                        <div style='background:#b9b9b9;padding:20px;'>
                            <div style='margin-bottom:10px;font-size:20px;font-weight:bold;font-family:Arial;'>Psychic-Contact.com</div>
                            <div style='background:#FFF;padding:20px;'>{$message}</div>
                        </div>
                        ";

            $config['mailtype'] = 'html';
            $CI->email->initialize($config);

            // Send Email
            $from[1] = $CI->config->item('email_from_email_address');
            $from[2] = $CI->config->item('email_from_name');

            $CI->email->clear(true);
            $recipient = $form_data['email'];

            $CI->email->from($from[1], $from[2]);
            $CI->email->to($recipient);
            $CI->email->subject($subject);
            $CI->email->message($content);

            $CI->email->send();
        }

        redirect('/articles/responder_view');
    }

    function responder_view()
    {
        $data = $this->responder_model->get_email_form('article');
        $params['responder_name'] = $data[0]['responder_name'];
        $params['responder_content'] = $data[0]['responder_content'];
        $params['responder_image'] = $data[0]['responder_image'];

        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Responder Page",
            'header' => $this->load->view('frontend/layouts/header', $param_header, TRUE),
            'slider' => "",
            'content' => $this->load->view('/frontend/pages/responder_view', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/page', $data);
    }


}
