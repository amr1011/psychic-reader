<?php

class Phone extends CI_Controller
{

    function __construct()
    {

        parent :: __construct();
    }

    function index()
    {

        $params = array();
        $params['page'] = '0';
        $params['readers'] = $this->readers->getAll();        
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        
        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/layouts/header', $param_header, TRUE),
            'banner' => $this->load->view('frontend/layouts/banner', '', TRUE),
            'content' => $this->load->view('frontend/psychics/phone_readers', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', '', TRUE)
        );

        $this->load->library('parser');        
        $this->parser->parse('frontend/layouts/sub_page', $data);
    }

    public function initiateCall()
    {
        $name= $this->input->post('telephone');
        $name= $this->input->post('telephone');
        $client = new \GuzzleHttp\Client();

        $data =   [ 
                    'client_id'         => $this->input->post('member'), 
                    'reader_id'         => $this->input->post('reader'),
                    'client_number'     => $this->input->post('telephone'),
                    'max_call_duration' => 5
                   ];          
        $URL = "https://voiceapi.psychic-contact.com/api/voice/call";
        $response = $client->request('POST', $URL, [
                                        'debug'  => false,
                                        'verify' => false,
                                        'exceptions' => false,
                                        'headers' => [
                                              'Accept' => 'application/json',
                                              'Content-Type'  => 'application/json',
                                              'Authorization' => 'Bearer $2y$10$lIgoC2TQsyNTpZ2V6L9YseTmBGZ1WWAgGdXKqU/fxTHEwIHnK2gn.'
                                          ],   
                                       'json'    =>$data
                                  ]);
        header('Content-Type: application/json');        
        echo $response->getBody()->getContents();
        // return json_decode($response->getBody()->getContents(),true);        

    }

    function search()
    {

        // Format variables for easy use
        $category_id = trim($this->input->post('category'));
        $query = trim($this->input->post('query'));
        $searchResults = $this->readers->search($category_id, $query);
        if ($searchResults['error'] == '1')
        {

            $this->session->set_flashdata('warning', $searchResults['message']);
            redirect("/psychics");
        } else
        {

            $t['readers'] = $searchResults['readers'];

            $this->load->view('frontend/partial/header');
            $this->load->view('frontend/psychics/main', $t);
            $this->load->view('frontend/partial/footer');
        }
    }

}
