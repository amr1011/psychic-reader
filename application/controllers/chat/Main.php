<?php

require_once(APPPATH . "/controllers/Popup.php");

class main extends popup
{
    function __construct()
    {
        parent :: __construct();

        $this->load->model('reader');
        $this->load->model('chatmodel');
        $this->load->model('Member');
        $this->load->library('my_common');
        $this->settings = $this->system_vars->get_settings();
    }

    function chat_options($chat_type, $chat_id = null)
    {
        $chatObject = $this->chatmodel->setSession($chat_id);
        if ($chatObject == false)
        {
            $this->error("The chat session you requested has been expired.  Please retry. ");
            return;
        }
        $mem_id = $this->member->data['id'];
        $this->member->set_member_id($this->chatmodel->object['reader_id']);

        $chat['chat_type'] = $chat_type;
        $chat['chat_id'] = $chat_id;
        $chat['reader_username'] = $this->member->data['username'];
        $this->member->set_member_id($mem_id);

        $this->session->set_userdata("chat_id", $chat_id);

        $this->load->view("frontend/partial/header");
        $this->load->view('frontend/chat/timeout_options', $chat);
        $this->load->view("frontend/partial/footer");
    }

    function chat_ended($chat_id = null)
    {
        $chatObject = $this->chatmodel->setSession($chat_id);
        if ($chatObject == false)
        {
            $this->error("The chat session you requested has been expired.  Please retry. ");
            return;
        }
        $array['chat_id'] = $chat_id;
        $this->load->view("frontend/partial/header");
        $this->load->view('frontend/chat/chat_ended', $array);
        $this->load->view("frontend/partial/footer");
    }

    function submit_testimonial($chat_id = null)
    {
        $this->form_validation->set_rules('rating', 'Rating', 'required|xss_clean|trim');
        $this->form_validation->set_rules('review', 'Review', 'required|xss_clean|trim');

        if (!$this->form_validation->run())
        {
            // Show error
            $this->chat_ended($chat_id);
        } else
        {
            $chatObject = $this->chatmodel->setSession($chat_id);
            if ($chatObject == false)
            {
                $this->error("The chat session you requested has been expired.  Please retry. ");
                return;
            }
            $insert['member_id'] = $this->chatmodel->object['client_id'];
            $insert['reader_id'] = $this->chatmodel->object['reader_id'];
            $insert['rating'] = set_value("rating");
            $insert['datetime'] = date("Y-m-d G:i:s");
            $insert['review'] = set_value("review");
            $insert['chat_id'] = $chat_id;

            $this->db->insert("testimonials", $insert);
            $this->session->set_flashdata("response", "Testimonial submitted.");

            $this->load->model("messages_model");
            $this->messages_model->sendAdminMessage($this->chatmodel->object['reader_id'], "Testimonial Submitted", "Testimonial Submitted for Chat #" . $chat_id);

            redirect("/frontend/my_account/chats/transcript/{$chat_id}");
        }
    }

    function index($reader_username = null)
    {
        // var_dump($this->session->userdata('member_logged'));
        // var_dump($params = $this->reader->init($reader_username)->data);
        // die();

        $reader = $this->reader->init($reader_username)->data;
        //$params = $this->reader->init($reader_username)->data;
        //var_dump($this->reader->get_member_status($params["member_id"],true));
        //die();
        // if ($this->reader->get_member_status($reader['member_id'])->status=="offline") {
        //     echo 'abort';
        //     die();
        //     exit();
        // }
        if ($reader_username)
        {
            if ($this->session->userdata('member_logged'))
            {
                if($this->session->userdata('member_logged') == "1")
                {
                    $params = $this->reader->init($reader_username)->data;
                    $params['reader_data'] = $this->reader->init($reader_username)->data;
                    $params['time_balance'] = 99999; // converted to seconds

                    $params['title'] = "Confirm New Chat";
                    $params['detect'] = $this->detect;
                    $params['member_logged'] = $this->session->userdata('member_logged');

                    // update reader's last chat pending request
                    $this->reader->update_last_pending_request();


                    $reader = $this->reader->init($reader_username)->data;
                    $client_id = $this->session->userdata('member_logged');

                    $lastManualAbortChats = $this->chatmodel->getLastManualAbortChats($reader['member_id'], $client_id, 1);
                    $now = date('Y-m-d H:i:s');

                    if ($lastManualAbortChats) {

                        if (count($lastManualAbortChats) >= 1) {

                            $to_time = strtotime($now);
                            $from_time = strtotime($lastManualAbortChats[0]['create_datetime']);

                            $idel_time = round(abs($to_time - $from_time) / 60, 0);

                            $params['idel_time'] = $idel_time;
                        }
                    }

                    $this->output('frontend/chat/admin_new_chat', $params);

                } else
                {
                    $params = $this->reader->init($reader_username)->data;
                    $params['reader_data'] = $this->reader->init($reader_username)->data;
                    $params['time_balance'] = $this->member_funds->minute_balance() * 60 + $this->member_funds->free_minute_balance() * 60; // converted to seconds

                    if ($params['time_balance'] < 60) {
                        redirect("/chat/main/purchase_time/{$reader_username}");
                    } elseif ($this->session->userdata('chat_id')) {

                        $params['session_id'] = $this->session->userdata('chat_id');

                        $chatObject = $this->chatmodel->openChatRoom(array('session_id' => $params['session_id']))->object;
                        $params['chat_session_id'] = $chatObject['chat_session_id'];
                        $params['title'] = "Continue Previous Chat";
                        $this->output('chat/continue_chat', $params);
                    } else {
                        $params['title'] = "Confirm New Chat";
                        $params['detect'] = $this->detect;
                        $params['member_logged'] = $this->session->userdata('member_logged');

                        // update reader's last chat pending request
                        $this->reader->update_last_pending_request();


                        $reader = $this->reader->init($reader_username)->data;
                        $client_id = $this->session->userdata('member_logged');

                        $lastManualAbortChats = $this->chatmodel->getLastManualAbortChats($reader['member_id'], $client_id, 1);
                        $now = date('Y-m-d H:i:s');

                        if ($lastManualAbortChats) {

                            if (count($lastManualAbortChats) >= 1) {
                                // echo $lastManualAbortChats[0]['create_datetime'];
                                $to_time = strtotime($now);
                                $from_time = strtotime($lastManualAbortChats[0]['create_datetime']);
                                // echo '<br>';
                                $idel_time = round(abs($to_time - $from_time) / 60, 0);

                                $params['idel_time'] = $idel_time;
                            }

                        }

                        $this->output('frontend/chat/new_chat', $params);
                    }

                }
            } else
            {
                $this->session->set_userdata('redirect', "/chat/main/index/{$reader_username}");
                redirect("/popup/login");
            }
        } else
        {
            $this->error("You have not specified a reader to chat with");
        }
    }

    function confirm($readerId = null)
    {
        $topic = trim($this->input->post('topic'));
        $minutes = trim($this->input->post('minutes'));
        $user_time_balance = $this->member_funds->minute_balance();

        if (!is_numeric($minutes) || $minutes < 1)
        {

            $array = array();
            $array['error'] = "1";
            $array['message'] = "Please enter at least 1 minute into the chat length field";
        }

        //--- verify the user has sufficient minutes to proceed
        elseif ($minutes > $user_time_balance)
        {

            $array = array();
            $array['error'] = "1";
            $array['message'] = "You do not have enough minutes in your balance to start a chat for {$minutes} minutes";
        } elseif (!is_numeric($readerId) || !$topic)
        {

            $array = array();
            $array['error'] = "1";
            $array['message'] = "Please enter a chat topic";
        } else
        {
            if ($readerId != $this->session->userdata('member_logged'))
            {

                $array = array();
                $array['reader_id'] = $readerId;
                $array['client_id'] = $this->session->userdata('member_logged');
                $array['topic'] = $topic;
                $array['set_length'] = $minutes * 60;

                $chatObject = $this->chatmodel->openChatRoom($array)->object;
                $this->reader->init($array['reader_id'])->update_last_chat_request();

                $array = array();
                $array['error'] = "0";
                $array['redirect'] = "/chat/chatInterface/index/{$chatObject['id']}/{$chatObject['chat_session_id']}";
            } else
            {

                $array = array();
                $array['error'] = "1";
                $array['message'] = "You may not chat with yourself";
            }
        }
        echo json_encode($array);
    }

    function confirm_json()
    {
        $now = date("Y-m-d H:i:s");

        $final_result = array();

        $client_id = trim($this->input->post('client_id'));
        $client_id_hash = trim($this->input->post('client_id_hash'));
        $reader_id = trim($this->input->post('reader_id'));
        $topic = trim($this->input->post('topic'));
        $minutes = trim($this->input->post('minutes'));
        $free_minutes = trim($this->input->post('free_minutes'));
        $tier = $this->input->post("tier");
        $user_time_balance = $this->member_funds->minute_balance($client_id) + $this->member_funds->free_minute_balance($client_id);
        
        $client_url = $this->input->post("client_url");

        $member = $this->member->get_member_data($client_id);

        if($client_id == '1')
        {
            $reader = $this->member->get_member_data($reader_id);
            $final_result['status'] = true;
            $final_result['reader_id'] = $reader_id;
            $final_result['reader_username'] = $reader['username'];
            $final_result['client_id'] = $client_id;
            $final_result['client_username'] = $member['username'];
            $final_result['topic'] = $topic;
            $final_result['set_length'] = $minutes * 60;
            $final_result['tier'] = $tier;
            $final_result['total_free_length'] = $free_minutes;

            $chatObject = $this->chatmodel->openChatRoom($final_result)->object;

            $final_result['reader_hash'] = $this->chatmodel->generate_member_hash($reader_id, $chatObject['reader_seed'], $chatObject['chat_session_id']);
            $final_result['client_hash'] = $this->chatmodel->generate_member_hash($client_id, $chatObject['client_seed'], $chatObject['chat_session_id']);
            $final_result['chat_id'] = $chatObject['id'];
            $final_result['chat_session_id'] = $chatObject['chat_session_id'];
            $final_result['time_balance'] = $chatObject['set_length'] - $chatObject['length'];
            $final_result['max_chat_length'] = $chatObject['set_length'];
            $final_result['chat_length'] = $chatObject['length'];
            $final_result['redirect_url'] = "/chat/chatInterface/index/{$chatObject['id']}/{$chatObject['chat_session_id']}";
            $final_result['client_url'] = $client_url;

            echo json_encode($final_result);

        } else
        {
            if ($client_id_hash != $member['member_id_hash'])
            {
                $final_result['status'] = false;
                $final_result['message'] = "Invalid Login";
            } elseif (!is_numeric($minutes) || $minutes < 1)
            {

                $final_result['status'] = false;
                $final_result['message'] = "Please enter at least 1 minute into the chat length field";
            } elseif ($minutes > $user_time_balance)
            {
                //--- verify the user has sufficient minutes to proceed
                $final_result['error'] = false;
                $final_result['message'] = "You do not have enough minutes in your balance to start a chat for {$minutes} minutes";
            } elseif (!is_numeric($reader_id) || !$topic)
            {
                $final_result['status'] = false;
                $final_result['message'] = "Please enter a chat topic";
            } else
            {
                if ($member['member_type'] == 'client')
                {
                    $reader = $this->member->get_member_data($reader_id);


                    $final_result['data_trace'] = array();

                    //$lastManualAbortChats = $this->chatmodel->getLastManualAbortChats($reader_id, $client_id, MANUAL_QUIT_DISABLE_MAX_TIME);
                    //$old_chat_record = $lastManualAbortChats[MANUAL_QUIT_DISABLE_MAX_TIME - 1];

                    //$final_result['data_trace']['old_chat_record'] = $old_chat_record;

                    //$final_result['data_trace']['MANUAL_QUIT_DISABLE_MAX_TIME'] = MANUAL_QUIT_DISABLE_MAX_TIME;
                    //$final_result['data_trace']['MANUAL_QUIT_DISABLE_PERIOD'] = MANUAL_QUIT_DISABLE_PERIOD;

                    //$final_result['data_trace']['strtotime_now'] = strtotime($now);
                    //$final_result['data_trace']['strtotime_old_chat_record["create_datetime"]'] = strtotime($old_chat_record['create_datetime']);



                    if ($reader['status'] != "online")
                    {
                        $final_result['status'] = false;
                        $final_result['message'] = "Reader is not online";
                    } else
                    {
                        // check if it is the 4th time
                        $is_disable_chat = false;
                        $lastManualAbortChats = $this->chatmodel->getLastManualAbortChats($reader_id, $client_id, MANUAL_QUIT_DISABLE_MAX_TIME);
                        if ($lastManualAbortChats == false || empty($lastManualAbortChats) || count($lastManualAbortChats) < MANUAL_QUIT_DISABLE_MAX_TIME)
                        {
                            $is_disable_chat = false;
                        } else
                        {
                            $old_chat_record = $lastManualAbortChats[MANUAL_QUIT_DISABLE_MAX_TIME - 1];

                            //$final_result['data_trace']['attempt__________________'] = count($lastManualAbortChats);
                            //$final_result['data_trace']['attempt___strtotime_________'] = strtotime($now) - strtotime($old_chat_record['create_datetime']);

                            $time_trigger = strtotime($now) - strtotime($old_chat_record['create_datetime']);

                            if ( ($time_trigger>199) && ($time_trigger<MANUAL_QUIT_DISABLE_PERIOD)  ) {
                                $this->load->model('reader');
                                $this->reader->set_status('offline', true, $reader_id);
                            }



                            if (strtotime($now) - strtotime($old_chat_record['create_datetime']) < MANUAL_QUIT_DISABLE_PERIOD)
                            {
                                $is_disable_chat = true;
                                //$is_disable_chat = false;
                                //$final_result['data_trace']['attempt__is_disable_chat_:_false________________'] = count($lastManualAbortChats);

                            } else
                            {
                                $is_disable_chat = false;
                            }
                        }

                        if ($is_disable_chat)
                        {
                            $email_params = array();
                            $email_params['client_username'] = $member['username'];

                            $this->system_vars->omail($reader['email'], 'three_chat_quit', $email_params);
                            $this->system_vars->omail($this->settings['admin_email'], 'three_chat_quit', $email_params);
                            $this->system_vars->omail($member['email'], 'three_chat_quit', $email_params);

                            $this->load->model('reader');
                            $this->reader->set_status('online', true, $reader_id);

                            $final_result['status'] = false;
                            $final_result['is_max_manual_quit'] = true;
                        } else
                        {
                            $final_result['status'] = true;
                            $final_result['reader_id'] = $reader_id;
                            $final_result['reader_username'] = $reader['username'];
                            $final_result['client_id'] = $client_id;
                            $final_result['client_username'] = $member['username'];
                            $final_result['topic'] = $topic;
                            if($minutes > $this->member_funds->minute_balance($client_id)) {
                                $minutes = $this->member_funds->minute_balance($client_id);
                            }
                            $final_result['set_length'] = intval($minutes * 60);
                            $final_result['tier'] = $tier;
                            $final_result['total_free_length'] = $free_minutes;

                            $chatObject = $this->chatmodel->openChatRoom($final_result)->object;

                            $final_result['reader_hash'] = $this->chatmodel->generate_member_hash($reader_id, $chatObject['reader_seed'], $chatObject['chat_session_id']);
                            $final_result['client_hash'] = $this->chatmodel->generate_member_hash($client_id, $chatObject['client_seed'], $chatObject['chat_session_id']);
                            $final_result['chat_id'] = $chatObject['id'];
                            $final_result['chat_session_id'] = $chatObject['chat_session_id'];
                            $final_result['time_balance'] = $chatObject['set_length'] - $chatObject['length'];
                            $final_result['max_chat_length'] = $chatObject['set_length'];
                            $final_result['chat_length'] = $chatObject['length'];
                            $final_result['redirect_url'] = "/chat/chatInterface/index/{$chatObject['id']}/{$chatObject['chat_session_id']}";
                            $final_result['client_url'] = $client_url;
                        }
                    }
                } else
                {

                    $final_result['status'] = false;
                    $final_result['message'] = "You may not chat with yourself";
                }
            }
            echo json_encode($final_result);
        }


    }

    //--- Purcahse Time
    function purchase_time($reader_id, $chat_id = '')
    {

        $this->requireLogin();

        $params = array();
        $params['reader_id'] = $reader_id;
        if ($chat_id != '')
        {
            $params["chat_id"] = $chat_id;
        }

        $this->output('frontend/chat/purchase_time', $params);
    }

    // deprecated  use chatInterface.addStoredTime
    function add_stored_time($username)
    {
        $this->requireLogin();

        $params = array();
        $params['username'] = $username;
        $params['time_balance'] = $params['time_balance'] = ($this->member_funds->minute_balance() * 60);

        $this->output('chat/add_stored_time', $params);
    }

    // deprecated.  use chatInterface.addStoredTime
    function add_stored_time_submit($username)
    {
        $minutes = trim($this->input->post('minutes'));
        $user_time_balance = $this->member_funds->minute_balance();
        $chatid = $this->session->userdata('chat_id');

        if (!is_numeric($minutes) || $minutes < 1)
        {
            $array = array();
            $array['error'] = "1";
            $array['message'] = "Please enter at least 1 minute into the chat length field";
        }

        //--- verify the user has sufficient minutes to proceed
        elseif ($minutes > $user_time_balance)
        {
            $array = array();
            $array['error'] = "1";
            $array['message'] = "You do not have enough minutes in your balance to start a chat for {$minutes} minutes";
        } else
        {
            //--- Update the table with the minute balance
            $chatObject = $this->chatmodel->openChatRoom(array('session_id' => $chatid))->object;

            $update = array();
            $update['set_length'] = $chatObject['set_length'] + ($minutes * 60);

            $this->db->where('id', $chatid);
            $this->db->update('chats', $update);

            //--- Return
            $array = array();
            $array['error'] = "0";
            $array['redirect'] = "/chat/chatInterface/index/{$chatObject['id']}/{$chatObject['chat_session_id']}";
        }

        echo json_encode($array);
    }

    //Same as frontend/my_account/transactions/add_billing_profile -except for reader_id
    function add_billing_profile($merchant_type = null, $package_id = null, $reader_id = null, $chat_id=null)
    {
        if (!$merchant_type)
        {
            $this->session->set_flashdata('error', "You must select a package before you can attempt to add a new billing profile. Profiles are dependent on package pricing.");
            redirect("/frontend/chat/purchase_time");
        } else
        {
            if ($package_id)
            {
                $params['merchant_type'] = $merchant_type;

                $params['pinfo'] = $this->site->get_package($package_id);
                $params['user'] = $this->member->data;

                // Order Number
                $last_trans_number = $this->db->query("SELECT MAX(id) FROM transactions")->result();
                $last_trans_number = $last_trans_number[0];
                $last_trans_number = $last_trans_number->{'MAX(id)'} + 1;

                $params['order_number'] = date('Y').date("m").date("d").$last_trans_number;

                $this->session->set_userdata("package_id", $package_id);

                $this->requireLogin();

                // $params = $this->member->get_member_data($username);
                $params['reader_id'] = $reader_id;
                $params['chat_id'] = $chat_id;

                $this->output('frontend/chat/chat_add_billing_profile', $params);
            } else
            {
                $this->purchase_time($reader_id);
            }
        }
    }

    //--- Confirmation of purchased funds
    public function funds_confirmation($chat_id = null, $package_id = null, $transaction_id = null)
    {
        $t = array();
        $t['chatId'] = $chat_id;
        $t['package'] = $this->site->get_package($package_id);
        $t['transaction'] = $this->db->query("SELECT * FROM transactions WHERE id = {$transaction_id}")->row_array();

        // get the chat object
        $chatObject = $this->chatmodel->openChatRoom(array('session_id' => $chat_id))->object;
        $t['chat_session_id'] = $chatObject['chat_session_id'];
        $this->output('frontend/chat/funds_confirmation', $t);
    }

    public function get_readers_list()
    {
        $pban_ids = [];
        $this->db->where("type", "personal");
        $query = $this->db->get("member_bans");
        if ($query->num_rows() > 0)
        {
            foreach($query->result() as $row)
            {
                $pban_ids[] = $row->reader_id;
            }
        }
        
        $this->db->from("members a");
        $this->db->join("member_profiles b", "b.member_id=a.id", "LEFT");        
        $this->db->where("b.member_id is not null", NULL, false);
        if (count($pban_ids) > 0)
        {
            $this->db->where_not_in("a.id", $pban_ids);
        }
        $query = $this->db->get();
        
        $html = '';
        if ($query->num_rows() > 0)
        {
            $html = $this->load->view("frontend/chat/readers_list", ["readers" => $query->result()], true);
        }
        
        echo json_encode(["readers_list" => $html]);
    }
    
    public function check_register_client()
    {
        $username = $this->input->post("username");
        $email = $this->input->post("email");
        
        $this->db->where("username", $username);
        $this->db->where("email", $email);
        $query = $this->db->get("members");
        
        if ($query->num_rows() > 0)
        {
            $user = $query->row();
            // Check if banned user?
            if ($user->banned)
            {
                echo json_encode(["is_banned" => 1]);
                exit;
            }
            $member_hash = $this->member->generate_member_id_hash($user->id, $user->registration_date, 'client');
            $user->member_id_hash = $member_hash;
            
            echo json_encode($user);
            exit;
        }
        else
        {
            $data = [];
            $data["username"] = $username;
            $data["email"] = $email;            
            $data["password"] = $this->bcrypt->hash_password("123456");
            $this->db->insert("members",$data);
            $user_id = $this->db->insert_id();
            
            $this->db->where("id", $user_id);
            $query = $this->db->get("members");
            
            if ($query->num_rows() > 0)
            {
                $user = $query->row();
                $member_hash = $this->member->generate_member_id_hash($user->id, $user->registration_date, 'client');
                $user->member_id_hash = $member_hash;
                
                $data = [];
                $data["member_id"] = $user_id;
                $data["type"] = "reading";
                $data["tier"] = "regular";
                $data["region"] = "us";
                $data["total"] = 100;
                $data["balance"] = 100;                
                $this->db->insert("member_balance", $data);
                echo json_encode($user);
            }
        }
    }

    function get_reader_status($member_id = null,$is_online=false){
        die(json_encode($this->reader->get_member_status($member_id,$is_online)));
    }

    /*Thom ho*/
    function set_attempt_number() {

        
        $data = $_POST['data'];
        $_SESSION["num"] = $data["data"];
        $result["num"] = "success";
        echo json_encode($result);
    }

    function get_attempt_number() {

        $num = isset( $_SESSION["num"] ) ? $_SESSION["num"] : null;
        $data["num"] = $num;
        echo json_encode($data);
    }
    /*Thom ho*/

    /*9-2*/
    function get_latest_chat_info($client_id, $reader_id)
    {
         $getChatinfo = $this->db->query
                ("
                    SELECT
                        *
                    FROM chats
    
                    WHERE reader_id = '{$reader_id}' AND client_id = '{$client_id}'

                    ORDER BY id DESC
            
                    LIMIT 1
    
                ");


            $chat_info = $getChatinfo->row_array();

            $last_chat_time = $chat_info['start_datetime'];
            $topic = $chat_info['topic'];
            $time_used = $chat_info['length'];

            $return['last_chat_time'] = $last_chat_time;
            $return['topic'] = $topic;
            $return['time_used'] = $time_used;

            echo json_encode($return);
    }

    /*9-5*/
    function set_appointment()
    {
        $reader_id = $this->input->post("reader_id");
        $client_id = $this->input->post("client_id");
        $date = $this->input->post("date");
        $date = strtotime($date);
        $date = date ("Y-m-d H:i:s", $date);

        $getAppointment = $this->db->query
        ("
                SELECT
                    *
                FROM appointment

                WHERE reader_id = '{$reader_id}' and client_id = '{$client_id}'

        ");

        $data = $getAppointment->result_array();

        if(count($data) > 1)
        {
            if(strtotime("-1 week") > $data[count($data)-2]['time_attempted'])
            {

                $created_at = strtotime("now");

                $data = array(

                    'reader_id' => $reader_id,
                    'client_id' => $client_id,
                    'appointment_date' => $date,
                    'time_attempted' => $created_at

                );
                $this->db->insert('appointment', $data);  
            }
        } 
        else
        {

            $created_at = strtotime("now");

            $data = array(

                'reader_id' => $reader_id,
                'client_id' => $client_id,
                'appointment_date' => $date,
                'time_attempted' => $created_at

            );
            $this->db->insert('appointment', $data);
        }
    }

    function set_multi_appointment()
    {
        $reader_id_array = $this->input->post("reader_id");
        $client_id = $this->input->post("client_id");
        $date = $this->input->post("date");
        $date = strtotime($date);
        $date = date ("Y-m-d H:i:s", $date);

        foreach($reader_id_array as $reader_id)
        {
            $getAppointment = $this->db->query
            ("
                SELECT
                    *
                FROM appointment

                WHERE reader_id = '{$reader_id}' and client_id = '{$client_id}'

        ");

            $data = $getAppointment->result_array();

            if(count($data) > 1)
            {
                if(strtotime("-1 week") > $data[count($data)-2]['time_attempted'])
                {

                    $created_at = strtotime("now");

                    $data = array(

                        'reader_id' => $reader_id,
                        'client_id' => $client_id,
                        'appointment_date' => $date,
                        'time_attempted' => $created_at

                    );
                    $this->db->insert('appointment', $data);
                }
            }
            else
            {

                $created_at = strtotime("now");

                $data = array(

                    'reader_id' => $reader_id,
                    'client_id' => $client_id,
                    'appointment_date' => $date,
                    'time_attempted' => $created_at

                );
                $this->db->insert('appointment', $data);
            }
        }
    }
    function set_chat_request()
    {
        $reader_id_array = $this->input->post("reader_id");
        $client_id = $this->input->post("client_id");
        $topic = $this->input->post("topic");
        $time = $this->input->post("time_size");

        foreach($reader_id_array as $reader_id)
        {
            $data = array(
                'datetime' => date('Y-m-d H:i:s'),
                'sender_id' => $client_id,
                'ricipient_id' => $reader_id,
                'name' => 'chat_request',
                'subject' => 'Chat Request',
                'message' => 'I want to chat with in next ' . $time . 'minutes with topic ' . $topic,
                'read' => 0,
                'type' => NULL
            );

            $this->db->insert('messages', $data);
        }
    }
    /*9-4*/
    function get_appointment($reader_id = null)
    {
        $getAppointment = $this->db->query
        ("
                SELECT
                    *
                FROM appointment

                WHERE reader_id = '{$reader_id}' and checked = 0

        ");

        $appointments = [];

        $data = $getAppointment->result_array();

        foreach($data as $item)
        {
            $this->db->where('id', $item['id']);
            $this->db->update('appointment', ["checked" => 1]);
        }

        foreach($data as $item) 
        {

            $reader_id = $item["reader_id"];
            $client_id = $item["client_id"];

            $reader_array = $this->db->query("SELECT * FROM members WHERE id = '{$reader_id}'")->row_array();
            $client_array = $this->db->query("SELECT * FROM members WHERE id = '{$client_id}'")->row_array();

            $user_array = array("reader_username"=>$reader_array["username"], "reader_firstname"=>$reader_array["first_name"], "reader_lastname"=>$reader_array["last_name"], "client_username"=>$client_array["username"], "client_firstname"=>$client_array["first_name"], "client_lastname"=>$client_array["last_name"]);

            $new_array = [];

            foreach ($item as $key => $value) {
                
                $new_array[$key] = $value;
            }

            foreach ($user_array as $key => $value) {
                
                $new_array[$key] = $value;
            }
        
            array_push($appointments, $new_array);
        }

        $return['appointments'] = $appointments;

        echo json_encode($return);

    }
    /*9-5*/
    function set_accept($appointment_id)
    {
        $this->db->where('id', $appointment_id);
        $this->db->update('appointment', ['confirm' => 1, 'decline' => 0, 'decline_checked' => 0]);

        $return['appointment_id'] = $appointment_id;
        $return['status'] = "success";

        echo json_encode($return);
    }

    function set_decline($appointment_id)
    {
        $this->db->where('id', $appointment_id);
        $this->db->update('appointment', ['decline' => 1, 'confirm' => 0, 'checked' => 0]);

        $return['appointment_id'] = $appointment_id;
        $return['status'] = "success";

        echo json_encode($return);
    }

    function delete_appointment($appointment_id)
    {
        $this->db->where('id', $appointment_id);
        $this->db->delete('appointment');

        $return['appointment_id'] = $appointment_id;
        $return['status'] = "removed";

        echo json_encode($return);
    }

    function get_admin_appointment()
    {
        $getAppointment = $this->db->query
        ("
                SELECT
                    *
                FROM appointment

                WHERE  admin_checked = 0

        ");

        $appointments = [];

        $data = $getAppointment->result_array();

        foreach($data as $item)
        {
            $this->db->where('id', $item['id']);
            $this->db->update('appointment', ["admin_checked" => 1]);
        }

        foreach($data as $item) 
        {

            $reader_id = $item["reader_id"];
            $client_id = $item["client_id"];

            $reader_array = $this->db->query("SELECT * FROM members WHERE id = '{$reader_id}'")->row_array();
            $client_array = $this->db->query("SELECT * FROM members WHERE id = '{$client_id}'")->row_array();

            $user_array = array("reader_username"=>$reader_array["username"], "reader_firstname"=>$reader_array["first_name"], "reader_lastname"=>$reader_array["last_name"], "client_username"=>$client_array["username"], "client_firstname"=>$client_array["first_name"], "client_lastname"=>$client_array["last_name"]);

            $new_array = [];

            foreach ($item as $key => $value) {
                
                $new_array[$key] = $value;
            }

            foreach ($user_array as $key => $value) {
                
                $new_array[$key] = $value;
            }
        
            array_push($appointments, $new_array);
        }

        $return['appointments'] = $appointments;

        echo json_encode($return);
    }

    function get_decline($client_id)
    {
        $getAppointment = $this->db->query
        ("
                SELECT
                    *
                FROM appointment

                WHERE client_id = '{$client_id}' and decline_checked = 0

                and decline = 1

        ");

        $appointments = [];

        $data = $getAppointment->result_array();

        foreach($data as $item)
        {
            $this->db->where('id', $item['id']);
            $this->db->update('appointment', ["decline_checked" => 1]);
        }

        foreach($data as $item) 
        {

            $reader_id = $item["reader_id"];
            $client_id = $item["client_id"];

            $reader_array = $this->db->query("SELECT * FROM members WHERE id = '{$reader_id}'")->row_array();
            $client_array = $this->db->query("SELECT * FROM members WHERE id = '{$client_id}'")->row_array();

            $user_array = array("reader_username"=>$reader_array["username"], "reader_firstname"=>$reader_array["first_name"], "reader_lastname"=>$reader_array["last_name"], "client_username"=>$client_array["username"], "client_firstname"=>$client_array["first_name"], "client_lastname"=>$client_array["last_name"]);

            $new_array = [];

            foreach ($item as $key => $value) {
                
                $new_array[$key] = $value;
            }

            foreach ($user_array as $key => $value) {
                
                $new_array[$key] = $value;
            }

            $new_array['appointment_date'] = date('m/d/Y H:i', strtotime($new_array['appointment_date']));

            array_push($appointments, $new_array);
        }

        $return['appointments'] = $appointments;
        echo json_encode($return);
    }

    function get_accept($client_id)
    {
        $getAppointment = $this->db->query
        ("
                SELECT
                    *
                FROM appointment

                WHERE client_id = '{$client_id}' and checked = 0

                and confirm = 1

        ");

        $appointments = [];

        $data = $getAppointment->result_array();

        foreach($data as $item)
        {
            $this->db->where('id', $item['id']);
            $this->db->update('appointment', ["checked" => 1]);
        }

        foreach($data as $item)
        {

            $reader_id = $item["reader_id"];
            $client_id = $item["client_id"];

            $reader_array = $this->db->query("SELECT * FROM members WHERE id = '{$reader_id}'")->row_array();
            $client_array = $this->db->query("SELECT * FROM members WHERE id = '{$client_id}'")->row_array();

            $user_array = array("reader_username"=>$reader_array["username"], "reader_firstname"=>$reader_array["first_name"], "reader_lastname"=>$reader_array["last_name"], "client_username"=>$client_array["username"], "client_firstname"=>$client_array["first_name"], "client_lastname"=>$client_array["last_name"]);

            $new_array = [];

            foreach ($item as $key => $value) {

                $new_array[$key] = $value;
            }

            foreach ($user_array as $key => $value) {

                $new_array[$key] = $value;
            }

            $new_array['appointment_date'] = date('m/d/Y H:i', strtotime($new_array['appointment_date']));

            array_push($appointments, $new_array);
        }

        $return['appointments'] = $appointments;
        echo json_encode($return);
    }

    function get_admin_decline()
    {
        $getAppointment = $this->db->query
        ("
                SELECT
                    *
                FROM appointment

                WHERE  admin_decline_checked = 0 and decline = 1

        ");

        $appointments = [];

        $data = $getAppointment->result_array();

        foreach($data as $item)
        {
            $this->db->where('id', $item['id']);
            $this->db->update('appointment', ["admin_decline_checked" => 1]);
        }

        foreach($data as $item) 
        {

            $reader_id = $item["reader_id"];
            $client_id = $item["client_id"];

            $reader_array = $this->db->query("SELECT * FROM members WHERE id = '{$reader_id}'")->row_array();
            $client_array = $this->db->query("SELECT * FROM members WHERE id = '{$client_id}'")->row_array();

            $user_array = array("reader_username"=>$reader_array["username"], "reader_firstname"=>$reader_array["first_name"], "reader_lastname"=>$reader_array["last_name"], "client_username"=>$client_array["username"], "client_firstname"=>$client_array["first_name"], "client_lastname"=>$client_array["last_name"]);

            $new_array = [];

            foreach ($item as $key => $value) {
                
                $new_array[$key] = $value;
            }

            foreach ($user_array as $key => $value) {
                
                $new_array[$key] = $value;
            }
        
            array_push($appointments, $new_array);
        }

        $return['appointments'] = $appointments;
        echo json_encode($return);
    }

    /*9-14*/
    function get_gift_info($client_id)
    {
        $gift_data_array = [];
        $giveback_array = $this->db->query("SELECT * FROM reader_giveback WHERE client_id = '{$client_id}' AND status = 0")->result_array();

        foreach ($giveback_array as $giveback) {
            $reader = $this->db->query("SELECT * FROM members WHERE id = '{$giveback['reader_id']}'")->row_array();
            $gift_item_array['reader_id'] = $giveback['reader_id'];
            $gift_item_array['week_day'] = $giveback['day_of_week'];
            $gift_item_array['gift_size'] = $giveback['gift_size'];
            $gift_item_array['reader_name'] = $reader['username'];

            array_push($gift_data_array, $gift_item_array);
        }

        $return['data'] = $gift_data_array;
        echo json_encode($return);
    }

    /*9-20*/
    function wait_in_line($reader_username = null, $reader_id = null, $client_id = null)
    {
        $reader = $this->reader->init($reader_username)->data;

        if ($reader_username) {
            if ($this->session->userdata('member_logged')) {

                $params = $this->reader->init($reader_username)->data;
                $params['reader_data'] = $this->reader->init($reader_username)->data;
                $params['time_balance'] = ($this->member_funds->minute_balance() * 60); // converted to seconds

                if ($params['time_balance'] < 60) {
                    redirect("/chat/main/purchase_time/{$reader_username}");
                } elseif ($this->session->userdata('chat_id')) {

                    $params['session_id'] = $this->session->userdata('chat_id');

                    $chatObject = $this->chatmodel->openChatRoom(array('session_id' => $params['session_id']))->object;
                    $params['chat_session_id'] = $chatObject['chat_session_id'];
                    $params['title'] = "Continue Previous Chat";
                    $this->output('chat/continue_chat', $params);
                } else {

                    /*save in DB*/
                    $data = array(
                        "reader_id" => $reader_id,
                        "client_id" => $client_id,
                        "created_at" => date("Y-m-d H:i:s")
                    );

                    $this->db->insert('wait_in_line', $data);

                    $params['title'] = "Confirm New Chat";
                    $params['detect'] = $this->detect;
                    $params['member_logged'] = $this->session->userdata('member_logged');

                    // update reader's last chat pending request
                    $this->reader->update_last_pending_request();


                    $reader = $this->reader->init($reader_username)->data;
                    $client_id = $this->session->userdata('member_logged');

                    $lastManualAbortChats = $this->chatmodel->getLastManualAbortChats($reader['member_id'], $client_id, 1);
                    $now = date('Y-m-d H:i:s');

                    if ($lastManualAbortChats) {

                        if (count($lastManualAbortChats) >= 1) {
                            // echo $lastManualAbortChats[0]['create_datetime'];
                            $to_time = strtotime($now);
                            $from_time = strtotime($lastManualAbortChats[0]['create_datetime']);
                            // echo '<br>';
                            $idel_time = round(abs($to_time - $from_time) / 60, 0);

                            $params['idel_time'] = $idel_time;
                        }

                    }
                    $params['reader_id'] = $reader['member_id'];
                    $params['client_id'] = $client_id;

                    $reader_chat_session_info = $this->db->query("SELECT * FROM chats WHERE reader_id = '{$reader_id}' AND ended = 0 ORDER BY id  DESC")->row_array();
                    $reader_chat_start_time = $reader_chat_session_info['create_datetime'];
                    $params['wait_in_line_time'] = $reader_chat_session_info['set_length'] - (strtotime("now") - strtotime($reader_chat_start_time));

                    $this->output('frontend/chat/new_chat_wait_in_line', $params);
                }

            } else {
                $this->session->set_userdata('redirect', "/chat/main/index/{$reader_username}");
                redirect("/popup/login");
            }
        } else {
            $this->error("You have not specified a reader to chat with");
        }
    }

    function get_wait_in_line_clients($reader_id = null)
    {
        $getWaitInLineClients = $this->db->query
        ("
                SELECT
                    *
                FROM wait_in_line

                WHERE reader_id = '{$reader_id}' and ended = 0

        ");

        $wait_in_line_clients = [];

        $data = $getWaitInLineClients->result_array();

        foreach($data as $item)
        {

            $reader_id = $item["reader_id"];
            $client_id = $item["client_id"];

            $reader_array = $this->db->query("SELECT * FROM members WHERE id = '{$reader_id}'")->row_array();
            $client_array = $this->db->query("SELECT * FROM members WHERE id = '{$client_id}'")->row_array();

            $user_array = array("reader_username" => $reader_array["username"],
                "reader_first_name" => $reader_array["first_name"],
                "reader_last_name" => $reader_array["last_name"],
                "client_user_name" => $client_array["username"],
                "client_first_name" => $client_array["first_name"],
                "client_last_name" => $client_array["last_name"]
            );

            $new_array = [];

            foreach ($item as $key => $value) {

                $new_array[$key] = $value;
            }

            foreach ($user_array as $key => $value) {

                $new_array[$key] = $value;
            }

            array_push($wait_in_line_clients, $new_array);
        }

        $return['wait_in_line_clients'] = $wait_in_line_clients;

        echo json_encode($return);

    }

    function cancel_wait_in_line($client_id = null, $reader_id = null)
    {
        $this->db->where('reader_id', $reader_id);
        $this->db->where('client_id', $client_id);

        $this->db->update('wait_in_line', ["ended" => 1]);

        $return['status'] = 'success';
        echo json_encode($return);
    }

    function get_wait_in_line_sequence($client_id, $reader_id)
    {
        $sequence = 1;
        $clients = $this->db->query("SELECT * FROM wait_in_line WHERE reader_id = '{$reader_id}' AND ended = 0")->result_array();

        foreach ($clients as $client) {
            if ($client['client_id'] != $client_id) {
                $sequence = $sequence + 1;
            } else {
                break;
            }
        }
        $return['sequence'] = $sequence;
        echo json_encode($return);
    }

    function add_to_giveback($client_id = null)
    {
        // -----check if gift for new group exists
        $gifts_to_new_client = $this->db->query("SELECT * FROM reader_giveback WHERE (group_type = 1 AND status = 0) OR (group_type = 4 AND status = 0)")->result_array();

        // -----check if this client is registered client
        $gifts_to_current_client = $this->db->query("SELECT * FROM reader_giveback WHERE client_id = '{$client_id}'")->result_array();

        if(count($gifts_to_new_client) > 0 && count($gifts_to_current_client) == 0)
        {
            $group_id = -1;
            foreach($gifts_to_new_client as $gift)
            {
                if($group_id != $gift['group_id'])
                {
                    $data = array(
                        'reader_id' => $gift['reader_id'],
                        'client_id' => $client_id,
                        'gift_size' => $gift['gift_size'],
                        'day_of_week' => $gift['day_of_week'],
                        'group_id' => $gift['group_id'],
                        'group_type' => $gift['group_type'],
                        'created_at' => $gift['created_at']
                    );
                    $this->db->insert('reader_giveback', $data);

                    $group_id = $gift['group_id'];
                }
            }
        }
    }

    function get_chat_transcript()
    {
        $chat_id = $this->input->post('chat_id');

        $getTranscripts = $this->db->query("SELECT chat_transcripts.*, members.username FROM chat_transcripts LEFT JOIN members ON members.id = chat_transcripts.member_id WHERE chat_id = {$chat_id} ");

        $getTranscripts = $getTranscripts->result_array();

        foreach($getTranscripts as $key => $chat)
        {
            if(strpos($chat['message'], 'system') !== false)
            {
                unset($getTranscripts[$key]);
            }
        }

        $getTranscripts = array_values($getTranscripts);

        $return['data'] = $getTranscripts;

        echo json_encode($return);

    }

}




