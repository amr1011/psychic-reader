<?php

require_once(APPPATH . "/controllers/Popup.php");

class chatInterface extends popup
{

    function __construct()
    {

        parent :: __construct();

        $this->load->model("chatmodel");
        $this->load->model("member");
        $this->load->model("warning_model");
        $this->settings = $this->system_vars->get_settings();
        //$this->requireLogin();
    }

    //--- Load Main Chat Window
    function index($chatId = null, $chatSessionId = null)
    {
        if (empty($chatId) || empty($chatSessionId))
        {
            $this->output('frontend/chat/chat_error_page.php');
            return;
        }

        $this->requireLogin();
        //--- IF continuing a previous chat
        //--- AND IF the logged member IS the client
        //--- Then set the termination of the chat back to 0

        if ($this->session->userdata('chat_id'))
        {
            $this->session->unset_userdata('chat_id');
            $this->db->where('id', $chatId);
            $this->db->update('chats', array('ended' => 0));
        }

        //--- Set the chat session accordingly
        $chatObject = $this->chatmodel->setSession($chatId);
        if ($chatObject == false)
        {
            $this->error("The chat session you requested has been expired.  Please retry. ");
            return;
        }

        $params = $chatObject->object;

        // validate chat session id
        if ($chatSessionId != $params['chat_session_id'])
        {
            $this->error("This chat has been terminated. The chat id is not valid.");
            return;
        }

        if (!$params['ended'])
        {
            $this->hideLogo = true;

            $params['reader'] = $this->member->get_member_data($params['reader_id']);
            $params['client'] = $this->member->get_member_data($params['client_id']);

            //--- Store the member info depending if the member is the reader or client (to avoid another DB request)
            if ($params['reader_id'] == $this->member->data['id'])
            {
                $params['title'] = "Chat with " . $params['client']['username'];
                $params['member'] = $params['reader'];
                $params['member_type'] = 'reader';
                $params['member_hash'] = $this->chatmodel->generate_member_hash($params['reader_id'], $params['reader_seed'], $params['chat_session_id']);
            } else
            {
                $params['title'] = "Chat with " . $params['reader']['username'];
                $params['member'] = $params['client'];
                $params['member_type'] = 'client';
                $params['member_hash'] = $this->chatmodel->generate_member_hash($params['client_id'], $params['client_seed'], $params['chat_session_id']);
            }

            //--- Check for chat transcripts
            //--- IF exist, load them into the interface
            $params['transcripts'] = $chatObject->loadTranscripts();
            $params['time_balance'] = $params['set_length'] - $params['length'];
            $params['max_chat_length'] = $params['set_length'];
            $params['chat_length'] = $params['length'];            

            if ($params['time_balance'] < 0)
            {
                $params['time_balance'] = 0;
            }
            //--- Set reader status to BUSY if reader logged in.
            if ($this->session->userdata('member_logged') == $params['reader_id'])
            {
                $this->load->model('reader');
                $this->reader->init($params['reader_id'])->set_status('busy');
            } else
            {
                //$this->reader->init($params['reader_id'])->update_last_chat_request();
            }
            //--- Load Chat Interface
            $params['detect'] = $this->detect;
            $params['readers'] = $this->readers->getAll(); //david_v3
            
            
            $settings = $this->system_vars->get_settings();
            
            $params["client_time_balance"] = gmdate("H:i:s", $this->member_funds->minute_balance($params["client_id"]) * 60 );
            $params['client_free_time_balance'] = gmdate("H:i:s", $this->member_funds->free_minute_balance($params['client_id']) * 60);
            $params["client_allow_freetime"] = $this->member_funds->is_allow_freetime($params["client_id"], $params["reader_id"], $settings["client_free_mins_max_reader_per_month"], $settings["client_max_free_mins_per_reader"]);
            $params["client_max_free_mins_per_reader"] = $settings["client_max_free_mins_per_reader"];

            // $params['tier'] = $this->member_funds->free_minute_balance($params['client_id']) > 0 ? 'free' : 'paid';

            $params['client_total_balance'] =intval($this->member_funds->minute_balance($params['client_id']) * 60);
            $params['client_total_free_balance'] = ($this->member_funds->free_minute_balance($params['client_id']));
            $this->output('frontend/chat/interface', $params);
        } else
        {

            $this->error("This chat has been terminated. You don't have access to open a terminated chat, but you can always start a new chat.");
        }
    }

    //david_v2
    function video_chat($chatId = null, $chatSessionId = null)
    {
        if (empty($chatId) || empty($chatSessionId))
        {
            $this->output('frontend/chat/chat_error_page.php');
            return;
        }

        $this->requireLogin();
        //--- IF continuing a previous chat
        //--- AND IF the logged member IS the client
        //--- Then set the termination of the chat back to 0

        if ($this->session->userdata('chat_id'))
        {

            $this->session->unset_userdata('chat_id');

            $this->db->where('id', $chatId);
            $this->db->update('chats', array('ended' => 0));
        }

        //--- Set the chat session accordingly
        $chatObject = $this->chatmodel->setSession($chatId);
        if ($chatObject == false)
        {
            $this->error("The chat session you requested has been expired.  Please retry. ");
            return;
        }

        $params = $chatObject->object;

        // validate chat session id
        if ($chatSessionId != $params['chat_session_id'])
        {
            $this->error("This chat has been terminated. The chat id is not valid.");
            return;
        }

        if (!$params['ended'])
        {
            $this->hideLogo = true;

            $params['reader'] = $this->member->get_member_data($params['reader_id']);
            $params['client'] = $this->member->get_member_data($params['client_id']);

            //--- Store the member info depending if the member is the reader or client (to avoid another DB request)
            if ($params['reader_id'] == $this->member->data['id'])
            {
                $params['title'] = "Chat with " . $params['client']['username'];
                $params['member'] = $params['reader'];
                $params['member_type'] = 'reader';
                $params['member_hash'] = $this->chatmodel->generate_member_hash($params['reader_id'], $params['reader_seed'], $params['chat_session_id']);
            } else
            {
                $params['title'] = "Chat with " . $params['reader']['username'];
                $params['member'] = $params['client'];
                $params['member_type'] = 'client';
                $params['member_hash'] = $this->chatmodel->generate_member_hash($params['client_id'], $params['client_seed'], $params['chat_session_id']);
            }

            //--- Check for chat transcripts
            //--- IF exist, load them into the interface
            $params['transcripts'] = $chatObject->loadTranscripts();
            $params['time_balance'] = $params['set_length'] - $params['length'];
            $params['max_chat_length'] = $params['set_length'];
            $params['chat_length'] = $params['length'];

            if ($params['time_balance'] < 0)
            {
                $params['time_balance'] = 0;
            }
            //--- Set reader status to BUSY if reader logged in.
            if ($this->session->userdata('member_logged') == $params['reader_id'])
            {
                $this->load->model('reader');
                $this->reader->init($params['reader_id'])->set_status('busy');
            } else
            {
                //$this->reader->init($params['reader_id'])->update_last_chat_request();
            }
            //--- Load Chat Interface
            $params['detect'] = $this->detect;
            $this->load->view('frontend/chat/video_chat', $params);
        } else
        {

            $this->error("This chat has been terminated. You don't have access to open a terminated chat, but you can always start a new chat.");
        }
    }

    function test()
    {
        echo "Ok";
    }

    function set_annonymous($member_id = null)
    {

        $this->member->set_annonymous($member_id);
        redirect($_SERVER['HTTP_REFERER']);

    }

    function test_error()
    {
        $this->error("This chat has been terminated. Yo");
    }

    // it should be off when in production
    function create_dummy_chat_session()
    {
        $passphrase = $this->input->post('pass');
        $reader_id = $this->input->post('reader_id');
        $client_id = $this->input->post('client_id');

        $final_result = array();
        if ($passphrase != 'Brunch')
        {
            $final_result = array('status' => false, "pass" => $passphrase, "reader_id" => $reader_id);
            echo json_encode($final_result);
            return;
        }

        $param = array('reader_id' => $reader_id, 'client_id' => $client_id, 'set_length' => 100, 'topic' => 'dummy test');

        $result = $this->chatmodel->openChatRoom($param);
        $final_result = $result->object;
        $final_result['member_hash'] = $this->chatmodel->generate_member_hash($client_id, $final_result['client_seed'], $final_result['chat_session_id']);
        $final_result['client_hash'] = $this->chatmodel->generate_member_hash($client_id, $final_result['client_seed'], $final_result['chat_session_id']);
        $final_result['reader_hash'] = $this->chatmodel->generate_member_hash($reader_id, $final_result['reader_seed'], $final_result['chat_session_id']);
        $reader = $this->member->get_member_data($reader_id);
        $final_result['reader_id_hash'] = $reader['member_id_hash'];
        $client = $this->member->get_member_data($client_id);
        $final_result['client_id_hash'] = $client['member_id_hash'];
        
        echo json_encode($final_result);        
    }

    // post method
    function validate()
    {
        $chat_id = $this->input->post('chat_id');
        $chat_session_id = $this->input->post('chat_session_id');
        $member_hash = $this->input->post('member_hash');

        $result = $this->chatmodel->validateChatSession($chat_id, $chat_session_id, $member_hash);

        $final_result = array();
        if ($result === false)
        {
            $final_result = array('status' => false);
        } else
        {
            $final_result = $result;
            $final_result['status'] = true;

            // it is good.. then find out member username;
            $reader = $this->member->get_member_data($result['reader_id']);
            if (!is_null($reader))
            {
                $final_result['reader_username'] = $reader['username'];
            } else
            {
                $final_result['reader_username'] = "Unavailable";
            }
            $client = $this->member->get_member_data($result['client_id']);
            if (!is_null($reader))
            {
                $final_result['client_username'] = $client['username'];
            } else
            {
                $final_result['client_username'] = "Unavailable";
            }

            if ($result['reader_id'] == $result['member_id'])
            {
                $final_result['member_username'] = $final_result['reader_username'];
            } else if ($result['client_id'] == $result['member_id'])
            {
                $final_result['member_username'] = $final_result['client_username'];
            }
        }
        echo json_encode($final_result);
    }

    function validate_member()
    {
        $member_id = $this->input->post('member_id');
        $member_id_hash = $this->input->post('member_id_hash');

        $result = $this->member->get_member_data($member_id);
        $final_result = array();
        if ($result == false)
        {
            $final_result['status'] = false;
            $final_result['reason'] = 'ID not exist';
        } else
        {
            if ($result['member_id_hash'] == $member_id_hash)
            {
                $final_result['status'] = true;
                $final_result['member_type'] = $result['member_type'];
            } else
            {
                $final_result['status'] = false;
                $final_result['reason'] = 'Unable to validate the ID';
            }
        }
        echo json_encode($final_result);
    }

    function check_time_balance()
    {
        $chat_session_id = $this->input->post('chat_session_id');

        if ($object = $this->chatmodel->openActiveChatRoomByChatSession(array('chat_session_id' => $chat_session_id)))
        {
            $final_result['status'] = true;
            $final_result['time_balance'] = $object->object['set_length'] - $object->object['length'];
            if ($final_result['time_balance'] < 0)
            {
                $final_result['time_balance'] = 0;
            }
            $final_result['max_chat_length'] = $object->object['set_length'];
            $final_result['chat_length'] = $object->object['length'];
            $final_result['start_time'] = $object->object['start_datetime'];
            echo json_encode($final_result);
        } else
        {
            $final_result['status'] = false;
            $final_result['reason'] = "Unable to start chat. Chat session not found $chat_session_id";
            echo json_encode($final_result);
        }
    }

    //--- start billing
    function start_chat()    // previous $session_id = null
    {
        $chat_session_id = $this->input->post('chat_session_id');

        if ($object = $this->chatmodel->openActiveChatRoomByChatSession(array('chat_session_id' => $chat_session_id)))
        {
            $final_result['status'] = true;

            $now = date('Y-m-d H:i:s');
            $final_result['object'] = $object;
            //echo "what is... object " . json_encode($object) . " <br>";

            if (empty($object->object['start_datetime']))
            {
                // not started yet, so start now
                $final_result['start_time'] = $object->startChat();
                $final_result['started_already'] = false;
            } else
            {
                $final_result['start_time'] = $object->object['start_datetime'];
                $final_result['started_already'] = true;
            }

            $final_result['time_balance'] = $object->object['set_length'] - $object->object['length'];
            if ($final_result['time_balance'] < 0)
            {
                $final_result['time_balance'] = 0;
            }
            $final_result['max_chat_length'] = $object->object['set_length'];
            $final_result['chat_length'] = $object->object['length'];

            echo json_encode($final_result);
        } else
        {
            $final_result['status'] = false;
            $final_result['reason'] = "Unable to start chat. Chat session not found $chat_session_id";
            echo json_encode($final_result);
        }
    }

    //--- Record Chat Length
    // previous signature: $session_id = null, $timer = 0
    function record_chat()
    {
        $chat_session_id = $this->input->post('chat_session_id');
        $timer = $this->input->post('timer');

        if ($object = $this->chatmodel->openActiveChatRoomByChatSession(array('chat_session_id' => $chat_session_id)))
        {
            $object->recordLength($timer);

            $final_result["chat_max_length"] = $object->object["set_length"];
            //$final_result["time_balance"] = -1;
            $final_result['status'] = true;
            echo json_encode($final_result);
        } else
        {
            $final_result['status'] = false;
            $final_result['reason'] = "Unable to record chat. Chat session not found $chat_session_id";
            echo json_encode($final_result);
        }
    }
    
    //--- Update Free Timer
    function update_free_timer()      // previous signature: $session_id = null, $timer = 0
    {
        $chat_session_id = $this->input->post('chat_session_id');
        $timer = $this->input->post('timer');

        if ($object = $this->chatmodel->openActiveChatRoomByChatSession(array('chat_session_id' => $chat_session_id)))
        {
            $tmp = $object->recordFreeLength($timer);

            $final_result["total_free_length"] = $tmp["total_free_length"];
            $final_result["free_length"] = $tmp["free_length"];
            $final_result["free_time_balance"] = $tmp["total_free_length"] - $tmp["free_length"];
            $final_result["time_balance"] = $tmp["set_length"] - $tmp["length"];
            $final_result['status'] = true;
            
            echo json_encode($final_result);
        } else
        {
            $final_result['status'] = false;
            $final_result['reason'] = "Unable to record chat. Chat session not found $chat_session_id";
            echo json_encode($final_result);
        }
    }

    //--- Record Chat Messages
    function log_message()    // previous $session_id = null
    {
        $chat_session_id = $this->input->post('chat_session_id');
        $message = $this->input->post('message');
        $member_id = $this->input->post('member_id');

        if ($object = $this->chatmodel->openActiveChatRoomByChatSession(array('chat_session_id' => $chat_session_id)))
        {
            $object->logMessage($member_id, $message);

            $final_result['status'] = true;
            echo json_encode($final_result);
        } else
        {
            $final_result['status'] = false;
            $final_result['reason'] = "Unable to log message. Chat session not found $chat_session_id";
            echo json_encode($final_result);
        }
    }

    //--- Refund Chat
    function refund_chat()    // previous $session_id = null
    {
        $chat_session_id = $this->input->post('chat_session_id');

        if ($object = $this->chatmodel->openActiveChatRoomByChatSession(array('chat_session_id' => $chat_session_id)))
        {
            $final_result['status'] = true;
            $final_result['refund'] = $object->refundChat();
            echo json_encode($final_result);
        } else
        {
            $final_result['status'] = false;
            $final_result['reason'] = "Unable to refund. Chat session not found $chat_session_id";
            echo json_encode($final_result);
        }
    }

    function banUser()  // previous $reader_id,$member_id,$type
    {
        $reader_id = $this->input->post('reader_id');
        $member_id = $this->input->post('member_id');
        $type = $this->input->post('type');
        $this->chatmodel->banUser($reader_id, $member_id, $type);

        $final_result['status'] = true;
        echo json_encode($final_result);
    }

    function abort_chat()
    {
        exit;
        $now = date("Y-m-d H:i:s");

        $chat_session_id = $this->input->post('room_name'); // chat_session_id
        $member_hash = $this->input->post('member_hash');
        $aborted_by = $this->input->post('aborted_by');
        $timer = $this->input->post('timer');
        $type = $this->input->post('type');

        $result = $this->chatmodel->validateChatSession(null, $chat_session_id, $member_hash, $aborted_by);

        $final_result = array();
        $final_result['abort_to_logout'] = false;
        if ($result === false)
        {
            $final_result['status'] = false;
            $final_result['reason'] = "Unable to validate chat session with $chat_session_id, $member_hash and $aborted_by";
        } else
        {
            if ($object = $this->chatmodel->openActiveChatRoomByChatSession(array('chat_session_id' => $chat_session_id)))
            {
                $post_reader_status = "online";
                $this->load->model('reader');
                $reader = $this->reader->init($object->object['reader_id']);
                $client = $this->member->get_member_data($object->object['client_id']);

                if ($type == "auto")
                {
                    $final_result['type'] = "auto";
                    if (strtotime($now) - strtotime($object->object['create_datetime']) >= CHAT_MAX_WAIT)
                    {
                        $last_abort = $this->chatmodel->getLastAutoAbortChat($object->object['reader_id'], $object->object['client_id']);
                        $final_result['last_abort_time'] = $last_abort['create_datetime'];
                        $final_result['time_diff'] = strtotime($now) - strtotime($last_abort['create_datetime']);
                        if ($last_abort !== false && strtotime($now) - strtotime($last_abort['create_datetime']) <= ABORT_TIME_DIFF)
                        {
                            // if abort within 2 mins, then set reader to offline
                            $post_reader_status = "offline";
                            $final_result['abort_to_logout'] = true;
                            // send email
                            $params['client_username'] = $client['username'];
                            $params['reader_username'] = $reader->data['username'];
                            $params['topic'] = $object->object['topic'];

                            $this->system_vars->omail($reader->data['email'], 'reader_auto_abort_offline', $params);
                            $this->system_vars->omail($this->settings['admin_email'], 'reader_auto_abort_offline', $params);
                            //$this->system_vars->omail("murvinlai@gmail.com",'reader_auto_abort_offline',$params);
                        } else
                        {
                            $final_result['abort_to'] = $post_reader_status;
                            // send email
                            $params['client_username'] = $client['username'];
                            $params['reader_username'] = $reader->data['username'];
                            $params['topic'] = $object->object['topic'];

                            $this->system_vars->omail($reader->data['email'], 'reader_auto_abort', $params);
                            //$this->system_vars->omail("murvinlai@gmail.com",'reader_auto_abort',$params);
                        }
                        $reader->update_last_abort_time();

                        try {
                            if ($this->chatmodel->setSession($result['id'])->abortChat($post_reader_status, true))
                            {
                                // get the final time. 
                                $final_result['status'] = true;
                                $final_result['reason'] = 'Abort Chat success';
                            } else
                            {
                                $final_result['status'] = false;
                                $final_result['reason'] = 'Unable to abort chat';
                            }
                        }
                        catch (Exception $e) {
                            $final_result['status'] = false;
                            $final_result['reason'] = 'Unable to identify chat session';
                        }
                    }
                } else
                {
                    // manual abort
                    $reader->update_last_abort_time(); // general abort time.

                    $params['client_username'] = $client['username'];
                    $params['reader_username'] = $reader->data['username'];
                    $params['topic'] = $object->object['topic'];
                    $this->system_vars->omail($reader->data['email'], 'client_quit', $params);
                    //$this->system_vars->omail("murvinlai@gmail.com",'client_quit',$params);
                    try {
                        if ($this->chatmodel->setSession($result['id'])->abortChat($post_reader_status, false))
                        {
                            // get the final time. 
                            $final_result['status'] = true;
                            
                            $this->load->model('Reader');
                            $this->reader->set_status('online',false,$reader->data["id"]);
                            $final_result['reason'] = 'Manual Abort Chat success';
                        } else
                        {
                            $final_result['status'] = false;
                            $final_result['reason'] = 'Unable to abort chat';
                        }
                    }
                    catch (Exception $e) {
                        $final_result['status'] = false;
                        $final_result['reason'] = 'Unable to identify chat session';
                    }
                }
            } else
            {
                $final_result['status'] = false;
                $final_result['reason'] = "Unable to abort chat. Chat session not found $chat_session_id";
            }
        }
        echo json_encode($final_result);
    }

    function reject_chat()
    {
        $chat_session_id = $this->input->post('room_name'); // chat_session_id
        $member_hash = $this->input->post('member_hash');
        $rejected_by = $this->input->post('rejected_by');
        $timer = $this->input->post('timer');

        $result = $this->chatmodel->validateChatSession(null, $chat_session_id, $member_hash, $rejected_by);

        $final_result = array();
        if ($result === false)
        {
            $final_result['status'] = false;
            $final_result['reason'] = "Unable to validate chat session with $chat_session_id, $member_hash";
        } else
        {
            if ($object = $this->chatmodel->openActiveChatRoomByChatSession(array('chat_session_id' => $chat_session_id)))
            {

                $final_result['status'] = true;

                try {
                    if ($this->chatmodel->setSession($result['id'])->rejectChat())
                    {
                        // get the final time. 
                        $final_result['reason'] = 'Reject Chat success';
                    } else
                    {
                        $final_result['status'] = false;
                        $final_result['reason'] = 'Unable to reject chat';
                    }
                }
                catch (Exception $e) {
                    $final_result['status'] = false;
                    $final_result['reason'] = 'Unable to identify chat session';
                }
            } else
            {
                $final_result['status'] = false;
                $final_result['reason'] = "Unable to reject chat. Chat session not found $chat_session_id";
            }
        }
        echo json_encode($final_result);
    }

    //--- End Chat
    function end_chat()
    {
        $chat_session_id = $this->input->post('room_name'); // chat_session_id
        $member_hash = $this->input->post('member_hash');
        $terminated_by = $this->input->post('terminated_by');
        $timer = $this->input->post('timer');

        $result = $this->chatmodel->validateChatSession(null, $chat_session_id, $member_hash, $terminated_by);

        //echo json_encode($result);

        $final_result = array();
        if ($result === false)
        {
            $final_result['status'] = false;
            $final_result['reason'] = "Unable to validate chat session with $chat_session_id, $member_hash and $terminated_by";
        } else
        {
            if ($object = $this->chatmodel->openActiveChatRoomByChatSession(array('chat_session_id' => $chat_session_id)))
            {
                $object->recordLength($timer);

                $final_result['status'] = true;
                $final_result['reason'] = 'EndChat success';
                $final_result['start_time'] = $object->object['start_datetime'];
                $final_result['max_chat_length'] = $object->object['set_length'];
                $final_result['chat_length'] = $object->object['length'] + $timer;

                $final_result['time_balance'] = $final_result['max_chat_length'] - $final_result['chat_length'];
                if ($final_result['time_balance'] < 0)
                {
                    $final_result['time_balance'] = 0;
                }

                try {
                    if ($object->endChat())
                    // if ($this->chatmodel->setSession($result['id'])->endChat())
                    {
                        // EndChat success, reader switch to break
                        //$result['reader_id'];
                        $this->reader->set_status("break", false,$result['reader_id']);
                        $this->reader->set_break_time(10,$result['reader_id']);
                        // get the final time. 
                        $final_result['reason'] = 'EndChat success';
                    } else
                    {
                        $final_result['status'] = false;
                        $final_result['reason'] = 'Unable to end chat';
                    }
                }
                catch (Exception $e) {
                    $final_result['status'] = false;
                    $final_result['reason'] = 'Unable to identify chat session';
                }
            } else
            {
                $final_result['status'] = false;
                $final_result['reason'] = "Unable to end chat. Chat session not found $chat_session_id";
            }
        }
        echo json_encode($final_result);
    }

    function test_chat()
    {
        $this->db->where(array
            (
            'member_id' => 6,
            'type' => 'reading',
            'balance >' => 0
        ));
        $this->db->order_by("tier = 'promo' desc");
        $this->db->order_by("id");
        $total = 0;
        // Get matching transactions
        foreach ($this->db->get('member_balance')->result() as $a) {
            echo $a->id . " " . $a->tier . "<br />";
        }
    }

    //--- Purchase More time
    function purchaseMoreTime() // previous $session_id = null
    {
        $chat_session_id = $this->input->post('chat_session_id');

        if ($object = $this->chatmodel->openActiveChatRoomByChatSession(array('chat_session_id' => $chat_session_id)))
        {
            $this->session->set_userdata('chat_id', $object->object['id']);

            $reader = $this->member->get_member_data($object->object['reader_id']);

            $final_result['status'] = true;
            $final_result['data'] = array('redirect' => "/chat/main/purchase_time/{$reader['username']}");
            echo json_encode($final_result);
        } else
        {
            $final_result['status'] = false;
            $final_result['reason'] = "Unable to purchase more time. Chat session not found $chat_session_id";
            echo json_encode($final_result);
        }
    }

    //--- addStoredTime
    function getStoredTime()
    {
        $chat_session_id = $this->input->post('chat_session_id');

        if ($object = $this->chatmodel->openActiveChatRoomByChatSession(array('chat_session_id' => $chat_session_id)))
        {
            $this->session->set_userdata('chat_id', $object->object['id']);
            //$reader = $this->member->get_member_data( $object->object['reader_id'] );

            $stored_time = $this->member_funds->minute_balance($object->object['client_id']) - floor($object->object['set_length'] / 60);

            if ($stored_time < 0)
            {
                $stored_time = 0;
            }

            $final_result['status'] = true;
            $final_result['stored_time'] = $stored_time;
            echo json_encode($final_result);
        } else
        {
            $final_result['status'] = false;
            $final_result['reason'] = "Unable to get stored time. Chat session not found $chat_session_id";
            echo json_encode($final_result);
        }
    }

    // deprecaited
    function addStoredTime()    //$session_id = null
    {
        $minutes = trim($this->input->post('minutes'));
        $chat_session_id = $this->input->post('chat_session_id');

        $final_result = array();
        if (!is_numeric($minutes) || $minutes < 1)
        {
            $final_result['status'] = false;
            $final_result['reason'] = "Please enter at least 1 minute into the chat length field: $minutes";
            echo json_encode($final_result);
            return;
        }

        if ($object = $this->chatmodel->openActiveChatRoomByChatSession(array('chat_session_id' => $chat_session_id)))
        {
            $user_time_balance = $this->member_funds->minute_balance($object->object['client_id']) - floor($object->object['set_length'] / 60);

            if ($user_time_balance <= 0)
            {
                $final_result['status'] = false;
                $final_result['reason'] = "You only have zero minute in your account.  ";
                echo json_encode($final_result);
                return;
            }

            $this->session->set_userdata('chat_id', $object->object['id']);
            if ($minutes > $user_time_balance)
            {
                $final_result['status'] = false;
                $final_result['reason'] = "You do not have enough minutes in your balance to start a chat for {$minutes} minutes";
                echo json_encode($final_result);
                return;
            }

            $chat_id = $object->object['id'];

            $new_set_length = $object->object['set_length'] + ($minutes * 60);

            $update = array();
            $update['set_length'] = $new_set_length;

            $this->db->where('id', $chat_id);
            $this->db->update('chats', $update);


            $final_result['time_balance'] = $new_set_length - $object->object['length'];
            if ($final_result['time_balance'] < 0)
            {
                $final_result['time_balance'] = 0;
            }
            $final_result['max_chat_length'] = $new_set_length;
            $final_result['chat_length'] = $object->object['length'];
            $final_result['total_free_length'] = $object->object['total_free_length'];
            $final_result['added_time'] = $minutes;

            $final_result['status'] = true;

            echo json_encode($final_result);
        } else
        {
            $final_result['status'] = false;
            $final_result['reason'] = "Unable to Add stored time. Chat session not found $chat_session_id";
            echo json_encode($final_result);
        }
    }

    function addFreeTime()    //$session_id = null
    {
        $minutes = trim($this->input->post('minutes'));
        $chat_session_id = $this->input->post('chat_session_id');

        $final_result = array();
        if (!is_numeric($minutes))
        {
            $final_result['status'] = false;
            $final_result['reason'] = "Please enter at least 1 minute into the free chat length field: $minutes";
            echo json_encode($final_result);
            return;
        }

        if ($object = $this->chatmodel->openActiveChatRoomByChatSession(array('chat_session_id' => $chat_session_id)))
        {

            $user_time_balance = $this->member_funds->minute_balance($object->object['client_id']) - floor($object->object['set_length'] / 60);

            if ($user_time_balance / 60 > 1000 || ($user_time_balance / 60 + $minutes) > 1000)
            {
                $final_result['status'] = false;
                $final_result['reason'] = "You try to add too much free time.  ";
                echo json_encode($final_result);
                return;
            }
            
            $this->session->set_userdata('chat_id', $object->object['id']);
            
            $chat_id = $object->object['id'];

            //$new_set_length = $object->object['set_length'] + ($minutes * 60);
            $new_set_length = $object->object['set_length'];
            $total_free_length = $object->object['total_free_length'] + ($minutes * 60);

            $update = array();
            $update['set_length'] = $new_set_length;
            $update['total_free_length'] = $total_free_length;

            $this->db->where('id', $chat_id);
            $this->db->update('chats', $update);

            //$final_result['time_balance'] = $new_set_length - $object->object['length'];
            $final_result['time_balance'] = $object->object['length'];
            if ($final_result['time_balance'] < 0)
            {
                $final_result['time_balance'] = 0;
            }
            $final_result['max_chat_length'] = $new_set_length;
            $final_result['chat_length'] = $object->object['length'];
            $final_result['total_free_length'] = $object->object['total_free_length'] + $minutes * 60;
            $final_result['free_time_added'] = $minutes;

            $final_result['status'] = true;

            echo json_encode($final_result);
        } else
        {
            $final_result['status'] = false;
            $final_result['reason'] = "Unable to Add Free time. Chat session not found $chat_session_id";
            echo json_encode($final_result);
        }
    }

    //--- Reset chat - this is called by webpage, not by node.js chat server. 
    function resetChat()
    {
        $object = $this->chatmodel->openChatRoom(array('session_id' => $this->session->userdata('chat_id')))->object;

        $this->session->unset_userdata('chat_id');
        $reader = $this->member->get_member_data($object['reader_id']);

        redirect();
    }

    function test_popup()
    {

        $seconds = trim($this->input->get('seconds'));

        if (empty($seconds))
        {
            $seconds = 0;
        }

        sleep($seconds);

        $final_result = array(
            'status' => true,
            'time' => $seconds
        );

        echo json_encode($final_result);
    }
    
    function check_max_length()
    {
        $chat_id = $this->input->post("chat_id");
        $max_chat_length = $this->input->post("max_chat_length");
        
        $this->db->where("id", $chat_id);
        $query = $this->db->get("chats");

        $return = [];
        $return["status"] = "FAILED";
        if ( $query->num_rows() > 0 )
        {
            $row = $query->row();
            if ( (int)$row->set_length != (int)$max_chat_length )
            $return["status"] = "OK";
        }        
	
        echo json_encode($return);
        
    }

    function save_sound_settings()
    {
        $user_id = $this->input->post("user_id");
        $sound = $this->input->post("sound");
        
        $this->db->where("id", $user_id);
        $this->db->update("members", ["chat_notify_sound" => $sound]);
        
        $return["status"] = "OK";
        echo json_encode($return);
    }

    function save_doorbell_sound_settings() {
        $user_id = $this->input->post("user_id");
        $sound = $this->input->post("sound");

        $this->db->where("id", $user_id);
        $this->db->update("members", ["chat_try_sound" => $sound]);

        $return["status"] = "OK";
        echo json_encode($return);
    }

    /*9-8*/
    function save_reader_pause_timer() {
        $user_id = $this->input->post("user_id");
        $type = $this->input->post("type");

        $this->db->where("id", $user_id);
        $this->db->update("members", ["pause_timer" => $type]);

        $return["status"] = "OK";
        echo json_encode($return);
    }

    /*9-8*/
    function save_reader_pause_timer_size() {
        $user_id = $this->input->post("user_id");
        $size = $this->input->post("size");

        $this->db->where("id", $user_id);
        $this->db->update("members", ["pause_timer_size" => $size]);

        $return["status"] = "OK";
        echo json_encode($return);
    }

    /*9-8*/
    function save_reader_pause_timer_flag() {
        $user_id = $this->input->post("user_id");
        $flag = $this->input->post("flag");

        $this->db->where("id", $user_id);
        $this->db->update("members", ["pause_timer_flag" => $flag]);

        $return["status"] = "OK";
        echo json_encode($return);
    }

    /*9-8*/
    function get_pause_timer_data() {
        $id = $this->input->post("id");

        $reader_data = $this->db->query("SELECT * FROM members WHERE id = '{$id}'")->row_array();

        $return['data'] = $reader_data;
        echo json_encode($return);

    }

    /*9-8*/
    function get_repeat_info() {
        $reader_id = $this->input->post("reader_id");
        $client_id = $this->input->post("client_id");

        $repeat_info = $this->db->query("SELECT * FROM chats WHERE reader_id = '{$reader_id}' AND client_id = '{$client_id}'")->result_array();
        $repeat_num = count($repeat_info);

        $return['data'] = $repeat_num;

        echo json_encode($return);
    }

    /*9-8*/
    function get_repeat_client() {
        $reader_id = $this->input->post("id");

        $repeat_client = $this->db->query("SELECT * FROM chats WHERE reader_id = '{$reader_id}' AND client_id <> 1")->result_array();
        $client_id_array = [];
        $repeate_client_array = [];
        foreach($repeat_client as $item) {

            array_push($client_id_array, $item['client_id']);
        }
        $client_id_array = array_unique($client_id_array);

        foreach($client_id_array as $item1) {
         
            $client_info = $this->db->query("SELECT * FROM members WHERE id = '{$item1}'")->row_array();

            array_push($repeate_client_array, $client_info); 
        }

        $return["repeat_client"] = $repeate_client_array;
        echo json_encode($return);
    }

    /*9-8*/
    function save_repeat_client() {
        $reader_id = $this->input->post("id");
        $data = $this->input->post("data");
        $this->db->where('id', $reader_id);
        $this->db->update('members', ["repeat_client" => serialize($data)]);

        $return['data'] = $data;
        echo json_encode($return);
    }

    /*9-8*/
    function get_saved_repeat_client() {

        $reader_id = $this->input->post("id");
        $reader_info = $this->db->query("SELECT * FROM members WHERE id = '{$reader_id}'")->row_array();
        $saved_repeat_client = unserialize($reader_info["repeat_client"]); 
        $return['data'] = $saved_repeat_client;

        echo json_encode($return);
    }

    function save_fontcolor() {

        $user_id = $this->input->post("user_id");
        $font_color = $this->input->post("data");
        $this->db->where("id", $user_id);
        $this->db->update("members", ["font_color" => $font_color]);
        $return["status"] = "OK";
        echo json_encode($return);
    }

    function save_fontsize() {

        $user_id = $this->input->post("user_id");
        $font_size = $this->input->post("data");
        $this->db->where("id", $user_id);
        $this->db->update("members", ["font_size" => $font_size]);
        $return["status"] = "OK";
        echo json_encode($return);

    }

    function get_fontcolor()
    {

        $user_id = $this->input->post("user_id");
      
       
        $getChat = $this->db->query
            ("
                SELECT
                    *
                FROM members

                WHERE id = '{$user_id}'

                LIMIT 1

            ");
        $data = $getChat->row_array();
        $font_color = $data;
        $return['font_color'] = $font_color;
        echo json_encode($return);

    }

    function get_fontsize() {

        $user_id = $this->input->post("user_id");
     
        $getChat = $this->db->query
            ("
                SELECT
                    *
                FROM members

                WHERE id = '{$user_id}'

                LIMIT 1

            ");
        $data = $getChat->row_array();
       
        $font_size = $data;
        $return['font_size'] = $font_size;
        echo json_encode($return);

    }

    function get_block_words() 
    {
        $getData = $this->db->query
        ("
                SELECT
                    *
                FROM word_block

        ");

        $data = $getData->result_array();
        $return['block_words'] = $data;
        echo json_encode($return);
    }

    /*9-2*/
    function save_note()
    {
        $reader_id = $this->input->post("reader_id");
        $client_id = $this->input->post("client_id");
        $client_username = $this->input->post("client_username");
        $client_firstname = $this->input->post("client_firstname");
        $client_lastname = $this->input->post("client_lastname");
        $note = $this->input->post("note");

        $query = $this->db->get_where('reader_note', array(
            'reader_id' => $reader_id,
            'client_id' => $client_id
        ));

        $count = $query->num_rows();

        if ($count === 0) { //if such row doesn't exist;

            $data = array(

                'reader_id' => $reader_id,
                'client_id' => $client_id,
                'note'      => $note,
                'updated_at' => date("Y-m-d H:i:s")
            );
            $this->db->insert('reader_note', $data);
        } else {

            $this->db->where('reader_id', $reader_id);
            $this->db->where('client_id', $client_id);
            $this->db->update('reader_note', ["note" => $note, "updated_at" => date("Y-m-d H:i:s")]);
        }

        $return['note'] = $note;
        $return['reader_id'] = $reader_id;
        $return['client_id'] = $client_id;

        echo json_encode($return);
    }

    function get_note()
    {
        $reader_id = $this->input->post("reader_id");
        $client_id = $this->input->post("client_id");

        $query = $this->db->get_where('reader_note', array(
            'reader_id' => $reader_id,
            'client_id' => $client_id
        ));

        $count = $query->num_rows();

        if ($count === 0) 
        {

            $note = "";

        } 

        else 
        {

            $getNote = $this->db->query
                ("
                    SELECT
                        `note`
                    FROM reader_note
    
                    WHERE reader_id = '{$reader_id}' AND client_id = '{$client_id}'
    
                    LIMIT 1
    
                ");
            $record = $getNote->row_array();
            $note = $record['note'];
        }

        $return['note'] = $note;
        $return['reader_id'] = $reader_id;
        $return['client_id'] = $client_id;

        echo json_encode($return);
    }

    /*9-9*/
    function get_favorite_info() 
    {
        $reader_id = $this->input->post("reader_id");
        $client_id = $this->input->post("client_id");

        $favorite_info = $this->db->query("SELECT `id` FROM favorites WHERE reader_id = '{$reader_id}' AND client_id = '{$client_id}'")->result_array();
        $favorite_check = count($favorite_info);

        $return["data"] = $favorite_check;
        echo json_encode($return);
    }

    function set_favorite()
    {
        $reader_id = $this->input->post("reader_id");
        $client_id = $this->input->post("client_id");

        $data = array(

            'reader_id' => $reader_id,
            'client_id' => $client_id,
        );

        $this->db->insert('favorites', $data);

        $return["status"] = "ok";
        echo json_encode($return);
    }

    function set_unfavorite()
    {
        $reader_id = $this->input->post("reader_id");
        $client_id = $this->input->post("client_id");

        $this->db->where('reader_id', $reader_id);
        $this->db->where('client_id', $client_id);
        $this->db->delete('favorites');

        $return['status'] = 'ok';
        echo json_encode($return);
    }

    /*9-12*//*9-28*/
    function save_give_back() {
        $user_id = $this->input->post("id");
        $type = $this->input->post("type");
        $size = $this->input->post("size");
        $data = $this->input->post("data");
        $weekday = $this->input->post("weekday");
        $now = date('Y-m-d');

        $previous_gift_info = $this->db->query("SELECT `id` FROM reader_giveback")->result_array();
        if (count($previous_gift_info) == 0) {
            $group_id = 0;
        } else {
            $last_group_info = $this->db->query("SELECT * FROM reader_giveback ORDER BY id DESC LIMIT 1")->row_array();
            $group_id = $last_group_info['group_id'] + 1;
        }

        $this->db->where("id", $user_id);
        if($data == null)
        {
            $this->db->update("members", ["pause_timer" => $type, "pause_timer_size" => $size, "repeat_client" => null, "week_day"=>$weekday, "giveback_created_at" => $now]);
        }
        else
        {
            $this->db->update("members", ["pause_timer" => $type, "pause_timer_size" => $size, "repeat_client" => serialize($data), "week_day"=>$weekday, "giveback_created_at" => $now]);
        }
        /*9-27*/
        switch ($type) {

            case 1: //new clients
                $clients_all = $this->db->query("SELECT `id` FROM members WHERE type IS NULL AND id <> 1")->result_array();
                foreach ($clients_all as $key => $client) {
                    $client_id = $client["id"];
                    $chat_history_info = $this->db->query("SELECT `client_id` FROM chats WHERE reader_id = '{$user_id}' AND client_id = '{$client_id}'")->result_array();
                    foreach ($chat_history_info as $chat) {
                        if ($chat["client_id"] == $client_id) {
                            unset($clients_all[$key]);
                        }
                    }
                }
                foreach($clients_all as $client)
                {
                    $data = array(
                        'reader_id' => $user_id,
                        'client_id' => $client['id'],
                        'gift_size' => $size,
                        'day_of_week' => $weekday,
                        'group_id' => $group_id,
                        'group_type' => 1,
                        'created_at' => $now
                    );
                    $this->db->insert('reader_giveback', $data);
                }
                break;

            case 2://repeat clients
                $repeat_client_array = $data;
                foreach ($repeat_client_array as $client_id) {
                    $data = array(
                        'reader_id' => $user_id,
                        'client_id' => $client_id,
                        'gift_size' => $size,
                        'day_of_week' => $weekday,
                        'group_id' => $group_id,
                        'group_type' => 2,
                        'created_at' => $now
                    );
                    $this->db->insert('reader_giveback', $data);
                    $group_id++;
                }
                break;

            case 3://favorite clients
                $favorite_clients = $this->db->query("SELECT `client_id` FROM favorites WHERE reader_id = '{$user_id}'")->result_array();
                foreach ($favorite_clients as $favorite_client) {
                    $client_id = $favorite_client["client_id"];
                    $data = array(
                        'reader_id' => $user_id,
                        'client_id' => $client_id,
                        'gift_size' => $size,
                        'day_of_week' => $weekday,
                        'group_id' => $group_id,
                        'group_type' => 3,
                        'created_at' => $now
                    );
                    $this->db->insert('reader_giveback', $data);
                }
                break;

            case 4://all clients
                $clients = $this->db->query("SELECT `id` FROM members WHERE type IS NULL AND id <> 1")->result_array();
                foreach($clients as $client)
                {
                    $data = array(
                        'reader_id' => $user_id,
                        'client_id' => $client['id'],
                        'gift_size' => $size,
                        'day_of_week' => $weekday,
                        'group_id' => $group_id,
                        'group_type' => 4,
                        'created_at' => $now
                    );
                    $this->db->insert('reader_giveback', $data);
                }
                break;
        }

        $return["status"] = "OK";
        echo json_encode($return);
    }
    /*9-13*/
    function get_give_back_info()
    {
        $reader_id = $this->input->post("reader_id");
        $client_id = $this->input->post("client_id");
        $day_of_week = $this->input->post("week_day");

        $available_gift_array = $this->db->query("SELECT * FROM reader_giveback 
                                                  WHERE (reader_id = '{$reader_id}' AND client_id = '{$client_id}' AND day_of_week = '{$day_of_week}' AND status = 0)
                                                  OR (reader_id = '{$reader_id}' AND client_id = '{$client_id}' AND day_of_week = 7 AND status = 0)
                                                  ")->result_array();

        $available_gift = $available_gift_array[0];
        $return['data'] = $available_gift;

        echo json_encode($return);
    }

    function save_applied_client()
    {
        $reader_id = $this->input->post("reader_id");
        $client_id = $this->input->post("client_id");
        /*9-22*/
        $gift_id = $this->input->post("gift_id");
        $now = date('Y-m-d');

        $this->db->where("id", $gift_id);
        $this->db->update("reader_giveback", ["status" => 1, 'created_at' => $now]);

        $return['reader_id'] = $reader_id;
        $return['client_id'] = $client_id;
        $return['date'] = date("Y-m-d");

        echo json_encode($return);
    }

    /*9-18*/
    function get_client_admin_interrupt_message()
    {
        $chat_session_id = $this->input->post("chat_session_id");

        $admin_interrupt_messages = $this->db->query("SELECT `id` FROM admin_interrupt WHERE (interrupt_type = 2 OR interrupt_type = 3) AND chat_session_id = '{$chat_session_id}' AND client_check = 0")->result_array();
        foreach ($admin_interrupt_messages as $item) {
            $this->db->where("id", $item['id']);
            $this->db->update("admin_interrupt", ["client_check" => 1]);
        }

        $return['admin_interrupt_messages'] = $admin_interrupt_messages;
        echo json_encode($return);
    }

    function get_reader_admin_interrupt_message()
    {
        $chat_session_id = $this->input->post("chat_session_id");

        $admin_interrupt_messages = $this->db->query("SELECT `id` FROM admin_interrupt WHERE (interrupt_type = 1 OR interrupt_type = 3) AND chat_session_id = '{$chat_session_id}' AND reader_check = 0")->result_array();
        foreach ($admin_interrupt_messages as $item) {
            $this->db->where("id", $item['id']);
            $this->db->update("admin_interrupt", ["reader_check" => 1]);
        }

        $return['admin_interrupt_messages'] = $admin_interrupt_messages;
        echo json_encode($return);
    }

    /*9-21*/
    function get_available_client()
    {
        $reader_id = $this->input->post("id");
        $available_clients = $this->db->query("SELECT * FROM reader_giveback WHERE reader_id = '{$reader_id}' AND status = 0")->result_array();
        $clients = [];

        foreach ($available_clients as $client)
        {
            $client_info = $this->db->query("SELECT `username` FROM members WHERE id = '{$client['client_id']}'")->row_array();
            $client_info_data['id'] = $client['id'];
            $client_info_data['username'] = $client_info['username'];
            $client_info_data['amount'] = $client['gift_size'];
            $client_info_data['day_of_week'] = $client['day_of_week'];
            $client_info_data['group_id'] = $client['group_id'];
            $client_info_data['group_type'] = $client['group_type'];
            $client_info_data['created_at'] = $client['created_at'];

            array_push($clients, $client_info_data);
        }

        $return["clients"] = $clients;
        echo json_encode($return);
    }
    /*9-21*/
    function save_give_back_cancel()
    {
        $cancelled_groups = $this->input->post("data");
        $now = date('Y-m-d');

        foreach($cancelled_groups as $group_id)
        {
            $this->db->where('group_id', $group_id);
            $this->db->update('reader_giveback', ['status' => 2, 'created_at' => $now]);
        }

        $return["status"] = "OK";
        echo json_encode($return);
    }

    /*9-23*/
    function save_admin_reply()
    {
        $member_id = $this->input->post("member_id");
        $message = $this->input->post("message");
        $chat_session_id = $this->input->post("chat_session_id");

        $data = array(
            'member_id' => $member_id,
            'chat_session_id' => $chat_session_id,
            'reply_text' => $message
        );
        $this->db->insert('admin_interrupt_reply', $data);
    }

    function delete_give_back_history()
    {
        $deleted_give_back_data = $this->input->post("data");

        if(count($deleted_give_back_data) > 0)
        {
            foreach($deleted_give_back_data as $data)
            {
                $this->db->where("id", $data);
                $this->db->delete("reader_giveback");
            }
        }

        $return["status"] = "Ok";
        echo json_encode($return);
    }

    /*10-16*/
    function check_client_warning()
    {
        $client_id = $this->input->post("client_id");
        $account_info = $this->db->query("SELECT `email`, `banned`, `username` FROM members WHERE id = '{$client_id}'")->row_array();

        $client_email = $account_info["email"];
        $search_by_email = $this->db->query("SELECT `id` FROM members WHERE email = '{$client_email}'")->result_array();
        $client_banned_info = $account_info["banned"];

        $return["warning_account"] = false;
        $return["warning_banned"] = false;

        if(count($search_by_email) > 1)
        {
            $return["warning_account"] = true;
        }
        if($client_banned_info == 1)
        {
            $return["warning_banned"] = true;
        }
        if($account_info['username'] == 'Jake3')
        {
            $return['warning_jake3'] = true;
        }

        echo json_encode($return);
    }

    function get_warning_messages()
    {
        $warnings = $this->warning_model->get_all_warnings();
        $return["data"] = $warnings;

        echo json_encode($return);
    }

    function get_chat_ended()
    {
        $chat_session_id = $this->input->post("chat_session_id");
        $chat = $this->db->query("SELECT `ended` FROM chats WHERE chat_session_id = '{$chat_session_id}'")->row_array();

        $return['data'] = $chat["ended"];

        echo json_encode($return);
    }

    // 10-18
    function are_you_there_logout()
    {
        $member = $this->member->data;
        $member['type'] = NULL;
        $this->system_vars->m_omail($member['id'], 'are_you_there_logout', $member);

        $member['type'] = 'email';
        $this->system_vars->m_omail($member['id'], 'are_you_there_logout', $member);
        $this->system_vars->m_omail(1, 'are_you_there_logout', $member);

        $return["status"] = "successful";
        echo json_encode($return);
    }

    function save_browser_info()
    {
        $member_type = $this->input->post('member_type');
        $chat_session_id = $this->input->post('chat_session_id');
        $browser_info = $this->input->post('browser_info');
        $user_agent = $this->input->post('user_agent');

        if($member_type == 'reader')
        {
            $data = array(
                'reader_browser' => $browser_info,
                'reader_agent' => $user_agent
            );
            $this->db->where('chat_session_id', $chat_session_id);
            $this->db->update('chats', $data);
        }

        if($member_type == 'client')
        {
            $data = array(
                'client_browser' => $browser_info,
                'client_agent' => $user_agent
            );
            $this->db->where('chat_session_id', $chat_session_id);
            $this->db->update('chats', $data);
        }

    }
}
