<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class package extends CI_Controller
{

    function __construct()
    {
        parent :: __construct();

        $this->load->library('parser');
        $this->load->helper('CustomCalendar');

        $this->settings = $this->system_vars->get_settings();
        if ($this->session->userdata('member_logged'))
        {
            redirect('/my_account');
            exit;
        }
        parse_str($_SERVER['QUERY_STRING'], $_GET);
    }

    function index()
    {

        $this->load->model('sitepackage');

        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();
        $record['packages'] = $this->sitepackage->getPackages();
        $data = array(
            'title' => 'Psychic Contact',
            'header' => $this->load->view('frontend/layouts/header', $param_header, TRUE),
            'slider' => $this->load->view('frontend/layouts/slider', '', TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE),
            'content' => $this->load->view('/frontend/pages/prices', $record, TRUE)
        );

        $this->load->library('parser');
        // $this->load->library('calendar');
        // $this->load->helper('date');
        $this->parser->parse('frontend/layouts/sub_page', $data);
    }

}
