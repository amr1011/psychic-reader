<?php

	class faqs extends CI_Controller
	{
	
		function __construct()
		{
		
			parent :: __construct();
			
		}
		
		function index()
		{
		
			$getQuestions = $this->db->query("SELECT * FROM faqs WHERE type = 'Clients' ORDER BY sort ");
			$t['clients'] = $getQuestions->result_array();
			
			$getQuestions = $this->db->query("SELECT * FROM faqs WHERE type = 'Readers' ORDER BY sort ");
			$t['readers'] = $getQuestions->result_array();
		
			// $this->load->view('frontend/layouts/header');
			// $this->load->view('frontend/faqs/faq_main', $t);
			// $this->load->view('frontend/layouts/footer');
			$params = '';
			$param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
			$param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

			$data = array(
				'title' => "FAQs",
				'header' => $this->load->view('frontend/layouts/header_login', $param_header, TRUE),
				'slider' => "",
				'content' => $this->load->view('/frontend/faqs/faq_main', $t, TRUE),
				'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
			);
	
			$this->load->library('parser');
			$this->parser->parse('frontend/layouts/page', $data);
		
		}
			
	}