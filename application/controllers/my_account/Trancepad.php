<?php

class trancepad extends CI_Controller
{

    function __construct()
    {

        parent :: __construct();

        $this->settings = $this->system_vars->get_settings();

        if (!$this->session->userdata('member_logged'))
        {
            $this->session->set_flashdata('error', "You must login before you can gain access to secured areas");
            redirect('/register/login');
            exit;
        }
    }

    function index()
    {
        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/trancepad', '', TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function toggle($status = 0)
    {

        $this->member->update_profile(array('trancepad_enabled' => $status));

        redirect("/my_account/trancepad");
    }

}
