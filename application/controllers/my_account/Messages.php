<?php

class messages extends CI_Controller
{

    function __construct()
    {

        parent :: __construct();

        $this->settings = $this->system_vars->get_settings();

        if (!$this->session->userdata('member_logged'))
        {

            $this->session->set_flashdata('error', "You must login before you can gain access to secured areas");
            redirect('/register/login');
            exit;
        }
    }

    function test_omail()
    {
        $recipient_id = 6;
        $sender_id = 7;
        $email_template = "cc_account_funded";
        $type = 'email';
        $this->messages_model->notify(null, $recipient_id, null, null, $type, $email_template);
    }

    function index()
    {

        $params['title'] = "My Messages: Inbox";

        $messages = [];
        $senders = [];
        $sender = [];

        $senders_array = $this->db->query("SELECT DISTINCT messages.id, sender_id, members.first_name, members.username FROM messages 
                                      LEFT JOIN members ON messages.sender_id=members.id
                                      WHERE ricipient_id ={$this->member->data['id']}  AND sender_id IS NOT NULL  AND sender_id <> 1 AND (messages.`type`<>'email' OR messages.`type` IS NULL) ORDER BY messages.id DESC")->result_array();
        foreach($senders_array as $item)
        {
            $unread = $this->db->query("SELECT COUNT(id) as unread FROM messages WHERE  ricipient_id ={$this->member->data['id']} AND sender_id='{$item['sender_id']}' AND `read` = 0")->row_array();
            $sender['first_name'] = $item['first_name'];
            $sender['username'] = $item['username'];
            $sender['sender_id'] = $item['sender_id'];
            $sender['unread'] = $unread['unread'];
            if($this->member->data['member_type'] == 'reader')
            {
                $sender['banned'] = 0;
                $check_ban = $this->db->query("SELECT id FROM member_bans WHERE reader_id='{$this->member->data['id']}' AND member_id='{$sender['sender_id']}' AND `type`='personal'")->result_array();
                if(count($check_ban) > 0)
                {
                    $sender['banned'] = 1;
                }
            }

            array_push($senders, $sender);
            $messages_sub = $this->db->query("SELECT * FROM messages WHERE ricipient_id = {$this->member->data['id']} AND (`type`<>'email' or `type` IS NULL) AND sender_id = '{$sender['sender_id']}' ORDER BY id DESC")->result_array();
            $messages[$sender['first_name']] = $messages_sub;
        }
        $unread_admin = $this->db->query("SELECT COUNT(id) as unread_admin FROM messages WHERE  ricipient_id ={$this->member->data['id']} AND (`type`='admin' OR sender_id=1) AND `read` = 0")->row_array();

        $params['messages'] = $messages;
        $params['senders'] = $senders;
        $params['unread_admin'] = $unread_admin;
        $params['v_from'] = "";
        
        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/messages', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );
        $data['load_js'] = "frontend/my_account/messages_js";


        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function view($message_id)
    {

        $getMessages = $this->db->query("SELECT *
			                                 FROM   messages
			                                 WHERE  id = {$message_id}
			                                        AND (ricipient_id = {$this->member->data['id']}
			                                        OR sender_id = {$this->member->data['id']}) LIMIT 1");
        $t = $getMessages->row_array();

        switch ($t['type'])
        {
            case "email":
                $t['v_from'] = "System";
                break;

            case "admin":
                $t['v_from'] = "Administrator";
                break;

            case "reader":
                $sender = $this->member->get_member_data($t['sender_id']);
                $t['v_from'] = "{$sender['first_name']} {$sender['last_name']}";
                break;

            case "client":
                $sender = $this->member->get_member_data($t['sender_id']);
                $t['v_from'] = "{$sender['first_name']} {$sender['last_name']}";
                break;
        }


        $t['to'] = $this->system_vars->get_member($t['ricipient_id']);

        if($t['sender_id']) {
            $t['from'] = $this->member->get_member_data($t['sender_id']);
        } else {
            $t['from'] = $this->member->get_member_data($this->member->data['id']);
        }

        $this->messages_model->markMessageRead($t['id']);

        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/layouts/header_authenticated', $param_header, TRUE),
            'slider' => "",
            'content' => $this->load->view('frontend/my_account/messages_view', $t, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);

    }

    function delete($message_id)
    {

        $this->db->where('id', $message_id);
        $this->db->where('ricipient_id', $this->member->data['id']);
        $this->db->delete('messages');

        $this->session->set_flashdata('response', "Message has been deleted");

        redirect("/my_account/messages");
    }

    function outbox()
    {
        $params['title'] = "My Messages: Inbox";

        $messages = [];
        $receivers = [];
        $receiver = [];

        $receiver_array = $this->db->query("SELECT DISTINCT ricipient_id, members.first_name, members.username FROM messages 
                                      LEFT JOIN members ON messages.ricipient_id=members.id
                                      WHERE sender_id ={$this->member->data['id']}  AND ricipient_id IS NOT NULL AND ricipient_id <> '{$this->member->data['id']}' AND ricipient_id <> 1 AND (messages.`type`<>'email' OR messages.`type` IS NULL) ORDER BY messages.id DESC")->result_array();
        foreach($receiver_array as $item)
        {
            $receiver['first_name'] = $item['first_name'];
            $receiver['username'] = $item['username'];
            $receiver['receiver_id'] = $item['ricipient_id'];

            if($this->member->data['type'] == 'READER')
            {
                $receiver['banned'] = 0;
                $check_ban = $this->db->query("SELECT id FROM member_bans WHERE reader_id='{$this->member->data['id']}' AND member_id='{$receiver['receiver_id']}' AND `type`='personal'")->result_array();
                if(count($check_ban) > 0)
                {
                    $receiver['banned'] = 1;
                }
            }

            array_push($receivers, $receiver);
            $messages_sub = $this->db->query("SELECT * FROM messages WHERE sender_id = {$this->member->data['id']} AND (`type`<>'email' or `type` IS NULL) AND ricipient_id = '{$receiver['receiver_id']}' ORDER BY id DESC")->result_array();
            $messages[$receiver['first_name']] = $messages_sub;
        }

        $params['messages'] = $messages;
        $params['receivers'] = $receivers;
        $params['v_from'] = "";

        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/messages_outbox', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );
            $data['load_js'] = "frontend/my_account/messages_outbox_js";

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function compose($message_reply_id = null, $type = "reply")
    {
        // reader, get clients they have read for
        if ($this->member->data['member_type'] == "reader")
        {
            $sql = "SELECT 
                        members.id,
                        members.username,
                        members.first_name 
                    FROM
                        chats
                    JOIN members 
                    WHERE members.id = chats.client_id AND chats.reader_id = " . $this->member->data['profile_id'] . "
                    GROUP BY
                        members.id
                    ORDER BY members.username";


        } else
        {
            $sql = "SELECT 
                        members.id,
                        members.username,
                        members.first_name 
                    FROM
                        chats
                    JOIN members 
                    WHERE members.id = chats.reader_id AND chats.client_id = " . $this->member->data['id'] . "
                    GROUP BY
                        members.id
                    ORDER BY members.username";
        }
        
        $getAllMembers = $this->db->query($sql);
        $users = $getAllMembers->result_array();

        foreach($users as $key=>$user)
        {
            $ban_check = count($this->db->query("SELECT id FROM member_bans WHERE member_id={$this->member->data['id']} AND reader_id={$user['id']}")->result_array());
            if($ban_check > 0)
            {
                unset($users[$key]);
            }
        }

        $params['users'] = $users;
        $params['to'] = "";
        $params['subject'] = "";
        $params['message'] = "";

        // If a reply id was passed… Then
        // prepopulate the fields
        if ($message_reply_id && $type == "reply")
        {
            $sql = "SELECT * FROM messages WHERE id = {$message_reply_id} AND (ricipient_id = {$this->member->data['id']} OR sender_id = {$this->member->data['id']}) LIMIT 1";
            $getMessage = $this->db->query($sql);

            if ($getMessage->num_rows() == 1)
            {
                $message = $getMessage->row_array();

                $sender = $this->system_vars->get_member($message['sender_id']);

                $params['to'] = $message['sender_id'];
                $params['to_user'] = $sender;
                $params['subject'] = "Re: " . $message['subject'];
                $params['message'] = "\n\n\n+++ Original Message: {$sender['first_name']} {$sender['last_name']} - {$sender['username']} on " . date("m/d/y h:i:s", strtotime($message['datetime'])) . " +++\n\n" . html_entity_decode($message['message']);
            }
        } else if ($message_reply_id && $type != "reply")
        {
            $params['to'] = $message_reply_id;
            $params['to_user'] = $this->system_vars->get_member($message_reply_id);
            $params['subject'] = "";
            $params['message'] = "";
        }
        
        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/messages_compose', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);

       
    }

    function upload_file()
    {
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png|iso|dmg|zip|rar|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|rtf|sxc|sxi|txt|exe|avi|mpeg|mp3|mp4|3gp';
        $config['max_size'] = 204800;
        $config['max_height'] = 768;
        $config['max_width'] = 1024;
        $this->load->library('upload', $config);

        if($this->upload->do_upload('attach'))
        {
            return $this->upload->data();
        }
        else
        {
            return $this->upload->display_errors();
        }
    }

    function compose_submit()
    {
        $this->form_validation->set_rules("recipient", "Recipient", "trim|required");
        $this->form_validation->set_rules("subject", "Subject", "trim|required");
        $this->form_validation->set_rules("message", "Message", "trim|required");

        $block_words = $getData = $this->db->query
        ("
                SELECT
                    block_words
                FROM word_block

        ")->result_array();



        $from = $this->input->post('from');
        $message = $this->input->post("message");
        $recipients = explode(",", $this->input->post('recipient'));
        $subject = $this->input->post("subject");
        
        foreach($block_words as $block_word)
        {
            if(strpos(strip_tags($message), $block_word['block_words']) !== false) // if message contains block word, it is stored in DB and admin will review it.
            {
                $attachment_file = null;
                $file_data = $this->upload_file();

                if(!is_string($file_data))
                {
                    $attachment_file = $file_data['full_path'];

                } else {

                }
                foreach($recipients as $recipient)
                {
                    $message_data = array(
                        'from' => $from,
                        'to' => $recipient,
                        'subject' => $subject,
                        'message' => $message,
                        'attached_file'=> $attachment_file,
                        'reason' => '',
                        'created_at' => date("Y-m-d H:i:s"),
                        'message_status' => 'PENDING'
                    );

                    $this->db->insert('message_center_notifications', $message_data);
                }

                redirect('/my_account/messages');
            }
        }

        $recipients = explode(",", set_value('recipient'));

        if(count($recipients) > 2)
        {

            $attachment_file = null;
            $file_data = $this->upload_file();

            if(!is_string($file_data))
            {
                $attachment_file = $file_data['full_path'];

            } else {

            }

            $message_data = array(
                'from' => $from,
                'to' => set_value('recipient'),
                'subject' => $subject,
                'message' => $message,
                'attached_file'=> $attachment_file,
                'reason' => '',
                'created_at' => date("Y-m-d H:i:s"),
                'message_status' => 'PENDING'
            );

            $this->db->insert('message_center_group_notifications', $message_data);

            redirect('/my_account/messages');
        }

        if (!$this->form_validation->run())
        {

            $this->compose();

        } else
        {

            // send mail
            $CI = & get_instance();
            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'mail.taroflash.com',
                'smtp_port' => 465,
                'smtp_crypto' => 'ssl',
                'smtp_timeout' => 7,
                'smtp_user' => 'norreply@psychic-contact.com',
                'smtp_pass' => 'NoRep2017*1245Klm1P32124',
                'mailtype'  => 'html',
                'charset'   => 'iso-8859-1'
            );
            $CI->load->library('email', $config);

            // Parse system email template
            $subject = set_value('subject');
            $pre_content = html_entity_decode(nl2br(set_value('message')));

            $content = "
                        <div style='background:#b9b9b9;padding:20px;'>
                            <div style='margin-bottom:10px;font-size:20px;font-weight:bold;font-family:Arial;'>Psychic-Contact.com</div>
                            <div style='background:#FFF;padding:20px;'>{$pre_content}</div>
                        </div>
                        ";

            $file_data = $this->upload_file();

            // Configure Email Options
            $config['mailtype'] = 'html';
            $CI->email->initialize($config);

            // Send Email
            $from[1] = $CI->config->item('email_from_email_address');
            $from[2] = $CI->config->item('email_from_name');

            // IF the TO is a number
            // Get the member and send the message to their local inbox
            // as well as their email address
            $recipients = explode(",", set_value('recipient'));

            foreach($recipients as $recipient_id)
            {
                $CI->email->clear(true);
                $recipient = null;
                if (is_numeric($recipient_id))
                {
                    $member = $this->db->query("SELECT email FROM members WHERE `id`='{$recipient_id}'")->row_array();
                    $recipient = $member['email'];
                }

                $CI->email->from($from[1], $from[2]);
                $CI->email->to($recipient);
                $CI->email->subject($subject);
                $CI->email->message($content);

                if(!is_string($file_data))
                {
                    $CI->email->attach($file_data['full_path']);
                    $this->member->notify($recipient_id, $this->member->data['id'], set_value('subject'), nl2br(set_value('message')), $file_data['file_name']);
                } else {
                    $this->member->notify($recipient_id, $this->member->data['id'], set_value('subject'), nl2br(set_value('message')), null);
                }
                $log = $CI->email->send();
                log_message('debug', $log);
                // $this->member->notify($recipient_id, $this->member->data['id'], set_value('subject'), nl2br(set_value('message')), $file_data['file_name']);
            }



            $this->session->set_flashdata('response', "Your message has been sent");

            redirect('/my_account/messages');
        }
    }

    function getMessages()
    {
        $sender_id = $this->input->post('user_id');
        $messages = $this->db->query("SELECT * FROM messages WHERE ricipient_id ={$this->member->data['id']} AND sender_id = '{$sender_id}' ORDER BY datetime DESC")->result_array();
        $return['messages'] = $messages;

        echo json_encode($return);
    }

    function getAdminMessages()
    {
        $messages = $this->db->query("SELECT * FROM messages WHERE ricipient_id ={$this->member->data['id']} AND (`type`='admin' OR sender_id=1)  ORDER BY datetime DESC")->result_array();
        $return['messages'] = $messages;

        echo json_encode($return);
    }

    function getSentMessages()
    {
        $receiver_id = $this->input->post('user_id');
        $messages = $this->db->query("SELECT * FROM messages WHERE sender_id='{$this->member->data['id']}' AND ricipient_id='{$receiver_id}' ORDER BY datetime DESC")->result_array();

        $return['messages'] = $messages;
        echo json_encode($return);
    }

    function getAdminSentMessages()
    {
        $messages = $this->db->query("SELECT * FROM messages WHERE sender_id ={$this->member->data['id']} AND (`type`='admin' OR ricipient_id=1) ")->result_array();
        $return['messages'] = $messages;

        echo json_encode($return);
    }

    function getSystemMessages()
    {
        $messages = $this->db->query("SELECT * FROM messages WHERE ricipient_id='{$this->member->data['id']}' AND subject = 'No Online Reader' AND `type` IS NULL")->result_array();
        $return['messages'] = $messages;

        echo json_encode($return);
    }



}
