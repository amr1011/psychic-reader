<?php

class chats extends CI_Controller
{

    function __construct()
    {

        parent :: __construct();

        $this->settings = $this->system_vars->get_settings();

        if (!$this->session->userdata('member_logged'))
        {

            $this->session->set_flashdata('error', "You must login before you can gain access to secured areas");
            redirect('/register/login');
            exit;
        }
        $this->load->model("chatmodel");
        $this->load->model("member_funds");
    }

    function index()
    {
        $sql = "SELECT * 
                FROM chats 
                WHERE (reader_id = {$this->member->data['id']} OR client_id  = {$this->member->data['id']}) AND client_id <> 1
                ORDER BY start_datetime DESC ";
        $getChats = $this->db->query($sql);
        $params['chats'] = $getChats->result_array();
        $params['search'] = 1;
        $params['username_direction'] = 'ASC';
        $params['datetime_direction'] = 'DESC';
        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/partial/backend/header_client', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/chats', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function transcript($chat_id)
    {

        $array['session_id'] = $chat_id;
        $this->chatmodel->openChatRoom($array);

        $data['chat_id'] = $chat_id;

        $data['chat_reader'] = ($this->chatmodel->object['reader_id'] == $this->member->data['id'] ? 1 : 0);
        $data['chat_amount'] = "";
        $reader_id = $this->chatmodel->object['reader_id'];
        $reader = $this->member->get($reader_id);
        $data['reader_name'] = $reader['username'];
        
        if ($data['chat_reader'] == 1)
        {
            $chat_bal = $this->db->query("select *
                                  from   profile_balance pb
                                  where  pb.member_id = {$reader_id}
                                         and pb.type = 'reading'
                                         and pb.type_id = {$chat_id} limit 1");

            $results = $chat_bal->row_array();


            if (count($results) > 0)
            {
                $data['chat_amount'] = $results['commission'];
            }
        }

        /*
          Rob: check to see if transaction has been refunded (history needs to persist, but the reader can't refund again)
         */
        // flag to hide refund buttons if no transaction exists
        $data['transNotFound'] = 0;

        $query = "select * from transactions where summary = 'Chat Session #{$chat_id}' limit 1";
        $getTrans = $this->db->query($query);
        $transData = $getTrans->row_array();

        // transNotFound flag to show buttons in view
        if (count($transData) <= 0)
            $data['transNotFound'] = 1;
        /*9-15
                  @pen
                */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/partial/backend/header_client', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/chat_transcript', $data, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $data['load_js'] = "frontend/my_account/chat_transcript_js";


        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);

    }

    function give_timeback($chat_id)
    {

        // gather chat data
        $query = "select * from chats where id = '{$chat_id}' limit 1";
        $getChat = $this->db->query($query);
        $chatData = $getChat->row_array();

        $timeback = $_REQUEST['timeback'];
        $timeback_in_secs = $timeback * 60;

        // convert to dollars, so we can re-use the function
        $cents_to_return = $timeback * 100; // convert to cents


        /* 			
          $this->myDebug("cents_to_return",$cents_to_return);
          exit();
          $this->myDebug("timeback",$timeback);
          $this->myDebug("timeback2",$timeback_in_secs);
          $this->myDebug("length",$chatData['length']);

          exit();
         */

        if (($chatData['length'] >= $timeback_in_secs) and $timeback)
        {


            /*
              need to modify the transaction so the reader only gets paid the other part remaining
             */
            $updated_amt = 0;
            /*
              asked murvin to sow me where he calculates the commission, then i can recalc based on the new chat session length
              - convert entire chat length to seconds, deduct the timebackinsecs, now we have new total chat length, and create a commission value based on its
             */

            // get chat amount, return time to client
            $amount_returned = $this->return_partial_time_to_client($chatData['id'], $cents_to_return, $updated_amt);
            if ($amount_returned)
            {

                $msg = "The partial refund has been processed & the client has been notified";

                // advise the client of the returned time
                $this->member->notify($chat['client_id'], $chat['reader_id'], "Chat Time Returned", nl2br("I have returned a portion of your chat time amount for our chat (#$chat_id) for the amount of ($amount_returned)"));

                // send message to reader
                $this->member->notify($chat['reader_id'], $chat['reader_id'], "Chat Time Returned", nl2br("You have returned a portion of the chat time amount for the chat (#$chat_id) for the amount of ($amount_returned) to the client."));

                // redirect back to chat history, as we have deleted the transaction
                $msg = "You have successfully refunded a portion of the amount for chat session #$chat_id to the client.";
                $this->session->set_flashdata('response', $msg);
                redirect("/frontend/my_account/chats");
            } else
            {
                $msg = "Unable to partial refund. Chat session amount not found $chat_id";
            }
        } else
        {
            $msg = "Unable to partial refund. Chat session #$chat_id less than 1 minute";
        }

        $this->session->set_flashdata('response', $msg);
        redirect("/frontend/my_account/chats/transcript/{$chat_id}");
    }

    /*
      Rob: borrowed from Murvin's chatinterface.php
     */

    function process_refund($chat_id)    // previous $session_id = null
    {

        $query = "select * from chats where id = '{$chat_id}' limit 1";
        $getChat = $this->db->query($query);
        $chatData = $getChat->row_array();

        //print_r($transData);

        if ($chatData['id'] == $chat_id)
        {
            // get chat amount, return time to client
            $amount_returned = $this->return_time_to_client($chatData['id'], $chatData['length']);
            if ($amount_returned)
            {

                $msg = "The refund has been processed & the client has been notified";

                // advise the client of the returned time
                $this->member->notify($chatData['client_id'], $chatData['reader_id'], "Chat Time Returned", nl2br("I have returned the entire chat time amount for our chat (#$chat_id)"));

                // send message to reader
                $this->member->notify($chatData['reader_id'], $chatData['reader_id'], "Chat Time Returned", nl2br("You have returned the entire chat time amount for the chat (#$chat_id) to the client."));

                // redirect back to chat history, as we have deleted the transaction
                $msg = "You have successfully refunded the entire amount for chat session #$chat_id to the client.";
                $this->session->set_flashdata('response', $msg);
                redirect("/frontend/my_account/chats");
            } else
            {
                $msg = "Unable to refund. Chat #$chat_id session amount not found";
            }
        } else
        {
            $msg = "Unable to refund. Chat #$chat_id session not found";
        }

        $this->session->set_flashdata('response', $msg);
        redirect("/frontend/my_account/chats/transcript/{$chat_id}");
    }

    function return_partial_time_to_client($chat_id, $cents_to_return, $updated_amt)
    {
        //$this->myDebug("chat_id",$chat_id);
        // get chat sesson amount spent
        /*
          Rob: this is not a perfect solution as the transactions table has no actual chat_id in it...
         */
        $query = "select * from transactions where summary = 'Chat Session #{$chat_id}' limit 1";
        $getAmount = $this->db->query($query);

        $transData = $getAmount->row_array();
        $trans_id = $transData['id'];
        $client_id = $transData['member_id'];

        // we have an amount to refund to client
        if ($length)
        {

            $query = "select * from members where id = '{$client_id}' limit 1";
            $getMember = $this->db->query($query);
            $memberData = $getMember->row_array();

            $length = $cents_to_return; // already converted to pennies
            // add to member_balance
            $insert = array();
            $insert['member_id'] = $client_id;
            $insert['datetime'] = date("Y-m-d H:i:s");
            $insert['type'] = "reading";
            $insert['tier'] = "regular";
            $insert['total'] = $length;
            $insert['balance'] = $length;
            $insert['transaction_id'] = $transData['id'];
            $insert['region'] = (trim(strtolower($memberData['country'])) == 'ca' ? "ca" : "us");

            // Insert balance
            $this->db->insert('member_balance', $insert);

            // update the transaction as the reader has refunded a partial amount
            $query = "UPDATE 
				transactions SET amount = \"$updated_amt\" WHERE 
				id = {$trans_id}";

            $this->db->query($query);

            // return the amount so we know we were successful
            return $length;
        } else
        {
            // no amount found, return an error
            return 0;
        }
    }

    function return_time_to_client($chat_id, $length)
    {
        //$this->myDebug("chat_id",$chat_id);
        // get chat sesson amount spent
        /*
          Rob: this is not a perfect solution as the transactions table has no actual chat_id in it...
         */
        $query = "select * from transactions where summary = 'Chat Session #{$chat_id}' limit 1";
        $getAmount = $this->db->query($query);

        $transData = $getAmount->row_array();
        $trans_id = $transData['id'];
        $client_id = $transData['member_id'];

        // we have an amount to refund to client
        if ($length)
        {

            $query = "select * from members where id = '{$client_id}' limit 1";
            $getMember = $this->db->query($query);
            $memberData = $getMember->row_array();

            $length = round($length / 100, 2); // convert to pennies
            // add to member_balance
            $insert = array();
            $insert['member_id'] = $client_id;
            $insert['datetime'] = date("Y-m-d H:i:s");
            $insert['type'] = "reading";
            $insert['tier'] = "regular";
            $insert['total'] = $length;
            $insert['balance'] = $length;
            $insert['transaction_id'] = $transData['id'];
            $insert['region'] = (trim(strtolower($memberData['country'])) == 'ca' ? "ca" : "us");

            // Insert balance
            $this->db->insert('member_balance', $insert);

            // delete the transaction as the reader has refunded the entire amount
            $query = "DELETE FROM 
				transactions WHERE 
				id = {$trans_id}";

            $this->db->query($query);

            // return the amount so we know we were successful
            return $length;
        } else
        {
            // no amount found, return an error
            return 0;
        }
    }

    /*
      Rob dev function
     */

    function myDebug($name, $value)
    {
        echo "<br>*$name*/*$value*";
    }

    function get_client_time_remaining($client_id)
    {

        $getData = $this->db->query
                ("
				SELECT
				
					chats.*
					
				FROM
					chats 
									
				WHERE
					chats.id = {$chat_id}
				
			");

        $data = $getChat->row_array();
    }

    /* 		
      function process_refund($chat_id)
      {

      $getChat = $this->db->query
      ("
      SELECT

      chats.*,
      refunds.amount as refund_amount,

      expert.username as expert,

      client.username as client,
      client.email as client_email,
      client.first_name as client_first_name,
      client.last_name as client_last_name

      FROM
      chats

      LEFT JOIN refunds ON chat_id = chats.id
      LEFT JOIN members as expert ON expert.id = chats.expert_id
      LEFT JOIN members as client ON client.id = chats.client_id

      WHERE
      chats.id = {$chat_id}

      ");

      $chat = $getChat->row_array();

      // Subtract this amount FROM the expert
      $insert = array();
      $insert['member_id'] = $chat['expert_id'];
      $insert['datetime'] = date("Y-m-d H:i:s");
      $insert['type'] = 'purchase';
      $insert['amount'] = $chat['refund_amount'];
      $insert['summary'] = "Refund to {$chat['client']} for chat session {$chat['session_id']} from {$chat['expert']}";

      $this->db->insert('transactions', $insert);

      // Give funds TO expert
      $insert = array();
      $insert['member_id'] = $chat['client_id'];
      $insert['datetime'] = date("Y-m-d H:i:s");
      $insert['type'] = 'deposit';
      $insert['amount'] = $chat['refund_amount'];
      $insert['summary'] = "Refund to {$chat['client']} for chat session {$chat['session_id']} from {$chat['expert']}";

      $this->db->insert('transactions', $insert);

      // Update refund transaction
      $update = array();
      $update['status'] = 'processed';

      $this->db->where('chat_id', $chat_id);
      $this->db->update('refunds', $update);

      // Send Email to client (refund_processed)
      $params = array();
      $params['client_first_name'] = $chat['client_first_name'];
      $params['client_last_name'] = $chat['client_last_name'];
      $params['expert_username'] = $chat['expert'];
      $params['amount'] = number_format($chat['refund_amount'], 2);

      $this->system_vars->omail($chat['client_email'], 'refund_processed', $params);

      // Redirect When Done
      $this->session->set_flashdata('response', "The refund has been processed & the client has been notified");
      redirect("/frontend/my_account/chats/transcript/{$chat_id}");

      }
     */

    function submit_rejection($chat_id)
    {

        $getChat = $this->db->query
                ("
				SELECT
				
					chats.*,
					refunds.amount as refund_amount,
					
					expert.username as expert,
					
					client.username as client,
					client.email as client_email,
					client.first_name as client_first_name,
					client.last_name as client_last_name
					
				FROM
					chats 
					
				LEFT JOIN refunds ON chat_id = chats.id
				LEFT JOIN members as expert ON expert.id = chats.expert_id
				LEFT JOIN members as client ON client.id = chats.client_id
					
				WHERE
					chats.id = {$chat_id}
				
			");

        $chat = $getChat->row_array();

        // Update refund transaction
        $update = array();
        $update['rejected_reason'] = $this->input->post('reason');
        $update['status'] = 'rejected';

        $this->db->where('chat_id', $chat_id);
        $this->db->update('refunds', $update);

        // Send Email to client (refund_processed)
        $params = array();
        $params['client_first_name'] = $chat['client_first_name'];
        $params['client_last_name'] = $chat['client_last_name'];
        $params['expert_username'] = $chat['expert'];
        $params['amount'] = number_format($chat['refund_amount'], 2);
        $params['rejection_reason'] = $this->input->post('reason');

        $this->system_vars->omail($chat['client_email'], 'refund_rejected', $params);

        // Redirect When Done
        $this->session->set_flashdata('response', "The refund has been rejected, and the client has been notified");
        redirect("/frontend/my_account/chats/transcript/{$chat_id}");
    }

    function chat_history($reader_id, $client_id)
    {
        $mysql = $this->db->query("SELECT chats.topic, chats.id
                                    FROM chats
                                    WHERE reader_id='{$reader_id}' AND client_id='{$client_id}' ORDER BY id DESC");
        $topics = $mysql->result_array();
        $client = $this->db->query("SELECT username, first_name FROM members WHERE id='{$client_id}'")->row_array();
        $params['topics'] = $topics;
        $params['client'] = $client;
            /*9-15
              @pen
            */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact - Ban Users",
            'header' => $this->load->view('frontend/layouts/header_authenticated', $param_header, TRUE),
            'slider' => "",
            'content' => $this->load->view('frontend/my_account/chat_history', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function getChatHistory()
    {
        $chat_id = $this->input->post('chat_id');
        $chats = $this->db->query("SELECT members.username, chat_transcripts.message FROM chat_transcripts
                                    LEFT JOIN members
                                    ON members.id = chat_transcripts.member_id
                                    WHERE chat_id='{$chat_id}'    
                                  ")->result_array();
        $return['chats'] = $chats;
        echo json_encode($return);
    }

    function view_chats($reader_id, $client_id, $start_year, $start_month, $start_day, $end_year, $end_month, $end_day)
    {

        $sql = "SELECT * 
                FROM chats 
                WHERE reader_id = {$reader_id} AND client_id  = {$client_id} AND create_datetime > '{$start_year}-{$start_month}-{$start_day} 00:00:00' AND create_datetime <  '{$end_year}-{$end_month}-{$end_day} 23:59:59'
                ORDER BY start_datetime DESC ";
        $getChats = $this->db->query($sql);
        $params['chats'] = $getChats->result_array();
        $params['search'] = 0;
        $params['username_direction'] = 'ASC';
        $params['datetime_direction'] = 'DESC';

        $params['start_year'] = $start_year;
        $params['start_month'] = $start_month;
        $params['start_day'] = $start_day;

        $params['end_year'] = $end_year;
        $params['end_month'] = $end_month;
        $params['end_day'] = $end_day;

        $params['client_id'] = $client_id;
        $params['reader_id'] = $reader_id;

        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/chat_history', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function client_chat_search()
    {
        $search_type = $this->input->post('search_type');
        $username = trim($this->input->post('query'));
        $sql = "SELECT *
                FROM   members m,
                       chats c                 
                WHERE   (c.reader_id = {$this->member->data['id']} OR c.client_id = {$this->member->data['id']})
                        AND (m.id = c.client_id OR m.id = c.reader_id)
                        AND m.id <> 1
                       AND UPPER(m.username)  LIKE UPPER('%" . $username . "%') 
                ORDER BY m.username";

        $getChats         = $this->db->query($sql);
        $params['chats']  = $getChats->result_array();
        $params['search'] = 1;
        $params['username_direction'] = 'ASC';
        $params['datetime_direction'] = 'DESC';

        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/partial/backend/header_client', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/chats', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );
            $data['load_js'] = "frontend/my_account/chats_js";

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);

    }

    function client_short_chat_search()
    {
        $search_type = $this->input->post('search_type');
        $username = trim($this->input->post('query'));

        $sql = "SELECT *
                FROM   members m,
                       chats c                 
                WHERE   (c.reader_id = {$this->member->data['id']} OR c.client_id = {$this->member->data['id']})
                        AND (m.id = c.client_id OR m.id = c.reader_id)
                        AND m.id <> 1
                        AND c.length < 240
                        AND c.start_datetime >= DATE_SUB(CURDATE(), INTERVAL 14 DAY)
                       AND UPPER(m.username)  LIKE UPPER('%" . $username . "%') 
                ORDER BY m.username";
        $getChats = $this->db->query($sql);
        $params['chats'] = $getChats->result_array();
        $params['search'] = 1;
        $params['username_direction'] = 'ASC';
        $params['datetime_direction'] = 'DESC';
        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/layouts/header_authenticated', $param_header, TRUE),
            'slider' => "",
            'content' => $this->load->view('frontend/my_account/chats', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);

    }

    function filter_chats_datetime()
    {
        $direction = $this->input->post('datetime_direction');

        $sql = "SELECT * 
                FROM chats 
                WHERE (reader_id = {$this->member->data['id']} OR client_id  = {$this->member->data['id']}) 
                ORDER BY start_datetime {$direction} ";
        $getChats = $this->db->query($sql);
        $params['chats'] = $getChats->result_array();
        $params['search'] = 1;

        if($direction == 'ASC')
        {
            $params['username_direction'] = 'ASC';
            $params['datetime_direction'] = 'DESC';

        } else
        {
            $params['username_direction'] = 'ASC';
            $params['datetime_direction'] = 'ASC';
        }
        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/layouts/header_authenticated', $param_header, TRUE),
            'slider' => "",
            'content' => $this->load->view('frontend/my_account/chats', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function filter_chats_datetime_searched()
    {
        $direction = $this->input->post('datetime_direction');

        $start_year = $this->input->post('start_year');
        $start_month = $this->input->post('start_month');
        $start_day = $this->input->post('start_day');

        $end_year = $this->input->post('end_year');
        $end_month = $this->input->post('end_month');
        $end_day = $this->input->post('end_day');

        $reader_id = $this->input->post('reader_id');
        $client_id = $this->input->post('client_id');

        $sql = "SELECT * 
                FROM chats 
                WHERE reader_id = {$reader_id} AND client_id  = {$client_id} AND create_datetime > '{$start_year}-{$start_month}-{$start_day} 00:00:00' AND create_datetime <  '{$end_year}-{$end_month}-{$end_day} 23:59:59'
                ORDER BY start_datetime {$direction} ";
        $getChats = $this->db->query($sql);
        $params['chats'] = $getChats->result_array();
        $params['search'] = 0;

        if($direction == 'ASC')
        {
            $params['username_direction'] = 'ASC';
            $params['datetime_direction'] = 'DESC';

        } else
        {
            $params['username_direction'] = 'ASC';
            $params['datetime_direction'] = 'ASC';
        }
        $params['start_year'] = $start_year;
        $params['start_month'] = $start_month;
        $params['start_day'] = $start_day;

        $params['end_year'] = $end_year;
        $params['end_month'] = $end_month;
        $params['end_day'] = $end_day;

        $params['client_id'] = $client_id;
        $params['reader_id'] = $reader_id;
        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/layouts/header_authenticated', $param_header, TRUE),
            'slider' => "",
            'content' => $this->load->view('frontend/my_account/chat_history', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function filter_chats_username()
    {
        $direction = $this->input->post('username_direction');

        if($this->member->data['member_type'] == 'reader')
        {
            $sql = "SELECT chats.*, members.username
                FROM chats 
                LEFT JOIN members
                ON chats.client_id = members.id
                WHERE chats.reader_id = '{$this->member->data['id']}'
                ORDER BY members.username {$direction} ";

        } else if($this->member->data['member_type'] == 'client') {
            $sql = "SELECT chats.*, members.username 
                FROM chats 
                LEFT JOIN members
                ON chats.reader_id = members.id
                WHERE chats.client_id = {$this->member->data['id']}
                ORDER BY members.username {$direction} ";
        }



        $getChats = $this->db->query($sql);
        $params['chats'] = $getChats->result_array();
        $params['search'] = 1;
        if($direction == 'ASC')
        {
            $params['username_direction'] = 'DESC';
            $params['datetime_direction'] = 'DESC';

        } else
        {
            $params['username_direction'] = 'ASC';
            $params['datetime_direction'] = 'DESC';
        }
        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/layouts/header_authenticated', $param_header, TRUE),
            'slider' => "",
            'content' => $this->load->view('frontend/my_account/chats', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }
    function filter_chats_username_searched()
    {
        $direction = $this->input->post('username_direction');

        $start_year = $this->input->post('start_year');
        $start_month = $this->input->post('start_month');
        $start_day = $this->input->post('start_day');

        $end_year = $this->input->post('end_year');
        $end_month = $this->input->post('end_month');
        $end_day = $this->input->post('end_day');

        $reader_id = $this->input->post('reader_id');
        $client_id = $this->input->post('client_id');

        if($this->member->data['member_type'] == 'reader')
        {
            $sql = "SELECT chats.*, members.username
                FROM chats 
                LEFT JOIN members
                ON chats.client_id = members.id
                WHERE chats.reader_id = {$reader_id} AND chats.client_id  = {$client_id} AND create_datetime > '{$start_year}-{$start_month}-{$start_day} 00:00:00' AND create_datetime <  '{$end_year}-{$end_month}-{$end_day} 23:59:59'
                ORDER BY members.username {$direction} ";

        } else if($this->member->data['member_type']) {
            $sql = "SELECT chats.*, members.username 
                FROM chats 
                LEFT JOIN members
                ON chats.reader_id = members.id
                WHERE chats.reader_id = {$reader_id} AND chats.client_id  = {$client_id} AND create_datetime > '{$start_year}-{$start_month}-{$start_day} 00:00:00' AND create_datetime <  '{$end_year}-{$end_month}-{$end_day} 23:59:59'
                ORDER BY members.username {$direction} ";
        }



        $getChats = $this->db->query($sql);
        $params['chats'] = $getChats->result_array();
        $params['search'] = 0;
        if($direction == 'ASC')
        {
            $params['username_direction'] = 'DESC';
            $params['datetime_direction'] = 'DESC';

        } else
        {
            $params['username_direction'] = 'ASC';
            $params['datetime_direction'] = 'DESC';
        }
        $params['start_year'] = $start_year;
        $params['start_month'] = $start_month;
        $params['start_day'] = $start_day;

        $params['end_year'] = $end_year;
        $params['end_month'] = $end_month;
        $params['end_day'] = $end_day;

        $params['client_id'] = $client_id;
        $params['reader_id'] = $reader_id;
        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/layouts/header_authenticated', $param_header, TRUE),
            'slider' => "",
            'content' => $this->load->view('frontend/my_account/chat_history', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function time_back($chat_id)
    {
        $chat_info = $this->db->query("SELECT * FROM chats WHERE id={$chat_id}")->row_array();
        $params['chat'] = $chat_info;
        $params['reader'] = $this->db->query("SELECT * FROM members WHERE id={$this->member->data['id']}")->row_array();
        $params['client'] = $this->db->query("SELECT * FROM members WHERE id={$chat_info['client_id']}")->row_array();

        $reader_post_count = $this->db->query("select count(member_id) from chat_transcripts where chat_id={$chat_info['id']} and member_id={$chat_info['reader_id']}" )->result_array();
        $client_post_count = $this->db->query("select count(member_id) from chat_transcripts where chat_id={$chat_info['id']} and member_id={$chat_info['client_id']}" )->result_array();

        $params['reader_post'] = $reader_post_count[0]['count(member_id)'];
        $params['client_post'] = $client_post_count[0]['count(member_id)'];

        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/time_back', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

          $data['load_js'] = "frontend/my_account/time_back_js";

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function give_time_back()
    {
        
        $time_back_balance = (float)$this->input->post('paid-time');
        $time_length = (float)$this->input->post('paid-time-back-length');
        $time_back_length = $time_back_balance * $time_length/60;
        $chat_id = $this->input->post('chat-id');
        $client_id = $this->input->post('client-id');
        $reader_id = $this->input->post('reader-id');

        $this->db->where('id', $chat_id);
        $this->db->update('chats', array('time_back' => $time_back_balance));


        // charge client account
        $this->member->set_member_id($client_id);
        $region = (trim(strtolower($this->member->data['country']))=='ca' ? "ca" : "us");
        $this->member_funds->fund_account('reading', "regular", $time_back_length, null, $region); // member_balance
        $this->member_funds->insert_transaction('refund', $time_back_length * -1, $region, "Time back For Chat #" . $chat_id); // transactions

        // charge reader account
        $this->member->set_member_id($reader_id);
        $region = (trim(strtolower($this->member->data['country']))=='ca' ? "ca" : "us");
        $this->member_funds->fund_account('reading', "regular", $time_back_length * -1, null, $region); // member_balance
        $this->member_funds->insert_transaction('refund', $time_back_length, $region, "Time back For Chat #" . $chat_id); // transactions
        $this->session->set_flashdata('response', "Succesfully Give Paid Time Back!");
        $this->time_back($chat_id);

    }

    function give_free_time_back()
    {
        $free_time_back_balance = (float)$this->input->post('free-time');
        $free_time_length = (float)$this->input->post('free-time-back-length');
        $free_time_back_length = $free_time_back_balance * $free_time_length/60;
        $chat_id = $this->input->post('chat-id');
        $client_id = $this->input->post('client-id');
        $reader_id = $this->input->post('reader-id');

        $this->db->where('id', $chat_id);
        $this->db->update('chats', array('free_time_back' => $free_time_back_balance));

        // charge client account
        $this->member->set_member_id($client_id);
        $region = (trim(strtolower($this->member->data['country']))=='ca' ? "ca" : "us");
        $this->member_funds->fund_account('reading', "free", $free_time_back_length, null, $region); // member_balance
        $this->member_funds->insert_transaction('refund', $free_time_back_length * -1, $region, "FreeTime back For Chat #" . $chat_id); // transactions

        // charge reader account
        $this->member->set_member_id($reader_id);
        $region = (trim(strtolower($this->member->data['country']))=='ca' ? "ca" : "us");
        $this->member_funds->fund_account('reading', "free", $free_time_back_length * -1, null, $region); // member_balance
        $this->member_funds->insert_transaction('refund', $free_time_back_length, $region, "FreeTime back For Chat #" . $chat_id); // transactions

        $this->session->set_flashdata('response', "Succesfully Give Free Time Back!");
        $this->time_back($chat_id);
        
    }

    function delete_chat($chat_id=null)
    {
        $this->db->where('id', $chat_id);
        $this->db->delete('chats');

        $this->session->set_flashdata("response_delete", "Chat #".$chat_id." has been deleted successfully.");

        $this->index();
    }

    function soft_delete_chat($chat_id=null)
    {
        $this->db->where('id', $chat_id);
        $this->db->update('chats', array('is_client_deleted' => 1));


        $this->session->set_flashdata("response_delete", "Chat #".$chat_id." has been deleted successfully.");
        $this->index();
        
    }

    function download($chat_id, $type)
    {


        if ($type == 1) {

            $this->load->library('m_pdf');
            //now pass the data//
            $this->data['title']="Chat History";
            $this->data['description']="";

            $chats = array();

            $getTranscripts = $this->db->query("SELECT chat_transcripts.*, members.username FROM chat_transcripts LEFT JOIN members ON members.id = chat_transcripts.member_id WHERE chat_id = {$chat_id} ");

            $ta = $getTranscripts->result_array();

            foreach ($ta as $transcript) {
                $chat_history = $transcript['datetime'] . "    " . $transcript['username'] . "  :   " . strip_tags($transcript['message']) . "\n";
                array_push($chats, $chat_history);
            }

            $this->data['description']=$chats;
            //now pass the data //



            $html=$this->load->view('frontend/pdf_out',$this->data, true); //load the pdf_output.php by passing our data and get all data in $html varriable.

            //this the the PDF filename that user will get to download
            $pdfFilePath ="chats-".time()."-download.pdf";


            //actually, you can pass mPDF parameter on this load() function
            $pdf = $this->m_pdf->load();
            //generate the PDF!
            $pdf->WriteHTML($html,2);
            //offer it to user via browser download! (The PDF won't be saved on your server HDD)
            $pdf->Output($pdfFilePath, "D");
        }

        if ($type == 2) {

            $check = $this->db->query("SELECT * FROM chat_transcripts WHERE chat_id = {$chat_id} ")->result_array();

            if (count($check) > 0) {

                $data = "";

                $getTranscripts = $this->db->query("SELECT chat_transcripts.*, members.username FROM chat_transcripts LEFT JOIN members ON members.id = chat_transcripts.member_id WHERE chat_id = {$chat_id} ");

                $ta = $getTranscripts->result_array();

                foreach ($ta as $transcript) {
                    $chat_history = $transcript['datetime'] . "    " . $transcript['username'] . "  :   " . strip_tags($transcript['message']) . "\n";
                    $data = $data . $chat_history;
                }

                $this->load->helper('download');
                $name = 'chat-history.txt';
                force_download($name, $data);
            }
        }
    }
}
