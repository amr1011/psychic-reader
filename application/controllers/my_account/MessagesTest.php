<?php

class messages extends CI_Controller
{

    function __construct()
    {

        parent :: __construct();

        $this->settings = $this->system_vars->get_settings();

        if (!$this->session->userdata('member_logged'))
        {

            $this->session->set_flashdata('error', "You must login before you can gain access to secured areas");
            redirect('/register/login');
            exit;
        }
    }

    function email_test()
    {

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.taroflash.com',
            'smtp_port' => 587,
            'smtp_user' => 'noreply@psychic-contact.com',
            'smtp_pass' => 'NoRep2017*1245Klm1P32124*',
            'mailtype'  => 'html',
            'charset'   => 'iso-8859-1'
        );

        $this->load->library('email', $config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");

        $htmlContent = '<h1>Sending email via SMTP server</h1>';
        $htmlContent .= '<p>This email has sent via SMTP server from CodeIgniter application.</p>';

        $this->email->to('recipient@example.com');
        $this->email->from('sender@example.com','MyWebsite');
        $this->email->subject('How to send email via SMTP server in CodeIgniter');
        $this->email->message($htmlContent);

        //Send email
        $this->email->send();


    }
}
