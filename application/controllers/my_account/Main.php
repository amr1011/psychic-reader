<?php

class main extends CI_Controller
{

    function __construct()
    {

        parent :: __construct();
        $this->load->library('my_common');

        $this->settings = $this->system_vars->get_settings();

        if ($this->my_common->is_on_full_ban()) {
            redirect('/main/logout');
            exit;
        }
        if (!$this->session->userdata('member_logged'))
        {
            $this->session->set_flashdata('error', "You must login before you can gain access to secured areas");
            redirect('/register/login');
            exit;
        }
    }

    function set_status($status)
    {
        if ($status == "break")
        {
            $is_manual = true;
        } else
        {
            $is_manual = false;
        }
        $this->reader->set_status($status, $is_manual);
        if (!$this->input->is_ajax_request()) {
            redirect("/my_account");
        }else{
            die(json_encode($status));
        }
    }

    function setoffline($reader_id){
        
            $this->db->where('id', $reader_id);
            $update = array(
                'status' => 'offline',
                'manual_break_time' => '',
                'break_time' => '',
                'last_pending_time' => '',
                'last_chat_request' => '',
            );
            $this->db->update('member_profiles', $update);

        //$this->reader->set_status('offline', false,$reader_id);
        die(json_encode(true));
    }
    function setoonline($reader_id){
        
            $this->db->where('id', $reader_id);
            $update = array(
                'status' => 'online',
                'manual_break_time' => '',
                'break_time' => '',
                'last_pending_time' => '',
                'last_chat_request' => '',
            );
            $this->db->update('member_profiles', $update);

        //$this->reader->set_status('offline', false,$reader_id);
        die(json_encode(true));
    }

    function set_break_time()
    {
        $ReaderBreakTime = $this->input->post('ReaderBreakTime');
        if ($ReaderBreakTime!=0) {
            $this->reader->set_break_time($ReaderBreakTime);
        }
        redirect("/my_account");

        // if ($status == "break")
        // {
        //     $is_manual = true;
        // } else
        // {
        //     $is_manual = false;
        // }
        // $this->reader->set_status($status, $is_manual);
        // 
    }

    function index()
    {


        $now = date("Y-m-d H:i:s");
        if ($this->member->data['profile_id'])
        {
            $reader_status = $this->member->data['status'];


            $query = "SELECT status, manual_break_time, last_chat_request, last_pending_time, break_time FROM member_profiles WHERE id=" . intval($this->member->data['profile_id']);
            $result = $this->db->query($query);


            $row = $result->row_array();

            if ($row['status'] == "online")
            {
                if (strtotime($row['last_chat_request']) < strtotime($row['last_pending_time']))
                {
                    // then just consider the pending time. 
                    if (strtotime($now) - strtotime($row['last_pending_time']) < CHAT_MAX_PENDING)
                    {
                        $reader_status = "online";
                    }
                } else
                {

                    if (strtotime($now) - strtotime($row['last_chat_request']) < CHAT_MAX_WAIT)
                    {
                        $reader_status = "online";
                    }
                }

                if (strtotime($now) < strtotime($row['break_time']))
                {
                    $reader_status = "break";
                }
            }
            $this->member->data['status'] = $reader_status;

            /*9-15
              @pen
            */
            $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
            $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

            $data = array(
                'title' => "Psychic Contact",
                'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
                'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
                'content' => $this->load->view('frontend/my_account/dashboard', '', TRUE),
                'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
            );
            $data['load_js'] = "frontend/my_account/dashboard_js";

            $this->load->library('parser');        
            $this->parser->parse('frontend/layouts/dashboard', $data);

        } else
        {
            /*9-15
              @pen
            */
            $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
            
            $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();
            $data = array(
                'title' => "Psychic Contact",
                'header' => $this->load->view('frontend/partial/backend/header_client', $param_header, TRUE),
                'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
                'content' => $this->load->view('frontend/my_account/main_client', '', TRUE),
                'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
            );

            $this->load->library('parser');        
            $this->parser->parse('frontend/layouts/dashboard', $data);
        }
    }

    function client_list()
    {
        $params['clients'] = $this->reader->get_clients();
        $params['year'] = date('Y');
        $params['username_direction'] = 'ASC';
        $params['firstname_direction'] = 'ASC';
        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();
        $data = array(
            'title' => "Psychic Contact - Client List",
            'header' => $this->load->view('frontend/partial/backend/header_client', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/client_list', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );
        $this->load->library('parser');
        $data['load_js'] = "frontend/my_account/client_list_js";

        $this->parser->parse('frontend/layouts/dashboard', $data );
    }

    function blog_management()
    {
        $params['blogs'] = $this->db->query("SELECT * FROM blogs WHERE reader_id='{$this->member->data['id']}'")->result_array();

        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();
        $data = array(
            'title' => "Psychic Contact - Client List",
            'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/blog', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );
        $data['load_js'] = "frontend/my_account/blog_js";
        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function blog_edit($blog_id = null)
    {
        if($blog_id == null)
        {
            // add new blog
            $params['blog'] = null;
            $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
            $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();
            $data = array(
                'title' => "Psychic Contact - Client List",
                'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
                'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),    
                'content' => $this->load->view('frontend/my_account/blog_form', $params, TRUE),
                'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
            );

            $this->load->library('parser');
            $this->parser->parse('frontend/layouts/dashboard', $data);
        }
        else
        {
            // edit blog
            $blog = $this->db->query("SELECT * FROM blogs WHERE id='{$blog_id}'")->result_array();
            $params['blog'] = $blog[0];
            $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
            $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();
            $data = array(
                'title' => "Psychic Contact - Client List",
                'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
                'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),    
                'content' => $this->load->view('frontend/my_account/blog_form', $params, TRUE),
                'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
            );
            $data['load_js'] = "frontend/my_account/blog_form_js";
            $this->load->library('parser');
            $this->parser->parse('frontend/layouts/dashboard', $data);
        }
    }

    function blog_submit()
    {
        $blog_data = [];

        if (trim($_FILES['blog_image']['tmp_name']))
        {
            //--- Set unlimited memory for the GD pocess
            ini_set('memory_limit', '-1');

            // Configure the image uploading
            $config['upload_path'] = $_SERVER["DOCUMENT_ROOT"] . "/media/assets";
            $config['allowed_types'] = 'gif|jpg|png';
            $config['file_name'] = 'blog_img_' . time();

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('blog_image'))
            {
                $this->session->set_flashdata('error', "Error uploading profile image: " . $this->upload->display_errors());
            } else
            {

                $file = $this->upload->data();
                $blog_data['image'] = $file['file_name'];

                //--- Fit the new image
                $config['protocol'] = 'gd2';
                $config['source_image'] = $file['full_path'];
                $config['width'] = 200;

                $this->load->library('image_lib', $config);

                if (!$this->image_lib->fit())
                {

                    die("Image FIT error: " . $this->upload->display_errors());
                } else
                {

                }
            }
        }

        // if (trim($_FILES['blog_doc']['tmp_name']))
        // {
        //     //--- Set unlimited memory for the GD pocess
        //     ini_set('memory_limit', '-1');
        //
        //     // Configure the image uploading
        //     $config1['upload_path'] = $_SERVER["DOCUMENT_ROOT"] . "/media/assets";
        //     $config1['allowed_types'] = 'docx|doc|DOC|DOCX';
        //     $config1['file_name'] = 'blog_doc_' . time();
        //
        //     $this->load->library('upload');
        //     $this->upload->initialize($config1);
        //
        //     if (!$this->upload->do_upload('blog_doc'))
        //     {
        //         $this->session->set_flashdata('error', "Error uploading profile image: " . $this->upload->display_errors());
        //     } else
        //     {
        //
        //         $file = $this->upload->data();
        //         $blog_data['document'] = $file['file_name'];
        //
        //         //--- Fit the new image
        //         $config['protocol'] = 'gd2';
        //         $config['source_image'] = $file['full_path'];
        //         $config['width'] = 200;
        //
        //         $this->load->library('image_lib', $config);
        //
        //         if (!$this->image_lib->fit())
        //         {
        //
        //             die("Image FIT error: " . $this->upload->display_errors());
        //         } else
        //         {
        //
        //         }
        //     }
        // }

        $blog_data['title'] = set_value('title');
        $blog_data['content'] = set_value('content');

        $blog_data['reader_id'] = $this->member->data['id'];
        $blog_data['username'] = $this->member->data['username'];
        $blog_data['date'] = date("Y-m-d");

        $blog_data['category'] = set_value('categories');

        $blog_id = set_value('blog_id');
        if($blog_id != '')
        {
            $this->db->where('id', $blog_id);
            $this->db->update('blogs', $blog_data);
        } else {
            $this->db->insert('blogs', $blog_data);
        }

        $this->blog_management();

    }
    /*9-2*/
    function note_list($reader_id)
    {
        $params['notes'] = $this->reader->get_note($reader_id);
        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact - Client List",
            'header' => $this->load->view('frontend/partial/backend/header_client', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/note_list', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );
        $data['load_js'] = "frontend/my_account/note_list_js";

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    /*9-4*//*add a new php file named appointment_list.php*/
    function appointment_list($reader_id)
    {

        $params['appointments'] = $this->reader->get_appointment($reader_id);
        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact - Client List",
            'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),   
            'content' => $this->load->view('frontend/my_account/appointment_list', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $data['load_js'] = "frontend/my_account/appointment_list_js";        

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function give_back($reader_id)
    {

        $params['reader'] = $this->reader->get_reader_info($reader_id);
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact - Client List",
            'header' => $this->load->view('frontend/partial/backend/header_client', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/give_back', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );
        $data['load_js'] = "frontend/my_account/give_back_js";

        $this->load->library('parser');        
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function give_back_history($reader_id)
    {
        $params['gifts'] = $this->reader->get_gift_history($reader_id);
       
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact - Client List",
            'header' => $this->load->view('frontend/partial/backend/header_client', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/give_back_history', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );
        $data['load_js'] = "frontend/my_account/give_back_history_js";

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function nrr($chat_id = null)
    {
        $this->load->model("chatmodel");
        if (!$chat_id)
        {
            $params['chat'] = null;
        } else
        {
            $this->chatmodel->setSession($chat_id);
            $params['chat'] = $this->chatmodel->object;
        }


        $params['readers'] = $this->member->get_used_readers();


        foreach($params['readers'] as $key=> $reader)
        {
            $chats = $this->member->get_chats($reader['id']);
            $count = 0;
            foreach($chats as $chat)
            {
                // $chat_logon_time = strtotime($chat['create_datetime']);
                // $now_time = strtotime("now");
                // var_dump($chat);
                // var_dump($chat_logon_time);
                // exit;
                // $duration = ($now_time - $chat_logon_time)/(3600);

                if($chat['length'] < 72)
                {
                    $count = $count +1;
                } else {

                }
            }
            if($count == 0)
            {
                unset( $params['readers'][$key]);
            }
        }

      
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact - Client List",
            'header' => $this->load->view('frontend/partial/backend/header_client', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/nrr_form', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $data['load_js'] = "frontend/my_account/nrr_form_js";

        $this->load->library('parser');        
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function nrr_submit()
    {

        $insert['reader_id'] = set_value('reader');
        $insert['member_id'] = $this->member->data['id'];

        $insert['chat_id'] = set_value('chat');

        $insert['type'] = set_value('type');

        // $insert['slow_timeback'] = set_value('slow_timeback');

        $insert['disconnect'] = set_value('disconnect');
        // $insert['disconnect_timeback'] = set_value('disconnect_timeback');

        $insert['unhappy'] = set_value('unhappy');
        // $insert['unhappy_timeback'] = set_value('unhappy_timeback');

        $time_back = floatval(set_value('slow_timeback')) + floatval(set_value('disconnect_time_back')) + floatval(set_value('unhappy_timeback'));

        if($time_back > floatval(set_value('max-length')))
        {
            $insert['time_back'] = floatval(set_value('max-length'));
        }
        else
        {
            $insert['time_back'] = $time_back;
        }

        $insert['suggest'] = set_value('suggest');

        $insert['date'] = date('Y-m-d G:i:s');

        $this->db->insert("nrr", $insert);

        $this->session->set_flashdata("response", "NRR Request has been submitted successfully.");

        redirect("/my_account/main/nrr");

    }

    function nrr_chat_session($reader_id = null)
    {
        $chats = $this->member->get_chats($reader_id);

        echo json_encode($chats);
    }

    function check_email_price($string = null)
    {

        if ($this->input->post('available_for_email') == '1')
        {

            if ($string)
            {

                return true;
            } else
            {

                $this->form_validation->set_message('check_email_price', 'Please enter a cost per email');
                return false;
            }
        } else
        {

            return true;
        }
    }

    function testimonials()
    {
        $params['testis'] = $this->reader->get_testimonials();

        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/testimonials', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function testimonial_toggle($tid)
    {
        $testi = $this->reader->get_testimonials($tid);

        if ($testi['reader_approved'] == '1')
        {
            $update['reader_approved'] = 0;
        } else
        {
            $update['reader_approved'] = 1;
        }

        $this->db->where("id", $tid);
        $this->db->update("testimonials", $update);


        redirect("/my_account/main/testimonials");
    }

    public function edit_profile() 
    {
        $getCategories = $this->db->query("SELECT * FROM categories ORDER BY title ");
        $params['categories'] = $getCategories->result_array();

        if (!empty($_POST['categories']))
        {
            $registeredCategories = $_POST['categories'];
        } else
        {
            $getRegisteredCategories = $this->db->query("SELECT * FROM profile_categories WHERE profile_id = {$this->member->data['id']} ");
            $registeredCategories = array();

            foreach ($getRegisteredCategories->result_array() as $c) {
                $registeredCategories[] = $c['category_id'];
            }
        }

        $params['registered_categories'] = $registeredCategories;        

        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact - Edit Profile",
            'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/edit_profile', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $data['load_js'] = "frontend/my_account/edit_profile_js";
        $this->load->library('parser');        
        $this->parser->parse('frontend/layouts/dashboard', $data);

    }

    function checkEmailEnabled($val = null)
    {
        if ($this->input->post('enable_email') == '1')
        {
            if (!trim($val))
            {
                $this->form_validation->set_message('checkEmailEnabled', "Please answer the total number of days to answer a question via email.");
                return false;
            } else
            {
                return true;
            }
        } else
        {
            return true;
        }
    }

    function save_changed_profile()
    {
        $update = array();

        // save categories without approval requirement
        $categories = $this->input->post('categories');
        if(count($categories) >0 )
        {
            foreach($categories as $category) {

                $check_category = count($this->db->query("SELECT id FROM profile_categories WHERE profile_id={$this->member->data['id']} AND category_id={$category}")->result_array());
                if($check_category > 0) {
                    continue;
                }
                $data = array(
                    'profile_id' => $this->member->data['id'],
                    'category_id' => $category
                );
                $this->db->insert('profile_categories', $data);
            }
        }


        $db_categories = $this->db->query("SELECT category_id FROM profile_categories")->result_array();

        foreach($db_categories as $db_category) // if admin deleted category so if some categories does not exist
        {
            if($categories != null)
            {
                if(!in_array($db_category['category_id'], $categories))
                {
                    $this->db->where('category_id', (int)$db_category['category_id']);
                    $this->db->delete('profile_categories');
                }
            }
        }
        $this->edit_profile();

        // save password without approval requirement
        if (set_value('password'))
        {
            $update['password'] = $this->bcrypt->hash_password(set_value('password'));
            $this->db->where('id', $this->member->data['id']);
            $this->db->update('members', $update);
        }

        $email = array('email'=>set_value('email'));
        $this->db->where('id', $this->member->data['id']);
        $this->db->update('members', $email);

        // $this->db->where('id', $this->member->data['id']);
        // $this->db->update('members', $update);

        // get profile data
        $dob = set_value('dob_year') . "-" . set_value('dob_month') . "-" . set_value('dob_day');

        $mem_update = array();

        // profile image
        if (trim($_FILES['profile_image']['tmp_name']))
        {
            //--- Set unlimited memory for the GD pocess
            ini_set('memory_limit', '-1');

            // Configure the image uploading
            $config['upload_path'] = $_SERVER["DOCUMENT_ROOT"] . "/media/assets/";
            $config['allowed_types'] = 'gif|jpg|png';
            $config['file_name'] = time();

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('profile_image'))
            {
                $this->session->set_flashdata('error', "Error uploading profile image: " . $this->upload->display_errors());
            } else
            {

                $file = $this->upload->data();
                $mem_update['profile_image'] = $file['file_name'];

                //--- Fit the new image
                $config['protocol'] = 'gd2';
                $config['source_image'] = $file['full_path'];
                $config['width'] = 200;

                $this->load->library('image_lib', $config);

                if (!$this->image_lib->fit())
                {

                    die("Image FIT error: " . $this->upload->display_errors());
                } else
                {
                    // Remove old profile image here
                    $old_profile_path = trim("./media/assets/{$this->member->data['profile_image']}");

                    if (!empty($this->member->data['profile_image']))
                    {
                        if (file_exists($old_profile_path))
                        {
                            unlink($old_profile_path);
                        }
                    }
                }
            }
        }

        // $mem_update['first_name'] = set_value('first_name');
        // $mem_update['last_name'] = set_value('last_name');
        $mem_update['gender'] = set_value('gender');
        $mem_update['dob'] = $dob;
        $mem_update['country'] = set_value('country');
        $mem_update['paypal_email'] = set_value('paypal_email');
        $mem_update['member_id'] = $this->member->data['id'];
        $mem_update['username'] = $this->member->data['username'];
        $mem_update['title'] = set_value('title');

        $mem_update['biography'] = set_value('biography');
        $mem_update['area_of_expertise'] = set_value('area_of_expertise');

        $mem_update['enable_email'] = set_value('enable_email');
        $mem_update['email_total_days'] = set_value('email_total_days');


        $this->db->insert('changed_member_profiles', $mem_update);

        // $this->load->model("messages_model");
        // $this->messages_model->sendAdminMessage($this->member->data['id'], "Profile Edited", "Profile #" . $this->member->data['id'] . " updated.");

        // Redirect with success
        $this->session->set_flashdata('response', "Your expert profile has been submitted for approval!");

        redirect("/my_account");
    }

    function save_profile()
    {
        $this->form_validation->set_rules('paypal_email', 'Paypal Email', 'trim');
        $this->form_validation->set_rules('password', 'Password', 'trim|matches[password2]');
        $this->form_validation->set_rules('password2', 'Re-Type Password', 'trim|matches[password]');
        $this->form_validation->set_rules('first_name', 'First Name', 'required|trim');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required|trim');
        $this->form_validation->set_rules('gender', 'Gender', 'required|trim');
        $this->form_validation->set_rules('dob_month', 'Month of Birth', 'required|trim');
        $this->form_validation->set_rules('dob_day', 'Day of Birth', 'required|trim');
        $this->form_validation->set_rules('dob_year', 'Year of Birth', 'required|trim');
        $this->form_validation->set_rules('country', 'Country', 'required|trim');

        $this->form_validation->set_rules('title', 'Title', 'required|trim');
        $this->form_validation->set_rules('biography', 'Biography', 'required|trim');
        $this->form_validation->set_rules('area_of_expertise', 'Area of Expertise', 'required|trim');
        $this->form_validation->set_rules('categories[]', "Categories", "required");

        $this->form_validation->set_rules('enable_email', 'Experience', 'trim');
        $this->form_validation->set_rules('email_total_days', 'Experience', 'trim|callback_checkEmailEnabled');

        if (!$this->form_validation->run())
        {
            $categories = $this->input->post('categories');
            if(count($categories) >0 )
            {
                foreach($categories as $category) {

                    $check_category = count($this->db->query("SELECT id FROM profile_categories WHERE profile_id={$this->member->data['id']} AND category_id={$category}")->result_array());
                    if($check_category > 0) {
                        continue;
                    }
                    $data = array(
                        'profile_id' => $this->member->data['id'],
                        'category_id' => $category
                    );
                    $this->db->insert('profile_categories', $data);
                }
            }


            $db_categories = $this->db->query("SELECT category_id FROM profile_categories")->result_array();

            foreach($db_categories as $db_category)
            {
                if($categories != null)
                {
                    if(!in_array($db_category['category_id'], $categories))
                    {
                        $this->db->where('category_id', (int)$db_category['category_id']);
                        $this->db->delete('profile_categories');
                    }
                }
            }
            $this->edit_profile();
        } else
        {
            $dob = set_value('dob_year') . "-" . set_value('dob_month') . "-" . set_value('dob_day');

            // Profle Image
            $mem_update = array();

            if (trim($_FILES['profile_image']['tmp_name']))
            {
                //--- Set unlimited memory for the GD pocess
                ini_set('memory_limit', '-1');

                // Configure the image uploading
                $config['upload_path'] = $_SERVER["DOCUMENT_ROOT"] . "/media/assets/";
                $config['allowed_types'] = 'gif|jpg|png';
                $config['file_name'] = time();

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('profile_image'))
                {
                    $this->session->set_flashdata('error', "Error uploading profile image: " . $this->upload->display_errors());
                } else
                {

                    $file = $this->upload->data();
                    $mem_update['profile_image'] = $file['file_name'];

                    //--- Fit the new image
                    $config['protocol'] = 'gd2';
                    $config['source_image'] = $file['full_path'];
                    $config['width'] = 200;

                    $this->load->library('image_lib', $config);

                    if (!$this->image_lib->fit())
                    {

                        die("Image FIT error: " . $this->upload->display_errors());
                    } else
                    {
                        // Remove old profile image here
                        $old_profile_path = trim("./media/assets/{$this->member->data['profile_image']}");

                        if (!empty($this->member->data['profile_image']))
                        {
                            if (file_exists($old_profile_path))
                            {
                                unlink($old_profile_path);
                            }
                        }
                    }
                }
            }

            // Create record            
            if (set_value('password'))
            {
                $mem_update['password'] = $this->bcrypt->hash_password(set_value('password'));
            }
            
            $mem_update['first_name'] = set_value('first_name');
            $mem_update['last_name'] = set_value('last_name');
            $mem_update['gender'] = set_value('gender');
            $mem_update['dob'] = $dob;
            $mem_update['country'] = set_value('country');
            $mem_update['paypal_email'] = set_value('paypal_email');

            $this->db->where('id', $this->member->data['id']);
            $this->db->update('members', $mem_update);

            $update['id'] = $this->member->data['id'];
            $update['title'] = set_value('title');

            $update['biography'] = set_value('biography');
            $update['area_of_expertise'] = set_value('area_of_expertise');

            $update['enable_email'] = set_value('enable_email');
            $update['email_total_days'] = set_value('email_total_days');

            // Check to see if profile exists
            // If does NOT exist, add a new one
            if ($this->member->data['profile_id'])
            {
                $this->db->where('id', $this->member->data['profile_id']);
                $this->db->update('profiles', $update);
                unset($update['id']);
                $this->db->where('id', $this->member->data['profile_id']);
                $this->db->update('member_profiles', $update);

            } else
            {
                $this->db->insert('profiles', $update);
                $this->db->insert('member_profiles', $update);
                $this->member->data['profile_id'] = $this->db->insert_id();
            }

            // Delete ALL current subcategories before adding new ones so we dont' create duplicates
            $this->db->where('profile_id', $this->member->data['profile_id']);
            $this->db->delete('profile_categories');

            // Insert subcategories
            foreach ($this->input->post('categories') as $category_id) {

                if (trim($category_id) && is_numeric($category_id))
                {

                    $insert = array();
                    $insert['profile_id'] = $this->member->data['profile_id'];
                    $insert['category_id'] = $category_id;

                    $this->db->insert('profile_categories', $insert);
                }
            }

            $this->load->model("messages_model");
            $this->messages_model->sendAdminMessage($this->member->data['id'], "Profile Edited", "Profile #" . $this->member->data['id'] . " updated.");

            // Redirect with success
            $this->session->set_flashdata('response', "Your expert profile has been saved!");

            redirect("/my_account");
        }
    }

    function client_search()
    {
        $params['clients'] = $this->reader->get_clients($this->input->post('search_type'),trim($this->input->post('query')));
        $params['year'] = date('Y');
        $params['username_direction'] = 'ASC';
        $params['firstname_direction'] = 'ASC';
        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact - Ban Users",
            'header' => $this->load->view('frontend/layouts/header_authenticated', $param_header, TRUE),
            'slider' => "",
            'content' => $this->load->view('frontend/my_account/client_list', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }
    function get_state()
    {
        $country_code = $this->input->post('country_code');
        $country = $this->db->query("SELECT id FROM countries WHERE code='{$country_code}' AND name IS NOT NULL")->row_array();
        $states = $this->db->query("SELECT * FROM states WHERE country_id='{$country['id']}'")->result_array();

        $return['states'] = $states;
        echo json_encode($return);

    }

    function get_city()
    {
        $state_id = $this->input->post('state_id');
        $cities = $this->db->query("SELECT * FROM cities WHERE state_id='{$state_id}'")->result_array();

        $return['cities'] = $cities;
        echo json_encode($return);
    }

    function filter_client_list_username()
    {
        $direction = $this->input->post('username_direction');

        $sql = "SELECT DISTINCT m.first_name, m.last_name, m.username, m.id as mid, last_login_date
                FROM   members m,
                       chats c                 
                WHERE   c.reader_id = {$this->member->data['id']}
                        AND m.id = c.client_id
                        AND m.id <> 1
                ORDER BY m.username {$direction}";

        $clients = $this->db->query($sql);

        $params['clients'] = $clients->result_array();

        if($direction == 'ASC')
        {
            $params['username_direction'] = 'DESC';
            $params['firstname_direction'] = 'ASC';

        } else
        {
            $params['username_direction'] = 'ASC';
            $params['firstname_direction'] = 'ASC';
        }

        $params['year'] = date('Y');
        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();
        $data = array(
            'title' => "Psychic Contact - Client List",
            'header' => $this->load->view('frontend/partial/backend/header_client', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/client_list', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function filter_client_list_firstname()
    {
        $direction = $this->input->post('firstname_direction');

        $sql = "SELECT DISTINCT m.first_name, m.last_name, m.username, m.id as mid, last_login_date
                FROM   members m,
                       chats c                 
                WHERE   c.reader_id = {$this->member->data['id']}
                        AND m.id = c.client_id
                        AND m.id <> 1
                ORDER BY m.first_name {$direction}";

        $clients = $this->db->query($sql);

        $params['clients'] = $clients->result_array();

        if($direction == 'ASC')
        {
            $params['firstname_direction'] = 'DESC';
            $params['username_direction'] = 'ASC';

        } else if($direction == 'DESC')
        {
            $params['username_direction'] = 'ASC';
            $params['firstname_direction'] = 'ASC';
        }

        $params['year'] = date('Y');
        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();
        $data = array(
            'title' => "Psychic Contact - Client List",
            'header' => $this->load->view('frontend/partial/backend/header_client', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/client_list', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function page_readers()
    {
        $params = array();
        $params['page'] = '0';
        $params['readers'] = $this->readers->getAll();
        $params['client'] = $this->db->query("SELECT * FROM members WHERE id={$this->member->data['id']}")->row_array();

        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/partial/backend/header_client', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'slider' => "",
            'content' => $this->load->view('frontend/my_account/page_readers', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );
        $data['load_js'] = "frontend/my_account/page_readers_js";

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function favorite_readers()
    {
        $params = array();
        $params['readers'] = $this->readers->getAll();
        $params['client'] = $this->db->query("SELECT * FROM members WHERE id={$this->member->data['id']}")->row_array();

        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/layouts/header', $param_header, TRUE),
            'slider' => "",
            'content' => $this->load->view('frontend/psychics/page_readers', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/page', $data);
    }

    function set_favorite()
    {
        $client_id = $this->input->post('client');
        $reader_id = $this->input->post('reader');

        $client_name = $this->db->query("SELECT username FROM members WHERE id='{$client_id}'")->result_array();
        $client_name = $client_name[0]['username'];

        $reader_name = $this->db->query("SELECT username FROM members WHERE id='{$reader_id}'")->result_array();
        $reader_name = $reader_name[0]['username'];

        $data = array(
            'client_id' => $client_id,
            'reader_id' => $reader_id
        );
        $this->db->insert('favorite_reader', $data);

        // NoBo to Reader
        $member = $this->member->get($reader_id);
        $member['type'] = 'email';
        $member['description'] = "<p>$client_name set you to Favorite.</p>";
        $this->system_vars->m_omail($member['id'], 'favorite_reader', $member);

        redirect("/profile/".$reader_name);
    }

    function un_set_favorite()
    {
        $client_id = $this->input->post('client');
        $reader_id = $this->input->post('reader');
        $reader_name = $this->db->query("SELECT username FROM members WHERE id='{$reader_id}'")->result_array();
        $reader_name = $reader_name[0]['username'];
        $data = array(
            'client_id' => $client_id,
            'reader_id' => $reader_id
        );
        $this->db->where($data);
        $this->db->delete('favorite_reader');
        redirect("/profile/".$reader_name);
    }

    function save_option()
    {
        $options = $this->input->post('option_data');
        $member_id = $this->member->data['id'];

        $check = $this->db->query("SELECT id FROM favorite_option WHERE client_id='{$member_id}'")->result_array();
        $check = count($check);
        if($check == 0)
        {
            $data = array(
                'client_id'=>$member_id,
                'options'=>serialize($options)
            );
            $this->db->where('client_id', $member_id);
            $this->db->insert('favorite_option', $data);
        }
        else
        {
            $data = array(
                'options'=>serialize($options)
            );
            $this->db->where('client_id', $member_id);
            $this->db->update('favorite_option', $data);
        }
        $return['cities'] = 'asdfasdf';
        echo json_encode($return);
    }

    function get_saved_option()
    {
        $member_id = $this->member->data['id'];
        $options = $this->db->query("SELECT * FROM favorite_option WHERE client_id='{$member_id}'")->result_array();
        if(count($options) > 0)
        {
            $options = unserialize($options[0]['options']);

            $return['data'] = $options;
            echo json_encode($return);
        }
        else
        {
            $return['data'] = null;
            echo json_encode($return);
        }
    }

    function testimonial($reader_id=null)
    {
        $params = array();
        $params['readers'] = $this->member->get_used_readers();
        $params['reader_selected'] = $reader_id;
        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/testimonial_form', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $data['load_js'] = "frontend/my_account/testimonial_form_js";

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function submit_testimonial()
    {
        $reader_id = set_value('reader-id');
        $content = set_value('content');
        $rating = set_value(('score'));

        $data = array(
            'datetime' => date('Y-m-d H:i:s'),
            'member_id' => $this->member->data['id'],
            'reader_id' => $reader_id,
            'rating' => $rating,
            'review' => $content,
        );
        if($content == '') {

            $this->session->set_flashdata('testimonial_content_required', "Please Add Content");
            redirect('/my_account/main/testimonial');
        }

        $this->db->insert('testimonials', $data);
        $this->session->set_flashdata('testimonial_submit', "Thank you for your feedback");
        redirect("/my_account");

    }

    function short_chats()
    {
        $sql = "SELECT * 
                FROM chats 
                WHERE (reader_id = {$this->member->data['id']} OR client_id  = {$this->member->data['id']}) AND client_id <> 1 AND length < 240 AND start_datetime >= DATE_SUB(CURDATE(), INTERVAL 14 DAY)
                ORDER BY start_datetime DESC ";
        $getChats = $this->db->query($sql);
        $params['chats'] = $getChats->result_array();
        $params['search'] = 1;
        $params['username_direction'] = 'ASC';
        $params['datetime_direction'] = 'DESC';
        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/short_chats', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );
        $data['load_js'] = "frontend/my_account/short_chats_js";
        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function request_short_chat_payment()
    {
        $data = $this->input->post();
        $message = "<div>
                        <p>Date: {$data['date']}</p>
                        <p>Chat ID: {$data['chat-id']}</p>
                        <p>Client First Name: {$data['first-name']}</p>
                        <p>Client User Name: {$data['username']}</p>
                        <p>Comments {$data['comments']}</p>
                        <p>Reason: {$data['reason']}</p>
                    </div>";

        $message_data = array(
            'datetime' => date('Y-m-d H:i:s'),
            'sender_id' => $this->member->data['id'],
            'ricipient_id' => 1,
            'name' => null,
            'subject' => 'Payment Request For Short Session',
            'message' => $message,
            'read' => 0,
            'type' => NULL
        );

        $this->db->insert('messages', $message_data);
        redirect('/my_account/main/short_chats');
    }

    function testimonial_list()
    {
        $params = array();
        $params['testis'] = $this->member->get_submitted_testimonials();

        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),

            'content' => $this->load->view('frontend/my_account/testis_list', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/page', $data);
    }

    public function profile($name)
    {

        // $params['testis'] = $this->reader->get_testimonials(null, 1);
            $this->load->model('reader');
            $params['reader']  = $this->reader->get_reader_name($name);

            if($this->member->data['member_type'] == 'client' )
            {
                $params['favorite'] = 0;
                $check_favorite_reader = $this->db->query("SELECT id FROM favorite_reader WHERE client_id={$this->member->data['id']} AND reader_id={$this->reader->reader_id}")->result_array();
                if(count($check_favorite_reader) > 0)
                {
                    $params['favorite'] = 1;
            }
        }
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();
        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/partial/backend/header_client', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/profile', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $data['load_js'] = "frontend/my_account/nrr_form_js";

        $this->load->library('parser');        
        $this->parser->parse('frontend/layouts/dashboard', $data);        
    }

    function getBalance(){

        $id = $this->member->data['id'];
        $region = $this->input->post('region');

         if ($region == "") {
            $region = "us";
        }
        else{
            $region = $this->input->post('region');

        }
       
       $this->load->model('reader');
       $data = $this->reader->get_reader_balance($id,$region);

        $response = array(
            "status" => true,
            "message" => "",
            "data" => $data
        );

        header("Content-Type:application/json;");
        echo json_encode($response);
    }

     function invoice($chat_id = null)
    {
        

        $params['readers'] = $this->member->get_used_readers();


        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact - Client List",
            'header' => $this->load->view('frontend/partial/backend/header_client', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/invoice', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $data['load_js'] = "frontend/my_account/nrr_form_js";

        $this->load->library('parser');        
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }
}
