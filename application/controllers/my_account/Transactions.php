<?php
require $_SERVER["DOCUMENT_ROOT"] . '/paypal/autoload.php';

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\ExecutePayment;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

class transactions extends CI_Controller
{

    function __construct()
    {

        parent :: __construct();

        $this->settings = $this->system_vars->get_settings();

        if (!$this->session->userdata('member_logged'))
        {

            $this->session->set_flashdata('error', "You must login before you can gain access to secured areas");
            redirect('/register/login');
            exit;
        }
    }

    function index($type = 'purchase')
    {
        $query = "SELECT * FROM transactions WHERE member_id = {$this->member->data['id']} ORDER BY datetime DESC";
        $getTransactions = $this->db->query($query);
        $params['transactions'] = $getTransactions->result_array();

        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/transactions', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }
    
    function time_back()
    {
        $params['clients'] = $this->reader->get_clients();
        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact - Time Back",
            'header' => $this->load->view('frontend/layouts/header_authenticated', $param_header, TRUE),
            'slider' => "",
            'content' => $this->load->view('frontend/my_account/transactions/time_back', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');        
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function get_billing_profiles($package_id = null)
    {

        // The billing profiles depend on package price, get package then pass the price.
        $package = $this->site->get_package($package_id);

        // Get billing profiles by package price
        $billing_profiles = $this->member_billing->get_billing_profiles($package['price']);

        // Get Merchant
        $merchantType = $this->member_billing->check_merchant_type($package['price']);


        if (!$billing_profiles)
        {

            $array = array();
            $array['error'] = '1';
            $array['merchant'] = $merchantType;
            $array['redirect'] = "/frontend/my_account/transactions/add_billing_profile/{$merchantType}";
        } else
        {

            $array = array();
            $array['error'] = "0";
            $array['merchant'] = $merchantType;
            $array['profiles'] = $billing_profiles;
        }

        echo json_encode($array);
    }

    function get_billing_profiles_jsonp($package_id = null)
    {

        // The billing profiles depend on package price, get package then pass the price.
        $package = $this->site->get_package($package_id);

        // Get billing profiles by package price
        $billing_profiles = $this->member_billing->get_billing_profiles($package['price']);

        // Get Merchant
        $merchantType = $this->member_billing->check_merchant_type($package['price']);


        if (!$billing_profiles)
        {

            $array = array();
            $array['error'] = '1';
            $array['merchant'] = $merchantType;
            $array['redirect'] = "/frontend/my_account/transactions/add_billing_profile/{$merchantType}";
        } else
        {

            $array = array();
            $array['error'] = "0";
            $array['merchant'] = $merchantType;
            $array['profiles'] = $billing_profiles;
        }

        echo "processJSON(" . json_encode($array) . ")";
    }

    function payments()
    {

        $getTransactions = $this->db->query("SELECT * FROM transactions WHERE type = 'payment' AND member_id = {$this->member->data['id']} ORDER BY datetime DESC");
        $t['transactions'] = $getTransactions->result_array();
        $t['title'] = "Transactions - Payments";

        $t['balance'] = $this->system_vars->member_balance($this->member->data['id']);

        $this->load->view('frontend/partial/header');
        $this->load->view('frontend/my_account/header');
        $this->load->view('frontend/my_account/transactions', $t);
        $this->load->view('frontend/my_account/footer');
        $this->load->view('frontend/partial/footer');
    }

    function deposits()
    {

        $getTransactions = $this->db->query("SELECT * FROM transactions WHERE type = 'deposit' AND member_id = {$this->member->data['id']} ORDER BY datetime DESC");
        $t['transactions'] = $getTransactions->result_array();
        $t['title'] = "Transactions - Deposits";

        $t['balance'] = $this->system_vars->member_balance($this->member->data['id']);

        $this->load->view('frontend/partial/header');
        $this->load->view('frontend/my_account/header');
        $this->load->view('frontend/my_account/transactions', $t);
        $this->load->view('frontend/my_account/footer');
        $this->load->view('frontend/partial/footer');
    }

    function request_payout()
    {

        $t['balance'] = $this->system_vars->member_balance($this->member->data['id']);

        $this->load->view('frontend/partial/header');
        $this->load->view('frontend/my_account/header');
        $this->load->view('frontend/my_account/transaction_request_payout', $t);
        $this->load->view('frontend/my_account/footer');
        $this->load->view('frontend/partial/footer');
    }

    function check_amount($amount)
    {

        if ($amount)
        {

            if (is_numeric($amount))
            {

                if ($amount > 0)
                {

                    $balance = $this->system_vars->member_balance($this->member->data['id']);

                    if ($amount <= $balance)
                    {

                        return true;
                    } else
                    {

                        $this->form_validation->set_message('check_amount', "You cannot request a payout greater than your balance.");
                        return false;
                    }
                } else
                {

                    $this->form_validation->set_message('check_amount', "Your amount must be greater than 0");
                    return false;
                }
            } else
            {

                $this->form_validation->set_message('check_amount', "Your amount must be a number greater than 0");
                return false;
            }
        } else
        {

            return true;
        }
    }

    function submit_payout_request()
    {

        $this->form_validation->set_rules('amount', 'Amount', 'required|trim|callback_check_amount');

        if (!$this->form_validation->run())
        {

            $this->request_payout();
        } else
        {

            $insert = array();
            $insert['member_id'] = $this->member->data['id'];
            $insert['datetime'] = date("Y-m-d H:i:s");
            $insert['type'] = 'payment';
            $insert['amount'] = set_value('amount');
            $insert['summary'] = "Payout Request";

            $this->db->insert('transactions', $insert);

            $this->session->set_flashdata('response', "Your payout request has been submitted. Please allow up to 48 hours for your request to be processed.");

            redirect("/frontend/my_account/transactions");
        }
    }

    function fund_your_account()
    {
        $params['balance'] = $this->system_vars->member_balance($this->member->data['id']);

        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();



        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/partial/backend/header_client', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/transaction_fund_account', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );
        $data['load_js'] = "frontend/my_account/transaction_fund_account_js";
        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function fund_with_paypal($package_id = null, $chat_id = '', $reader_id=null)
    {
        /*
        $package = $this->site->get_package($package_id);

        // Set paypal variables
        $t['item'] = $package['title'];
        $t['item_number'] = $package['id'];
        $t['custom'] = $this->member->data['id'] . "*" . $package['id'];
        $t['return_url'] = site_url("my_account/transactions/paypal_return");
        $t['cancel_url'] = site_url("my_account/transactions/paypal_cancel");
        $t['notify_url'] = site_url("paypal_ipn/deposit");
        $t['amount'] = $package['price'];
        */

        //$package_id = 7;
        $package = $this->site->get_package($package_id);
        
        
        
        // 1. Autoload the SDK Package. This will include all the files and classes to your autoloader
        

        // 2. Provide your Secret Key. Replace the given one with your app clientId, and Secret
        // https://developer.paypal.com/webapps/developer/applications/myapps
        /*$apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                'AY1M5qARKeCMyUUeO9aRf7fqI2Fdp0zATTLRfLjx_iHXivdaITON1pIGgSCuFl4oFsKkHV0wUdNrmkO4',     // ClientID
                'EHpiHq7UOBpTQItKDKoxlypimp3m3hzMB8bBsGsfWEBXHix22fqE6xVvSi9uG0sbpCSs5XV6noMi4Ir3'      // ClientSecret
            )
        );*/
        
        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                'AZk2ErTtoCU5f-hJupmAWi2m8A8IEqRDAj9g3gXI1xVqzAS_x2dwqA3mHzlMajAgJzdhMMucHQGlqBaQ',     // ClientID
                'EAKf0s4EmIymFtOm82tHSGT29w56PRhzJ5Cs85GPpifDpPBkRbHQ85-nuskLxL5ia4zDpucX4UNaiWYO'      // ClientSecret
            )
        );

        // 3. Lets try to create a Payment
        // https://developer.paypal.com/docs/api/payments/#payment_create
        $payer = new \PayPal\Api\Payer();
        $payer->setPaymentMethod('paypal');
        
        $item_name = $package['title'];
        $sku = $package['id'];
        $price = $package['price'];
        
        if ($chat_id != '')
        {
            $custom = $this->member->data['id'] . "*" . $package['id'] . "*" . $chat_id;
        }
        else
        {
            $custom = $this->member->data['id'] . "*" . $package['id'];
        }
        
        
        $item = new \PayPal\Api\Item();
        $item->setName($item_name)
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setSku($sku) 
            ->setPrice($price);

        $itemList = new \PayPal\Api\ItemList();
        $itemList->setItems(array($item));
        

        $amount = new \PayPal\Api\Amount();
        $amount->setTotal($price);
        $amount->setCurrency('USD');

        $notify_url = site_url("paypal_ipn/deposit");
        
        $transaction = new \PayPal\Api\Transaction();
        $transaction->setAmount($amount)
                ->setItemList($itemList)
                ->setCustom($custom)
                ->setNotifyUrl( $notify_url );
        
        
        $return_url = site_url("my_account/transactions/paypal_return/".$chat_id."/".$reader_id);
        $cancel_url = site_url("my_account/transactions/paypal_cancel");

        $redirectUrls = new \PayPal\Api\RedirectUrls();
        $redirectUrls->setReturnUrl( $return_url )
            ->setCancelUrl( $cancel_url );

        $payment = new \PayPal\Api\Payment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setTransactions(array($transaction))
            ->setRedirectUrls($redirectUrls);

        // 4. Make a Create Call and print the values
        try {
            $payment->create($apiContext);
            redirect($payment->getApprovalLink());
        }
        catch (\PayPal\Exception\PayPalConnectionException $ex) {
            // This will print the detailed information on the exception.
            //REALLY HELPFUL FOR DEBUGGING
            echo $ex->getData();
        }
        
        
        // Load paypal module
        //$this->load->view('frontend/pages/paypal', $t);
    }

    function paypal_return($chat_id='', $reader_id=null)
    {
    
        $apiContext = new \PayPal\Rest\ApiContext(
                new \PayPal\Auth\OAuthTokenCredential(
                'AZk2ErTtoCU5f-hJupmAWi2m8A8IEqRDAj9g3gXI1xVqzAS_x2dwqA3mHzlMajAgJzdhMMucHQGlqBaQ', // ClientID
                'EAKf0s4EmIymFtOm82tHSGT29w56PRhzJ5Cs85GPpifDpPBkRbHQ85-nuskLxL5ia4zDpucX4UNaiWYO'      // ClientSecret
                )
        );

        // Get payment object by passing paymentId
        $paymentId = $_GET['paymentId'];
        $payment = Payment::get($paymentId, $apiContext);

        $payerId = $_GET['PayerID'];

        // Execute payment with payer id
        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);

        try {
            // Execute payment
            $result = $payment->execute($execution, $apiContext);

            if ($result->state == "approved")
            {
                $this->deposit($result, $chat_id, $reader_id);
            }
        }
        catch (PayPal\Exception\PayPalConnectionException $ex) {
            echo $ex->getCode();
            echo $ex->getData();
            die($ex);
        }
        catch (Exception $ex) {
            die($ex);
        }

        $this->session->set_flashdata('response', "Your PayPal payment has been processed. Please allow a little time for the changes to take affect.");

        $redirect = $this->session->userdata('redirect');
        if ($redirect)
        {
            redirect($redirect);
            exit;
        }

        redirect("/my_account");
    }
    
    
    function deposit($paypal, $chat_id='', $reader_id=null)
    {
        //$status = $this->input->post('payment_status');
        //$custom = $this->encryption->decrypt(base64_decode($this->input->post("custom")));
        $custom = $paypal->transactions[0]->custom;
        $transaction_id = $paypal->cart;
        $fund_amount = $paypal->transactions[0]->amount->total;
        $currency = $paypal->transactions[0]->amount->currency;
        $item_name = $paypal->transactions[0]->item_list->items[0]->name;

       
        //list($memberid, $packageid) = explode("*", $custom);
        $tmp = explode("*", $custom);
        $memberid = $tmp[0];
        $packageid = $tmp[1];
        
        

        // Set member id
        $this->member->set_member_id($memberid);

        if (is_numeric($packageid))
        {
            $package = $this->site->get_package($packageid);
            // Fund account
            $this->member_funds->fund_account($package['type'], 'regular', $package['value']);
            if ($package['freebies'] > 0)
                $this->member_funds->fund_account($package['type'], 'half', $package['freebies']);
            
            if (isset($tmp[2]))
            {
                $this->session->set_userdata(array("return_chat_id" => $tmp[2]));
                $chat_id = $tmp[2];
                $this->update_chat_set_length($chat_id, $package['value']);
            }

            // log transaction
            $readerid = $memberid;
            $this->member->data['id'] = $readerid;
            $this->member_funds->insert_transaction('payment', $fund_amount, 'US', "PayPal payment", "PayPal", $currency, $transaction_id, $chat_id, $reader_id);

            // log deposit
            $readerid = $memberid;
            $this->member->data['id'] = $readerid;
            $this->member_funds->insert_deposit($fund_amount, 'PayPal:' . $transaction_id, $item_name, $currency, $chat_id, $reader_id);
        }
        else
        {
            // log transaction
            $readerid = $memberid;
            $this->member->data['id'] = $readerid;
            $this->member_funds->insert_transaction('payment', $fund_amount, 'US', "PayPal payment", "PayPal", $currency, $transaction_id, $chat_id, $reader_id);
        }

        // Get member info	
        $member = $this->member->data;
        $member['transaction_id'] = $transaction_id;
        $member['amount'] = "$" . number_format($fund_amount, 2);

        $member['type'] = 'email';
        $this->system_vars->m_omail($member['id'], 'paypal_purchase', $member);
    }
    
    function update_chat_set_length($chat_id, $add_time)
    {
        $sql = "UPDATE chats SET set_length = set_length + " . ($add_time * 60) . " WHERE id = " . $chat_id;
        $this->db->query($sql);
    }

    function paypal_cancel()
    {


        $this->session->set_flashdata('error', "Your PayPal payment has been canceled. ");

        $redirect = $this->session->userdata('redirect');
        if ($redirect)
        {
            redirect($redirect);
            exit;
        }

        redirect("/my_account");
    }

    function add_billing_profile($merchant_type = null, $package_id = null)
    {
    
        if ( ( $merchant_type == null ) or ( $package_id == null) )
        {
            $this->session->set_flashdata('error', "You must select a package before you can attempt to add a new billing profile. Profiles are dependent on package pricing.");
            redirect("/my_account/transactions/fund_your_account");
        } else
        {

            $t['merchant_type'] = $merchant_type;
            $t['package_id'] = $package_id;
            $t['pinfo'] = $this->site->get_package($package_id);
            $t['user'] = $this->member->data;

            // Order Number
            $last_trans_number = $this->db->query("SELECT MAX(id) FROM transactions")->result();
            $last_trans_number = $last_trans_number[0];
            $last_trans_number = $last_trans_number->{'MAX(id)'} + 1;

            $t['order_number'] = date('Y').date("m").date("d").$last_trans_number;

            //Rob:  store package_id in session
            $this->session->set_userdata('package_id', $package_id);

            $data_pages = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
            $param_header['pages'] = $data_pages;
            $param_footer['pages'] = $data_pages;

            $data = array(
                'title' => "Psychic Contact",
                'header' => $this->load->view('frontend/partial/backend/header_client', $param_header, TRUE),
                'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
                'content' => $this->load->view('frontend/my_account/transaction_add_billing_profile', $t, TRUE),
                'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
            );

            $this->load->library('parser');
            $this->parser->parse('frontend/layouts/dashboard', $data);
        }
    }

    function submit_billing_profile($merchant_type = null, $package_id = null, $username = null)
    {
        $this->form_validation->set_rules("cc_num", "Credit Card Number", "required|trim");
        $this->form_validation->set_rules("exp_month", "Expiration Month", "required|trim");
        $this->form_validation->set_rules("exp_year", "Expiration Year", "required|trim");
        $this->form_validation->set_rules("first_name", "First Name", "required|trim");
        $this->form_validation->set_rules("last_name", "Last Name", "required|trim");
        $this->form_validation->set_rules("address", "Address", "required|trim");
        $this->form_validation->set_rules("city", "City", "required|trim");
        $this->form_validation->set_rules("state", "State/Province", "required|trim");
        $this->form_validation->set_rules("zip", "Zip/Postal Code", "required|trim");
        $this->form_validation->set_rules("country", "Country", "required|trim");

        if (!$this->form_validation->run())
        {
            if ($username)
            {
                $this->session->set_flashdata("error", validation_errors());
                redirect("/chat/main/add_billing_profile/{$merchant_type}/{$package_id}/{$username}");

            } else
            {
                $this->add_billing_profile($merchant_type);
            }
        } else
        {
            $array = array();
            $data = array();

            $array['id'] = $this->member->data['id'];
            $array['first_name'] = set_value('first_name');
            $array['last_name'] = set_value('last_name');
            $array['address'] = set_value('address');
            $array['city'] = set_value('city');
            $array['state'] = set_value('state');
            $array['zip'] = set_value('zip');
            $array['country'] = set_value('country');
            $array['card_number'] = set_value('cc_num');
            $array['card_exp_month'] = set_value('exp_month');
            $array['card_exp_year'] = set_value('exp_year');
            $array['sku'] = set_value('sku');
            $array['sku_denomination'] = set_value('sku_denomination');
            $array['order_number'] = set_value('order_number');
            $array['payment_source'] = set_value('payment_source');
            $array['site'] = set_value('site');
            $array['cvv_code'] = set_value('cvv_code');
            $array['amount'] = set_value('total');
            $array['coupon'] = set_value('coupon');

            $coupon_code = set_value('coupon');

            if($coupon_code) {

                $coupon_data = $this->db->query("SELECT * FROM coupons WHERE code = '{$coupon_code}'")->row_array();
                $coupon_type = $coupon_data['coupon_type'];

                if($coupon_type == 'Percentage') {
                    $amount = set_value('total');
                    $coupon_amount_discount = $amount * $coupon_data['value'] / 100;
                } else {
                    // Amount
                    $coupon_amount_discount = $coupon_data['value'];
                }

                $array['coupon_amount_discount'] = $coupon_amount_discount;
                $data['$coupon_amount_discount'] = $coupon_amount_discount;
            }

            if ($package_id)
            {
                $data['firstname'] = set_value('first_name');
                $data['lastname'] = set_value('last_name');
                $data['address'] = set_value('address');
                $data['city'] = set_value('city');
                $data['state'] = set_value('state');
                $data['zip'] = set_value('zip');
                $data['country'] = set_value('country');
                $data['card_number'] = set_value('cc_num');
                $data['expiration_month'] = set_value('exp_month');
                $data['expiration_year'] = set_value('exp_year');
                $data['sku'] = set_value('sku');
                $data['sku_denomination'] = set_value('sku_denomination');
                $data['order_number'] = set_value('order_number');
                $data['payment_source'] = set_value('payment_source');
                $data['site'] = set_value('site');
                $data['cvv'] = set_value('cvv_code');
                $data['amount'] = set_value('total');
                $data['coupon'] = set_value('coupon');

                $cc_checkout = $this->cc_payment_api($data);

                $this->session->unset_userdata("package_id");

                if($cc_checkout->status == 'Ok') {

                    $msg = $cc_checkout->message;
                    $this->session->set_flashdata('success', $msg);

                    $array['customer_id '] = $cc_checkout->customer_profile_id;
                    $array['payment_id'] = $cc_checkout->customer_payment_profile_id;

                    // if fund card is successful, add into billing_profile, transaction, member_balance.
                    $billing_profile = $this->member_billing->create_billing_profile($merchant_type, $array);

                    redirect("/my_account/transactions/submit_deposit/{$billing_profile['billing_profile_id']}/{$package_id}/{$username}");

                } else if($cc_checkout->status == 'Error') {

                    $msg = $cc_checkout->message;
                    $this->session->set_flashdata('error', $msg);
                    $this->add_billing_profile($merchant_type, $package_id);
                }

            } else
            {
                if ($username)
                {
                    redirect("/frontend/chat/main/purchase_time/{$username}");
                } else
                {
                    $this->add_billing_profile($merchant_type);
                }
            }
        }
    }

    function cc_payment_api($card)
    {
        $curl = curl_init( 'https://bayad.psychic-contact.com/api/v1/login' );
        curl_setopt( $curl, CURLOPT_POST, true );
        curl_setopt( $curl, CURLOPT_POSTFIELDS, array(
            "email" => "payment@psychic-contact.com",
            "password" => "teEE39fGyjQh9Mzo0rkwV6VtQAY3pXocIDCO0W6TZaHb72AkZ8"
        ) );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl,  CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl,  CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl,  CURLOPT_SSL_VERIFYHOST, false);

        $response = json_decode(curl_exec(  $curl ));
        $access_token = $response->access_token;

        curl_close($curl);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://bayad.psychic-contact.com/api/v1/payment/checkout",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($card),
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Authorization: Bearer ".  $access_token,
                "Content-Type: application/json"
            ),
        ));

        $result = curl_exec($curl);
        $result = json_decode($result);

        if($result->data) {
            // success
            $data = $result->data;
            return $data;

        } else {
            // error
            $error = $result->error;
            return $error;
        }
    }

// end

    function psy_debug($name, $value)
    {
        echo "<br>*$name*/*$value*";
    }

// end

    function psy_showArray($arr)
    {
        echo "<pre>";
        print_r($arr);
        echo "</pre>";
    }

// end

    function billing_profile_confirmation($billing_profile_id = null)
    {

        $t = $this->member_billing->get_billing_profile($billing_profile_id);

        $this->load->view('frontend/partial/header');
        $this->load->view('frontend/my_account/header');
        $this->load->view('frontend/my_account/transaction_billing_profile_confirmation', $t);
        $this->load->view('frontend/my_account/footer');
        $this->load->view('frontend/partial/footer');
    }

    function delete_billing_profile($billing_id)
    {

        $getBP = $this->db->query("SELECT * FROM billing_profiles WHERE id = {$billing_id} AND member_id = {$this->member['id']} LIMIT 1");
        $b = $getBP->row_array();

        $delete = $this->authcim->delete_profile($b['customer_id']);

        if (!$delete['status'])
        {

            $this->session->set_flashdata('error', $delete['message']);
            redirect("/frontend/my_account/transactions/fund_your_account");
        } else
        {

            $this->db->where('id', $billing_id);
            $this->db->where('member_id', $this->member['id']);
            $this->db->delete('billing_profiles');

            redirect("/frontend/my_account/transactions/fund_your_account");
        }
    }

    function submit_deposit($billing_profile_id, $package_id, $username = null)
    {

        $package = $this->site->get_packages($package_id);

        $response = $this->member_billing->charge_billing_profile($billing_profile_id, $package_id);

        if ($response['error'] == '1')
        {

            if ($username)
            {
                $merchant_type = null;
                $this->session->set_flashdata("error", $response['message']);
                redirect("/chat/main/add_billing_profile/{$merchant_type}/{$package_id}/{$username}");
            } else
            {
                $this->session->set_flashdata('error', $response['message']);
                redirect("/my_account/transactions/fund_your_account");
            }
        } else
        {

            $upd['received_promo'] = 1;
            $this->member->update($upd);

            if ($redirect = $this->session->userdata('redirect'))
            {
                redirect($redirect);
            } else
            {
                redirect("/my_account/transactions/deposit_confirmation/{$package_id}/{$response['transaction_id']}");
            }
        }
    }
    
    function submit_deposit2($billing_profile_id, $package_id, $chat_id = '',$reader_id=null)
    {

        $package = $this->site->get_packages($package_id);

        $response = $this->member_billing->charge_billing_profile($billing_profile_id, $package_id, $chat_id, $reader_id);

        if ($response['error'] == '1')
        {

            if ($username)
            {

                $this->session->set_flashdata("error", $response['message']);
                redirect("/chat/main/add_billing_profile/{$merchant_type}/{$package_id}/{$username}");
            } else
            {

                $this->session->set_flashdata('error', $response['message']);
                redirect("/my_account/transactions/fund_your_account");
            }
        } else
        {
            $upd['received_promo'] = 1;
            $this->member->update($upd);

            if ($redirect = $this->session->userdata('redirect'))
            {
                redirect($redirect);
            } else
            {
                redirect("/my_account/transactions/deposit_confirmation/{$package_id}/{$response['transaction_id']}/{$chat_id}");
            }
        }
    }

    public function deposit_confirmation($package_id = null, $transaction_id = null, $chat_id = null)
    {

        //--- Rob's Hack
        //--- Because I didn't want to implement this code in like 5 spots I added it here
        //--- IF this is a purchase while a user was in a chat session then we redirect to a
        //--- Confirmation within the chat popup class

        //$chatId = $this->session->userdata('chat_id');

        if ($chat_id)
        {
            //redirect("/chat/main/funds_confirmation/{$chatId}/{$package_id}/{$transaction_id}");
            
            $package = $this->site->get_package($package_id);
            // Fund account
            $this->member_funds->fund_account($package['type'], 'regular', $package['value']);
            if ($package['freebies'] > 0)
                $this->member_funds->fund_account($package['type'], 'half', $package['freebies']);
            
            if (isset($chat_id))
            {
                $this->session->set_userdata(array("return_chat_id" => $chat_id));
                $this->session->set_userdata(array("mins_purchased" => $package['value']));
                $this->update_chat_set_length($chat_id, $package['value']);
            }
            
            redirect("/my_account");
            
        } else
        {
            $data = array();
            $data['package'] = $this->site->get_package($package_id);
            $data['transaction'] = $this->db->query("SELECT * FROM transactions WHERE id = {$transaction_id}")->row_array();

            /*9-15
              @pen
            */
            $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
            $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

            $data = array(
                'title' => "Psychic Contact",
                'header' => $this->load->view('frontend/partial/backend/header_client', $param_header, TRUE),
                'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),

                'content' => $this->load->view('frontend/my_account/transaction_deposit_confirmation', $data, TRUE),
                'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
            );

            $this->load->library('parser');
            $this->parser->parse('frontend/layouts/dashboard', $data);
            
           
        }
    }
    /*pen 10/14*/
    function limit_mail()
    {
        $member = $this->member->data;
        $member['type'] = 'email';
        $this->system_vars->m_omail($member['id'], 'fund_limit', $member);
    }

}
