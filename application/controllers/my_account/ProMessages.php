<?php

class promessages extends CI_Controller
{

    function __construct()
    {

        parent :: __construct();

        $this->settings = $this->system_vars->get_settings();

        if (!$this->session->userdata('member_logged'))
        {

            $this->session->set_flashdata('error', "You must login before you can gain access to secured areas");
            redirect('/register/login');
            exit;
        }
    }

    function test_omail()
    {
        $recipient_id = 6;
        $sender_id = 7;
        $email_template = "cc_account_funded";
        $type = 'email';
        $this->messages_model->notify(null, $recipient_id, null, null, $type, $email_template);
    }

    function index($message_reply_id = null, $type = "reply")
    {

        $sql = "SELECT 
                    members.id,
                    members.username,
                    members.first_name 
                FROM
                     members 
                WHERE members.active = 1 AND members.deleted=0 AND members.id<>1
                
                ORDER BY members.username";

        $getAllMembers = $this->db->query($sql);
        $users = $getAllMembers->result_array();

        foreach($users as $key=>$user)
        {
            $ban_check = count($this->db->query("SELECT id FROM member_bans WHERE member_id={$this->member->data['id']} AND reader_id={$user['id']}")->result_array());
            if($ban_check > 0)
            {
                unset($users[$key]);
            }
        }

        $promotion = $this->db->query("SELECT * FROM promotion WHERE member_id = {$this->member->data['id']} AND status=1 LIMIT 1")->row_array();
        $check_promotion = $promotion == null ? 0 : 1;

        $params['users'] = $users;
        $params['to'] = "";
        $params['subject'] = "";
        $params['message'] = "";
        $params['promotion_check'] = $check_promotion;
        $params['promotion_id'] = $promotion['id'];

        // If a reply id was passed… Then
        // prepopulate the fields
        if ($message_reply_id && $type == "reply")
        {
            $sql = "SELECT * FROM messages WHERE id = {$message_reply_id} AND (ricipient_id = {$this->member->data['id']} OR sender_id = {$this->member->data['id']}) LIMIT 1";
            $getMessage = $this->db->query($sql);

            if ($getMessage->num_rows() == 1)
            {
                $message = $getMessage->row_array();

                $sender = $this->system_vars->get_member($message['sender_id']);

                $params['to'] = $message['sender_id'];
                $params['to_user'] = $sender;
                $params['subject'] = "Re: " . $message['subject'];
                $params['message'] = "\n\n\n+++ Original Message: {$sender['first_name']} {$sender['last_name']} - {$sender['username']} on " . date("m/d/y h:i:s", strtotime($message['datetime'])) . " +++\n\n" . html_entity_decode($message['message']);
            }
        } else if ($message_reply_id && $type != "reply")
        {
            $params['to'] = $message_reply_id;
            $params['to_user'] = $this->system_vars->get_member($message_reply_id);
            $params['subject'] = "";
            $params['message'] = "";
        }
        
        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/pro_messages_compose', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);

    }

    function upload_file()
    {
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png|iso|dmg|zip|rar|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|rtf|sxc|sxi|txt|exe|avi|mpeg|mp3|mp4|3gp';
        $config['max_size'] = 204800;
        $config['max_height'] = 768;
        $config['max_width'] = 1024;
        $this->load->library('upload', $config);

        if($this->upload->do_upload('attach'))
        {
            return $this->upload->data();
        }
        else
        {
            return $this->upload->display_errors();
        }
    }

    function compose_submit()
    {
        $this->form_validation->set_rules("subject", "Subject", "trim|required");
        $this->form_validation->set_rules("message", "Message", "trim|required");

        $block_words = $getData = $this->db->query
        ("
                SELECT
                    block_words
                FROM word_block

        ")->result_array();

        $from = $this->input->post('from');
        $message = $this->input->post("message");
        $to = $this->input->post("to");
        $subject = $this->input->post("subject");


        $promotion_id = $this->input->post('promotion_id');
        $data = array('status' => 3);
        $this->db->where('id', $promotion_id);
        $this->db->update('promotion', $data);

        foreach($block_words as $block_word)
        {
            if(strpos(strip_tags($message), $block_word['block_words']) !== false) // if message contains block word, it is stored in DB and admin will review it.
            {
                $attachment_file = null;
                $file_data = $this->upload_file();

                if(!is_string($file_data))
                {
                    $attachment_file = $file_data['full_path'];

                } else {

                }

                foreach($to as $item)
                {

                    $message_data = array(
                        'from' => $from,
                        'to' => $item,
                        'subject' => $subject,
                        'message' => $message,
                        'attached_file'=> $attachment_file,
                        'reason' => '',
                        'created_at' => date("Y-m-d H:i:s"),
                        'message_status' => 'PENDING'
                    );

                    $this->db->insert('message_center_notifications', $message_data);
                }


                redirect('/my_account/promessages');

            }
        }

        if (!$this->form_validation->run())
        {
            $this->index();

        } else
        {
            //send mail
            $CI = & get_instance();
            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'mail.taroflash.com',
                'smtp_port' => 465,
                'smtp_crypto' => 'ssl',
                'smtp_timeout' => 7,
                'smtp_user' => 'norreply@psychic-contact.com',
                'smtp_pass' => 'NoRep2017*1245Klm1P32124',
                'mailtype'  => 'html',
                'charset'   => 'iso-8859-1'
            );
            $CI->load->library('email', $config);

            // Parse system email template
            $subject = set_value('subject');
            $pre_content = html_entity_decode(nl2br(set_value('message')));

            $content = "
                        <div style='background:#b9b9b9;padding:20px;'>
                            <div style='margin-bottom:10px;font-size:20px;font-weight:bold;font-family:Arial;'>Psychic-Contact.com</div>
                            <div style='background:#FFF;padding:20px;'>{$pre_content}</div>
                        </div>
                        ";

            $file_data = $this->upload_file();

            // Configure Email Options
            $config['mailtype'] = 'html';
            $CI->email->initialize($config);

            // Send Email
            $from[1] = $CI->config->item('email_from_email_address');
            $from[2] = $CI->config->item('email_from_name');

            // IF the TO is a number
            // Get the member and send the message to their local inbox
            // as well as their email address
            $tos = set_value('to');

            foreach($tos as $to)
            {
                if (is_numeric($to))
                {

                    $member = $this->db->query("SELECT email FROM members WHERE `id`='{$to}'")->row_array();

                    $to = $member['email'];
                }

                $CI->email->from($from[1], $from[2]);
                $CI->email->to($to);
                $CI->email->subject($subject);
                $CI->email->message($content);

                if(!is_string($file_data))
                {
                    $CI->email->attach($file_data['full_path']);
                } else {

                }
                $log = $CI->email->send();
                log_message('debug', $log);

                $this->member->notify(set_value('to'), $this->member->data['id'], set_value('subject'), nl2br(set_value('message')));

                $this->session->set_flashdata('response', "Your message has been sent");
                $this->session->set_flashdata('response', "Your message has been sent");
            }

            redirect('/my_account/promessages');
        }
    }

    function getMessages()
    {
        $sender_id = $this->input->post('user_id');
        $messages = $this->db->query("SELECT * FROM messages WHERE ricipient_id ={$this->member->data['id']} AND sender_id = '{$sender_id}' ORDER BY datetime DESC")->result_array();
        $return['messages'] = $messages;

        echo json_encode($return);
    }

    function getAdminMessages()
    {
        $messages = $this->db->query("SELECT * FROM messages WHERE ricipient_id ={$this->member->data['id']} AND (`type`='admin' OR sender_id=1)  ORDER BY datetime DESC")->result_array();
        $return['messages'] = $messages;

        echo json_encode($return);
    }

    function getSentMessages()
    {
        $receiver_id = $this->input->post('user_id');
        $messages = $this->db->query("SELECT * FROM messages WHERE sender_id='{$this->member->data['id']}' AND ricipient_id='{$receiver_id}' ORDER BY datetime DESC")->result_array();

        $return['messages'] = $messages;
        echo json_encode($return);
    }

    function getAdminSentMessages()
    {
        $messages = $this->db->query("SELECT * FROM messages WHERE sender_id ={$this->member->data['id']} AND (`type`='admin' OR ricipient_id=1) ")->result_array();
        $return['messages'] = $messages;

        echo json_encode($return);
    }

    function getSystemMessages()
    {
        $messages = $this->db->query("SELECT * FROM messages WHERE ricipient_id='{$this->member->data['id']}' AND subject = 'No Online Reader' AND `type` IS NULL")->result_array();
        $return['messages'] = $messages;

        echo json_encode($return);
    }

    function request_admin($member_id)
    {
        $username = $this->db->query("SELECT username FROM members WHERE id={$member_id}")->row_array();
        $username = $username['username'];
        $data = array(
            'member_id' => $member_id,
            'username' => $username,
            'status' => 0,
            'updated_at' => date("Y-m-d H:i:s")
        );
        $this->db->insert('promotion', $data);
        redirect('/my_account/promessages');
    }

}
