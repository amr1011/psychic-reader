<?php
require $_SERVER["DOCUMENT_ROOT"] . '/paypal/autoload.php';


class psychic_readers extends CI_Controller
{

     function __construct()
    {
        parent :: __construct();
        $this->load->library('my_common');


    }

    function index()
    {

        $params = array();
        $params['page'] = '0';
        $params['readers'] = $this->readers->getAll();
        
        if($this->member->data['id']) {
            $whitelist_reader = $this->db->query("SELECT * FROM reader_whitelist WHERE client_id={$this->member->data['id']}")->result_array();

            if(count($whitelist_reader) > 0)
            {
                $params['readers'] = $this->readers->getWhiteList($this->member->data['id']);
            }
        }

        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();
        
        $data = array(
            'title' => "Psychic Contact - Chat with reader",
            'header' => $this->load->view('frontend/partial/backend/header_client', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/psychic_readers', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $data['load_js'] = "frontend/my_account/psychic_readers_js";

        $this->load->library('parser');        
        $this->parser->parse('frontend/layouts/dashboard', $data);

    }

    function page_readers()
    {
        $params = array();
        $params['page'] = '0';
        $params['readers'] = $this->readers->getAll();
        $params['client'] = $this->db->query("SELECT * FROM members WHERE id={$this->member->data['id']}")->row_array();

        // if($this->member->data['id']) {
        //     $whitelist_reader = $this->db->query("SELECT * FROM reader_whitelist WHERE client_id={$this->member->data['id']}")->result_array();
        //     if(count($whitelist_reader) > 0)
        //     {
        //         $params['readers'] = $this->readers->getWhiteList($this->member->data['id']);
        //     }
        // }

        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/layouts/header', $param_header, TRUE),
            'slider' => "",
            'content' => $this->load->view('frontend/psychics/page_readers', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/page', $data);
    }

    function search()
    {

        // Format variables for easy use
        $category_id = trim($this->input->post('category'));
        $query = trim($this->input->post('query'));

        $searchResults = $this->readers->search($category_id, $query);

        if ($searchResults['error'] == '1')
        {

            $this->session->set_flashdata('warning', $searchResults['message']);
            redirect("/psychics");
        } else
        {

            $t['readers'] = $searchResults['readers'];

            $this->load->view('frontend/partial/header');
            $this->load->view('frontend/psychics/main', $t);
            $this->load->view('frontend/partial/footer');
        }
    }

}
