<?php

class ban_users extends CI_Controller
{

    function __construct()
    {

        parent :: __construct();

        $this->settings = $this->system_vars->get_settings();

        if(!$this->session->userdata('member_logged')){
            $this->session->set_flashdata('error', "You must login before you can gain access to secured areas");
            redirect('/register/login');
            exit;
        }
        $this->reader->init($this->session->userdata('member_logged'));
    }

    function index()
    {
        $params['clients'] = $this->reader->get_banned_clients();
        $params['all_clients'] = $this->reader->get_read_clients();

        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact - Ban Users",
            'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/ban_users', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');        
        $data['load_js'] = "frontend/my_account/ban_users_js";

        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function search()
    {
        $params['clients'] = $this->reader->get_clients($this->input->post('search_type'),trim($this->input->post('query')));

        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact - Ban Users",
            'header' => $this->load->view('frontend/layouts/header_authenticated', $param_header, TRUE),
            'slider' => "",
            'content' => $this->load->view('frontend/my_account/ban_users', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');        
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function unban($ban_id)
    {
        $this->reader->unbanUser($ban_id);
        $this->session->set_flashdata("response","User unbanned");

        redirect("/my_account/ban_users");
    }

    function ban($member_id)
    {
        $this->member->set_member_id($member_id);
        $this->member->banUser($this->reader->data['id']);
        $username = $this->member->data['username'];

        $this->session->set_flashdata("warning","User {$username} banned.");

        redirect("/my_account/ban_users");
    }
}
 ?>