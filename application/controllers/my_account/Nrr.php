<?php

class nrr extends CI_Controller
{

    function __construct()
    {

        parent :: __construct();

        $this->settings = $this->system_vars->get_settings();

        if(!$this->session->userdata('member_logged'))
        {

            $this->session->set_flashdata('error', "You must login before you can gain access to secured areas");
            redirect('/register/login');
            exit;

        }
        $this->load->model("chatmodel");
        $this->load->model("nrr_model");
    }

    function index(){

        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact - NRR",
            'header' => $this->load->view('frontend/layouts/header_authenticated_reader', $param_header, TRUE),
            'slider' => "",
            'content' => $this->load->view('frontend/my_account/nrr_list', null, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');        
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function details($nrr_id = null){

        if($nrr_id)
        {
            $arr = $this->nrr_model->getNRR($nrr_id);

            /*9-15
              @pen
            */
            $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
            $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

            $data = array(
                'title' => "Psychic Contact",
                'header' => $this->load->view('frontend/layouts/header_authenticated', $param_header, TRUE),
                'slider' => "",
                'content' => $this->load->view('frontend/my_account/nrr_details', $arr, TRUE),
                'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
            );

            $this->load->library('parser');
            $this->parser->parse('frontend/layouts/dashboard', $data);
        }
        else
        {
            redirect('/frontend/my_account/nrr');
        }
    }

    function give_timeback($nrr_id = null)
    {
        $this->form_validation->set_rules('type','Type','required|trim');
        $this->form_validation->set_rules('timeback', "Time Back","required|trim|numeric|greater_than[0]");

        if(!$this->form_validation->run()){

            $this->details($nrr_id);

        }else{

            $array = $this->nrr_model->process($nrr_id, set_value('type'), set_value('timeback'));

            // var_dump($array);
            // exit;

            if($array['error'] == '1'){

                $this->session->set_flashdata('error', $array['message']);
                redirect("/my_account/nrr/details/{$nrr_id}");


            }else{

                $this->session->set_flashdata("response", "Refunded " . $array['timeback'] . " time.");
                redirect("/my_account/nrr/details/{$nrr_id}");

            }

        }
    }

    function delete($nrr_id=null)
    {
        $this->db->where('id', $nrr_id);
        $this->db->delete('nrr');

        redirect("/my_account/nrr");
    }
}