<?php

class email_readings extends CI_Controller
{

    function __construct()
    {
        parent :: __construct();

        $this->settings = $this->system_vars->get_settings();

        if (!$this->session->userdata('member_logged'))
        {
            redirect('/register/email_reading_register');

        }
    }

    function preview_question()
    {
        if($this->session->userdata('package'))
        {
            $t['data'] = $this->reader->get_email_package($this->session->userdata('package'));
        }

        $t['countries'] = $this->db->query("SELECT * FROM countries ORDER BY `name` ASC")->result_array();
        $t['readers'] = $this->db->query("SELECT id, username FROM members WHERE `type`='READER' ")->result_array();
        $year = date("Y");
        $t['expiration_year'] = [$year, $year+1, $year+2, $year+3, $year+4, $year+5, $year+6];

        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/layouts/header_authenticated', $param_header, TRUE),
            'slider' => "",
            'content' => $this->load->view('frontend/my_account/email_reading_request_view', $t, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function open_requests()
    {
        $params['open'] = 1;
        $params['emails'] = $this->db->query("SELECT * FROM questions WHERE profile_id = {$this->member->data['id']} AND status = 'new' ORDER BY id")->result_array();

        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),   
            'content' => $this->load->view('frontend/my_account/email_readings_list', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function closed_requests()
    {
        $params['open'] = 0;
        $params['emails'] = $this->db->query("SELECT * FROM questions WHERE profile_id = {$this->member->data['id']} AND status = 'answered' ORDER BY id")->result_array();

        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),   
            'content' => $this->load->view('frontend/my_account/email_readings_list', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function email_specials()
    {

        if (!$this->member->data['profile_id'])
        {

            $this->session->set_flashdata('error', "You must setup your expert profile before you can add email specials");
            redirect("/frontend/my_account/main/edit_profile");
        }

        $getEmailSpecials = $this->db->query("SELECT * FROM email_specials WHERE member_id = {$this->member->data['profile_id']} ");
        $t['specials'] = $getEmailSpecials->result_array();

        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),   
            'content' => $this->load->view('frontend/my_account/email_specials', $t, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );
        $data['load_js'] = "frontend/my_account/email_specials_js";
        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    /*
     Email Reading List
    */
    function client_emails()
    {

        $getEmailSpecials = $this->db->query("SELECT * FROM questions WHERE member_id = {$this->member->data['id']} ");
        $params['emails'] = $getEmailSpecials->result_array();
        
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/partial/backend/header_client', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),

            'content' => $this->load->view('frontend/my_account/email_readings_client', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function client_view($question_id = null)
    {

        $t = $this->db->query("SELECT * FROM questions WHERE id = {$question_id} LIMIT 1")->row_array();

        $t['expert'] = $this->system_vars->get_member($t['profile_id']);

        // Get Messages from this thread
        $t['messages'] = $this->db->query
                        ("
				SELECT 
					questions_thread.*,
					members.username as username 
					
				FROM 
					questions_thread 
					
				JOIN members on members.id = questions_thread.member_id
					
				WHERE 
					question_id = {$question_id} 
					
				ORDER BY 
					id DESC
			
			")->result_array();

        $this->load->view('frontend/partial/header');
        $this->load->view('frontend/my_account/header');
        $this->load->view('frontend/my_account/email_reading_client_view', $t);
        $this->load->view('frontend/my_account/footer');
        $this->load->view('frontend/partial/footer');
    }

    function reader_view($question_id = null)
    {

        $t = $this->db->query("SELECT * FROM questions WHERE id = {$question_id} LIMIT 1")->row_array();

        $t['expert'] = $this->system_vars->get_member($t['profile_id']);

        // Get Messages from this thread
        $t['messages'] = $this->db->query
                        ("
				SELECT 
					questions_thread.*,
					members.username as username 
					
				FROM 
					questions_thread 
					
				JOIN members on members.id = questions_thread.member_id
					
				WHERE 
					question_id = {$question_id} 
					
				ORDER BY 
					id DESC
			
			")->result_array();

        $this->load->view('frontend/partial/header');
        $this->load->view('frontend/my_account/header');
        $this->load->view('frontend/my_account/email_reading_reader_view', $t);
        $this->load->view('frontend/my_account/footer');
        $this->load->view('frontend/partial/footer');
    }

    function new_special()
    {

        $params['title'] = "";
        $params['total_questions'] = "1";
        $params['price'] = "1";
        $params['form_action'] = "/my_account/email_readings/submit_new";

        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),   
            'content' => $this->load->view('frontend/my_account/email_special_form', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);

    }

    function submit_new()
    {

        $this->form_validation->set_rules("title", "Title", "trim|required");
        $this->form_validation->set_rules("total_questions", "Total Questions", "trim|required|greater_than[0]");
        $this->form_validation->set_rules("price", "Price", "trim|required|greater_than[0]");

        if (!$this->form_validation->run())
        {

            $this->new_special();
        } else
        {

            $insert = array();
            $insert['member_id'] = $this->member->data['id'];
            $insert['title'] = set_value('title');
            $insert['description'] = set_value('description');
            $insert['total_questions'] = set_value('total_questions');
            $insert['price'] = set_value('price');

            $this->db->insert('email_specials', $insert);

            $this->session->set_flashdata('response', "Your Email Special has been added successfully");

            redirect("/my_account/email_readings/email_specials");
            // redirect("/my_account/main/edit_profile");
        }
    }

    function edit_special($id)
    {

        $getEmail = $this->db->query("SELECT * FROM email_specials WHERE id = {$id} LIMIT 1");
        $t = $getEmail->row_array();

        $t['form_action'] = "/my_account/email_readings/save_special/{$id}";

        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/layouts/header_authenticated', $param_header, TRUE),
            'slider' => "",
            'content' => $this->load->view('frontend/my_account/email_special_edit_form', $t, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);

        /*$this->load->view('frontend/partial/header');
        $this->load->view('frontend/my_account/header');
        $this->load->view('frontend/my_account/email_special_form', $t);
        $this->load->view('frontend/my_account/footer');
        $this->load->view('frontend/partial/footer');*/
    }

    function save_special($id)
    {

        $this->form_validation->set_rules("title", "Title / Short Description", "trim|required");
        $this->form_validation->set_rules("total_questions", "Total Questions", "trim|required|greater_than[0]");
        $this->form_validation->set_rules("price", "Price", "trim|required|greater_than[0]");

        if (!$this->form_validation->run())
        {

            $this->edit_special($id);
        } else
        {

            $insert = array();
            $insert['member_id'] = $this->member->data['id'];
            $insert['title'] = set_value('title');
            $insert['description'] = set_value('description');
            $insert['total_questions'] = set_value('total_questions');
            $insert['price'] = set_value('price');

            $this->db->where('id', $id);
            $this->db->update('email_specials', $insert);

            $this->session->set_flashdata('response', "Your Email Special has been saved successfully");

            redirect("/my_account/email_readings/email_specials");
        }
    }

    function delete_special($id)
    {

        $this->db->where('id', $id);
        $this->db->where('member_id', $this->member->data['id']);
        $this->db->delete('email_specials');

        $this->session->set_flashdata("response", "You have successfully removed an email special");

        redirect("/my_account/email_readings/email_specials");
    }

    function mark_as_answered($question_id = null)
    {

        // Get questions detials
        $question = $this->db->query
                        ("
			
				SELECT
					c.id as client_id,
					c.email as client_email,
					c.username as client_username,
					c.country as client_country,
					
					questions.*
					
				FROM
					questions
					
				JOIN members as c ON c.id = questions.member_id 
					
				WHERE
					questions.id = {$question_id}
					
			")->row_array();

        // Update status of question
        $this->db->where('id', $question_id)->update('questions', array('status' => 'answered'));

        // Send email to client
        $params = array();
        $params['name'] = $question['client_username'];
        $params['type'] = "email";
        $this->system_vars->m_omail($question['client_id'], 'email_reading_answered', $params);

        // Give funds to reader, we need to pass in the region so we can keep track of Canadian balance and US Balance
        // Rob: 10/15/2014 Do we need this call, is the reader paid elsewhere?
        /*
          $region = (trim(strtolower($question['client_country']))=='ca' ? "ca" : "us");
          $this->member_funds->pay_reader('email', $question['price'], "Email Reading #{$question['id']}", $region);
         */

        // Redirect
        $this->session->set_flashdata('response', "Email reading has been marked as answered, the client has been notified and funds were released to your account.");
        redirect("/frontend/my_account/email_readings/open_requests");
    }

    function send_message($question_id = null)
    {

        if ($this->input->post('message'))
        {

            // Get question, reader and client info
            $question = $this->db->query
                            ("
				
					SELECT
						r.id as reader_id,
						r.email as reader_email,
						r.username as reader_username,
						
						c.id as client_id,
						c.email as client_email,
						c.username as client_username,
						
						questions.*
						
					FROM
						questions
						
					JOIN members as r ON r.id = questions.profile_id
					JOIN members as c ON c.id = questions.member_id 
						
					WHERE
						questions.id = {$question_id}
						
				")->row_array();

            $insert = array();
            $insert['datetime'] = date("Y-m-d H:i:s");
            $insert['question_id'] = $question_id;
            $insert['member_id'] = $this->member->data['id'];
            $insert['message'] = $this->input->post('message');

            $this->db->insert('questions_thread', $insert);

            // Compile email params
            $params = array();
            $params['name'] = $this->member->data['username'];
            $params['message'] = $this->input->post('message');

            // Notify Reader of a new message
            if ($this->member->data['id'] != $question['reader_id'])
            {

                $this->session->set_flashdata('response', "Your message has been sent to {$question['reader_username']}");
                $this->system_vars->omail($this->member->data['id'], 'new_email_reading_message', $params);

                redirect("/frontend/my_account/email_readings/client_view/{$question_id}");

                exit;
            }

            // Notify Client of a new message
            if ($this->member->data['id'] != $question['client_id'])
            {

                $this->session->set_flashdata('response', "Your message has been sent to {$question['client_username']}");
                $this->system_vars->omail($question['client_email'], 'new_email_reading_message', $params);

                redirect("/frontend/my_account/email_readings/reader_view/{$question_id}");

                exit;
            }
        }
    }

    function redirect_to_sign_up()
    {
        redirect("/register/email_reading_register");
    }

    function submit()
    {
        $question1 = $this->input->post('question1');
        $question2 = $this->input->post('question2');
        $question3 = $this->input->post('question3');
        $question4 = $this->input->post('question4');
        $question5 = $this->input->post('question5');
        $member_id = $this->input->post('member-id');
        $first_name = $this->input->post('first-name');
        $last_name = $this->input->post('last-name');
        $address_1 = $this->input->post('address-1');
        $address_2 = $this->input->post('address-2');

        $country = $this->input->post('country');
        $state = $this->input->post('state');
        $zip_code = $this->input->post('zip-code');
        $city = $this->input->post('city');
        $phone_number = $this->input->post('phone-number');
        $email = $this->input->post('email');
        $date_birth = $this->input->post('date-birth');
        $gender = $this->input->post('gender');
        $time_birth = $this->input->post('time-birth');
        $birth_time_ap = $this->input->post('birth-time-ap');
        $place_birth = $this->input->post('place-birth');
        $name_numerology = $this->input->post('name-numerology');
        $date_numerology = $this->input->post('date-numerology');
        $topic_word = $this->input->post('topic-word');
        $reader_id = $this->input->post('reader-id');
        $special_instruction = $this->input->post('special-instruction');
        $additional_info = $this->input->post('additional-info');
        $payment_info = $this->input->post('payment-info');
        $f_name_card = $this->input->post('f-name-card');
        $l_name_card = $this->input->post('l-name-card');
        $billing_address = $this->input->post('billing-address');
        $billing_city = $this->input->post('billing-city');
        $billing_state = $this->input->post('billing-state');
        $billing_zip_code = $this->input->post('billing-zip-code');
        $billing_country = $this->input->post('billing-country');

        $card_number = $this->input->post('card-number');
        $card_cvv_code = $this->input->post('card-cvv-code');
        $card_expiration = $this->input->post('card-expiration');

        $email_order_data = array (
            'member_id' => $member_id, 'first_name' => $first_name, 'last_name'=>$last_name,
            'address_1'=>$address_1, 'address_2'=>$address_2,
            'state_id' => $state, 'city_id'=>$city, 'country_code'=>$country,'zipcode'=>$zip_code,
            'phone_number'=>$phone_number, 'email'=>$email, 'gender'=>$gender, 'dob'=>$date_birth,
            'birth_time'=>$time_birth, 'birth_time_ap'=>$birth_time_ap, 'birth_place'=>$place_birth,
            'name_numerology'=>$name_numerology, 'dob_numerology'=>$date_numerology, 'topic'=>$topic_word,
            'reader_id'=>$reader_id, 'special_instruction'=>$special_instruction, 'additional_instruction'=>$additional_info,
            'payment_info'=>$payment_info, 'card_first_name'=>$f_name_card, 'card_last_name'=>$l_name_card, 'billing_address'=>$billing_address,
            'billing_city'=>$billing_city, 'billing_state'=>$billing_state, 'billing_zipcode'=>$billing_zip_code,
            'billing_country'=>$billing_country, 'card_number'=>$card_number, 'card_cvv_code'=>$card_cvv_code,
            'card_expiration'=>$card_expiration
        );
        $this->db->insert('email_reading', $email_order_data);

    }

}
