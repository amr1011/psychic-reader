<?php

class articles extends CI_Controller
{

    function __construct()
    {
        parent :: __construct();

        $this->settings = $this->system_vars->get_settings();

        if (!$this->session->userdata('member_logged'))
        {

            $this->session->set_flashdata('error', "You must login before you can gain access to secured areas");
            redirect('/register/login');
            exit;
        }
    }

    function index()
    {

        $getArticles = $this->db->query("SELECT * FROM articles WHERE profile_id = {$this->member->data['id']} ");
        $data['articles'] = $getArticles->result_array();

        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();
        $data['categories'] = $this->db->query("SELECT * FROM categories")->result_array();
        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/articles', $data, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $data['load_js'] = "frontend/my_account/articles_js";

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function submit($article_id = null)
    {
        if($article_id == null)
        {
            /*9-15
              @pen
            */
            $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
            $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();
            $data['categories'] = $this->db->query("SELECT * FROM categories")->result_array();
            $data['article'] = null;
            $data = array(
                'title' => "Psychic Contact",
                'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
                'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
                'content' => $this->load->view('frontend/my_account/article_form', $data, TRUE),
                'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
            );

            $this->load->library('parser');
            $this->parser->parse('frontend/layouts/dashboard', $data);
        } else {

            /*9-15
              @pen
            */
            $article = $this->db->query("SELECT * FROM articles WHERE id='{$article_id}'")->result_array();
            $params['article'] = $article[0];
            $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
            $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();
            $params['categories'] = $this->db->query("SELECT * FROM categories")->result_array();
            $data = array(
                'title' => "Psychic Contact",
                'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
                'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
                'content' => $this->load->view('frontend/my_account/article_form', $params, TRUE),
                'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
            );
            $data['load_js'] = "frontend/my_account/article_form_js";

            $this->load->library('parser');
            $this->parser->parse('frontend/layouts/dashboard', $data);
        }
    }

    function submit_post()
    {

        $config['upload_path'] = $_SERVER["DOCUMENT_ROOT"] . '/media/articles';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = '2048';
        $config['file_name'] = 'article_img_' . time();

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file'))
        {
            $insert = array();
            $insert['datetime'] = date("Y-m-d H:i:s");
            $insert['profile_id'] = $this->member->data['id'];
            $insert['title'] = $this->input->post('title');
            $insert['url'] = $this->input->post('url');
            $insert['seo_keyword'] = $this->input->post('seo-keyword');
            $insert['seo_description'] = $this->input->post('seo-description');
            $insert['approved'] = 0;
            $insert['category_id'] = $this->input->post('category-id');

            $insert['content'] = $this->input->post('content');

            $article_id = set_value('article_id');
            if($article_id != '')
            {
                $this->db->where('id', $article_id);
                $this->db->update('articles', $insert);
            } else {
                $this->db->insert('articles', $insert);
            }

            $this->session->set_flashdata('response', "Thank you for your submission!");
            redirect("/my_account/articles/index");
        } else
        {

            $file = $this->upload->data();

            $insert = array();
            $insert['datetime'] = date("Y-m-d H:i:s");
            $insert['profile_id'] = $this->member->data['id'];
            $insert['filename'] = $file['file_name'];
            $insert['title'] = $this->input->post('title');
            $insert['url'] = $this->input->post('url');
            $insert['seo_keyword'] = $this->input->post('seo-keyword');
            $insert['seo_description'] = $this->input->post('seo-description');
            $insert['approved'] = 0;

            $insert['content'] = $this->input->post('content');
            $insert['category_id'] = $this->input->post('category-id');

            $article_id = set_value('article_id');
            if($article_id != '')
            {
                $this->db->where('id', $article_id);
                $this->db->update('articles', $insert);
            } else {
                $this->db->insert('articles', $insert);
            }

            $this->session->set_flashdata('response', "Thank you for your submission!");
            redirect("/my_account/articles/index");
        }
    }

    function edit($article_id)
    {

        $getArticle = $this->db->query("SELECT * FROM articles WHERE id = {$article_id} AND member_id = {$this->member->data['id']} LIMIT 1");
        $t = $getArticle->row_array();

        $t['page_title'] = "Edit Article";
        $t['save_button'] = "Save Article";
        $t['form_action'] = "/frontend/my_account/articles/edit_submit/{$article_id}";

        $t['categories'] = $this->system_vars->get_categories('expert', $this->member->data['id']);

        $this->load->view('frontend/partial/header');
        $this->load->view('frontend/my_account/header');
        $this->load->view('frontend/my_account/article_form', $t);
        $this->load->view('frontend/my_account/footer');
        $this->load->view('frontend/partial/footer');
    }

    function check_article_length($string)
    {

        if (strlen($string) >= 200)
        {

            return true;
        } else
        {

            $this->form_validation->set_message('check_article_length', 'Your article must be at least 200 characters long');
            return false;
        }
    }

    function create_new_submit()
    {

        $this->form_validation->set_rules('category', 'Category', 'required|trim|xss_clean');
        $this->form_validation->set_rules('subcategory', 'Subcategory', 'required|trim|xss_clean');
        $this->form_validation->set_rules('title', 'Article Title', 'required|trim|xss_clean');
        $this->form_validation->set_rules('content', 'Article Content', 'required|trim|callback_check_article_length');

        if (!$this->form_validation->run())
        {

            $this->create_new();
        } else
        {

            // Find the profile id being used based on category
            $getProfile = $this->db->query("SELECT * FROM profiles WHERE member_id = {$this->member->data['id']} AND category_id = " . set_value('category') . " LIMIT 1");
            $profile = $getProfile->row_array();

            $insert = array();
            $insert['member_id'] = $this->member->data['id'];
            $insert['datetime'] = date("Y-m-d H:i:s");
            $insert['category_id'] = set_value('category');
            $insert['subcategory_id'] = set_value('subcategory');
            $insert['title'] = set_value('title');
            $insert['content'] = set_value('content');
            $insert['profile_id'] = $profile['id'];

            $this->db->insert('articles', $insert);
            $article_id = $this->db->insert_id();

            redirect("/frontend/my_account/articles");
        }
    }

    function edit_submit($article_id)
    {

        $this->form_validation->set_rules('category', 'Category', 'required|trim|xss_clean');
        $this->form_validation->set_rules('subcategory', 'Subcategory', 'required|trim|xss_clean');
        $this->form_validation->set_rules('title', 'Article Title', 'required|trim|xss_clean');
        $this->form_validation->set_rules('content', 'Article Content', 'required|trim|callback_check_article_length');

        if (!$this->form_validation->run())
        {

            $this->create_new();
        } else
        {

            // Find the profile id being used based on category
            $getProfile = $this->db->query("SELECT * FROM profiles WHERE member_id = {$this->member->data['id']} AND category_id = " . set_value('category') . " LIMIT 1");
            $profile = $getProfile->row_array();

            $insert = array();
            $insert['category_id'] = set_value('category');
            $insert['subcategory_id'] = set_value('subcategory');
            $insert['title'] = set_value('title');
            $insert['content'] = set_value('content');
            $insert['profile_id'] = $profile['id'];

            $this->db->where('id', $article_id);
            $this->db->update('articles', $insert);

            redirect("/frontend/my_account/articles");
        }
    }

    function delete($article_id)
    {

        $this->db->where('id', $article_id);
        $this->db->where('member_id', $this->member->data['id']);
        $this->db->delete('articles');

        redirect('/frontend/my_account/articles');
    }

}
