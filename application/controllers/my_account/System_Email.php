<?php

class system_email extends CI_Controller
{

    function __construct()
    {

        parent :: __construct();

        $this->settings = $this->system_vars->get_settings();

        if (!$this->session->userdata('member_logged'))
        {

            $this->session->set_flashdata('error', "You must login before you can gain access to secured areas");
            redirect('/register/login');
            exit;
        }
    }

    function index()
    {
        $params['title'] = "System Emails(Inbox)";
        $setting_email = array();
        $system_email_array = array();
        $all_messages = array();
        $system_emails = $this->db->query("SELECT * FROM system_emails_setting WHERE user_id = '{$this->member->data['id']}'")->result_array();
        foreach($system_emails as $email)
        {
            array_push($setting_email, $email["email_name"]);
        }

        $getMessages = $this->db->query("SELECT * FROM messages WHERE ricipient_id = {$this->member->data['id']} AND `type` = 'email' ORDER BY id DESC")->result_array();
        $getAllSystemNotifications = $this->db->query("SELECT * FROM system_emails ORDER BY `name` ASC")->result_array();
        foreach($getAllSystemNotifications as $item)
        {
            $data = $this->db->query("SELECT * FROM system_emails_setting WHERE user_id = '{$this->member->data['id']}' AND email_name='{$item['name']}' AND has_folder = 1")->result_array();
            if(in_array($item['name'], $setting_email) && count($data) > 0)
            {
                $item['email_type'] = $data[0]['email_type'];
                array_push($system_email_array, $item);
            }

        }

        foreach($getMessages as $key => $message)
        {
            if(in_array($message["name"], $setting_email))
            {
                $data = $this->db->query("SELECT * FROM system_emails_setting WHERE user_id = '{$this->member->data['id']}' AND email_name='{$message['name']}'")->row_array();
                $message['email_type'] = $data['email_type'];
            } else {
                $message['email_type'] = "0";
            }

            array_push($all_messages, $message);
        }

        $params['messages'] = $all_messages;
        $params['notifications']  = $system_email_array;
        $params['v_from'] = "";

        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/system_email', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $data['load_js'] = "frontend/my_account/system_email_js";

        

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function view($message_id)
    {

        $getMessages = $this->db->query("SELECT * FROM messages WHERE  id = {$message_id}");
        $t = $getMessages->row_array();

        switch ($t['type'])
        {
            case "email":
                $t['v_from'] = "System";
                break;

            case "admin":
                $t['v_from'] = "Administrator";
                break;

            case "reader":
                $sender = $this->member->get_member_data($t['sender_id']);
                $t['v_from'] = "{$sender['first_name']} {$sender['last_name']}";
                break;

            case "client":
                $sender = $this->member->get_member_data($t['sender_id']);
                $t['v_from'] = "{$sender['first_name']} {$sender['last_name']}";
                break;
        }


        $t['to'] = $this->system_vars->get_member($t['ricipient_id']);
        $t['from'] = $this->system_vars->get_member($this->member->data['id']);
        $t['title'] = "System Emails(View)";
        $this->messages_model->markMessageRead($t['id']);

        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/layouts/header_authenticated', $param_header, TRUE),
            'slider' => "",
            'content' => $this->load->view('frontend/my_account/system_messages_view', $t, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function delete($message_id)
    {

        $this->db->where('id', $message_id);
        $this->db->where('ricipient_id', $this->member->data['id']);
        $this->db->delete('messages');

        $this->session->set_flashdata('response', "Message has been deleted");

        redirect("/my_account/system_email");
    }

    function private_mail()
    {
        $params['title'] = "System Emails(Private)";

        $private_email = array();
        $system_emails = $this->db->query("SELECT * FROM system_emails_setting WHERE email_type = 1 AND user_id = '{$this->member->data['id']}'")->result_array();
        foreach($system_emails as $email)
        {
            array_push($private_email, $email["email_name"]);
        }
        $privateMessages = array();

        $getMessages = $this->db->query("SELECT * FROM messages WHERE ricipient_id = {$this->member->data['id']} AND `type` = 'email' ORDER BY id DESC")->result_array();
        foreach($getMessages as $message)
        {
            if(in_array($message["name"], $private_email))
            {
                array_push($privateMessages, $message);
            }
        }
        $params['messages'] = $privateMessages;
        $params['v_from'] = "";

        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/layouts/header_authenticated', $param_header, TRUE),
            'slider' => "",
            'content' => $this->load->view('frontend/my_account/system_email', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function trash_mail()
    {
        $params['title'] = "System Emails(Trash)";

        $trash_email = array();
        $system_emails = $this->db->query("SELECT * FROM system_emails_setting WHERE email_type = 2  AND user_id = '{$this->member->data['id']}'")->result_array();
        foreach($system_emails as $email)
        {
            array_push($trash_email, $email["email_name"]);
        }
        $trashMessages = array();

        $getMessages = $this->db->query("SELECT * FROM messages WHERE ricipient_id = {$this->member->data['id']} AND `type` = 'email' ORDER BY id DESC")->result_array();
        foreach($getMessages as $message)
        {
            if(in_array($message["name"], $trash_email))
            {
                array_push($trashMessages, $message);
            }
        }
        $params['messages'] = $trashMessages;
        $params['v_from'] = "";

        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/layouts/header_authenticated', $param_header, TRUE),
            'slider' => "",
            'content' => $this->load->view('frontend/my_account/system_email', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function email_setting()
    {
        $params['title'] = "System Emails(Setting)";

        $getMessages = $this->db->query("SELECT * FROM system_emails ORDER BY created_at DESC ");
        $params['messages'] = $getMessages->result_array();

        /*9-15
          @pen
        */
        $param_header['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_header = 1 ORDER BY sort_header ASC")->result_array();
        $param_footer['pages'] = $this->db->query("SELECT * FROM pages WHERE add_to_footer = 1 ORDER BY sort_footer ASC")->result_array();

        $data = array(
            'title' => "Psychic Contact",
            'header' => $this->load->view('frontend/partial/backend/header_reader', $param_header, TRUE),
            'sidebar' => $this->load->view('frontend/partial/backend/sidebar_client', $param_header, TRUE),
            'content' => $this->load->view('frontend/my_account/system_email_setting', $params, TRUE),
            'footer' => $this->load->view('frontend/layouts/footer', $param_footer, TRUE)
        );
        $data['load_js'] = "frontend/my_account/system_email_setting_js";

        $this->load->library('parser');
        $this->parser->parse('frontend/layouts/dashboard', $data);
    }

    function save_email_setting()
    {
        $user_id = $this->input->post("user_id");
        $private_emails = $this->input->post("private");
        $trash_emails = $this->input->post("trash");
        $has_folders = $this->input->post("has_folders");

        $now = date('Y-m-d H:i:s');

        $this->db->where("user_id", $user_id);
        $this->db->delete("system_emails_setting");

        if(count($private_emails) > 0)
        {
            foreach($private_emails as $email)
            {
                $email_setting = $this->db->query("SELECT * FROM system_emails_setting WHERE user_id = '{$user_id}' AND email_name = '{$email}'")->result_array();
                if(count($email_setting) == 0)
                {
                    $setting = array(
                        "user_id" => $user_id,
                        "email_name" => $email,
                        "email_type" => 1,
                        "created_at" => $now
                    );
                    $this->db->insert('system_emails_setting', $setting);
                } else
                {
                    $this->db->where("user_id", $user_id);
                    $this->db->where("email_name", $email);
                    $this->db->update("system_emails_setting", ["email_type" => 1, "created_at" => $now]);
                }
            }
        }

        if(count($trash_emails) > 0)
        {
            foreach($trash_emails as $email)
            {
                $email_setting = $this->db->query("SELECT * FROM system_emails_setting WHERE user_id = '{$user_id}' AND email_name = '{$email}'")->result_array();
                if(count($email_setting) == 0)
                {
                    $setting = array(
                        "user_id" => $user_id,
                        "email_name" => $email,
                        "email_type" => 2,
                        "created_at" => $now
                    );
                    $this->db->insert('system_emails_setting', $setting);
                } else
                {
                    $this->db->where("user_id", $user_id);
                    $this->db->where("email_name", $email);
                    $this->db->update("system_emails_setting", ["email_type" => 4, "created_at" => $now]);
                }
            }
        }

        if(count($has_folders) > 0)
        {
            foreach($has_folders as $email)
            {
                $email_setting = $this->db->query("SELECT * FROM system_emails_setting WHERE user_id = '{$user_id}' AND email_name = '{$email}'")->result_array();
                if(count($email_setting) == 0)
                {
                    $setting = array(
                        "user_id" => $user_id,
                        "email_name" => $email,
                        "has_folder" => 1,
                        "created_at" => $now
                    );
                    $this->db->insert('system_emails_setting', $setting);
                } else
                {
                    $this->db->where("user_id", $user_id);
                    $this->db->where("email_name", $email);
                    $this->db->update("system_emails_setting", ["has_folder" => 1, "created_at" => $now]);
                }
            }
        }

        $return['private'] = $private_emails;
        $return['trash'] = $trash_emails;
        echo json_encode($return);
    }

    function get_email_setting()
    {
        $user_id = $this->input->post("user_id");
        $email_settings = $this->db->query("SELECT * FROM system_emails_setting WHERE user_id = '{$user_id}'")->result_array();
        $private_emails = array();
        $trash_emails = array();
        $has_folders = array();

        foreach($email_settings as $setting)
        {

            switch ($setting['email_type']){
                case 1: // private
                    array_push($private_emails, $setting['email_name']);
                    break;
                case 2: // trash
                    array_push($trash_emails, $setting['email_name']);
                    break;
                case 4: // both
                    array_push($private_emails, $setting['email_name']);
                    array_push($trash_emails, $setting['email_name']);
                    break;
            }

            if($setting['has_folder'] == 1)
            {
                array_push($has_folders, $setting['email_name']);
            }
        }

        $return['private'] = $private_emails;
        $return['trash'] = $trash_emails;
        $return['has_folders'] = $has_folders;

        echo json_encode($return);

    }

    function getMessages()
    {
        $user_id = $this->input->post("user_id");

        $setting_email = array();
        $all_messages = array();
        $system_email_array = array();
        $system_emails = $this->db->query("SELECT * FROM system_emails_setting WHERE user_id = '{$user_id}'")->result_array();
        foreach($system_emails as $email)
        {
            array_push($setting_email, $email["email_name"]);
        }
        $getAllSystemNotifications = $this->db->query("SELECT * FROM system_emails ORDER BY `name` ASC")->result_array();
        $getMessages = $this->db->query("SELECT * FROM messages WHERE ricipient_id = {$this->member->data['id']} AND `type` = 'email' ORDER BY id DESC")->result_array();

        foreach($getMessages as $message)
        {
            if(in_array($message["name"], $setting_email))
            {
                $data = $this->db->query("SELECT * FROM system_emails_setting WHERE user_id = '{$this->member->data['id']}' AND email_name='{$message['name']}'")->row_array();
                $message['email_type'] = $data['email_type'];
            } else {
                $message['email_type'] = "0";
            }

            array_push($all_messages, $message);
        }

        foreach($getAllSystemNotifications as $item)
        {
            $data = $this->db->query("SELECT * FROM system_emails_setting WHERE user_id = '{$this->member->data['id']}' AND email_name='{$item['name']}' AND has_folder = 1")->result_array();
            if(in_array($item['name'], $setting_email) && count($data) > 0)
            {
                $item['email_type'] = $data[0]['email_type'];
                array_push($system_email_array, $item);
            }

        }
        $trash_system_emails = $this->db->query("SELECT * FROM system_emails_setting WHERE user_id = '{$this->member->data['id']}' AND (email_type = 2 OR email_type = 4) ")->result_array();
        $trash_folder_names = array();
        foreach($trash_system_emails as $trash_system_email) {
            array_push($trash_folder_names, $trash_system_email['email_name']);
        }

        $return['messages'] = $all_messages;
        $return['notifications'] = $system_email_array;
        $return['notifications_trash'] = $trash_folder_names;
        echo json_encode($return);
    }

    function getMember()
    {
        $member_id = $this->input->post('user_id');
        $member = $this->db->query("SELECT * FROM members WHERE id='{$member_id}'")->row_array();

        $return['member'] = $member;
    }

}
