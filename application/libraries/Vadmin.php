<?php

class vadmin
{

    public function __construct()
    {
        $this->ci = & get_instance();
    }

    function get_field_spec($tableName = null, $type = null)
    {

        $getSpec = $this->ci->db->query("SELECT * FROM vadmin_specs WHERE `table` = '{$tableName}' AND `type` = '{$type}' LIMIT 1");

        if ($getSpec->num_rows() == 0)
            return false;
        else
            return $getSpec->row_array();
    }

    function get_table_specs($tableName = null)
    {

        $getSpec = $this->ci->db->query("SELECT * FROM vadmin_specs WHERE `table` = \"{$tableName}\" ");

        if ($getSpec->num_rows() == 0)
            return false;
        else
        {

            foreach ($getSpec->result_array() as $ra) {

                if (!$ra['field'])
                {
                    $returnArray[$ra['type']] = $ra;
                } else
                {
                    $returnArray[$ra['type']][$ra['field']] = $ra;
                }
            }

            return $returnArray;
        }
    }

    function get_admin($id = null, $user = null, $pass = null)
    {

        if ($id)
        {

            $checkDb = $this->ci->db->query("SELECT * FROM vadmin_users WHERE id = '{$id}' ");

            if ($checkDb->num_rows() == 0)
            {

                return false;
            } else
            {

                return $checkDb->row_array();
            }
        } else
        {

            $superUser = $this->ci->config->item('superadmin_username');
            $superPass = $this->ci->config->item('superadmin_password');

            if ($user == $superUser && $pass == $superPass)
            {

                return true;
            } else
            {

                $checkDb = $this->ci->db->query("SELECT * FROM vadmin_users WHERE username = '{$user}' AND password = '" . md5($pass) . "' LIMIT 1");

                if ($checkDb->num_rows() == 0)
                {

                    return false;
                } else
                {

                    return $checkDb->row_array();
                }
            }
        }
    }



    public function getMenus()
    {
        $html_menu = "";
        $menu = $this->ci->db->query(" SELECT `id`, `icon`, `title`, `table`, `module_name`, `sort` 
                                        FROM vadmin_nav WHERE hidden = 0 ORDER BY `sort` ");

        foreach ($menu->result_array() as $key => $value) {

            $sub_menu = $this->ci->db->query("SELECT * FROM vadmin_navsub WHERE nav_id = {$value['id']} ORDER BY `sort` ");

            if ($sub_menu->num_rows() > 0)
            {
                $html_menu .= '<li><a href="#"><i class=" fa '. $value['icon'] .  '"></i> <span class="nav-label">' . $value['title'] .
                        '</span><span class="fa arrow"></span></a>';

                $html_menu .= '<ul class="nav nav-second-level collapse">';
                foreach ($sub_menu->result_array() as $sub_menu_value) {
                    if (!empty($value['module_name']))
                    {
                        $html_menu .= "<li><a href='/admin/{$value['module_name']}'>{$sub_menu_value['title']}</a></li>";
                    } else if(!empty ($sub_menu_value['controller'])){
                        $html_menu .= "<li><a href='/admin/{$sub_menu_value['controller']}'>{$sub_menu_value['title']}</a></li>";                        
                    } else
                    {
                        $html_menu .= "<li><a href='/admin/pages/index/{$value['id']}/{$sub_menu_value['id']}'>" .
                                $sub_menu_value['title'] . '</a></li>';
                    }
                }
                $html_menu .= "</ul></li>";
            } else
            {
                $html_menu .= '<li> <a href="/admin/pages/index/' . $value['id'] . '"><i class="fa fa-globe"></i> <span class="nav-label">' .
                        $value['title'] . '</span><span class="fa arrow"></span></a></li>';
            
}
        }

        return $html_menu;
    }

}

?>
