<?php

class html
{
    public $isRequired = false;
    function config($name, $value, $params = null)
    {
        $this->ci = & get_instance();

        $this->name = $name;
        $this->value = $value;
        $this->isRequired = true;
    }

    function field_view()
    {

        return "<textarea class='tinymce' name='{$this->name}'>{$this->value}</textarea>";
    }

    function display_view()
    {

        return $this->value;
    }

    function process_form()
    {

        return $this->value;
    }

}

?>