<?php

	class sb
	{
	
		function config($name, $value, $params = null)
	    {
	    
	        $this->ci =& get_instance();
	        
	        $this->name = $name;
			$this->value = $value;
			
			// Count total params after 0
			$this->param_array = $params;
	        
	    }
		
		function field_view()
		{
			$diff_val = '';
			$return = "<select name='{$this->name}' class='form-control'>";
			
			foreach($this->param_array as $i=>$pa)
			{
				if(trim($pa) && $i>0){
					//diff val means different label from the value
					if($i == 1 && $pa == 'diff_val'){
						$diff_val = 1;
						$return .= "<option value=''".($this->value==$pa ? " selected" : "").">No Reader</option>";
					}
					
					if($diff_val == 1 && $i>1){
						$getvalue = explode(",",$pa);
						$label = $getvalue[1]; 
						$pa = $getvalue[0];
						$return .= "<option value='{$pa}'".($this->value==$pa ? " selected" : "").">{$label}</option>";
					}else{
						if($diff_val != 1){
							$return .= "<option value='{$pa}'".($this->value==$pa ? " selected" : "").">{$pa}</option>";
						}
					}      
				}

			}
			
			$return .= "</select>";

		
			return $return;
		
		}
		
		function display_view()
		{
		
			return $this->value;
		
		}
		
		function process_form()
		{
			
			return $this->value;
			
		}
	
	}

?>