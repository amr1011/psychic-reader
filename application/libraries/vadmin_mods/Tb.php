<?php

class tb
{
    public $isRequired = false;
    function config($name, $value, $params = null)
    {
        $this->ci = & get_instance();

        $this->name = $name;
        $this->value = $value;
        $this->size = (isset($params[1]) ? $params[1] : '50');        
        $this->isRequired = (isset($params[2]) ? $params[2] : '');
        $this->tbtype = (isset($params[3]) ? $params[3] : 'text');
        if ($name == 'title')
        {
            $this->isRequired = 'required';
        }
        
    }

    function field_view()
    {
        if($this->name == 'email' || $this->name == 'username' || $this->name=='first_name' || $this->name=='last_name' || $this->name=='dob') {

            return "<input type='text' autocomplete='off' name='{$this->name}' value='{$this->value}' class='form-control' required>";
        } else {

            return "<input type='text' autocomplete='off' name='{$this->name}' value='{$this->value}' class='form-control'>";
        }
    }

    function display_view()
    {

        return $this->value;
    }

    function process_form()
    {
        return $this->value;   
    }

}

?>