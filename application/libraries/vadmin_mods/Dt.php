<?php

class dt
{
    function config($name, $value, $params = null)
    {
        $this->ci = & get_instance();
        $this->name = $name;
        $this->value = $value;
    }

    function field_view()
    {
        if($this->name == 'day_of_birth' || $this->name == 'dob' || $this->name == 'date_updated' || $this->name == 'appointment_date')
        {
            if ($this->value && $this->value != '0000-00-00 00:00:00')
                $value = date('m-d-Y', strtotime($this->value));
            else
                $value = '';

            $readonly_arr = array('date_created', 'date_updated');
            $readonly = '';
        
            if(in_array($this->name, $readonly_arr)){
                $readonly = 'readonly';
            }

            return "<input type='text' name='{$this->name}' value='{$value}' class='date-dob form-control' $readonly>";

        } else
        {
            if ($this->value)
                $value = date('m/d/Y @ h:i A', strtotime($this->value));
            else
                $value = date("m/d/Y @ h:i A");

            $readonly_arr = array('date_created', 'date_updated');
            $readonly = '';
        
            if(in_array($this->name, $readonly_arr)){
                $readonly = 'readonly';
            }

            return "<input type='text' name='{$this->name}' value='{$value}' class='datetime form-control' $readonly>";
        }

    }

    function display_view()
    {

        if($this->name == 'day_of_birth' || $this->name == 'appointment_date')
        {
            $value = str_replace('@', '', $this->value);
            $a = 'How are you?';

            if ($this->value == null || $this->value == '0000-00-00 00:00:00')
                return "";
            else
                return date('m-d-Y', strtotime($value));
        } else
        {
            $value = str_replace('@', '', $this->value);

            if ($this->value == null || $this->value == '0000-00-00 00:00:00')
                return "";
            else
                return date('m/d/Y @ h:i A', strtotime($value));
        }
    }

    function process_form()
    {
        $value = str_replace('@', '', $this->value);

        $return_val = date('Y-m-d H:i:s', strtotime($value));

        if ($this->value == null || $this->value == '0000-00-00 00:00:00')
            if($this->name == 'Date Created'){
                return $return_val;
            }else{
                return "";
            }
        else
            return $return_val;
    }
}

?>