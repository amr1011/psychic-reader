<?php

class cb
{
    function config($name, $value, $params = null)
    {
    
        $this->ci =& get_instance();
        
        $this->name = $name;
        $this->value = $value;
        
        // Count total params after 0
        $this->param_array = $params;
        
    }
    
    function field_view()
    {
        $return = '';
     
        $diff_val = '';
        $ctr = 0;
        $chk  ='';
        foreach($this->param_array as $i=>$pa)
        {  
           if(trim($pa) && $i>0){   
                //diff val means different label from the value
                if($i == 1 && $pa == 'diff_val'){
                    $diff_val = 1;
                }

                if($diff_val == 1 && $i>1){
                    $getvalue = explode(",",$pa);
                    $label = $getvalue[1]; 
                    $pa = $getvalue[0];

                    //explode save value from db
                    $val_arr =  explode(",",$this->value);
                    $search = in_array($pa,$val_arr);
                    
                    if($search == 1){
                        $chk = 'checked';
                    }

                    $return .= "<div class='cb-con'><input type='checkbox' name='{$this->name}[]' value='{$pa}' {$chk} class='form-check-input checkbox-s'><span class='checkboxtext'>{$label}</span></div>";
                    $chk = '';
                }else{
                    if($diff_val != 1){
                        $return .= "<div class='cb-con'><input type='checkbox' name='{$this->name}[]' value='{$pa}'".($this->value==$pa ? " checked" : "")." class='form-check-input checkbox-s'><span class='checkboxtext'>{$pa}</span></div>";
                    }
                }               
           } 
           
        }
        return $return;
    }
    
    function display_view()
    {
    
        return $this->value;
    
    }
    
    function process_form()
    {
        return implode(",",$this->value); 
        
    }



}

?>