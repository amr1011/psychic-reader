<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
**
*/
class My_common
{
	var $member_logged = null;
	var $CI = null;
	
	function __construct()
	{
		$this->CI =& get_instance();
		$this->member_logged =  $this->CI->session->userdata('member_logged');

	}

	public function user_ban_type_by_reader($reader_id=null){

		if (!$reader_id || !$this->member_logged) 
			return;
		$u = $this->CI->db->query("SELECT DISTINCT reader_id, type FROM member_bans where member_id={$this->member_logged} AND reader_id={$reader_id} ");

        if ($u->num_rows() > 0)
        {
            $member = $u->result();
            return $member[0]->type;
        }
	}
	public function is_on_full_ban(){

		$u = null;
		if ($this->member_logged) {
			$u = $this->CI->db->query("SELECT DISTINCT reader_id, type FROM member_bans where member_id={$this->member_logged} AND type='full' ");
		}

		if ($u!=null) {
			if ($u->num_rows() > 0)
			{
				return true;
			}else{
				return false;
			}
		}else{
				return false;
			}
        
	}

}