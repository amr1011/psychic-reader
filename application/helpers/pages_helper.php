<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function parseFields($table, $f, $specs)
{
    $ci = & get_instance();
    // Get Field Based On SPEC Data
    $spec_type = (isset($specs['spec'][$f]) ? $specs['spec'][$f]['value'] : "TB||50");
    $spec_array = explode("||", $spec_type);

    // Load & Configure The Module
    $specMod = strtolower(trim($spec_array[0]));
    if ($f == 'id')
        $specMod = 'lb';

    // Load Field
    $ci->$specMod->config($f, $table[$f], $spec_array);
    return $ci->$specMod->display_view();
}

function getTransactionLevel($table, $nav)
{
    if (!empty($nav['id']) && $nav['id'] == '13')
    {

        $transactionLabel = "";

        switch ($table['settled'])
        {

            case "voided":
                $transactionType = 'voided';
                $transactionLabel = "<span class='label label-important'>Voided</span>";
                break;

            case "settled":
                $transactionType = 'settled';
                $transactionLabel = "<span class='label label-success'>Settled</span>";
                break;

            default:
                $transactionType = 'pending';
                $transactionLabel = "<span class='label label-warning'>Pending</span>";
                break;
        }

        return $transactionLabel;
    }
}

function getFieldValue($f, $specs, $data)
{
    $ci = & get_instance();
 
    $formattedField = (isset($specs['title'][$f]) ? $specs['title'][$f]['value'] : ucwords(str_replace("_", " ", $f)));

    $caption = (isset($specs['caption'][$f]) ? "<div class='caption'>{$specs['caption'][$f]['value']}</div>" : "");

    // Get Field Based On SPEC Data
    $spec_type = (isset($specs['spec'][$f]) ? $specs['spec'][$f]['value'] : "TB||50");
    $spec_array = explode("||", $spec_type);

    // Load & Configure The Module
    $specMod = strtolower(trim($spec_array[0]));
    if ($f == 'id')
        $specMod = 'lb';

    // Load Field
    $ci->$specMod->config($f, (empty($data[$f]) ? null : $data[$f]), $spec_array);
    $fieldView = $ci->$specMod->field_view();

    $tmp["formattedField"] = $formattedField;
    $tmp["caption"] = $caption;
    $tmp["fieldView"] = $fieldView;

    return $tmp;
}

function isBanned($userId)
{
    $ci = & get_instance();
    $ci->db->where("id", $userId);
    $query = $ci->db->get("members");
    if ($query->num_rows() > 0)
    {
        $row = $query->row();
        return $row->banned;
    }

    return false;
}

function isFeatured($userId)
{
    $ci = & get_instance();
    $ci->db->where("member_id", $userId);
    $ci->db->where("featured", '1');
    $query = $ci->db->get("member_profiles");
    if ($query->num_rows() > 0)
    {
        return true;
    }

    return false;
}

/**
 * Truncates the given string at the specified length.
 *
 * @param string $str The input string.
 * @param int $width The number of chars at which the string will be truncated.
 * @return string
 */
function truncate($str, $width) {
    return strtok(wordwrap($str, $width, "...\n"), "\n");
}
