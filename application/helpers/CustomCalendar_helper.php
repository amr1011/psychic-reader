<?php


/**

*/
function generate_year_options()
{
	$_year = range(1930, date('Y') + 50);
	$year_selection = '';
	foreach($_year as $value) {
		$year_selection .= '<option value="'. $value . '">'.  $value .'</option>';
	}
	return $year_selection;

}

function generate_day_options()
{
	$_days = range(1, 31);
	$days_selection = '';
	foreach($_days as $value) {
		$days_selection .= '<option value="'. $value . '">'.  $value .'</option>';
	}
	return $days_selection;

}