<?php
    $unread = $this->messages_model->getUnreadMessages();
    $badge = "";
    if ($unread > 0)
    {
        $badge = "<span class=\"badge badge-important\" id=\"email-count\">{$unread}</span>";
    }

    $unread_system_email = $this->messages_model->getUnreadSystemMessages();
    $unread_email = "";
    if($unread_system_email > 0)
    {
        $unread_email = "<span class=\"badge badge-important\" id=\"system_email-count\">{$unread_system_email}</span>";
    }
?>


<div class="sidebar" data-color="purple" data-background-color="black" data-image="/theme/template/images/sidebar-2.jpg">

  <div class="logo">
    <a href="/" class="simple-text logo-normal">PSYCHIC CONTACT</a>
  </div>

  <div class="sidebar-wrapper">
    

    <ul class="nav">
      <?php if ($this->member->data['profile_id']) : ?>

      <li class="nav-item">
        <a class="nav-link" href="/my_account/messages/compose/1/send">
          <i class="material-icons">message</i>
          <p> Contact Admin </p>
        </a>
      </li>                


        <li class="nav-item <?php echo $this->router->fetch_class() == 'main' && $this->router->fetch_method() == 'index' ? 'active' : ''; ?> ">
            <a class="nav-link" href="/my_account">
              <i class="material-icons">dashboard</i>
              <p> Dashboard </p>
        </a>

        <li class="nav-item <?php echo $this->router->fetch_method() == 'nrr' ? 'active' : ''; ?> ">
            <a class="nav-link" href="/my_account/main/nrr">
              <i class="material-icons">send</i>
              <p> Client NRR </p>
            </a>
        </li>

       <li class="nav-item <?php echo $this->router->fetch_class() == 'chats' ? 'active' : ''; ?>">
            <a class="nav-link" href="/my_account/chats">
              <i class="material-icons">history</i>
              <p> Chat History </p>
            </a>
        </li>

       <li class="nav-item <?php echo $this->router->fetch_method() == 'short_chats' ? 'active' : ''; ?>">
            <a class="nav-link" href="/my_account/main/short_chats">
              <i class="material-icons">chat</i>
              <p> Short Chat </p>
            </a>
        </li>

      <li class="nav-item <?php echo $this->router->fetch_method() == 'client_list' ? 'active' : ''; ?>">
            <a class="nav-link" href="/my_account/main/client_list">
              <i class="material-icons">list_alt</i>
              <p> Client List </p>
            </a>
        </li>

        <li class="nav-item <?php echo $this->router->fetch_class() == 'ban_users' ? 'active' : ''; ?>">
            <a class="nav-link" href="/my_account/ban_users">
              <i class="material-icons">block</i>
              <p> Personal Ban </p>
            </a>
        </li>


       <li class="nav-item <?php echo $this->router->fetch_class() == 'articles' ? 'active' : ''; ?>">
            <a class="nav-link" href="/my_account/articles/index">
              <i class="material-icons">pages</i>
              <p> Submit Article </p>
            </a>
        </li>


        <li class="nav-item <?php echo $this->router->fetch_class() == 'email_readings' ? 'active' : ''; ?>">
            <a class="nav-link" href="/my_account/email_readings/open_requests">
              <i class="material-icons">email</i>
              <p> Email Readings </p>
            </a>
        </li>


        <li class="nav-item <?php echo $this->router->fetch_method() == 'appointment_list' ? 'active' : ''; ?>">
            <a class="nav-link" href="/my_account/main/appointment_list/<?php echo($this->member->data['profile_id']);?>">
              <i class="material-icons">calendar_today</i>
              <p> Appointments </p>
            </a>
        </li>

        <li class="nav-item <?php echo $this->router->fetch_method() == 'blog_management' ? 'active' : ''; ?>">
            <a class="nav-link" href="/my_account/main/blog_management">
              <i class="material-icons">pages</i>
              <p> Blog Management </p>
            </a>
        </li>


        <li class="nav-item <?php echo $this->router->fetch_method() == 'note_list' ? 'active' : ''; ?>">
            <a class="nav-link" href="/my_account/main/note_list/<?php echo($this->member->data['profile_id']);?>">
              <i class="material-icons">note_add</i>
              <p> Notes </p>
            </a>
        </li>


        <li class="nav-item <?php echo $this->router->fetch_method() == 'give_back' ? 'active' : ''; ?>">
            <a class="nav-link" href="/my_account/main/give_back/<?php echo($this->member->data['profile_id']);?>">
              <i class="material-icons">person_add</i>
              <p> Sunday AM Giveback </p>
            </a>
        </li>

        <li class="nav-item <?php echo $this->router->fetch_method() == 'give_back_history' ? 'active' : ''; ?>">
            <a class="nav-link" href="/my_account/main/give_back_history/<?php echo($this->member->data['profile_id']);?>">
              <i class="material-icons">person_add</i>
              <p> Giveback History </p>
            </a>
        </li>


        <li class="nav-item <?php echo $this->router->fetch_method() == 'testimonials' ? 'active' : ''; ?>">
            <a class="nav-link" href="/my_account/main/testimonials">
              <i class="material-icons">star</i>
              <p> Testimonials </p>
            </a>
        </li>

        <li class="nav-item <?php echo $this->router->fetch_class() == 'trancepad' ? 'active' : ''; ?>">
            <a class="nav-link" href="/my_account/trancepad">
              <i class="material-icons">web</i>
              <p> Trancepad </p>
            </a>
        </li>


        <li class="nav-item <?php echo $this->router->fetch_class() == 'edit_profile' ? 'active' : ''; ?>">
            <a class="nav-link" href="/my_account/main/edit_profile/<?php echo $this->member->data['username']; ?>">
              <i class="material-icons">pageview</i>
              <p> View My Psychic Profile </p>
            </a>
        </li>

        <li class="nav-item <?php echo $this->router->fetch_class() == 'fund_your_account' && $this->router->fetch_method() != 'time_back' ? 'active' : ''; ?>">
            <a class="nav-link" href="/my_account/transactions/index">
              <i class="material-icons">money</i>
              <p> Earning & Transaction </p>
            </a>
        </li>
        

        <li class="nav-item <?php echo $this->router->fetch_class() == 'messages' ? 'active' : ''; ?>">
            <a class="nav-link" href="/my_account/messages">
              <i class="material-icons">message</i>
              <p> Message Center <?php echo $badge; ?> </p>
            </a>
        </li>


        <li class="nav-item <?php echo $this->router->fetch_class() == 'promessages' ? 'active' : ''; ?>">
            <a class="nav-link" href="/my_account/promessages">
              <i class="material-icons">message</i>
              <p> Pro Message Center </p>
            </a>
        </li>
        

        <li class="nav-item <?php echo $this->router->fetch_class() == 'system_email' ? 'active' : ''; ?>">
            <a class="nav-link" href="/my_account/system_email">
              <i class="material-icons">notifications</i>
              <p> Notification Board <?php echo $unread_email; ?> </p>
            </a>
        </li>        



      <?php else: ?>


        <li class="nav-item <?php echo $this->router->fetch_class() == 'main' && 
                                  $this->router->fetch_method() == 'index' ? 'active' : ''; ?> ">
            <a class="nav-link" href="/my_account">
              <i class="material-icons">dashboard</i>
              <p> Dashboard </p>
            </a>
        </li>

        <li class="nav-item <?php echo $this->router->fetch_method() == 'fund_your_account' ? 'active' : ''; ?> ">
            <a class="nav-link" href="/my_account/transactions/fund_your_account">
              <i class="material-icons">payment</i>
              <p> Fund My Account </p>
            </a>
        </li>

        <li class="nav-item <?php echo $this->router->fetch_class() == 'psychic_readers' ? 'active' : ''; ?> ">
            <a class="nav-link" href="/my_account/psychic_readers">
              <i class="material-icons">chat</i>
              <p> Start Chat </p>
            </a>
        </li>      

        <li class="nav-item <?php echo $this->router->fetch_method() == 'client_emails' ? 'active' : ''; ?> ">
            <a class="nav-link" href="/my_account/email_readings/client_emails">
              <i class="material-icons">email</i>
              <p> My Email Readings </p>
            </a>
        </li>      

        <li class="nav-item <?php echo $this->router->fetch_class() == 'chats' ? 'active' : ''; ?> ">
            <a class="nav-link" href="/my_account/chats">
              <i class="material-icons">history</i>
              <p> Chat History </p>
            </a>
        </li>


        <li class="nav-item <?php echo $this->router->fetch_method() == 'page_readers' ? 'active' : ''; ?> ">
            <a class="nav-link" href="/my_account/main/page_readers">
              <i class="material-icons">send</i>
              <p> Page Readers </p>
            </a>
        </li>

        <li class="nav-item <?php echo $this->router->fetch_method() == 'nrr' ? 'active' : ''; ?> ">
            <a class="nav-link" href="/my_account/main/nrr">
              <i class="material-icons">send</i>
              <p> NRR Form </p>
            </a>
        </li>
        <li class="nav-item <?php echo $this->router->fetch_class() == 'favorites' ? 'active' : ''; ?> ">
            <a class="nav-link" href="/my_account/favorites">
              <i class="material-icons">bookmark</i>
              <p> Favorite Readers </p>
            </a>
        </li>

        <li class="nav-item ">
          <a class="nav-link" href="/my_account/messages/compose/1/send">
            <i class="material-icons">contact_support</i>
            <p> Contact Admin </p>
          </a>
        </li>

        <li class="nav-item <?php echo $this->router->fetch_class() == 'transactions' && $this->router->fetch_method() == 'index' ? 'active' : ''; ?> ">
          <a class="nav-link" href="/my_account/transactions">
            <i class="material-icons">receipt</i>
            <p> Billing & Transactions </p>
          </a>
        </li>


        <li class="nav-item <?php echo $this->router->fetch_method() == 'testimonial' ? 'active' : ''; ?> ">
          <a class="nav-link" href="/my_account/main/testimonial">
            <i class="material-icons">star</i>
            <p> Submit Testimonial </p>
          </a>
        </li>
        

        <li class="nav-item <?php echo $this->router->fetch_class() == 'messages' ? 'active' : ''; ?> ">
          <a class="nav-link" href="/my_account/messages">
            <i class="material-icons">message</i>
            <p> Message Center <?php echo $badge; ?> </p>
          </a>
        </li>        
         

        <li class="nav-item <?php echo $this->router->fetch_class() == 'system_email' ? 'active' : ''; ?> ">
          <a class="nav-link" href="/my_account/system_email">
            <i class="material-icons">notifications</i>
            <p> Notification Board <?php echo $unread_email; ?> </p>
          </a>
        </li>        



          <!--
          <li class="<?php echo $this->router->fetch_class() == 'account' ? 'active' : ''; ?>"><a href='/my_account/account'>Edit My Account</a></li>
          -->

      <?php endif; ?>       


     

    </ul>
  </div>

</div>






 

<!--     <?php if($this->member->data['id'] == 1) :?>
    <li><a href="/admin">Back To Admin Tool</a></li>
    <?php endif; ?>
    <li><a href="/main/logout">Logout</a></li> -->
