<!--     *********    TEAM 1     *********      -->
<div class="team-1" id="team-1">
    <div class="container">
  
        <div class="row">
            <div class="text-center">
                <h2 class="title">Psychic Contact Email Readings ("E-Readings")</h2>
                <h5 class="description">Login to read more information about each reader and select your favorite..</h5>
            </div>
        </div>

        <div class="row">

            <div class="title">
                    <p>Are you in need of advice on an important issue or decision that's making you feel, confused, worried or stressed?</p>
                    <p>Our amazing Psychics can help you in all the areas of your life.</p>
                    <h4>No time for having a Chat or Phone Reading?</h4>
                    <h4>No problem!</h4>
                    <p>We know our clients may be busy and perhaps don't have the time needed for an online psychic chat or intuitive phone reading session.</p>
                    <p>Or maybe you prefer sitting back, and taking your time to Read your accurate Email Reading from one of our gifted Psychic Life Coaches or Astrologers.</p>
                    <h4>How To Order Your "E-Reading"?</h4>
                    <ul>
                        <li>Step 1: Log on to your account @ Psychic-Contact.com (or create a new account if you do not have one yet)</li>
                        <li>Step 2: Choose your Reader (we suggest studying their online profiles)</li>
                        <li>Step 3: Choose the "type" of Reading you want to order</li>
                        <li>Step 4: When filling out your order form - Please take your time and enter in your questions and/or concerns where prompted</li>
                        <li>Step 5: Make your payment online</li>
                        <li>Step 6: Submit your order</li>
                        <li>Step 7: Wait for your completed Reading to arrive in your private inbox as well as on your account page on our site (only you can view the Reading from there!)</li>
                    </ul>
                    <h4>Tips On Getting Your Email Reading:</h4>
                    <ul>
                        <li>Relax and try some "quiet deep breathing" so that you feel "connected".</li>
                        <li>Make sure you have your questions clearly thought-out in advance.</li>
                        <li>Ask your question concisely for optimum results.</li>
                        <li>It's important that you think about what is happening in your life as you begin to put together your questions for your Psychic or Life Coaching Reading.</li>
                        <li>The Reader you choose will tune in to your question, and you will receive an exceptionally accurate Reading.</li>
                    </ul>
                    <h4><b>Note: Most Email readings will be completed within 24-48 hours unless payment funds are not cleared or your reader specifically states a longer time frame for completion of your reading on their profile
                            (they may contact you for more info as soon as they receive your order - so please check your email soon).</b>
                    </h4>
                    <h4>Please feel free to make contact to the Site Admin, if after 48 hours has passed without reply from your Reader.</h4>
                    <h4>To get started please fill out this account setup form:</h4>
            </div>



<div class="content">

        <div class='form-container'>
            <h4>If you live in Canada, please use our Canadian site <a href="https://www.psychic-contact.ca">www.psychic-contact.ca</a></h4>
            <div class="row">
                <div class="col-lg-12 col-md-6 col-sm-6">
                    <b>Do you have your account?</b> <a href="/register/login">Click Here to Sign In</a>
                </div>
            </div>
            <form id="form-data" name="contact-information" action="/register/submit" method="post" class="form-horizontal">

                <div class="form-group">
                    <label class="col-sm-4 control-label">Email Address <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input type='text' name='email' class="form-control " required value='<?= set_value('email') ?>' >
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Username <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input type='text' name='username' class="form-control" required  value='<?= set_value('username') ?>' >
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Password <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input type="password" class="form-control" name="password" required id="password" value='<?= set_value('password') ?>'>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Re-type password <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input type="password" class="form-control" name="password2" required id="password2" value='<?= set_value('password2') ?>'>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">First Name <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input type='text' name='first_name' class="form-control" required value='<?= set_value('first_name') ?>' >
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Last Name <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input type='text' name='last_name' class="form-control" required value='<?= set_value('last_name') ?>' >
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Sex <i>(at time of birth) </i>  <span class="text-danger">*</span></label>
                    <div class="col-sm-5">
                        <input type="radio" name="gender" id="sex-male" value="sex-male">
                        <label for="sex-male" class="control-label">Male </label>

                        <input type="radio" name="gender" id="sex-female" value="sex-female" checked>
                        <label for="sex-female" class="control-label"> Female </label>
                    </div>
                </div>

                <div class=" form-row">
                    <label class="col-sm-2 control-label">Date of Birth  <span class="text-danger">*</span></label>
                </div>
                <div class=" form-row">
                    <div class="form-group col-md-3">
                      <label for="inputState">Month</label>
                            <select name="dob_month" class="form-control" required >
                                <option value="" selected disabled>Month </option>
                                <option value="1">January</option>
                                <option value="2">February</option>
                                <option value="3">March</option>
                                <option value="4">April</option>
                                <option value="5">May</option>
                                <option value="6">June</option>
                                <option value="7">July</option>
                                <option value="8">August</option>
                                <option value="9">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                            </select>
                    </div>
                    <div class="form-group col-md-3">
                      <label for="inputState">Year</label>
                       <select name="dob_year" class="form-control" required style="width: 90px;">
                                <option value="" selected disabled>Year</option>
                                <?php echo $year; ?>
                            </select>
                    </div>
                    <div class="form-group col-md-3">
                      <label for="inputState">Day</label>
                      <select name="dob_day" class="form-control" required style="width: 60px;">
                                <option value="" selected disabled> Day </option>
                                <?php echo $days; ?>
                            </select>
                    </div>
                  </div>

                

                <div class="form-group"><label class="col-sm-4 control-label">Country <span class="text-danger">*</span></label>
                    <div class="col-sm-5">
                        <select name="country" class="form-control" required style="width: 160px;">
                            <option value="" selected disabled>Select Country</option>
                            <?php foreach($countries as $country) {?>
                                <option value="<?php echo($country['code']);?>"><?php echo($country['name']);?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-sm-4 control-label">Newsletter</label>
                    <div class="col-sm-1">
                        <input type='checkbox'   class="form-control"  name='newsletter' value='1' <?= set_checkbox('newsletter', '1', TRUE) ?>
                        <label for="is_sales" class="form-control"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Terms</label>
                    <div class="col-sm-1">
                        <input type='checkbox' class="form-control"  name='terms' value='1' <?= set_checkbox('terms', '1') ?>
                        <label for="is_sales" class="form-control"></label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label"></label>
                    <label class="col-sm-8 control-label">I have read and agreed to all the Member <a href='/terms' target='_blank'>Terms and Conditions</a> </label>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label"></label>
                    <div class="col-sm-6">
                        <div class="g-recaptcha" data-sitekey="6LfgyacUAAAAAC2sk_Uf8oVs3MlPhuGbPYZnkNX8"></div>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-3"></div>
                    <div class="form-data__buttoms more-info__buttons">
                        <input type="submit" name="submit" value='Register' class='btn btn-primary'>
                    </div>
                </div>

            </form>

        </div>

        <div class="col-md-4">
            <?php //$this->load->view('frontend/pages/online_readers'); ?>
        </div>
    </div>



        </div>
    </div>
</div>    