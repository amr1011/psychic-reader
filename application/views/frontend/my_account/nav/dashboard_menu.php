<ul class="nav nav-pills nav-stacked">
    <?php
    $unread = $this->messages_model->getUnreadMessages();
    $badge = "";
    if ($unread > 0)
    {
        $badge = "<span class=\"badge badge-important\" id=\"email-count\">{$unread}</span>";
    }

    $unread_system_email = $this->messages_model->getUnreadSystemMessages();
    $unread_email = "";
    if($unread_system_email > 0)
    {
        $unread_email = "<span class=\"badge badge-important\" id=\"system_email-count\">{$unread_system_email}</span>";
    }
    ?>

    <?php if ($this->member->data['profile_id']) : ?>
        <li ><a class="btn btn-success active" href="/my_account/messages/compose/1/send">Contact Admin</a></li></hr>
        <li class="<?php echo $this->router->fetch_class() == 'main' && $this->router->fetch_method() == 'index' ? 'active' : ''; ?>"><a href="/my_account">Dashboard</a></li>
        <li class="<?php echo $this->router->fetch_method() == 'edit_profile' ? 'active' : ''; ?>"><a href="/my_account/main/edit_profile">Edit My Profile</a></li>
        <li class="<?php echo $this->router->fetch_class() == 'nrr' ? 'active' : ''; ?>"><a href="/my_account/nrr">Client NRR</a></li>
        <li class="<?php echo $this->router->fetch_class() == 'ban_users' ? 'active' : ''; ?>"><a href="/my_account/ban_users">P-Ban Users</a></li>
        <li class="<?php echo $this->router->fetch_class() == 'articles' ? 'active' : ''; ?>"><a href="/my_account/articles/index">Submit Article</a></li>
        <li class="<?php echo $this->router->fetch_class() == 'chats' ? 'active' : ''; ?>"><a href="/my_account/chats">Chat History</a></li>
        <li class="<?php echo $this->router->fetch_method() == 'short_chats' ? 'active' : ''; ?>"><a href="/my_account/main/short_chats">Short Chat</a></li>
        <li class="<?php echo $this->router->fetch_method() == 'client_list' ? 'active' : ''; ?>"><a href="/my_account/main/client_list">Client List</a></li>
        <li class="<?php echo $this->router->fetch_class() == 'email_readings' ? 'active' : ''; ?>"><a href="/my_account/email_readings/open_requests">Email Readings</a></li>
        
        <!-- 9-4 -->
        <li class="<?php echo $this->router->fetch_method() == 'blog_management' ? 'active' : ''; ?>"><a href="/my_account/main/blog_management">Blog Management</a></li>
        <li class="<?php echo $this->router->fetch_method() == 'appointment_list' ? 'active' : ''; ?>"><a href="/my_account/main/appointment_list/<?php echo($this->member->data['profile_id']);?>">Appointments</a></li>
        <!-- 9-4 end -->
        <!-- 9-2 -->
        <li class="<?php echo $this->router->fetch_method() == 'note_list' ? 'active' : ''; ?>"><a href="/my_account/main/note_list/<?php echo($this->member->data['profile_id']);?>">Notes</a></li>
        <!-- 9-2 end -->
        <!-- 9-12 -->
        <li class="<?php echo $this->router->fetch_method() == 'give_back' ? 'active' : ''; ?>"><a href="/my_account/main/give_back/<?php echo($this->member->data['profile_id']);?>">Sunday AM Giveback</a></li>
        <!-- 9-12 end -->
        <!-- 9-22 -->
        <li class="<?php echo $this->router->fetch_method() == 'give_back_history' ? 'active' : ''; ?>"><a href="/my_account/main/give_back_history/<?php echo($this->member->data['profile_id']);?>">Giveback History</a></li>
        <!-- 9-22 end -->
        <li class="<?php echo $this->router->fetch_method() == 'testimonials' ? 'active' : ''; ?>"><a href="/my_account/main/testimonials">Testimonials</a></li>
        <li class="<?php echo $this->router->fetch_class() == 'trancepad' ? 'active' : ''; ?>"><a href="/my_account/trancepad">Trancepad</a></li>
        <li><a href="/profile/<?php echo $this->member->data['username']; ?>">View My Psychic Profile</a></li>
        <li class="<?php echo $this->router->fetch_class() == 'transactions' && $this->router->fetch_method() != 'time_back' ? 'active' : ''; ?>"><a href="/my_account/transactions/index">Earning & Transaction</a></li>
        <li class="<?php echo $this->router->fetch_class() == 'messages' ? 'active' : ''; ?>"><a href="/my_account/messages">Message Center <?php echo $badge; ?></a></li>
        <li class="<?php echo $this->router->fetch_class() == 'promessages' ? 'active' : ''; ?>"><a href="/my_account/promessages">Pro-Message Center</a></li>
        <li class="<?php echo $this->router->fetch_class() == 'system_email' ? 'active' : ''; ?>"><a href="/my_account/system_email">Notification Board <?php echo $unread_email; ?></a></li>

    <?php else: ?>
        <li ><a class="btn btn-success active" href="/my_account/messages/compose/1/send">Contact Admin</a></li></hr>
        <li class="<?php echo $this->router->fetch_class() == 'main' && $this->router->fetch_method() == 'index' ? 'active' : ''; ?>"><a href='/my_account'>Dashboard</a></li>
        <li class="<?php echo $this->router->fetch_method() == 'fund_your_account' ? 'active' : ''; ?>"><a href='/my_account/transactions/fund_your_account'>Fund My Account</a></li>
        <li class="<?php echo $this->router->fetch_method() == 'nrr' ? 'active' : ''; ?>"><a href='/my_account/main/nrr'>NRR</a></li>
        <li class="<?php echo $this->router->fetch_class() == 'psychics' ? 'active' : ''; ?>"><a href='/psychics'>Start Chat</a></li>
        <li class="<?php echo $this->router->fetch_method() == 'client_emails' ? 'active' : ''; ?>"><a href='/my_account/email_readings/client_emails'>My Email Readings</a></li>
        <li class="<?php echo $this->router->fetch_class() == 'chats' ? 'active' : ''; ?>"><a href='/my_account/chats'>Chat History</a></li>
        <li class="<?php echo $this->router->fetch_method() == 'page_readers' ? 'active' : ''; ?>"><a href='/my_account/main/page_readers'>Page Readers</a></li>
        <li class="<?php echo $this->router->fetch_class() == 'favorites' ? 'active' : ''; ?>"><a href='/my_account/favorites'>Favorite Readers</a></li>
        <li class="<?php echo $this->router->fetch_class() == 'transactions' && $this->router->fetch_method() == 'index' ? 'active' : ''; ?>"><a href='/my_account/transactions'>Billing & Transactions</a></li>
        <li class="<?php echo $this->router->fetch_method() == 'testimonial_list' ? 'active' : ''; ?>"><a href='/my_account/main/testimonial_list'>Testimonials</a></li>
        <li class="<?php echo $this->router->fetch_method() == 'testimonial' ? 'active' : ''; ?>"><a href='/my_account/main/testimonial'>Submit Testimonial</a></li>
        <li class="<?php echo $this->router->fetch_class() == 'messages' ? 'active' : ''; ?>"><a href='/my_account/messages'>Message Center <?php echo $badge; ?></a></li>
        <li class="<?php echo $this->router->fetch_class() == 'account' ? 'active' : ''; ?>"><a href='/my_account/account'>Edit My Account</a></li>
        <li class="<?php echo $this->router->fetch_class() == 'system_email' ? 'active' : ''; ?>"><a href="/my_account/system_email">Notification Board <?php echo $unread_email; ?></a></li>
    <?php endif; ?>
    <?php if($this->member->data['id'] == 1) :?>
    <li><a href="/admin">Back To Admin Tool</a></li>
    <?php endif; ?>
    <li><a href="/main/logout">Logout</a></li>
</ul>