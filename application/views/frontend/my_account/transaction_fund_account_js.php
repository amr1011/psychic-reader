
<script src="/theme/admin/js/notify.min.js"></script>  <!-- thom ho8-5 -->
<script>

    $(document).ready(function () {

        if($('p.result').html()) {

            $.notify($('p.result').html(), {className:"error",align:"center", verticalAlign:"top", hideDuration: 700, autoHideDelay: 5000,});
        }
        var member_fund_type = "<?= $this->member->data["member_fund_type"]?>";
        if (member_fund_type == "preferred") {

            $("#chat_options h2").html("Select A Package: (You can charge with no limit)");
        }

        $(document).on('click', '.make-payment', function (e) {

            e.preventDefault();

            var paymentType = $(this).attr('data-payment-method');
            var packageSelected = $("input[name='package']:checked").val();

            if (paymentType == 'paypal') {

                window.location = '/my_account/transactions/fund_with_paypal/' + packageSelected;

            } else if(paymentType == 'merchant') {

                if (confirm('By clicking OK below, you are allowing PsychicContact to charge your credit card. Do you want to continue?')) {

                    var billing_profile_id = $(this).attr('data-billing-profile-id');

                    window.location = '/my_account/transactions/submit_deposit/' + billing_profile_id + '/' + packageSelected;
                }
            }
        });

        $("input[name='package']").change(function () {

            var value = $(this).val();

            var promoFlag = 0;
            if (value == '') {
                $('#billing_profile_div').html("");
                $('#add_new_card').html("");
            } else {
                if (member_fund_type != "preferred") {

                    var fund_amount_day = "<?= $this->member_funds->fund_amount_day($this->member->data["id"]);?>";
                    var fund_amount_month = "<?= $this->member_funds->fund_amount_month($this->member->data["id"]);?>";

                    var fund_limit_day = "<?= $this->member_funds->get_fund_limit_day($this->member->data["id"])?>";
                    var fund_limit_month = "<?= $this->member_funds->get_fund_limit_month($this->member->data["id"])?>";

                    if (fund_amount_day < fund_limit_day && fund_amount_month < fund_limit_month) {
                        $.get('/my_account/transactions/get_billing_profiles/' + value, function (object) {

                            var selectHTML = "<div class='btn-group btn-group-vertical profile_selector' data-toggle='buttons-radio'>";

                            if (object.error == '0') {

                                $(object.profiles).each(function (i) {
                                    selectHTML = selectHTML + "<a href='/' data-payment-method='merchant' data-billing-profile-id='" + object.profiles[i].id + "' class='btn btn-round make-payment'>" + object.profiles[i].card_name + " (**** " + object.profiles[i].card_number + ")</a>";
                                });
                            }
                            if (promoFlag != 1) {
                                selectHTML = selectHTML + "<a href='/'  data-payment-method='paypal' class='btn btn-round make-payment ppal'>Pay With PayPal</a>";
                            }
                            // $('#billing_profile_div').html(selectHTML);

                            var addHTML = "<a href='/my_account/transactions/add_billing_profile/" + object.merchant + "/" + value + "' class='btn btn-round'>Credit / Debit Card Payment</a></div>";
                            $('#add_new_card').html(addHTML);

                            if (object.error == '1') {
                            } else {
                            }

                        }, 'json');
                    } else {
                        //limited mail to NoBo
                        $.post("/my_account/transactions/limit_mail", {user_id: "<?= $this->member->data["id"]?>"},
                            function (data) {

                            }, "json");
                    }
                } else {
                    $.get('/my_account/transactions/get_billing_profiles/' + value, function (object) {
                        var selectHTML = "<div class='btn-group btn-group-vertical profile_selector' data-toggle='buttons-radio'>";
                        if (object.error == '0') {
                            $(object.profiles).each(function (i) {
                                selectHTML = selectHTML + "<a href='/' data-payment-method='merchant' data-billing-profile-id='" + object.profiles[i].id + "' class='btn btn-primary make-payment'>" + object.profiles[i].card_name + " (**** " + object.profiles[i].card_number + ")</a>";
                            });
                        }
                        if (promoFlag != 1) {
                            selectHTML = selectHTML + "<a href='/'  data-payment-method='paypal' class='btn btn-round make-payment ppal'>Pay With PayPal</a>";
                        }

                        $('#billing_profile_div').html(selectHTML);
                        var addHTML = "<a href='/my_account/transactions/add_billing_profile/" + object.merchant + "/" + value + "' class='btn btn-round'>Add A New Credit / Debit Card</a></div>";
                        $('#add_new_card').html(addHTML);

                        if (object.error == '1') {
                            // window.location = object.redirect;
                        } else {
                        }

                    }, 'json');
                }
            }
        });
    });
</script>