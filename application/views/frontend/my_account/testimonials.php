<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Testimonials</h4>

        <div class="card-description">
            <div class="material-datatables">

            <div class="row">
            <?php if (count($testis) > 0): ?>

                    <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <tr>
                            <th>Date</th>
                            <th>Rating</th>
                            <th>Review</th>
                            <!--                                <th>&nbsp;</th>-->
                        </tr>
                        <?php foreach ($testis as $t): ?>
                            <tr>
                                <td style="vertical-align:top;width:175px;"><?= date('m/d/Y h:i:s a', strtotime($t['datetime'])) ?></td>
                                <td style="vertical-align:top;width:25px"><?= $t['rating'] ?></td>
                                <td style="vertical-align:top;"><?= html_entity_decode(nl2br($t['review'])) ?></td>

                                <!--                                    <td style="vertical-align:top;width:60px;">-->
                                <!--                                        <a  href="/my_account/main/testimonial_toggle/-->
                                <? //= $t['id'] ?><!--"-->
                                <!--                                            --><? //= ($t['reader_approved'] != 1 ? "class='btn btn-primary'>Approve" : "class='btn btn-danger'>Deny") ?>
                                <!--                                        </a>-->
                                <!--                                    </td>-->
                            </tr>
                        <?php endforeach; ?>
                    </table>
                    <?php else: ?>
                    <p>You have no testimonials.</p>
                    <?php endif; ?>
            </div>
        </div>
    </div>
</div>
