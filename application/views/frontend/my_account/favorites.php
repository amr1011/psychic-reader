<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css">

<!-- new layout -->
<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">My Favorite Readers</h4>

        <div class="card-description">

            <div class="material-datatables">
                <div class="col-lg-5 col-md-6 col-sm-3">
                    <label for="favorite-option">Select Options</label>
                    <select name='favorite-option' class="selectpicker" data-style="btn btn-primary btn-sm btn-round" id="favorite-option" multiple="multiple" required>
                        <option value="1">Reader makes a change to their Specials</option>
                        <option value="2">Reader makes a change to their profile</option>
                        <option value="3">Reader posts to our Blog or posts/edits an Article</option>
                        <option value="4">Reader logs on and sets themselves as available</option>
                        <option value="5">Reader will be appearing on our Radio Show</option>
                        <option value="6">Reader sends the client a message to their MeBo inbox</option>
                    </select>
                    <button class="btn btn-primary btn-sm" id="save-favorite-option">Save</button>
                </div>
                     <?php
                    if($favorite_experts)
                    {
                        echo "<table class='table table-striped table-hover'>";

                        foreach($favorite_experts as $profile)
                        {
                            $member = $this->system_vars->get_member($profile['id']);

                            echo "
                            <tr>
                                <td><a href='/profile/{$profile['username']}'><img src='{$member['profile']}' width='75' class='img-polaroid'></a></td>
                                <td ><a href='/profile/{$profile['username']}' class='expert_name'>{$member['username']}</a></td>
                                <td>                                
                                    <a href='/my_account/favorites/delete/{$profile['id']}' onClick=\"Javascript:return confirm('Are you sure you want to delete this expert\'s profile from your favorites?');\" class='btn btn-link'><span class='icon icon-trash'></span></a>
                                    <a href='/profile/{$profile['username']}' class='btn'>View</a>
                                
                                </td>
                            </tr>
                            ";
                        }
                        echo "</table>";
                    }
                    else
                    {
                        echo "<div>You do not have any favorite experts</div><hr />";
                    }
                ?>

                <h4>Readers I've Chatted With</h4>

                <?php

                    if($recent_experts)
                    {
                        echo "<table class='table table-striped table-hover'>";

                        foreach($recent_experts as $profile)
                        {
                            $member = $this->system_vars->get_member($profile['id']);

                            echo "
                            <tr>
                                <td><img src='{$member['profile']}' width='75' class='img-polaroid'></td>
                                <td><a href='/my_account/main/profile/{$profile['username']}' class='btn'>View</a></td>
                            </tr>
                            ";
                        }
                        echo "</table>";
                    }
                    else
                    {
                        echo "<div>You have not chatted with any experts</div>";
                    }
                ?>


            </div>
        </div>
    </div>
</div>    