<script>

    $(document).ready(function () {
        if($('p.record').html()) {
        $.notify($('p.record').html(), {className:"success",align:"center", verticalAlign:"top", hideDuration: 700, autoHideDelay: 5000,});
        }

        var max_paid = "<?= round(floatval($chat['length'])/60, 2)?>";
        var max_free = "<?= round(floatval($chat['total_free_length'])/60, 2)?>";
        max_paid = parseFloat(max_paid);
        max_free = parseFloat(max_free);

        $('input.time-back').on('keydown keyup', function(e){
            console.log($(this).val());
            console.log(max_paid)
            if (parseFloat($(this).val()) > max_paid) {
                e.preventDefault();
                $(this).val(max_paid);
            }
            if (parseFloat($(this).val()) < 0) {
                e.preventDefault();
                $(this).val(0);
            }
        });

        $('input.free-time-back').on('keydown keyup', function(e){
            console.log($(this).val());
            console.log(max_free)
            if (parseFloat($(this).val()) > max_free) {
                e.preventDefault();
                $(this).val(max_free);
            }
            if (parseFloat($(this).val()) < 0) {
                e.preventDefault();
                $(this).val(0);
            }
        });
    });

function yesno(params) {
    if(params == 'gft_form'){
        var message = 'Give Free Time ';
    }else{
        var message = 'Give Paid Time ';
    }
    var r = confirm("Are you sure, you want to " + message +"back?");
    if (r == true) {
        $( "#" + params ).submit();
    } 
}


</script>