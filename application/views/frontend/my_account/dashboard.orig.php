<!-- 9-4 -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/css/bootstrap-notify.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/css/styles/alert-bangtidy.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/css/styles/alert-blackgloss.css">
<!-- <style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #337ab7;
}

input:focus + .slider {
  box-shadow: 0 0 1px #337ab7;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style> -->
<div class="row">
 <div class="my-container page-70-30">

     <div class="content-block-30">
        <?php $this->load->view('frontend/my_account/nav/dashboard_menu'); ?>
     </div><!-- .content-block-20 -->
     <div class="content-block-70">

        <div class="row">
            <div class="col-md-8 col-sm-7">     
                <h2>Hi <?=$this->member->data['first_name']?> <?=$this->member->data['last_name']?></h2>        
                <a class="btn btn-primary" id="logoutbtn" href="main/logout">Logout</a>
                <p>
                    Promote yourself using this URL: <span>
                    <a href="<?=SITE_URL ?>/profile/<?=$this->member->data['username']?>"><?=SITE_URL ?>/profile/<?=$this->member->data['username']?></a>
                    </span>
                </p>
            </div>

            <div class="col-md-4 col-sm-5" >
                    <h4>Balance (US):  <span>$<?=number_format($this->reader->get_balance('us'),2)?></span></h4>
                    <h4>Balance (CAN): <span>$<?=number_format($this->reader->get_balance('ca'),2)?></span></h4>
                    
                    <div class="btn-group">
                        <a id="reader_status_online_button" href='/my_account/main/set_status/online' class="btn  <?=($this->member->data['status']=='online' ? "btn-success" : (($this->member->data['status']=='busy')? "btn-warning":"btn-default")    )?>">Online</a>
                        <a id="reader_status_break_button" href='/my_account/main/set_status/break' class="btn <?=($this->member->data['status']=='break' || $this->member->data['status']=='booked' || $this->member->data['status']=='busy' ||  $this->member->data['status']=='away' ? "btn-primary" : "btn-default")?>">Break</a>
                        <a id="reader_status_offline_button" href='/my_account/main/set_status/offline' class="btn <?=($this->member->data['status']=='offline' ? "btn-default active" : "btn-default")?>">Offline</a>
                    </div>      
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
               <form action="/my_account/main/set_break_time" method="POST" role="form">
                <div class="form-group  col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <label for="ReaderBreakTime">Break time</label>
                    <select name="ReaderBreakTime" id="ReaderBreakTime" class="form-control" required="required">
                        <option value="0" <?php  echo((  (int)$this->member->data['break_time_amount']==0)?  "selected":null)  ?> >Select the “Break time” amount</option>
                        <option value="2" <?php  echo((  (int)$this->member->data['break_time_amount']==2 )?  "selected":null)  ?> > 2 minutes </option>
                        <option value="5" <?php  echo((  (int)$this->member->data['break_time_amount']==5 )?  "selected":null)  ?> > 5 minutes </option>
                        <option value="7" <?php  echo((  (int)$this->member->data['break_time_amount']==7 )?  "selected":null)  ?> > 7 minutes </option>
                        <option value="10" <?php echo ((  (int)$this->member->data['break_time_amount']==10 )? "selected":null)  ?> > 10 minutes</option>
                    </select>
                    <button type="submit" class="btn btn-primary" style="margin-top: 20px;">Save</button>
                    
                </div>   
                </form>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
               
                <div class="form-group  col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <label for="reader-doorbell-sound">Select Sound Notification</label>
                    <select name="reader-doorbell-sound" id="reader-doorbell-sound" class="form-control" required="required">
                        <option value="" selected="true">Select the “Doorbell sound” </option>
                        <option value="door_bell"> door bell </option>
                        <option value="bell_ring"> bell ring </option>
                        <option value="church_bell"> church bell </option>
                        <option value="railroad_bell"> railroad bell </option>
                        <option value="salamisound_bell"> salamisound bell </option>
                        <option value="service_bell"> service bell </option>
                        <!-- <option value="siren_bell"> siren bell </option> -->
                        <option value="warbling_bell"> warbling bell </option>
                    </select>
                    <button id="save-doorbell-sound" class="btn btn-primary" style="margin-top: 20px;">Save</button>
                    <div id="reader-doorbell-sound-value" style="display: none;"></div>
                </div>    
            </div>
        </div>
         <div class="row" style="text-align: center;">
             <h2 style="color: red; display: none;" id="no-reader-text">There are less than 2 online readers right now.</h2>
         </div>
        <!-- 9-8 -->
        <!-- <hr>
        <div class="row">
            <h2 style="margin-left: 30px;">Free Time Gift</h2>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="form-group col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <label for="reader-pause-timer">Pause Timer</label>
                    <select name="reader-pause-timer" id="reader-pause-timer" class="form-control" required="required">
                        <option value="0" selected="true">Select the “Client Type” </option>
                        <option value="1"> New Clients </option>
                        <option value="2"> Repeat Clients </option>
                        <option value="3"> Favourite Clients </option>
                        <option value="4"> All Clients </option>
                        <option value="5"> Random </option>
                    </select>
                    <button id="save-reader-pause-timer" class="btn btn-primary" style="margin-top: 20px;">Save</button>
                    
                </div>    
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                <div class="form-group col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <label for="reader-pause-timer-size">Free Time Range</label>
                    <select name="reader-pause-timer-size" id="reader-pause-timer-size" class="form-control" required="required">
                        <option value="0" selected="true">Select the “Client Type” </option>
                        <option value="5"> 5 Minutes </option>
                        <option value="10"> 10 Minutes </option>
                        <option value="15"> 15 Minutes </option>
                        <option value="20"> 20 Minutes </option>
                        <option value="25"> 25 Minutes </option>
                        <option value="30"> 30 Minutes </option>
                    </select>
                    <button id="save-reader-pause-timer-size" class="btn btn-primary" style="margin-top: 20px;">Save</button>
                </div>    
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <div class="form-group col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <label for="toggle-pause-timer">Switch Pause Timer</label><br>
                    <label class="switch" style="float: left;margin-right: 5px;">
                      <input name="toggle-pause-timer" type="checkbox" id="pause-timer-switch">
                      <span class="slider round"></span>
                    </label>
                    <span id="switch-text"></span>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                <div class="form-group col-xs-12 col-sm-12 col-md-8 col-lg-8" id="repeat-client" style="display: none;">
                    <label for="repeat-reader-select">Select Repeated Clients</label>
                    <select name="repeat-reader-select" id="select-repeat-client" multiple="multiple">
                    
                    </select><br>
                    <button id="save-repeat-reader-select" class="btn btn-primary" style="margin-top: 20px;">Save</button>
                </div>    
            </div>
        </div>
        <hr> -->
     </div><!-- .content-block-80 -->

    </div><!-- .my-container -->   
</div>

<script src='https://www.google.com/recaptcha/api.js'></script>

<script src="/chat/reader_my_account.js?current_status=<?=$this->member->data['status'] ?>&reader_id=<?=$this->member->data['profile_id'] ?>"></script>

<!-- 9-4 -->
<script>
    $(document).ready(function() {

        var appointmentTimer = setInterval(function(){

            $.ajax({

                url: '/chat/main/get_appointment/'+'<?=$this->member->data['id']?>',
                type: 'GET',

            }).done(function(response) {

                var obj = JSON.parse(response);
                var data = obj['appointments'];

                for(var i=0; i<data.length; i++) {

                    var messageText = 'Hi ' + '<?=$this->member->data['username']?>, ' + data[i]['client_firstname'] + ' ' + data[i]['client_lastname'] 
                                    + ' sent an appointment for ' + data[i]['appointment_date'];
              
                    $.notify( messageText,  {className: "info", hideDuration: 700, autoHideDelay: 60000, position: 'right buttom'});
                }
                console.log(obj);

            }).fail(function(error) {
                console.error(error);
            });

        }, 3000);

        var no_reader_check = setInterval(function(){

            $.ajax({

                url: '/main/no_reader_check/'+'<?=$this->member->data['id']?>',
                type: 'GET',

            }).done(function(response) {

                let obj = JSON.parse(response);
                let status = obj.status;

                if(status == true) {
                    $("#no-reader-text").css("display", "block");
                } else {
                    $("#no-reader-text").css("display", "none");
                }

            }).fail(function(error) {
                console.error(error);
            });

        }, 1000);

        /*9-20*/
        var waitInLineTimer = setInterval(function () {

            $.ajax({

                url: '/chat/main/get_wait_in_line_clients/' + '<?php echo $this->member->data['id']?>',
                type: 'GET',

            }).done(function (response) {

                var obj = JSON.parse(response);
                var data = obj['wait_in_line_clients'];

                console.log("wait in line clients ===>" + data);

                var number_wait_in_line_clients = data.length;

                if (number_wait_in_line_clients > 0) {

                    var messageText = 'You have ' + number_wait_in_line_clients + 'clients waiting in line : ';

                    for (var i = 0; i < data.length; i++) {

                        messageText += data[i]['client_user_name'] + '  ';
                    }
                    $.notify(messageText, {
                        className: "success",
                        hideDuration: 700,
                        autoHideDelay: 6000000,
                        position: 'left bottom'
                    });
                    clearInterval(waitInLineTimer);
                }

                console.log(obj);

            }).fail(function (error) {
                console.error(error);
            });
        }, 3000);

    });
</script>