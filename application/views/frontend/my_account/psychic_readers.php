<div class="card-header card-header-image div-readers" data-header-animation="true">

    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title"></h4>
        <div class="card-description">

<!--     *********    TEAM 1     *********      -->
      <div class="team-1" id="team-1">
        <div class="container">
          
          <div class="row">
            <div class="col-md-8 ml-auto mr-auto text-center">
              <h2 class="title">Meet our Professional Psychic Readers!</h2>
            </div>
          </div>

          <div class="row">
            <?php $i=0; ?>
            
            <?php foreach ($readers as $reader): ?>


            <div class="col-md-4 d-flex align-items-stretch " >
              <div class="card card-profile ">
                <div class="card-header card-avatar">
                    <?php if (file_exists(FCPATH . $reader["profile"])): ?>
                          <a href="#">
                            <img class="img" src="<?php echo $reader["profile"]; ?>" style="width: 100px; height: 100px;" />
                          </a>
                    <?php else: ?>
                          <a href="#">
                            <img class="img" src="/media/assets/no-image-available.jpg" style="width: 100px; height: 100px;">
                          </a>
                    <?php endif; ?>
                </div>
                <div class="card-body ">
                  <h4 class="card-title"><?php echo ucfirst($reader['username']); ?></h4>
                  <?php 
                    switch ($reader['status']) {
                        case 'online':
                            $status = "I am available";
                            $attrib ="text-success";

                            break;
                        case 'break';

                            $status = "Be Right Back on ". $reader['break_time_amount'] ."  minutes";
                            $attrib ="text-muted";
                            break;                        
                        default:
                            $status = "I am offline";
                            $attrib ="text-muted";
                            break;
                    }


                  ?>
                  <div class="div-readers" id="div-reader-<?=$reader['id'];?>" 
                          data-username="<?=$reader['username']; ?>" 
                          data-reader-id="<?=$reader['id'];?>"  >
                    <h6 class="card-category  <?php echo $attrib; ?>">
                      <?php echo $status; ?></h6>
                    <?php if ($reader['status']  == "online"): ?>
                        <a  id="div-reader-<?=$reader['id'];?>" data-username="<?=$reader['username']; ?>" data-reader-id="<?=$reader['id'];?>" class="btn btn-success  btn-sm btn-round btn-chat"  href="/chat/main/index/<?php echo $reader["username"]; ?>"><?php echo $status; ?></a>
                    <?php else: ?>
                      <a  id="div-reader-<?=$reader['id'];?>" data-username="<?=$reader['username']; ?>" data-reader-id="<?=$reader['id'];?>" class="btn btn-round btn-sm btn-chat disabled "  href="#"><?php echo $status; ?></a>

                      <!-- <a  class="btn btn-round btn-sm btn-appointment disabled  "  href="#"> Appointment <i class="fa fa-calendar"></i> </a> -->



                    <?php endif; ?>
                    <?php if ($reader['enable_phone'] == 1): ?>
                      <a href="#" target="_self"  class="btn btn-just-icon btn-link btn-phone btn-success"><i class="fa fa-phone"></i></a>
                    <?php endif; ?>

                  </div>

                  <p class="card-description">
                    <?php echo truncate($reader['area_of_expertise'], 80); ?>
                  </p>
                </div>
                    <input type="hidden" class="reader-id" value="<?php echo($reader['id']);?>">
                    <input type="hidden" class="client-id" value="<?php echo($this->session->userdata('member_logged')); ?>">
                    <input type="hidden" class="reader-name" value="<?php echo $reader["username"]; ?>">
              </div>
            </div>
            <?php $i++; ?>
            <?php endforeach; ?>

           
            
          </div>


        

        </div>
      </div>
      <!--     *********    END TEAM 1      *********      -->

        <!-- <div class="wrapper-content" id="box-appointment" >
          <section class="content-block templete-block">
              <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <span >APPOINTMENT  to  </span>
                      <span id="reader"></span>
                      <button type="button" class="close" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
              </div>
              <div class="row">
                  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-8">
                      <span >Please select date</span><i class="fa fa-calendar"></i>
                      <input id="datepicker" >
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-4">
                      <button type="button" class="btn btn-success" id="confirm-appointment">Appointment</button>
                      <button type="button" class="btn btn-danger cancel">Cancel</button>
                  </div>
              </div>
          </section>
      </div> -->

    


 



        </div>
    </div>
    <div class="card-footer">
    
    </div>
  </div>
</div>