
<!-- 9-12 -->

<?php $pause_timer_type = $reader['pause_timer']; ?>
<?php $pause_timer_size = $reader['pause_timer_size']; ?>
<?php $pause_timer_switch = $reader['pause_timer_flag']; ?>
<?php $mem_id = $reader['id']; ?>

<script src="/theme/admin/js/notify.min.js"></script>
<script>

    function get_day_of_week(day) {
        switch (parseInt(day)) {
            case 1:
                return "Mon";
                break;
            case 2:
                return "Tue";
                break;
            case 3:
                return "Wed";
                break;
            case 4:
                return "Thu";
                break;
            case 5:
                return "Fri";
                break;
            case 6:
                return "Sat";
                break;
            case 0:
                return "Sun";
                break;
            case 7:
                return "Any Day";
                break;
        }
    }

    $(document).ready(function() {

        console.log('<?php echo($reader["giveback_created_at"])?>');
        /*9-22*/
        var week_day;

        /*9-12*/
        var repeat_client_data = [];
        var saved_repeat_client_data = [];
        /*9-21*/
        var available_client_data = [];
        var current_giveback_type = parseInt("<?=($pause_timer_type)?>");
        /*9-22*/
        var cancelled_client_data = [];

        $("#repeat-client").css('display', 'none');

        if('<?=$reader['week_day']?>' > -1) {
            $("input[value='0']").prop("checked","unchecked");
            $("input[value='"+ '<?=$reader['week_day']?>' +"']").prop("checked", "checked");
            $("input#week-day").val('<?=$reader['week_day']?>');
        }

        $.ajax({
            'type': "POST",
            'url': "/chat/chatInterface/get_repeat_client",
            'data': { id : '<?=$mem_id;?>'},
            success: function (data) {

                obj = JSON.parse(data);
                repeat_client_data = obj.repeat_client;
            }
        });

        $.ajax({
            'type': "POST",
            'url': "/chat/chatInterface/get_saved_repeat_client",
            'data': { id : '<?=$mem_id;?>'},
            success: function (data) {

                obj = JSON.parse(data);
                saved_repeat_client_data = obj.data;
                console.log('saved=',saved_repeat_client_data);
            }
        });

        /*9-21*/
        $.ajax({
            'type': "POST",
            'url': "/chat/chatInterface/get_available_client",
            'data': { id : '<?=$mem_id;?>'},
            success: function (data) {

                obj = JSON.parse(data);
                available_client_data = obj.clients;

            }
        });

        setTimeout(function() {

            console.log(repeat_client_data);
            $('#select-repeat-client').empty();
            for(var i=0; i<repeat_client_data.length; i++) {

                $('#select-repeat-client').append("<option value='"+ repeat_client_data[i]["id"] +"'>"+ repeat_client_data[i]["username"] +"</option>");
            }
            if(saved_repeat_client_data != [] && saved_repeat_client_data != null) {

                for (var j=0; j<saved_repeat_client_data.length; j++) {

                    $("select#select-repeat-client option[value='"+saved_repeat_client_data[j]+"']").attr("selected","selected");
                }
            }

            $('#select-repeat-client').multiselect({

                numberDisplayed: 6,
                disableIfEmpty: true,
                includeSelectAllOption: true,
                filterPlaceholder: 'Search',
                enableFiltering: true,
                includeFilterClearBtn: false,
                selectAllNumber: false,
            });

            /*9-21*/

            $('#select-cancel-client').empty();

            var group_id = -1;

            for(var i=0; i<available_client_data.length; i++) {

                if(group_id != parseInt(available_client_data[i]['group_id']) && parseInt(available_client_data[i]['group_type']) != 2) {

                    group_id = parseInt(available_client_data[i]['group_id']);

                    if(parseInt(available_client_data[i]['group_type']) == 1) { // new clients
                        $('#select-cancel-client').append("<option value='"+ available_client_data[i]["group_id"] +"'>"+ "New Clients" + " (" + available_client_data[i]["amount"] + "mins on " + get_day_of_week(available_client_data[i]["day_of_week"]) + ")"+ " issued at " + available_client_data[i]["created_at"] + "</option>");
                    }
                    if(parseInt(available_client_data[i]['group_type']) == 3) { // favorite clients
                        $('#select-cancel-client').append("<option value='"+ available_client_data[i]["group_id"] +"'>"+ "Favorite Clients" + " (" + available_client_data[i]["amount"] + "mins on " + get_day_of_week(available_client_data[i]["day_of_week"]) + ")"+ " issued at " + available_client_data[i]["created_at"] + "</option>");
                    }
                    if(parseInt(available_client_data[i]['group_type']) == 4) { // all clients
                        $('#select-cancel-client').append("<option value='"+ available_client_data[i]["group_id"] +"'>"+ "All Clients" + " (" + available_client_data[i]["amount"] + "mins on " + get_day_of_week(available_client_data[i]["day_of_week"]) + ")"+ " issued at " + available_client_data[i]["created_at"] + "</option>");
                    }
                }
                if(parseInt(available_client_data[i]['group_type']) == 2) {

                    $('#select-cancel-client').append("<option value='"+ available_client_data[i]["group_id"] +"'>"+ available_client_data[i]["username"] + " (" + available_client_data[i]["amount"] + "mins on " + get_day_of_week(available_client_data[i]["day_of_week"]) + ")"+ " issued at " + available_client_data[i]["created_at"] + "</option>");

                }
            }

            $('#box-cancel').css('display','initial');

            $('#select-cancel-client').multiselect({
                numberDisplayed: 1,
                disableIfEmpty: true,
                includeSelectAllOption: true,
                filterPlaceholder: 'Search',
                enableFiltering: true,
                includeFilterClearBtn: false,
                selectAllNumber: false,
            });

            if(parseInt("<?=($pause_timer_type)?>") == 2) {

                $("#repeat-client").css('display', 'block');
                
            } else {

                $("#repeat-client").css('display', 'none');
            }
        }, 1000);
        
        $("select#reader-pause-timer").val("<?=($pause_timer_type)?>");  
        $("select#reader-pause-timer-size").val("<?=($pause_timer_size)?>");  
        $('#pause-timer-switch').prop('checked', parseInt("<?=($pause_timer_switch)?>"));

        if(parseInt("<?=($pause_timer_switch)?>") == 1) {

            $("#switch-text").text("On").css('color', '#337ab7');

        } else if(parseInt("<?=($pause_timer_switch)?>") == 0) {

            $("#switch-text").text("Off").css('color', 'black');
        } 

        /*9-12*/
        $("#save-give-back").click(function() {
            console.log("save data=",$("#select-repeat-client").val());
            $.ajax({

                'type': "POST",
                'url': "/chat/chatInterface/save_give_back",
                'data': { id : '<?=$mem_id;?>', type : $("#reader-pause-timer").val(), size : $("#reader-pause-timer-size").val(), data: $("#select-repeat-client").val(), weekday: $("input#week-day").val()},
                success: function (data) {

                    obj = JSON.parse(data);
                    console.log(obj.status);

                    setTimeout(function () {
                        location.reload();
                    }, 1000);

                    // $.notify("Free Time Gift saved successfully.",  {className: "info", hideDuration: 700, autoHideDelay: 60000, position: 'right buttom'});

                }
            });
        });

        /*9-21*//*9-22*/
        $("#btn-save-cancel").click(function() {
            console.log("save cancel data=",$("#select-cancel-client").val());
            $.ajax({

                'type': "POST",
                'url': "/chat/chatInterface/save_give_back_cancel",
                'data': { id : '<?=$mem_id;?>', data: $("#select-cancel-client").val()},
                success: function (data) {

                    obj = JSON.parse(data);
                    console.log(obj.status);

                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                }
            });
        });

        // $('#pause-timer-switch').change(function() {

        //     if(this.checked) {
                
        //         $.post("/chat/chatInterface/save_reader_pause_timer_flag", {user_id : '<?=$mem_id;?>', flag : 1}, 
        //             function(data){
        //                 if (data.status == "OK")
        //                 {
        //                     console.log('pause_timer_flag is saved.');
        //                 }
        //         }, "json"); 

        //         $("#switch-text").text("On").css('color', '#337ab7');
        //     }

        //     if(!this.checked) {
                
        //         $.post("/chat/chatInterface/save_reader_pause_timer_flag", {user_id : '<?=$mem_id;?>', flag : 0}, 
        //             function(data){
        //                 if (data.status == "OK")
        //                 {
        //                     console.log('pause_timer_flag is saved.');
        //                 }
        //         }, "json"); 

        //         $("#switch-text").text("Off").css('color', 'black');
        //     }
                  
        // });

        $("#reader-pause-timer").change(function() {

            if($("#reader-pause-timer").val() == 6) {

                $.post("/chat/chatInterface/save_reader_pause_timer_flag", {user_id : '<?=$mem_id;?>', flag : 0}, 
                    function(data){
                        if (data.status == "OK")
                        {
                            console.log('pause_timer_flag is saved.');
                        }
                }, "json"); 
            } 

            if($("#reader-pause-timer").val() != 6) {

                $.post("/chat/chatInterface/save_reader_pause_timer_flag", {user_id : '<?=$mem_id;?>', flag : 1}, 
                    function(data){
                        if (data.status == "OK")
                        {
                            console.log('pause_timer_flag is saved.');
                        }
                }, "json"); 
            }

            if($("#reader-pause-timer").val() == 2) {
                
                $("#repeat-client").css("display", "block");
            } else {

                $("#repeat-client").css("display", "none");
                
            }
            /*9-21*/
            if($("#reader-pause-timer").val() != current_giveback_type){
                $("#box-cancel").css("display","none");
            } else {
                $("#box-cancel").css("display","initial");
            }

        });

        $("input").click(function (event) {
            $("input#week-day").val($(this).val());
        });

    })
</script>
<!-- btn btnYesFreeTime -->