<!-- new layout -->
<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Compose A New Message</h4>

        <div class="card-description">

            <?php if($this->session->flashdata('response')):?>
                <p class='record text-warning' > <?=$this->session->flashdata('response')?> </p>
            <?php endif?>

            <div class="material-datatables">
                    <ul class="nav nav-pills nav-pills-primary" role="tablist">
                        <li class="nav-item" <?= ($this->uri->segment('3') == '' ? " class=\"active\"" : "") ?>>
                            <a  class="nav-link" href="/my_account/messages">Inbox</a></li>
                        <li class="nav-item" <?= ($this->uri->segment('3') == 'outbox' ? " class=\"active\"" : "") ?>>
                            <a class="nav-link" href="/my_account/messages/outbox">Outbox</a></li>
                        <li class="nav-item" <?= ($this->uri->segment('3') == 'compose' ? " class=\"active\"" : "") ?>>
                            <a class="nav-link" href="/my_account/messages/compose"><span class='icon icon-comment'></span> Compose A New Message</a></li>
                    </ul>

                    <div class="content-notification-folder col-md-4">
                          <div class="notification-folder admin-folder">
                                <span class="fa fa-folder folder-icon enabled"><input type="hidden" class="folder-status" value="0"></span>
                                <span class="folder-name enabled">Administrator</span>
                                <?php if($unread_admin['unread_admin'] > 0) {?>
                                    <span class="unread enabled"><?= $unread_admin['unread_admin'];?></span>
                                <?php }?>
                            </div>
                             <div class="main-folders">
                                <?php foreach($senders as $sender) {?>
                                        <?php if($this->member->data['member_type'] == 'reader') {?>
                                            <?php if($sender['banned'] && $sender['banned'] == '1') {?>
                                                <div class="notification-folder message-folder disabled">
                                                    <span class="fa fa-folder folder-icon disabled"></span>
                                                    <span class="folder-name disabled sender-id" style="display: none;"><?= $sender['sender_id']?></span>
                                                    <span class="folder-name disabled show-folder-name"><?= $sender['username'] .' ('. $sender['first_name'] .')'?></span>
                                                </div>
                                            <?php } else {?>
                                                <div class="notification-folder message-folder enabled">
                                                    <span class="fa fa-folder folder-icon enabled text-"><input type="hidden" class="folder-status" value="0"></span>
                                                    <span class="folder-name enabled sender-id" style="display: none;"><?= $sender['sender_id']?></span>
                                                    <span class="folder-name enabled show-folder-name"><?= $sender['username'] .' ('. $sender['first_name'] .')'?></span>
                                                    <?php if($sender['unread'] > 0) {?>
                                                        <span class="unread enabled"><?= $sender['unread'];?></span>
                                                    <?php }?>
                                                </div>
                                            <?php }?>
                                        <?php } else {?>
                                            <div class="notification-folder message-folder enabled">
                                                <span class="fa fa-folder folder-icon text-warning enabled"><input type="hidden" class="folder-status" value="0"></span>
                                                <span class="folder-name enabled sender-id" style="display: none;"><?= $sender['sender_id']?></span>
                                                <span class="folder-name enabled show-folder-name"><?= $sender['username'] .' ('. $sender['first_name'] .')'?></span>
                                                <?php if($sender['unread'] > 0) {?>
                                                    <span class="unread enabled"><?= $sender['unread'];?></span>
                                                <?php }?>
                                            </div>
                                        <?php }?>
                                <?php }?>
                            </div>


                    </div>

                     <div class="content-notification-messages col-md-6" >
                            <div class="box-message"></div>
                        </div>
                  

            </div>
        </div>
    </div>
</div>            
