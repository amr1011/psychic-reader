<!-- new layout -->
<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">NRR Request Form</h4>

        <div class="card-description">

            <?php if($this->session->flashdata('response')):?>
                <p class='record text-warning' > <?=$this->session->flashdata('response')?> </p>
            <?php endif?>

            <div class="material-datatables">

                <form class="form-horizontal" action="/my_account/main/nrr_submit" name="nrr_form" method="post">

                <?php if (count($this->member->get_chats())): ?>

                <h3>Client: <?= $this->member->data['first_name'] ?> <?= $this->member->data['last_name'] ?></h3>
                <select name="reader" class="selectpicker" data-style="btn btn-primary btn-round" required>
                    <option value="0" disabled selected>Select your reader</option>
                    <?php
                        foreach ($readers as $r):
                        if (isset($chat['id']))
                        {
                            $rid = $chat['reader_id'];
                        } else
                        {
                            $rid = null;
                        }
                    ?>
                    <option <?= ($rid == $r['id'] ? "selected=selected" : "") ?>  value="<?= $r['id'] ?>"><?= $r['username'] ?></option>
                <?php endforeach; ?>
            </select>

            <select name="chat" class="form-control" required>
                <option>Reader chat sessions</option>
            </select>
            <p>
                Please use this form to let us know if you are requesting time back from the reader for the chat session that just ended, or any other session you recently had.
                IF MORE THAN ONE OCCURRENCE TOOK PLACE IN WHICH YOU ARE REQUESTING TIME BACK- PLEASE USE A SEPARATE NRR FORM FOR EACH DESCRIPTION/REQUEST.
            </p>

            <ol style="margin:0px auto;width:100%;padding: 20px;">
                <li><input type="checkbox" checked="checked"  value="slow" name="type">&nbsp;&nbsp;The chat was running slow.</li>
                <li><input type="checkbox"  value="disconnect" name="type">&nbsp;&nbsp;The chat ended suddenly and I was booted out.</li>
                <li><input type="checkbox"  value="unhappy_reading" name="type">&nbsp;&nbsp;I was unhappy with my reading from my reader.</li>
            </ol>
                                <div class="well">
                                    <div id="slow" class="request-reason">
                                        <p>
                                            We're sorry you feel the chat was running slower than usual. Usually its Internet connectivity problems that cause slow chatting....
                                            In most cases the reader will compensate you during the chat and/or ask you to leave the chatroom, re-boot
                                            and then return to continue your session.
                                            If you did not bring it to the attention of your Reader during the chat or no compensation was
                                            given please select the amount of time you are requesting back and submit this form
                                        </p>
                                        <div class="control-group">
                                            <label class="control-label" for="slow_timeback">Time Back</label>
                                            <div class="controls">
                                                <input type="number" class="form-control time-back col-md-2" name="slow_timeback" min="0" placeholder="Input Time Back" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="disconnect" class="request-reason">
                                        Chances are your Reader has already added your full time back (Please check your account now before submitting this form). <br />

                                        Unfortunately due to various reasons (most likely internet related) your chat with <span class="readerName"></span> ended abruptly.
                                        We apologize for this occurrence.  In most cases your Reader has already returned  time back to your account.
                                        Please check your account now before submitting this form:

                                        <ol style="padding-top:10px;" id="disconnect2" type="a">
                                            <li>
                                                <label class="radio">
                                                    <input type="radio" checked="" value="1" name="disconnect">
                                                    &nbsp; I have checked my account &amp; time was restored: <b>I will try to re-enter chat asap.</b>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="radio">
                                                    <input type="radio" value="2" name="disconnect">
                                                    &nbsp; I have checked my account &amp; time was restored: <b>I will save my time for another session and/or reader</b>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="radio">
                                                    <input type="radio" value="3" name="disconnect">
                                                    &nbsp; I have checked my account <b>no minutes were returned:</b> <b>I am requesting that <br/> the reader add time
                                                        back to my account in the amount of: </b> <input type="number" class="form-control time-back" min="0" style="display: inline; width: auto;" value="" name="disconnect_time_back" placeholder="Input Time Back">
                                                </label>
                                            </li>
                                        </ol>

                                    </div>

                                    <div id="unhappy_reading" class="request-reason">
                                        Please describe in detail why you feel the reader should return time back to your account<br>
                                        <textarea style="width:100%;height:100px;" name="unhappy"></textarea> <br>
                                        <div style="padding:10px;" class="control-group">
                                            <label class="control-label" for="slow_unhappy">Time Back</label>
                                            <div class="controls">
                                                <input type="number" class="form-control time-back col-md-2" name="unhappy_timeback" min="0" placeholder="Input Time Back">
                                            </div>
                                        </div>

                                        <br>WE ARE SORRY THAT YOU ARE NOT SATISFIED WITH THE SESSION THAT JUST ENDED WITH:
                                        <span class="readerName"></span>
                                        OUR POLICY IS THAT WE GUARANTEE SATISFACTION FOR ALL SESSIONS
                                        IF YOU HONESTLY FEEL THAT THE READER DID NOT LIVE UP TO YOUR EXPECTATIONS- WE WILL GLADLY ADD TIME BACK TO YOUR ACCOUNT
                                        BUT! IN THE FUTURE YOU MUST TELL THE READER WITHIN THE FIRST 5MINS AFTER HITTING THE 'HIRE ME' BUTTON AND THE READER WILL EITHER GIVE YOU YOUR TIME BACK OR PAUSE THE TIMER SO THAT YOU AND THE READER CAN TRY TO COMMUNICATE BETTER.
                                        NEXT TIME PLEASE DISCUSS YOUR FEELINGS ABOUT THE SESSION WITH THE READER
                                    </div>

                                </div>
                                    <textarea style="width:100%;height:100px;" class="form-control" name="suggest"></textarea>
                                    <input class="btn btn-primary" type="submit" value="Submit">
                                <input type="hidden" name="max-length" id="max-length"/>
                            </form>
                        <?php else: ?>
                            <div style="padding:10px;">
                                You must have participated in a chat to submit a NRR.
                            </div>
                        <?php endif; ?>
            </div>
        </div>
    </div>
</div>


<script>

    $(document).ready(function ()
    {
        var max;
        $("form[name=nrr_form]").submit(function ()
        {
            var sel = $("select[name=reader]").val();
            var complaint_type = $("input[name=type]:checked").val();
            var reader = $("select[name=reader] option:selected").text();

            if (sel != 0)
            {
                if (confirm('Are you sure you want to proceed with NRR request form?'))
                {
                    switch (complaint_type)
                    {
                        case "slow":

                            if (!$("input[name=slow_timeback]").val())
                            {
                                $("#universal-form-error").html("<div class='alert alert-error page-notifs'><strong>There was an error:</strong><p>Please input time you wished returned.</div>");
                                return false;
                            }
                            break;
                        case "disconnect":

                            if (!$("input[name=disconnect]").val())
                            {
                                $("#universal-form-error").html("<div class='alert alert-error page-notifs'><strong>There was an error:</strong><p>Please select a disconnect option.</div>");
                                return false;
                            } else
                            {
                                if ($("input[name=disconnect]").val() == "3")
                                {
                                    if (!$("input[name=disconnect_timeback]").val())
                                    {
                                        $("#universal-form-error").html("<div class='alert alert-error page-notifs'><strong>There was an error:</strong><p>Please input time you wished returned.</div>");
                                        return false;
                                    }
                                }
                            }
                            break;
                        case "unhappy_reading":

                            if (!$("textarea[name=unhappy]").val())
                            {
                                $("#universal-form-error").html("<div class='alert alert-error page-notifs'><strong>There was an error:</strong><p>Please provide the reason you are unhappy with this chat..</div>");
                                return false;
                            } else
                            {
                                if (!$("input[name=unhappy_timeback]").val())
                                {

                                    $("#universal-form-error").html("<div class='alert alert-error page-notifs'><strong>There was an error:</strong><p>Please input time you wished returned.</div>");
                                    return false;
                                }
                            }
                            break;
                    }

                } else
                {
                    return false;
                }
            } else
            {
                $("#universal-form-error").html("<div class='alert alert-error page-notifs'><strong>There was an error:</strong><p>You must select a reader to submit the form.</div>");
                return false;
            }

        });

        $("#slow").show();
        $("#disconnect").hide();
        $("#unhappy_reading").hide();

        // $("input[name=type]").change(function ()
        // {
        //     var type = $(this).val();
        //     switch (type)
        //     {
        //         case "slow":
        //             $("#disconnect").hide();
        //             $("#unhappy_reading").hide();
        //             $("#slow").show();
        //             break;
        //
        //         case "disconnect":
        //             $("#disconnect").show();
        //             $("#unhappy_reading").hide();
        //             $("#slow").hide();
        //             break;
        //
        //         case "unhappy_reading":
        //             $("#unhappy_reading").show();
        //             $("#disconnect").hide();
        //             $("#slow").hide();
        //             break;
        //     }
        //
        // });

        $("input[name=type]").change(function ()
        {
            var type = $(this).val();
            switch (type)
            {
                case "slow":

                    $(this).is(':checked') == true ? $("#slow").show() : $("#slow").hide();
                    break;

                case "disconnect":

                    $(this).is(':checked') == true ? $("#disconnect").show().find('input[name="disconnect_time_back"]').prop('required',true) : $("#disconnect").hide().find('input[name="disconnect_time_back"]').removeAttr('required');
                    break;

                case "unhappy_reading":

                    $(this).is(':checked') == true ? $("#unhappy_reading").show().find('input[name="unhappy_timeback"]').prop('required',true) : $("#unhappy_reading").hide().find('input[name="unhappy_timeback"]').removeAttr('required');
                    break;
            }

        });

        <?php if (isset($chat['reader_id'])): ?>
            $.ajax({
            dataType: "json",
                    url: '/my_account/main/nrr_chat_session/<?= $chat['reader_id'] ?>',
                    success: function (data)
                    {

                        var sel = $("select[name=chat]");
                        sel.html("");
                        var title = "";

                        for (r in data)
                        {

                            title = truncateString(data[r].topic, 17);
                            // sel.append("<option value='" + data[r].id + "'>" + "#" + data[r].id + " " + data[r].chat_date + " - " + title + "</option>");
                            let chat_logon_time = new Date(data[r]['create_datetime']).getTime();
                            let now_time = new Date().getTime();
                            let duration = (now_time - chat_logon_time)/(3600*1000);
                            let length = (data[r].length/60).toFixed(2);


                            if(duration - 7.0012 > 72)
                            {
                                sel.append("<option value='" + data[r].id + "' disabled style='color: #d2cece'>" + "#" + data[r].id + " " + data[r].chat_date + " - " + title + "</option>");
                                continue;
                            }

                            sel.append("<option limit='" + length + "' value='" + data[r].id + "'>" + "#" + data[r].id + " " + data[r].chat_date + " - " + title + "</option>");
                        }

                    }
            });
            <?php endif; ?>
            $("select[name=reader]").change(function ()
            {
                var cid = $(this).val();
                $(".readerName").html($("option:selected", this).text());
                $.ajax({
                    dataType: "json",
                    url: '/my_account/main/nrr_chat_session/' + cid,
                    success: function (data)
                    {

                        var sel = $("select[name=chat]");
                        sel.html("");
                        var title = "";

                        for (r in data)
                        {

                            title = truncateString(data[r].topic, 17);
                            let chat_logon_time = new Date(data[r]['create_datetime']).getTime();
                            let now_time = new Date().getTime();
                            let duration = (now_time - chat_logon_time)/(3600*1000);
                            let length = (data[r].length/60).toFixed(2);


                            if(duration - 7.0012 > 72)
                            {
                                sel.append("<option value='" + data[r].id + "' disabled style='color: #d2cece'>" + "#" + data[r].id + " " + data[r].chat_date + " - " + title + "</option>");
                                continue;
                            }

                            sel.append("<option limit='" + length + "' value='" + data[r].id + "'>" + "#" + data[r].id + " " + data[r].chat_date + " - " + title + "</option>");
                        }

                        max = $('select[name=chat] option:eq(0)').attr('limit');
                        if(max != undefined) {
                            $('input.time-back').attr("placeholder", "Time Back Limit: " + max +
                                " mins");
                            $('input.time-back').attr("limit",  max);
                            $('input#max-length').val(max);
                        } else {
                            $('input.time-back').attr("placeholder", "Time Back");
                        }
                    }
                });
            });



        $("Select[name=chat]").change(function()
        {
           max =  $(this).find('option:selected').attr("limit");

           $('input.time-back').attr("placeholder", "Time Back Limit: " + max +
               "mins");
            $('input.time-back').attr("limit",  max);
            $('input#max-length').val(max);
        });

        $('input.time-back').on('keydown keyup', function(e){
            console.log($(this).val());
            console.log(max)
            if (parseFloat($(this).val()) > max) {
                e.preventDefault();
                $(this).val(max);
            }
            if (parseFloat($(this).val()) < 0) {
                e.preventDefault();
                $(this).val(0);
            }
        });

        if($('p.record').html()) {
            $.notify($('p.record').html(), {className:"success",align:"center", verticalAlign:"top", hideDuration: 700, autoHideDelay: 5000,});
        }


    });

    function truncateString(str, length) {
        return str.length > length ? str.substring(0, length - 3) + '...' : str
    }


</script>

