
<!-- new layout -->
<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Reader Profile</h4>

        <div class="card-description">

            <div class="material-datatables">

 			<div class="col-lg-8 col-md-8 col-sm-8">
                <div class="">
                    <div style="width: 200px;">
                        <img style="width: 100%; border-radius: 15px;" src="<?= $this->reader->data['profile']?>" class="profile-image"/>
                    </div>
                </div>
                <br/>
                <h3><?= $reader['username']?></h3>

                <h4>Biography</h4>
                <div class='desc'><?php echo nl2br($reader['biography']) ?></div>

                <hr/>
                <h4>Area of Expertise</h4>
                <div class='desc'><?php echo nl2br($reader['area_of_expertise']) ?></div>
                
            </div>







            </div>
        </div>
    </div>
</div>    