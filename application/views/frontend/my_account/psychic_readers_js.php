<script>
    $(document).ready(function(){

        // $('body').addClass('sidebar-mini');
        
        $('#minimizeSidebar').trigger( "click" );
        // we simulate the window Resize so the charts will get updated in realtime.
        /*9-3*/
        var reader_id;
        var client_id;
        var reader_name;
      
        var check_interval = 15000;
        var check_interval_handler = setInterval(check_reader_status, check_interval);

        function check_reader_status() {
            
            $(".div-readers").each(function(){
                var reader_id = $(this).attr("data-reader-id");
                var username = $(this).attr("data-username");
                var client_id = <?php echo ($this->session->userdata('member_logged'))? $this->session->userdata('member_logged'):0 ?>;
                
                var url = "/ajax_functions.php?mode=checkReaderMyAccountStatus&reader_id="+reader_id+"&client_id="+client_id;
                $.ajax({    
                        url: url,  
                        cache: false,  
                        success: function(retValue){
                            // convert string to json object 
                            var d = jQuery.parseJSON(retValue);

                            if (d.logoff == true) {
                                    window.location.href = "/main/logout";
                                    return;
                            }
                            var online_reders = $('.readers-container .readers-content[data-status="online"]');
                            var break_readers = $('.readers-container .readers-content[data-status="break"]');
                            var busy_readers = $('.readers-container .readers-content[data-status="busy"]');
                            var offline_readers = $('.readers-container .readers-content[data-status="offline"]');

                            switch(d.status) {
                                case 'online':
                                if (d.ban_type!=null) {
                                    $("#div-reader-" + reader_id).html('<span class="text-warning">Reader signaled a '+d.ban_type+' ban.</span>');
                                } else {
                                    // $("#div-reader-" + reader_id).html('<button data-href="/chat/main/index/'+username+'" class="btn btn-success  btn-sm btn-round btn-chat" type="button">I AM AVAILABLE</button>');
                                    $("#div-reader-" + reader_id).html( '<a  id="div-reader-" + reader_id data-username= "' +  username + '" data-reader-id="' + reader_id +  '" class="btn btn-success  btn-sm btn-round btn-chat"  href="/chat/main/index/' + username + '">I AM AVAILABLE</a>');

                                }
                                
                                if( $("#div-reader-" + reader_id).parents(".readers-content").data("status")!="online"){
                                    $("#div-reader-" + reader_id).parents(".readers-content").data("status",d.status);
                                    $("#div-reader-" + reader_id).parents(".readers-content").attr("data-status",d.status);
                                    if (online_reders.length) {

                                        $(".div-reader-" + reader_id).insertBefore( $(online_reders[0])).fadeIn().css('display','');
                                    } else {
                                        if (busy_readers.length) {
                                            $(".div-reader-" + reader_id).insertBefore( $(busy_readers[0])).fadeIn().css('display','');
                                        } else {
                                            if (break_readers.length) {
                                                $(".div-reader-" + reader_id).insertBefore( $(break_readers[0])).fadeIn().css('display','');
                                            } else {
                                                $(".div-reader-" + reader_id).insertBefore( $(offline_readers[0])).fadeIn().css('display','');
                                            }

                                    }
                                }
                            }
                                break;
                            case 'busy':

                                var xdiff_minutes = " 0 ";
                                if (d.last_pending_time!=null) {
                                    var last_pending_time = new Date(d.last_pending_time);
                                    var now = new Date();

                                    xdiff_minutes = diff_minutes(last_pending_time, now);
                                }
                                if (d.ban_type!=null) {
                                    $("#div-reader-" + reader_id).html('<span>Reader signaled a '+d.ban_type+' ban.</span>');
                                } else {
                                    /*9-20*/
                                   $("#div-reader-" + reader_id).html('<button class="btn btn-round btn-sm  btn-warning btn-sidebar btn-session" type="button"> in session: '+xdiff_minutes+' mins</button>'
                                       + '<button data-href="/chat/main/wait_in_line/' + username + '/' + reader_id + '/<?php echo($this->session->userdata("member_logged")); ?>" class="btn btn-sm btn-warning btn-sidebar btn-break btn-wait-in-line" type="button">WAIT IN LINE</button>'
                                   );
                                }
                                //console.log(d.last_pending_time);

                                    

                                    if( $("#div-reader-" + reader_id).parents(".readers-content").data("status")!="busy") {

                                        $("#div-reader-" + reader_id).parents(".readers-content").data("status",d.status);

                                        $("#div-reader-" + reader_id).parents(".readers-content").attr("data-status",d.status);

                                        if (online_reders.length) {
                                            $(".div-reader-" + reader_id).insertBefore( $(online_reders[online_reders.length-1])).fadeIn().css('display','');
                                        } else {
                                            if (break_readers.length) {
                                                $(".div-reader-" + reader_id).insertBefore( $(break_readers[0])).fadeIn().css('display','');
                                            } else {
                                                $(".div-reader-" + reader_id).insertBefore( $(offline_readers[0])).fadeIn().css('display','');
                                            }
                                        }
                                    }
                                break;
                            case 'break':
                                if (d.ban_type!=null) {
                                    $("#div-reader-" + reader_id).html('<span>Reader signaled a '+d.ban_type+' ban.</span>');
                                } else {
                                    /*9-20*/
                                   $("#div-reader-" + reader_id).html('<button class="btn btn-round btn-sm btn-warning btn-sidebar btn-break" type="button"> Be Right Back: '+ d.break_time_amount +' mins</button>'
                                       + '<button data-href="/chat/main/wait_in_line/' + username + '/' + userid + '/<?php echo($this->session->userdata("member_logged")); ?>" class="btn btn-round btn-sm btn-warning btn-sidebar btn-break btn-wait-in-line" type="button">Wait In Line</button>'
                                   );

                                }

                                    if( $("#div-reader-" + reader_id).parents(".readers-content").data("status")!="break"){
                                        $("#div-reader-" + reader_id).parents(".readers-content").data("status",d.status);
                                        $("#div-reader-" + reader_id).parents(".readers-content").attr("data-status",d.status);

                                        if (offline_readers.length) {
                                            $(".div-reader-" + reader_id).insertBefore( $(offline_readers[0])).fadeIn().css('display','');
                                        } else {
                                            if (busy_readers.length) {
                                                $(".div-reader-" + reader_id).insertBefore( $(busy_readers[busy_readers.length-1])).fadeIn().css('display','');
                                            } else {
                                                $(".div-reader-" + reader_id).insertBefore( $(online_reders[online_reders.length-1])).fadeIn().css('display','');
                                            }
                                        }
                                    }

                                    break;
                                case 'offline':
                                    if (d.ban_type!=null) {
                                        $("#div-reader-" + reader_id).html('<span>Reader signaled a '+d.ban_type+' ban.</span>');
                                    } else {
                                       // $("#div-reader-" + reader_id).html('<button class="btn-sidebar btn-offline" type="button"> offline</button>');
                                       $("#div-reader-" + reader_id).html('<button class="btn btn-round btn-sm btn-sidebar btn-offline" type="button"> offline</button>'+'<button class="btn btn-round btn-sm btn-sidebar btn-offline btn-appointment" type="button"> Appointment <i class="fa fa-calendar"></i></button>'+'<input type="hidden" class="reader-id" value="'+ reader_id + '">' 
                                        +'<input type="hidden" class="client-id" value="<?php echo($this->session->userdata("member_logged")); ?>">'
                                        +'<input type="hidden" class="reader-name" value="' + username + '">');
                                    }
                                
                                if( $("#div-reader-" + reader_id).parents(".readers-content").data("status")!="offline"){
                                    $("#div-reader-" + reader_id).parents(".readers-content").data("status",d.status);
                                    $("#div-reader-" + reader_id).parents(".readers-content").attr("data-status",d.status);

                                    if (offline_readers.length) {

                                        $(".div-reader-" + reader_id).insertAfter( $(offline_readers[offline_readers.length-1])).fadeIn().css('display','');

                                    } else {
                                        if (break_readers.length) {
                                            $(".div-reader-" + reader_id).insertBefore( $(break_readers[break_readers.length-1])).fadeIn().css('display','');
                                        } else {
                                            if (busy_readers.length) {
                                                $(".div-reader-" + reader_id).insertBefore( $(busy_readers[busy_readers.length-1])).fadeIn().css('display','');
                                            } else {
                                                $(".div-reader-" + reader_id).insertBefore( $(online_reders[online_reders.length-1])).fadeIn().css('display','');
                                            }
                                        }
                                    }
                                }


                                break;
                                default:
                                    console.log('undefined status');
                                break;

                            }
                        }  
                }); 
                
                
            });
            
            
            
        }

    });
</script>
