<div class="row">
    <div class="my-container page-70-30">
        <div class="content-block-30">
            <?php $this->load->view('frontend/my_account/nav/dashboard_menu'); ?>
        </div><!-- .content-block-20 -->

        <div class="content-block-70">
            <div class="row">
                <div class="col-md-12 col-sm-7">
                    <h2><?= $title ?></h2>
                    <hr/>
                    <ul class="nav nav-tabs">
                        <li <?= ($this->uri->segment('3') == '' ? " class=\"active\"" : "") ?>><a href="/my_account/system_email"><span class="fa fa-user"></span> NoBo</a></li>
                        <li <?= ($this->uri->segment('3') == 'email_setting' ? " class=\"active\"" : "") ?>><a href="/my_account/system_email/email_setting"><span class='fa fa-gear'></span> Setting</a></li>
                    </ul>

                    <div class='well'>
                        Topic: <?=$subject?><br />
                        From: <?=$from['first_name']?> <?=$from['last_name']?><br />
                        To: <?=$to['first_name']?> <?=$to['last_name']?><br />
                        Sent: <?=date("M d, Y @ h:i A", strtotime($datetime))?> EST
                    </div>

                    <?=nl2br($message)?>
                    <hr />
<!--                    <a href='/my_account/messages/compose/--><?//=$id?><!--' class='btn'>Send A Reply</a>-->
                </div>
            </div>
        </div><!-- .content-block-80 -->
    </div><!-- .my-container -->
</div>