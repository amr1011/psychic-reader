<!-- new layout -->
<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">My Email Readings</h4>
        <div class="card-description">
            <div class="table-responsive">
                        <?php if ($emails) : ?>
                        <table class="table">
                            <thead class=" text-primary"></thead>
                            <tbody>
                            <?php foreach ($emails as $e) : ?>
                                 <tr>
                                    <td><b><?php echo $e['package_title'];?></b>
                                        <?php echo date("m/d/Y @ h:i A", strtotime($e['datetime'])); ?>
                                    </td>
                                    <td><?php echo number_format($e['price'], 2);?></td>
                                    <td><?php echo ($e['status']=='new' ? "<span class='label label-important'>Waiting For Reading</span>" : "<span class='label label-success'>Complete</span>") ?>                                        
                                    </td>
                                    <td>
                                        <a href='/my_account/email_readings/client_view/<?php echo $e['id'];?>' class='btn'>View
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>


                        <?php else : ?>
                                <p>You have not ordered any email readings</p>
                        <?php endif; ?>

            </div>


        </div>
    </div>
</div>
