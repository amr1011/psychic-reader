<style>
    div.content {
        padding: 20px;
        font-size: 16px;
        line-height: 10px;
        font-family: monospace;
        border: 1px solid #95c7b2;
        border-radius: 4px;
    }
</style>
<div class="row">
    <div class="my-container page-70-30">

        <div class="content-block-30">
            <?php $this->load->view('frontend/my_account/nav/dashboard_menu'); ?>
        </div><!-- .content-block-20 -->

        <div class="content-block-70">

            <div class="row">
                <div class="col-md-12 col-sm-7">

                    <h2>Message</h2>
                    <hr/>

                    <ul class="nav nav-tabs">
                        <li <?=($this->uri->segment('3')==''||$this->uri->segment('3')=='view' ? " class=\"active\"" : "")?>><a href="/my_account/messages">Inbox</a></li>
                        <li <?=($this->uri->segment('3')=='outbox' ? " class=\"active\"" : "")?>><a href="/my_account/messages/outbox">Outbox</a></li>
                        <li <?=($this->uri->segment('3')=='compose' ? " class=\"active\"" : "")?>><a href="/my_account/messages/compose"><span class='icon icon-comment'></span> Compose A New Message</a></li>
                    </ul>

                    <div class='well'>
                        From: <?=$from['first_name']?> <?=$from['last_name']?><br />
                        To: <?=$to['first_name']?> <?=$to['last_name']?><br />
                        Sent: <?=date("M d, Y @ h:i A", strtotime($datetime))?> EST
                    </div>

                    <div class="content">
                        <?= html_entity_decode($message) ?>
                    </div>

                    <?php if ($file) : ?>
                        <a href="/uploads/<?php echo($file); ?>" download>
                            <img src="/uploads/<?php echo($file); ?>" style="width:100px;"/>
                        </a>
                    <?php endif; ?>
                    <hr />

                    <a href='/my_account/messages/compose/<?=$id?>' class='btn btn-primary'>Send A Reply</a>

                </div>

            </div>

        </div><!-- .content-block-80 -->

    </div><!-- .my-container -->
 </div>