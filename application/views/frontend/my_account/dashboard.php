<!-- 9-4 -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/css/styles/alert-bangtidy.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/css/styles/alert-blackgloss.css">

<div class="card-header card-header-image">

    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title"></h4>
        <div class="card-description">
            <div class="row">
                <div class="col-md-6">

                    
                    <h4 class="text-warning" id="no-reader-text">There are less than 2 online readers right now.</h4>
                    


                    <h4>Hi <?=$this->member->data['first_name']?> <?=$this->member->data['last_name']?></h4>

                    <p>Promote yourself using this URL:<a href="<?=SITE_URL ?>/profile/<?=$this->member->data['username']?>"><?=SITE_URL ?>/profile/<?=$this->member->data['username']?></a></p>
                    

                     <form action="/my_account/main/set_break_time" method="POST" role="form">
                            <label for="ReaderBreakTime">Set Break time</label>
                            <select name="ReaderBreakTime" id="ReaderBreakTime" class="form-control" required="required">
                                <option value="0" <?php  echo((  (int)$this->member->data['break_time_amount']==0)?  "selected":null)  ?> >Select the “Break time” amount</option>
                                <option value="2" <?php  echo((  (int)$this->member->data['break_time_amount']==2 )?  "selected":null)  ?> > 2 minutes </option>
                                <option value="5" <?php  echo((  (int)$this->member->data['break_time_amount']==5 )?  "selected":null)  ?> > 5 minutes </option>
                                <option value="7" <?php  echo((  (int)$this->member->data['break_time_amount']==7 )?  "selected":null)  ?> > 7 minutes </option>
                                <option value="10" <?php echo ((  (int)$this->member->data['break_time_amount']==10 )? "selected":null)  ?> > 10 minutes</option>
                            </select>
                            <button type="submit" class="btn btn-primary" style="margin-top: 20px;">Save</button>
                        
                    </form>

                    <label for="reader-doorbell-sound">Set Sound Notification</label>
                    <select name="reader-doorbell-sound" id="reader-doorbell-sound" class="form-control" required="required">
                        <option value="" selected="true">Select the “Doorbell sound” </option>
                        <option value="door_bell"> door bell </option>
                        <option value="bell_ring"> bell ring </option>
                        <option value="church_bell"> church bell </option>
                        <option value="railroad_bell"> railroad bell </option>
                        <option value="salamisound_bell"> salamisound bell </option>
                        <option value="service_bell"> service bell </option>
                        <!-- <option value="siren_bell"> siren bell </option> -->
                        <option value="warbling_bell"> warbling bell </option>
                    </select>
                        <button id="save-doorbell-sound" class="btn btn-primary" style="margin-top: 20px;">Save</button>
                        <div id="reader-doorbell-sound-value" style="display: none;"></div>


                </div>
                <div class="col-md-6">
                        <h4>Balance (US):  <span>$<?=number_format($this->reader->get_balance('us'),2)?></span></h4>
                        <h4>Balance (CAN): <span>$<?=number_format($this->reader->get_balance('ca'),2)?></span></h4>

                        <div class="btn-group">
                            <a id="reader_status_online_button" href='/my_account/main/set_status/online' class="btn  <?=($this->member->data['status']=='online' ? "btn-success" : (($this->member->data['status']=='busy')? "btn-warning":"btn-default")    )?>">Online</a>
                            <a id="reader_status_break_button" href='/my_account/main/set_status/break' class="btn <?=($this->member->data['status']=='break' || $this->member->data['status']=='booked' || $this->member->data['status']=='busy' ||  $this->member->data['status']=='away' ? "btn-primary" : "btn-default")?>">Break</a>
                            <a id="reader_status_offline_button" href='/my_account/main/set_status/offline' class="btn <?=($this->member->data['status']=='offline' ? "btn-default active" : "btn-default")?>">Offline</a>                        
                        </div>   
                        <a class="btn btn-primary" id="logoutbtn" href="main/logout">Logout</a>
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer">
    
    </div>
  </div>
</div>

