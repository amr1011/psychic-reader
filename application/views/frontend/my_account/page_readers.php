

<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Appointment List</h4>

        <div class="card-description">

            <div class="material-datatables">

                 <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>User Info</legend>
                            <h4>Name: <?= $client['username']?> (<?= $client['first_name']?>)</h4>
                            <h4>Status: Client</h4>
                                <p>Please fill out this request form and hit the submit button</p>
                                <p>Readers will be notified immediately and will either contact you via our email system, or they will log on.</p>
                        </fieldset>

                        <div class="row">
                              <label class="col-sm-3 col-form-label label-checkbox">Request Chat Session</label>
                              <div class="col-sm-4 checkbox-radios">
                                
                                    <div class="form-check">
                                      <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="page-reader" value="1" checked/>
                                        <span class="circle">
                                          <span class="check"></span>
                                        </span>
                                      </label>
                                    </div>

                              </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 col-form-label">With in next:</label>
                              <div class="col-sm-10">
                                <div class="form-group">
                                    <select name="time-size" class="time-size selectpicker" data-style="select-with-transition">
                                        <option value="15" selected>15 minutes</option>
                                        <option value="30">30 minutes</option>
                                        <option value="60">1 hour</option>
                                    </select>

                                </div>
                              </div>
                        </div>

 
                        <div class="row">
                            <label class="col-sm-2 col-form-label">Topic</label>
                              <div class="col-sm-10">
                                <div class="form-group">
                                 <input type="text" class="form-control topic" placeholder="Please input topic">
                                </div>
                              </div>
                        </div>                           

                        <h4>Chat Appointment</h4>      
                           
                        <div class="row">
                            <label class="col-sm-3 col-form-label">Make an appointment</label>
                              <div class="col-sm-8">
                                <div class="form-group">
                                 <input type="radio" name="page-reader" value="2" />
                                </div>
                              </div>
                        </div>        

                        <div class="row">
                            <label class="col-sm-3 col-form-label">Select Date</label>
                              <div class="col-sm-8">
                                <div class="form-group">                                    
                                    <input id="datepicker" class="form-control">

                                </div>
                              </div>
                        </div>        
                        <fieldset>

                            <div>
                            </div>
                        </fieldset>
                        <div class="row">
                            <div class="content-pagination col-sm-8 " >
                                <div class="view-number-content col-sm-3" >
                                    <select class="view-number form-control">
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                    </select>
                                    <div class="pagination col-sm-8"></div>                                
                                </div>
                            </div>
                        </div>
                    
                        <table class='table table-striped table-hover table-bordered' style="width: 100%;margin-top: 40px;">
                            <thead>
                            <tr>
                                <th style="width: 80%">Reader Name</th>
                                <th><input type="checkbox" class="select-all">&nbsp;&nbsp;<span>Select All</span></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($readers as $reader): ?>
                                <tr style="display: none;">
                                    <td><?= $reader['username']?></td>
                                    <td><input type="checkbox" class="select-one" value="<?= $reader['id']?>"/></td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                        <button class="btn btn-primary send-notification" style="margin-bottom: 30px;">Send Notification</button>

                                <input type="hidden" class="client-id" value="<?php echo($this->session->userdata('member_logged')); ?>">

                </div>
            </div>
        </div>
    </div>
</div>    

