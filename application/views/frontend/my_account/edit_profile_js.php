
<script>

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#profile_image').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).ready(function () {

        var profile_image;
        var categories;

        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    profile_image = e.target.result;
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input[name=profile_image]").change(function() {
            readURL(this);
        });

        $('#submitBtn').click(function() {

            categories = '';

            $('#email').text($('input[name=email]').val());
            $('span#username').text($('span[name=username]').text());
            $('#pp-email').text($('input[name=paypal_email]').val());
            $('#first-name').text($('input[name=first_name]').val());
            $('#last-name').text($('input[name=last_name]').val());
            $('#gender').text($('input[name=gender]').val());
            $('#month').text($('select[name=dob_month]').val());
            $('#day').text($('select[name=dob_day]').val());
            $('#year').text($('select[name=dob_year]').val());
            $('#country').text($("select[name=country] option:selected").text());

            if(profile_image == null) {
                $('img#profile-image').attr('src', '<?php echo($this->member->data['profile_image'])?>');
            } else {
                $('img#profile-image').attr('src', profile_image);
            }

            $('span#title').text($('input[name=title]').val());

            $('input[name="categories[]"]:checked').each(function () {
                categories = categories + $(this).parent().find('span.category-title').text() + ',';
            });

            $('span#categories').text(categories);
            $('span#biography').text($.trim($("textarea[name=biography]").val()));
            $('span#area_of_expertise').text($.trim($("textarea[name=area_of_expertise]").val()));
            $('#enable_email_readings').text($('input[name=enable_email]').prop("checked") == true ? 'Yes' : 'No');
            $('#enable_phone_readings').text($('input[name=enable_phone]').prop("checked") == true ? 'Yes' : 'No');
            $('#email_total_days').text($('input[name=email_total_days]').val());
        });

        $('#submit').click(function(){
            $('#form-field').submit();
        });
    })
</script>

