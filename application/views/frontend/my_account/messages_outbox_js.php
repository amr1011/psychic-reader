
<script>
    function insertMessage(messages) {
        console.log('messages'+messages);
        let view = "<div class='table-responsive'><table width='80%' cellPadding='0' cellSpacing='0' class='table table-striped table-hover'>";
        var member = null;
        for(let i=0; i<messages.length; i++) {

            let class_name = 'read';
            if(messages[i]['read'] == 0) {
                class_name = 'unread';
            }

            view = view + "<tr class="+class_name+">" + "<td width='150'>" + messages[i]['datetime'] + " EST</td>"
                + "<td>" + messages[i]['subject'] + "</td>"
                + "<td width='50' align='center'><a href='/my_account/messages/view/"+ messages[i]['id'] + "' class='btn'>View</a></td>"
                + "<td width='50' align='center'><a href='/my_account/messages/delete/" + messages[i]['id'] + "' onClick=\"Javascript:return confirm('Are you sure you want to delete this message?');\" class='btn'>Delete</a></td>"
                + "</tr>";
        }
        view = view + "</table></ div>";
        return view;
    }

    $(document).ready(function () {

        var messages;
        var admin_messages;

        function getData(receiver_id) {
            $.ajax({
                'type': "POST",
                'url': "/my_account/messages/getSentMessages",
                'data': { user_id : receiver_id},
                success: function (data) {

                    let obj = JSON.parse(data);
                    messages = obj.messages;
                    console.log(messages);
                }
            });
        }

        function getAdminData() {
            $.ajax({
                'type': "POST",
                'url': "/my_account/messages/getAdminSentMessages",
                'data': {},
                success: function (data) {

                    let obj = JSON.parse(data);
                    admin_messages = obj.messages;
                    console.log(admin_messages);
                }
            });
        }

        $(".message-folder").click(function () {

            let sender_id = $(this).find('.sender-id').html();
            getData(sender_id);

            $(".box-message").empty();
            let folder_status = $(this).find(".folder-status").val();
            if(folder_status == 0) {

            } else {
                setTimeout(function () {
                    let view = insertMessage(messages);
                    $(".box-message").append(view);
                }, 500);
            }
        });

        $(".admin-folder").click(function () {

            getAdminData();

            $(".box-message").empty();
            let folder_status = $(this).find(".folder-status").val();
            if(folder_status == 0) {

            } else {
                setTimeout(function () {
                    let view = insertMessage(admin_messages);
                    $(".box-message").append(view);
                }, 500);
            }
        });

        // folder open/close

        $(".notification-folder .enabled").click(function () {

            let folder_status = $(this).parent().find(".folder-status").val();
            if(folder_status == 0) {
                $(".fa-folder-open").removeClass("fa-folder-open").addClass("fa-folder");
                $(".folder-status").val(0);
                $(".folder-name").css('color', '#4c4b4b');
                $(this).parent().find(".fa-folder").removeClass("fa-folder").addClass("fa-folder-open");
                $(this).parent().find(".folder-name").css('color', '#337ab7');
                $(this).parent().find(".folder-status").val(1);

                // show contained messages

            } else {
                $(this).parent().find(".fa-folder-open").removeClass("fa-folder-open").addClass("fa-folder");
                $(this).parent().find(".folder-status").val(0);
                $(".folder-name").css('color', '#4c4b4b');
            }
        });
    });
</script>




