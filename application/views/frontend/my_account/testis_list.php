<style>
    td {
        word-wrap: break-word
    }
</style>
<div class="row">
    <div class="my-container page-70-30">
        <div class="content-block-30">
            <?php $this->load->view('frontend/my_account/nav/dashboard_menu'); ?>
        </div><!-- .content-block-20 -->

        <div class="content-block-70">
            <div class="row">
                <div class="col-md-12 col-sm-7">    
                    <h2>Submitted Testimonials</h2>
                    <hr />
                    <?php if (count($testis) > 0): ?>
                        <table class='table table-striped table-hover table-bordered' style="border: 1px solid black; width: 100%; word-wrap:break-word;
              table-layout: fixed;">
                        <tr>
                            <th>Reader</th>
                            <th>Date</th>
                            <th>Rating</th>
                            <th>Review</th>
                        </tr>
                        <?php foreach ($testis as $t): ?>
                            <tr>
                                <td style="vertical-align:top;width:60px"><?= $t['username']?></td>
                                <td style="vertical-align:top;width:175px;"><?= date('m/d/Y h:i:s a', strtotime($t['datetime'])) ?></td>
                                <td style="vertical-align:top;width:25px"><?= $t['rating'] ?></td>
                                <td style="vertical-align:top;"><?= html_entity_decode(nl2br($t['review'])) ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </table>
                    <?php else: ?>
                        <p>You have no testimonials.</p>
                    <?php endif; ?>

                </div>
            </div>
        </div><!-- .content-block-80 -->
    </div><!-- .my-container -->
</div>