<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Client List</h4>

        <div class="card-description">
            <div class="material-datatables">
            <div class="row">


                <div class="col-md-12 col-sm-7">    

                    <form class="filter-table-form-username" action='/my_account/main/filter_client_list_username/' method='POST' style="display:none;">
                        <input name="username_direction" value="<?= $username_direction?>"/>
                    </form>

                    <form class="filter-table-form-firstname" action='/my_account/main/filter_client_list_firstname/' method='POST' style="display: none;">
                        <input name="firstname_direction" value="<?= $firstname_direction?>"/>
                    </form>

                    <form class="search_form_client_list"action='/my_account/main/client_search/' method='POST' class='form-inline'>


                    <div class="row">
                      <label class="col-sm-2 col-form-label">Search </label>
                      <div class="col-sm-10">
                        <div class="row">
                          <div class="col-md-3">
                            <div class="form-group">
                                <select name='search_type' class='form-control classic' style='margin:0;'>
                                        <option value='username' <?= ($this->input->post("search_type") == 'username' ? "selected" : "") ?> >Username</option>
                                        <option value='client_first_name' <?= ($this->input->post("search_type") == 'client_first_name' ? "selected" : "") ?> >Clients First Name</option>
                                </select>                              

                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                            <input type='text' class='form-control' name='query' value='<?= $this->input->post('query') ?>' style='margin:0;' placeholder='Search for clients' class='input-xlarge' required/>
                            </div>
                          </div>
                          <div class="col-md-5">
                            <div class="form-group">
                            <input type='submit' value='Search' class='btn btn-primary search-client'>
                            <button class="btn btn-success refresh" >Reset Filter</button>

                            </div>
                          </div>

                        </div>
                      </div>
                    </div>

                    </form>

                    <?php if (count($clients) > 0): ?>
                            
                        <div class="col-md-5">
                                <select class="form-control">
                                    <option value="5">5</option>
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                </select>
                            <div class="pagination" ></div>
                        </div>

                        <table class="table table-striped table-bordered" >
                            <thead>
                                <th id="username-th" >Username<?php if($username_direction == 'ASC') : ?><i class="fa fa-sort-asc" style="margin-left: 5px; font-size: 12px;"></i><?php else:?><i class="	fa fa-sort-desc" style="margin-left: 5px; font-size: 12px;"></i><?php endif; ?></th>
                                <th id="firstname-th" >First Name<?php if($firstname_direction == 'ASC') : ?><i class="fa fa-sort-asc" style="margin-left: 5px; font-size: 12px;"></i><?php else:?><i class="fa fa-sort-desc" style="margin-left: 5px; font-size: 12px;"></i><?php endif; ?></th>
                                <th>Chat History</th>
                                <th >Message</th>
                            </thead>
                            <?php foreach($clients as $c): ?>
                            <tr style="display: none;">
                                <td class="text-center"><?= $c['username'] ?></td>
                                <td class="text-center"><?= $c['first_name'] ?></td>
                                <td class="text-center">

                                    <div class="col-md-10 col-sm-10 col-xs-10 text-center">
                                            <div class="row">Start</div>                                            
                                            <div class="row">                                            
                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                    <select class="start-month form-control">
                                                        <option value="" selected disabled>Month</option>
                                                        <option value="1">Jan</option><option value="2">Feb</option><option value="3">Mar</option><option value="4">Apr</option>
                                                        <option value="5">May</option><option value="6">Jun</option><option value="7">Jul</option><option value="8">Aug</option>
                                                        <option value="9">Sep</option><option value="10">Oct</option><option value="11">Nov</option><option value="12">Dec</option>
                                                    </select></div>
                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                    <select class="start-day form-control">
                                                        <option value="" selected disabled>Day</option>
                                                        <option value="1" >1st</option>
                                                        <option value="16">16th</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                    <select class="start-year form-control">
                                                        <option value="" selected disabled>Year</option>
                                                        <option value="<?= $year?>" ><?= $year?></option>
                                                        <option value="<?= $year-1?>"><?= $year-1?></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                    End
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                    <select class="end-month form-control">
                                                        <option value="" selected disabled>Month</option>
                                                        <option value="1" >Jan</option><option value="2">Feb</option><option value="3">Mar</option><option value="4">Apr</option>
                                                        <option value="5">May</option><option value="6">Jun</option><option value="7">Jul</option><option value="8">Aug</option>
                                                        <option value="9">Sep</option><option value="10">Oct</option><option value="11">Nov</option><option value="12">Dec</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                    <select class="end-day form-control">
                                                        <option value="" selected disabled>Day</option>
                                                        <option value="15">15th</option>
                                                        <option value="31">EOM</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                    <select class="end-year form-control">
                                                        <option value="" selected disabled>Year</option>
                                                        <option value="<?= $year?>" ><?= $year?></option>
                                                        <option value="<?= $year-1?>"><?= $year-1?></option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                        <a class="btn-sm btn-success btn-view" href="/my_account/chats/view_chats/<?php echo $this->member->data['id']?>/<?= $c['mid']?>/">View</a>
                                    </div>
                                </td>
                                <td ><a class="btn btn-primary" href="/my_account/messages/compose/<?= $c['mid'] ?>/send">Send Message</a></td>
                            </tr>
                            <?php endforeach; ?>
                        </table>
                        <div class="page-number"></div>
                    <?php else: ?>
                        <p>You have no clients.</p>
                    <?php endif; ?>
                </div>
            </div>                
        </div>
    </div>
</div>



           