<style>
    input[type='radio'] {
        display: inline-block !important;
    }
    .alert-error {
        color: #b94a48;
        background-color: #f2dede;
        border-color: #eed3d7;
    }
    .alert {
        margin-bottom: 0px;
    }
</style>

 <div class="col-md-12">
              <div class="card ">
                <div class="card-header card-header-rose card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">payment</i>
                  </div>
                  <h4 class="card-title">Fund Your Account</h4>
                </div>
                <div class="card-body ">

                    <?php
                    if($this->session->flashdata('error'))
                    {
                        echo "<div class='alert alert-error page-notifs'><strong>There are errors:</strong><p>".$this->session->flashdata('error')."</p></div>";
                    }
                    ?>
                    <h4 class="card-title ">Select A Package</h4>

 

                        <?php foreach ($this->site->get_packages() as $r) : ?>
                        <div class="form-check form-check-radio">

                            <?php if (($r['promo'] == 1 && $this->member->data['received_promo'] != 1) || $r['promo'] == 0): ?>
                            <label class="form-check-label">

                                <input type='radio' class="form-check-input" promo='<?php echo $r['promo']; ?>' name='package'
                                           value='<?php echo $r['id']; ?>'>
                                <?php echo $r['title']; ?>           
                                <span class="circle">
                                    <span class="check"></span>
                                </span>
                            </label>
                            <?php endif; ?>

                        </div>       
                        <?php endforeach; ?>
                    

                        <h4 class="card-title">Select Payment Method</h4>

                      <div id='billing_profile_div' style='margin:15px 0;'>
                        <span style='color:#999;'>Select A Package Above
                        </span>
                       </div>
                    <div id="add_new_card" style='margin:15px 0;'></div>


                </div>
                <!-- <div class="card-footer ">
                  <button type="submit" class="btn btn-fill btn-rose">Submit</button>
                </div> -->
              </div>
            </div>




