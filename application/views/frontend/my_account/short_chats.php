<style>
    td, th {
        text-align: center;
    }

    .pagination {
        display: inline-block;
        margin: 0px;
    }

    .pagination button {
        color: black;
        float: left;
        padding: 1px 10px;
        text-decoration: none;
    }

    .pagination button.active {
        background-color: #4CAF50;
        color: white;
        border-radius: 5px;
    }

    .pagination button:hover:not(.active) {
        background-color: #ddd;
        border-radius: 5px;
    }
    button, .btn-sm {
        cursor: pointer;
        border: 0px;
        border-radius: 5px;
        margin: 1px;
    }
    .content-pagination {
        display: inline-flex;
        float: right;
        margin-top: 30px;
    }
    .btn-previous, .btn-next {
        margin-top: 20px;
    }
    .btn-previous:hover, .btn-next:hover {
        background-color: #0a6aa1;
        color: white;
    }
    .pagination button {
        width: 35px;
    }
    select.view-number {
        background-image: linear-gradient(45deg, transparent 50%, black 50%),
        linear-gradient(135deg, black 50%, transparent 50%),
        linear-gradient(to right, #ffffff, #ffffff);
        background-position:
                calc(100% - 24px) calc(1em - 4px),
                calc(100% - 19px) calc(1em - 4px),
                100% 0;
        background-size:
                5px 5px,
                5px 5px,
                2.5em 2.5em;
        background-repeat: no-repeat;
    }
    td {
        vertical-align: middle;
    }
    .isDisabled {
        color: currentColor;
        cursor: not-allowed;
        opacity: 0.5;
        text-decoration: none;
        pointer-events: none;
    }
</style>



<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Short Chats Less Than 4 Mins</h4>

        <div class="card-description">


                     <form class="filter-table-form-username" action='/my_account/chats/filter_chats_username/' method='POST' style="display:none;">
                        <input name="username_direction" value="<?= $username_direction?>"/>
                    </form>
                    <form class="filter-table-form-datetime" action='/my_account/chats/filter_chats_datetime/' method='POST' style="display: none;">
                        <input name="datetime_direction" value="<?= $datetime_direction?>"/>
                    </form>
                    <?php if($search == 1) {?>
                        <form class="search_form_client_list"action='/my_account/chats/client_short_chat_search/' method='POST' class='form-inline'>
                            <div class="form-group">
                                <div class="col-md-4 col-sm-4" style="margin-bottom:15px;">
                                    <input type='text' class='form-control' name='query' value='<?= $this->input->post('query') ?>' style='margin:0;' placeholder='Search with Username' class='input-xlarge' required/>
                                </div>
                                <div class="col-md-2 col-sm-2" style="margin-bottom:15px;">
                                    <input type='submit' value='Search' class='btn btn-round btn-sm btn-primary search-client' style='margin:0;'>
                                </div>
                            </div>
                        </form>
                        <button class="btn btn-round btn-sm btn-success refresh" >Reset Filter</button>
                    <?php }?>
                    <?php echo validation_errors(); ?>

                    <?php if ($chats) : ?>

                            

                        <table class='table table-striped table-hover table-bordered'>
                            <thead>
                                <?php //if($this->member->data['member_type'] == 'client') {?>
                                <!--    <th>Feedback</th>-->
                                <?php //}?>
                                <th id="datetime-th">Date</th>
                                <th id="username-th">Username</th>
                                <?php if($this->member->data['member_type'] == 'reader') {?>
                                <th id="session-th">Chat Session</th>
                                <?php }?>
                                <th>Subject</th>
                                <th>Duration</th>
                                <th>Paid Time</th>
                                <th>Free Time</th>
                                <?php if($this->member->data['member_type'] == 'reader') {?>
                                    <th>Due to Reader</th>
                                <?php }?>
                                <th>Details</th>
                                <?php if($this->member->data['member_type'] == 'client') {?>
                                    <th>NRR</th>
                                <?php }?>
                                <?php if($this->member->data['member_type'] == 'client') {?>
                                    <th>Delete</th>
                                <?php }?>
                                <th style="display:none"></th>
                                <th style="display:none"></th>
                            </thead>
                            <?php foreach ($chats as $c) : ?>
                                <?php
                                if ($this->member->data['id'] == $c['client_id'])
                                {
                                    //--- Get reader
                                    $user = $this->system_vars->get_member($c['reader_id']);
                                } else
                                {
                                    //--- Get client
                                    $user = $this->system_vars->get_member($c['client_id']);
                                }
                                
                                $fname = $this->member->data['first_name'];
                                $email = $this->member->data['email'];
                                $country = $this->member->data['country'];
                                $time = date("m/d/Y @ h:i A", strtotime($c['start_datetime']));

                                if ($this->member->data['member_type'] == "reader")
                                {
                                    $type = 1;
                                    $murl = "http://www.psychic-contact.com/modules/chattestingform/reader_form.php?un=" . $this->member->data['username'] . "&cn=" . $user['username'] . "&fn=$fname&email=$email&country=$country&time=$time&type=$type";
                                    $link_name = "Submit";
                                } else
                                {
                                    $type = 0;
                                    $murl = "http://www.psychic-contact.com/modules/chattestingform/client_form.php?un=" . $this->member->data['username'] . "&rn=" . $user['username'] . "&fn=$fname&email=$email&country=$country&time=$time&type=$type";
                                    $link_name = "Submit";
                                }
                                $t_url = "<br><a href=\"$murl\" target=\"_blank\">[$link_name Feedback]</a>";
                                ?>

                                <tr style="display: none;">
                                    <?php //if($this->member->data['member_type'] == 'client') {?>
                                    <!--    <td width='175' style='vertical-align:middle;'>--><?php //echo $c['id'] . " $t_url "; ?><!--</td>-->
                                    <?php //}?>
                                    <td width='175' style='vertical-align:middle;'><?php echo date("m/d/Y @ h:i A", strtotime($c['start_datetime'])); ?></td>
                                    <td style='vertical-align:middle;'><?php echo $user['username'] ?></td>
                                    <?php if($this->member->data['member_type'] == 'reader') {?>
                                        <td><?php echo($c['chat_session_id'])?></td>
                                    <?php }?>
                                    <td><?php echo($c['topic'])?></td>
                                    <td><?php echo round(((floatval ($c['length']) + floatval($c['total_free_length'])) - (floatval($c['length']) + floatval($c['total_free_length'])) % 60)/60, 0) . 'mins and ' . (floatval($c['length']) + floatval($c['total_free_length'])) % 60 . 'secs'?></td>
                                    <td><?php echo round(((floatval ($c['length'])) - (floatval($c['length'])) % 60)/60, 0) . 'mins and ' . (floatval($c['length'])) % 60 . 'secs'?></td>
                                    <td><?php echo($c['total_free_length'] ? round(((floatval($c['total_free_length'])) - floatval($c['total_free_length']) % 60)/60, 0) . 'mins and ' . floatval($c['total_free_length']) % 60 . 'secs' : 0 .'secs')?></td>
                                    <?php if($this->member->data['member_type'] == 'reader') {?>
                                        <td><?php echo($this->member->data['username'])?></td>
                                    <?php }?>
                                    <td style='width:100px; vertical-align:middle; text-align:right'><a href='/my_account/chats/transcript/<?php echo $c["id"]?>' class='btn'>Details</a><?php if($this->member->data['member_type'] == 'reader') {?><button href="" class="btn-sm btn-success btn-request" data-toggle='modal' data-target='#confirm-submit'>Request Payment</button><?php }?></td>
                                    <td style="display: none"><?php echo($user['first_name'])?></td>
                                    <td style="display: none"><?php echo($c['id'])?></td>
                                </tr>

                            <?php endforeach; ?>

                        </table>

                        <div class="view-number-content" >
                                <select class="view-number">
                                    <option value="5">5</option>
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                </select>
                            </div>
                        <div class="pagination" ></div>

                        <div class="page-number" ></div>
                    <?php else : ?>
                        <div>You do not have a chat history.</div>
                    <?php endif; ?>            


        </div>
    </div>
</div>    




<div class="modal" id="confirm-submit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3><b>Request Payment For Short Sessions</b></h3>
            </div>
            <form action="/my_account/main/request_short_chat_payment" method="post">
                <div class="modal-body">
                    <div class="row">
                        <label class="col-md-4 label-control"><b>Date</b></label>
                        <div class="col-md-8">
                            <span id="date" ></span>
                            <input name="date" type="hidden"/>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-4 label-control"><b>Chat ID</b></label>
                        <div class="col-md-8">
                            <span id="chat-id" ></span>
                            <input name="chat-id" type="hidden"/>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-4 label-control"><b>Client First Name</b></label>
                        <div class="col-md-8">
                            <span id="first-name" ></span>
                            <input name="first-name" type="hidden"/>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-4 label-control"><b>Client User Name</b></label>
                        <div class="col-md-8">
                            <span id="username" ></span>
                            <input name="username" type="hidden"/>
                        </div>
                    </div>

                    <input name="comments" type="hidden"/>

                    <div class="row">
                        <label class="col-md-4 label-control"><b>Reason</b></label>
                        <div class="col-md-8">
                            <p style="display: inline-flex;"><input type="radio" name="reason" style="width: 15px; height: 15px; display: block; margin-right:3px;" value="Client left the chat."/>  Client left the chat.</p>
                            <p style="display: inline-flex;"><input type="radio" name="reason" style="width: 15px; height: 15px;  display: block; margin-right: 3px;" value="This was a continue of a previous chat session."/> This was a continue of a previous chat session.</p>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" id="submit" class="btn btn-success success"/>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {

        $('.refresh').click(function () {

            location.href = window.location.protocol + "//" + window.location.host + "/" + "my_account/chats";
        });

        var pagination = function(per_page) {

            var number_logs = $("tbody tr").length;
            var number_logs_per_page = per_page;
            var pages = number_logs/number_logs_per_page;
            var page_module = number_logs % number_logs_per_page;
            var group_number = 1;

            pages = Math.trunc(pages);
            $('div.pagination').html('');
            if($('button.btn-previous')) {
                $('button.btn-previous').remove();
            }
            if($('button.btn-next')) {
                $('button.btn-next').remove();
            }
            if(page_module > 0) {
                pages = pages + 1;
            }
            if(number_logs > 0) {
                $("<button class='btn-previous'>&laquo;</button>").insertBefore('.pagination');
                $("<button class='btn-next'>&raquo;</button>").insertAfter('.pagination');
            }

            for (var p=1; p<=pages; p++) {
                if(p == 1) {
                    $("div.pagination").append("<button class='active'>"+p+"</button>");
                } else {
                    $("div.pagination").append("<button>"+p+"</button>");
                }
            }

            $('.page-number').html('page '+1+' of '+pages);

            $('button.btn-previous').hover().css('cursor', 'not-allowed');
            $('button.btn-previous').prop('disabled', true);

            if(pages <= 5) {

                $('button.btn-next').hover().css('cursor', 'not-allowed');
                $('button.btn-next').prop('disabled', true);
            }

            if(pages > 5) {

                for(let n=6; n<=pages; n++) {

                    $("div.pagination button:nth-child("+n+")").css('display', 'none');
                }
            }

            $('button.btn-next').click(function () {

                group_number = group_number + 1;

                if(group_number > 1) {

                    $('button.btn-previous').hover().css('cursor', 'pointer');
                    $('button.btn-previous').prop('disabled', false);
                }
                if((pages % 5 == 0 && group_number >= pages/5) || (pages % 5 > 0 && group_number > pages/5)) {

                    $('button.btn-next').hover().css('cursor', 'not-allowed');
                    $('button.btn-next').prop('disabled', true);
                }

                for(let n=1; n<=pages; n++) {
                    if(n >= ((group_number-1) * 5 + 1) && n <= (group_number*5)) {
                        $("div.pagination button:nth-child("+n+")").css('display', 'block');
                    } else {
                        $("div.pagination button:nth-child("+n+")").css('display', 'none');
                    }
                }
                $("div.pagination").find(".active").removeClass("active");
                $("div.pagination button:nth-child("+((group_number-1)*5+1)+")").addClass("active");

                for (let i = 1; i <= number_logs; i++) {

                    $("tbody tr:nth-child(" + i + ")").css('display', 'none');
                }

                for (let j = number_logs_per_page * ((group_number-1)*5) + 1; j <= number_logs_per_page * ((group_number-1)*5+1); j++) {
                    $("tbody tr:nth-child(" + j + ")").css('display', 'table-row');
                }

                $('.page-number').html('page ' + ((group_number-1)*5+1) + ' of ' + pages);
            });

            $('button.btn-previous').click(function () {

                group_number = group_number - 1;

                if(group_number == 1) {

                    $('button.btn-previous').hover().css('cursor', 'not-allowed');
                    $('button.btn-previous').prop('disabled', true);
                }

                if((pages % 5 == 0 && group_number < pages/5) || (pages % 5 > 0 && group_number <= pages/5)) {

                    $('button.btn-next').hover().css('cursor', 'pointer');
                    $('button.btn-next').prop('disabled', false);
                }

                for(let n=1; n<=pages; n++) {
                    if(n >= ((group_number-1) * 5 + 1) && n <= (group_number*5)) {
                        $("div.pagination button:nth-child("+n+")").css('display', 'block');
                    } else {
                        $("div.pagination button:nth-child("+n+")").css('display', 'none');
                    }
                }
                $("div.pagination").find(".active").removeClass("active");
                $("div.pagination button:nth-child("+((group_number-1)*5+1)+")").addClass("active");

                for (let i = 1; i <= number_logs; i++) {

                    $("tbody tr:nth-child(" + i + ")").css('display', 'none');
                }

                for (let j = number_logs_per_page * ((group_number-1)*5) + 1; j <= number_logs_per_page * ((group_number-1)*5+1); j++) {
                    $("tbody tr:nth-child(" + j + ")").css('display', 'table-row');
                }

                $('.page-number').html('page ' + ((group_number-1)*5+1) + ' of ' + pages);
            });


            if (number_logs > number_logs_per_page * 1) {

                for (var i = 1; i <= number_logs_per_page * 1; i++) {

                    $("tbody tr:nth-child(" + i + ")").css('display', 'table-row');
                }
            } else {

                for (var i = 1; i <= number_logs; i++) {

                    $("tbody tr:nth-child(" + i + ")").css('display', 'table-row');
                }
            }

            var page = 1;


            $(".pagination button").click(function () {

                page = parseInt($(this).text());
                if(page == pages) {

                    console.log('click last page');
                } else {

                    $("#btn-load").attr("disabled", false);
                }

                console.log("current page = "+page);

                $("div.pagination").find(".active").removeClass("active");
                $("div.pagination button:nth-child("+page+")").addClass("active");

                for (var i = 1; i <= number_logs; i++) {

                    $("tbody tr:nth-child(" + i + ")").css('display', 'none');
                }

                if (number_logs > number_logs_per_page * page) {

                    for (var i = 1 + number_logs_per_page * (page - 1); i <= number_logs_per_page * page; i++) {

                        $("tbody tr:nth-child(" + i + ")").css('display', 'table-row');
                    }

                } else {

                    for (var i = 1 + number_logs_per_page * (page - 1); i <= number_logs; i++) {

                        $("tbody tr:nth-child(" + i + ")").css('display', 'table-row');
                    }

                }
                $('.page-number').html('page ' + page + ' of ' + pages);

            });
        }

        pagination(5);

        // $('#username-th').click(function () {
        //     $('.filter-table-form-username').submit();
        // });
        //
        // $('#datetime-th').click(function () {
        //     $('.filter-table-form-datetime').submit();
        // });
        $('.view-number').change(function () {
            pagination($(this).val());
            $(".pagination button:nth-child(1)").click();
        })

        $('.btn-request').click(function () {

            let data = $(this).parent().parent();

            $('#date').text(data.find('td:eq(0)').html());
            $('input[name="date"]').val(data.find('td:eq(0)').html());
            $('#chat-id').text('#'+data.find('td:eq(10)').html());
            $('input[name="chat-id"]').val('#'+data.find('td:eq(10)').html());
            $('#first-name').text(data.find('td:eq(9)').html());
            $('input[name="first-name"]').val(data.find('td:eq(9)').html());
            $('#username').text(data.find('td:eq(1)').html());
            $('input[name="username"]').val(data.find('td:eq(1)').html());
            $.ajax({
                'type': "POST",
                'url': "/chat/main/get_chat_transcript",
                'data': { chat_id: data.find('td:eq(10)').html()},
                success: function (response) {

                    let obj = JSON.parse(response);
                    let chats = obj['data'];
                    let comments = '';
                    for(let i=0; i<chats.length; i++) {
                        comments += '<p style="color: mediumblue">'+ chats[i]["username"]+ ' : ' + chats[i]["message"] + '</p>'
                    }
                    // $('#comments').append(comments);
                    $('input[name="comments"]').val(comments);
                }
            });
        })
    });
</script>

