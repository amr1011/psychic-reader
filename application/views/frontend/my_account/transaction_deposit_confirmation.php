<!-- new layout -->
<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Your Purchase Was Successful!</h4>

        <div class="card-description">

            <div class="material-datatables">
                <?php
                    if($this->session->flashdata('success'))
                    {
                    echo "<div class='alert alert-success page-notifs'><strong>Success:</strong><p>".$this->session->flashdata('success')."</p></div>";
                    }
                    ?>


                        <?php echo $this->session->flashdata('response'); ?>

                    <?php if ($package['type'] == 'reading'): ?>
                        <div class="text-success">Your purchase was a success! The chat time you purchased has been credited to your account and you can being using it immediately! </div>
                        <div class="text-success">   <b>Thank you for using Psychic-Contact!</b></div>
                    <?php else: ?>
                        <div class="text-success">Your purchase was a success! Your email balance has been updated and you can begin your email reading immediately!</div>
                        <div class="text-success">   <b>Thank you for using Psychic-Contact!</b></div>
                    <?php endif; ?>

                        <div class="well">
                            <table class="table" width="100%">
                            <tr>
                                <td width="150"><b>Package:</b></td>
                                <td><?= $package['title'] ?></td>
                            </tr>

                            <tr>
                                <td width="150"><b>Transaction ID:</b></td>
                                <td><?= $transaction['transaction_id'] ?></td>
                            </tr>

                            <tr>
                                <td width="150"><b>Amount:</b></td>
                                <td>$<?= number_format($transaction['amount'], 2) ?></td>
                            </tr>

                        </table>
                    </div>

                    <div align="center">
                        <a href="/my_account/psychic_readers" class="btn btn-warning btn-large">Find A Reader Now!</a>
                    </div>


            </div>
        </div>
    </div>
</div>
