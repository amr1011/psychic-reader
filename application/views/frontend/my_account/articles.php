<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Article List</h4>

        <div class="card-description">
            <div class="material-datatables">
                <div class="row">

                        <?php if (count($articles) > 0): ?>

                            <div class="row">
                                    <select class="view-number form-control" >
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                    </select>
                                <div class="pagination"> </div>
                                <a class="btn btn-primary btn-sm" href="/my_account/articles/submit">Add New </a>
                            </div>


                                <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                        <th>Title</th>
                                        <th>Image</th>
                                        <th>Reject reason</th>
                                        <th>Approve</th>
                                        <th>Action</th>
                                    </thead>
                                    <?php foreach($articles as $b): ?>

                                        <tr style="display: none;">
                                            <td><?= $b['title']?></td>

                                            <td>
                                                <?php if($b['filename'] != null) : ?><img src="/media/articles/<?= $b['filename']?>" style="width: 100px;"/>
                                                <?php else : ?><img src="/media/assets/no-image-available.jpg" style="width: 100px;"/>
                                                <?php endif;?>
                                            </td>
                                            <td><?=html_entity_decode(nl2br($b['reject_reason'])); ?></td>
                                            <td><?php if ($b['approved']== 0): echo"pending"; elseif ($b['approved'] == 1): echo"approved";elseif ($b['approved'] == 2): echo"rejected";endif;?></td>
                                            <td style="text-align:center;"><a href="/my_account/articles/submit/<?= $b['id']?>" class="btn-sm btn-primary"><i class="fa fa-pencil"></i></a></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                                
                                <div class="page-number"></div>
                        <?php else: ?>
                            <p>You have no clients.</p>
                        <?php endif; ?>
                        
                        
                </div>
            </div>    
        </div>
    </div>
</div>


