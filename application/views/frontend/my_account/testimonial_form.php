<style>
    .rating { float: left; }
    .rating span { float: right; position: relative;}
    .rating span input { position: absolute;top: 0px;left: 0px;opacity: 0; }
    .rating span label { display: inline-block;width: 30px;height: 30px;text-align: center;color: #FFF;background: #ccc;font-size: 30px;margin-right: 2px;line-height: 30px;border-radius: 50%;-webkit-border-radius: 50%;
    }
    .rating span:hover ~ span label,
    .rating span:hover label,
    .rating span.checked label,
    .rating span.checked ~ span label { background: #F90; color: #FFF;}
</style>

<!-- new layout -->
<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Submit Testimonial</h4>

        <div class="card-description">

                <?php if($this->session->flashdata('testimonial_content_required')):?>
                    <p class='record text-warning' style="display: none;"> <?=$this->session->flashdata('testimonial_content_required')?> </p>
                <?php endif?>

                <?php echo validation_errors(); ?>

            <div class="material-datatables"> 
                  <form action='/my_account/main/submit_testimonial' method='POST' id="testimonial-form"
                          enctype='multipart/form-data'>
                        <div class="col-md-10 col-sm-offset-1">

                            <div style='margin:15px 0 0;'>
                                <div><b>Reader:</b></div>
                                <div style='margin:5px 0 0;'>
                                    <select class="selectpicker" data-style="btn btn-primary btn-round" name="reader-id" required>
                                        <option>Choose a reader</option>
                                        <?php foreach ($readers as $reader): ?>
                                            <option value="<?= $reader['id'] ?>" <?php if($reader['id'] == $reader_selected) : echo("selected"); else : echo(""); endif; ?>><?= $reader['username'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>

                                <div><b>Enter An Article Content:</b></div>
                                <div style='margin:5px 0 0;'>
                                    <textarea name="content" class='form-control'
                                              style='width:100%;height:150px;'></textarea>
                                </div>
                                <br/>
                                <div><b>Rating</b></div>
                                <div class="rating">
                                    <div id='rating' data-rating=""></div>
                                </div>
                            </div>
                            <br/>
                            <div style='margin:35px 0 0;'>
                                <a href="/my_account/main/index" class="btn btn-warning">Cancel</a>
                                <input type='submit' name='submit' value='Submit' class='btn btn-primary'/>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>    




<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=e465y2qbjbdi14qglxeq486et6jfhc6d0zy21wr3akkqps42"></script>

<script>
    tinymce.init({
        selector: 'textarea',
        height: 350,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor link image code",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste imagetools wordcount", 'image code textcolor colorpicker',
        ],
        browser_spellcheck: true,
        image_dimensions: false,
        image_description: false,
        relative_urls: false,
        remove_script_host: false,
        convert_urls: true,
        toolbar: "insertfile | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor",
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ],
        automatic_uploads: true,
        imagetools_cors_hosts: ['mydomain.com', 'otherdomain.com'], paste_data_images: true,

        file_picker_callback: function (cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');

            input.onchange = function () {
                var file = this.files[0];

                var reader = new FileReader();
                reader.onload = function () {

                    var id = 'blobid' + (new Date()).getTime();
                    var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                    var base64 = reader.result.split(',')[1];
                    var blobInfo = blobCache.create(id, file, base64);
                    blobCache.add(blobInfo);
                    cb(blobInfo.blobUri(), {title: file.name});
                };
                reader.readAsDataURL(file);
            };

            input.click();
        }
    });
   
</script>