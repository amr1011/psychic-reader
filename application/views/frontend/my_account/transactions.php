<!-- new layout -->
<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Billing & Transaction History</h4>

        <div class="card-description">

            <?php if($this->session->flashdata('response')):?>
                <p class='record text-warning' > <?=$this->session->flashdata('response')?> </p>
            <?php endif?>

            <div class="material-datatables">

                 <?php if ($transactions) : ?>

                        <div class='table-responsive'><table width='100%' class='table table-striped table-hover' cellPadding='5' cellSpacing='0'>

                                <thead>
                                    <tr>
                                        <td>Date</td>
                                        <td width='100'>Type</td>
                                        <td>Summary</td>
                                        <td>Amount</td>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php foreach ($transactions as $t) : ?>

                                        <?php
                                        switch ($t['type'])
                                        {

                                            case "earning":
                                                $amountString = "$" . number_format($t['amount'], 2);
                                                $label = "Earning";
                                                break;

                                            case "payment":
                                                $amountString = "$" . number_format($t['amount'], 2);
                                                $label = "Payment";
                                                break;

                                            case "purchase":
                                                $amountString = "$" . number_format($t['amount'], 2);
                                                $label = "Purchase";
                                                break;

                                            case "consume":
                                                $amountString = "$" . number_format($t['amount'], 2);
                                                $label = "Chat Time Used";
                                                $t['summary'] = "Used chat time or email credits";
                                                break;

                                            case "refund":
                                                $amountString = "$" . number_format($t['amount'] * -1, 2);
                                                $label = "Refunded";
                                                break;
                                        }
                                        ?>


                                        <tr>
                                            <td width='100'><?php echo date("m/d/Y", strtotime($t['datetime'])); ?></td>
                                            <td width='75'><?php echo $label; ?></td>
                                            <td><?php echo $t['summary']; ?></td>
                                            <td width='100'><?php echo $amountString; ?></td>
                                        </tr>
                                    <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>
                    <?php else : ?>
                        <p>There are no transactions</p>
                    <?php endif; ?>

            </div>
        </div>
    </div>
</div>     


