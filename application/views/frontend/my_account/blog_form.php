

<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Submit A Blog</h4>

        <div class="card-description">
            <div class="material-datatables">
                <div class="row">

                <?php echo validation_errors(); ?>

                    <form action='/my_account/main/blog_submit' method='POST' enctype='multipart/form-data' id="blog-form">
                        <div class="col-md-10 col-sm-offset-1"
                            <p>Use the form below to upload your blog. An administrator will review all blog and post them accordingly.</p>

                            <hr />

                            <div style='margin:15px 0 0;'>
                                <div><b>Enter An Article Title:</b></div>
                                <input type="text" name="title" class="form-control" value="<?php if($blog!=null): echo $blog['title']; endif;?>" placeholder="your blog title" required/><br/>
                                <div><b>Enter An Article Content:</b></div>
                                <div style='margin:5px 0 0;'>
                                    <textarea class='form-control' name="content" form="blog-form" style='width:100%;height:150px;'><?php if($blog!=null): echo $blog['content']; endif;?></textarea>
                                </div>
                            </div>

                            <div style='margin:15px 0 0;'><b>Select Blog Image From Your Computer:</b></div>
                            <div style='margin:5px 0 0;'>
                                <input type='file' id="imgInp" name='blog_image' style="max-width:200px;" accept=".png,.jpg"/>
                                <img id="blah" src="<?php if($blog!=null): echo '/media/assets/'.$blog['image']; endif;?>" alt="your image" />
                            </div>

                    <!--                            <div style='margin:15px 0 0;'><b>Select Blog Doc From Your Computer:</b></div>-->
                    <!--                            <div style='margin:5px 0 0;'>-->
                    <!--                                <input type='file' id="docInp" name='blog_doc' style="max-width:200px;"/>-->
                    <!--                            </div>-->

                            <input type="hidden" value="<?php if($blog != null): echo $blog['id']; endif;?>" name="blog_id"/>
                            <hr />
                            <div><b>Category:</b></div>
                            <div>
                                <div><input type='radio' name='categories' value='ASTROLOGY + TAROT + DIVINATION' <?php if('ASTROLOGY + TAROT + DIVINATION' == $blog['category']) : echo "checked"; endif;?>/> <span class='category-title'>ASTROLOGY + TAROT + DIVINATION</span></div>
                                <div><input type='radio' name='categories' value='FAMILY AND FRIENDS' <?php if('FAMILY AND FRIENDS' == $blog['category']): echo "checked"; endif;?>/> <span class='category-title'>FAMILY AND FRIENDS</span></div>
                                <div><input type='radio' name='categories' value='HEALING' <?php if('HEALING' == $blog['category']): echo "checked"; endif;?>/> <span class='category-title'>HEALING</span></div>
                                <div><input type='radio' name='categories' value='LIFE COACHING' <?php if('LIFE COACHING' == $blog['category']): echo "checked"; endif;?>/> <span class='category-title'>LIFE COACHING</span></div>
                                <div><input type='radio' name='categories' value='LOVE AND RELATIONSHIP' <?php if('LOVE AND RELATIONSHIP' == $blog['category']): echo "checked"; endif;?>/> <span class='category-title'>LOVE AND RELATIONSHIP</span></div>
                                <div><input type='radio' name='categories' value='METAPHYSICAL TECHNIQUES & PRACTICES' <?php if('METAPHYSICAL TECHNIQUES & PRACTICES' == $blog['category']): echo "checked"; endif;?>/> <span class='category-title'>METAPHYSICAL TECHNIQUES & PRACTICES</span></div>
                            </div>
                            <hr/>
                            <div style='margin:35px 0 0;'>
                                <a href="/my_account/main/blog_management" class="btn btn-warning">Cancel</a>
                                <input type='submit' name='submit' value='Submit' class='btn btn-primary' />
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>            
</div>
