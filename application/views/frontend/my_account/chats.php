<!-- new layout -->
<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Chat History</h4>
        <div class="card-description">

            <div class="material-datatables">

                <?php if($this->session->flashdata('response_delete')):?>
                    <p class='record-delete' style="display: none;"> <?=$this->session->flashdata('response_delete')?> </p>
                <?php endif?>

                <p><?php echo validation_errors(); ?></p>
                <form class="filter-table-form-username" action='/my_account/chats/filter_chats_username/' method='POST' style="display:none;">
                    <input name="username_direction" value="<?= $username_direction?>"/>
                </form>

                <form class="filter-table-form-datetime" action='/my_account/chats/filter_chats_datetime/' method='POST' style="display: none;">
                    <input name="datetime_direction" value="<?= $datetime_direction?>"/>
                </form>


                <?php if($search == 1) {?>
                    <form class="search_form_client_list"action='/my_account/chats/client_chat_search/' method='POST' class='form-inline'>
                        <div class="form-group">
                        <input type='text' class='form-control' name='query' value='<?= $this->input->post('query') ?>' placeholder='Search with Username' class='input-xlarge' required/>
                        <input type='submit' value='Search' class='btn btn-primary btn-sm btn-round search-client'>
                        <button class="btn btn-success btn-sm  btn-round refresh" >Reset Filter</button>
                        </div>
                    </form>                        
                <?php }?>


                <?php if ($chats) : ?>


                    <div class="content-pagination">
                        <div class="view-number-content">
                            <select class="view-number form-control" >
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                            </select>
                        </div>
                        <div class="pagination"></div>
                    </div>

                        <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%" >
                            <thead>
                                <?php if($this->member->data['member_type'] == 'client') {?>
                                    <th>Feedback</th>
                                <?php }?>
                                <th id="datetime-th">Date<?php if($datetime_direction == 'ASC') : ?><i class="fa fa-sort-asc" style="margin-left: 5px; font-size: 12px;"></i><?php else:?><i class="    fa fa-sort-desc" style="margin-left: 5px; font-size: 12px;"></i><?php endif; ?></th>
                                <th id="username-th">Username<?php if($username_direction == 'ASC') : ?><i class="fa fa-sort-asc" style="margin-left: 5px; font-size: 12px;"></i><?php else:?><i class="    fa fa-sort-desc" style="margin-left: 5px; font-size: 12px;"></i><?php endif; ?></th>
                
                                <th>Subject</th>
                                <th>Duration</th>
                                <th>Paid Time</th>
                                <th>Free Time</th>
                                <?php if($this->member->data['member_type'] == 'reader') {?>
                                    <th>Due to Reader</th>
                                <?php }?>
                                <th>Details</th>
                                <?php if($this->member->data['member_type'] == 'client') {?>
                                    <th>NRR</th>
                                <?php }?>
                                <?php //if($this->member->data['member_type'] == 'client') {?>
                                    <th>Delete</th>
                                <?php //}?>
                            </thead>
                            <?php foreach ($chats as $c) : ?>
                                <?php
                                if ($this->member->data['id'] == $c['client_id'])
                                {
                                    //--- Get reader
                                    $user = $this->system_vars->get_member($c['reader_id']);
                                } else
                                {
                                    //--- Get client
                                    $user = $this->system_vars->get_member($c['client_id']);
                                }
                                
                                $fname = $this->member->data['first_name'];
                                $email = $this->member->data['email'];
                                $country = $this->member->data['country'];
                                $time = date("m/d/Y @ h:i A", strtotime($c['start_datetime']));

                                if ($this->member->data['member_type'] == "reader")
                                {
                                    $type = 1;
                                    $murl = "http://www.psychic-contact.com/modules/chattestingform/reader_form.php?un=" . $this->member->data['username'] . "&cn=" . $user['username'] . "&fn=$fname&email=$email&country=$country&time=$time&type=$type";
                                    $link_name = "Submit";
                                } else
                                {
                                    $type = 0;
                                    $murl = "http://www.psychic-contact.com/modules/chattestingform/client_form.php?un=" . $this->member->data['username'] . "&rn=" . $user['username'] . "&fn=$fname&email=$email&country=$country&time=$time&type=$type";
                                    $link_name = "Submit";
                                }
                                $t_url = "<br><a class='text-rose' href=\"$murl\" target=\"_blank\">[ $link_name Feedback ]</a>";
                                ?>
                                <?php if( $c['is_client_deleted'] != 1 ) {?>
                                    <tr>
                                        <?php if($this->member->data['member_type'] == 'client') {?>
                                            <td ><?php echo $c['id'] . " $t_url "; ?></td>
                                        <?php }?>
                                        <td width='175' style='vertical-align:middle;'><?php echo date("m/d/Y @ h:i A", strtotime($c['start_datetime'])); ?></td>
                                        <td><?php echo $user['username'] ?></td>
                      
                                        <td><?php echo ($c['topic'])?></td>
                                        <td><?php echo (($c['length']+$c['free_length']) - ($c['length']+$c['free_length'])%60)/60 . 'mins and ' . ($c['length']+$c['free_length'])%60 . 'secs'?></td>
                                        <td><?php echo ($c['length'] - $c['length']%60)/60 . 'mins and ' . $c['length']%60 . 'secs'?></td>
                                        <td><?php echo ($c['free_length'] - $c['free_length']%60)/60 . 'mins and ' . $c['free_length']%60 . 'secs'?></td>
                                        <?php if($this->member->data['member_type'] == 'reader') {?>
                                            <td><?php echo($this->member->data['username'])?></td>
                                        <?php }?>
                                        <td ><a href='/my_account/chats/transcript/<?php echo $c["id"]?>' class='btn btn-sm btn-rose'>Details</a>

                                            <?php if($this->member->data['member_type'] == 'reader') {?>
                                                <a href="/my_account/chats/time_back/<?php echo $c['id']?>">TimeBack</a>
                                            <?php } ?>

                                        </td>
                                        
                                        <td>
                                        <?php if($this->member->data['member_type'] == 'client') {?>
                                        <a class="btn btn-sm btn-primary <?php if((strtotime("now") - strtotime($c['create_datetime']))/3600 > 72) : echo('isDisabled'); endif;?>" href="/my_account/main/nrr/<?= $c['id']?>" "><span>NRR</span> </a>
                                        </td>
                                        <?php }?>
                                        <td>
                                        <!-- <td style="vertical-align: middle"><a href="/my_account/chats/delete_chat/<?= $c['id']?>" onClick="Javascript:return confirm('Are you sure you want to delete chat #<?= $c["id"]?>?');">Delete</a> -->

                                        <?php if($this->member->data['member_type'] == 'reader') {
                                            $today = date("Y-m-d H:i:s");
                                            $date_ = $c['start_datetime'];
                                            $today_diff = strtotime($today) - strtotime($date_);
                                            $date_diff = round($today_diff / (60 * 60 * 24));

                                            if($date_diff > 30){
                                                ?>

                                                <a href="/my_account/chats/soft_delete_chat/<?= $c['id']?>" onClick="Javascript:return confirm('Are you sure you want to delete chat #<?= $c["id"]?>?');">Delete</a>

                                                <?php
                                            }
                                        }
                                        ?>

                                        </td>
                                        
                                    </tr>
                                <?php } ?>
                            <?php endforeach; ?>

                        </table>
                        <div class="page-number" style="float: right; margin-top: 10px;"></div>
                    <?php else : ?>
                        <div>You do not have a chat history.</div>
                    <?php endif; ?>
                </div> 

        </div>
    </div>
</div>
