<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Email Readings</h4>

        <div class="card-description">
            <div class="material-datatables">
                <div class="row">

                <div class="col-md-12 col-sm-7">    
                    <div >To modify your "Default Reading Settings", visit your "Edit My Pyschic Profile" page by <a href='/my_account/main/edit_profile'>clicking here</a>.</div>

                    <ul class="nav">
                        <li class="nav-item" ><a  class="nav-link <?= !$open ? "class='active'" : "" ?>"  href="/my_account/email_readings/closed_requests">Closed Email Requests</a></li>
                        <li class="nav-item"><a  class="nav-link <?= !$open ? "class='active'" : "" ?>" href="/my_account/email_readings/email_specials">My Email Specials</a></li>
                    </ul>

                    <?php
                    if ($emails)
                    {
                        
                        echo "<table class='table table-striped table-no-bordered table-hover' cellspacing='0' width='100%' style='width:100%'>
                        <thead>
                            <tr><th>Package Ordered / Date:</th><th>&nbsp;</th></tr>
			            </thead>
			            <tbody>";

                        foreach ($emails as $e) {

                            echo "<tr>
					            <td style='vertical-align:middle;'><div><b>{$e['package_title']}</b></div>" . date("m/d/Y @ h:i A", strtotime($e['datetime'])) . "</td>
					            <td style='vertical-align:middle;text-align:right;width:60px;'><a href='/my_account/email_readings/reader_view/{$e['id']}' class='btn'>View</a></td>
				                </tr>";
                        }
                        echo "</tbody></table>";
                    } else
                    {

                        echo "<div>" . ($open ? "You do not have any open email requests" : "You do not have any closed email requests") . "</div>";
                    }
                    ?>



                </div>                
            </div>                
        </div>
    </div>
</div>






