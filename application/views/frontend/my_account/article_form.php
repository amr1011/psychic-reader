<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Submit An Article</h4>

        <div class="card-description">
            <div class="material-datatables">
                <div class="row">

                <?php echo validation_errors(); ?>

                        <form action='/my_account/articles/submit_post' method='POST' enctype='multipart/form-data'>
                            <div class="col-md-10 col-sm-offset-1"
                                <p>Use the form below to upload your article. An administrator will review all articles and post them accordingly.</p>

                                <hr />
                                <input type="hidden" value="<?php if($article != null): echo $article['id']; endif;?>" name="article_id"/>
                                <div style='margin:15px 0 0;'>
                                    <div><b>Enter An Article Title:</b></div>
                                    <div style='margin:5px 0 0;'>
                                        <input type="text" name='title' class='form-control' value="<?php if($article!=null): echo $article['title']; endif;?>">
                                    </div>
                                    <div><b>Category:</b></div>
                                    <div style='margin:5px 0 0;'>
                                        <select class="form-control" name="category-id">
                                            <option>Choose a category</option>
                                            <?php foreach($categories as $category):?>
                                            <option value="<?= $category['id']?>" <?php if($category['id'] == $article['category_id']): echo('selected'); endif;?>><?= $category['title']?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                    <div><b>Enter An Article Url:</b></div>
                                    <div style='margin:5px 0 0;'>
                                        <input type="text" name='url' class='form-control' value="<?php if($article!=null): echo $article['url']; endif;?>">
                                    </div>

                                    <div><b>Enter An Article Content:</b></div>
                                    <div style='margin:5px 0 0;'>
                                        <textarea name="content" class='form-control' style='width:100%;height:150px;' ><?php if($article!=null): echo $article['content']; endif;?></textarea>
                                    </div>
                                </div>

                                <div style='margin:15px 0 0;'><b>Select An Article From Your Computer:</b></div>
                                <div class='caption'>(Accepted formats: jpg, png)</div>

                                <div style='margin:5px 0 0;'>
                                    <input type='file' name='file' style="max-width:200px;" id="imgInp"/>
                                    <img id="blah" src="<?php if($article!=null): echo '/media/articles/'.$article['filename']; endif;?>" alt="your image" style="width: 100px;"/>
                                </div>

                                <hr />
                                <h3>Search Engine Optimization</h3>
                                <div style='margin:5px 0 0;'>
                                    <label for="seo-keyword" style="width: 150px;">SEO Keyword</label><input type='text' name='seo-keyword' class="form-control" value="<?php if($article!=null): echo $article['seo_keyword']; endif;?>"/>
                                </div>
                                <div style="margin: 5px 0 0;">
                                    <label for="seo-description" style="width: 150px;">SEO Description</label><input type='text' name='seo-description' class="form-control"  value="<?php if($article!=null): echo $article['seo_description']; endif;?>"/>
                                </div>
                                <div style='margin:35px 0 0;'>
                                    <a href="/my_account/articles/index" class="btn btn-warning">Cancel</a>
                                    <input type='submit' name='submit' value='Upload My Article' class='btn btn-primary'/>
                                </div>
                            </div>
                        </form>                



                </div>
            </div>
        </div>
    </div>
</div>    


<script src="/theme/admin/js/jquery-3.1.1.min.js"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=e465y2qbjbdi14qglxeq486et6jfhc6d0zy21wr3akkqps42"></script>
<script>
    tinymce.init({
        selector: 'textarea',
        height: 350,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor link image code",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste imagetools wordcount", 'image code textcolor colorpicker',
        ],
        browser_spellcheck: true,
        image_dimensions: false,
        image_description: false,
        relative_urls : false,
        remove_script_host : false,
        convert_urls : true,
        toolbar: "insertfile | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor",
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ],
        automatic_uploads: true,
        imagetools_cors_hosts: ['mydomain.com', 'otherdomain.com'],paste_data_images: true,

        file_picker_callback: function(cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');

            input.onchange = function() {
                var file = this.files[0];

                var reader = new FileReader();
                reader.onload = function () {

                    var id = 'blobid' + (new Date()).getTime();
                    var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                    var base64 = reader.result.split(',')[1];
                    var blobInfo = blobCache.create(id, file, base64);
                    blobCache.add(blobInfo);
                    cb(blobInfo.blobUri(), { title: file.name });
                };
                reader.readAsDataURL(file);
            };

            input.click();
        }
    });
    $(document).ready(function () {
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function(){
            readURL(this);
        });
    })
</script>