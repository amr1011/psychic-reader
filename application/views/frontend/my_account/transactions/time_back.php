<div class="row">
    <div class="my-container page-70-30">
        <div class="content-block-30">
            <?php $this->load->view('frontend/my_account/nav/dashboard_menu'); ?>
        </div><!-- .content-block-20 -->

        <div class="content-block-70">
            <div class="row">
                <div class="col-md-12 col-sm-7">    
                    <h2>Time Back</h2>
                    <hr />
                    <?php if (count($clients) > 0): ?>
                        <table class="table table-striped table-bordered" style="margin:15px 0 35px;">
                            <tr>
                                <th>Username</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>&nbsp;</th>
                            </tr>
                            <?php foreach($clients as $c): ?>
                            <tr>
                                <td><?= $c['username'] ?></td>
                                <td><?= $c['first_name'] ?></td>
                                <td><?= $c['last_name'] ?></td>
                                <td style="text-align:center;">
                                    <a class="btn btn-primary" href="/my_account/transactions/time_back/<?= $c['mid'] ?>">Add Time Back</a>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </table>
                    <?php else: ?>
                        <p>You have no clients.</p>
                    <?php endif; ?>
                </div>
            </div>
        </div><!-- .content-block-80 -->
    </div><!-- .my-container -->   
</div>