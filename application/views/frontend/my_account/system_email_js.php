<script>
    function insertMessage(messages) {
        let view = "<div class='table-responsive'><table width='80%' cellPadding='0' cellSpacing='0' class='table table-striped table-hover'>";
        var member = null;
        for(let i=0; i<messages.length; i++) {
            let from;
            let class_name = 'read';
            if(messages[i]['read'] == 0) {
                class_name = 'unread';
            }
            switch(messages[i]["type"]) {
                case "email":
                    from = "System";
                    break;

                case "admin":
                    from = "Administrator";
                    break;

                case "reader":
                    let sender_id = messages[i]['sender_id'];
                    $.ajax({
                        'type': "POST",
                        'url': "/my_account/system_email/getMember",
                        'data': { user_id : sender_id},
                        success: function (data) {
                            let obj = JSON.parse(data);
                            member = obj.member;
                        }
                    });
                    from = member['first_name'] + member['last_name'];
                    break;
            }
            view = view + "<tr class="+class_name+">" + "<td width='150'>" + messages[i]['datetime'] + " EST</td>"
                    + "<td>" + from + "</td>" + "<td>" + messages[i]['subject'] + "</td>"
                    + "<td width='50' align='center'><a href='/my_account/system_email/view/"+ messages[i]['id'] + "' class='btn'>View</a></td>"
                    + "<td width='50' align='center'><a href='/my_account/system_email/delete/" + messages[i]['id'] + "' onClick=\"Javascript:return confirm('Are you sure you want to delete this message?');\" class='btn'>Delete</a></td>"
                    + "</tr>";
        }
        view = view + "</table></ div>";
        return view;
    }

    $(document).ready(function () {

        var messages;
        var notifications;
        var trash_notifications;

        function getData() {
            $.ajax({
                'type': "POST",
                'url': "/my_account/system_email/getMessages",
                'data': { user_id : '<?=$this->member->data['id'];?>'},
                success: function (data) {
                    let obj = JSON.parse(data);
                    messages = obj.messages;
                    notifications = obj.notifications;
                    trash_notifications = obj.notifications_trash;
                    // console.log(notifications);
                    // console.log(messages);
                    // console.log(trash_notifications);
                }
            });
        }

        getData();

        setTimeout(function () {
            for(let i = 1; i <= notifications.length; i++) {
                let email_name = $(".main-folders .notification-folder:nth-child("+i+")").find(".folder-name").text();
                let array = email_name.split("_");
                let email_name_show = "";
                for(let n = 0; n< array.length; n++) {

                    email_name_show = email_name_show + array[n].charAt(0).toUpperCase() + array[n].slice(1) + " ";
                }
                $(".main-folders .notification-folder:nth-child("+i+")").find(".show-folder-name").text(email_name_show);
            }
            $(".main-folders").css("display", "block");
        }, 500);

        $(".notification-folder .enabled").click(function () {

           let folder_status = $(this).parent().find(".folder-status").val();
            console.log(folder_status);
           if(folder_status == 0) {
               $(".fa-folder-open").removeClass("fa-folder-open").addClass("fa-folder");
               $(".folder-status").val(0);
               $(".folder-name").css('color', '#4c4b4b');
               $(this).parent().find(".fa-folder").removeClass("fa-folder").addClass("fa-folder-open");
               $(this).parent().find(".folder-name").css('color', '#337ab7');
               $(this).parent().find(".folder-status").val(1);
               $('.trash-folder').css('color', '#4c4b4b');

               // show contained messages

           } else {
               $(this).parent().find(".fa-folder-open").removeClass("fa-folder-open").addClass("fa-folder");
               $(this).parent().find(".folder-status").val(0);
               $(".folder-name").css('color', '#4c4b4b');
               $('.trash-folder').css('color', '#4c4b4b');
           }
        });

        $(".notification-trash-folder").click(function () {
            $(".notification-folder").find(".fa-folder-open").removeClass("fa-folder-open").addClass("fa-folder");
            $(this).parent().find(".trash-folder").css('color', '#337ab7');
            $('.folder-name').css('color', '#4c4b4b');
            $(".folder-status").val(0);
        });

        $(".inbox-folder").click(function () {
            getData();
            $(".box-message").empty();
            let folder_status = $(this).parent().find(".folder-status").val();
            if(folder_status == 0) {

            } else {

                for(let i=0; i<notifications.length; i++) {
                    for(let j=messages.length-1; j>=0; j--) {
                        if(messages[j]['name'] == notifications[i]['name']) {
                            messages.splice(j, 1);
                        }
                    }
                }
                let view = insertMessage(messages);
                $(".box-message").append(view);
            }
        });
        $(".message-folder").click(function () {

            getData();

            $(".box-message").empty();
            let folder_status = $(this).find(".folder-status").val();
            if(folder_status == 0) {

            } else {
                let folder_name = $(this).find(".email-name").text();
                console.log(folder_name);
                for(let j=messages.length-1; j>=0; j--) {
                    if(messages[j]['name'] != folder_name) {
                        messages.splice(j, 1);
                    }
                }

                let view = insertMessage(messages);
                $(".box-message").append(view);
            }
        });

        $(".notification-trash-folder").click(function () {
           // var trash_notifications = [];
           var trash_messages = [];
           getData();
           $(".box-message").empty();

           // for(let n=notifications.length-1; n>=0; n--) {
           //
           //     if(notifications[n]['email_type'] == 2 || notifications[n]['email_type'] == 4) {
           //         trash_notifications.push(notifications[n]);
           //     }
           // }
           console.log(trash_notifications);
            for(let i=0; i<trash_notifications.length; i++) {
                for(let j=messages.length-1; j>=0; j--) {
                    if(messages[j]['name'] == trash_notifications[i]) {
                        trash_messages.push(messages[j]);
                    }
                }
            }
            let view = insertMessage(trash_messages);
            $(".box-message").append(view);
        });
    });
</script>


