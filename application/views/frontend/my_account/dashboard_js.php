

<script src="/chat/reader_my_account.js?current_status=<?=$this->member->data['status'] ?>&reader_id=<?=$this->member->data['profile_id'] ?>"></script>

<!-- 9-4 -->
<script>
    $(document).ready(function() {

        var appointmentTimer = setInterval(function(){

            $.ajax({

                url: '/chat/main/get_appointment/'+'<?=$this->member->data['id']?>',
                type: 'GET',

            }).done(function(response) {

                var obj = JSON.parse(response);
                var data = obj['appointments'];

                for(var i=0; i<data.length; i++) {

                    var messageText = 'Hi ' + '<?=$this->member->data['username']?>, ' + data[i]['client_firstname'] + ' ' + data[i]['client_lastname'] 
                                    + ' sent an appointment for ' + data[i]['appointment_date'];
              
                    $.notify( messageText,  {className: "info", hideDuration: 700, autoHideDelay: 60000, position: 'right buttom'});
                }
                console.log(obj);

            }).fail(function(error) {
                console.error(error);
            });

        }, 3000);

        var no_reader_check = setInterval(function(){

            $.ajax({

                url: '/main/no_reader_check/'+'<?=$this->member->data['id']?>',
                type: 'GET',

            }).done(function(response) {

                let obj = JSON.parse(response);
                let status = obj.status;

                if(status == true) {
                    $("#no-reader-text").css("display", "block");
                } else {
                    $("#no-reader-text").css("display", "none");
                }

            }).fail(function(error) {
                console.error(error);
            });

        }, 1000);

        /*9-20*/
        var waitInLineTimer = setInterval(function () {

            $.ajax({

                url: '/chat/main/get_wait_in_line_clients/' + '<?php echo $this->member->data['id']?>',
                type: 'GET',

            }).done(function (response) {

                var obj = JSON.parse(response);
                var data = obj['wait_in_line_clients'];

                console.log("wait in line clients ===>" + data);

                var number_wait_in_line_clients = data.length;

                if (number_wait_in_line_clients > 0) {

                    var messageText = 'You have ' + number_wait_in_line_clients + 'clients waiting in line : ';

                    for (var i = 0; i < data.length; i++) {

                        messageText += data[i]['client_user_name'] + '  ';
                    }
                    $.notify(messageText, {
                        className: "success",
                        hideDuration: 700,
                        autoHideDelay: 6000000,
                        position: 'left bottom'
                    });
                    clearInterval(waitInLineTimer);
                }

                console.log(obj);

            }).fail(function (error) {
                console.error(error);
            });
        }, 3000);

    });
</script>