<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">TrancePad</h4>

        <div class="card-description">
            <div class="material-datatables">

            <div class="row">

                    <?php if ($this->member->data['trancepad_enabled'] == '0'): ?>
                        <a href='/my_account/trancepad/toggle/1' class='btn btn-large btn-primary'>Enable TrancePad on My Account</a>
                    <?php else : ?>

                        <a href='/my_account/trancepad/toggle/0' class='btn btn-large btn-danger'>Disable TrancePad</a>
                    <?php endif; ?>

            </div>
        </div>
    </div>
</div>

