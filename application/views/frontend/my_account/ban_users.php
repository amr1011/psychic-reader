
<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Ban Users</h4>

        <div class="card-description">
            <div class="material-datatables">
                <div class="row">

                        <?php echo validation_errors(); ?>

                        <form class="search_form_ban">
                            <label class="col-sm-12 col-form-label">Select client to ban</label>
                            <div class="col-sm-12">
                                <div class="form-group has-success">
                                    <select name='search_type' class='form-control client-list' >
                                        <option selected disabled>Choose a client</option>
                                        <?php foreach($all_clients as $client) : ?>
                                            <option value="<?= $client['client_id']?>" data-foo="<?= $client['username']?>"><?= $client['username']?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                
                            </div>                            
                            <div class="form-group">
                                <div class="col-md-12 col-sm-4">
                                    
                                </div>
                            </div>
                        </form>


                        <div class='table-responsive' style='overflow-x:inherit;'>
                            <table class='table table-striped table-hover'>
                            <?php if ($clients) : ?>
                                <?php foreach ($clients as $c) : ?>
                                    <?php if($c["type"] != "full") : ?>
                                        <tr>
                                            <td width='175' style='vertical-align:middle;'><?php echo $c['username']; ?></td>
                                            <td style='vertical-align:middle;'><?php echo ($c['date'] ? "Ban Date: " . date("m/d/Y @ h:i A", strtotime($c['date'])) : "") ?></td>
                                            <td style='vertical-align:middle;'>
                                            </td>
                                            <td style='width:150px; vertical-align:middle; text-align:right'>
                                                <?php
                                                if ($c['type'] == "full") : ?>
                                                    <a href='#' class='btn btn-danger disabled'>Full Banned</a>
                                                <?php elseif($c['type'] == "personal") : ?>
                                                    <a onClick='Javascript:return confirm("Are you sure you want to unban member?");'  href='/my_account/ban_users/unban/<?=$c["id"]?>' class='btn'>Unban User</a>
                                                <?php else : ?>
                                                    <a onClick='Javascript:return confirm("Are you sure you want to ban member?");' href='/my_account/ban_users/ban/<?=$c["mid"]?>' class='btn btn-danger'>Ban User</a>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </table></div>
                    <?php if (!$clients) : ?>

                        <?php ($this->input->post("query") ? "No results for \"" . $this->input->post("query") . "\"" : "<div>You have not banned any users.</div>"); ?>

                    <?php endif; ?>                


                </div>
            </div>                    
        </div>
    </div>
</div>


