<script>
    $(document).ready(function () {

        $('.refresh').click(function () {

            location.href = window.location.protocol + "//" + window.location.host + "/" + "my_account/chats";
        });

        var pagination = function(per_page) {

            var number_logs = $("tbody tr").length;
            var number_logs_per_page = per_page;
            var pages = number_logs/number_logs_per_page;
            var page_module = number_logs % number_logs_per_page;
            var group_number = 1;

            pages = Math.trunc(pages);
            $('div.pagination').html('');
            if($('button.btn-previous')) {
                $('button.btn-previous').remove();
            }
            if($('button.btn-next')) {
                $('button.btn-next').remove();
            }
            if(page_module > 0) {
                pages = pages + 1;
            }
            if(number_logs > 0) {
                $("<button class='btn-previous'>&laquo;</button>").insertBefore('.pagination');
                $("<button class='btn-next'>&raquo;</button>").insertAfter('.pagination');
            }

            for (var p=1; p<=pages; p++) {
                if(p == 1) {
                    $("div.pagination").append("<button class='active'>"+p+"</button>");
                } else {
                    $("div.pagination").append("<button>"+p+"</button>");
                }
            }

            $('.page-number').html('page '+1+' of '+pages);

            $('button.btn-previous').hover().css('cursor', 'not-allowed');
            $('button.btn-previous').prop('disabled', true);

            if(pages <= 5) {

                $('button.btn-next').hover().css('cursor', 'not-allowed');
                $('button.btn-next').prop('disabled', true);
            }

            if(pages > 5) {

                for(let n=6; n<=pages; n++) {

                    $("div.pagination button:nth-child("+n+")").css('display', 'none');
                }
            }

            $('button.btn-next').click(function () {

                group_number = group_number + 1;

                if(group_number > 1) {

                    $('button.btn-previous').hover().css('cursor', 'pointer');
                    $('button.btn-previous').prop('disabled', false);
                }
                if((pages % 5 == 0 && group_number >= pages/5) || (pages % 5 > 0 && group_number > pages/5)) {

                    $('button.btn-next').hover().css('cursor', 'not-allowed');
                    $('button.btn-next').prop('disabled', true);
                }

                for(let n=1; n<=pages; n++) {
                    if(n >= ((group_number-1) * 5 + 1) && n <= (group_number*5)) {
                        $("div.pagination button:nth-child("+n+")").css('display', 'block');
                    } else {
                        $("div.pagination button:nth-child("+n+")").css('display', 'none');
                    }
                }
                $("div.pagination").find(".active").removeClass("active");
                $("div.pagination button:nth-child("+((group_number-1)*5+1)+")").addClass("active");

                for (let i = 1; i <= number_logs; i++) {

                    $("tbody tr:nth-child(" + i + ")").css('display', 'none');
                }

                for (let j = number_logs_per_page * ((group_number-1)*5) + 1; j <= number_logs_per_page * ((group_number-1)*5+1); j++) {
                    $("tbody tr:nth-child(" + j + ")").css('display', 'table-row');
                }

                $('.page-number').html('page ' + ((group_number-1)*5+1) + ' of ' + pages);
            });

            $('button.btn-previous').click(function () {

                group_number = group_number - 1;

                if(group_number == 1) {

                    $('button.btn-previous').hover().css('cursor', 'not-allowed');
                    $('button.btn-previous').prop('disabled', true);
                }

                if((pages % 5 == 0 && group_number < pages/5) || (pages % 5 > 0 && group_number <= pages/5)) {

                    $('button.btn-next').hover().css('cursor', 'pointer');
                    $('button.btn-next').prop('disabled', false);
                }

                for(let n=1; n<=pages; n++) {
                    if(n >= ((group_number-1) * 5 + 1) && n <= (group_number*5)) {
                        $("div.pagination button:nth-child("+n+")").css('display', 'block');
                    } else {
                        $("div.pagination button:nth-child("+n+")").css('display', 'none');
                    }
                }
                $("div.pagination").find(".active").removeClass("active");
                $("div.pagination button:nth-child("+((group_number-1)*5+1)+")").addClass("active");

                for (let i = 1; i <= number_logs; i++) {

                    $("tbody tr:nth-child(" + i + ")").css('display', 'none');
                }

                for (let j = number_logs_per_page * ((group_number-1)*5) + 1; j <= number_logs_per_page * ((group_number-1)*5+1); j++) {
                    $("tbody tr:nth-child(" + j + ")").css('display', 'table-row');
                }

                $('.page-number').html('page ' + ((group_number-1)*5+1) + ' of ' + pages);
            });


            if (number_logs > number_logs_per_page * 1) {

                for (var i = 1; i <= number_logs_per_page * 1; i++) {

                    $("tbody tr:nth-child(" + i + ")").css('display', 'table-row');
                }
            } else {

                for (var i = 1; i <= number_logs; i++) {

                    $("tbody tr:nth-child(" + i + ")").css('display', 'table-row');
                }
            }

            var page = 1;


            $(".pagination button").click(function () {

                page = parseInt($(this).text());
                if(page == pages) {

                    console.log('click last page');
                } else {

                    $("#btn-load").attr("disabled", false);
                }

                console.log("current page = "+page);

                $("div.pagination").find(".active").removeClass("active");
                $("div.pagination button:nth-child("+page+")").addClass("active");

                for (var i = 1; i <= number_logs; i++) {

                    $("tbody tr:nth-child(" + i + ")").css('display', 'none');
                }

                if (number_logs > number_logs_per_page * page) {

                    for (var i = 1 + number_logs_per_page * (page - 1); i <= number_logs_per_page * page; i++) {

                        $("tbody tr:nth-child(" + i + ")").css('display', 'table-row');
                    }

                } else {

                    for (var i = 1 + number_logs_per_page * (page - 1); i <= number_logs; i++) {

                        $("tbody tr:nth-child(" + i + ")").css('display', 'table-row');
                    }

                }
                $('.page-number').html('page ' + page + ' of ' + pages);

            });
        }

        pagination(5);

        // $('#username-th').click(function () {
        //     $('.filter-table-form-username').submit();
        // });
        //
        // $('#datetime-th').click(function () {
        //     $('.filter-table-form-datetime').submit();
        // });
        $('.view-number').change(function () {
            pagination($(this).val());
            $(".pagination button:nth-child(1)").click();
        })

        $('.btn-request').click(function () {

            let data = $(this).parent().parent();

            $('#date').text(data.find('td:eq(0)').html());
            $('input[name="date"]').val(data.find('td:eq(0)').html());
            $('#chat-id').text('#'+data.find('td:eq(10)').html());
            $('input[name="chat-id"]').val('#'+data.find('td:eq(10)').html());
            $('#first-name').text(data.find('td:eq(9)').html());
            $('input[name="first-name"]').val(data.find('td:eq(9)').html());
            $('#username').text(data.find('td:eq(1)').html());
            $('input[name="username"]').val(data.find('td:eq(1)').html());
            $.ajax({
                'type': "POST",
                'url': "/chat/main/get_chat_transcript",
                'data': { chat_id: data.find('td:eq(10)').html()},
                success: function (response) {

                    let obj = JSON.parse(response);
                    let chats = obj['data'];
                    let comments = '';
                    for(let i=0; i<chats.length; i++) {
                        comments += '<p style="color: mediumblue">'+ chats[i]["username"]+ ' : ' + chats[i]["message"] + '</p>'
                    }
                    // $('#comments').append(comments);
                    $('input[name="comments"]').val(comments);
                }
            });
        })
    });
</script>

