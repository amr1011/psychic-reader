<?php
if (!isset($ts))
{
    $ts = time();
}
?>

<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Appointment List</h4>

        <div class="card-description">
            <div class="material-datatables">
                <div class="row">


                    <?php if (count($appointments) > 0): ?>

                            <div class="row">
                                    <select class="view-number form-control" >
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                    </select>
                                <div class="pagination"> </div>
                                <button class="btn btn-danger delete btn-sm"  style="float:right;">Delete</button>
                            </div>

                           
                            <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>User Name</th>
                                        <th>First Name</th>
                                        <th>Date/Time</th>
                                        <th style="width: 200px;">Accept/Decline</th>
                                        <th>Status</th>
                                        <th><input type="checkbox" class="select-all">&nbsp;&nbsp;<span>Select All</span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($appointments as $c): ?>
                                        <?php

                                            $c['appointment_date'] = date('m/d/Y H:i', strtotime($c['appointment_date']));
                                        ?>

                                    <tr data-row-id="<?php echo($c['id']);?>" style="display: none;">
                                        <td><?= $c['client_username'] ?></td>
                                        <td><?= $c['client_firstname'] ?></td>
                                        <td><?= $c['appointment_date'] ?></td>
                                        <td>
                                            <?php if($c['confirm'] == 0 && $c['decline'] == 0){ ?>
                                                <button class="btn btn-primary btn-sm btn-xs accept">Accept</button>
                                                <button class="btn btn-warning btn-xs btn-sm  decline">Decline</button>
                                            <?php } else if($c['confirm'] == 1 && $c['decline'] == 0) {?>
                                                <button class="btn btn-success btn-xs btn-sm  accept">Accepted</button>
                                            <?php } else if($c['confirm'] == 0 && $c['decline'] == 1) {?>
                                                <button class="btn btn-danger btn-xs decline btn-sm ">Declined</button>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <?php if($c['status'] == 0 ) { ?>
                                                Not Completed
                                            <?php } else {?>
                                                Completed
                                            <?php } ?>
                                            
                                        </td>
                                        <td><input type="checkbox" class="select-one"></td>
                                        <!--  <td>
                                            <?php if($c['decline'] == 0){ ?>
                                                <button class="btn btn-warning decline">Decline</button>
                                            <?php } else {?>
                                                <button class="btn btn-danger decline">Declined</button>
                                            <?php }?>
                                        </td> -->
                                        <!-- 9-5 -->
                                        <input type="hidden" class="id" value="<?php echo($c['id']);?>">
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div class="page-number" style="float: right; margin-top: 10px;"></div>
                        <?php else: ?>
                            <p>You have no clients.</p>
                        <?php endif; ?>


                </div>
            </div>                
        </div>            
    </div>        
</div>
