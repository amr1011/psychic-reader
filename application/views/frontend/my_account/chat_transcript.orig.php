<!-- <style>
    td, th {
        text-align: center;
    }

    .pagination {
        display: inline-block;
        margin: 0px;
    }

    .pagination button {
        color: black;
        float: left;
        padding: 1px 10px;
        text-decoration: none;
    }

    .pagination button.active {
        background-color: #4CAF50;
        color: white;
        border-radius: 5px;
    }

    .pagination button:hover:not(.active) {
        background-color: #ddd;
        border-radius: 5px;
    }
    button {
        cursor: pointer;
        border: 0px;
        border-radius: 5px;
        margin: 1px;
    }
    .content-pagination {
        display: inline-flex;
        float: right;
        margin-top: 30px;
    }
    .btn-previous, .btn-next {
        margin-top: 20px;
    }
    .btn-previous:hover, .btn-next:hover {
        background-color: #0a6aa1;
        color: white;
    }
    .pagination button {
        width: 35px;
    }
    select.view-number {
        background-image: linear-gradient(45deg, transparent 50%, black 50%),
        linear-gradient(135deg, black 50%, transparent 50%),
        linear-gradient(to right, #ffffff, #ffffff);
        background-position:
                calc(100% - 24px) calc(1em - 4px),
                calc(100% - 19px) calc(1em - 4px),
                100% 0;
        background-size:
                5px 5px,
                5px 5px,
                2.5em 2.5em;
        background-repeat: no-repeat;
    }
</style> -->
<div class="row">
    <div class="my-container page-70-30">

        <div class="content-block-30">
            <?php //$this->load->view('frontend/my_account/nav/dashboard_menu'); ?>
        </div><!-- .content-block-20 -->

        <div class="content-block-70">

            <div class="row">
                <div class="col-md-12 col-sm-7">

                    <h2>Chat Details</h2>

                    <div >
                        <table width='100%' cellPadding='10'>

                            <tr>
                                <td><b>Chat ID:</b></td>
                                <td>#<?php echo  $this->chatmodel->object['id']?></td>
                            </tr>

                            <tr>
                                <td><b>Date:</b></td>
                                <td><?php echo date("m/d/Y h:i A", strtotime($this->chatmodel->object['start_datetime']))?></td>
                            </tr>

                            <tr>
                                <td><b>Chat Length:</b></td>
                                <td><?php echo $this->system_vars->time_generator($this->chatmodel->object['length']); ?></td>
                            </tr>

                            <?php if($chat_reader == 1 && $chat_amount && !$transNotFound):    ?>
                                <tr>
                                    <td><b>Amount Paid:</b></td>
                                    <td>$<?php echo number_format($chat_amount,2)?></td>
                                </tr>

                                <tr>
                                    <td>
                                      
                                        <form action="/my_account/chats/process_refund/<?php echo $this->chatmodel->object['id']; ?>" method="POST">
                                            <input type="submit" value=" Return Entire Chat Time " onclick='return confirm("Are you sure?")'>
                                        </form>
                                    </td>
                                    <td>
                                        <?php



                                        $max = floor($this->chatmodel->object['length'] / 60);
                                        $word = ($max == 1 ? "Minute" : "Minutes");

                                        if ($max)
                                        {
                                            // create dropdown of avail mins to return
                                            ?>
                                            <form action="/my_account/chats/give_timeback/<?php echo $this->chatmodel->object['id']; ?>" method="POST">
                                                <input type="hidden" name="type" value="chat">
                                                <select name="timeback" style="width:100px;">
                                                    <?php
                                                    $min = 1;
                                                    echo "<option value=''>-- Select --</option>/n";
                                                    while ($min <= $max)
                                                    {
                                                        echo "<option value='$min'>$min</option>/n";
                                                        $min++;
                                                    }
                                                    ?>
                                                </select>&nbsp;&nbsp;
                                                <input type="submit" value=" Return <?php echo $word; ?> "></form>
                                            <?php
                                        }
                                        else
                                        {
                                            // no full mins available to return
                                            echo "&nbsp;";
                                        }
                                        ?>
                                    </td>
                                </tr>
                            <? endif; ?>

                        </table>

                    </div>
                    <?php if($chat_reader == 1):    ?>
                        <h2>NRRs</h2>
                        <div >
                            <form class="form-inline" id="timeback_form" action="/my_account/chats/give_timeback/<?=$this->chatmodel->object['id']?>" method="POST">
                                <? $nr = $this->chatmodel->getNRRs(); ?>
                                <? if(count($nr) > 0): ?>

                                    <table class="table table-striped table-bordered" style="margin:15px 0 35px;">
                                        <tr>
                                            <th>Type</th>
                                            <th>Time Requested (mins)</th>
                                            <th>Date</th>
                                            <th>&nbsp;</th>
                                        </tr>

                                        <?php foreach($nr as $n):
                                            $amount = 0;
                                            switch($n['type'])
                                            {
                                                case "unhappy_reading":
                                                    $amount = $n['time_back'];
                                                    break;

                                                case "disconnect":
                                                    $amount = $n['time_back'];
                                                    break;

                                                case "slow":
                                                    $amount = $n['time_back'];
                                                    break;


                                            }
                                            ?>
                                            <tr>
                                                <td><?=ucwords(str_replace('_', ' ',$n['type']))?></td>
                                                <td><?=$amount?> </td>
                                                <td><?=$n['date']?></td>
                                                <td><a href="/my_account/nrr/details/<?=$n['id']?>" class="btn">Details</a></td>
                                            </tr>

                                        <?php endforeach; ?>



                                    </table>

                                <?php else: ?>

                                    <p>There are no NRRs for this chat.</p>

                                <?php endif; ?>

                            </form>
                        </div>
                    <?php endif; ?>

                    <h2>Transcript</h2>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4"></div>
                        <div class="col-lg-2 col-md-2 col-sm-2"></div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <select class="form-control" name="doc-format" style="height: 33px;">
                                <option value="1">.pdf</option>
                                <option value="2">.txt</option>
                            </select>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2"><a href="/my_account/chats/download/<?= $chat_id?>/1" id="download-history" class="btn-sm btn-primary">Download</a></div>
                    </div>

                    <?php $ta = $this->chatmodel->loadTranscripts(); ?>


                    <?php if(count($ta) > 0 && $ta != false): ?>
                        <div class="content-pagination">
                            <div class="view-number-content">
                                <select class="view-number" >
                                    <option value="5">5</option>
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                </select>
                            </div>
                            <div class="pagination" ></div>
                        </div>

                        <ul class="timeline">
                                    <li class="timeline-inverted">
                                      <div class="timeline-badge danger">
                                        <i class="material-icons">card_travel</i>
                                      </div>
                                      <div class="timeline-panel">
                                        <div class="timeline-heading">
                                          <span class="badge badge-pill badge-danger">Some Title</span>
                                        </div>
                                        <div class="timeline-body">
                                          <p>Wifey made the best Father's Day meal ever. So thankful so happy so blessed. Thank you for making my family We just had fun with the “future” theme !!! It was a fun night all together ... The always rude Kanye Show at 2am Sold Out Famous viewing @ Figueroa and 12th in downtown.</p>
                                        </div>
                                        <h6>
                                          <i class="ti-time"></i> 11 hours ago via Twitter
                                        </h6>
                                      </div>
                                    </li>

                                     <li class="timeline-inverted">
                                          <div class="timeline-badge info">
                                            <i class="material-icons">fingerprint</i>
                                          </div>
                                          <div class="timeline-panel">
                                            <div class="timeline-heading">
                                              <span class="badge badge-pill badge-info">Another Title</span>
                                            </div>
                                            <div class="timeline-body">
                                              <p>Called I Miss the Old Kanye That’s all it was Kanye And I love you like Kanye loves Kanye Famous viewing @ Figueroa and 12th in downtown LA 11:10PM</p>
                                              <p>What if Kanye made a song about Kanye Royère doesn't make a Polar bear bed but the Polar bear couch is my favorite piece of furniture we own It wasn’t any Kanyes Set on his goals Kanye</p>
                                              <hr>
                                            </div>
                                            <div class="timeline-footer">
                                              <div class="dropdown">
                                                <button type="button" class="btn btn-round btn-info dropdown-toggle" data-toggle="dropdown">
                                                  <i class="material-icons">build</i>
                                                </button>
                                                <div class="dropdown-menu">
                                                  <a class="dropdown-item" href="#">Action</a>
                                                  <a class="dropdown-item" href="#">Another action</a>
                                                  <a class="dropdown-item" href="#">Something else here</a>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </li>
                                </ul>
                        <table class="table table-striped table-bordered chats" >
                            <tbody>


                                <?php foreach($ta as $transcript): ?>




                                    <tr>

                                        <?php if(strpos($transcript['message'],'system') !== false) {?>

                                            <td><b style="color: orange;">System</b><p><?php echo($transcript['username']);?></p></td>
                                            <td><b><?php echo($transcript['message'])?></b></td>

                                        <?php } else {?>

                                            <td><?=($this->session->userdata('member_logged')==$transcript['member_id'] ? "<b style=\"color:blue;\">{$transcript['username']}</b>" : $transcript['username'])?></td>
                                            <td><?=($this->session->userdata('member_logged')==$transcript['member_id'] ? "<span style=\"color:blue;\">{$transcript['message']}</span>" : $transcript['message'])?></td>
                                        <?php } ?>

                                    </tr>

                                <?php endforeach; ?>
                            </tbody>


                        </table>
                        <div class="page-number" style="float: right; margin-top: 10px;"></div>
                    <?php else: ?>

                        <p>There were no messages transacted in this chat</p>

                    <?php endif; ?>

                </div>

            </div>

        </div><!-- .content-block-80 -->

    </div><!-- .my-container -->
</div>
<script>
    $(document).ready(function () {
        $('select[name=doc-format]').change(function () {
            var format = $( "select[name=doc-format] option:selected" ).val();
            console.log(format);
            $("a#download-history").attr("href", "/my_account/chats/download/<?= $chat_id?>/"+format);
        })
        $('.system.warning-message').css('background', 'none');
        $('.chat_system span').css('margin','0px');
        var pagination = function(per_page) {

            var number_logs = $("table.chats tbody tr").length;
            var number_logs_per_page = per_page;
            var pages = number_logs/number_logs_per_page;
            var page_module = number_logs % number_logs_per_page;
            var group_number = 1;

            pages = Math.trunc(pages);
            $('div.pagination').html('');
            if($('button.btn-previous')) {
                $('button.btn-previous').remove();
            }
            if($('button.btn-next')) {
                $('button.btn-next').remove();
            }
            if(page_module > 0) {
                pages = pages + 1;
            }
            if(number_logs > 0) {
                $("<button class='btn-previous'>&laquo;</button>").insertBefore('.pagination');
                $("<button class='btn-next'>&raquo;</button>").insertAfter('.pagination');
            }

            for (var p=1; p<=pages; p++) {
                if(p == 1) {
                    $("div.pagination").append("<button class='active'>"+p+"</button>");
                } else {
                    $("div.pagination").append("<button>"+p+"</button>");
                }
            }

            $('.page-number').html('page '+1+' of '+pages);

            $('button.btn-previous').hover().css('cursor', 'not-allowed');
            $('button.btn-previous').prop('disabled', true);

            if(pages <= 5) {

                $('button.btn-next').hover().css('cursor', 'not-allowed');
                $('button.btn-next').prop('disabled', true);
            }

            if(pages > 5) {

                for(let n=6; n<=pages; n++) {

                    $("div.pagination button:nth-child("+n+")").css('display', 'none');
                }
            }

            $('button.btn-next').click(function () {

                group_number = group_number + 1;

                if(group_number > 1) {

                    $('button.btn-previous').hover().css('cursor', 'pointer');
                    $('button.btn-previous').prop('disabled', false);
                }
                if((pages % 5 == 0 && group_number >= pages/5) || (pages % 5 > 0 && group_number > pages/5)) {

                    $('button.btn-next').hover().css('cursor', 'not-allowed');
                    $('button.btn-next').prop('disabled', true);
                }

                for(let n=1; n<=pages; n++) {
                    if(n >= ((group_number-1) * 5 + 1) && n <= (group_number*5)) {
                        $("div.pagination button:nth-child("+n+")").css('display', 'block');
                    } else {
                        $("div.pagination button:nth-child("+n+")").css('display', 'none');
                    }
                }
                $("div.pagination").find(".active").removeClass("active");
                $("div.pagination button:nth-child("+((group_number-1)*5+1)+")").addClass("active");

                for (let i = 1; i <= number_logs; i++) {

                    $("table.chats tbody tr:nth-child(" + i + ")").css('display', 'none');
                }

                for (let j = number_logs_per_page * ((group_number-1)*5) + 1; j <= number_logs_per_page * ((group_number-1)*5+1); j++) {
                    $("table.chats tbody tr:nth-child(" + j + ")").css('display', 'table-row');
                }

                $('.page-number').html('page ' + ((group_number-1)*5+1) + ' of ' + pages);
            });

            $('button.btn-previous').click(function () {

                group_number = group_number - 1;

                if(group_number == 1) {

                    $('button.btn-previous').hover().css('cursor', 'not-allowed');
                    $('button.btn-previous').prop('disabled', true);
                }

                if((pages % 5 == 0 && group_number < pages/5) || (pages % 5 > 0 && group_number <= pages/5)) {

                    $('button.btn-next').hover().css('cursor', 'pointer');
                    $('button.btn-next').prop('disabled', false);
                }

                for(let n=1; n<=pages; n++) {
                    if(n >= ((group_number-1) * 5 + 1) && n <= (group_number*5)) {
                        $("div.pagination button:nth-child("+n+")").css('display', 'block');
                    } else {
                        $("div.pagination button:nth-child("+n+")").css('display', 'none');
                    }
                }
                $("div.pagination").find(".active").removeClass("active");
                $("div.pagination button:nth-child("+((group_number-1)*5+1)+")").addClass("active");

                for (let i = 1; i <= number_logs; i++) {

                    $("table.chats tbody tr:nth-child(" + i + ")").css('display', 'none');
                }

                for (let j = number_logs_per_page * ((group_number-1)*5) + 1; j <= number_logs_per_page * ((group_number-1)*5+1); j++) {
                    $("table.chats tbody tr:nth-child(" + j + ")").css('display', 'table-row');
                }

                $('.page-number').html('page ' + ((group_number-1)*5+1) + ' of ' + pages);
            });


            if (number_logs > number_logs_per_page * 1) {

                for (var i = 1; i <= number_logs_per_page * 1; i++) {

                    $("table.chats tbody tr:nth-child(" + i + ")").css('display', 'table-row');
                }
            } else {

                for (var i = 1; i <= number_logs; i++) {

                    $("table.chats tbody tr:nth-child(" + i + ")").css('display', 'table-row');
                }
            }

            var page = 1;


            $(".pagination button").click(function () {

                page = parseInt($(this).text());
                if(page == pages) {

                    console.log('click last page');
                } else {

                    $("#btn-load").attr("disabled", false);
                }

                console.log("current page = "+page);

                $("div.pagination").find(".active").removeClass("active");
                $("div.pagination button:nth-child("+page+")").addClass("active");

                for (var i = 1; i <= number_logs; i++) {

                    $("table.chats tbody tr:nth-child(" + i + ")").css('display', 'none');
                }

                if (number_logs > number_logs_per_page * page) {

                    for (var i = 1 + number_logs_per_page * (page - 1); i <= number_logs_per_page * page; i++) {

                        $("table.chats tbody tr:nth-child(" + i + ")").css('display', 'table-row');
                    }

                } else {

                    for (var i = 1 + number_logs_per_page * (page - 1); i <= number_logs; i++) {

                        $("table.chats tbody tr:nth-child(" + i + ")").css('display', 'table-row');
                    }

                }
                $('.page-number').html('page ' + page + ' of ' + pages);

            });
        }

        pagination(5);
        $('.view-number').change(function () {
            pagination($(this).val());
            $(".pagination button:nth-child(1)").click();
        })
    })
</script>