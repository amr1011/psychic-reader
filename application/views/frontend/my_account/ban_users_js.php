<script>
    $(document).ready(function () {
        $("a.disabled").click(function (e) {
            e.preventDefault();
        });

        $(".search_form_ban").submit(function ()
        {
            var query = $("input[name=query]").val();
            if (!query)
            {
                $("#universal-form-error").html("<div class='alert alert-error page-notifs'><strong>There are errors:</strong><p>Search box cannot be blank.</p></div>");

            }
        });

        $(".client-list").change(function() {

            let client_id = $(this).val();

            let selected = $(this).find('option:selected');
            let client_name = selected.data('foo');

            console.log(client_name);
            console.log(client_id);

            let new_row = "<tr><td width='175' style='vertical-align:middle;'>"+client_name+"</td>"
                        + "<td style='vertical-align:middle;'></td>"
                        + "<td style='vertical-align:middle;'></td>"
                        + "<td style='width:150px; vertical-align:middle; text-align:right'>"
                        + "<a href='/my_account/ban_users/ban/" + client_id + "' class='btn btn-danger' onClick=\"Javascript:return confirm('Are you sure you want to ban member?');\">Ban User</a></td></tr>";
            $("table").append(new_row);
        });
    });
</script>





