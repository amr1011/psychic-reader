
<?php
if (!isset($ts))
{
    $ts = time();
}
?>





<style>
    /*9-2*/

    .reader-note {

        border: none;
        overflow: auto;
        outline: none;
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        box-shadow: none;
        width:100%; 
        height: 100%; 
        resize: none;
        background-image: -webkit-linear-gradient(left, white 10px, transparent 10px), -webkit-linear-gradient(right, white 10px, transparent 10px), -webkit-linear-gradient(white 30px, #ccc 30px, #ccc 31px, white 31px);
        background-image: -moz-linear-gradient(left, white 10px, transparent 10px), -moz-linear-gradient(right, white 10px, transparent 10px), -moz-linear-gradient(white 30px, #ccc 30px, #ccc 31px, white 31px);
        background-image: -ms-linear-gradient(left, white 10px, transparent 10px), -ms-linear-gradient(right, white 10px, transparent 10px), -ms-linear-gradient(white 30px, #ccc 30px, #ccc 31px, white 31px);
        background-image: -o-linear-gradient(left, white 10px, transparent 10px), -o-linear-gradient(right, white 10px, transparent 10px), -o-linear-gradient(white 30px, #ccc 30px, #ccc 31px, white 31px);
        background-image: linear-gradient(left, white 10px, transparent 10px), linear-gradient(right, white 10px, transparent 10px), linear-gradient(white 30px, #ccc 30px, #ccc 31px, white 31px);
        background-size: 100% 100%, 100% 100%, 100% 31px;
        border-radius: 8px;
        box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
        line-height: 31px;
        font-family: Arial, Helvetica, Sans-serif;
        color: black;
        font-size: 16px;
        padding-left: 15px;

    }

    .btn-save {
        float: right;
        border: 1px solid #337ab7;
        padding: 0px 3px 0px 3px;
        font-size: 14px;
        background: #337ab7;
        color: white;
    }

    .reader-id, .client-id, .client-username, .client-firstname, .client-lastname {
        display: none;
    }
</style>

<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Note List</h4>

        <div class="card-description">
            <div class="material-datatables">

            <div class="row">
            <?php if (count($notes) > 0): ?>
                        <table class="table table-striped table-bordered" style="margin:15px 0 35px;">
                            <tr>
                                <th>User Name</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Notes</th>
                            </tr>
                            <?php foreach($notes as $c): ?>
                            <tr>
                                <td><?= $c['client_username'] ?></td>
                                <td><?= $c['client_firstname'] ?></td>
                                <td><?= $c['client_lastname'] ?></td>
                                <td>
                                    <div class="contactlist-formbox" style="position: unset;" data-toggle="tooltip" title="Click here to view or edit note">
                                        <div class="contactlist-formbox-head" style="background: #337ab7;">Note
                                            <button class="btn btn-default btn-save">save</button>
                                            <p class="reader-id"><?= $c['reader_id'] ?></p>
                                            <p class="client-id"><?= $c['client_id'] ?></p>
                                            <p class="client-username"><?= $c['client_username'] ?></p>
                                            <p class="client-firstname"><?= $c['client_firstname'] ?></p>
                                            <p class="client-lastname"><?= $c['client_lastname'] ?></p>
                                        </div>
                                        <div class="contactlist-formbox-body" style="overflow: unset; border: 1px solid #337ab7;">
                                            <textarea class="reader-note"><?= $c['note'] ?></textarea>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </table>
                    <?php else: ?>
                        <p>You have no clients.</p>
                    <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {

        $(".btn-save").click(function() {

            $.ajax({
                'type': "POST",
                'url': "/chat/chatInterface/save_note",
                'data': { reader_id : $(this).parent().find('.reader-id').text(), client_id: $(this).parent().find('.client-id').text(),
                            client_username: $(this).parent().find('.client-username').text(),
                            client_firstname: $(this).parent().find('.client-firstname').text(), client_lastname: $(this).parent().find('.client-lastname').text(), note: $(this).parent().parent().find('.contactlist-formbox-body').find('.reader-note').val()},
                success: function (data) {
                    obj = JSON.parse(data);
                }
            });
        });

        $('[data-toggle="tooltip"]').tooltip();   

        $(".contactlist-formbox-head").click(function () {

            $(this).parent().find(".contactlist-formbox-body").slideToggle(300);
        });
    })
</script>