<!-- new layout -->
<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Email Readings</h4>
        <div class="card-description">

            <div class="material-datatables">
                <?php if($this->session->flashdata('response')):?>
                <p class='record' style="display: none;"> <?=$this->session->flashdata('response')?> </p>
                <?php endif?>
                <p>To modify your "Default Reading Settings", visit your "Edit My Expert Profile" page by <a href='/my_account/main/edit_profile'>clicking here</a></p>
                <ul class="nav">
                    <li class="nav-item"><a class="nav-link" href="/my_account/email_readings/open_requests">Open Email Requests</a></li>
                    <li class="nav-item"><a class="nav-link" href="/my_account/email_readings/closed_requests">Closed Email Requests</a></li>
                    <li class="nav-item"><a  class="nav-link active" href="/my_account/email_readings/email_specials">My Email Specials</a></li>
                    <li class="nav-item"><a class="nav-link" href="/my_account/email_readings/new_special"><span class='icon icon-tag'></span> Create A New Special</a></li>
                </ul>


                <?php

                if($specials)
                {

                    echo "<div class='table-responsive'><table class='table table-hover table-striped'>
                        
                        <thead>
                            
                            <tr>
                                <th>Title:</th>
                                <th>Questions Allowed:</th>
                                <th>Total Credits:</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                            </tr>
                            
                        </thead>
                        
                        <tbody>
                    ";

                    foreach($specials as $s)
                    {

                        echo "
                        <tr>
                            <td style='word-break: break-word; width: 35%'>{$s['title']}</td>
                            <td>{$s['total_questions']}</td>
                            <td>{$s['price']}</td>
                            <td style='width:75px;text-align:right;'><a href='/my_account/email_readings/edit_special/{$s['id']}' class='btn btn-mini'>Edit</a></td>
                            <td style='width:40px;text-align:right;'><a href='/my_account/email_readings/delete_special/{$s['id']}' onClick=\"Javascript:return confirm('Are you sure you want to delete this email special?');\" class='btn'>&nbsp;<span class='fa fa-trash'></span> &nbsp;</a></td>
                        </tr>
                        ";

                    }

                    echo "</tobdy>
                    </table></div>";

                }
                else
                {

                    echo "<p>You have not added any email specials. To create your own email specials, click the \"Create A New Special\" tab at the top or <a href='/my_account/email_readings/new_special'>click here</a>.</p>";

                }

                ?>
            </div>                
        </div>

    </div>
</div>
