    <style>
        /*h1, h4 {*/
            /*color: #126595;*/
            /*font-family: Verdana,Geneva,sans-serif;*/
            /*font-size: 18px;*/
            /*font-weight: normal;*/
            /*padding: 0 0 8px;*/
        /*}*/
        /*h3{font-size:16px;color:#126595;font-weight:normal;}*/
        .clear{clear:both;}

        #slow{display:none;}
        #disconnect{display:none;}
        #unhappy_reading{display:none;}

        .readerName{font-weight:bold;font-style:italic;}

    </style>
    <?
        $description = "";
        $timeback = 0;
        $dc_desc = "";
        switch($type)
        {
            case "slow":
                    $description = "The chat was running slow.";
                    $timeback = $time_back;
                break;

            case "disconnect":
                    $description = "The chat ended suddenly and I was booted out.";
                    switch($disconnect)
                    {
                        case 1:
                            $dc_desc = "I have checked my account & time was restored: I will try to re-enter chat asap.";
                            $timeback = $time_back;
                        break;

                        case 2:
                            $dc_desc = "I have checked my account & time was restored: I will save my time for another session and/or reader";
                            $timeback = $time_back;
                        break;

                        case 3:
                            $dc_desc = " I have checked my account no minutes were returned.";
                            $timeback = $time_back;
                        break;
                    }
                break;

            case "unhappy_reading":
                    $description = "I was unhappy with my reading from my reader.";
                    $timeback = $time_back;
                break;
        }
    ?>
    <div class="row">
    <div class="my-container page-70-30">

        <div class="content-block-30">
            <?php $this->load->view('frontend/my_account/nav/dashboard_menu'); ?>
        </div><!-- .content-block-20 -->

        <div class="content-block-70">
            <div class="row">
                <div class="col-md-12 col-sm-7">

                    <h2>NRR Details</h2>

                    <div class='well' style="margin:15px 0 35px;">

                        <div class="pull-left"><h4>Client: <?=$first_name . " " . $last_name ?></h4></div>
                        <div class="pull-right"><a class="btn btn-inverse" href="/my_account/chats/transcript/<?=$chat_id?>">View Chat</a></div>
                        <div class="clear"></div>
                        <div style="margin-top:10px;"><h4>Date: <?=date('m/d/Y @ h:i:s a',strtotime($date))?></h4></div>
                        <div style="margin-top:20px;" class="well well-small">
                            <b>Complaint Type:</b> <?=$description?>
                        </div>
                        <? if($type == 'disconnect'): ?>
                            <div style="margin-top:20px;" class="well well-small">
                                <b>Disconnect Selection:</b> <?=$dc_desc?>
                            </div>
                            <? if(floatval($time_back) > 0): ?>
                                <div style="margin-top:20px;" class="well well-small">
                                       <b>Requested Time back:</b> <?=$timeback?> (mins)
                                </div>
                            <? endif; ?>
                        <? else: ?>
                           <div style="margin-top:20px;" class="well well-small">
                               <b>Requested Time back:</b> <?=$timeback?>
                           </div>
                        <? endif; ?>

                        <? if($type == 'unhappy_reading'): ?>
                            <h4>Reason:</h4>
                            <div style="margin-top:20px;" class="well large-well">
                                <p>
                                    <?=$unhappy?>
                                </p>
                            </div>
                        <? endif; ?>
                        <h4>Suggestions:</h4>
                        <div style="margin-top:20px;" class="well large-well">
                            <p>
                                <?=$suggest?>
                            </p>
                        </div>

                        <script>
                            $(document).ready(function()
                            {
                                var timeback = <?=$timeback?>;

                                $("#timeback_form").submit(function()
                                {
                                    var tb = $("input[name=timeback]").val();
                                    if($("select[name=type]").val() == 'paid' && timeback < tb)
                                    {
                                       $("#universal-form-error").html("<div class='alert alert-error page-notifs'><strong>There was an error:</strong><p>Incorrect Refund Amount.</p></div>");
                                        return false;
                                    }
                                });


                            });
                        </script>
                        <? if($refunded != 1): ?>
                        <h4>Give Time Back</h4>
                        <div style="padding:20px 0 0 20px;">
                            <form class="form-inline" id="timeback_form" action="/my_account/nrr/give_timeback/<?=$id?>" method="POST">

                                <label for="timeback">Type</label>

                                <select style="width:150px;margin-right:20px;" name="type">
                                    <option value="paid">paid</option>
                                    <option value="free">free</option>
                                </select>

                                <label for="timeback">Time Back</label>

                                <input class="form-control" style="width:150px;margin-right:20px;" type="text" name="timeback" placeholder="Time Back">

                                <input type="submit" class="btn btn-primary" value="Submit">

                            </form>
                        </div>
                        <? else : ?>
                            <h4>Refunded</h4>
                            <div style="padding:20px 0 0 20px;">
                                <div><b><?=$amount?></b></div>
                            </div>
                        <? endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>