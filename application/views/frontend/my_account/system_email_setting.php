<!-- new layout -->
<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Notification Board</h4>

        <div class="card-description">

            <?php if($this->session->flashdata('response')):?>
                <p class='record text-warning' > <?=$this->session->flashdata('response')?> </p>
            <?php endif?>

            <div class="material-datatables">
                <ul class="nav nav-pills nav-pills-primary" role="tablist">
                        <li  <?= ($this->uri->segment('3') == '' ? " class=\"nav-item active\"" : "") ?>>
                            <a href="/my_account/system_email"><span class="fa fa-user"></span> Notification Board (NoBo)</a></li>                        
                        <li class="nav-item" <?= ($this->uri->segment('3') == 'email_setting' ? " class=\" nav-item active\"" : "") ?>>
                            <a href="/my_account/system_email/email_setting"><span class='fa fa-gear'></span> Setting</a>
                        </li>
                </ul>

                     <?php
                    if ($messages) {
                        echo "<div class='btn-box'>
                                    <div class=\"sk-circle\" style=\"width: 60px;height: 60px; position: fixed; left: 60%; display: none;\">
                                    <div class=\"sk-circle1 sk-child\"></div>
                                    <div class=\"sk-circle2 sk-child\"></div>
                                    <div class=\"sk-circle3 sk-child\"></div>
                                    <div class=\"sk-circle4 sk-child\"></div>
                                    <div class=\"sk-circle5 sk-child\"></div>
                                    <div class=\"sk-circle6 sk-child\"></div>
                                    <div class=\"sk-circle7 sk-child\"></div>
                                    <div class=\"sk-circle8 sk-child\"></div>
                                    <div class=\"sk-circle9 sk-child\"></div>
                                    <div class=\"sk-circle10 sk-child\"></div>
                                    <div class=\"sk-circle11 sk-child\"></div>
                                    <div class=\"sk-circle12 sk-child\"></div>
                                </div>
                                <button class='btn btn-primary' id='save-email-setting'>Save</button></div>";
                        echo "<div class='table-responsive'><table width='100%' cellPadding='0' cellSpacing='0' class='table table-striped table-hover'><thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Private/select all <input type='checkbox' class='select-all-private'></th>
                                    <th>Trash/select all <input type='checkbox' class='select-all-trash'></th>
                                    <th>Create Folder/select all <input type='checkbox' class='select-all-has-folder'></th>
                                    <th style='display: none; width: 0px;'></th>
                                </tr>
                                </thead>
                                <tbody>
                        ";
                        $i = 1;
                        foreach ($messages as $m) {
                            if(strtotime($m['created_at']) > strtotime(date("Y-m-d")))
                            {
                                echo "
                                    <tr>
                                        <td>{$i}</td>
                                        <td class='email-name' style='font-weight: bold; font-size: 18px; color: dodgerblue;'>{$m['name']}</td>
                                        <td><input type='checkbox' class='email-private'></td>
                                        <td><input type='checkbox' class='email-trash'></td>
                                        <td><input type='checkbox' class='email-has-folder'></td>
                                        <td style='display:none;'><input type='hidden' class='email-setting-type'/></td>
                                        <td style='display:none;'><input type='hidden' class='email-setting-has-folder'/></td>
                                    </tr>";
                            } else
                            {
                                echo "
                                    <tr>
                                        <td>{$i}</td>
                                        <td class='email-name' style='display: none;'>{$m['name']}</td>
                                        <td class='email-name-show'></td>
                                        <td><input type='checkbox' class='email-private'></td>
                                        <td><input type='checkbox' class='email-trash'></td>
                                        <td><input type='checkbox' class='email-has-folder'></td>
                                        <td style='display:none;'><input type='hidden' class='email-setting-type'/></td>
                                        <td style='display:none;'><input type='hidden' class='email-setting-has-folder'/></td>
                                    </tr>";
                            }

                            $i++;
                        }
                        echo "</tbody></table></div>";
                    } else {
                        echo "<p>You do not have any messages</p>";
                    }
                    ?>
            </div>
        </div>
    </div>
</div>