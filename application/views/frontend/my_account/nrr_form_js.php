<script>

    $(document).ready(function ()
    {
        var max;
        $("form[name=nrr_form]").submit(function ()
        {
            var sel = $("select[name=reader]").val();
            var complaint_type = $("input[name=type]:checked").val();
            var reader = $("select[name=reader] option:selected").text();

            if (sel != 0)
            {
                if (confirm('Are you sure you want to proceed with NRR request form?'))
                {
                    switch (complaint_type)
                    {
                        case "slow":

                            if (!$("input[name=slow_timeback]").val())
                            {
                                $("#universal-form-error").html("<div class='alert alert-error page-notifs'><strong>There was an error:</strong><p>Please input time you wished returned.</div>");
                                return false;
                            }
                            break;
                        case "disconnect":

                            if (!$("input[name=disconnect]").val())
                            {
                                $("#universal-form-error").html("<div class='alert alert-error page-notifs'><strong>There was an error:</strong><p>Please select a disconnect option.</div>");
                                return false;
                            } else
                            {
                                if ($("input[name=disconnect]").val() == "3")
                                {
                                    if (!$("input[name=disconnect_timeback]").val())
                                    {
                                        $("#universal-form-error").html("<div class='alert alert-error page-notifs'><strong>There was an error:</strong><p>Please input time you wished returned.</div>");
                                        return false;
                                    }
                                }
                            }
                            break;
                        case "unhappy_reading":

                            if (!$("textarea[name=unhappy]").val())
                            {
                                $("#universal-form-error").html("<div class='alert alert-error page-notifs'><strong>There was an error:</strong><p>Please provide the reason you are unhappy with this chat..</div>");
                                return false;
                            } else
                            {
                                if (!$("input[name=unhappy_timeback]").val())
                                {

                                    $("#universal-form-error").html("<div class='alert alert-error page-notifs'><strong>There was an error:</strong><p>Please input time you wished returned.</div>");
                                    return false;
                                }
                            }
                            break;
                    }

                } else
                {
                    return false;
                }
            } else
            {
                $("#universal-form-error").html("<div class='alert alert-error page-notifs'><strong>There was an error:</strong><p>You must select a reader to submit the form.</div>");
                return false;
            }

        });

        $("#slow").show();
        $("#disconnect").hide();
        $("#unhappy_reading").hide();

        // $("input[name=type]").change(function ()
        // {
        //     var type = $(this).val();
        //     switch (type)
        //     {
        //         case "slow":
        //             $("#disconnect").hide();
        //             $("#unhappy_reading").hide();
        //             $("#slow").show();
        //             break;
        //
        //         case "disconnect":
        //             $("#disconnect").show();
        //             $("#unhappy_reading").hide();
        //             $("#slow").hide();
        //             break;
        //
        //         case "unhappy_reading":
        //             $("#unhappy_reading").show();
        //             $("#disconnect").hide();
        //             $("#slow").hide();
        //             break;
        //     }
        //
        // });

        $("input[name=type]").change(function ()
        {
            var type = $(this).val();
            switch (type)
            {
                case "slow":

                    $(this).is(':checked') == true ? $("#slow").show() : $("#slow").hide();
                    break;

                case "disconnect":

                    $(this).is(':checked') == true ? $("#disconnect").show().find('input[name="disconnect_time_back"]').prop('required',true) : $("#disconnect").hide().find('input[name="disconnect_time_back"]').removeAttr('required');
                    break;

                case "unhappy_reading":

                    $(this).is(':checked') == true ? $("#unhappy_reading").show().find('input[name="unhappy_timeback"]').prop('required',true) : $("#unhappy_reading").hide().find('input[name="unhappy_timeback"]').removeAttr('required');
                    break;
            }

        });

        <?php if (isset($chat['reader_id'])): ?>
            $.ajax({
            dataType: "json",
                    url: '/my_account/main/nrr_chat_session/<?= $chat['reader_id'] ?>',
                    success: function (data)
                    {

                        var sel = $("select[name=chat]");
                        sel.html("");
                        var title = "";

                        for (r in data)
                        {

                            title = truncateString(data[r].topic, 17);
                            // sel.append("<option value='" + data[r].id + "'>" + "#" + data[r].id + " " + data[r].chat_date + " - " + title + "</option>");
                            let chat_logon_time = new Date(data[r]['create_datetime']).getTime();
                            let now_time = new Date().getTime();
                            let duration = (now_time - chat_logon_time)/(3600*1000);
                            let length = (data[r].length/60).toFixed(2);

                            if(duration - 7.0012 > 72)
                            {
                                sel.append("<option value='" + data[r].id + "' disabled style='color: #d2cece'>" + "#" + data[r].id + " " + data[r].chat_date + " - " + title + "</option>");
                                continue;
                            }
                            
                            sel.append("<option limit='" + length + "' value='" + data[r].id + "'>" + "#" + data[r].id + " " + data[r].chat_date + " - " + title + "</option>");
                        }

                    }
            });
            <?php endif; ?>
            $("select[name=reader]").change(function ()
            {
                var cid = $(this).val();
                $(".readerName").html($("option:selected", this).text());
                $.ajax({
                    dataType: "json",
                    url: '/my_account/main/nrr_chat_session/' + cid,
                    success: function (data)
                    {

                        var sel = $("select[name=chat]");
                        sel.html("");
                        var title = "";

                        for (r in data)
                        {

                            title = truncateString(data[r].topic, 17);
                            let chat_logon_time = new Date(data[r]['create_datetime']).getTime();
                            let now_time = new Date().getTime();
                            let duration = (now_time - chat_logon_time)/(3600*1000);
                            let length = (data[r].length/60).toFixed(2);


                            if(duration - 7.0012 > 72)
                            {
                                sel.append("<option value='" + data[r].id + "' disabled style='color: #d2cece'>" + "#" + data[r].id + " " + data[r].chat_date + " - " + title + "</option>");
                                continue;
                            }

                            sel.append("<option limit='" + length + "' value='" + data[r].id + "'>" + "#" + data[r].id + " " + data[r].chat_date + " - " + title + "</option>");
                        }

                        max = $('select[name=chat] option:eq(0)').attr('limit');
                        if(max != undefined) {
                            $('input.time-back').attr("placeholder", "Time Back Limit: " + max +
                                " mins");
                            $('input.time-back').attr("limit",  max);
                            $('input#max-length').val(max);
                        } else {
                            $('input.time-back').attr("placeholder", "Time Back");
                        }
                    }
                });
            });



        $("Select[name=chat]").change(function()
        {
           max =  $(this).find('option:selected').attr("limit");

           $('input.time-back').attr("placeholder", "Time Back Limit: " + max +
               "mins");
            $('input.time-back').attr("limit",  max);
            $('input#max-length').val(max);
        });

        $('input.time-back').on('keydown keyup', function(e){
            console.log($(this).val());
            console.log(max)
            if (parseFloat($(this).val()) > max) {
                e.preventDefault();
                $(this).val(max);
            }
            if (parseFloat($(this).val()) < 0) {
                e.preventDefault();
                $(this).val(0);
            }
        });

        if($('p.record').html()) {
            $.notify($('p.record').html(), {className:"success",align:"center", verticalAlign:"top", hideDuration: 700, autoHideDelay: 5000,});
        }


    });

    function truncateString(str, length) {
        return str.length > length ? str.substring(0, length - 3) + '...' : str
    }
</script>