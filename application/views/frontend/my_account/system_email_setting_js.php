<script>
    $(document).ready(function () {
        // email-setting-type  1:private 2:trash 3:delete 4:both
        $(".email-private").change(function () {
            let private_checked = $(this).is(':checked');
            if (private_checked) {
                if($(this).parent().parent().find(".email-trash").prop('checked') == true) {
                    $(this).parent().parent().find("input.email-setting-type").val(4); // both selected
                } else {
                    $(this).parent().parent().find("input.email-setting-type").val(1); // private selected
                }
                // $(this).parent().parent().find(".email-trash").prop('checked', false);
            } else {
                if($(this).parent().parent().find(".email-trash").prop('checked') == true) {
                    $(this).parent().parent().find("input.email-setting-type").val(2); // trash selected
                } else {
                    $(this).parent().parent().find("input.email-setting-type").val(3); // both unselected
                }
            }
        });

        $(".select-all-private").change(function () {

            let email_length = $("tbody tr").length;
            let private_checked = $(this).is(':checked');

            if (private_checked) {
                for(let i=1; i<=email_length; i++) {
                    let trash_status = $("tbody tr:nth-child(" + i + ")").find(".email-trash").prop('checked');
                    if(trash_status == true) {
                        $("tbody tr:nth-child(" + i + ")").find("input.email-setting-type").val(4);
                    } else {
                        $("tbody tr:nth-child(" + i + ")").find("input.email-setting-type").val(1);
                    }
                }
                $(".email-private").prop('checked', true);
            } else {

                for(let i=1; i<=email_length; i++) {
                    let trash_status = $("tbody tr:nth-child(" + i + ")").find(".email-trash").prop('checked');
                    if(trash_status == true) {
                        $("tbody tr:nth-child(" + i + ")").find("input.email-setting-type").val(2);
                    } else {
                        $("tbody tr:nth-child(" + i + ")").find("input.email-setting-type").val(3);
                    }
                }
                $(".email-private").prop('checked', false);
            }
        });

        $(".select-all-trash").change(function () {

            let email_length = $("tbody tr").length;
            let trash_checked = $(this).is(':checked');

            if (trash_checked) {
                for(let i=1; i<=email_length; i++) {
                    let private_status = $("tbody tr:nth-child(" + i + ")").find(".email-private").prop('checked');
                    if(private_status == true) {
                        $("tbody tr:nth-child(" + i + ")").find("input.email-setting-type").val(4);
                    } else {
                        $("tbody tr:nth-child(" + i + ")").find("input.email-setting-type").val(2);
                    }
                }
                $(".email-trash").prop('checked', true);
            } else {
                for(let i=1; i<=email_length; i++) {
                    let private_status = $("tbody tr:nth-child(" + i + ")").find(".email-private").prop('checked');
                    if(private_status == true) {
                        $("tbody tr:nth-child(" + i + ")").find("input.email-setting-type").val(1);
                    } else {
                        $("tbody tr:nth-child(" + i + ")").find("input.email-setting-type").val(3);
                    }
                }
                $(".email-trash").prop('checked', false);
            }
        });

        $(".email-trash").change(function () {

            let trash_checked = $(this).is(':checked');
            if (trash_checked) {
                if($(this).parent().parent().find(".email-private").prop('checked') == true) {

                    $(this).parent().parent().find("input.email-setting-type").val(4);
                } else {
                    $(this).parent().parent().find("input.email-setting-type").val(2);
                }

            } else {
                if($(this).parent().parent().find(".email-private").prop('checked') == true) {
                    $(this).parent().parent().find("input.email-setting-type").val(1);
                } else {
                    $(this).parent().parent().find("input.email-setting-type").val(3);
                }
            }
        });

        $(".email-has-folder").change(function () {

            let has_folder_checked = $(this).is(':checked');
            if(has_folder_checked) {
                $(this).parent().parent().find("input.email-setting-has-folder").val(1);
            } else {
                $(this).parent().parent().find("input.email-setting-has-folder").val(0);
            }
        });

        $(".select-all-has-folder").change(function () {

            let email_length = $("tbody tr").length;
            let has_folder_checked = $(this).is(':checked');

            if (has_folder_checked) {
                for(let i=1; i<=email_length; i++) {
                    $("tbody tr:nth-child(" + i + ")").find("input.email-setting-has-folder").val(1);
                }
                $(".email-has-folder").prop('checked', true);
            } else {
                for(let i=1; i<=email_length; i++) {
                    $("tbody tr:nth-child(" + i + ")").find("input.email-setting-has-folder").val(0);
                }
                $(".email-has-folder").prop('checked', false);
            }
        });

        $("#save-email-setting").on('click', function () {

            $(".sk-circle").css("display", "block");

            let email_length = $("tbody tr").length;
            let private_emails = [];
            let trash_emails = [];
            let canceled_emails = [];
            let has_folders = [];

            for (let i = 1; i <= email_length; i++) {
                let email_setting = $("tbody tr:nth-child(" + i + ")").find(".email-setting-type").val();
                let email_setting_has_folder = $("tbody tr:nth-child(" + i + ")").find(".email-setting-has-folder").val();
                if (email_setting == 1) {
                    private_emails.push($("tbody tr:nth-child(" + i + ")").find(".email-name").text());
                }
                if (email_setting == 2) {
                    trash_emails.push($("tbody tr:nth-child(" + i + ")").find(".email-name").text());
                }
                if (email_setting == 4) {
                    // canceled_emails.push($("tbody tr:nth-child(" + i + ")").find(".email-name").text());
                    private_emails.push($("tbody tr:nth-child(" + i + ")").find(".email-name").text());
                    trash_emails.push($("tbody tr:nth-child(" + i + ")").find(".email-name").text());
                }
                if(email_setting_has_folder == 1) {
                    has_folders.push($("tbody tr:nth-child(" + i + ")").find(".email-name").text());
                }
            }

            setTimeout(function () {
                $.ajax({
                    'type': "POST",
                    'url': "/my_account/system_email/save_email_setting",
                    'data': { user_id : '<?=$this->member->data['id'];?>', private: private_emails, trash: trash_emails, has_folders: has_folders },
                    success: function (data) {
                        let obj = JSON.parse(data);
                        console.log(obj.private);
                        console.log(obj.trash);
                        $(".sk-circle").css("display", "none");
                    }
                });
            }, 1000);
        });

        $.ajax({
            'type': "POST",
            'url': "/my_account/system_email/get_email_setting",
            'data': { user_id : '<?=$this->member->data['id'];?>'},
            success: function (data) {
                let obj = JSON.parse(data);
                let private_emails = obj.private;
                let trash_emails = obj.trash;
                let has_folders = obj.has_folders;

                let count_private = 0;
                let count_trash = 0;
                let count_has_folders = 0;
                let email_length = $("tbody tr").length;

                for(let k = 1; k <= email_length; k++) {
                    let email_name = $("tbody tr:nth-child(" + k + ")").find(".email-name").text();
                    let array = email_name.split("_");
                    let email_name_show = "";
                    for(let n = 0; n< array.length; n++) {

                        email_name_show = email_name_show + array[n].charAt(0).toUpperCase() + array[n].slice(1) + " ";
                    }
                    $("tbody tr:nth-child(" + k + ")").find(".email-name-show").text(email_name_show);

                }

                for(let i = 0; i < private_emails.length; i++) {

                    for (let j = 1; j <= email_length; j++) {

                        if($("tbody tr:nth-child(" + j + ")").find(".email-name").text() == private_emails[i]) {
                            $("tbody tr:nth-child(" + j + ")").find(".email-private").prop('checked', true);
                            count_private++;
                        }

                    }
                }
                for(let l = 0; l < trash_emails.length; l++) {

                    for (let k = 1; k <= email_length; k++) {

                        if($("tbody tr:nth-child(" + k + ")").find(".email-name").text() == trash_emails[l]) {
                            $("tbody tr:nth-child(" + k + ")").find(".email-trash").prop('checked', true);
                            count_trash++;
                        }

                    }
                }
                for(let m = 0; m < has_folders.length; m++) {

                    for(let n = 1; n <= email_length; n++) {
                        if($("tbody tr:nth-child(" + n + ")").find(".email-name").text() == has_folders[m]) {
                            $("tbody tr:nth-child(" + n + ")").find(".email-has-folder").prop('checked', true);
                            count_has_folders++;
                        }
                    }
                }
                if(count_private == email_length) {
                    $(".select-all-private").prop('checked', true);
                }
                if(count_trash == email_length) {
                    $(".select-all-trash").prop('checked', true);
                }
                if(count_has_folders == email_length) {
                    $(".select-all-has-folder").prop('checked', true);
                }
                setTimeout(function () {
                    for(let r=1; r<=$("tbody tr").length; r++) {
                        let private_status = $("tbody tr:nth-child(" + r + ")").find(".email-private").prop('checked');
                        let trash_status = $("tbody tr:nth-child(" + r + ")").find(".email-trash").prop('checked');
                        let has_folder_status = $("tbody tr:nth-child(" + r + ")").find(".email-has-folder").prop('checked');
                        if(private_status && trash_status) {
                            $("tbody tr:nth-child(" + r + ")").find(".email-setting-type").val(4);
                        } else if(private_status && !trash_status) {
                            $("tbody tr:nth-child(" + r + ")").find(".email-setting-type").val(1);
                        } else if(!private_status && trash_status) {
                            $("tbody tr:nth-child(" + r + ")").find(".email-setting-type").val(2);
                        } else {
                            $("tbody tr:nth-child(" + r + ")").find(".email-setting-type").val(3);
                        }
                        if(has_folder_status) {
                            $("tbody tr:nth-child(" + r + ")").find(".email-setting-has-folder").val(1);
                        } else {
                            $("tbody tr:nth-child(" + r + ")").find(".email-setting-has-folder").val(0);
                        }
                    }
                }, 1000);
            }
        });
    })
</script>