<!-- new layout -->
<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title"><?= $title ?></h4>

        <div class="card-description">

            <?php if($this->session->flashdata('response')):?>
                <p class='record text-warning' > <?=$this->session->flashdata('response')?> </p>
            <?php endif?>

            <div class="material-datatables">
                    <ul class="nav nav-pills nav-pills-primary" role="tablist">
                        <li class="nav-item" <?= ($this->uri->segment('3') == '' ? " class=\"active\"" : "") ?>>
                            <a  class="nav-link" href="/my_account/messages">Inbox</a></li>
                        <li class="nav-item" <?= ($this->uri->segment('3') == 'outbox' ? " class=\"active\"" : "") ?>>
                            <a class="nav-link" href="/my_account/messages/outbox">Outbox</a></li>
                        <li class="nav-item" <?= ($this->uri->segment('3') == 'compose' ? " class=\"active\"" : "") ?>>
                            <a class="nav-link" href="/my_account/messages/compose"><span class='icon icon-comment'></span> Compose A New Message</a></li>
                    </ul>
                    <div class="content-block-60" style="display: inline-flex; width: 100%;">
                        <div class="content-notification-folder col-md-4" >
                            <div class="notification-folder admin-folder">
                                <span class="fa fa-folder folder-icon enabled"><input type="hidden" class="folder-status" value="0"></span>
                                <span class="folder-name enabled">Administrator</span>

                            </div>
                            <div class="main-folders">
                                <?php foreach($receivers as $receiver) {?>
                                    <div class="notification-folder message-folder">
                                        <?php if($this->member->data['member_type'] == 'READER') { ?>
                                            <?php if($receiver['banned'] && $receiver['banned'] == 1) {?>
                                                <span class="fa fa-folder folder-icon disabled"></span>
                                                <span class="folder-name disabled sender-id" style="display: none;"><?= $receiver['receiver_id']?></span>
                                                <span class="folder-name disabled show-folder-name"><?= $receiver['username'] .' ('. $receiver['first_name'] .')'?></span>
                                            <?php } else {?>
                                                <span class="fa fa-folder folder-icon enabled"><input type="hidden" class="folder-status" value="0"></span>
                                                <span class="folder-name enabled sender-id" style="display: none;"><?= $receiver['receiver_id']?></span>
                                                <span class="folder-name enabled show-folder-name"><?= $receiver['username'] .' ('. $receiver['first_name'] .')'?></span>

                                            <?php }?>
                                        <?php } else {?>
                                            <span class="fa fa-folder folder-icon enabled"><input type="hidden" class="folder-status" value="0"></span>
                                            <span class="folder-name enabled sender-id" style="display: none;"><?= $receiver['receiver_id']?></span>
                                            <span class="folder-name enabled show-folder-name"><?= $receiver['username'] .' ('. $receiver['first_name'] .')'?></span>
                                        <?php }?>
                                    </div>
                                <?php }?>
                            </div>
                        </div>
                        <div class="content-notification-messages col-md-6">
                            <div class="box-message">

                            </div>
                        </div>
                    </div>

            </div>
        </div>
    </div>
</div>    