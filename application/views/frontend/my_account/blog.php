
<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Blog List</h4>

        <div class="card-description">
            <div class="material-datatables">
                <div class="row">

                    <?php if (count($blogs) > 0): ?>

                            <div class="row" >
                                <select class="view-number form-control">
                                    <option value="5">5</option>
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                </select>
                                <div class="pagination" ></div>
                                <a class="btn btn-primary btn-sm" href="/my_account/main/blog_edit">Add New</a>

                            </div>

                        <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <th style="width: 15%">Title</th>
                                <th>Image</th>
                                <th>Category</th>
                                <th>Approve</th>
                                <th>Action</th>
                            </thead>
                            <?php foreach($blogs as $b): ?>
   
                                <tr style="display: none;">
                                    <td><?= $b['title']?></td>
                                    <td>
                                        <?php if($b['image'] != null) : ?>
                                            <img src="/media/assets/<?= $b['image']?>" style="width: 200px; height: 150px;"/>
                                        <?php else : ?>
                                            <img src="/media/assets/no-image-available.jpg" style="width: 200px; height: 150px;"/>
                                        <?php endif; ?>
                                    </td>
                                    <td style="font-size: 12px;"><?= $b['category']?></td>
                                    <td><?= $b['approved'] == 1 ? ("approved") : ("un approved"); ?></td>
                                    <td style="text-align:center;"><a href="/my_account/main/blog_edit/<?= $b['id']?>" class="btn-sm btn-primary"><i class="fa fa-pencil"></i></a></td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                        <div class="page-number" style="float: right; margin-top: 10px;"></div>
                    <?php else: ?>
                        <p>You have no clients.</p>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>    
</div>    

