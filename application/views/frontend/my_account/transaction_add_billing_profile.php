
<!-- new layout -->
<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Payment</h4>

        <div class="card-description">

            <?php if($this->session->flashdata('response')):?>
                <p class='record text-warning' > <?=$this->session->flashdata('response')?> </p>
            <?php endif?>
            <div class="material-datatables">
                <p>To add a credit card to your account, fill out the form below. You will be able to use this credit/debit card to fund your account.</p>
                <p><b>Please Note:</b> To verify your card, we will authorize $1.00. This authorization will be refunded back to you within 7-10 business days.</p>

                <?php
                    if($this->session->flashdata('success')) {
                        echo "<div class='alert alert-success page-notifs'><strong>Success:</strong><p>". $this->session->flashdata('success') . "</p></div>";
                    }
                ?>
                <?php
                    if($this->session->flashdata('error'))
                    {
                        echo "<div class='alert alert-error page-notifs'><strong>There are errors:</strong><p>" . $this->session->flashdata('error') . "</p></div>";
                    }
                ?>

                  <form class="form-horizontal" action='/my_account/transactions/submit_billing_profile/<?=$merchant_type?>/<?=$package_id?>' method='POST'>


                        <input type="hidden" name="sku" value="<?= $pinfo['title']?>" />
                        <input type="hidden" name="sku_denomination" value="<?= $pinfo['value']?>" />
                        <input type="hidden" name="order_number" value="<?= $order_number?>" />
                        <input type="hidden" name="payment_source" value="Add Fund" />
                        <input type="hidden" name="site" value="PSYCHIC-CONTACT.COM" />

                    <div class="row">
                      <label class="col-sm-3 col-form-label">Credit Card Number</label>
                      <div class="col-sm-6">
                        <div class="form-group">
                            <input class="form-control" type='text' name='cc_num' value='<?=set_value('cc_num')?>' required>
                        </div>
                      </div>
                    </div>
          

                        <div class="row">
                            <div class="col">
                                <label class="">Expiration Month</label>
                                   <select name="exp_month" class="form-control" required >
                                        <option value="" selected disabled>Month </option>
                                        <option value="01">January</option>
                                        <option value="02">February</option>
                                        <option value="03">March</option>
                                        <option value="04">April</option>
                                        <option value="05">May</option>
                                        <option value="06">June</option>
                                        <option value="07">July</option>
                                        <option value="08">August</option>
                                        <option value="09">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>  
                            </div>
                            <div class="col">
                                <label class="">Expiration Year</label>
                                <select name="exp_year" class="form-control" required >
                                        <option value="" selected disabled>Year </option>
                                        <option value="2019">2019</option>
                                        <option value="2020">2020</option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                        <option value="2023">2023</option>
                                        <option value="2024">2024</option>
                                        <option value="2025">2025</option>
                                        <option value="2026">2026</option>
                                        <option value="2027">2027</option>
                                        <option value="2028">2028</option>
                                        <option value="2029">2029</option>
                                        <option value="2030">2030</option>
                                    </select>  
                            </div>
                        </div>


                    <div class="row">
                      <label class="col-sm-3 col-form-label">CVV Code</label>
                      <div class="col-sm-6">
                        <div class="form-group">
                            <input class="form-control" type='text' name='cvv_code' value='<?=set_value('cvv_code')?>' required>
                            <span class="bmd-help">Last 3 digits on the back of your card</span>
                        </div>
                      </div>
                    </div>
                   

                    <div class="row">
                      <label class="col-sm-3 col-form-label">Total Purchase Amount</label>
                      <div class="col-sm-6">
                        <div class="form-group">
                            <input class="form-control text-right" type='text' name='total' value='<?=$pinfo['price']?>' readonly >
                        </div>
                      </div>
                    </div>


                    
                    <div class="row">
                      <label class="col-sm-3 col-form-label">Promo/Coupon Code</label>
                      <div class="col-sm-6">
                        <div class="form-group">
                            <input class="form-control" type='text' name='coupon' value='<?=set_value('coupon')?>'>
                            <span class="bmd-help">Please don't forget to use your Promo Code if you have one.</span>

                        </div>
                      </div>
                    </div>


                    <div class="row">
                      <label class="col-sm-3 col-form-label">First name</label>
                      <div class="col-sm-6">
                        <div class="form-group">
                            <input class="form-control" type='text' name='first_name' value='<?=$user['first_name']?>' readonly>
                        </div>
                      </div>
                    </div>


                    <div class="row">
                      <label class="col-sm-3 col-form-label">Last name</label>
                      <div class="col-sm-6">
                        <div class="form-group">
                            <input class="form-control" type='text' name='last_name' value='<?=$user['last_name']?>' readonly>
                        </div>
                      </div>
                    </div>


                    <div class="row">
                      <label class="col-sm-3 col-form-label">Address</label>
                      <div class="col-sm-6">
                        <div class="form-group">
                            <input class="form-control" type='text' name='address' value='<?=set_value('address')?>' required>
                        </div>
                      </div>
                    </div>
                
                    <div class="row">
                      <label class="col-sm-3 col-form-label">City</label>
                      <div class="col-sm-6">
                        <div class="form-group">
                            <input class="form-control" type='text' name='city' value='<?=set_value('city')?>' required>
                        </div>
                      </div>
                    </div>



                    <div class="row">
                      <label class="col-sm-3 col-form-label">State/Provice</label>
                      <div class="col-sm-6">
                        <div class="form-group">
                            <input class="form-control" type='text' name='state' value='<?=set_value('state')?>' required>
                        </div>
                      </div>
                    </div>


                    <div class="row">
                      <label class="col-sm-3 col-form-label">Zip/Postal Code</label>
                      <div class="col-sm-6">
                        <div class="form-group">
                            <input class="form-control" type='text' name='zip' value='<?=set_value('zip')?>' required>
                        </div>
                      </div>
                    </div>


                  <div class="row">
                      <label class="col-sm-3 col-form-label">Country</label>
                      <div class="col-sm-6">
                        <div class="form-group">
                            <?=$this->system_vars->country_array_select_box('country', set_value('country'))?>
                        </div>
                      </div>
                    </div>

                    <h4><b>Package Information</b></h4>

                    <table class="table">
                        <thead class=" text-primary">
                            <td>Type</td>
                            <td>Title</td>
                            <td>Price</td>
                        </th>
                        <tbody>
                            <tr>
                                <td><?=$pinfo['type'] ?></td>
                                <td><?=$pinfo['title'] ?></td>
                                <td>$<?=number_format($pinfo['price'],2)?></td>
                            </tr>
                        </tbody>
                    </table>        
                        
                        <input type='submit' name='submit' value='Checkout' class='btn btn-large btn-primary'>
                                    <a href='/my_account/transactions/fund_your_account' class='btn btn-large btn-link'>Cancel</a>
                    </form>




            </div>
        </div>
    </div>
</div>    

