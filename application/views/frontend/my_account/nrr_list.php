<style>
    th, td {
        text-align: center;
    }

    tr.refunded {
        color: lightgray;
    }

    .isDisabled {
        color: currentColor;
        cursor: not-allowed;
        opacity: 0.5;
        text-decoration: none;
    }

</style>
<div class="row">
    <div class="my-container page-70-30">

        <div class="content-block-30">
            <?php $this->load->view('frontend/my_account/nav/dashboard_menu'); ?>
        </div><!-- .content-block-20 -->

        <div class="content-block-70">


            <div class="row">
                <div class="col-md-12 col-sm-7">    

                    <h2>NRRs</h2>

                    <hr />


                    <form class="form-inline" id="timeback_form" action="/my_account/chats/give_timeback/<?= $this->chatmodel->object['id'] ?>" method="POST">
                        <?php $nr = $this->nrr_model->get_nrr(null, null, $this->member->data['id']); ?>
                        <?php if (count($nr) > 0): ?>

                            <table class="table table-striped table-bordered" style="margin:15px 0 35px;">
                                <tr>
                                    <th>Type</th>
                                    <th>Time Requested (mins)</th>
                                    <th>Date</th>
                                    <th>Detail</th>
                                    <th>Delete</th>
                                </tr>
                                <?php
                                foreach ($nr as $n):
                                    $amount = $n['time_back'];

                                    ?>
                                    <tr class="<?php if ($n['refunded'] == '1') echo('refunded'); ?>">
                                        <td><?= ucwords(str_replace('_', ' ', $n['type'])) ?></td>
                                        <td><?= $amount ?> </td>
                                        <td><?= $n['date'] ?></td>
                                        <td><a href="/my_account/nrr/details/<?= $n['id'] ?>" class="btn-sm btn-primary">Details</a></td>
                                        <td>
                                            <?php if($n['refunded'] == null) { ?>
                                                <span class="btn-sm btn-danger isDisabled">Delete</span>
                                            <?php } else {?>
                                                <a href="/my_account/nrr/delete/<?= $n['id'] ?>" class="btn-sm btn-danger">Delete</a>
                                            <?php } ?>
                                        </td>
                                    </tr>

                                <?php endforeach; ?>

                            </table>

                        <?php else: ?>

                            <p>You have no NRRs.</p>

                        <?php endif; ?>

                    </form>


                </div>

            </div>

        </div><!-- .content-block-80 -->

    </div><!-- .my-container -->   
</div>


