<!-- new layout -->
<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Compose A New Message</h4>

        <div class="card-description">

            <?php if($this->session->flashdata('response')):?>
                <p class='record text-warning' > <?=$this->session->flashdata('response')?> </p>
            <?php endif?>

            <div class="material-datatables">
                    <ul class="nav nav-pills nav-pills-primary" role="tablist">
                        <li class="nav-item" <?= ($this->uri->segment('3') == '' ? " class=\"active\"" : "") ?>>
                            <a  class="nav-link" href="/my_account/messages">Inbox</a></li>
                        <li class="nav-item" <?= ($this->uri->segment('3') == 'outbox' ? " class=\"active\"" : "") ?>>
                            <a class="nav-link" href="/my_account/messages/outbox">Outbox</a></li>
                        <li class="nav-item" <?= ($this->uri->segment('3') == 'compose' ? " class=\"active\"" : "") ?>>
                            <a class="nav-link" href="/my_account/messages/compose"><span class='icon icon-comment'></span> Compose A New Message</a></li>
                    </ul>


                        <form action='/my_account/messages/compose_submit' method='POST' class="form-horizontal" enctype="multipart/form-data">

                        <input type="hidden" name="from" value="<?= $this->member->data['id']?>"/>

                      <div class='form-group'>
                            <input type='hidden' name='to' value='<?php echo $to ?>' class='input-mini'>

                            <label >Recipient  
                                <?php if ($this->uri->segment('4')) : ?>
                                    <?php echo $to_user['username'] .' ('.$to_user['first_name'].')'; ?> 
                            </label>
                                <?php else: ?>


                                    <select name='recipient' class='selectpicker' data-style="select-with-transition" multiple="multiple">
                                        <?php foreach ($users as $u) : ?>
                                            <option value='<?php echo $u['id']; ?>' <?php echo set_select('to', $u['id'], ($to == $u['id'] ? TRUE : FALSE)) ?>><?php echo $u['username'] .'  ('. $u['first_name'] . ')'; ?>                                                
                                            </option>
                                        <?php endforeach; ?>
                                    </select>

                                <?php endif; ?>
                        </div>

                        <div class='form-group'>
                            <label>Subject</label>
                                <input type='text' class='form-control' name='subject' value='<?= set_value('subject', $subject) ?>'>
                        </div>
                        <div class="form-group">
                            <label>Message</label>
                        </div>
                        <div class="form-group">
                                <textarea name='message' class='form-control' style='width:100%;height:150px;'><?= set_value('message', $message) ?></textarea>
                        </div>

                        <div class="col-lg-12" style="text-align: center">
                            <input type="file" name="attach" />
                            <input type='submit' name='submit' style="margin-left:15px;" value='Send Message' class='btn btn-large btn-primary'>
                        </div>

                    </form>


            </div>
        </div>
    </div>
</div>            


<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=e465y2qbjbdi14qglxeq486et6jfhc6d0zy21wr3akkqps42"></script>
<script type="text/javascript">
    tinymce.init({
        selector: 'textarea',
        height: 350,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor link image code",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste imagetools wordcount", 'image code textcolor colorpicker spellchecker',
        ],
        browser_spellcheck: true,
        image_dimensions: false,
        image_description: false,
        relative_urls : false,
        remove_script_host : false,
        convert_urls : true,
        toolbar: "insertfile | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor spellchecker",
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ],
        automatic_uploads: true,
        imagetools_cors_hosts: ['mydomain.com', 'otherdomain.com'],paste_data_images: true,

        file_picker_callback: function(cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');

            input.onchange = function() {
                var file = this.files[0];

                var reader = new FileReader();
                reader.onload = function () {

                    var id = 'blobid' + (new Date()).getTime();
                    var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                    var base64 = reader.result.split(',')[1];
                    var blobInfo = blobCache.create(id, file, base64);
                    blobCache.add(blobInfo);
                    cb(blobInfo.blobUri(), { title: file.name });
                };
                reader.readAsDataURL(file);
            };

            input.click();
        }
    });
</script>
