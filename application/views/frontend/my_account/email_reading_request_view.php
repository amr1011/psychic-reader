<link rel="stylesheet" href="/media/javascript/jqui/css/overcast/jquery-ui-1.8.16.custom.css" />
<link rel="stylesheet" href="/media/javascript/datetime/jquery-ui-timepicker-addon.css" />
<style>

    form.pull-left {
        width: 75%;
    }

    .title {
        background: #f5f5f5;
        border: 1px solid #e3e3e3;
        border-radius: 5px;
        padding: 15px;
        margin-top: 40px;
        margin-bottom: 20px;
        color: #4c4b4b;
    }

    .content {
        border: 1px solid black;
        border-radius: 5px;
        padding: 30px 30px 30px 100px;
    }

    input, select, textarea {
        width: 350px;
        border-radius: 4px;
        background: #f5f5f5;
        padding-left: 10px;
    }

    input#birth-time {
        width: 100px;
        margin-right: 40px;
    }
    input[name='birth-time-ap'] {
        width: 20px;
        margin-right: 10px;
    }
    input#name-numerology, #birth-date-numerology {
        width: 228px;
        margin-top: 5px;
    }

</style>

<div class='content_area'>

    <div align='center' class="title">
        <h2>Email Reading Order Form</h2>
        <p style='padding:5px 0 0;'>Using this form, you will be able to place your order for an Email Reading with us.</p>
        <p style='padding:5px 0 0;'>All the payment information you enter on this form will be encrypted and can only be read by our secure & encrypted payment gateway system.</p>
        <p>Your privacy is assured.</p>
    </div>

    <div class="content">
        <h3>Contact Information</h3>
        <h4>(Fields marked with * are required)</h4>
        <br/>
        <form class='pull-left' action='/my_account/email_readings/submit' method='POST'>

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Please select the type of Reading you would like to have from the following drop-down menu. *:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <select name="reading-type" required>
                        <option value="">Please choose from this menu</option>
                        <option value="special" >SPECIAL</option>
                        <option value="1">1 question - 29.95</option>
                        <option value="2">2 questions - 39.95</option>
                        <option value="3">3 questions - 49.95</option>
                        <option value="4">4 questions - 59.95</option>
                        <option value="5">5 questions - 69.95</option>
                    </select>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Question 1 :</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" class="question" name="question1" value="" disabled/></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Question 2 :</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" class="question" name="question2" value="" disabled/></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Question 3:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" class="question"name="question3" value="" disabled/></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Question 4:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" class="question" name="question4" value="" disabled/></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Question 5:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" class="question" name="question5" value="" disabled/></div>
            </div>
            <hr/>

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Your Name *:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <input type="text" name="name" value="<?=$this->member->data['first_name']?> <?=$this->member->data['last_name']?>" required/>
                    <input type="hidden" name="first-name" value="<?=$this->member->data['first_name']?>"/>
                    <input type="hidden" name="last-name" value="<?=$this->member->data['last_name']?>"/>
                    <input type="hidden" name="member-id" value="<?=$this->member->data['id']?>"/>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>UserName *:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="username" value="<?=$this->member->data['username']?>" required/></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Street Address(1):</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="address-1" value="" /></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Street Address(2):</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="address-2" value="" /></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Country:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <select name="country">
                        <optgroup>
                            <option value="">please select</option>
                            <option value="US">United States</option>
                            <option value="CA">Canada</option>
                            <option value="GB">United Kingdom</option>
                            <option value="AU">Australia</option>
                            <option value="NZ">New Zealand</option>
                        </optgroup>
                        <optgroup label="______________________________">
                            <?php foreach($countries as $country) { ?>
                                <option value="<?php echo$country['code']?>"><?= $country['name']?></option>
                            <?php } ?>
                        </optgroup>
                    </select>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>State / Province:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><select name="state"><option value="">please select</option></select></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Town / City:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><select name="city"><option value="">please select</option></select></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Zip Code:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="zip-code" value="" /></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Phone Number:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="phone-number" value="" /></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Email Address *:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="email" value="<?=$this->member->data['email']?>" required/></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Please re-enter your Email *:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="reenter-email" value="" required/></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Gender *:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><div style="display: inline-flex;">
                        Male&nbsp;<input type="radio" name="gender" value="Male" style="display: block; width: 30px;"/>
                        Female&nbsp;<input type="radio" name="gender" value="Female" style="display: block; width: 30px;"/>
                        Other&nbsp;<input type="radio" name="gender" value="Other" style="display: block; width: 30px;"/>
                    </div></div>
            </div>
            <br/>

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Your Date of Birth *:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="date-birth" id="birth-day" value="<?=date("m-d-Y", strtotime($this->member->data['dob']))?>" required/></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>If ordering an Astrology Reading - Very Important! The time you were born (approx):</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><div style="display: inline-flex;"><input id="birth-time" type="text" name="time-birth"/>AM&nbsp;<input type="radio" name="birth-time-ap" value="AM" style="display: block;" checked="checked"/>
                        PM&nbsp;<input type="radio" name="birth-time-ap" value="PM" style="display: block;"/>
                        Unknown&nbsp;<input type="radio" name="birth-time-ap" value="UN" style="display: block;"/></div></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>If ordering an Astrology Reading - Very Important!: Your place of birth (City, State, Country):</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="place-birth" value="" /></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>If ordering a Numerology Reading Please Enter the name and/or
                        Date of Birth you wish the Reading done for (Nicknames or Married names are ok):</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <span style="margin-right: 70px;">Name:</span> <input type="text" name="name-numerology" id="name-numerology" value="" placeholder="Your Numerology Name"/><br/>
                    <span style="margin-right: 22px;">Date of Birth:</span> <input type="text" name="date-numerology" id="birth-date-numerology" />
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>In one or two words only, Please enter the topic that concerns you the most (optional)
                        (I.E.: Love, Relationship, Health, Career, $$$$, etc.):</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="topic-word" value="" /></div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>If you would like to choose your own personal Psychic, Reader, Astrologer,
                        or Numerologist from the list of our Readers, please check the button at the right of this window and type in
                        their name here: (If you leave this black, we will choose the Reader for you) * :</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <select name="reader-id" required>
                        <?php foreach($readers as $reader) {?>
                            <option value="<?= $reader['id']?>"><?= $reader['username']?></option>
                        <?php }?>
                    </select>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Special Instructions:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><textarea name="special-instruction"></textarea></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Please enter any additional information for the Reader to review
                        before starting your E-Reading <br/>
                        If you would like to order a Reader's "SPECIAL" E-Reading
                        Please visit their Profile: to review their offer (you can order from there) :</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><textarea name="additional-info"></textarea></div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Payment Information *:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <select name="payment-info" required>
                        <option value="">please select</option>
                        <option value="cc">Credit Card</option>
                        <option value="pp">Pay Pal</option>
                    </select>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>First Name on Card:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="f-name-card"/></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Last Name on Card:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="l-name-card"/></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Billing Address:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="billing-address"/></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Billing City:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="billing-city"/></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Billing State:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="billing-state"/></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Billing Zip code:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="billing-zip-code"/></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Billing Country:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <select name="billing-country">
                        <option value="">please select</option>
                        <?php foreach($countries as $country) { ?>
                            <option value="<?php echo$country['code']?>"><?= $country['name']?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Card Number:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="card-number"/></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Card CVV Code:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="card-cvv-code"/></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Card Expiration:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <select name="card-expiration" style="margin-top: 5px;">
                        <option value="">please select</option>
                        <?php foreach($expiration_year as $year) {?>
                            <option value="<?=$year;?>"><?=$year;?></option>
                        <?php }?>
                    </select>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12"><b>* NOTICE: Any individual who chooses to submit ",Fraudulent",
                        Credit Card or Checking Account information will be prosecuted to the full extent allowable by law.
                        Please click the "SEND EMAIL READING ORDER", button below to submit your order.
                        Processing might take up to a minute or so.
                    </b></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12"><b style="color: floralwhite; font-size: 20px;">** DO NOT HIT YOUR BACK BUTTON OR THE SUBMIT BUTTON AGAIN
                        OR IT WILL DOUBLE CHARGE YOU! **
                    </b></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <input type="submit" value="SEND EMAIL READING ORDER" class="btn btn-success"/>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <button id="clear-order-form" style="border-radius: 4px; font-size: 14px;" class="btn btn-primary">CLEAR ORDER FORM</button>
                </div>
            </div>
            <br/>

        </form>

        <div class='clearfix'></div>
    </div>
</div>

<script src='/media/javascript/jqui/jquery-ui-1.8.16.custom.min.js'></script>
<script src='/media/javascript/datetime/jquery-ui-timepicker-addon.js'></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

<script>
    $(document).ready(function()
    {
        $('#birth-day').datepicker ({
            ampm: true,
            separator: ' @ ',
            dateFormat: "mm-dd-yy",
            changeYear: true,
            yearRange: "-70:+0"
        });

        if("<?= $this->member->data['gender']; ?>" == 'Male') {$('input:radio[name=gender]')[0].checked = true;}
        if("<?= $this->member->data['gender']; ?>" == 'Female') {$('input:radio[name=gender]')[1].checked = true;}
        if("<?= $this->member->data['gender']; ?>" == 'Unknown') {$('input:radio[name=gender]')[2].checked = true;}

        $('#birth-date-numerology').datepicker({
            ampm: true,
            separator: ' @ ',
            dateFormat: "mm-dd-yy",
            changeYear: true,
            yearRange: "-70:+0"
        });

        var check_fields = function check_fields() {

            $('input[type="text"]').each(function() {

                if ($.trim($(this).val()) == '') {
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#DDD"
                    });
                } else {
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });

            $('textarea').each(function() {
                if ($.trim($(this).val()) == '') {
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#DDD"
                    });
                } else {
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
        }

        check_fields();

        $('input').keyup(function () {
            if($(this).val() == '') {
                $(this).css({
                    "border": "1px solid red",
                    "background": "#DDD"
                });
            } else {
                $(this).css({
                    "border": "",
                    "background": ""
                });
            }
        });

        $('textarea').keyup(function () {
            if($(this).val() == '') {
                $(this).css({
                    "border": "1px solid red",
                    "background": "#DDD"
                });
            } else {
                $(this).css({
                    "border": "",
                    "background": ""
                });
            }
        });


        $("input[name='deadline']").click(function() {

            if($(this).is(':checked')) {
                $('#div_deadline').show();
            }
            else {
                $('#div_deadline').hide();
            }
        });

        $("button#clear-order-form").click(function(){
            $('input[type=text]').each(function () {
                $(this).val('');
            });
            check_fields();
        });

        $("button#clear-order-form").click(function(){
            $('textarea').each(function () {
                $(this).val('');
            });
            check_fields();
        });

        $('select[name="reading-type"]').change(function () {

            switch ($(this).val()) {
                case 'special':
                    $('input.question').prop('disabled', 'true').css({'background':'darkgray', 'border':'1px solid black'});
                    break;
                case '1':
                    $('input.question').removeAttr('disabled').css({'background':'rgb(221,221,221)', 'border':'1px solid red'});
                    $('input[name="question2"]').prop('disabled', 'true').css({'background':'darkgray', 'border':'1px solid black'});
                    $('input[name="question3"]').prop('disabled', 'true').css({'background':'darkgray', 'border':'1px solid black'});
                    $('input[name="question4"]').prop('disabled', 'true').css({'background':'darkgray', 'border':'1px solid black'});
                    $('input[name="question5"]').prop('disabled', 'true').css({'background':'darkgray', 'border':'1px solid black'});
                    break;
                case '2':
                    $('input.question').removeAttr('disabled').css({'background':'rgb(221,221,221)', 'border':'1px solid red'});
                    $('input[name="question3"]').prop('disabled', 'true').css({'background':'darkgray', 'border':'1px solid black'});
                    $('input[name="question4"]').prop('disabled', 'true').css({'background':'darkgray', 'border':'1px solid black'});
                    $('input[name="question5"]').prop('disabled', 'true').css({'background':'darkgray', 'border':'1px solid black'});
                    break;
                case '3':
                    $('input.question').removeAttr('disabled').css({'background':'rgb(221,221,221)', 'border':'1px solid red'});
                    $('input[name="question4"]').prop('disabled', 'true').css({'background':'darkgray', 'border':'1px solid black'});
                    $('input[name="question5"]').prop('disabled', 'true').css({'background':'darkgray', 'border':'1px solid black'});
                    break;
                case '4':
                    $('input.question').removeAttr('disabled').css({'background':'rgb(221,221,221)', 'border':'1px solid red'});
                    $('input[name="question5"]').prop('disabled', 'true').css({'background':'darkgray', 'border':'1px solid black'});
                    break;
                case '5':
                    $('input.question').removeAttr('disabled').css({'background':'rgb(221,221,221)', 'border':'1px solid red'});
                    break;
            }
        });
        $('select[name="payment-info"]').change(function () {

           if($(this).val() == 'pp') {
               $('input[name="f-name-card"]').prop('disabled', 'true').css({'background':'darkgray', 'border':'1px solid black'});
               $('input[name="l-name-card"]').prop('disabled', 'true').css({'background':'darkgray', 'border':'1px solid black'});
               $('input[name="billing-address"]').prop('disabled', 'true').css({'background':'darkgray', 'border':'1px solid black'});
               $('input[name="billing-city"]').prop('disabled', 'true').css({'background':'darkgray', 'border':'1px solid black'});
               $('input[name="billing-state"]').prop('disabled', 'true').css({'background':'darkgray', 'border':'1px solid black'});
               $('input[name="billing-zip-code"]').prop('disabled', 'true').css({'background':'darkgray', 'border':'1px solid black'});
               $('input[name="card-number"]').prop('disabled', 'true').css({'background':'darkgray', 'border':'1px solid black'});
               $('input[name="card-cvv-code"]').prop('disabled', 'true').css({'background':'darkgray', 'border':'1px solid black'});
           }
            if($(this).val() == 'cc') {
                $('input[name="f-name-card"]').removeAttr('disabled').css({'background':'rgb(221,221,221)', 'border':'1px solid red'});
                $('input[name="l-name-card"]').removeAttr('disabled').css({'background':'rgb(221,221,221)', 'border':'1px solid red'});
                $('input[name="billing-address"]').removeAttr('disabled').css({'background':'rgb(221,221,221)', 'border':'1px solid red'});
                $('input[name="billing-city"]').removeAttr('disabled').css({'background':'rgb(221,221,221)', 'border':'1px solid red'});
                $('input[name="billing-state"]').removeAttr('disabled').css({'background':'rgb(221,221,221)', 'border':'1px solid red'});
                $('input[name="billing-zip-code"]').removeAttr('disabled').css({'background':'rgb(221,221,221)', 'border':'1px solid red'});
                $('input[name="card-number"]').removeAttr('disabled').css({'background':'rgb(221,221,221)', 'border':'1px solid red'});
                $('input[name="card-cvv-code"]').removeAttr('disabled').css({'background':'rgb(221,221,221)', 'border':'1px solid red'});
            }
        });

        $('select[name="country"]').change(function () {
            $('select[name="state"]')
                .find('option')
                .remove();

            $.ajax({

                'type': "POST",
                'url': "/my_account/main/get_state",
                'data': { country_code: $(this).val()},
                success: function (data) {

                    obj = JSON.parse(data);
                    console.log(obj.states);
                    let states = obj.states;

                    setTimeout(function () {

                        for(let i=0; i < states.length; i++) {

                            var o = new Option(states[i]['id'], states[i]['id']);

                            $(o).html(states[i]['name']);
                            $('select[name="state"]').append(o);
                        }
                    }, 1000);
                }
            });
        });

        $('select[name="state"]').change(function () {

            $('select[name="city"]')
                .find('option')
                .remove();

            $.ajax({

                'type': "POST",
                'url': "/my_account/main/get_city",
                'data': { state_id: $(this).val()},
                success: function (data) {

                    obj = JSON.parse(data);
                    console.log(obj.cities);
                    let cities = obj.cities;

                    setTimeout(function () {

                        for(let i=0; i < cities.length; i++) {

                            var p = new Option(cities[i]['id'], cities[i]['id']);

                            $(p).html(cities[i]['name']);
                            $('select[name="city"]').append(p);
                        }
                    }, 1000);
                }
            });
        })
    });
</script>