<style>
    input[type='radio'] {     
        display: inline-block !important;
        margin-right: 5px;
    }
</style>

<div class="row">
    <div class="my-container page-70-30">
        <div class="content-block-30">
            <?php $this->load->view('frontend/my_account/nav/dashboard_menu'); ?>
        </div><!-- .content-block-20 -->

        <?php if($this->session->flashdata('response')):?>
            <p class='record' style="display: none;"> <?=$this->session->flashdata('response')?> </p>
        <?endif?>

        <?php if($this->session->flashdata('error')):?>
            <p class='record-error' style="display: none;"> <?=$this->session->flashdata('error')?> </p>
        <?endif?>

        <div class="content-block-70">
            <div class="row">
                <div class="col-md-12 col-sm-7">
                    <h2>Update My Account</h2>
<!--                    <h3>credit: $--><?//= $credit?><!-- / balance: $--><?//= $balance?><!--</h3>-->
                    <h4>Chat Time: <span
                                style='color:#666;'><?= gmdate("H:i:s", ($this->member_funds->minute_balance() * 60)) ?></span>
                    </h4>
                    <h4>Free Mins: <span
                                style='color:#666;'><?= gmdate("H:i:s", ($this->member_funds->free_minute_balance() * 60)) ?></span>
                    </h4>
                    <h4>Emails Funds: <span
                                style='color:#666;'>$ <?= number_format($this->member_funds->email_balance(), 2) ?></span>
                    </h4>
                    <hr />

                    <?php echo validation_errors(); ?>
                    <form action='/my_account/account/save_account' method='POST' enctype="multipart/form-data" class="well form-horizontal">
                        <div class="form-group">
                            <label class="col-md-4 control-label"><b>Email Address</b></label>
                            <div class="col-md-8">
                                <input type="text" name="email" class="form-control" value="<?= $this->member->data['email'] ?>" required/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label"><b>Username</b></label>
                            <div class="col-md-8">
                                <?= $this->member->data['username'] ?>
                            </div>
                        </div>
                        <div style='color:#C0C0C0; text-align: center'>Leave password fields blank to keep your current password</div>
                        <div class="form-group">
                            <label class="col-md-4 control-label"><b>Choose A New Password</b></label>
                            <div class="col-md-8">
                                <input type='password' name='password' class='form-control' value='<?= set_value('password') ?>'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label"><b>Re-Type New Password</b></label>
                            <div class="col-md-8">
                                <input type='password' class='form-control' name='password2' class='form-control' value='<?= set_value('password2') ?>'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label"><b>First Name</b></label>
                            <div class="col-md-8">
                                <?= set_value('first_name', $this->member->data['first_name']) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label"><b>Last Name</b></label>
                            <div class="col-md-8">
                                <?= set_value('last_name', $this->member->data['last_name']) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label"><b>Gender</b></label>
                            <div class="col-md-8" style="padding-top:7px;">
                                <input type='radio' name='gender' value='Male' <?= set_radio('gender', 'Male', ($this->member->data['gender'] == 'Male' ? TRUE : FALSE)) ?>> Male &nbsp; &nbsp; <input type='radio'  name='gender' value='Female' <?= set_radio('gender', 'Female', ($this->member->data['gender'] == 'Female' ? TRUE : FALSE)) ?>> Female   &nbsp;&nbsp;&nbsp;&nbsp; <input type='radio'  name='gender' value='Other' <?= set_radio('gender', 'Other', ($this->member->data['gender'] == 'Other' ? TRUE : FALSE)) ?>> Other
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label"><b>Date of Birth</b></label>
                            <div class="col-md-8">
                                <?php
                                $dob_year = $dob_month = $dob_day = '';
                                if (isset($this->member->data['dob'])):
                                    list($dob_year, $dob_month, $dob_day) = explode("-", $this->member->data['dob']);
                                endif;
                                echo $this->system_vars->dob_custom('dob', set_value('dob_month', $dob_month), set_value('dob_day', $dob_day), set_value('dob_year', $dob_year));
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label"><b>Country</b></label>
                            <div class="col-md-8">
                                <?= $this->system_vars->country_array_select_box('country', set_value('country', $this->member->data['country'])) ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label"><b>Upload Profile Image</b></label>
                            <div class="col-md-8">
                                <img src='<?= $this->member->data['profile_image'] ?>' style='border:solid 1px #000; width: 130px;'>

                                <div style='padding:10px 0;color:#C0C0C0'>To keep your current profile image, leave the field below blank3</div>

                                <input type='file' style="max-width:200px;" name='profile_image'>
                            </div>
                        </div>
                        <input type='submit' name='submit' value='Save My Profile' class='btn btn-large btn-warning'>	
                    </form>

                </div>
            </div>
        </div><!-- .content-block-80 -->
    </div><!-- .my-container -->   
</div>

<script>
    $(document).ready(function () {

        if($('p.record').html()) {
            $.notify($('p.record').html(), {className:"success",align:"center", verticalAlign:"top", hideDuration: 700, autoHideDelay: 5000,});
        }
        if($('p.record-error').html()) {
            $.notify($('p.record-error').html(), {className:"error",align:"center", verticalAlign:"top", hideDuration: 700, autoHideDelay: 5000,});
        }
    })
</script>