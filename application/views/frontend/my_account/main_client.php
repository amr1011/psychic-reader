<div class="row">

       
        <div class="col-md-12 d-flex align-items-stretch">
              <div class="card card-profile">
                <div class="card-avatar">
                  <a href="/my_account/account">
                    <img class="img" src="<?= $this->member->data['profile_image'] ?>">
                  </a>
                </div>
                <div class="card-body">
                  <h6 class="card-category text-gray"><?= $this->member->data['username'] ?></h6>
                  <h4 class="card-title"><?= $this->member->data['first_name'] ?> <?= $this->member->data['last_name'] ?></h4>
                  <p class="card-description">
                    Welcome to Psychic Contact
                  </p>
                    <a href="/my_account/transactions/fund_your_account" class="btn btn-rose btn-round">Fund My Account</a>
                </div>
              </div>
          </div>
        


        <div class="col-lg-3 col-md-6 col-sm-6 d-flex align-items-stretch">
            <div class="card card-stats">
              <div class="card-header  card-header-warning card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">chat</i>
                </div>
                <p class="card-category">Chat Available Time</p>
                <h3 class="card-title"><?= gmdate("H:i:s", ($this->member_funds->minute_balance() * 60)) ?></h3>
              </div>
              <div class="card-footer">
              </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6 d-flex align-items-stretch">
            <div class="card card-stats ">
              <div class="card-header  card-header-success card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">chat</i>
                </div>
                <p class="card-category">Free Chat Available Time</p>
                <h3 class="card-title"><?= gmdate("H:i:s", ($this->member_funds->free_minute_balance() * 60)) ?></h3>
              </div>
              <div class="card-footer">
              </div>
            </div>
        </div>

         <div class="col-lg-3 col-md-6 col-sm-6 d-flex align-items-stretch">
            <div class="card card-stats">
              <div class="card-header card-header-rose card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">email</i>
                </div>
                <p class="card-category">Email Balance</p>
                <h3 class="card-title"><?= number_format($this->member_funds->email_balance(), 2) ?></h3>
              </div>
              <div class="card-footer">
              </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6 d-flex align-items-stretch">
            <div class="card card-stats">
              <div class="card-header card-header-primary card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">phone</i>
                </div>
                <p class="card-category">Phone Call Balance</p>
                <h3 class="card-title"><?= gmdate("H:i:s", ($this->member_funds->phone_balance() * 60)) ?></h3>
              </div>
              <div class="card-footer">
              </div>
            </div>
        </div>

</div>




<?php if ($this->session->flashdata('testimonial_submit')): ?>
    <p class='record' style="display: block;"><?= $this->session->flashdata('testimonial_submit') ?> </p>
<?php endif; ?>

<?php

if ($this->session->userdata('return_chat_id')) {
    echo "
    <script>
        alert('" . $this->session->userdata('mins_purchased') . " mins purchase Successful! You are being transferred back to the active chat';)
        window.close();
    </script>";
}

?>





