<style>
    div.content-notification-folder { padding-right: 20px;margin-top: 20px;border-right: solid 1px #cecbcb; }
    span.folder-icon { font-size:20px; color: #ece12c;}
    span.trash-icon {font-size:20px;color: #e01e1e;}
    span.folder-name {font-size: 15px;}
    div.notification-folder, .notification-trash-folder, .notification-inbox-folder {cursor: pointer;padding-bottom: 5px; }
    .disabled { color: #cecbcb !important;color: currentColor;cursor: not-allowed;text-decoration: none; }
    .trash-folder { font-size: 15px; }
    tr.unread td { color: red; }
    tr.unread a { color: red; }
</style>


<!-- new layout -->
<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">System Notification Board </h4>

        <div class="card-description">

            <?php if($this->session->flashdata('response')):?>
                <p class='record text-warning' > <?=$this->session->flashdata('response')?> </p>
            <?php endif?>

            <div class="material-datatables">


                <ul class="nav nav-pills nav-pills-primary" role="tablist">
                        <li  <?= ($this->uri->segment('3') == '' ? " class=\"nav-item active\"" : "") ?>>
                            <a href="/my_account/system_email"><span class="fa fa-user"></span> Notification Board (NoBo)</a></li>                        
                        <li class="nav-item" <?= ($this->uri->segment('3') == 'email_setting' ? " class=\" nav-item active\"" : "") ?>>
                            <a href="/my_account/system_email/email_setting"><span class='fa fa-gear'></span> Setting</a>
                        </li>
                </ul>
                <div class="content-block-60" style="display: inline-flex; width: 100%;">
                        <div class="content-notification-folder" style="width: 35%;">
                            <div class="notification-folder inbox-folder">
                                <span class="fa fa-folder folder-icon enabled"><input type="hidden" class="folder-status" value="0"></span>
                                <span class="folder-name enabled">Inbox Folder</span>
                            </div>
                            <div class="main-folders" style="display: none;">
                                <?php foreach($notifications as $notification) {?>
                                    <?php if($notification['email_type'] == 2 || $notification['email_type'] == 4) : ?>
                                        <div class="notification-folder message-folder-disabled">
                                    <?php else: ?>
                                            <div class="notification-folder message-folder">
                                    <?php endif;?>
                                        <?php if($notification['email_type'] == 2 || $notification['email_type'] == 4) {?>
                                            <span class="fa fa-folder folder-icon disabled"></span>
                                            <span class="folder-name disabled email-name" style="display: none;"><?= $notification['name']?></span>
                                            <span class="folder-name disabled show-folder-name"></span>
                                            <i class="fa fa-trash disabled"></i>
                                        <?php } else {?>
                                            <span class="fa fa-folder folder-icon enabled"><input type="hidden" class="folder-status" value="0"></span>
                                            <span class="folder-name enabled email-name" style="display: none;"><?= $notification['name']?></span>
                                            <span class="folder-name enabled show-folder-name"></span>
                                        <?php }?>
                                    </div>
                                <?php }?>
                            </div>
                            <div class="notification-trash-folder">
                                <span class="fa fa-trash trash-icon enable"></span>
                                <span class="trash-folder enable">Trash</span>
                            </div>
                        </div>
                        <div class="content-notification-messages" style="width: 65%;">
                            <div class="box-message">

                            </div>
                        </div>
                </div>


            </div>
        </div>
    </div>
</div>    
