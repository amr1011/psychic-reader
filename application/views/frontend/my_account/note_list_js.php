<script>
    $(document).ready(function() {

        $(".btn-save").click(function() {

            $.ajax({
                'type': "POST",
                'url': "/chat/chatInterface/save_note",
                'data': { reader_id : $(this).parent().find('.reader-id').text(), client_id: $(this).parent().find('.client-id').text(),
                            client_username: $(this).parent().find('.client-username').text(),
                            client_firstname: $(this).parent().find('.client-firstname').text(), client_lastname: $(this).parent().find('.client-lastname').text(), note: $(this).parent().parent().find('.contactlist-formbox-body').find('.reader-note').val()},
                success: function (data) {
                    obj = JSON.parse(data);
                }
            });
        });

        $('[data-toggle="tooltip"]').tooltip();   

        $(".contactlist-formbox-head").click(function () {

            $(this).parent().find(".contactlist-formbox-body").slideToggle(300);
        });
    })
</script>