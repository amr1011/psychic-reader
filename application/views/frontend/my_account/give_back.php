<?php
if (!isset($ts))
{
    $ts = time();
}
?>


<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Sunday AM Giveback</h4>

        <div class="card-description">
            <div class="material-datatables">

                <div class="row">


                        <div class="form-group col-xs-12 col-sm-12 col-md-8 col-lg-8">
                            <h4>Choose Client Type</h4>
                        </div>
                        <div class="form-group col-xs-12 col-sm-12 col-md-8 col-lg-8">
                            <label for="reader-pause-timer">Pause Timer</label>
                            <select name="reader-pause-timer" id="reader-pause-timer" class="form-control" required="required">
                                <option value="0" selected="true">Select the “Client Type” </option>
                                <option value="1"> New Clients </option>
                                <option value="2"> Repeat Clients </option>
                                <option value="3"> Favorite Clients </option>
                                <option value="4"> All Clients </option>
                                <option value="5"> Random </option>
                                <option value="6" style="color:red;"> NO GIVEBACK TODAY </option>
                            </select>
                        </div>
                        <div class="form-group col-xs-12 col-sm-12 col-md-8 col-lg-8">
                            <label for="reader-pause-timer-size">Free Time Range</label>
                            <select name="reader-pause-timer-size" id="reader-pause-timer-size" class="form-control" required="required">
                                <option value="0" selected="true">Select the “Client Type” </option>
                                <option value="5"> 5 Minutes </option>
                                <option value="10"> 10 Minutes </option>
                                <option value="15"> 15 Minutes </option>
                                <option value="20"> 20 Minutes </option>
                                <option value="25"> 25 Minutes </option>
                                <option value="30"> 30 Minutes </option>
                            </select>
                        </div>    
                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6" id="repeat-client" style="display: none;">
                                <label for="repeat-reader-select" style="color: red;">*Select Repeat Clients</label>
                                <select name="repeat-reader-select" id="select-repeat-client" multiple="multiple">                                
                                </select>
                            </div>    
                        </div>
       
     

                        <div class="row">
                            <div class="col-sm-12">
                            <p>Choose A Day of The Week</p>
                                <div class="form-group">
                                    <input type="radio" name="week" id="sun" value="0" checked><span style="color: red;"> Sun</span>
                                    <input type="radio" name="week" id="mon" value="1"> Mon 
                                    <input type="radio" name="week" id="tue" value="2"> Tue 
                                    <input type="radio" name="week" id="wed" value="3"> Wed 
                                    <input type="radio" name="week" id="thu" value="4"> Thu 
                                    <input type="radio" name="week" id="fri" value="5"> Fri 
                                    <input type="radio" name="week" id="sat" value="6"> Sat 
                                    <input type="radio" name="week" id="any" value="7"><span style="color: red;">Any Day of Week</span>
                                </div>
                            </div>
                        </div>         
                        
                           
                        <input type="hidden" value="0" id="week-day">

                        <div class="form-group col-xs-12 col-sm-12 col-md-8 col-lg-8" >
                            <button id="save-give-back" class="btn btn-primary" >SAVE</button>
                        </div>   

                        <div id="box-cancel" style="display: none;">
                        <h4 style="color: #BBB">Select Issued Clients To Cancel</h4><br>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                    <label for="select-cancel" style="color: red;">*Select Clients To Cancel</label>
                                    <select name="select-cancel" id="select-cancel-client" multiple="multiple">
                                    </select><br>
                                </div>
                                <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4" style="text-align: center;">
                                    <button id="btn-save-cancel" class="btn btn-primary">Save Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
        </div>
    </div>
</div>

<!-- 9-12 -->
<?php $pause_timer_type = $reader['pause_timer']; ?>
<?php $pause_timer_size = $reader['pause_timer_size']; ?>
<?php $pause_timer_switch = $reader['pause_timer_flag']; ?>
<?php $mem_id = $reader['id']; ?>
