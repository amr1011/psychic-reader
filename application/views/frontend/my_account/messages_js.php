<!-- new layout -->
<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Compose A New Message</h4>

        <div class="card-description">

            <?php if($this->session->flashdata('response')):?>
                <p class='record text-warning' > <?=$this->session->flashdata('response')?> </p>
            <?php endif?>

            <div class="material-datatables">
                    <ul class="nav nav-pills nav-pills-primary" role="tablist">
                        <li class="nav-item" <?= ($this->uri->segment('3') == '' ? " class=\"active\"" : "") ?>>
                            <a  class="nav-link" href="/my_account/messages">Inbox</a></li>
                        <li class="nav-item" <?= ($this->uri->segment('3') == 'outbox' ? " class=\"active\"" : "") ?>>
                            <a class="nav-link" href="/my_account/messages/outbox">Outbox</a></li>
                        <li class="nav-item" <?= ($this->uri->segment('3') == 'compose' ? " class=\"active\"" : "") ?>>
                            <a class="nav-link" href="/my_account/messages/compose"><span class='icon icon-comment'></span> Compose A New Message</a></li>
                    </ul>

                    <div class="content-notification-folder col-md-4">
                          <div class="notification-folder admin-folder">
                                <span class="fa fa-folder folder-icon enabled"><input type="hidden" class="folder-status" value="0"></span>
                                <span class="folder-name enabled">Administrator</span>
                                <?php if($unread_admin['unread_admin'] > 0) {?>
                                    <span class="unread enabled"><?= $unread_admin['unread_admin'];?></span>
                                <?php }?>
                            </div>
                             <div class="main-folders">
                                <?php foreach($senders as $sender) {?>
                                        <?php if($this->member->data['member_type'] == 'reader') {?>
                                            <?php if($sender['banned'] && $sender['banned'] == '1') {?>
                                                <div class="notification-folder message-folder disabled">
                                                    <span class="fa fa-folder folder-icon disabled"></span>
                                                    <span class="folder-name disabled sender-id" style="display: none;"><?= $sender['sender_id']?></span>
                                                    <span class="folder-name disabled show-folder-name"><?= $sender['username'] .' ('. $sender['first_name'] .')'?></span>
                                                </div>
                                            <?php } else {?>
                                                <div class="notification-folder message-folder enabled">
                                                    <span class="fa fa-folder folder-icon enabled text-"><input type="hidden" class="folder-status" value="0"></span>
                                                    <span class="folder-name enabled sender-id" style="display: none;"><?= $sender['sender_id']?></span>
                                                    <span class="folder-name enabled show-folder-name"><?= $sender['username'] .' ('. $sender['first_name'] .')'?></span>
                                                    <?php if($sender['unread'] > 0) {?>
                                                        <span class="unread enabled"><?= $sender['unread'];?></span>
                                                    <?php }?>
                                                </div>
                                            <?php }?>
                                        <?php } else {?>
                                            <div class="notification-folder message-folder enabled">
                                                <span class="fa fa-folder folder-icon text-warning enabled"><input type="hidden" class="folder-status" value="0"></span>
                                                <span class="folder-name enabled sender-id" style="display: none;"><?= $sender['sender_id']?></span>
                                                <span class="folder-name enabled show-folder-name"><?= $sender['username'] .' ('. $sender['first_name'] .')'?></span>
                                                <?php if($sender['unread'] > 0) {?>
                                                    <span class="unread enabled"><?= $sender['unread'];?></span>
                                                <?php }?>
                                            </div>
                                        <?php }?>
                                <?php }?>
                            </div>


                    </div>

                     <div class="content-notification-messages col-md-6" >
                            <div class="box-message"></div>
                        </div>
                  

            </div>
        </div>
    </div>
</div>            






<script>
    function insertMessage(messages) {
        console.log('messages'+messages);
        let view = "<div class='table-responsive'><table width='80%' cellPadding='0' cellSpacing='0' class='table table-striped table-hover'>";
        var member = null;
        for(let i=0; i<messages.length; i++) {

            let class_name = 'read';
            if(messages[i]['read'] == 0) {
                class_name = 'unread';
            }

            if(class_name == 'read')
            {
                view = view + "<tr class="+class_name+">" + "<td width='150'>" + messages[i]['datetime'] + " EST</td>"
                    + "<td>" + messages[i]['subject'] + "</td>"
                    + "<td width='50' align='center'><a href='/my_account/messages/view/"+ messages[i]['id'] + "' class='btn btn-primary'>View</a></td>"
                    + "<td width='50' align='center'><a href='/my_account/messages/delete/" + messages[i]['id'] + "' onClick=\"Javascript:return confirm('Are you sure you want to delete this message?');\" class='btn btn-danger'>Delete</a></td>"
                    + "</tr>";
            } else if (class_name == 'unread')
            {
                view = view + "<tr class="+class_name+">" + "<td width='150'>" + messages[i]['datetime'] + " EST</td>"
                    + "<td>" + messages[i]['subject'] + "</td>"
                    + "<td width='50' align='center'><a href='/my_account/messages/view/"+ messages[i]['id'] + "' class='btn btn-primary'>View</a></td>"
                    + "<td width='50' align='center'><a class='btn btn-danger'>Delete</a></td>"
                    + "</tr>";
            }


        }
        view = view + "</table></ div>";
        return view;
    }

    $(document).ready(function () {

        var messages;
        var admin_messages;
        var system_messages;

        $.ajax({
            'type': "POST",
            'url': "/my_account/messages/getSystemMessages",
            'data': {},
            success: function (data) {

                let obj = JSON.parse(data);
                system_messages = obj.messages;

                setTimeout(function () {

                    let view = insertMessage(system_messages);
                    $(".box-message").append(view);
                }, 500);

            }
        });

        function getData(sender_id) {
            $.ajax({
                'type': "POST",
                'url': "/my_account/messages/getMessages",
                'data': { user_id : sender_id},
                success: function (data) {

                    let obj = JSON.parse(data);
                    messages = obj.messages;
                    console.log(messages);
                }
            });
        }

        function getAdminData() {
            $.ajax({
                'type': "POST",
                'url': "/my_account/messages/getAdminMessages",
                'data': {},
                success: function (data) {

                    let obj = JSON.parse(data);
                    admin_messages = obj.messages;
                    console.log(admin_messages);
                }
            });
        }

        $(".message-folder.enabled").click(function () {

            let sender_id = $(this).find('.sender-id').html();
            getData(sender_id);

            $(".box-message").empty();
            let folder_status = $(this).find(".folder-status").val();
            if(folder_status == 0) {

            } else {
                setTimeout(function () {
                    let view = insertMessage(messages);
                    $(".box-message").append(view);
                }, 500);
            }
        });

        $(".admin-folder").click(function () {

            getAdminData();

            $(".box-message").empty();
            let folder_status = $(this).find(".folder-status").val();
            if(folder_status == 0) {

            } else {
                setTimeout(function () {
                    let view = insertMessage(admin_messages);
                    $(".box-message").append(view);
                }, 500);
            }
        });

        // folder open/close

        $(".notification-folder .enabled").click(function () {

            let folder_status = $(this).parent().find(".folder-status").val();
            if(folder_status == 0) {
                $(".fa-folder-open").removeClass("fa-folder-open").addClass("fa-folder");
                $(".folder-status").val(0);
                $(".folder-name").css('color', '#4c4b4b');
                $(this).parent().find(".fa-folder").removeClass("fa-folder").addClass("fa-folder-open");
                $(this).parent().find(".folder-name").css('color', '#337ab7');
                $(this).parent().find(".folder-status").val(1);

                // show contained messages

            } else {
                $(this).parent().find(".fa-folder-open").removeClass("fa-folder-open").addClass("fa-folder");
                $(this).parent().find(".folder-status").val(0);
                $(".folder-name").css('color', '#4c4b4b');
            }
        });
    });
</script>




