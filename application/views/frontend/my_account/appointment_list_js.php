<?php
if (!isset($ts))
{
    $ts = time();
}
?>


<script>
    $(document).ready(function() {

        $(".accept").click(function() {

            if($(this).hasClass('btn-primary')) {
                let r = confirm('Are you sure to accept this appointment?');

                if (r == true) {

                    var appointmentId = $(this).parent().parent().find("input.id").val();
                    var currentButton = $(this);
                    $.ajax({

                        url: '/chat/main/set_accept/'+ appointmentId,
                        type: 'GET',

                    }).done(function(response) {

                        var obj = JSON.parse(response);

                        console.log(obj['appointment_id']);
                        console.log(obj['status']);

                        currentButton.removeClass('btn-primary').addClass('btn-success').text('Accepted');
                        currentButton.parent().parent().find('.decline').remove();

                    }).fail(function(error) {

                        console.error(error);
                    });

                } else {

                }
            }

        });

        $(".decline").click(function() {

            if($(this).hasClass('btn-warning')) {
                let r = confirm('Are you sure to decline this appointment?');
                if(r == true) {

                    var appointmentId = $(this).parent().parent().find("input.id").val();
                    var currentButton = $(this);

                    $.ajax({

                        url: '/chat/main/set_decline/'+ appointmentId,
                        type: 'GET',

                    }).done(function(response) {

                        var obj = JSON.parse(response);

                        console.log(obj['appointment_id']);
                        console.log(obj['status']);

                        currentButton.removeClass('btn-warning').addClass('btn-danger').text('Declined');
                        currentButton.parent().parent().find('.accept').remove();

                    }).fail(function(error) {

                        console.error(error);
                    });

                } else {

                }
            }

        });

        $('.select-all').change(function() {

            if(this.checked) {
                
                $('.select-one').prop('checked', true);
            }

            if(!this.checked) {
                
                $('.select-one').prop('checked', false);
            }
                  
        });

        $(".delete").click(function() {

            var rowCount = $('tbody tr').length;

            var checked_array = [];


            for(var i=1; i<rowCount+1; i++) {

                if($('tbody tr:nth-child(' + i + ')').find('.select-one').attr('checked')) {

                    checked_array.push($('tbody tr:nth-child(' + i + ')').find('.id').val());
                }
            }

            $.each(checked_array, function( index, value ) {

                $('tbody tr').filter("[data-row-id='" + value + "']").remove();

                $.ajax({

                    url: '/chat/main/delete_appointment/'+ value,
                    type: 'GET',

                }).done(function(response) {

                    var obj = JSON.parse(response);
                    
                    console.log(obj['appointment_id']);
                    console.log(obj['status']);

                }).fail(function(error) {

                    console.error(error);
                });

            });

            $('.select-all').prop('checked', false);
        });

        // pagination
        var pagination = function(per_page) {

            var number_logs = $("tbody tr").length;
            var number_logs_per_page = per_page;
            var pages = number_logs/number_logs_per_page;
            var page_module = number_logs % number_logs_per_page;
            var group_number = 1;

            pages = Math.trunc(pages);
            $('div.pagination').html('');
            if($('button.btn-previous')) {
                $('button.btn-previous').remove();
            }
            if($('button.btn-next')) {
                $('button.btn-next').remove();
            }
            if(page_module > 0) {
                pages = pages + 1;
            }
            if(number_logs > 0) {
                $("<button class='btn-previous'>&laquo;</button>").insertBefore('.pagination');
                $("<button class='btn-next'>&raquo;</button>").insertAfter('.pagination');
            }

            for (var p=1; p<=pages; p++) {
                if(p == 1) {
                    $("div.pagination").append("<button class='active'>"+p+"</button>");
                } else {
                    $("div.pagination").append("<button>"+p+"</button>");
                }
            }

            $('.page-number').html('page '+1+' of '+pages);

            $('button.btn-previous').hover().css('cursor', 'not-allowed');
            $('button.btn-previous').prop('disabled', true);

            if(pages <= 5) {

                $('button.btn-next').hover().css('cursor', 'not-allowed');
                $('button.btn-next').prop('disabled', true);
            }

            if(pages > 5) {

                for(let n=6; n<=pages; n++) {

                    $("div.pagination button:nth-child("+n+")").css('display', 'none');
                }
            }

            $('button.btn-next').click(function () {

                group_number = group_number + 1;

                if(group_number > 1) {

                    $('button.btn-previous').hover().css('cursor', 'pointer');
                    $('button.btn-previous').prop('disabled', false);
                }
                if((pages % 5 == 0 && group_number >= pages/5) || (pages % 5 > 0 && group_number > pages/5)) {

                    $('button.btn-next').hover().css('cursor', 'not-allowed');
                    $('button.btn-next').prop('disabled', true);
                }

                for(let n=1; n<=pages; n++) {
                    if(n >= ((group_number-1) * 5 + 1) && n <= (group_number*5)) {
                        $("div.pagination button:nth-child("+n+")").css('display', 'block');
                    } else {
                        $("div.pagination button:nth-child("+n+")").css('display', 'none');
                    }
                }
                $("div.pagination").find(".active").removeClass("active");
                $("div.pagination button:nth-child("+((group_number-1)*5+1)+")").addClass("active");

                for (let i = 1; i <= number_logs; i++) {

                    $("tbody tr:nth-child(" + i + ")").css('display', 'none');
                }

                for (let j = number_logs_per_page * ((group_number-1)*5) + 1; j <= number_logs_per_page * ((group_number-1)*5+1); j++) {
                    $("tbody tr:nth-child(" + j + ")").css('display', 'table-row');
                }

                $('.page-number').html('page ' + ((group_number-1)*5+1) + ' of ' + pages);
            });

            $('button.btn-previous').click(function () {

                group_number = group_number - 1;

                if(group_number == 1) {

                    $('button.btn-previous').hover().css('cursor', 'not-allowed');
                    $('button.btn-previous').prop('disabled', true);
                }

                if((pages % 5 == 0 && group_number < pages/5) || (pages % 5 > 0 && group_number <= pages/5)) {

                    $('button.btn-next').hover().css('cursor', 'pointer');
                    $('button.btn-next').prop('disabled', false);
                }

                for(let n=1; n<=pages; n++) {
                    if(n >= ((group_number-1) * 5 + 1) && n <= (group_number*5)) {
                        $("div.pagination button:nth-child("+n+")").css('display', 'block');
                    } else {
                        $("div.pagination button:nth-child("+n+")").css('display', 'none');
                    }
                }
                $("div.pagination").find(".active").removeClass("active");
                $("div.pagination button:nth-child("+((group_number-1)*5+1)+")").addClass("active");

                for (let i = 1; i <= number_logs; i++) {

                    $("tbody tr:nth-child(" + i + ")").css('display', 'none');
                }

                for (let j = number_logs_per_page * ((group_number-1)*5) + 1; j <= number_logs_per_page * ((group_number-1)*5+1); j++) {
                    $("tbody tr:nth-child(" + j + ")").css('display', 'table-row');
                }

                $('.page-number').html('page ' + ((group_number-1)*5+1) + ' of ' + pages);
            });


            if (number_logs > number_logs_per_page * 1) {

                for (var i = 1; i <= number_logs_per_page * 1; i++) {

                    $("tbody tr:nth-child(" + i + ")").css('display', 'table-row');
                }
            } else {

                for (var i = 1; i <= number_logs; i++) {

                    $("tbody tr:nth-child(" + i + ")").css('display', 'table-row');
                }
            }

            var page = 1;


            $(".pagination button").click(function () {

                page = parseInt($(this).text());
                if(page == pages) {

                    console.log('click last page');
                } else {

                    $("#btn-load").attr("disabled", false);
                }

                console.log("current page = "+page);

                $("div.pagination").find(".active").removeClass("active");
                $("div.pagination button:nth-child("+page+")").addClass("active");

                for (var i = 1; i <= number_logs; i++) {

                    $("tbody tr:nth-child(" + i + ")").css('display', 'none');
                }

                if (number_logs > number_logs_per_page * page) {

                    for (var i = 1 + number_logs_per_page * (page - 1); i <= number_logs_per_page * page; i++) {

                        $("tbody tr:nth-child(" + i + ")").css('display', 'table-row');
                    }

                } else {

                    for (var i = 1 + number_logs_per_page * (page - 1); i <= number_logs; i++) {

                        $("tbody tr:nth-child(" + i + ")").css('display', 'table-row');
                    }

                }
                $('.page-number').html('page ' + page + ' of ' + pages);

            });
        }

        pagination(5);

        $('#username-th').click(function () {
            $('.filter-table-form-username').submit();
        });

        $('#datetime-th').click(function () {
            $('.filter-table-form-datetime').submit();
        });
        $('.view-number').change(function () {
            pagination($(this).val());
            $(".pagination button:nth-child(1)").click();
        })
    });
</script>