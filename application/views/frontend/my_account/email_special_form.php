<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Email Readings</h4>

        <div class="card-description">
            <div class="material-datatables">

            <div class="row">

            <div class="col-md-12 col-sm-7">    
                    <h2 style='margin-bottom:0;padding-bottom:0'>Email Readings</h2>
                    <div>To modify your "Default Reading Settings", visit your "Edit My Expert Profile" page by <a href='/my_account/main/edit_profile'>clicking here</a>.</div>
                    <hr />
                    <ul class="nav">
                        <li class="nav-item"><a class="nav-link"  href="/my_account/email_readings/open_requests">Open Email Requests</a></li>
                        <li class="nav-item"><a class="nav-link"  href="/my_account/email_readings/closed_requests">Closed Email Requests</a></li>
                        <li class="nav-item"><a class="nav-link"  href="/my_account/email_readings/email_specials">My Email Specials</a></li>
                        <li class="nav-item"><a class="nav-link active" href="/my_account/email_readings/new_special"><span class='icon icon-tag'></span> Create A New Special</a></li>
                    </ul>

                    <form action='<?= $form_action ?>' method='POST' style='padding:15px 0 0;' class='form-horizontal'>	
                        <div class="form-group">
                            <label class="col-lg-3 col-md-3"><b>Title</b></label>
                            <div class="col-lg-9 col-md-9">
                                <textarea type='text' name='title' value='<?= set_value('title', $title) ?>' class='input-xlarge form-control'> </textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 col-md-3"><b>Description</b></label>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-9 col-md-9">
                                <textarea id="description" type='text' name='description' class='input-xlarge form-control'></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-5 col-md-3"><b>How many questions will you allow for this special?</b></label>
                            <div class="col-lg-5 col-md-9">
                                <input type='text' name='total_questions' value='<?= set_value('total_questions', $total_questions) ?>' class='input-mini form-control'>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-5 col-md-3"><b>How much do you want to charge?</b></label>
                            <div class="col-lg-5 col-md-9 input-group" >
                                <span class="input-group-addon"><i class="fa fa-dollar"></i></span>	
                                <input type='text' name='price' value='<?= set_value('price', $price) ?>' class='input-mini form-control'>
                            </div>
                        </div>
                        <input type='submit' name='submit' value='Save Special' class='btn btn-primary btn-large' id="save-special">
                    </form>
                </div>



            </div>
        </div>
    </div>



<script src="/theme/admin/js/jquery-3.1.1.min.js"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=e465y2qbjbdi14qglxeq486et6jfhc6d0zy21wr3akkqps42"></script>
<script>
    tinymce.init({
        selector: '#description',
        height: 350,
        image_dimensions: false,
        image_description: false,
        browser_spellcheck: true,
        style_formats: [
            {
                selector: 'img',
                styles: {
                    'width': '150px',
                    'height': 'auto'
                }
            }
        ],
        plugins: [
            "advlist autolink lists link image charmap print preview anchor link image code",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste imagetools wordcount", 'image code textcolor colorpicker',
        ],
        relative_urls : false,
        remove_script_host : false,
        convert_urls : true,
        toolbar: "insertfile | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor",
        imagetools_cors_hosts: ['www.tinymce.com', 'codepen.io'],
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ],
        automatic_uploads: true,
        imagetools_cors_hosts: ['mydomain.com', 'otherdomain.com'],paste_data_images: true,
        file_picker_callback: function(cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');

            // Note: In modern browsers input[type="file"] is functional without
            // even adding it to the DOM, but that might not be the case in some older
            // or quirky browsers like IE, so you might want to add it to the DOM
            // just in case, and visually hide it. And do not forget do remove it
            // once you do not need it anymore.

            input.onchange = function() {
                var file = this.files[0];

                var reader = new FileReader();
                reader.onload = function () {
                    // Note: Now we need to register the blob in TinyMCEs image blob
                    // registry. In the next release this part hopefully won't be
                    // necessary, as we are looking to handle it internally.
                    var id = 'blobid' + (new Date()).getTime();
                    var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                    var base64 = reader.result.split(',')[1];
                    var blobInfo = blobCache.create(id, file, base64);
                    blobCache.add(blobInfo);

                    // call the callback and populate the Title field with the file name
                    cb(blobInfo.blobUri(), { title: file.name });
                };
                reader.readAsDataURL(file);
            };

            input.click();
        }
    });
</script>