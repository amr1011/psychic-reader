<script src="/theme/admin/js/jquery-3.1.1.min.js"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=e465y2qbjbdi14qglxeq486et6jfhc6d0zy21wr3akkqps42"></script>
<script>
    tinymce.init({
        selector: 'textarea',
        height: 350,
        plugins: [
            "advlist autolink lists link charmap print preview anchor link code",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste imagetools wordcount", ' code textcolor colorpicker',
        ],
        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
        image_dimensions: false,
        image_description: false,
        relative_urls : false,
        remove_script_host : false,
        convert_urls : true,
        toolbar: "insertfile | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor",
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ],
        automatic_uploads: true,
        imagetools_cors_hosts: ['mydomain.com', 'otherdomain.com'],paste_data_images: true,

        file_picker_callback: function(cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');

            input.onchange = function() {
                var file = this.files[0];

                var reader = new FileReader();
                reader.onload = function () {

                    var id = 'blobid' + (new Date()).getTime();
                    var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                    var base64 = reader.result.split(',')[1];
                    var blobInfo = blobCache.create(id, file, base64);
                    blobCache.add(blobInfo);
                    cb(blobInfo.blobUri(), { title: file.name });
                };
                reader.readAsDataURL(file);
            };

            input.click();
        }
    });
    $(document).ready(function () {
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function(){
            readURL(this);
        });
    })

</script>