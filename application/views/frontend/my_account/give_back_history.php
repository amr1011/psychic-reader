<?php
if (!isset($ts))
{
    $ts = time();
}
?>

<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Giveback List</h4>

        <div class="card-description">
            <div class="material-datatables">

            <div class="row">
                    <?php if (count($gifts) > 0): ?>
                        <div style="display:inline-flex; float: right;">
                            <div style="position: relative; right: 110%;display:none;"><span>page / </span><span id="page-number" style="color: red;"></span></div>
                            <span class="archive-text" style="float:right;margin-right:10px; display: none;">(Archive next 25)</span>
                            <div class="pagination">

                            </div>
                            <button class="btn btn-primary" id="btn-load">Delete Page</button>
                        </div>
                        <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    <th>User Name</th>
                                    <th>First Name</th>
                                    <th>Amount of Gift</th>
                                    <th>Day of Week</th>
                                    <th>Status</th>
                                    <th>Date</th>
                                    <th>Save</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($gifts as $c): ?>
                                <tr data-row-id="<?php echo($c['id']);?>" style="display: none;">
                                    <td><?= $c['client_username'] ?></td>
                                    <td><?= $c['client_firstname'] ?></td>
                                    <td><?= $c['gift_size'] ?> mins</td>
                                    <!--day of week-->
                                    <?php if($c['day_of_week'] == 0):?><td>Sunday</td>
                                    <?php elseif($c['day_of_week'] == 1):?><td>Monday</td>
                                    <?php elseif($c['day_of_week'] == 2):?><td>Tuesday</td>
                                    <?php elseif($c['day_of_week'] == 3):?><td>Wednesday</td>
                                    <?php elseif($c['day_of_week'] == 4):?><td>Thursday</td>
                                    <?php elseif($c['day_of_week'] == 5):?><td>Friday</td>
                                    <?php elseif($c['day_of_week'] == 6):?><td>Saturday</td>
                                    <?php elseif($c['day_of_week'] == 7):?><td>Any day</td>
                                    <?php endif; ?>
                                    <!--status-->
                                    <?php if($c['status'] == 0):?><td style="color: darkred;" class="status" status="0">Pending</td><?php elseif ($c['status'] == 1):?>
                                    <td style="color: green;" class="status" status="1">Completed</td><?php elseif($c['status'] == 2):?>
                                    <td class="status" status="2">Cancelled</td><?php endif;?>
                                    <td><?= $c['created_at'] ?></td>
                                    <!--checkbox to save-->
                                    <?php if($c['status'] == 0):?>
                                    <td><input type="checkbox" class="save-check" disabled></td>
                                    <?php else:?>
                                    <td><input type="checkbox" class="save-check"></td>
                                    <?php endif;?>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div class="pagination" style="float:right;">

                        </div>
                    <?php else: ?>
                        <p>You have no giveback history.</p>
                    <?php endif; ?>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {

        var number_logs = $("tbody tr").length;
        var number_logs_per_page = 25;
        var pages = number_logs/number_logs_per_page;
        var page_module = number_logs % number_logs_per_page;
        pages = Math.trunc(pages);
        if(page_module > 0) {
            pages = pages + 1;
        }

        for (var p=1; p<=pages; p++) {
            if(p == 1) {
                $("div.pagination").append("<button class='active'>"+p+"</button>");
            } else {
                $("div.pagination").append("<button>"+p+"</button>");
            }

        }
        var end = 0;

        if (number_logs > number_logs_per_page * 1) {

            for (var i = 1; i <= number_logs_per_page * 1; i++) {

                $("tbody tr:nth-child(" + i + ")").css('display', 'table-row');
            }
        } else {

            for (var i = 1; i <= number_logs; i++) {

                $("tbody tr:nth-child(" + i + ")").css('display', 'table-row');
            }

            end = 1;
            // $("#btn-load").attr("disabled", true);
            // $(".archive-text").text("(No Archive)");
        }

        console.log(number_logs);

        var page = 1;
        var last_delete_page = 0;
        $("#page-number").text(page);

        $("#btn-load").click(function () {

            if(page-1 != last_delete_page) {
                alert("CAN NOT DELETE THIS PAGE- DELETE  Lower # pages first please");
                return false;
            }

            console.log("page = "+page);
            last_delete_page = page;

            $("div.pagination button:nth-child("+page+")").attr("disabled", true);

            if (end == 1){
                console.log(end);
                console.log('end2');
                $("#btn-load").attr("disabled", true);
                $("div.pagination").find(".active").removeClass("active");
            }

            var array = [];

            // delete current page
            for(var j= 1 + (page-1)*number_logs_per_page; j <= page*number_logs_per_page; j++) {

                var log = $("tbody tr:nth-child(" + j + ")");

                if(log.find(".status").attr("status") != 0 && log.find(".save-check").prop("checked") == false ) {

                    array.push(log.attr("data-row-id"));
                }
            }

            setTimeout(function () {
                $.ajax({

                    'type': "POST",
                    'url': "/chat/chatInterface/delete_give_back_history",
                    'data': {data: array},
                    success: function (data) {

                        var obj = JSON.parse(data);
                        console.log(obj.status);
                    }
                });
            }, 1000);

            // load next page
            if (end == 0) {

                page += 1;
                console.log("=======>"+page);
                $("div.pagination").find(".active").removeClass("active");

                $("div.pagination button:nth-child("+page+")").addClass("active");
                $('div.pagination button:not(.disabled):first')
                $("#page-number").text(page);
                for (var i = 1; i <= number_logs; i++) {

                    $("tbody tr:nth-child(" + i + ")").css('display', 'none');
                }

                if (number_logs > number_logs_per_page * page) {

                    for (var i = 1 + number_logs_per_page * (page - 1); i <= number_logs_per_page * page; i++) {

                        $("tbody tr:nth-child(" + i + ")").css('display', 'table-row');
                    }
                } else if (number_logs <= number_logs_per_page * page && end == 0) {

                    end = 1;
                    console.log('end');
                    console.log(end);

                    for (var i = 1 + number_logs_per_page * (page - 1); i <= number_logs; i++) {

                        $("tbody tr:nth-child(" + i + ")").css('display', 'table-row');
                    }

                }
            }
        });

        $(".pagination button").click(function () {

            page = parseInt($(this).text());
            if(page == pages) {
                end = 1;
                console.log('click last page');
            } else {
                end = 0;
                $("#btn-load").attr("disabled", false);
            }

            console.log("current page = "+page);

            $("div.pagination").find(".active").removeClass("active");
            $("div.pagination button:nth-child("+page+")").addClass("active");

            for (var i = 1; i <= number_logs; i++) {

                $("tbody tr:nth-child(" + i + ")").css('display', 'none');
            }

            if (number_logs > number_logs_per_page * page) {

                for (var i = 1 + number_logs_per_page * (page - 1); i <= number_logs_per_page * page; i++) {

                    $("tbody tr:nth-child(" + i + ")").css('display', 'table-row');
                }

            } else {

                for (var i = 1 + number_logs_per_page * (page - 1); i <= number_logs; i++) {

                    $("tbody tr:nth-child(" + i + ")").css('display', 'table-row');
                }

            }

        })

    });
</script>