<!-- new layout -->

<div class="card ">
    <div class="card-header card-header-success card-header-icon">
        <div class="card-icon"><i class="material-icons">chat</i></div>
            <h4 class="card-title">Chat Details</h4>
        </div>
        <div class="card-body ">
                <div class="row">
                    <div class="col-md-6">
                        <div class="table-responsive table-sales">
                            <h4>Reader: <?php echo $reader_name; ?></h4>
                            <h4>Topic : <?php echo  $this->chatmodel->object['topic']?> </h4>
                            <h4>Session Id : <?php echo  $this->chatmodel->object['id']?> </h4>
                            <h4>Date : <?php echo date("m/d/Y h:i A", strtotime($this->chatmodel->object['start_datetime']))?> </h4>
                            <h4>Duration : <?php echo $this->system_vars->time_generator($this->chatmodel->object['length']); ?></h4>
                            
                            <?php if($chat_reader == 1 && $chat_amount && !$transNotFound):    ?>
                                <p><b>Amount Paid:</b></p>

                                <p><?php echo number_format($chat_amount,2)?></p>
                                <p>
                                    <form action="/my_account/chats/process_refund/<?php echo $this->chatmodel->object['id']; ?>" method="POST">
                                        <input type="submit" class="btn btn-sm btn-rose" value=" Return Entire Chat Time " onclick='return confirm("Are you sure?")'>
                                    </form>
                                </p>

                            <?php endif; ?>


                            <?php

                                $max = floor($this->chatmodel->object['length'] / 60);
                                $word = ($max == 1 ? "Minute" : "Minutes");

                                if ($max > 0 )
                                {
                            ?>

                                    <!-- 
                                    <form action="/my_account/chats/give_timeback/<?php echo $this->chatmodel->object['id']; ?>" method="POST">
                                        <input type="hidden" name="type" value="chat" class="form-control">
                                        <select name="timeback" class="form-control">
                                            <?php
                                            $min = 1;
                                            echo "<option value=''>-- Select --</option>/n";
                                            while ($min <= $max)
                                            {
                                                echo "<option value='$min'>$min</option>/n";
                                                $min++;
                                            }
                                            ?>
                                        </select>
                                        <input type="submit" class="btn btn-sm btn-rose" value=" Return <?php //echo $word; ?> "></form>
                                    -->
                                <?php
                                }
                                ?>


                                <!-- NRR -->


                                <?php if($chat_reader == 1):    ?>
                                    <h2>NRRs</h2>
                                    <div >
                                        <form class="form-inline" id="timeback_form" action="/my_account/chats/give_timeback/<?=$this->chatmodel->object['id']?>" method="POST">
                                            <?php $nr = $this->chatmodel->getNRRs(); ?>
                                            <?php if(count($nr) > 0): ?>

                                                <table class="table table-striped table-bordered">
                                                    <tr>
                                                        <th>Type</th>
                                                        <th>Time Requested (mins)</th>
                                                        <th>Date</th>
                                                        <th>&nbsp;</th>
                                                    </tr>

                                                    <?php foreach($nr as $n):
                                                        $amount = 0;
                                                        switch($n['type'])
                                                        {
                                                            case "unhappy_reading":
                                                                $amount = $n['time_back'];
                                                                break;

                                                            case "disconnect":
                                                                $amount = $n['time_back'];
                                                                break;

                                                            case "slow":
                                                                $amount = $n['time_back'];
                                                                break;


                                                        }
                                                        ?>
                                                        <tr>
                                                            <td><?=ucwords(str_replace('_', ' ',$n['type']))?></td>
                                                            <td><?=$amount?> </td>
                                                            <td><?=$n['date']?></td>
                                                            <td><a href="/my_account/nrr/details/<?=$n['id']?>" class="btn">Details</a></td>
                                                        </tr>

                                                    <?php endforeach; ?>

                                                </table>

                                            <?php else: ?>
                                                <p>There are no NRRs for this chat.</p>
                                            <?php endif; ?>
                                        </form>
                                    </div>
                                <?php endif; ?>           




                        </div>
                    </div>
                </div>
        </div>
</div>                    
<div class="card">
      <div class="card-body">
        <h4 class="card-title">Chat Transcripts</h4>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <select class="form-control" name="doc-format">
                    <option value="1">.pdf</option>
                    <option value="2">.txt</option>
                </select>
            </div>
            <a href="/my_account/chats/download/<?= $chat_id?>/1" id="download-history" class="btn btn-sm btn-primary">Download</a>
        </div>        
        <p class="card-text">
            <?php $transcripts = $this->chatmodel->loadTranscripts(); ?>
            <?php if(count( $transcripts ) > 0 && $transcripts != false): ?>

                <div class="content-pagination col-md-1">
                    <div class="view-number-content">                        
                        <select class="form-control view-number" >
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                        </select>
                    </div>
                    <div class="pagination" ></div>
                </div>
    
            <table class="table table-striped  chats">
                <tbody>
                    
                    <?php foreach($transcripts as $transcript): ?>
                        <tr>
                            <?php if(strpos($transcript['message'],'system') !== false) {?>
                                <td><b class="text-warning">System</b><p><?php echo($transcript['username']);?></p></td>
                                <td><b><?php echo($transcript['message'])?></b></td>

                            <?php } else {?>

                                <td><?=($this->session->userdata('member_logged')==$transcript['member_id'] ? ">{$transcript['username']}" : $transcript['username'])?></td>
                                <td><?=($this->session->userdata('member_logged')==$transcript['member_id'] ? "{$transcript['message']}" : $transcript['message'])?></td>
                            <?php } ?>

                        </tr>
                    <?php endforeach; ?>                    
                </tbody>

            </table>
            <?php endif; ?>

        </p>
  </div>
</div>       

