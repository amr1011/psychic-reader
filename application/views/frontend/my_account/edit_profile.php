
<!-- new layout -->
<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Edit Profile</h4>

        <div class="card-description">

            <div class="material-datatables">

                <div class="col-lg-12">
                <?php echo validation_errors();?>
                    <form action='/my_account/main/save_changed_profile/' class="form-horizontal" id="form-field" enctype="multipart/form-data" method='POST'>

                        <div class="form-group">
                            <label class="col-md-4 label-control"><b>Email Address</b></label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="email" value="<?= $this->member->data['email'] ?>" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 label-control"><b>Username</b></label>
                            <div class="col-md-8">
                                <span name="username"><?= $this->member->data['username'] ?></span>
                            </div>
                        </div>

                        <div style='color:#C0C0C0;'>Paypal Email Address required for paypal payments.</div>
                        <div class="form-group">
                            <label class="col-md-4 label-control"><b>Paypal Email Address</b></label>
                            <div class="col-md-8">
                                <input type='text' class="form-control" name='paypal_email' value='<?= set_value('paypal_email', $this->member->data['paypal_email']) ?>'/>
                            </div>
                        </div>

                        <div style='color:#C0C0C0;'>Leave password fields blank to keep your current password</div>

                        <div class="form-group">
                            <label class="col-md-4 label-control"><b>Choose A New Password</b></label>
                            <div class="col-md-8">
                                <input type='password' name='password' value='<?= set_value('password') ?>' class='form-control'>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 label-control"><b>Re-Type New Password</b></label>
                            <div class="col-md-8">
                                <input type='password' name='password2' value='<?= set_value('password2') ?>' class='form-control'>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 label-control"><b>First Name</b></label>
                            <div class="col-md-8">
                                <input type='text' name='first_name' value='<?= set_value('first_name', $this->member->data['first_name']) ?>' class='form-control'>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 label-control"><b>Last Name</b></label>
                            <div class="col-md-8">
                                <input type='text' name='last_name' value='<?= set_value('last_name', $this->member->data['last_name']) ?>' class='form-control'>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 label-control"><b>Gender</b></label>
                            <div class="col-md-8">
                                <div><input type='radio' name='gender' value='Male' <?php echo set_radio('gender', 'Male', ($this->member->data['gender'] == 'Male' ? TRUE : FALSE)) ?>> Male &nbsp; &nbsp; <input type='radio' name='gender' value='Female' <?php echo set_radio('gender', 'Female', ($this->member->data['gender'] == 'Female' ? TRUE : FALSE)) ?>> Female</div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 label-control"><b>Date of Birth</b></label>
                            <div class="col-md-8">
                                <?php
                                list($dob_year, $dob_month, $dob_day) = explode("-", $this->member->data['dob']);
                                echo $this->system_vars->dob_custom('dob', set_value('dob_month', $dob_month), set_value('dob_day', $dob_day), set_value('dob_year', $dob_year));
                                ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 label-control"><b>Country</b></label>
                            <div class="col-md-8">
                                <?= $this->system_vars->country_array_select_box('country', set_value('country', $this->member->data['country'])) ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 label-control"><b>Photo</b></label>
                            <div class="col-md-8">
                                <img src='<?= $this->member->data['profile_image'] ?>'>
                                <div style='padding:10px 0;color:#C0C0C0'>To keep your current profile image, leave the field below blank3</div>
                                <input type='file' name='profile_image'/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 label-control"><b>Title</b></label>
                            <div class="col-md-8">
                                <input type='text' name='title' value='<?= set_value('title', $this->member->data['title']) ?>' class='form-control' />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 label-control"><b>Category</b></label>
                            <div class="col-md-8">
                                <?php
                                foreach ($categories as $c) {
                                    echo "<div><input type='checkbox' name='categories[]' value='{$c['id']}' " . (in_array($c['id'], $registered_categories) ? " checked" : "") . "/> <span class='category-title'>{$c['title']}</span></div>";
                                }
                                ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 label-control"><b>Biography</b></label>
                            <div class="col-md-8">
                                <textarea rows='10' class='form-control' name='biography'><?= set_value('biography', $this->member->data['biography']) ?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 label-control"><b>Area of Expertise</b></label>
                            <div class="col-md-8">
                                <textarea rows='10' class='form-control' name='area_of_expertise'><?= set_value('area_of_expertise', $this->member->data['area_of_expertise']) ?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 label-control">
                                <input type='checkbox' name='enable_email' value='1' <?= set_checkbox('enable_email', '1', ($this->member->data['enable_email'] == '1' ? TRUE : FALSE)) ?>> Enable email readings
                                <p>** Please DISABLE when you cannot respond to email readings in a timely manner.</p>
                            </div>
                        </div>
        
                        <div class="form-group">
                            <label class="col-md-4 label-control"></label>
                            <div class="col-md-4">
                                <input type='text' class='form-control' name='email_total_days' value='<?= set_value('email_total_days', $this->member->data['email_total_days']) ?>' class='form-control' />
                                <p>How many days will it take you to complete 1 question via email?</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 label-control">
                                <input type='checkbox' name='enable_phone' value='1' <?= set_checkbox('enable_phone', '1', ($this->member->data['enable_phone'] == '1' ? TRUE : FALSE)) ?>> Enable phone readings
                            </div>
                        </div>


                        <input type='button' name='save' id='submitBtn' class='btn btn-primary btn-large' value='Submit Request' data-toggle='modal' data-target='#confirm-submit'/>
                    </form>
                    
                </div>

            </div>
        </div>
    </div>
</div>    




<div class="modal" id="confirm-submit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3><b>Preview Profile Submission</b></h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <label class="col-md-4 label-control"><b>Email Address</b></label>
                    <div class="col-md-8">
                        <span id="email" ></span>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-4 label-control"><b>Username</b></label>
                    <div class="col-md-8">
                        <span id="username" ></span>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-4 label-control"><b>Paypal Email</b></label>
                    <div class="col-md-8">
                        <span id="pp-email" ></span>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-4 label-control"><b>First Name</b></label>
                    <div class="col-md-8">
                        <span id="first-name" ></span>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-4 label-control"><b>Last Name</b></label>
                    <div class="col-md-8">
                        <span id="last-name" ></span>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-4 label-control"><b>Gender</b></label>
                    <div class="col-md-8">
                        <span id="gender" ></span>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-4 label-control"><b>Day Of Birth</b></label>
                    <div class="col-md-8">
                        <span id="month" ></span>-<span id="day" ></span>-<span id="year" ></span>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-4 label-control"><b>Country</b></label>
                    <div class="col-md-8">
                        <span id="country" ></span>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-4 label-control"><b>Profile Image</b></label>
                    <div class="col-md-8">
                        <img id="profile-image" src="#" alt="your image" style="width: 100px; height: 100px;"/>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-4 label-control"><b>Title</b></label>
                    <div class="col-md-8">
                        <span id="title" ></span>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-4 label-control"><b>Categories</b></label>
                    <div class="col-md-8">
                        <span id="categories" ></span>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-4 label-control"><b>Biography</b></label>
                    <div class="col-md-8">
                        <span id="biography" ></span>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-4 label-control"><b>Area of Expertise</b></label>
                    <div class="col-md-8">
                        <span id="area_of_expertise" ></span>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-4 label-control"><b>Enable email readings</b></label>
                    <div class="col-md-8">
                        <span id="enable_email_readings" ></span>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-4 label-control"><b>Email Total Days</b></label>
                    <div class="col-md-8">
                        <span id="email_total_days" ></span>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-4 label-control"><b>Enable phone readings</b></label>
                    <div class="col-md-8">
                        <span id="enable_phone_readings" ></span>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a href="#" id="submit" class="btn btn-success success">Submit</a>
            </div>
        </div>
    </div>
</div>

