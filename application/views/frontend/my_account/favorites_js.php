<script>
    $(document).ready(function () {
        $.ajax({
            url: '/my_account/main/get_saved_option',
            type: 'GET',
        }).done(function (response) {
            let obj = JSON.parse(response);
            var data = obj.data;
            if(data != null) {
                $("#favorite-option").val(data);
            }
        }).fail(function (error) {
            console.error(error);
        });

    })
</script>