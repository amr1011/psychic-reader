<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css">
<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Compose A New Pro Message</h4>

        <div class="card-description">
            <div class="material-datatables">

            <div class="row">

                <form action='/my_account/promessages/compose_submit' method='POST' class="form-horizontal" enctype="multipart/form-data">

                <a class="btn btn-primary <?php if ($promotion_check ==1 ) echo("isDisabled");?>" href="/my_account/promessages/request_admin/<?= $this->member->data['id']?>">Request To Admin</a>
                <input type="hidden" name="from" value="<?= $this->member->data['id']?>"/>
                <input type="hidden" name="promotion_id" value="<?= $promotion_id?>"/>

                <div class="row">
                    <label class="col-sm-4 col-form-label">Receipient</label>
                    <div class="col-sm-8">
                        <div class="form-group">
                            <?php if ($this->uri->segment('4')) : ?>
                                User: <?php echo $to_user['username'] .' ('.$to_user['first_name'].')'; ?> <input type='hidden' name='to' value='<?php echo $to ?>' class='input-mini'>
                                <?php else: ?>

                                <select name='to[]' class='form-control selectpicker' id="select-to" multiple="multiple" required>

                                    <?php foreach ($users as $u) : ?>

                                        <option value='<?php echo $u['id']; ?>' <?php echo set_select('to', $u['id'], ($to == $u['id'] ? TRUE : FALSE)) ?>><?php echo $u['username'] .'  ('. $u['first_name'] . ')'; ?></option>
                                    <?php endforeach; ?>

                                </select>

                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                      <label class="col-sm-4 col-form-label">Subject</label>
                      <div class="col-sm-8">
                        <div class="form-group">
                            <input type='text' class='form-control' name='subject' value='<?= set_value('subject', $subject) ?>'>
                        </div>
                      </div>
                </div>                



                <div class="row">
                      <label class="col-sm-4 col-form-label">Message</label>
                      <div class="col-sm-8">
                        <div class="form-group">
                        <textarea name='message' class='form-control' style='width:100%;height:150px;'><?= set_value('message', $message) ?></textarea>
                        </div>
                      </div>
                </div>                       
                <div class="row">
                      <label class="col-sm-4 col-form-label"></label>
                      <div class="col-sm-8">
                        <input type="file" name="attach" />
                        <input type='submit' name='submit' class="btn btn-primary <?php if ($promotion_check == 0 ) echo("isDisabled");?>" style="margin-left:15px;" value='Send Message' class='btn btn-large btn-primary'>
                      </div>
                </div>       


                </form>

            </div>
        </div>
    </div>
</div>



<script>
    $(document).ready(function () {
        $('#select-to').multiselect({

            numberDisplayed: 6,
            disableIfEmpty: true,
            includeSelectAllOption: true,
            filterPlaceholder: 'Search',
            enableFiltering: true,
            includeFilterClearBtn: false,
            selectAllNumber: false,
        });
    })
</script>