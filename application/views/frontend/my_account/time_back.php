<style>
    legend {
        border-bottom: unset;
        width: unset;
    }
    fieldset {
        border: 1px solid silver;
        border-radius: 5px;
        margin: 20px 0px;
        padding: .35em .625em .75em;
    }
    span.title {
        color: blue;
        font-weight: bold;
    }
    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
        background-color: #acacaf;
        opacity: 1;
    }
</style>

<!-- new layout -->
<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Time Back</h4>

        <div class="card-description">

            <?php if($this->session->flashdata('response')):?>
                <p class='record text-warning' > <?=$this->session->flashdata('response')?> </p>
            <?php endif?>

            <div class="material-datatables">

                
                    <div class="row">    

                        <div class="col-md-6">
                              <div class="card">
                                  <div class="card-header card-header-text card-header-primary">
                                    <div class="card-text"> <h4 class="card-title">Common Chat Info</h4> </div>
                                  </div>
                                  <div class="card-body">

                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <span class="title">Session Id: </span> <span><?= $chat['chat_session_id']?></span>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <span class="title">Date: </span> <span><?= $chat['create_datetime']?></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <span class="title">Client: </span> <span><?= $client['username']?></span>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <span class="title">Reader: </span> <span><?= $reader['username']?></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <span class="title">Paid Time: </span> <span><?= round(floatval($chat['length'])/60, 2)?></span>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <span class="title">Free Time: </span> <span><?= round(floatval($chat['total_free_length'])/60, 2)?></span>
                                            </div>
                                        </div>


                                  </div>
                              </div>
                        </div>



                        <div class="col-md-6">
                              <div class="card">
                                  <div class="card-header card-header-text card-header-primary">
                                    <div class="card-text"> <h4 class="card-title">Browser & User Agent Info</h4> </div>
                                  </div>
                                  <div class="card-body">

                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <span class="title">Client Browser: </span> <span><?= $chat['client_browser']?></span>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <span class="title">Reader Browser: </span> <span><?= $chat['reader_browser']?></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <span class="title">Client Agent: </span> <span><?= $chat['client_agent']?></span>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <span class="title">Reader Agent: </span> <span><?= $chat['reader_agent']?></span>
                                            </div>
                                        </div>                           

                                  </div>
                              </div>
                        </div>



                         <div class="col-md-12">
                              <div class="card">
                                  <div class="card-header card-header-text card-header-primary">
                                    <div class="card-text"> <h4 class="card-title">Service Info</h4> </div>
                                  </div>
                                  <div class="card-body">
                                    <fieldset>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <span class="title">Duration: </span> <span><?= ($chat['length']+$chat['total_free_length']) ?> sec</span>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <span class="title">Tier: </span> <span><?= $chat['tier']?></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <span class="title">Client Post Count: </span> <span><?= $client_post?></span>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <span class="title">Reader Post Count: </span> <span><?= $reader_post?></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <span class="title">Aborted: </span> <span><?= $chat['aborted'] == '0' ? 'No' : ($chat['aborted'] == '1' ? 'Automatically' : 'Manually')?></span>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <span class="title">Rejected: </span> <span><?= $chat['aborted'] == '0' ? 'No' : 'Yes'?></span>
                                            </div>
                                        </div>
                                    </fieldset>

                                  </div>
                              </div>
                        </div>                       




                         <div class="col-md-12">
                              <div class="card">
                                  <div class="card-body">


                                    <form action="/my_account/chats/give_time_back" id="gtb_form" method="post" style="display: inline-flex">
                                        <select class="form-control" name="paid-time" required style="width: 200px; margin-right: 30px; padding-left: 10px;">
                                            <?php if($chat['length'] == 0) : ?>
                                                <option selected disabled>Please select</option>
                                                <option value="1" disabled>All Time</option>
                                                <option value="0.5" disabled>Half Time</option>
                                            <?php else: ?>
                                                <?php if($chat['time_back'] == 0) : ?>
                                                <option selected disabled>Please select</option>
                                                <option value="1">All Time</option>
                                                <option value="0.5">Half Time</option>
                                                <?php elseif($chat['time_back'] == 0.5) : ?>
                                                <option selected disabled>Please select</option>
                                                <option value="1" disabled>All Time</option>
                                                <option value="0.5">Half Time</option>
                                                <?php else : ?>
                                                <option selected disabled>Please select</option>
                                                <option value="1" disabled>All Time</option>
                                                <option value="0.5" disabled>Half Time</option>
                                                <?php endif;?>
                                            <?php endif; ?>
                                        </select>
                                        <input type="hidden" name="paid-time-back-length" value="<?= $chat['length']?>"/>
                                        <input type="hidden" name="reader-id" value="<?= $chat['reader_id']?>"/>
                                        <input type="hidden" name="client-id" value="<?= $chat['client_id']?>"/>
                                        <input type="hidden" name="chat-id" value="<?= $chat['id']?>"/>
                                        <?php if($chat['time_back'] == 1 || $chat['length'] == 0) : ?>
                                            <input class="btn btn-primary btn-round" type="button" onclick="yesno('gtb_form')" value="Give Paid Time Back" disabled/>
                                        <?php else : ?>
                                            <input class="btn btn-primary btn-round" type="button" onclick="yesno('gtb_form')" value="Give Paid Time Back"  />
                                        <?php endif; ?>
                                    </form>
                                   
                                  </div>
                              </div>
                        </div>                       


                        <div class="col-md-12">
                              <div class="card">
                                  <div class="card-body">

                                            <form action="/my_account/chats/give_free_time_back" id="gft_form" method="post" style="display: inline-flex">
                                                <!--                        <input class="form-control free-time-back" type="number" placeholder="Free time back less than --><?//= $chat['total_free_length']/60?><!-- min" style="width: 350px; margin-right: 30px;"/>-->
                                            <select class="form-control" name="free-time" required style="width: 200px; margin-right: 30px; padding-left: 10px;">
                                                <?php if($chat['total_free_length'] == 0) : ?>
                                                    <option selected disabled>Please select</option>
                                                    <option value="1" disabled>All Free Time</option>
                                                    <option value="0.5" disabled>Half Free Time</option>
                                                <?php else: ?>
                                                    <?php if($chat['free_time_back'] == 0) : ?>
                                                        <option selected disabled>Please select</option>
                                                        <option value="1">All Free Time</option>
                                                        <option value="0.5">Half Free Time</option>
                                                    <?php elseif($chat['free_time_back'] == 0.5) : ?>
                                                        <option selected disabled>Please select</option>
                                                        <option value="1" disabled>All Free Time</option>
                                                        <option value="0.5">Half Free Time</option>
                                                    <?php else : ?>
                                                        <option selected disabled>Please select</option>
                                                        <option value="1" disabled>All Free Time</option>
                                                        <option value="0.5" disabled>Half Free Time</option>
                                                    <?php endif;?>
                                                <?php endif; ?>
                                            </select>
                                            <input type="hidden" name="free-time-back-length" value="<?= $chat['total_free_length']?>"/>
                                            <input type="hidden" name="reader-id" value="<?= $chat['reader_id']?>"/>
                                            <input type="hidden" name="client-id" value="<?= $chat['client_id']?>"/>
                                            <input type="hidden" name="chat-id" value="<?= $chat['id']?>"/>
                                            <?php if($chat['free_time_back'] == 1 || $chat['total_free_length'] == 0) : ?>
                                                <input class="btn btn-primary btn-round" type="button" onclick="yesno('gft_form')" value="Give Free Time Back"  disabled/>
                                            <?php else: ?>
                                                <input class="btn btn-primary btn-round" type="button" onclick="yesno('gft_form')" value="Give Free Time Back"  />
                                            <?php endif; ?>
                                        </form>



                                  </div>
                                </div>
                         </div>           


                    </div>



            </div>
        </div>
    </div>
</div>