

<div class="card-header card-header-image div-readers" data-header-animation="true">
    <div class="card-body">
        <div class="card-actions text-center"></div>
        <h4 class="card-title">Chat History</h4>

        <div class="card-description">
            <div class="material-datatables">

            <div class="row">

                    <form class="filter-table-form-username" action='/my_account/chats/filter_chats_username_searched/' method='POST' style="display:none;">
                        <input name="username_direction" value="<?= $username_direction?>"/>
                        <input name="start_year" value="<?= $start_year?>"/>
                        <input name="start_month" value="<?= $start_month?>"/>
                        <input name="start_day" value="<?= $start_day?>"/>
                        <input name="end_year" value="<?= $end_year?>"/>
                        <input name="end_month" value="<?= $end_month?>"/>
                        <input name="end_day" value="<?= $end_day?>"/>
                        <input name="reader_id" value="<?= $reader_id?>"/>
                        <input name="client_id" value="<?= $client_id?>"/>
                    </form>

                    <form class="filter-table-form-datetime" action='/my_account/chats/filter_chats_datetime_searched/' method='POST' style="display: none;">
                        <input name="datetime_direction" value="<?= $datetime_direction?>"/>
                        <input name="start_year" value="<?= $start_year?>"/>
                        <input name="start_month" value="<?= $start_month?>"/>
                        <input name="start_day" value="<?= $start_day?>"/>
                        <input name="end_year" value="<?= $end_year?>"/>
                        <input name="end_month" value="<?= $end_month?>"/>
                        <input name="end_day" value="<?= $end_day?>"/>
                        <input name="reader_id" value="<?= $reader_id?>"/>
                        <input name="client_id" value="<?= $client_id?>"/>
                    </form>

                    <form class="search_form_client_list"action='/my_account/chats/client_chat_search/' method='POST' class='form-inline'>
                        <div class="form-group">
                            <div class="col-md-4 col-sm-4" style="margin-bottom:15px;">
                                <input type='text' class='form-control' name='query' value='<?= $this->input->post('query') ?>' style='margin:0;' placeholder='Search for chats' class='input-xlarge' required/>
                            </div>
                            <div class="col-md-2 col-sm-2" style="margin-bottom:15px;">
                                <input type='submit' value='Search' class='btn btn-primary search-client' style='margin:0;'>
                            </div>

                        </div>
                    </form>

                    <?php echo validation_errors(); ?>

                    <?php if ($chats) : ?>

                        <div class="content-pagination">
                            <div class="view-number-content" style="padding-top: 20px;">
                                <select class="view-number" style=" width: 80px;position: absolute; right: 750px; padding-left: 20px;">
                                    <option value="5">5</option>
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                </select>
                            </div>
                            <div class="pagination" style="margin-top: 20px; float: right;"></div>
                        </div>

                        <table class='table table-striped table-hover table-bordered'>
                            <thead>
                            <th id="datetime-th">Date<?php if($datetime_direction == 'ASC') : ?><i class="fa fa-sort-asc" style="margin-left: 5px; font-size: 12px;"></i><?php else:?><i class="	fa fa-sort-desc" style="margin-left: 5px; font-size: 12px;"></i><?php endif; ?></th>
                            <th id="username-th">Username<?php if($username_direction == 'ASC') : ?><i class="fa fa-sort-asc" style="margin-left: 5px; font-size: 12px;"></i><?php else:?><i class="	fa fa-sort-desc" style="margin-left: 5px; font-size: 12px;"></i><?php endif; ?></th>
                            <th>Details</th>
                            </thead>
                            <?php foreach ($chats as $c) : ?>
                                <?php
                                if ($this->member->data['id'] == $c['client_id'])
                                {
                                    //--- Get reader
                                    $user = $this->system_vars->get_member($c['reader_id']);
                                } else
                                {
                                    //--- Get client
                                    $user = $this->system_vars->get_member($c['client_id']);
                                }

                                $fname = $this->member->data['first_name'];
                                $email = $this->member->data['email'];
                                $country = $this->member->data['country'];
                                $time = date("m/d/Y @ h:i A", strtotime($c['start_datetime']));

                                if ($this->member->data['member_type'] == "reader")
                                {
                                    $type = 1;
                                    $murl = "http://www.psychic-contact.com/modules/chattestingform/reader_form.php?un=" . $this->member->data['username'] . "&cn=" . $user['username'] . "&fn=$fname&email=$email&country=$country&time=$time&type=$type";
                                    $link_name = "Submit";
                                } else
                                {
                                    $type = 0;
                                    $murl = "http://www.psychic-contact.com/modules/chattestingform/client_form.php?un=" . $this->member->data['username'] . "&rn=" . $user['username'] . "&fn=$fname&email=$email&country=$country&time=$time&type=$type";
                                    $link_name = "Submit";
                                }
                                $t_url = "<br><a href=\"$murl\" target=\"_blank\">[$link_name Feedback]</a>";
                                ?>

                                <tr style="display: none;">
                                    <?php if($this->member->data['member_type'] == 'client') {?>
                                        <td width='175' style='vertical-align:middle;'><?php echo $c['id'] . " $t_url "; ?></td>
                                    <?php }?>
                                    <td width='175' style='vertical-align:middle;'><?php echo date("m/d/Y @ h:i A", strtotime($c['start_datetime'])); ?></td>
                                    <td style='vertical-align:middle;'><?php echo $user['username'] ?></td>

                                    <td style='width:100px; vertical-align:middle; text-align:right'><a href='/my_account/chats/transcript/<?php echo $c["id"]?>' class='btn'>Details</a></td>
                                </tr>

                            <?php endforeach; ?>

                        </table>
                        <div class="page-number" style="float: right; margin-top: 10px;"></div>
                    <?php else : ?>
                        <div>You do not have a chat history.</div>
                    <?php endif; ?>
            </div>
        </div>
    </div>                
</div>



<script>
    $(document).ready(function () {

        var pagination = function(per_page) {

            var number_logs = $("tbody tr").length;
            var number_logs_per_page = per_page;
            var pages = number_logs/number_logs_per_page;
            var page_module = number_logs % number_logs_per_page;
            var group_number = 1;

            pages = Math.trunc(pages);
            $('div.pagination').html('');
            if($('button.btn-previous')) {
                $('button.btn-previous').remove();
            }
            if($('button.btn-next')) {
                $('button.btn-next').remove();
            }
            if(page_module > 0) {
                pages = pages + 1;
            }
            if(number_logs > 0) {
                $("<button class='btn-previous'>&laquo;</button>").insertBefore('.pagination');
                $("<button class='btn-next'>&raquo;</button>").insertAfter('.pagination');
            }

            for (var p=1; p<=pages; p++) {
                if(p == 1) {
                    $("div.pagination").append("<button class='active'>"+p+"</button>");
                } else {
                    $("div.pagination").append("<button>"+p+"</button>");
                }
            }

            $('.page-number').html('page '+1+' of '+pages);

            $('button.btn-previous').hover().css('cursor', 'not-allowed');
            $('button.btn-previous').prop('disabled', true);

            if(pages <= 5) {

                $('button.btn-next').hover().css('cursor', 'not-allowed');
                $('button.btn-next').prop('disabled', true);
            }

            if(pages > 5) {

                for(let n=6; n<=pages; n++) {

                    $("div.pagination button:nth-child("+n+")").css('display', 'none');
                }
            }

            $('button.btn-next').click(function () {

                group_number = group_number + 1;

                if(group_number > 1) {

                    $('button.btn-previous').hover().css('cursor', 'pointer');
                    $('button.btn-previous').prop('disabled', false);
                }
                if((pages % 5 == 0 && group_number >= pages/5) || (pages % 5 > 0 && group_number > pages/5)) {

                    $('button.btn-next').hover().css('cursor', 'not-allowed');
                    $('button.btn-next').prop('disabled', true);
                }

                for(let n=1; n<=pages; n++) {
                    if(n >= ((group_number-1) * 5 + 1) && n <= (group_number*5)) {
                        $("div.pagination button:nth-child("+n+")").css('display', 'block');
                    } else {
                        $("div.pagination button:nth-child("+n+")").css('display', 'none');
                    }
                }
                $("div.pagination").find(".active").removeClass("active");
                $("div.pagination button:nth-child("+((group_number-1)*5+1)+")").addClass("active");

                for (let i = 1; i <= number_logs; i++) {

                    $("tbody tr:nth-child(" + i + ")").css('display', 'none');
                }

                for (let j = number_logs_per_page * ((group_number-1)*5) + 1; j <= number_logs_per_page * ((group_number-1)*5+1); j++) {
                    $("tbody tr:nth-child(" + j + ")").css('display', 'table-row');
                }

                $('.page-number').html('page ' + ((group_number-1)*5+1) + ' of ' + pages);
            });

            $('button.btn-previous').click(function () {

                group_number = group_number - 1;

                if(group_number == 1) {

                    $('button.btn-previous').hover().css('cursor', 'not-allowed');
                    $('button.btn-previous').prop('disabled', true);
                }

                if((pages % 5 == 0 && group_number < pages/5) || (pages % 5 > 0 && group_number <= pages/5)) {

                    $('button.btn-next').hover().css('cursor', 'pointer');
                    $('button.btn-next').prop('disabled', false);
                }

                for(let n=1; n<=pages; n++) {
                    if(n >= ((group_number-1) * 5 + 1) && n <= (group_number*5)) {
                        $("div.pagination button:nth-child("+n+")").css('display', 'block');
                    } else {
                        $("div.pagination button:nth-child("+n+")").css('display', 'none');
                    }
                }
                $("div.pagination").find(".active").removeClass("active");
                $("div.pagination button:nth-child("+((group_number-1)*5+1)+")").addClass("active");

                for (let i = 1; i <= number_logs; i++) {

                    $("tbody tr:nth-child(" + i + ")").css('display', 'none');
                }

                for (let j = number_logs_per_page * ((group_number-1)*5) + 1; j <= number_logs_per_page * ((group_number-1)*5+1); j++) {
                    $("tbody tr:nth-child(" + j + ")").css('display', 'table-row');
                }

                $('.page-number').html('page ' + ((group_number-1)*5+1) + ' of ' + pages);
            });


            if (number_logs > number_logs_per_page * 1) {

                for (var i = 1; i <= number_logs_per_page * 1; i++) {

                    $("tbody tr:nth-child(" + i + ")").css('display', 'table-row');
                }
            } else {

                for (var i = 1; i <= number_logs; i++) {

                    $("tbody tr:nth-child(" + i + ")").css('display', 'table-row');
                }
            }

            var page = 1;


            $(".pagination button").click(function () {

                page = parseInt($(this).text());
                if(page == pages) {

                    console.log('click last page');
                } else {

                    $("#btn-load").attr("disabled", false);
                }

                console.log("current page = "+page);

                $("div.pagination").find(".active").removeClass("active");
                $("div.pagination button:nth-child("+page+")").addClass("active");

                for (var i = 1; i <= number_logs; i++) {

                    $("tbody tr:nth-child(" + i + ")").css('display', 'none');
                }

                if (number_logs > number_logs_per_page * page) {

                    for (var i = 1 + number_logs_per_page * (page - 1); i <= number_logs_per_page * page; i++) {

                        $("tbody tr:nth-child(" + i + ")").css('display', 'table-row');
                    }

                } else {

                    for (var i = 1 + number_logs_per_page * (page - 1); i <= number_logs; i++) {

                        $("tbody tr:nth-child(" + i + ")").css('display', 'table-row');
                    }

                }
                $('.page-number').html('page ' + page + ' of ' + pages);

            });
        }

        pagination(5);

        $('#username-th').click(function () {
            $('.filter-table-form-username').submit();
        });

        $('#datetime-th').click(function () {
            $('.filter-table-form-datetime').submit();
        });
        $('.view-number').change(function () {
            pagination($(this).val());
            $(".pagination button:nth-child(1)").click();
        })
    });
</script>

