<style>
    .thumbnail-article, .read-more {
        text-align: center;
    }
    .read-more {
        margin-bottom: 20px;
    }
    .card {
        border: 1px solid #eee;
        border-radius: 5px;
        margin-bottom: 5px;
        margin-top: 5px;
        padding: 5px;
    }
    .caption {
        padding-left: 20px;
    }
    h3 {
        color: #0000cc;
        text-align: center;
    }
    #back-to-article {
        float: left;
        margin-left: 20px;
    }
</style>
<section class="page-form">
    <div class="page-form__header">
        <a href="/articles/" id="back-to-article">Back To Articles</a>
        <h4 style="float:right; color: blue; margin-right: 10px;">Result: <?= $count;?></h4>
        <h2>Searched Articles</h2>
    </div>
    <div class="">
        <div class="row" style='padding:20px;'>
            <div class='col-md-12 col-sm-12'>
                <div class="row">
                    <?php if ($articles) : ?>
                        <?php foreach ($articles as $a) : ?>
                            <div class='col-lg-3 col-md-3 col-sm-3'>
                                <div class="card">
                                    <div class='thumbnail-article'><a href='/articles/view/<?php echo $a['id']; ?> '>
                                            <?php if($a['filename'] != null) : ?><img src="/media/articles/<?php echo $a['filename']?>"/>
                                            <?php else : ?><img src="/media/assets/no-image-available.jpg"/>
                                            <?php endif; ?>
                                        </a></div>
                                    <div class='caption'>
                                        <div><strong>Author: </strong><?= $a['username'];?></div>
                                        <div><strong>Category: </strong><?= $a['category_title']?></div>
                                        <div><strong>Title: </strong><?= $a['title']?></div>
                                    </div>
                                    <div class="read-more"><a href='/articles/view/<?php echo $a['id']; ?> ' class="btn btn-primary">Read More</a></div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <div style="margin-top: 30px; min-height: 300px; text-align: center;"><img src="/media/images/no-result.png"/></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>