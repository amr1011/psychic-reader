<style>
    ul.ulp {
        columns: 3;
    }
    .thumbnail-article, .read-more {
        text-align: center;
    }
    .read-more {
        margin-bottom: 20px;
    }
    .card {
        border: 1px solid #eee;
        border-radius: 5px;
        margin-bottom: 5px;
        margin-top: 5px;
        padding: 5px;
    }
    .caption {
        padding-left: 20px;
    }
    h3 {
        color: #0000cc;
        text-align: center;
    }

    .pagination {
        display: inline-block;
        margin: 0px;
    }

    .pagination button {
        color: black;
        float: left;
        padding: 1px 10px;
        text-decoration: none;
    }

    .pagination button.active {
        background-color: #4CAF50;
        color: white;
        border-radius: 5px;
    }

    .pagination button:hover:not(.active) {
        background-color: #ddd;
        border-radius: 5px;
    }
    button {
        cursor: pointer;
        border: 0px;
        border-radius: 5px;
        margin: 1px;
    }

    .btn-previous, .btn-next {
        margin-top: 20px;
    }
    .btn-previous:hover, .btn-next:hover {
        background-color: #0a6aa1;
        color: white;
    }
    .pagination button {
        width: 35px;
    }
    .submit-form {
        border: 1px solid gray;
        border-radius: 4px;
        margin-top: 20px;
        padding: 20px 100px 20px 100px;
    }
    .form-control {
        width: 80%;
        display: inline;
    }
    .submit-form label {
        width: 17%;
        color: #993300;
    }
    .form-item {
        margin: 10px 0;
    }
    .form-item-center {
        text-align: center;
    }
    .submit-btn {
        width: 30%;
        margin-top: 30px;
        font-size: 18px;
    }
    .modal {
        position: absolute;
        top: 10px;
        right: 100px;
        bottom: 0;
        left: 0;
        z-index: 10040;
        overflow: auto;
        overflow-y: auto;
    }
    .modal-open {
        padding-right: 0px !important;
    }
</style>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<?php if($email_form_data != null && $email_form_data['header'] == 1 && $email_form_detail_data != null) : ?>
    <div class="submit-form">
        <form action="/articles/submit" method="POST">
            <div class="form-item-center">
                <h2><?= $email_form_data['page_name']?></h2>
            </div>
            <div class="form-item">
                <label class="" for="full-name">Full Name</label>
                <input class="form-control" name="full-name" placeholder="Enter your full name" required/>
            </div>
            <div class="form-item">
                <label class="" for="email">Email</label>
                <input class="form-control" name="email" type="email" placeholder="Enter your email" required/>
            </div>

            <?php foreach($email_form_detail_data as $email_form_detail_datum) : ?>
                <div class="form-item">
                    <?php if ($email_form_detail_datum['item_type'] == 'input') : ?>
                        <label class="" for="<?= strtolower($email_form_detail_datum['label'])?>"><?= $email_form_detail_datum['label']?></label>
                        <input class="form-control" name="<?= strtolower($email_form_detail_datum['label'])?>" placeholder="Enter your <?= strtolower($email_form_detail_datum['label'])?>" />
                    <?php endif;?>
                    <?php if ($email_form_detail_datum['item_type'] == 'textarea') : ?>
                        <label class="" for="<?= strtolower($email_form_detail_datum['label'])?>"><?= $email_form_detail_datum['label']?></label>
                        <textarea class="form-control" name="<?= strtolower($email_form_detail_datum['label'])?>" placeholder="Enter your <?= strtolower($email_form_detail_datum['label'])?>"></textarea>
                    <?php endif;?>
                    <?php if ($email_form_detail_datum['item_type'] == 'list') : ?>
                        <label class="" for="<?= strtolower($email_form_detail_datum['label'])?>"><?= $email_form_detail_datum['label']?></label>
                        <select class="form-control" name="<?= strtolower($email_form_detail_datum['label'])?>">
                            <option disabled selected>Please select</option>
                            <?php foreach($email_form_detail_datum['data'] as $datum) :?>
                                <option class="form-control" value="<?= $datum['username'];?>"><?= $datum['username'];?></option>
                            <?php endforeach; ?>
                        </select>
                    <?php endif;?>
                </div>
            <?php endforeach; ?>
            <div class="form-item-center">
                <input type="submit" value="Submit" class="btn btn-primary submit-btn"/>
            </div>
        </form>
    </div>
<?php endif;?>
<section class="page-form">
    <div class="page-form__header">
        <h2>Articles</h2>
    </div>
    <div class="">
        <div class="row" style="padding: 20px;">
            <h3>Search Article</h3>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <label for="search">Search Option</label>
                <select name="search" class="article-search-option form-control">
                    <option disabled selected>Please select option</option>
                    <option value="category">Category</option>
                    <option value="author">Author</option>
                    <option value="title">Title</option>
                    <option value="content">Content</option>
                    <option value="date">Date</option>
                </select>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <label>Search Value</label>
                <form action="/main/search_article" method="POST">
                    <input type="hidden" name="search-option" value="" class="search-option"/>
                    <div class="search-value category" style="display: none;">
                        <select name="category" class="form-control">
                            <option disabled selected>Please select category</option>
                            <?php
                            foreach ($this->site->get_categories() as $c)
                            {
                                echo "<option value='{$c['id']}'>{$c['title']}</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="search-value author" style="display: none;">
                        <select name="author" class="form-control">
                            <option disabled selected>Please select author</option>
                            <?php
                            foreach ($this->readers->get_all_readers() as $r)
                            {
                                echo "<option value='{$r['id']}'>{$r['username']}</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="search-value title" style="display: none;">
                        <input type="text" name="title" class="form-control" placeholder="Input title here."/>
                    </div>
                    <div class="search-value content" style="display: none;">
                        <textarea name="content" class="form-control"></textarea>
                    </div>
                    <div class="search-value date" style="display: none;">
                        <input type="text" class="form-control" name="daterange" value="" />
                        <input type="hidden" name="start-date" class="start-date" value=""/>
                        <input type="hidden" name="end-date" class="end-date" value=""/>
                    </div>
                    <input type="submit" value="Search" class="btn btn-primary search-button" style="display: none;margin-top:20px;"/>
                </form>
            </div>
        </div>
    </div>
    <hr/>
    <div class="">
        <div class="row" style='padding:20px;'>
            <div class='col-md-12 col-sm-12'>
                <h3 style='margin:15px 0 15px;'>Article Categories</h3>
                <ul class="ulp">
                    <?php
                    foreach ($this->site->get_categories() as $c)
                    {
                        $totalArticles = $this->db->query("SELECT id FROM articles WHERE category_id = {$c['id']} and approved = 1 ")->num_rows();
                        echo "<li><a class='btn' style='width:100%;text-align:left;margin-bottom:10px;' href='/articles/{$c['url']}' style='font-weight:Bold;'><b>{$c['title']}</b> <span style='color:#949494;'>({$totalArticles} articles)</span></a></li>";
                    }
                    ?>
                </ul>
                <hr>
                <h3 style='margin:15px 0 15px;'><?= $title?></h3>
                <div class="row">
                    <?php if ($articles) : ?>
                        <?php foreach ($articles as $a) : ?>
                            <div class='col-lg-3 col-md-3 col-sm-3'>
                                <div class="card">
                                    <div class='thumbnail-article'><a href='/articles/view/<?php echo $a['id']; ?> '>
                                            <?php if($a['filename'] != null) : ?><img src="/media/articles/<?php echo $a['filename']?>"/>
                                            <?php else : ?><img src="/media/assets/no-image-available.jpg"/>
                                            <?php endif; ?>
                                        </a></div>
                                    <div class='caption'>
                                        <div><strong>Author: </strong><?= $a['username'];?></div>
                                        <div><strong>Category: </strong><?= $a['category_title']?></div>
                                        <div><strong>Title: </strong><?= $a['title']?></div>
                                    </div>
                                    <div class="read-more"><a href='/articles/view/<?php echo $a['id']; ?> ' class="btn btn-primary">Read More</a></div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <div style="margin-top: 30px;"><h4>&nbsp;&nbsp;&nbsp;There are no articles to show.</h4></div>
                    <?php endif; ?>
                </div>
                <hr/>
                <h3 style='margin:15px 0 15px;'><?= $previous_title?></h3>
                <div class="row"  id="prev-articles">
                    <?php if($previous_articles):?>
                        <?php foreach($previous_articles as $a) :?>
                            <div class='col-lg-3 col-md-3 col-sm-3 article' style="display: none;">
                                <div class="card">
                                    <div class='caption'>
                                        <div><strong>Author: </strong><?= $a['username'];?></div>
                                        <div><strong>Category: </strong><?= $a['category_title']?></div>
                                        <div><strong>Title: </strong><?= $a['title']?></div>
                                    </div>
                                    <div class="read-more"><a href='/articles/view/<?php echo $a['id']; ?> ' class="btn btn-primary">Read More</a></div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <div style="margin-top: 30px;"><h4>&nbsp;&nbsp;&nbsp;There are no old articles to show.</h4></div>
                    <?php endif; ?>
                </div>
                <?php if($previous_articles):?>
                    <div class="page-content" style="display: inline-flex; float:right;">
                        <div class="pagination" style="margin-top:20px;">

                        </div>
                    </div>
                    <div class="page-number" style="margin-left: 20px; color: #0000cc;"></div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<?php if($this->session->userdata('member_logged')) { ?>


    
<?php if($email_form_data != null && $email_form_data['footer'] == 1 && $email_form_detail_data != null) : ?>
    <div class="submit-form">
        <form action="/articles/submit" method="POST">
            <div class="form-item-center">
                <h2><?= $email_form_data['page_name']?></h2>
            </div>
            <div class="form-item">
                <label class="" for="full-name">Full Name</label>
                <input class="form-control" name="full-name" placeholder="Enter your full name" required/>
            </div>
            <div class="form-item">
                <label class="" for="email">Email</label>
                <input class="form-control" name="email" type="email" placeholder="Enter your email" required/>
            </div>

            <?php foreach($email_form_detail_data as $email_form_detail_datum) : ?>
                <div class="form-item">
                    <?php if ($email_form_detail_datum['item_type'] == 'input') : ?>
                        <label class="" for="<?= strtolower($email_form_detail_datum['label'])?>"><?= $email_form_detail_datum['label']?></label>
                        <input class="form-control" name="<?= strtolower($email_form_detail_datum['label'])?>" placeholder="Enter your <?= strtolower($email_form_detail_datum['label'])?>" />
                    <?php endif;?>
                    <?php if ($email_form_detail_datum['item_type'] == 'textarea') : ?>
                        <label class="" for="<?= strtolower($email_form_detail_datum['label'])?>"><?= $email_form_detail_datum['label']?></label>
                        <textarea class="form-control" name="<?= strtolower($email_form_detail_datum['label'])?>" placeholder="Enter your <?= strtolower($email_form_detail_datum['label'])?>"></textarea>
                    <?php endif;?>
                    <?php if ($email_form_detail_datum['item_type'] == 'list') : ?>
                        <label class="" for="<?= strtolower($email_form_detail_datum['label'])?>"><?= $email_form_detail_datum['label']?></label>
                        <select class="form-control" name="<?= strtolower($email_form_detail_datum['label'])?>">
                            <option disabled selected>Please select</option>
                            <?php foreach($email_form_detail_datum['data'] as $datum) :?>
                                <option class="form-control" value="<?= $datum['username'];?>"><?= $datum['username'];?></option>
                            <?php endforeach; ?>
                        </select>
                    <?php endif;?>
                </div>
            <?php endforeach; ?>

            <div class="form-item-center">
                <input type="submit" value="Submit" class="btn btn-primary submit-btn"/>
            </div>
        </form>
    </div>
<?php endif;?>
<?php } ?>


<?php if($this->session->userdata('member_logged')){?>

<div class="modal" id="confirm-submit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="/articles/submit" method="POST">
                <div class="modal-header">
                    <h3><b><?= $email_form_data['page_name']?></b></h3>
                </div>
                <div class="modal-body">
                    <div class="submit-form" style="padding: 0px; border: none;">
                        <div class="form-item">
                            <label class="" for="full-name">Full Name</label>
                            <input class="form-control" name="full-name" placeholder="Enter your full name" required/>
                        </div>
                        <div class="form-item">
                            <label class="" for="email">Email</label>
                            <input class="form-control" name="email" type="email" placeholder="Enter your email" required/>
                        </div>

                        <?php foreach($email_form_detail_data as $email_form_detail_datum) : ?>
                            <div class="form-item">
                                <?php if ($email_form_detail_datum['item_type'] == 'input') : ?>
                                    <label class="" for="<?= strtolower($email_form_detail_datum['label'])?>"><?= $email_form_detail_datum['label']?></label>
                                    <input class="form-control" name="<?= strtolower($email_form_detail_datum['label'])?>" placeholder="Enter your <?= strtolower($email_form_detail_datum['label'])?>" />
                                <?php endif;?>
                                <?php if ($email_form_detail_datum['item_type'] == 'textarea') : ?>
                                    <label class="" for="<?= strtolower($email_form_detail_datum['label'])?>"><?= $email_form_detail_datum['label']?></label>
                                    <textarea class="form-control" name="<?= strtolower($email_form_detail_datum['label'])?>" placeholder="Enter your <?= strtolower($email_form_detail_datum['label'])?>"></textarea>
                                <?php endif;?>
                                <?php if ($email_form_detail_datum['item_type'] == 'list') : ?>
                                    <label class="" for="<?= strtolower($email_form_detail_datum['label'])?>"><?= $email_form_detail_datum['label']?></label>
                                    <select class="form-control" name="<?= strtolower($email_form_detail_datum['label'])?>">
                                        <option disabled selected>Please select</option>
                                        <?php foreach($email_form_detail_datum['data'] as $datum) :?>
                                            <option class="form-control" value="<?= $datum['username'];?>"><?= $datum['username'];?></option>
                                        <?php endforeach; ?>
                                    </select>
                                <?php endif;?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-item-center">
                        <input type="submit" value="Submit" class="btn btn-primary submit-btn" style="margin-top: 0px;"/>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php } ?>
<script src="/theme/jquery/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script>
    $(document).ready(function () {

        if(parseInt('<?php echo($email_form_data["popup"])?>') >= 0) {

            $("#confirm-submit").modal('show');
            let popup_position = parseInt('<?= $email_form_data["popup"]?>');
            switch (popup_position) {
                case 0:
                    $('.modal').css({
                        'right':'50%'
                    });
                    break;
                case 2:
                    $('.modal').css({
                        'left':'50%'
                    });
                    break;
                case 3:
                    $('.modal').css({
                        'right':'50%',
                        'top':'20%'
                    });
                    break;
                case 4:
                    $('.modal').css({
                        'top':'20%'
                    });
                    break;
                case 5:
                    $('.modal').css({
                        'left':'50%',
                        'top':'20%'
                    });
                    break;
                case 6:
                    $('.modal').css({
                        'right':'50%',
                        'top':'40%'
                    });
                    break;
                case 7:
                    $('.modal').css({
                        'top':'40%'
                    });
                    break;
                case 8:
                    $('.modal').css({
                        'left':'50%',
                        'top':'40%'
                    });
                    break;
            }
        }

        var pagination = function(per_page) {

            var number_logs = $("#prev-articles .article").length;
            var number_logs_per_page = per_page;
            var pages = number_logs/number_logs_per_page;
            var page_module = number_logs % number_logs_per_page;
            var group_number = 1;

            pages = Math.trunc(pages);
            $('div.pagination').html('');
            if($('button.btn-previous')) {
                $('button.btn-previous').remove();
            }
            if($('button.btn-next')) {
                $('button.btn-next').remove();
            }
            if(page_module > 0) {
                pages = pages + 1;
            }
            if(number_logs > 0) {
                $("<button class='btn-previous'>&laquo; Prev</button>").insertBefore('.pagination');
                $("<button class='btn-next'>Next &raquo;</button>").insertAfter('.pagination');
            }

            for (var p=1; p<=pages; p++) {
                if(p == 1) {
                    $("div.pagination").append("<button class='active'>"+p+"</button>");
                } else {
                    $("div.pagination").append("<button>"+p+"</button>");
                }
            }

            $('.page-number').html('page '+1+' of '+pages);

            $('button.btn-previous').hover().css('cursor', 'not-allowed');
            $('button.btn-previous').prop('disabled', true);

            if(pages <= 4) {

                $('button.btn-next').hover().css('cursor', 'not-allowed');
                $('button.btn-next').prop('disabled', true);
            }

            if(pages > 4) {

                for(let n=5; n<=pages; n++) {

                    $("div.pagination button:nth-child("+n+")").css('display', 'none');
                }
            }

            $('button.btn-next').click(function () {

                group_number = group_number + 1;

                if(group_number > 1) {

                    $('button.btn-previous').hover().css('cursor', 'pointer');
                    $('button.btn-previous').prop('disabled', false);
                }
                if((pages % 4 == 0 && group_number >= pages/4) || (pages % 4 > 0 && group_number > pages/4)) {

                    $('button.btn-next').hover().css('cursor', 'not-allowed');
                    $('button.btn-next').prop('disabled', true);
                }

                for(let n=1; n<=pages; n++) {
                    if(n >= ((group_number-1) * 4 + 1) && n <= (group_number*4)) {
                        $("div.pagination button:nth-child("+n+")").css('display', 'block');
                    } else {
                        $("div.pagination button:nth-child("+n+")").css('display', 'none');
                    }
                }
                $("div.pagination").find(".active").removeClass("active");
                $("div.pagination button:nth-child("+((group_number-1)*4+1)+")").addClass("active");

                for (let i = 1; i <= number_logs; i++) {

                    $("#prev-articles div.article:nth-child(" + i + ")").css('display', 'none');
                }

                for (let j = number_logs_per_page * ((group_number-1)*4) + 1; j <= number_logs_per_page * ((group_number-1)*4+1); j++) {
                    $("#prev-articles div.article:nth-child(" + j + ")").css('display', 'block');
                }

                $('.page-number').html('page ' + ((group_number-1)*4+1) + ' of ' + pages);
            });

            $('button.btn-previous').click(function () {

                group_number = group_number - 1;

                if(group_number == 1) {

                    $('button.btn-previous').hover().css('cursor', 'not-allowed');
                    $('button.btn-previous').prop('disabled', true);
                }

                if((pages % 4 == 0 && group_number < pages/4) || (pages % 4 > 0 && group_number <= pages/4)) {

                    $('button.btn-next').hover().css('cursor', 'pointer');
                    $('button.btn-next').prop('disabled', false);
                }

                for(let n=1; n<=pages; n++) {
                    if(n >= ((group_number-1) * 4 + 1) && n <= (group_number*4)) {
                        $("div.pagination button:nth-child("+n+")").css('display', 'block');
                    } else {
                        $("div.pagination button:nth-child("+n+")").css('display', 'none');
                    }
                }
                $("div.pagination").find(".active").removeClass("active");
                $("div.pagination button:nth-child("+((group_number-1)*4+1)+")").addClass("active");

                for (let i = 1; i <= number_logs; i++) {

                    $("#prev-articles div.article:nth-child(" + i + ")").css('display', 'none');
                }

                for (let j = number_logs_per_page * ((group_number-1)*4) + 1; j <= number_logs_per_page * ((group_number-1)*4+1); j++) {
                    $("#prev-articles div.article:nth-child(" + j + ")").css('display', 'block');
                }

                $('.page-number').html('page ' + ((group_number-1)*4+1) + ' of ' + pages);
            });


            if (number_logs > number_logs_per_page * 1) {

                for (var i = 1; i <= number_logs_per_page * 1; i++) {

                    $("#prev-articles div.article:nth-child(" + i + ")").css('display', 'block');
                }
            } else {

                for (var i = 1; i <= number_logs; i++) {

                    $("#prev-articles div.article:nth-child(" + i + ")").css('display', 'block');
                }
            }

            var page = 1;


            $(".pagination button").click(function () {

                page = parseInt($(this).text());
                if(page == pages) {

                    console.log('click last page');
                } else {

                    $("#btn-load").attr("disabled", false);
                }

                console.log("current page = "+page);

                $("div.pagination").find(".active").removeClass("active");
                $("div.pagination button:nth-child("+page+")").addClass("active");

                for (var i = 1; i <= number_logs; i++) {

                    $("#prev-articles div.article:nth-child(" + i + ")").css('display', 'none');
                }

                if (number_logs > number_logs_per_page * page) {

                    for (var i = 1 + number_logs_per_page * (page - 1); i <= number_logs_per_page * page; i++) {

                        $("#prev-articles div.article:nth-child(" + i + ")").css('display', 'block');
                    }

                } else {

                    for (var i = 1 + number_logs_per_page * (page - 1); i <= number_logs; i++) {

                        $("#prev-articles div.article:nth-child(" + i + ")").css('display', 'block');
                    }

                }
                $('.page-number').html('page ' + page + ' of ' + pages);

            });
        }

        pagination(4);

        $('.article-search-option').change(function () {
            switch ($(this).val()) {
                case 'category':
                    $('.search-value').css('display','none');
                    $('.category').css('display','block');
                    $('.search-button').css('display','block');
                    break;
                case 'author':
                    $('.search-value').css('display','none');
                    $('.author').css('display','block');
                    $('.search-button').css('display','block');
                    break;
                case 'title':
                    $('.search-value').css('display','none');
                    $('.title').css('display','block');
                    $('.search-button').css('display','block');
                    break;
                case 'content':
                    $('.search-value').css('display','none');
                    $('.content').css('display','block');
                    $('.search-button').css('display','block');
                    break;
                case 'date':
                    $('.search-value').css('display','none');
                    $('.date').css('display','block');
                    $('.search-button').css('display','block');
                    break;
            }
            $('input.search-option').val($(this).val());
        });
    })
</script>
<script>
    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('input[name="daterange"]').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
        $('input.start-date').val(start.format('YYYY-MM-DD'));
        $('input.end-date').val(end.format('YYYY-MM-DD'));
        console.log(start.format('YYYY-MM-DD'));
        console.log(end.format('YYYY-MM-DD'));
    }

    $('input[name="daterange"]').daterangepicker({
        startDate: start,
        endDate: end,
    }, cb);
    cb(start, end);
</script>