<script src='https://www.google.com/recaptcha/api.js'></script>
    

	<div class='content'>
		
				
		<!-- Main Content -->
		<div class="" style="padding:45px 45px 0 45px;"> 
			
			<div>
				<?=utf8_encode(html_entity_decode($content))?>
			</div>
			<form action='/register/email' method='POST' >
		
			<table width='100%' cellPadding='10' cellSpacing='0'>
			
				<tr>
					<td style='width:150px;'><b>Email Address:</b> <span style='color:red;'>*</span></td>
					<td><input type='text' name='email' value='<?=set_value('email')?>' ></td>
				</tr>		
				<tr>
					<td style='width:150px;'><b>Captcha:</b> <span style='color:red;'>*</span></td>
					<td><div class="g-recaptcha" data-sitekey="6LdzDBATAAAAAB17rt9v3zZbOuPDUHFovBRo5B-u"></div></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><input type="submit" value="Submit" class='btn btn-primary btn-large'></td>
				</tr>				
				
			</table>
		
		</form>
		</div>
		
		<div class='clear'></div>
	
	</div>

    <script>

        var current = 1;
        var timeout = 5000;
        var imageArray = [];
        var totalImages = 0;

        $(function(){

            $('#slideshow img').each(function(i, object){
                imageArray.push(object);
            });

            //--- Set the animation
            totalImages = imageArray.length;
            setInterval(showSlideshowImage, timeout);
            showSlideshowImage();

        });

        function showSlideshowImage(){

            var objectImage = imageArray[current-1];
            var lastObjectImage = imageArray[(current == 1 ? totalImages-1 : current)];

            $(lastObjectImage).fadeOut('slow');
            $(objectImage).fadeIn('slow');

            current = (current == totalImages ? 1 : current+1);

        }

    </script>