<section class="page-form">

    <div class="">
        <div class="row" style='padding:20px;'>
            <div class='col-md-12 col-sm-12'>
                <h3 style='margin:15px 0 15px;'><?= $title?></h3>
                <div class="row">
                    <?php if ($articles) : ?>
                        <?php foreach ($articles as $a) : ?>
                            <div class='col-lg-4 d-flex align-items-stretch'>

                            <div class="card card-plain card-blog ">
                              <div class="card-header card-header-image">

                                   <?php if($a['filename'] != null) : ?><img class="img img-raised" src="/media/articles/<?php echo $a['filename']?>"/>
                                            <?php else : ?><img class="img img-raised" src="/media/assets/no-image-available.jpg"/>
                                    <?php endif; ?>
                              </div>
                              <div class="card-body">
                                <h6 class="card-category text-info"><?= $a['category_title']?></h6>
                                <h6 class="card-category text-info">Author <?= $a['username'];?></h6>

                                <h4 class="card-title">
                                  <?= $a['title']?>
                                </h4>
                                <p class="card-description">
                                </p>
                                <p><a href='/articles/view/<?php echo $a['id']; ?> '> Read More </a></p>
                              </div>
                            </div>


                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <h4>There are no articles to show.</h4>
                    <?php endif; ?>
                </div>
                
            </div>
        </div>
    </div>
</section>