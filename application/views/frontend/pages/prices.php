 <!--     *********    PRICING 4     *********      -->
      <div class="pricing-4" id="pricing-4">
        <div class="container">
          <div class="row">
            <div class="col-md-12 ml-auto mr-auto text-center">
              <h2 class="title">Pick the best plan for you</h2>
              <h5 class="description">Pay by credit card safely on our secure server or by PayPal.</h5>
              <div class="section-space"></div>
            </div>
          </div>
          <div class="row">

            <div class="col-lg-12"><h3 class="text-danger">Chat Packages</h3></div>

            <?php foreach($packages as $package): ?>

            <?php if ($package['type'] == "reading"): ?>
              <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                <div class="card card-pricing ">
                  <div class="card-body">
                    <h6 class="card-category"> <?php echo $package['title']; ?></h6>
                    <h1 class="card-title">
                      <small>$</small><?php echo $package['price']; ?></h1>
                  </div>
                </div>
              </div>            
            <?php endif; ?>
            <?php endforeach; ?>
          </div>
    
          <div class="row">
            <div class="col-lg-12"><h3 class="text-danger">Email Reading Packages</h3></div>
            <?php foreach($packages as $package): ?>

            <?php if ($package['type'] == "email"): ?>
              <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                <div class="card card-pricing ">
                  <div class="card-body">
                    <h6 class="card-category"> <?php echo $package['title']; ?></h6>
                    <h1 class="card-title">
                      <small>$</small><?php echo $package['price']; ?></h1>
                  </div>
                </div>
              </div>            
            <?php endif; ?>
            <?php endforeach; ?>
          </div>


          <div class="row">
            <div class="col-lg-12"><h3 class="text-danger">Phone Reading Packages</h3></div>
            <?php foreach($packages as $package): ?>

            <?php if ($package['type'] == "phone"): ?>
              <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                <div class="card card-pricing ">
                  <div class="card-body">
                    <h6 class="card-category"> <?php echo $package['title']; ?></h6>
                    <h1 class="card-title">
                      <small>$</small><?php echo $package['price']; ?></h1>
                  </div>
                </div>
              </div>            
            <?php endif; ?>
            <?php endforeach; ?>
          </div>

          

        </div>
      </div>
      <!--     *********    END PRICING 4      *********      -->