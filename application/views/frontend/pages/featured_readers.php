<!--
<script>
    function showFeaturedReader()
    {
        var url = "/ajax_functions.php?mode=showFeaturedReader";

        $.ajax({
            url: url,
            cache: false,
            success: function (html) {
                $("#featuredReader").html(html);
            }
        });
    }

    function showReaderList()
    {
        var url = "/ajax_functions.php?mode=showReaderList&count=5";

        $.ajax({
            url: url,
            cache: false,
            success: function (html) {
                $("#readersList").html(html);
            }
        });
    }


    function OpenTestimonialsWindow(reader_id)
    {
        url = "<?= SITE_URL ?>/modules/testimonials/show.php?reader_id=" + reader_id;
        newwin = window.open(url, "testimonials", "scrollbars=yes,menubar=no,resizable=1,location=no,width=600,height=360,left=200,top=200");
        newwin.focus();
    }


    var mytimer = setInterval("showReaderList()", 10000);
    var mytimer2 = setInterval("showFeaturedReader()", 10000);

    $(document).ready(function () {
        showFeaturedReader();
        showReaderList();
    });

</script>
-->

<?php foreach ($online_readers as $reader): ?>
    <div class="readers-content"><!-- 1 -->
        <div class="readers-content__img">
            <?php if (file_exists(FCPATH . "/media/assets/" . $reader->profile_image)): ?>
                <img src="/media/assets/<?php echo $reader->profile_image; ?>" alt="No Image" style="width: 129px; height: 82px">
            <?php else: ?>
                <img src="/media/assets/no-image-available.jpg" alt="No Image" style="width: 129px; height: 82px">
            <?php endif; ?>
            <button class="btn-sidebar btn-session" type="button">available for chat</button>
        </div>
        <div class="readers-content__text-top">
            <span><?php echo $reader->username; ?></span>
            <span><?php echo truncate($reader->area_of_expertise, 60); ?> </span>
        </div>
        <div class="readers-content__text-bot">
            <a href="/profile/<?php echo $reader->username; ?>">Read biography<br>39 Client Testimonials</a>
        </div>
    </div><!-- .readers-content --><!-- 1 -->
<?php endforeach; ?>



<div class="readers-block__bot">
    <button class="btn-show" type="button" onclick="javascript:window.location.href = '/psychics';">Show all psychics</button>
</div>

<!--
<div id="featuredReader" class="online_readers"> 
    <center>
        <img style='margin-top: 100px;' src='/media/images/ajax_spinner.gif'>
    </center>
</div>            


<div id="readersList" class="online_readers">
    <center>
        <img style='margin-top: 100px;' src='/media/images/ajax_spinner.gif'>
    </center>
</div>
-->
