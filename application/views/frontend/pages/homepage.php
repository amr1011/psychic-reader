    <div class="banner row"><!-- david_v3 -->
        
        <div class="text col-md-5 col-sm-5 banner-text">
              <span style="font-family:Arial Narrow, sans-serif; color: #2e4194; font-size: 22px; margin: 0px; font-weight: normal; padding: 0 0 20px 0px;">
                  <font style="font-size: 28px;line-height: 35px;">Accurate, Compassionate,
                      Professional &amp; Ethical Psychic Readers.</font><br>
        <div style="padding-top: 13px; padding-bottom: 13px;">LIVE PSYCHIC READINGS 24/7</div></span>
            <div class="banner-btns"><p style="padding: 0 30px 0 0" class="col-sm-6">New Client Special:
                <br>10 Minutes $1.99
                <br><span style="font-size: 11px; font-weight: normal;">(New Users Only) </span>
                <br><a href="/psychics"><img class="chatnow-btn img-responsive" src="/media/images/chat-now-button.jpg"></a></p>
            <p class="col-sm-6 client-special">Repeat Client Special:
                <br>20 Minutes $27.99
                <br><br>
                <?php if($this->session->userdata('member_logged')){?>
                	<a href="/main/logout">
                    <img class="loginout-btn img-responsive" src="/media/images/logout-button.png">
                </a>
               <?php  }else{?>
                <a href="/register/login">
                    <img class="loginout-btn img-responsive" src="/media/images/login-button.jpg">
                </a>
                <?php }?>
            </p></div>
        </div>
        
        <div id="slide-container" class=" col-md-7 col-sm-7">
            <div id="slideshow">
                <img src="/media/images/banner02.jpg" class="img-responsive" style="display:none;">
                <img src="/media/images/banner03.jpg" class="img-responsive" style="display:none;">
            </div>
        </div>
        
    </div>

	<div class='content row'>		
		<!-- Main Content -->
		<div class="main_content col-md-8"> 
			
			<div>
				<?=utf8_encode(html_entity_decode($content))?>
			</div>

		</div>
		<!-- Side Content -->
		<div class='side_content col-md-4'>			
			<?=$this->load->view('frontend/pages/featured_readers', null, TRUE)?>
		</div>
		<div class='clear'></div>
	
	</div>

    <script>

        var current = 1;
        var timeout = 5000;
        var imageArray = [];
        var totalImages = 0;

        $(function(){

            $('#slideshow img').each(function(i, object){
                imageArray.push(object);
            });

            //--- Set the animation
            totalImages = imageArray.length;
            setInterval(showSlideshowImage, timeout);
            showSlideshowImage();

        });

        function showSlideshowImage(){

            var objectImage = imageArray[current-1];
            var lastObjectImage = imageArray[(current == 1 ? totalImages-1 : current)];

            $(lastObjectImage).fadeOut('slow');
            $(objectImage).fadeIn('slow');

            current = (current == totalImages ? 1 : current+1);

        }

    </script>