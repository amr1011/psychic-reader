<?php if (isset($online_readers) && $online_readers): ?>
    <?php foreach ($online_readers as $reader): ?>
        <div class="readers-content"><!-- 1 -->
            <div class="readers-content__img">
                <?php if (file_exists(FCPATH . "/media/assets/" . $reader->profile_image)): ?>
                    <img src="/media/assets/<?php echo $reader->profile_image; ?>" alt="No Image" style="width: 129px; height: 82px">
                <?php else: ?>
                    <img src="/media/assets/no-image-available.jpg" alt="No Image" style="width: 129px; height: 82px">
                <?php endif; ?>
                <button class="btn-sidebar btn-success" type="button">available for chat</button>
            </div>
            <div class="readers-content__text-top">
                <span><?php echo $reader->username; ?></span>
                <span><?php echo truncate($reader->area_of_expertise, 60); ?> </span>
            </div>
            <div class="readers-content__text-bot">
                <a href="/profile/<?php echo $reader->username; ?>">Read biography<br>39 Client Testimonials</a>
            </div>
        </div><!-- .readers-content --><!-- 1 -->
    <?php endforeach; ?>




    <div class="readers-block__bot">
        <button class="btn-show" type="button" onclick="javascript:window.location.href = '/psychics';">Show all psychics</button>
    </div>

<?php endif; ?>