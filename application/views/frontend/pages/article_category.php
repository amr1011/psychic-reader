<style>
    ul.ulp {
        columns: 3;
    }
    .thumbnail-article, .read-more {
        text-align: center;
    }
    .read-more {
        margin-bottom: 20px;
    }
    .card {
        border: 1px solid #eee;
        border-radius: 5px;
        margin-bottom: 5px;
        margin-top: 5px;
        padding: 5px;
    }
    .caption {
        padding-left: 20px;
    }
    h3 {
        color: #0000cc;
        text-align: center;
    }

    .pagination {
        display: inline-block;
        margin: 0px;
    }

    .pagination button {
        color: black;
        float: left;
        padding: 1px 10px;
        text-decoration: none;
    }

    .pagination button.active {
        background-color: #4CAF50;
        color: white;
        border-radius: 5px;
    }

    .pagination button:hover:not(.active) {
        background-color: #ddd;
        border-radius: 5px;
    }
    button {
        cursor: pointer;
        border: 0px;
        border-radius: 5px;
        margin: 1px;
    }

    .btn-previous, .btn-next {
        margin-top: 20px;
    }
    .btn-previous:hover, .btn-next:hover {
        background-color: #0a6aa1;
        color: white;
    }
    .pagination button {
        width: 35px;
    }
</style>
<section class="page-form">
    <div class="page-form__header">
        <h2>Articles</h2>
    </div>

    <div class="">
        <div class="row" style='padding:20px;'>
            <div class='col-md-12 col-sm-12'>
                <h3 style='margin:15px 0 15px;'><?= $category_title?></h3>
                <a href='/articles' style='font-weight:Bold;'><b>Articles</b></a><span>/</span><a href='/articles/<?= $url?>/' style='font-weight:Bold;'><b><?=$category_title?></b></a>
                <h3 style='margin:15px 0 15px;'><?= $title?></h3>
                <div class="row">
                    <?php if ($articles) : ?>
                        <?php foreach ($articles as $a) : ?>
                            <div class='col-lg-3 col-md-3 col-sm-3'>
                                <div class="card">
                                    <div class='thumbnail-article'><a href='/articles/view/<?php echo $a['id']; ?> '>
                                            <?php if($a['filename'] != null) : ?><img src="/media/articles/<?php echo $a['filename']?>"/>
                                            <?php else : ?><img src="/media/assets/no-image-available.jpg"/>
                                            <?php endif; ?>
                                        </a></div>
                                    <div class='caption'>
                                        <div><strong>Author: </strong><?= $a['username'];?></div>
                                        <div><strong>Category: </strong><?= $a['category_title']?></div>
                                        <div><strong>Title: </strong><?= $a['title']?></div>
                                    </div>
                                    <div class="read-more"><a href='/articles/view/<?php echo $a['id']; ?> ' class="btn btn-primary">Read More</a></div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <div style="margin-top: 30px;"><h4>&nbsp;&nbsp;&nbsp;There are no articles to show.</h4></div>
                    <?php endif; ?>
                </div>
                <hr/>
                <h3 style='margin:15px 0 15px;'><?= $previous_title?></h3>
                <div class="row"  id="prev-articles">
                    <?php if($previous_articles):?>
                        <?php foreach($previous_articles as $a) :?>
                            <div class='col-lg-3 col-md-3 col-sm-3 article' style="display: none;">
                                <div class="card">
                                    <div class='caption'>
                                        <div><strong>Author: </strong><?= $a['username'];?></div>
                                        <div><strong>Category: </strong><?= $a['category_title']?></div>
                                        <div><strong>Title: </strong><?= $a['title']?></div>
                                    </div>
                                    <div class="read-more"><a href='/articles/view/<?php echo $a['id']; ?> ' class="btn btn-primary">Read More</a></div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <div style="margin-top: 30px;"><h4>&nbsp;&nbsp;&nbsp;There are no old articles to show.</h4></div>
                    <?php endif; ?>
                </div>
                <?php if($previous_articles):?>
                    <div class="page-content" style="display: inline-flex; float:right;">
                        <div class="pagination" style="margin-top:20px;">

                        </div>
                    </div>
                    <div class="page-number" style="margin-left: 20px; color: #0000cc;"></div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<script src="/theme/jquery/jquery-3.2.1.min.js"></script>
<script>
    $(document).ready(function () {

        var pagination = function(per_page) {

            var number_logs = $("#prev-articles .article").length;
            var number_logs_per_page = per_page;
            var pages = number_logs/number_logs_per_page;
            var page_module = number_logs % number_logs_per_page;
            var group_number = 1;

            pages = Math.trunc(pages);
            $('div.pagination').html('');
            if($('button.btn-previous')) {
                $('button.btn-previous').remove();
            }
            if($('button.btn-next')) {
                $('button.btn-next').remove();
            }
            if(page_module > 0) {
                pages = pages + 1;
            }
            if(number_logs > 0) {
                $("<button class='btn-previous'>&laquo; Prev</button>").insertBefore('.pagination');
                $("<button class='btn-next'>Next &raquo;</button>").insertAfter('.pagination');
            }

            for (var p=1; p<=pages; p++) {
                if(p == 1) {
                    $("div.pagination").append("<button class='active'>"+p+"</button>");
                } else {
                    $("div.pagination").append("<button>"+p+"</button>");
                }
            }

            $('.page-number').html('page '+1+' of '+pages);

            $('button.btn-previous').hover().css('cursor', 'not-allowed');
            $('button.btn-previous').prop('disabled', true);

            if(pages <= 4) {

                $('button.btn-next').hover().css('cursor', 'not-allowed');
                $('button.btn-next').prop('disabled', true);
            }

            if(pages > 4) {

                for(let n=5; n<=pages; n++) {

                    $("div.pagination button:nth-child("+n+")").css('display', 'none');
                }
            }

            $('button.btn-next').click(function () {

                group_number = group_number + 1;

                if(group_number > 1) {

                    $('button.btn-previous').hover().css('cursor', 'pointer');
                    $('button.btn-previous').prop('disabled', false);
                }
                if((pages % 4 == 0 && group_number >= pages/4) || (pages % 4 > 0 && group_number > pages/4)) {

                    $('button.btn-next').hover().css('cursor', 'not-allowed');
                    $('button.btn-next').prop('disabled', true);
                }

                for(let n=1; n<=pages; n++) {
                    if(n >= ((group_number-1) * 4 + 1) && n <= (group_number*4)) {
                        $("div.pagination button:nth-child("+n+")").css('display', 'block');
                    } else {
                        $("div.pagination button:nth-child("+n+")").css('display', 'none');
                    }
                }
                $("div.pagination").find(".active").removeClass("active");
                $("div.pagination button:nth-child("+((group_number-1)*4+1)+")").addClass("active");

                for (let i = 1; i <= number_logs; i++) {

                    $("#prev-articles div.article:nth-child(" + i + ")").css('display', 'none');
                }

                for (let j = number_logs_per_page * ((group_number-1)*4) + 1; j <= number_logs_per_page * ((group_number-1)*4+1); j++) {
                    $("#prev-articles div.article:nth-child(" + j + ")").css('display', 'block');
                }

                $('.page-number').html('page ' + ((group_number-1)*4+1) + ' of ' + pages);
            });

            $('button.btn-previous').click(function () {

                group_number = group_number - 1;

                if(group_number == 1) {

                    $('button.btn-previous').hover().css('cursor', 'not-allowed');
                    $('button.btn-previous').prop('disabled', true);
                }

                if((pages % 4 == 0 && group_number < pages/4) || (pages % 4 > 0 && group_number <= pages/4)) {

                    $('button.btn-next').hover().css('cursor', 'pointer');
                    $('button.btn-next').prop('disabled', false);
                }

                for(let n=1; n<=pages; n++) {
                    if(n >= ((group_number-1) * 4 + 1) && n <= (group_number*4)) {
                        $("div.pagination button:nth-child("+n+")").css('display', 'block');
                    } else {
                        $("div.pagination button:nth-child("+n+")").css('display', 'none');
                    }
                }
                $("div.pagination").find(".active").removeClass("active");
                $("div.pagination button:nth-child("+((group_number-1)*4+1)+")").addClass("active");

                for (let i = 1; i <= number_logs; i++) {

                    $("#prev-articles div.article:nth-child(" + i + ")").css('display', 'none');
                }

                for (let j = number_logs_per_page * ((group_number-1)*4) + 1; j <= number_logs_per_page * ((group_number-1)*4+1); j++) {
                    $("#prev-articles div.article:nth-child(" + j + ")").css('display', 'block');
                }

                $('.page-number').html('page ' + ((group_number-1)*4+1) + ' of ' + pages);
            });


            if (number_logs > number_logs_per_page * 1) {

                for (var i = 1; i <= number_logs_per_page * 1; i++) {

                    $("#prev-articles div.article:nth-child(" + i + ")").css('display', 'block');
                }
            } else {

                for (var i = 1; i <= number_logs; i++) {

                    $("#prev-articles div.article:nth-child(" + i + ")").css('display', 'block');
                }
            }

            var page = 1;


            $(".pagination button").click(function () {

                page = parseInt($(this).text());
                if(page == pages) {

                    console.log('click last page');
                } else {

                    $("#btn-load").attr("disabled", false);
                }

                console.log("current page = "+page);

                $("div.pagination").find(".active").removeClass("active");
                $("div.pagination button:nth-child("+page+")").addClass("active");

                for (var i = 1; i <= number_logs; i++) {

                    $("#prev-articles div.article:nth-child(" + i + ")").css('display', 'none');
                }

                if (number_logs > number_logs_per_page * page) {

                    for (var i = 1 + number_logs_per_page * (page - 1); i <= number_logs_per_page * page; i++) {

                        $("#prev-articles div.article:nth-child(" + i + ")").css('display', 'block');
                    }

                } else {

                    for (var i = 1 + number_logs_per_page * (page - 1); i <= number_logs; i++) {

                        $("#prev-articles div.article:nth-child(" + i + ")").css('display', 'block');
                    }

                }
                $('.page-number').html('page ' + page + ' of ' + pages);

            });
        }

        pagination(4);
    })
</script>