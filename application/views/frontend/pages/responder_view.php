<style>
    .content {
        line-height:20px;
        font-size:20px;
        padding:30px;
    }
    img {
        width: 80%;
        height: auto;
    }
    .thumbnail-article {
        text-align: center;
    }
</style>
<section class="page-form">
    <div class="page-form__header">
        <h2><?=$responder_name?></h2>
    </div>

    <div class="">
        <div class="row" style='padding:20px;'>
            <div class='col-md-12 col-sm-12'>
                <div class="row">
                    <div class='content_area'>
                        <div class='thumbnail-article'><img src="/media/responders/<?php echo $responder_image?>"/></div>
                        <div class="content"><?=nl2br($responder_content)?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>