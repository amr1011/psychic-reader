<section class="page-form">

    <div class="">
        <div class="row" style='padding:20px;'>
            <div class='col-md-12 col-sm-12'>
                <!-- <a href='/articles' style='font-weight:Bold;'><b>Articles</b></a><span>/</span><a href='/articles/<?= $url?>/' style='font-weight:Bold;'><b><?=$category_title?></b></a><span>/</span><a href='/articles/view/<?= $id?>/' style='font-weight:Bold;'><b><?=$title?></b></a> -->
                <div class="row">
                    <div class='content_area'>
                        <div class='thumbnail-article'><img src="/media/articles/<?php echo $filename?>"/></div>
                        <h2 class="title"><?=$title?></h2>
                        <div class='caption' style='font-size:14px;'><?=date("F d, Y @ h:i A", strtotime($datetime))?> EST</div>
                        <div class="content"><?=nl2br($content)?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>