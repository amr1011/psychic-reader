<script src='https://www.google.com/recaptcha/api.js'></script>
<style>
    select {
        display: block;
        width: 100%;
        height: 40px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.2857143;
        color: #555;
        background-color: #fff;
        border: 1px solid #ccc;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    }
    select:focus {
        outline: none !important;
        border:1px solid #38aeff;
        box-shadow: 0 0 10px #719ECE;
    }
    .notifyjs-bootstrap-success{
        background-position: left center;
    }
</style>

<section class="page-form">
    <div class="page-form__header">
        <h2>Contact Us</h2>
    </div>

    <div style='padding:20px;'>

        <p>Please use the form below to contact us. We will respond in a timely manner.</p>

        <?php if($this->session->flashdata('response_delete')): ?>
        <p class='record-delete' style="display: none;"> <?=$this->session->flashdata('response_delete')?> </p>
        <?php endif?>

        <?php if($this->session->flashdata('response')):?>
            <p class='record' style="display: none;"> <?=$this->session->flashdata('response')?> </p>
        <?php endif?>

        <hr />
        <form action='/contact/submit' method='POST' style='margin:15px 0 0;padding-right:10px;' class="form-horizontal">
            <!-- You name david_v4 -->
            <div class="form-group">
                <label class="col-md-4"><b>Your name</b></label>
                <div class="col-md-8">
                    <input type='text' name='name' class="form-control" value='<?= set_value('name') ?>' placeholder="Please input your name" pattern="[A-Za-z0-9 ]+">
                </div>
            </div>
            <!-- Email address -->
            <div class="form-group">
                <label class="col-md-4"><b>Your Email Address</b></label>
                <div class="col-md-8">
                    <input type='text' name='email' class="form-control" value='<?= set_value('email') ?>' placeholder="Please input your email address" pattern="[a-z0-9]+@[a-z0-9.-]+.[a-z]{2,4}">
                </div>
            </div>
            <!-- Phone number -->
            <div class="form-group">
                <label class="col-md-4"><b>Your Phone number</b></label>
                <div class="col-md-8">
                    <input type='text' name='phone' value='<?= set_value('phone') ?>' class="form-control" placeholder="Please input your phone number" pattern="[0-9 -]+">
                </div>
            </div>

            

            <!-- Username -->
<!--            <div class="form-group">-->
<!--                <label class="col-md-4"><b>Your Username</b></label>-->
<!--                <div class="col-md-8">-->
<!--                    <input type='text' class="form-control" name='username' value='--><?//= set_value('username') ?><!--'>-->
<!--                </div>-->
<!--            </div>-->
            <!-- Subject -->
            <div class="form-group">
                <label class="col-md-4"><b>Subject</b></label>
                <div class="col-md-8">
<!--                    <select class="form-control" name="subject">-->
                    <select class="" name="subject" id="subject">
                        <option value=''>Please Select Subject</option>
                        <?php foreach($subjects as $subject) {?>

                            <option value="<?php echo($subject['subject']);?>`"><?php echo($subject['subject']);?></option>
                        <?php }?>
                    </select>
                </div>
            </div>
            <!-- Comments -->
            <div class="form-group">
                <label class="col-md-4"><b>Comments / Questions</b></label>
                <div class="col-md-8">
                    <textarea class="form-control" name='comments' rows='10' cols='50'><?= set_value('comments') ?></textarea>
                </div>
            </div>
            <!-- Captcha -->
<!--            <div class="form-group">-->
<!--                <label class="col-md-4"><b>Enter Security Code</b></label>-->
<!--                <div class="col-md-8">-->
<!--                    <div class="g-recaptcha" data-sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"></div>-->
<!--                </div>-->
<!--            </div>-->
            <input type='submit' class="btn btn-primary" name='submit' value='Send Inqury'>				
        </form>


    </div>
</section>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script>
    $(document).ready(function () {
        if($('p.record-delete').html()) {
            $.notify($('p.record-delete').html(), {className:"error",align:"center", verticalAlign:"top", hideDuration: 700, autoHideDelay: 5000,});
        }

        console.log($('p.record').html());

        if($('p.record').html()) {
            $.notify($('p.record').html(), {className:"success",align:"center", verticalAlign:"top", hideDuration: 700, autoHideDelay: 5000,});
        }

    });
    
</script>

