<section class="page-form">
    <div class="page-form__header">
        <h2>Page Not Found</h2>
    </div>
    <div style="padding: 20px">
        <p>The page you are looking for doesn't seem to exist. Please click on an active link in the navigation above.</p>
    </div>
</section>