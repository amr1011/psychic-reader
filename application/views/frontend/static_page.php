<section class="page-form">
    <div class="page-form__header">
        <h2><?php echo $title ?></h2>
    </div>

    <div style='padding:20px;'>
        <?php echo utf8_encode(html_entity_decode($content)); ?>
    </div>

</section>