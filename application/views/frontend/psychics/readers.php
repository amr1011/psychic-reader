<!--     *********    TEAM 1     *********      -->
      <div class="team-1" id="team-1">
        <div class="container">
          
          <div class="row">
            <div class="col-md-8 ml-auto mr-auto text-center">
              <h2 class="title">Meet our Professional Psychic Readers!</h2>
              <h5 class="description">Login to read more information about each reader and select your favorite..</h5>
            </div>
          </div>

          <div class="row">
            <?php $i=0; ?>
            
            <?php foreach ($readers as $reader): ?>
            <div class="col-md-4 d-flex align-items-stretch padding-bottom-30">
              <div class="card card-profile">
                <div class="card-header card-avatar">
                    <?php if (file_exists(FCPATH . $reader["profile"])): ?>
                          <a href="#">
                            <img class="img-raised rounded img-fluid"" src="<?php echo $reader["profile"]; ?>" />
                          </a>
                    <?php else: ?>
                          <a href="#">
                            <img src="/media/assets/no-image-available.jpg" class="img">
                          </a>
                    <?php endif; ?>
                </div>
                <div class="card-body ">
                  <h4 class="card-title"><?php echo ucfirst($reader['username']); ?></h4>
                  <?php 
                    switch ($reader['status']) {
                        case 'online':
                            $status = "I am available";
                            $attrib ="text-success";
                            break;
                        case 'break';

                            $status = "Be Right Back on ". $reader['break_time_amount'] ."  minutes";
                            $attrib ="text-muted";
                            break;                        
                        default:
                            $status = "I am offline";
                            $attrib ="text-muted";
                            break;
                    }


                  ?>


                  <h6 class="card-category  <?php echo $attrib; ?>"><?php echo $status; ?></h6>
                  <p class="card-description">
                    <?php echo truncate($reader['area_of_expertise'], 80); ?>
                  </p>
                </div>
                <div class="card-footer justify-content-center">
                  <?php if ($reader['enable_phone'] == 1): ?>
                    <a href="/register/login" target="_self"  class="btn btn-just-icon btn-link btn-phone btn-success"><i class="fa fa-phone"></i></a>
                    <?php endif; ?>

                  <a href="/register/login" target="_self"  class="btn btn-just-icon btn-link btn-chat"><i class="fa fa-comments"></i></a>
                  <a href="/register/login" target="_self"  class="btn btn-just-icon btn-link btn-appointment"><i class="fa fa-calendar"></i></a>
                    <a href="/profile/<?php echo $reader["username"];?>" class="btn btn-info btn-sm btn-round">Read biography</a>
                    <input type="hidden" class="reader-id" value="<?php echo($reader['id']);?>">
                    <input type="hidden" class="client-id" value="<?php echo($this->session->userdata('member_logged')); ?>">
                    <input type="hidden" class="reader-name" value="<?php echo $reader["username"]; ?>">
                </div>
              </div>
            </div>            

            <?php $i++; ?>
            <?php endforeach; ?>

           
            
          </div>


          <!-- 9-3 -->
            <div class="wrapper-content" id="box-appointment" style="margin-top: 190px; overflow: unset;">
                <section class="content-block templete-block"  style="padding-bottom: 10px; padding-top: 10px;">
                    <div class="row" style="margin-bottom: 20px;">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <span style="font-weight: bold; font-size: 18px;">APPOINTMENT  to  </span><span id="reader" style="font-weight: bold; font-size: 22px;color: red;"></span>
                            <button type="button" class="close" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-8">
                            <span style="font-weight: bold;">Please select date</span>&nbsp;&nbsp;&nbsp;<i class="fa fa-calendar"></i>
                            <input id="datepicker" style="border-radius: 5px;">
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-4">
                            <button type="button" class="btn btn-success" id="confirm-appointment">Appointment</button>
                            <button type="button" class="btn btn-danger cancel">Cancel</button>
                        </div>
                    </div>
                </section>
            </div>


        </div>
      </div>
      <!--     *********    END TEAM 1      *********      -->


    


 

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<script>
    $(document).ready(function(){
        /*9-3*/
        var reader_id;
        var client_id;
        var reader_name;
        var selected_date;
        $("#datepicker").datetimepicker();

        $("#box-appointment").css('display', 'none');

        $(document).on('click', '.btn-appointment', function (event) {
            
            $("#box-appointment").css('display', 'block');

            $('html, body').animate({
              scrollTop: $(window).height()
            }, 800);

            reader_name = $(this).parent().find(".reader-name").val();
            reader_id = $(this).parent().find(".reader-id").val();
            client_id = $(this).parent().find(".client-id").val();

            $("#reader").text(reader_name);

        });

        $(".close").on('click', function() {

            $("#box-appointment").css('display', 'none');    
            $('html, body').animate({
              scrollTop: 0
            }, 800);                
        });

        $(".cancel").on('click', function() {

            $("#box-appointment").css('display', 'none');  
            $('html, body').animate({
              scrollTop: 0
            }, 800);                  
        });

        $("#confirm-appointment").on('click', function() {

            $("#box-appointment").css('display', 'none');
            $.ajax({
                'type': "POST",
                'url': "/chat/main/set_appointment",
                'data': { reader_id : reader_id, client_id: client_id, date: $("#datepicker").val()},
                success: function (data) {
                    
                    
                }
            });

            $("#datepicker").val(null);

            $('html, body').animate({
              scrollTop: 0
            }, 800);
        });
        /*9-3*/
      
        $(document).on("click", ".btn-chat", function(){
            window.open($(this).attr("data-href"));
        });

        function diff_minutes(dt2, dt1) 
        {

          var diff =(dt2.getTime() - dt1.getTime()) / 1000;
          diff /= 60;
          return Math.abs(Math.round(diff));

      }
        
        var check_interval = 5000;
        var check_interval_handler = setInterval(check_reader_status, check_interval);


        function check_reader_status() {
            
            $(".div-readers").each(function(){
                var reader_id = $(this).attr("data-reader-id");
                var username = $(this).attr("data-username");
                var client_id = <?php echo ($this->session->userdata('member_logged'))? $this->session->userdata('member_logged'):0 ?>;
                
                var url = "/ajax_functions.php?mode=checkReaderMyAccountStatus&reader_id="+reader_id+"&client_id="+client_id;
                $.ajax({    
                        url: url,  
                        cache: false,  
                        success: function(retValue){
                            // convert string to json object 
                            var d = jQuery.parseJSON(retValue);

                            if (d.logoff == true) {
                                    window.location.href = "/main/logout";
                                    return;
                            }
                            var online_reders = $('.readers-container .readers-content[data-status="online"]');
                            var break_readers = $('.readers-container .readers-content[data-status="break"]');
                            var busy_readers = $('.readers-container .readers-content[data-status="busy"]');
                            var offline_readers = $('.readers-container .readers-content[data-status="offline"]');

                            switch(d.status) {
                                case 'online':
                                if (d.ban_type!=null) {
                                    $("#div-reader-" + reader_id).html('<span>Reader signaled a '+d.ban_type+' ban.</span>');
                                } else {
                                    $("#div-reader-" + reader_id).html('<button data-href="/chat/main/index/'+username+'" class="btn-sidebar btn-chat" type="button">avaliable for chat</button>');
                                }
                                
                                if( $("#div-reader-" + reader_id).parents(".readers-content").data("status")!="online"){
                                    $("#div-reader-" + reader_id).parents(".readers-content").data("status",d.status);
                                    $("#div-reader-" + reader_id).parents(".readers-content").attr("data-status",d.status);
                                    if (online_reders.length) {

                                        $(".div-reader-" + reader_id).insertBefore( $(online_reders[0])).fadeIn().css('display','');
                                    } else {
                                        if (busy_readers.length) {
                                            $(".div-reader-" + reader_id).insertBefore( $(busy_readers[0])).fadeIn().css('display','');
                                        } else {
                                            if (break_readers.length) {
                                                $(".div-reader-" + reader_id).insertBefore( $(break_readers[0])).fadeIn().css('display','');
                                            } else {
                                                $(".div-reader-" + reader_id).insertBefore( $(offline_readers[0])).fadeIn().css('display','');
                                            }

                                    }
                                }
                            }
                                break;
                            case 'busy':

                                var xdiff_minutes = " 0 ";
                                if (d.last_pending_time!=null) {
                                    var last_pending_time = new Date(d.last_pending_time);
                                    var now = new Date();

                                    xdiff_minutes = diff_minutes(last_pending_time, now);
                                }
                                if (d.ban_type!=null) {
                                    $("#div-reader-" + reader_id).html('<span>Reader signaled a '+d.ban_type+' ban.</span>');
                                } else {
                                    /*9-20*/
                                   $("#div-reader-" + reader_id).html('<button class="btn-sidebar btn-session" type="button"> in session: '+xdiff_minutes+' mins</button>'
                                       + '<button data-href="/chat/main/wait_in_line/<?php echo $reader["username"];?>/<?php echo($reader["id"]);?>/<?php echo($this->session->userdata("member_logged")); ?>" class="btn-sidebar btn-break btn-wait-in-line" type="button">Wait In Line</button>'
                                   );
                                }
                                //console.log(d.last_pending_time);

                                    

                                    if( $("#div-reader-" + reader_id).parents(".readers-content").data("status")!="busy"){
                                        $("#div-reader-" + reader_id).parents(".readers-content").data("status",d.status);
                                        $("#div-reader-" + reader_id).parents(".readers-content").attr("data-status",d.status);
                                        if (online_reders.length) {
                                            $(".div-reader-" + reader_id).insertBefore( $(online_reders[online_reders.length-1])).fadeIn().css('display','');
                                        } else {
                                            if (break_readers.length) {
                                                $(".div-reader-" + reader_id).insertBefore( $(break_readers[0])).fadeIn().css('display','');
                                            } else {
                                                $(".div-reader-" + reader_id).insertBefore( $(offline_readers[0])).fadeIn().css('display','');
                                            }
                                        }
                                    }

                                break;
                            case 'break':
                                if (d.ban_type!=null) {
                                    $("#div-reader-" + reader_id).html('<span>Reader signaled a '+d.ban_type+' ban.</span>');
                                } else {
                                    /*9-20*/
                                   $("#div-reader-" + reader_id).html('<button class="btn-sidebar btn-break" type="button"> Be Right Back: '+ d.break_time_amount +' mins</button>'
                                       + '<button data-href="/chat/main/wait_in_line/<?php echo $reader["username"];?>/<?php echo($reader["id"]);?>/<?php echo($this->session->userdata("member_logged")); ?>" class="btn-sidebar btn-break btn-wait-in-line" type="button">Wait In Line</button>'
                                   );

                                }

                                    if( $("#div-reader-" + reader_id).parents(".readers-content").data("status")!="break"){
                                        $("#div-reader-" + reader_id).parents(".readers-content").data("status",d.status);
                                        $("#div-reader-" + reader_id).parents(".readers-content").attr("data-status",d.status);

                                        if (offline_readers.length) {
                                            $(".div-reader-" + reader_id).insertBefore( $(offline_readers[0])).fadeIn().css('display','');
                                        } else {
                                            if (busy_readers.length) {
                                                $(".div-reader-" + reader_id).insertBefore( $(busy_readers[busy_readers.length-1])).fadeIn().css('display','');
                                            } else {
                                                $(".div-reader-" + reader_id).insertBefore( $(online_reders[online_reders.length-1])).fadeIn().css('display','');
                                            }
                                        }
                                    }

                                    break;
                                case 'offline':
                                    if (d.ban_type!=null) {
                                        $("#div-reader-" + reader_id).html('<span>Reader signaled a '+d.ban_type+' ban.</span>');
                                    } else {
                                       // $("#div-reader-" + reader_id).html('<button class="btn-sidebar btn-offline" type="button"> offline</button>');
                                       $("#div-reader-" + reader_id).html('<button class="btn-sidebar btn-offline" type="button"> offline</button>'+'<button class="btn-sidebar btn-offline btn-appointment" type="button">Appointment<i class="fa fa-calendar" style="float:right;"></i></button>'+'<input type="hidden" class="reader-id" value="<?php echo($reader["id"]);?>">'
                                        +'<input type="hidden" class="client-id" value="<?php echo($this->session->userdata("member_logged")); ?>">'
                                        +'<input type="hidden" class="reader-name" value="<?php echo $reader["username"]; ?>">');
                                    }
                                
                                if( $("#div-reader-" + reader_id).parents(".readers-content").data("status")!="offline"){
                                    $("#div-reader-" + reader_id).parents(".readers-content").data("status",d.status);
                                    $("#div-reader-" + reader_id).parents(".readers-content").attr("data-status",d.status);

                                    if (offline_readers.length) {

                                        $(".div-reader-" + reader_id).insertAfter( $(offline_readers[offline_readers.length-1])).fadeIn().css('display','');

                                    } else {
                                        if (break_readers.length) {
                                            $(".div-reader-" + reader_id).insertBefore( $(break_readers[break_readers.length-1])).fadeIn().css('display','');
                                        } else {
                                            if (busy_readers.length) {
                                                $(".div-reader-" + reader_id).insertBefore( $(busy_readers[busy_readers.length-1])).fadeIn().css('display','');
                                            } else {
                                                $(".div-reader-" + reader_id).insertBefore( $(online_reders[online_reders.length-1])).fadeIn().css('display','');
                                            }
                                        }
                                    }
                                }


                                break;
                                default:
                                    console.log('undefined status');
                                break;

                            }
                        }  
                }); 
                
                
            });
            
            
            
        }

    });
</script>