
<section class="page-form">
    <div class="page-form__header">
        <h2>Meet our Professional Psychic Readers!</h2>
    </div>

    <div style='padding:20px;'>

        <div>Login to read more information about each reader and select your favorite.</div>
        <?php
        $client_id;
        if ($this->session->userdata('member_logged'))
        {
            $client_id = $this->session->userdata('member_logged');
        } else
        {
            $client_id = '';
        }
        ?>

        <div>

            <div>
                
                <div>
                    <!-- david_v4 -->
                    <form action='/psychics/search/' method='POST' class="form-horizontal search-form">
                        <div class="form-group search-form-group">
                            <div class="col-xs-10 col-sm-4 col-md-4">
                                <?= $this->site->category_select_box('category', $this->input->post('category')) ?>
                            </div>
                            <div class="col-xs-10 col-sm-6 col-md-6">
                                <input type='text' name='query' value='<?= $this->input->post('query') ?>' placeholder='Enter name, username or keyword' class='form-control input-xlarge'>						
                            </div>
                            <div class="button-group col-xs-10 col-xs-offset-0 col-sm-1 col-sm-offset-0 col-md-1 col-md-offset-0">
                                <input type='submit' value='Search' class='btn btn-primary' style='margin:0;'>
                            </div>
                        </div>
                        <input type='hidden' name='page' value='<?= $page ?>'>			
                    </form>

                </div>

                <div style="clear: both;"></div>
                
               
            </div>
            
             <div class='row'>
                    <?php
                    if (isset($readers) && $readers)
                    {
                        // pre ordering 
                        $ordered_readers = array();
                        $online_readers = array();
                        $offline_readers = array();
                        $blocked_readers = array();
                        $break_readers = array();
                        $away_readers = array();
                        $busy_readers = array();
                        $other_readers = array();
                        foreach ($readers as $r) {
                            switch ($r['status'])
                            {
                                case "online":
                                    $online_readers[] = $r;
                                    break;
                                case "offline":
                                    $offline_readers[] = $r;
                                    break;
                                case "blocked":
                                    $blocked_readers[] = $r;
                                    break;
                                case "break":
                                    $break_readers[] = $r;
                                    break;
                                case "away":
                                    $away_readers[] = $r;
                                    break;
                                case "busy":
                                    $busy_readers[] = $r;
                                    break;
                                default:
                                    $other_readers[] = $r;
                            }
                        }
                        $ordered_readers = array_merge($ordered_readers, $online_readers);
                        $ordered_readers = array_merge($ordered_readers, $offline_readers);
                        $ordered_readers = array_merge($ordered_readers, $blocked_readers);
                        $ordered_readers = array_merge($ordered_readers, $break_readers);
                        $ordered_readers = array_merge($ordered_readers, $away_readers);
                        $ordered_readers = array_merge($ordered_readers, $busy_readers);
                        $ordered_readers = array_merge($ordered_readers, $other_readers);

                        foreach ($ordered_readers as $r) {
                            $link = "#";
                            $target = "_self";
                            if ($r['status'] == "online")
                            {
                                if (!empty($r['username']))
                                {
                                    $link = "/chat/main/index/" . $r['username'];
                                    $target = "_blank";
                                }
                            }
                            ?>
                            <div class="col-lg-3">
                                <div class='reader_div'>
                                    <div style='float:left;width:110px;text-align:center;'>
                                        <a href='/profile/<?php echo $r['username']; ?>'><img src='<?php echo $r['profile']; ?>' class='profile img-polaroid' style='width:100px;'></a>

                                        <!-- chat button-->

                                        <div class='btn-group' style='margin:15px 0 0;'>
                                            <a href='$link' target='$target' class='btn btn-mini chatButton' data-username='<?php echo $r['username']; ?>'></a>
                                        </div>


                                    </div>

                                    <div class='reader-bio'>

                                        <div class='username'><a href='/profile/<?php echo $r['username'] ?>'><?php echo $r['username']; ?></a></div>

                                        <div style='padding:5px 0 0;'>
                                            <div class='description'><?php echo (strlen($r['biography']) > 130 ? substr($r['biography'], 0, 50) . "…" : $r['biography']) ?> </div>

                                            <a href='/profile/<?php $r['username']; ?>' class='btn btn-mini' style='float:right'>Read Bio</a>

                                        </div>

                                    </div>
                                    <div class='clearfix'></div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>

                        <div class='clearfix'></div>
                        <?php
                    }
                    ?>
                </div>

            
            <div style="clear: both;"></div>

        </div>

    </div>
</section>



<script src='/chat/button.js?client_id=<?php echo $client_id ?>'></script>