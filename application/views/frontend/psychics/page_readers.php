<!-- 9-3 -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.css">
<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
<style>
    .btn-appointment {
        background: #383636b3;
        margin-left: 15px;
    }
    #date {
        height: 25px;
        border-radius: 3px;
    }
    .page-header {
        margin: 0px;
        border-bottom: unset;
    }
    /*9-20*/
    .btn-wait-in-line {
        background: #0058e9;
        margin-left: 15px;
        width: 90px;
        padding-left: 0px;
        padding-right: 0px;
        text-align: center;
    }

    .wrapper {
        padding-bottom: 10.14rem !important;
    }
    body {
        font-family: "Avenir LT Std Light", Helvetica, Arial, sans-serif;
        font-weight: 300;
        font-size: 1.6rem;
    }
    /*.nav>li>a {*/
        /*padding: 5px 5px;*/
    /*}*/

    input[type=radio] {
        display: inline-flex;
        height: 15px;
        width: 15px;
    }
    .text-label {
        margin-right: 10px;
        margin-bottom: 10px;
    }
    legend {
        border-bottom: unset;
        width: unset;
    }
    fieldset {
        border: 1px solid silver;
        border-radius: 5px;
        margin: 20px 0px;
        padding: .35em .625em .75em;
    }

    td, th {
        text-align: center;
    }

    .pagination {
        display: inline-block;
        margin: 0px;
    }

    .pagination button {
        color: black;
        float: left;
        padding: 1px 10px;
        text-decoration: none;
    }

    .pagination button.active {
        background-color: #4CAF50;
        color: white;
        border-radius: 5px;
    }

    .pagination button:hover:not(.active) {
        background-color: #ddd;
        border-radius: 5px;
    }
    .pagination button, .btn-previous, .btn-next {
        cursor: pointer;
        border: 0px;
        border-radius: 5px;
        margin: 1px;
    }
    .content-pagination {
        display: inline-flex;
        float: right;
        margin-bottom: 10px;
    }
    /*.btn-previous, .btn-next {*/
    /*margin-top: 20px;*/
    /*}*/
    .btn-previous:hover, .btn-next:hover {
        background-color: #0a6aa1;
        color: white;
    }
    .btn-next {
        margin-right: 10px;
    }
    .pagination button {
        width: 35px;
    }
    select.view-number {
        background-image: linear-gradient(45deg, transparent 50%, black 50%),
        linear-gradient(135deg, black 50%, transparent 50%),
        linear-gradient(to right, #ffffff, #ffffff);
        background-position:
                calc(100% - 24px) calc(1em - 4px),
                calc(100% - 19px) calc(1em - 4px),
                100% 0;
        background-size:
                6px 7px, 6px 7px,
                2.5em 2.5em;
        background-repeat: no-repeat;
    }

    .time-size {
        width: 120px;
    }

    .topic {
         width: 200px;
        display: inline-flex;
    }
</style>

<div class="row">
    <div class="my-container page-70-30">
        <div class="content-block-30">
            <?php $this->load->view('frontend/my_account/nav/dashboard_menu'); ?>
        </div>

        <div class="content-block-70">
            <div class="row">
                <div class="col-md-12 col-sm-7">
                    <h2>Appointment List</h2>
                    <hr />
                    <div class="col-md-12">
                        <fieldset>
                            <legend>User Info</legend>
                            <h4>Name: <?= $client['username']?> (<?= $client['first_name']?>)</h4>
                            <h4>Status: Client</h4>
                            <div style="margin: 20px 0">
                                <h4>Please fill out this request form and hit the submit button</h4>
                                <h4>Readers will be notified immediately and will either contact you via our email system, or they will log on..</h4>
                            </div>
                        </fieldset>

                        <fieldset><legend>Chat ASAP</legend>
                            <label for="page-reader" class="text-label">Request Chat Session</label><input type="radio" name="page-reader" value="1" checked/>
                            <div>
                                <span for="time-size" style="font-weight: bold;">With in next: </span>
                                <select name="time-size" class="time-size">
                                    <option value="15" selected>15 minutes</option>
                                    <option value="30">30 minutes</option>
                                    <option value="60">1 hour</option>
                                </select>
                            </div>
                            <div style="margin-top: 10px;">
                                <span for="time-size" style="font-weight: bold; margin-right: 46px;">Topic: </span>
                                <input type="text" class="form-control topic" placeholder="Please insert topic"/>
                            </div>
                        </fieldset>
                        <fieldset><legend>Chat Appointment</legend>
                            <div>
                                <label for="page-reader" class="text-label">Make an appointment</label><input type="radio" name="page-reader" value="2" />
                            </div>
                            <div>
                                <span style="font-weight: bold;">Please select date</span>&nbsp;&nbsp;&nbsp;<i class="fa fa-calendar"></i>
                                <input id="datepicker" style="border-radius: 5px;">
                            </div>
                        </fieldset>
                        <div class="content-pagination" style="margin-top: 20px;">
                            <div class="view-number-content" style="">
                                <select class="view-number" style=" width: 80px;position: absolute; left: 15px; padding-left: 20px;">
                                    <option value="5">5</option>
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                </select>
                            </div>
                            <div class="pagination" style=" float: right;"></div>
                        </div>
                        <table class='table table-striped table-hover table-bordered' style="width: 100%;margin-top: 40px;">
                            <thead>
                            <tr>
                                <th style="width: 80%">Reader Name</th>
                                <th><input type="checkbox" class="select-all">&nbsp;&nbsp;<span>Select All</span></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($readers as $reader): ?>
                                <tr style="display: none;">
                                    <td><?= $reader['username']?></td>
                                    <td><input type="checkbox" class="select-one" value="<?= $reader['id']?>"/></td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                        <button class="btn btn-primary send-notification" style="margin-bottom: 30px;">Send Notification</button>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" class="client-id" value="<?php echo($this->session->userdata('member_logged')); ?>">
    </div>
</div>



<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="/theme/admin/js/notify.min.js"></script>
<script>
    $(document).ready(function(){

        // pagination
        var pagination = function(per_page) {

            var number_logs = $("tbody tr").length;
            var number_logs_per_page = per_page;
            var pages = number_logs/number_logs_per_page;
            var page_module = number_logs % number_logs_per_page;
            var group_number = 1;

            pages = Math.trunc(pages);
            $('div.pagination').html('');
            if($('button.btn-previous')) {
                $('button.btn-previous').remove();
            }
            if($('button.btn-next')) {
                $('button.btn-next').remove();
            }
            if(page_module > 0) {
                pages = pages + 1;
            }
            if(number_logs > 0) {
                $("<button class='btn-previous'>&laquo;</button>").insertBefore('.pagination');
                $("<button class='btn-next'>&raquo;</button>").insertAfter('.pagination');
            }

            for (var p=1; p<=pages; p++) {
                if(p == 1) {
                    $("div.pagination").append("<button class='active'>"+p+"</button>");
                } else {
                    $("div.pagination").append("<button>"+p+"</button>");
                }
            }

            $('.page-number').html('page '+1+' of '+pages);

            $('button.btn-previous').hover().css('cursor', 'not-allowed');
            $('button.btn-previous').prop('disabled', true);

            if(pages <= 5) {

                $('button.btn-next').hover().css('cursor', 'not-allowed');
                $('button.btn-next').prop('disabled', true);
            }

            if(pages > 5) {

                for(let n=6; n<=pages; n++) {

                    $("div.pagination button:nth-child("+n+")").css('display', 'none');
                }
            }

            $('button.btn-next').click(function () {

                group_number = group_number + 1;

                if(group_number > 1) {

                    $('button.btn-previous').hover().css('cursor', 'pointer');
                    $('button.btn-previous').prop('disabled', false);
                }
                if((pages % 5 == 0 && group_number >= pages/5) || (pages % 5 > 0 && group_number > pages/5)) {

                    $('button.btn-next').hover().css('cursor', 'not-allowed');
                    $('button.btn-next').prop('disabled', true);
                }

                for(let n=1; n<=pages; n++) {
                    if(n >= ((group_number-1) * 5 + 1) && n <= (group_number*5)) {
                        $("div.pagination button:nth-child("+n+")").css('display', 'block');
                    } else {
                        $("div.pagination button:nth-child("+n+")").css('display', 'none');
                    }
                }
                $("div.pagination").find(".active").removeClass("active");
                $("div.pagination button:nth-child("+((group_number-1)*5+1)+")").addClass("active");

                for (let i = 1; i <= number_logs; i++) {

                    $("tbody tr:nth-child(" + i + ")").css('display', 'none');
                }

                for (let j = number_logs_per_page * ((group_number-1)*5) + 1; j <= number_logs_per_page * ((group_number-1)*5+1); j++) {
                    $("tbody tr:nth-child(" + j + ")").css('display', 'table-row');
                }

                $('.page-number').html('page ' + ((group_number-1)*5+1) + ' of ' + pages);
            });

            $('button.btn-previous').click(function () {

                group_number = group_number - 1;

                if(group_number == 1) {

                    $('button.btn-previous').hover().css('cursor', 'not-allowed');
                    $('button.btn-previous').prop('disabled', true);
                }

                if((pages % 5 == 0 && group_number < pages/5) || (pages % 5 > 0 && group_number <= pages/5)) {

                    $('button.btn-next').hover().css('cursor', 'pointer');
                    $('button.btn-next').prop('disabled', false);
                }

                for(let n=1; n<=pages; n++) {
                    if(n >= ((group_number-1) * 5 + 1) && n <= (group_number*5)) {
                        $("div.pagination button:nth-child("+n+")").css('display', 'block');
                    } else {
                        $("div.pagination button:nth-child("+n+")").css('display', 'none');
                    }
                }
                $("div.pagination").find(".active").removeClass("active");
                $("div.pagination button:nth-child("+((group_number-1)*5+1)+")").addClass("active");

                for (let i = 1; i <= number_logs; i++) {

                    $("tbody tr:nth-child(" + i + ")").css('display', 'none');
                }

                for (let j = number_logs_per_page * ((group_number-1)*5) + 1; j <= number_logs_per_page * ((group_number-1)*5+1); j++) {
                    $("tbody tr:nth-child(" + j + ")").css('display', 'table-row');
                }

                $('.page-number').html('page ' + ((group_number-1)*5+1) + ' of ' + pages);
            });


            if (number_logs > number_logs_per_page * 1) {

                for (var i = 1; i <= number_logs_per_page * 1; i++) {

                    $("tbody tr:nth-child(" + i + ")").css('display', 'table-row');
                }
            } else {

                for (var i = 1; i <= number_logs; i++) {

                    $("tbody tr:nth-child(" + i + ")").css('display', 'table-row');
                }
            }

            var page = 1;


            $(".pagination button").click(function () {

                page = parseInt($(this).text());
                if(page == pages) {

                    console.log('click last page');
                } else {

                    $("#btn-load").attr("disabled", false);
                }

                console.log("current page = "+page);

                $("div.pagination").find(".active").removeClass("active");
                $("div.pagination button:nth-child("+page+")").addClass("active");

                for (var i = 1; i <= number_logs; i++) {

                    $("tbody tr:nth-child(" + i + ")").css('display', 'none');
                }

                if (number_logs > number_logs_per_page * page) {

                    for (var i = 1 + number_logs_per_page * (page - 1); i <= number_logs_per_page * page; i++) {

                        $("tbody tr:nth-child(" + i + ")").css('display', 'table-row');
                    }

                } else {

                    for (var i = 1 + number_logs_per_page * (page - 1); i <= number_logs; i++) {

                        $("tbody tr:nth-child(" + i + ")").css('display', 'table-row');
                    }

                }
                $('.page-number').html('page ' + page + ' of ' + pages);

            });
        }

        pagination(5);

        $('#username-th').click(function () {
            $('.filter-table-form-username').submit();
        });

        $('#datetime-th').click(function () {
            $('.filter-table-form-datetime').submit();
        });
        $('.view-number').change(function () {
            pagination($(this).val());
            $(".pagination button:nth-child(1)").click();
        })

        /*9-3*/
        var notification_type = 1;
        var selected_readers = [];

        $("input[name=page-reader]").change(function () {
            if (this.value == '1') {
                notification_type = 1;
                console.log(notification_type)
            }
            else if (this.value == '2') {
                notification_type = 2;
                console.log(notification_type)
            }
        });

        $("#datepicker").datetimepicker();

        $('.select-all').change(function() {

            if(this.checked) {

                $('.select-one').prop('checked', true);
                selected_readers = [];
                $( "tbody tr" ).each(function() {
                    selected_readers.push($(this).find(".select-one").val());
                });
                console.log(selected_readers);
            }

            if(!this.checked) {

                $('.select-one').prop('checked', false);
                selected_readers = [];
                console.log(selected_readers);
            }
        });

        $('.select-one').change(function () {
            if(this.checked) {
                selected_readers.push($(this).val());
            } else {
                selected_readers.splice(selected_readers.indexOf($(this).val()), 1);
            }
            console.log(selected_readers);
        })

        $(".send-notification").click(function () {

            if(selected_readers.length > 0) {

                if(notification_type == 1) {

                    $.ajax({
                        'type': "POST",
                        'url': "/chat/main/set_chat_request",
                        'data': { reader_id : selected_readers, client_id: $("input.client-id").val(),time_size: $('.time-size').val(),topic: $(".topic").val()},
                        success: function (data) {
                            $.notify("Sent Chat Request Successfully", {
                                className: "success",
                                hideDuration: 700,
                                autoHideDelay: 60000,
                                position: 'right top'
                            });
                        }
                    });
                }

                if(notification_type == 2) {

                    $.ajax({
                        'type': "POST",
                        'url': "/chat/main/set_multi_appointment",
                        'data': { reader_id : selected_readers, client_id: $("input.client-id").val(), date: $("#datepicker").val()},
                        success: function (data) {
                            $.notify("Sent Appointment Request Successfully", {
                                className: "success",
                                hideDuration: 700,
                                autoHideDelay: 60000,
                                position: 'right top'
                            });
                        }
                    });
                }
            } else {

                $.notify("Please select at least one reader.", {
                    className: "error",
                    hideDuration: 700,
                    autoHideDelay: 60000,
                    position: 'right top'
                });
            }

        })

    });
</script>