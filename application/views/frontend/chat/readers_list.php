<div class="emd-container ">
    <div class="row">

        <?php foreach ($readers as $reader): ?>
            <article class="col-md-3 col-sm-4 col-xs-6 person" style="height: 261px;">
                <div class="person-thumb in">
                    <a href="javascript:void(0)" title="<?php echo $reader->first_name; ?>" class="btn-chat-now" data-reader-uname="<?php echo $reader->username; ?>" data-reader-id="<?php echo $reader->id; ?>">
                        <div style="border-color:#e97def" class="person-img circle">
                            <?php if(trim($reader->profile_image) != '' && @getimagesize(SITE_URL . 'media/assets/' . $reader->profile_image)): ?>
                                <img class="emd-img thumb" src="<?php echo SITE_URL . 'media/assets/' . $reader->profile_image; ?>" width="150" height="150" alt="">
                            <?php else: ?>
                                <img class="emd-img thumb" src="<?php echo SITE_URL . 'media/assets/no-image-available.jpg'; ?>" width="150" height="150" alt="">
                            <?php endif; ?>
                        </div>
                    </a>
                    <div class="person-tag text-center">
                        <a href="javascript:void(0)" class="btn-chat-now person-name" data-reader-uname="<?php echo $reader->username; ?>" data-reader-id="<?php echo $reader->id; ?>" title="Holly"><?php echo $reader->first_name; ?></a>  
                    </div>
                </div>
            </article>
        <?php endforeach; ?>

    </div>
</div>