<style>
    .form-control {
        height: auto;
        margin: 5px;
    }
    .inline-form-control {
        margin: 5px;
    }
    input.form-control {
        width: 250px;
    }
    .well {
        margin: 15px;
        padding: 15px;
    }
    span.comment {
        color: red;
    }
</style>
<div>
    <?php
    if($this->session->flashdata('error'))
    {
        echo "<div class='alert alert-error page-notifs'><strong>There are errors:</strong><p>".$this->session->flashdata('error')."</p></div>";
    }
    ?>

<h2>Add A New Billing Profile</h2>
<div style='padding:5px 0 0;'>To add a credit card to your account, fill out the form below. You will be able to use this credit/debit card to fund your account. <b>Please Note:</b> To verify your card, we will authorize $1.00. This authorization will be refunded back to you within 7-10 business days.</div>

<hr />

<form class="form-control" action='/my_account/transactions/submit_billing_profile/<?=$merchant_type?>/<?=$this->uri->segment(5)?>/<?=$this->uri->segment(6)?>' method='POST'>

    <input type="hidden" name="sku" value="<?= $pinfo['title']?>" />
    <input type="hidden" name="sku_denomination" value="<?= $pinfo['value']?>" />
    <input type="hidden" name="order_number" value="<?= $order_number?>" />
    <input type="hidden" name="payment_source" value="Add Fund" />
    <input type="hidden" name="site" value="PSYCHIC-CONTACT.COM" />

    <table width='100%' cellPadding='10'>

        <tr>
            <td width='150'><b><span class="comment">* </span>Credit Card Number:</b></td>
            <td><input class="form-control" type='text' name='cc_num' value='<?=set_value('cc_num')?>' required></td>
        </tr>

        <tr>
            <td width='150'><b><span class="comment">* </span>Expiration Date:</b> EST</td>
            <td><?=$this->system_vars->exp_month("exp_month", set_value('exp_month'))?> / <?=$this->system_vars->exp_year("exp_year", set_value('exp_year'))?></td>
        </tr>

        <tr>
            <td width='150'><b><span class="comment">* </span>CVV code:</b></td>
            <td><input class="form-control" type='text' name='cc_cvv' value='<?=set_value('cvv_code')?>'><span class="comment" required>&nbsp;* Last 3 digits on the back of your card</span></td>
        </tr>

        <tr><td colSpan='2'><hr /></td></tr>

        <tr>
            <td width='150'><b><span class="comment">* </span>Total Purchase Amount:</b></td>
            <td><input class="form-control" type='text' name='total' value='<?=$pinfo['price']?>' readonly></td>
        </tr>

        <tr>
            <td width='150'><b>Promo Code / Coupon:</b></td>
            <td><input class="form-control" type='text' name='coupon' value='<?=set_value('coupon')?>'><span class="comment">&nbsp;* Please don't forget to use your Promo Code if you have one.</span>

            </td>
        </tr>

        <tr><td colSpan='2'><hr /></td></tr>

        <tr>
            <td width='150'><b><span class="comment">* </span>First Name:</b></td>
            <td><input class="form-control" type='text' name='first_name' value='<?=$user['first_name']?>' readonly></td>
        </tr>

        <tr>
            <td width='150'><b><span class="comment">* </span>Last Name:</b></td>
            <td><input class="form-control" type='text' name='last_name' value='<?=$user['last_name']?>' readonly></td>
        </tr>

        <tr>
            <td width='150'><b><span class="comment">* </span>Address:</b></td>
            <td><input class="form-control" type='text' name='address' value='<?=set_value('address')?>' required></td>
        </tr>

        <tr>
            <td width='150'><b><span class="comment">* </span>City:</b></td>
            <td><input class="form-control" type='text' name='city' value='<?=set_value('city')?>' required></td>
        </tr>

        <tr>
            <td width='150'><b><span class="comment">* </span>State / Province:</b></td>
            <td><input class="form-control" type='text' name='state' value='<?=set_value('state')?>' required></td>
        </tr>

        <tr>
            <td width='150'><b><span class="comment">* </span>Zip / Postal Code:</b></td>
            <td><input class="form-control" type='text' name='zip' value='<?=set_value('zip')?>' required></td>
        </tr>

        <tr>
            <td width='150'><b><span class="comment">* </span>Country:</b></td>
            <td><?=$this->system_vars->country_array_select_box('country', set_value('country'))?></td>
        </tr>

        <tr><td colSpan='2'><hr /></td></tr>

        <tr>
            <td colSpan='2'>
                <div class="well">
                    <table cellpadding="10">
                        <tr>
                            <td colspan="3">
                                <b>Package Information</b>
                            </td>
                        </tr>
                        <tr style="font-weight:bold;">
                            <td>
                                Type
                            </td>
                            <td>
                                Title
                            </td>
                            <td>
                                Price
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?=$pinfo['type'] ?>
                            </td>
                            <td>
                                <?=$pinfo['title'] ?>
                            </td>
                            <td>
                                $<?=number_format($pinfo['price'],2)?>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>

        <tr>
            <td width='150'>&nbsp;</td>
            <td>

                <input type='submit' name='submit' value='Checkout' class='btn btn-large btn-primary'>
                <a href='/chat/main/purchase_time/<?=$reader_id?>/<?=$chat_id?>' class='btn btn-large btn-link'>Cancel</a>

            </td>
        </tr>

    </table>

</form>
</div>