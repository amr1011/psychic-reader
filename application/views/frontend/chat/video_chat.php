<!-- david_v3 -->
<?//$ts = 3;
    if (!isset($ts)) {
        $ts = time();
    }
?>
<html>
  <head>
    <title>Psycontact Video Chat</title>
    <link type="text/css" href="/media/video_chat/css/style.css" rel="stylesheet"></link>
	<link rel="stylesheet" href="/media/font-awesome-4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="/media/video_chat/css/bootstrap.css">
	<link rel="stylesheet" href="/media/video_chat/css/Lobibox.min.css">
	<script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript"></script>
	<script src="<?=CHAT_URL ?>:<?=CHAT_PORT?>/socket.io/socket.io.js"></script>
	<script src="/chat/app/chat_all_room.js?ts=<?=$ts ?>" ></script>	
    <script src="/media/video_chat/js/webrtc.io.js"></script>
   <? if($this->session->userdata('member_logged')): ?>

        <? $socket_url = "http://dev.psychic-contact.com"; ?>
        <? $socket_port = 3701 ?>
        <? $mem_id = $member['id']; ?>
        <? $member_username = $this->member->data['username']; ?>
        
       
        <script>
            var chat_init_data ={
                'chat_title'        : '<?=$title?>',
                'member_type'       : '<?=$member_type?>',
                'member_id'         : '<?=$mem_id?>',
                'member_hash'       : '<?=$member_hash?>',
                'member_username'   : '<?=$member_username?>',
                //'_disconnect_url'   : '<?=$this->config->item('site_url') . "/main/disconnect_user/" . $mem_id?>',
                'socket_url'        : '<?=CHAT_URL ?>',
                'socket_port'       : '<?=CHAT_PORT ?>',
                // now room data
                'chat_id'           : '<?=$id ?>',
                'chat_session_id'   : '<?=$chat_session_id ?>',
                'max_chat_length'     :  <?=floor($max_chat_length)?>,
                'chat_length'       :  <?=floor($chat_length)?>,
                'time_balance'       :  <?=floor($time_balance)?>,
                'client_username'  :  "<?=$client['username'] ?>",
                'client_first_name' :  "<?=$client['first_name'] ?>",
                'client_last_name'  :  "<?=$client['last_name'] ?>",
                'client_dob'        :  "<?=date("m/d/Y", strtotime($client['dob']))?>",
                
                'reader_username'  :  "<?=$reader['username'] ?>",
                
                
                'is_mobile'         : 0
            };
            
        </script>        
    <? endif; ?>
  </head>
  <body onload="init()">
    <div id="videos">     
      <video id="you" class="flip" autoplay width="150px" height="auto" style="position: absolute; right: 0px; top: 0px; z-index:9;"></video>
    </div>    
    <div class="buttonBox">
    	<div id="pauseChat" class=""><i class="fa fa-pause"></i></div>	  
	    <div id="mute" class=""><i class="fa fa-microphone"></i></div>
	    <div id="endChat" class="" data-socketId="" title="End Video Call"><i class="fa fa-phone fa-rotate-135"></i></div>
    </div>
    
    <script src="/media/video_chat/js/script.js"></script>
    <script src="/media/video_chat/js/lobibox.min.js"></script>
    <script>
    	Chat.run.room();
    	setInterval(Chat.manager.room.timer.run_chat_timer, Chat.manager.room.timer.chat_timer_interval );    	
    </script>
  </body>
</html>