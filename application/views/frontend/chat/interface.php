<?php
if (!isset($ts))
{
    $ts = time();
}
?>

<script src="<?= CHAT_URL ?>:<?= CHAT_PORT ?>/auth.js"></script>
<script src="<?= CHAT_URL ?>:<?= CHAT_PORT ?>/socket.io/socket.io.js"></script>


<link rel=stylesheet type="text/css" href="/media/css/chat.css?ts=<?= $ts ?>">
<link rel="stylesheet" href="/media/font-awesome-4.4.0/css/font-awesome.min.css"><!-- david_v1 -->
<link type="text/css" href="/media/video_chat/css/style.css" rel="stylesheet"></link><!-- david_v2 -->
<link rel="stylesheet" href="/media/video_chat/css/Lobibox.min.css" type="text/css">

<link rel="stylesheet" href="/chat/editor.css" type="text/css">

<!-- color picker -->
<!-- <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet"> -->
<link href="/media/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
<link href="/media/dist/css/bootstrap-colorpicker-plus.css" rel="stylesheet">
<!-- color picker -->

<?php if ($detect->isMobile()) : ?>
    <?php $is_mobile = 1; ?>
    <link rel="stylesheet" media="all and (orientation:portrait)" href="/media/css/iphone-portrait.css?ts=<?= $ts ?>">
    <link rel="stylesheet" media="all and (orientation:landscape)" href="/media/css/iphone-landscape.css?ts=<?= $ts ?>">
<?php else: ?>

    <?php $is_mobile = 0; ?>
    <link rel=stylesheet type="text/css" href="/media/css/web.css?ts=<?= $ts ?>">
<?php endif; ?>
    
    <input type="hidden" id="btnYesPaused" value="0" />
    <input type="hidden" id="warningFreeTime" value="0" />
    <input type="hidden" id="stopWatch" value="0" />
    <input type="hidden" id="allowFreeTime" value="<?php echo $client_allow_freetime; ?>" />
    <input type="hidden" id="addtime_mode" value="0" />
    <input type="hidden" id="selectFreeTime" value="0"/>
    <input type="hidden" id="selectAddedFreeTime" value="0"/>
    <input type="hidden" id="warning-one-minute" value="0"/>
    <input type="hidden" id="warning-two-minute" value="0"/>
    <input type="hidden" id="watchStandbyoff" value="0" />
    <input type="hidden" id="freetimeadded" value="0" />
    <style>
        .fs-18, textarea {
            font-size: 18px;
        }
        .btn {
            font-size: 18px;
            padding: 9px 27px 9px 27px;
        }
        #chat_window .interior .timer, #chat_window {
            font-size: 18px !important;
        }
        
        .navbar-inverse .brand, .navbar-inverse .nav > li > a,
        .navbar-inverse .brand{
            color: #FFFFFF;
        }
        
        .navbar-inverse .brand:hover, .navbar-inverse .nav > li > a:hover,
        .navbar-inverse .nav > li > a:focus, .navbar-inverse .nav > li > a:hover{
            color: #b0e2b2 !important;
        }
        
        .dropdown-menu li > a {
            padding: 7px 20px !important;
        }
        
        .isTyping {
            font-size: 18px !important;
            font-weight: bold;
        }
        .fullBanUserAnchor {
            font-weight: bold;
            color: red !important;
        }
        
        .table-borderless td, .table-borderless th {
            border: none;
        }
        
        .digital_font {
                border: 3px solid #424141;
                display: inline-block;
                padding: 2px 5px 2px 5px;
                border-radius: 7px;
                font-family: monospace;
                /*color: red;*/
        }
        
        .lh-30 {
            line-height: 30px;
        }
        .lh-50 {
            line-height: 50px;
        }
        
        .navbar .divider-vertical {
            height: 50px;
        }
        
        .caret {
            display: inline-block;
            width: 0;
            height: 0;
            vertical-align: top;
            border-top: 11px solid #000000;
            border-right: 8px solid transparent;
            border-left: 8px solid transparent;
            margin-top: 10px !important;
            content: "";            
        }
        
        .navbar .nav > li > a {
            padding: 10px 0 10px;
        }
        
        #chatMenu {
            height: 60px;
        }
        
        .chat_reader > .chat_username {
            color: #004cfc;
        }
        .chat_client > .chat_username {
            color: #000;
        }
        
        .emo-img {
            float:left;
            width:20px;
            margin-right:20px;   
        }
        
        [contenteditable]{
            width:100%;
            height:40px;
            background:#FFF;
            line-height:1em;
            font-size:0.8em;
            -webkit-appearance: textfield;
            appearance: textfield;
            padding: 10px;
            border-radius: 5px;
            font-size: 18px;
            overflow-y: scroll;
        }

        div.Editor-editor, div#statusbar_input_message {
            display: none;
        }

        div#input_message {
            display: block !important;
            overflow-y: hidden !important;
            width: 98% !important;
            border-bottom: 1px gray solid;
        }

        /*color picker*/
        .color-fill-icon{display:inline-block;width:16px;height:16px;border:1px solid #000;background-color:#fff;margin: 2px;}
        .dropdown-color-fill-icon{position:relative;float:left;margin-left:0;margin-right: 0}
        .well .markup{
            background: #fff;
            color: #777;
            position: relative;
            padding: 45px 15px 15px;
            margin: 15px 0 0 0;
            background-color: #fff;
            border-radius: 0 0 4px 4px;
            box-shadow: none;
        }

        .well .markup::after{
            content: "Example";
            position: absolute;
            top: 15px;
            left: 15px;
            font-size: 12px;
            font-weight: bold;
            color: #bbb;
            text-transform: uppercase;
            letter-spacing: 1px;
        /*color picker*/
        }

        .red {
            color: #3e903e !important;
        }

        /*9-2*/
        /*9-23*/
        #reader-note, #admin-reply-txt {

            border: none;
            overflow: auto;
            outline: none;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: none;
            width:94%; 
            height: 96%; 
            resize: none;
            background-image: -webkit-linear-gradient(left, white 10px, transparent 10px), -webkit-linear-gradient(right, white 10px, transparent 10px), -webkit-linear-gradient(white 30px, #ccc 30px, #ccc 31px, white 31px);
            background-image: -moz-linear-gradient(left, white 10px, transparent 10px), -moz-linear-gradient(right, white 10px, transparent 10px), -moz-linear-gradient(white 30px, #ccc 30px, #ccc 31px, white 31px);
            background-image: -ms-linear-gradient(left, white 10px, transparent 10px), -ms-linear-gradient(right, white 10px, transparent 10px), -ms-linear-gradient(white 30px, #ccc 30px, #ccc 31px, white 31px);
            background-image: -o-linear-gradient(left, white 10px, transparent 10px), -o-linear-gradient(right, white 10px, transparent 10px), -o-linear-gradient(white 30px, #ccc 30px, #ccc 31px, white 31px);
            background-image: linear-gradient(left, white 10px, transparent 10px), linear-gradient(right, white 10px, transparent 10px), linear-gradient(white 30px, #ccc 30px, #ccc 31px, white 31px);
            background-size: 100% 100%, 100% 100%, 100% 31px;
            border-radius: 8px;
            box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
            line-height: 31px;
            font-family: Arial, Helvetica, Sans-serif;
            color: black;
            font-size: 16px;

        }
        /*9-23*/
        #btn-save, #btn-admin-reply {
            float: right;
            border: 1px solid black;
            padding: 0px 3px 0px 3px;
            font-size: 14px;
            background: black;
            color: white;
        }

        /*9-18*/
        .lobibox-notify-wrapper-large.bottom, .lobibox-notify-wrapper.bottom {
            bottom: 20px !important;
        }
        /*9-23*/
        .contactlist-formbox-head-admin-reply {
            background-color: #000;
            color: #fff;
            padding: .5em 1em;
            border-radius: .5em .5em 0 0;
            cursor:pointer;
        }
        .contactlist-formbox-body-admin-reply {
            background-color: #fff;
            border: 1px solid #000;
            height: 22em;
            display: none;
            overflow-y: scroll;
        }
        .online_icon {
            position: absolute;
            height: 7px;
            width: 7px;
            background-color: #4cd137;
            border-radius: 50%;
            bottom: -5px;
            right: -2px;
            border: 1.5px solid white;
        }
        .well {
            /*padding: 0px;*/
            /*margin-bottom: 0px*/
        }
        #footer {
            /*margin-top: 0px;*/
        }
        .navbar {
            /*margin-bottom: 0px;*/
        }
    </style>
    
<?php if ($this->session->userdata('member_logged')): ?>

    <?php $socket_url = "http://dev.psychic-contact.com"; ?>
    <?php $socket_port = 3701 ?>
    <?php $mem_id = $member['id']; ?>
    <?php $member_username = $this->member->data['username']; ?>


    <script>
        var chat_init_data = {
            'chat_title': '<?= $title ?>',
            'member_type': '<?= $member_type ?>',
            'member_id': '<?= $mem_id ?>',
            'member_hash': '<?= $member_hash ?>',
            'member_username': '<?= $member_username ?>',
            //'_disconnect_url'   : '<?= $this->config->item('site_url') . "/main/disconnect_user/" . $mem_id ?>',
            'socket_url': '<?= CHAT_URL ?>',
            'socket_port': '<?= CHAT_PORT ?>',
            // now room data
            'chat_id': '<?= $id ?>',
            'chat_session_id': '<?= $chat_session_id ?>',
            'max_chat_length': <?= floor($max_chat_length) ?>,
            'chat_length': <?= floor($chat_length) ?>,
            'time_balance': <?= floor($time_balance) ?>,
            'client_username': "<?= $client['username'] ?>",
            'client_first_name': "<?= $client['first_name'] ?>",
            'client_last_name': "<?= $client['last_name'] ?>",
            /*9-8*/
            'client_id': "<?= $client['id'] ?>",
            
            'client_dob': "<?= date("m/d/Y", strtotime($client['dob'])) ?>",
            'reader_username': "<?= $reader['username'] ?>",
            /*9-16*/
            'reader_id': "<?= $reader['id'] ?>",

            'is_mobile': <?= $is_mobile ?>,
            'site_url': '<?= SITE_URL ?>',
            'tier' : '<?= $tier; ?>',
            'client_total_balance' : <?= $client_total_balance?>,
            'client_total_free_balance' : <?= $client_total_free_balance?>
        };

    </script>    
<?php endif; ?>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/1.4.11/jquery.scrollTo.min.js"></script>


<div id="chat_room">
    <!-- Navigation Bar -->
    <div id='navbar' class="navbar navbar-inverse fs-18">
        <div class="navbar-inner">
            <div class="container">
                <input type="hidden" id="member_type" value="<?php echo($member_type);?>">
                <?php if ($member_type == 'client'): ?>
                    <?php $chat_with = $reader['username']; ?>
                <?php else : ?>                
                    <?php $chat_with = $client['username']; ?>
                <?php endif; ?>
                <a class="brand lh-30" href="#">
                    <div class="img-content" style="position: relative; width: 35px; height: 35px; float: left;  margin-right: 10px;">
                        <img src="<?=$reader['profile_image']?>" style="width: 35px; border-radius: 30px;border: solid 2px white;"/>
                        <span class="online_icon"></span>
                    </div>

                    <span class="chat_with_text" style="position: relative; top: 5px;">Chat With <span class='client_first_name'><?= $chat_with ?></span></span></a>

                <ul id='chatMenu' class="nav pull-right">
                    <!--<li class="lh-50"><div id="startVchat" title="video call"><i class="fa fa-video-camera"></i></div></li> david_v2 -->
                    <li class="divider-vertical lh-30"></li>
                    <?php if ($member_type == 'reader') : ?>
                        <li id='clientInfo' class='dropdown lh-30'>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">Client Info <b class="caret"></b></a>
                            <div class="dropdown-menu">

                                <div style='padding:0 10px 10px;width:250px;'>
                                    <legend><span class='client_username'><?php echo $client['username'] ?></span><?php if($client['annonymous'] == 1) {?>
                                    <span style="color: red; font-size: 20px;">!!!!</span><?php }?>
                                    </legend>
                                    <table class="table table-borderless">
                                        <tr>
                                            <td width='75'>DOB:</td>
                                            <td><span class='client_dob'><?php echo date("m/d/Y", strtotime($client['dob'])) ?></span></td>
                                        </tr>

                                        <tr>
                                            <td width='75'>Name:</td>
                                            <td>
                                                <span class='client_first_name'><?php echo $client['first_name'] ?></span> 
                                                <!--<span class='client_last_name'><?= $client['last_name'] ?></span>-->
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td width='75'>Gender:</td>
                                            <td>
                                                <span class='client_first_name'><?php echo $client['gender'] ?></span>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td width='75'>Sign-up Date:</td>
                                            <td><span class='client_first_name'><?php echo $client['registration_date'] ?></span></td>
                                        </tr>

                                        <tr>
                                            <td width='75'>Topic:</td>
                                            <td><span><?php echo $topic ?></span></td>
                                        </tr>
                                        <tr>
                                            <td width='75'>Mins:</td>
                                            <td><span><?php echo (floor($max_chat_length)/60) ?> Min.</span></td>
                                        </tr>
                                    </table>
                                    <div style="width: 160px; float: left;">Warn Next Reader:</div>
                                
                                    <a href="/chat/ChatInterface/set_annonymous/<?php echo $client['id'] ?>" class="btn btn-danger" style="margin-top: -5px;" type="button">OK</a>
                                </div>

                            </div>
                        </li>
                        <li class="divider-vertical lh-50"></li>
                    <?php endif; ?>                    
                    <li>
                        <a>
                            <span id='timerSpan' class="digital_font"><?php echo floor($max_chat_length/60) ?></span>
                            <div style="font-size: 10px;text-align: center;" class="timer-label">Paid Time</div>
                        </a>
                    </li>
                    <li class="divider-vertical"></li>
                    <li>
                        <a>
                            <span id='timerSpanFree' class="digital_font"><?= $client_free_time_balance?></span>
                            <div style="font-size: 10px;text-align: center;" class="timer-label">Free Time</div>
                        </a>
                    </li>
                    
                    <li class="divider-vertical"></li>
                    <li class="dropdown chat_option lh-30">
                        <a id='settings_menu' data-toggle="dropdown" class="dropdown-toggle" href="#">Options <b class="caret"></b></a>
                        <ul id='settingsDropDownMenu' class="dropdown-menu">
                            <?php if ($member_type == 'client') : ?>
                                <!--<li><a href="#" class='pauseChatAnchor' style='display:none;'>Pause Chat</a></li> -->
                                <li><a href="javascript:void(0)" class='addStoredTime' style='display:none;'>Add More Time</a></li>
                                <li><a href="javascript:void(0)" class='resumeChatAnchor' style='display:none;'>Resume Chat</a></li> 
                                <li><a href="javascript:void(0)" class='contactAdmin' data-toggle='modal' data-target='#contactAdminModal'>Contact Admin</a></li>
                                <!--<li><a href="#" class='purchaseMoreTime'>Purchase More Time</a></li> -->


                            <?php else : ?>
                                <li><a href="javascript:void(0)" class='pauseChatAnchor' style='display:block;'>STANDBY</a></li>
                                <li><a href="javascript:void(0)" class='resumeChatAnchor' style='display:none;'>STANDBY to OFF</a></li>
                                <!--<li><a href="#" class='refundChatAnchor' style='display:block;'>Return Chat Time</a></li>-->
                                <li><a href="javascript:void(0)" class='addLostTime' style='display:block;'>ADD LOST</a></li> 
                                <!-- 9-9 -->
                                <li><a href="javascript:void(0)" class='addFavorite' style='display:none;'>Favorite</a></li>
                                <li><a href="javascript:void(0)" class='addUnfavorite' style='display:none; color:red;'>Unfavorite</a></li>

                                <li><a href="javascript:void(0)" class='addFreeTime' style='display:block;'>Free Time "Gift"</a></li>
                                <li><a href="javascript:void(0)" class='addFreeEmail' style='display:block;'>Free Email Reading "Gift"</a></li>
                                <li><a href="javascript:void(0)" class='personalBanUserAnchor' style='display:block;'>Personal Ban</a></li>
                                <!--<li><a href="#" class='fullBanUserAnchor' style='display:block;'>Full Ban User</a></li>-->
                            <?php endif; ?>
                        </ul>
                    </li>
                    <?php if ($member_type != 'client') : ?>
                    <li class="divider-vertical"></li>
                    <li><a href="javascript:void(0)" class='fullBanUserAnchor lh-30' style='display:block;'>FULL BAN!</a></li>
                    <?php endif; ?>
                    <li class="divider-vertical"></li>
                    <li class="chat_end lh-30"><a href="#" class='endChatAnchor' style='display:block;'>End Session</a></li>
                </ul>

            </div>
        </div>
        <div style="display:none;" id="lostTime">
            <input style="width:150px;margin-right:20px;margin-top:16px;" type="text" name="losttime" placeholder="Lost Time (in Minute)">
            <input id="lostTimeBtn" type="button" class="btn btn-primary" value="Submit">
            <input id="RemoveLostTimeBtn" type="button" class="btn removeLostTime" style="margin-left: 10px" value="Cancel">
        </div>
        <?php if (!$detect->isMobile()): ?>
            <div style="display:none;" id="addStoredTime">
                <div style="width:700px; margin-top:5px;">
                    <div id="remainingStoredTimeInput_div">
                        <!--<label for="remainingStoredTimeInput" id="remainingStoredTimeInput_label">Remaining Stored Time (Minute): </label>-->
                        <label for="remainingStoredTimeInput" id="remainingStoredTimeInput_label">Amount of Stored Mins to add: </label>
                    </div>
                    <input type="hidden" id="remainingStoredTimeInput"  style="border:0; color:#f6931f; font-weight:bold; width:50px; padding-top:0px"> 
                    <!--
                    <div style="float:left;">
                        <span style="vertical-align: top">Use </span>
                        <input type="text" id="remainingStoredTimeInput"  style="border:0; color:#f6931f; font-weight:bold; width:50px; padding-top:0px"> 
                        <span style="vertical-align: top">of </span> <span id="maxAddStoredTime" style="vertical-align: top">0</span><span style="vertical-align: top"> minutes</span>
                    </div>
                    <div style="float:left; margin-left: 10px;"><img id="addTimeHelp" src="/media/images/iconQuestionMark.gif" data-toggle="tooltip" title="Set the minutes from your account, then click 'Submit' to add on this chat session. If you want to add more minutes to your account, then click 'Buy Time'.  After more time is purchased, you can add time to this chat session. " ></div>
                    -->
                </div>
                <div style="clear:both"></div>
                <div>
                    <!--
                    <input style="width:150px;margin-right:20px;margin-top:16px;" type="text" name="add_stored_time" placeholder="Add Stored Time (in Minute)">
                    -->
                    <div>
                        <div style="float:left; width:215px; padding-top:13px">
                            <!--<div id="remaining_stored_time-slider-range-max" ></div>-->

                            <select id="sel_remaining_stored_time" name="sel_remaining_stored_time">
                                <?php for($i=5; $i < (($client_total_balance - $max_chat_length))/60; $i+=5): ?>

                                <option value="<?=$i?>"><?=$i; ?></option>

                                <?php endfor; ?>
                            </select>
                        </div>
                        
                        <div style="float:left; margin-left:20px">
                            <input id="addStoredTimeBtn" type="button" class="btn btn-primary" value="Submit">
                        </div>
                        <div style="float:left; margin-left:5px">
                            <input id="cancelAddStoredTimeBtn" type="button" class="btn cancelAddStoredTimeBtn" style="margin-left: 10px" value="Cancel">
                        </div>
                        <div style="float:left; width:50px;">

                        </div>
                        <div style="float:left;width: 74%;text-align: center;">
                            <a id="purchaseMoreTimeLink" href="/chat/main/purchase_time/<?= $reader['id'] ?>/<?=$id;?>" target="_blank">
                                <input id="purchaseMoreTimeBtn" type="button" class="btn" style="margin-left: 10px" value="Buy Time">
                            </a>
                        </div>
                        <div id="refreshPurchasedTime" style="float:left; margin-left:5px; display:none">
                            <input id="refreshPurchasedTimeBtn" type="button" class="btn" value="Refresh Stored Time">
                        </div>     
                    </div>
                    <div style="clear:both"></div>
                </div>
            </div>
        <?php else: ?>

            <div id="addStoredTime" style="display:none; margin-left:5px;">
                <div style="width:700px; margin-top:5px;">
                    <div id="remainingStoredTimeInput_div">
                        <label for="remainingStoredTimeInput" id="remainingStoredTimeInput_label">Remaining Stored Time (Minute): </label>
                    </div>
                    <div style="clear:both"></div>
                    <div>
                        <span style="vertical-align: top; font-size:11px;">Use </span><input type="text" id="remainingStoredTimeInput"  style="border:1px #999 solid; color:#f6931f; font-weight:bold; width:50px; padding-top:2px; height:14px;"> <span style="vertical-align: top; font-size:11px;">of </span> <span id="maxAddStoredTime" style="vertical-align: top; font-size:11px;">0</span><span style="vertical-align: top; font-size:11px;"> minutes</span>
                    </div>
                </div>
                <div style="clear:both"></div>
                <div>
                    <div>
                        <div style="float:left; margin-left:0px">
                            <input id="addStoredTimeBtn" type="button" class="btn btn-primary" value="Submit">
                        </div>
                        <div style="float:left; margin-left:5px">
                            <input id="cancelAddStoredTimeBtn" type="button" class="btn cancelAddStoredTimeBtn" style="margin-left: 10px" value="Cancel">
                        </div>
                        <div style="float:left; margin-left:0px">
                            <a id="purchaseMoreTimeLink" href="/chat/main/purchase_time/<?= $reader['username'] ?>/<?=$id;?>" target="_blank">
                                <input id="purchaseMoreTimeBtn" type="button" class="btn" style="margin-left: 10px" value="Buy Time">
                            </a>
                        </div>
                        <div id="refreshPurchasedTime" style="float:left; margin-left:5px; display:none">
                            <input id="refreshPurchasedTimeBtn" type="button" class="btn" value="Refresh Time">
                        </div>     
                    </div>
                    <div style="clear:both"></div>
                </div>
            </div>

        <?php endif; ?>


        <div id="addFreeTimeSection" style="display:none; margin-left:5px;">
            <div style="width:700px; margin-top:5px;">
                <div id="addFreeTimeInput_div">
                    <label for="remainingStoredTimeInput" id="remainingStoredTimeInput_label">Add Free Time (Minute) during this Chat Session: </label>
                </div>
                <div style="clear:both"></div>
                <div>
                    <span style="vertical-align: top; font-size:11px;">ADD </span>
                    <input type="text" id="freeTimeInput" value="<?php echo $client_total_free_balance; ?>"  style="border:1px #999 solid; color:#f6931f; font-weight:bold; width:50px; padding-top:2px; height:14px;">
                </div>
            </div>
            <div style="clear:both"></div>
            <div>
                <div>
                    <div style="float:left; margin-left:0px">
                        <input id="addFreeTimeBtn" type="button" class="btn btn-primary" value="Add">
                    </div>
                    <div style="float:left; margin-left:5px">
                        <input id="cancelAddFreeTimeBtn" type="button" class="btn cancelAddStoredTimeBtn" style="margin-left: 10px" value="Done">
                    </div>
                </div>
                <div style="clear:both"></div>
            </div>
        </div>

    </div>
    <?php if($member_type == 'client') {?>
        <div id="font-color" style="display: none;"><?php echo $client['font_color']; ?></div>
        <?php if($client['font_size'] > 5) {?>
            <div id="font-size" style="display: none;"><?php echo $client['font_size']; ?></div>
        <?php } else {?>
            <div id="font-size" style="display: none;">24</div>
        <?php }?>
    <?php }?>
    <?php if($member_type == 'reader') {?>
        <div id="font-color" style="display: none;"><?php echo $reader['font_color'];?></div>
        <?php if($reader['font_size'] > 5) {?>
            <div id="font-size" style="display: none;"><?php echo $reader['font_size'];?></div>
        <?php } else {?>
            <div id="font-size" style="display: none;">24</div>
        <?php }?>

    <?php }?>

    <!-- Chat Window -->
    <div id='chat_window' class='well'>
        <div id='messageContainer' class='interior'>
            <?php foreach ($transcripts as $t) : ?>
                <?php if ($t['member_id'] == $reader_id): ?>
                    <?php $t_memeber_type = 'reader'; ?>
                    <?php $t_sender_name = $reader['username']; ?>
                <?php else: ?>

                    <?php $t_memeber_type = 'client'; ?>
                    <?php $t_sender_name = $client['username']; ?>
                <?php endif; ?>
                <?php $class_name = "chat_" . $t_memeber_type; ?>

                <div class='<?php echo $class_name; ?>'><span class='chat_username'><?php echo $t_sender_name; ?>:</span>&nbsp;&nbsp;<?php echo $t['message']; ?></div>
            <?php endforeach; ?>

        </div>
        <!-- 9-6 -->
        <?php if ($member_type == 'reader') {?>    

            <div class="contactlist-formbox" style="z-index: 2; position: absolute;float: right;bottom: 290px;right: 300px;">
                <div class="contactlist-formbox-head">Notes to Self<button id="btn-save" class="btn btn-default">save</button></div>
                <div class="contactlist-formbox-body" style="overflow: unset;">
                    <textarea id="reader-note"></textarea>               
                </div>
            </div>

        <?php }?>
        <div class="contactlist-formbox" style="z-index: 2; position: absolute;float: right;bottom: 290px;right: 21px;">
            <div class="contactlist-formbox-head-admin-reply">Contact Admin<button id="btn-admin-reply" class="btn btn-default">send</button></div>
            <div class="contactlist-formbox-body-admin-reply" style="overflow: unset;">
                <textarea id="admin-reply-txt"></textarea>
            </div>
        </div>
    </div>

    <!-- video call request david_v2 -->
    <div id="vcall_req" class="vcall" style="display:none;">
        <h2>Calling you</h2>
        <div id="fountainG">
            <div id="fountainG_1" class="fountainG"></div>
            <div id="fountainG_2" class="fountainG"></div>
            <div id="fountainG_3" class="fountainG"></div>
            <div id="fountainG_4" class="fountainG"></div>
            <div id="fountainG_5" class="fountainG"></div>			
        </div>
        <div class="button acceptBtn">accept</div>
        <div class="button rejectBtn">reject</div>

    </div>
    <div id="vcall_wait_req" class="vcall" style="display:none;">
        <h2>Calling you</h2>
        <div id="fountainG">
            <div id="fountainG_1" class="fountainG"></div>
            <div id="fountainG_2" class="fountainG"></div>
            <div id="fountainG_3" class="fountainG"></div>
            <div id="fountainG_4" class="fountainG"></div>
            <div id="fountainG_5" class="fountainG"></div>			
        </div>
        <div class="button rejectBtn">End</div>

    </div>
    <!-- end video call request -->
    <!-- contact list david_v3 -->
    <?php
    /*if ($member_type == 'client')
    {
        if (isset($readers) && $readers)
        {
            $ordered_readers = array();
            $online_readers = array();
            $offline_readers = array();
            $blocked_readers = array();
            $break_readers = array();
            $away_readers = array();
            $busy_readers = array();
            $other_readers = array();
            foreach ($readers as $r) {
                if ($r['username'] == $reader['username'])
                {
                    $current_readers[] = $r;
                    continue;
                }

                switch ($r['status'])
                {
                    case "online":
                        $online_readers[] = $r;
                        break;
                    case "offline":
                        $offline_readers[] = $r;
                        break;
                    case "blocked":
                        $blocked_readers[] = $r;
                        break;
                    case "break":
                        $break_readers[] = $r;
                        break;
                    case "away":
                        $away_readers[] = $r;
                        break;
                    case "busy":
                        $busy_readers[] = $r;
                        break;
                    default:
                        $other_readers[] = $r;
                }
            }
            $ordered_readers = array_merge($ordered_readers, $current_readers);
            $ordered_readers = array_merge($ordered_readers, $online_readers);
            $ordered_readers = array_merge($ordered_readers, $offline_readers);
            $ordered_readers = array_merge($ordered_readers, $blocked_readers);
            $ordered_readers = array_merge($ordered_readers, $break_readers);
            $ordered_readers = array_merge($ordered_readers, $away_readers);
            $ordered_readers = array_merge($ordered_readers, $busy_readers);
            $ordered_readers = array_merge($ordered_readers, $other_readers);
            $list_html = '<div class="contactlist-formbox">
						  	<div class="contactlist-formbox-head">Reader List</div>
						  	<div class="contactlist-formbox-body">';
            foreach ($ordered_readers as $r) {
                $link = "#";
                $target = "_self";
                if ($r['status'] == "online")
                {
                    if (!empty($r['username']))
                    {
                        $link = "#" . $r['username'];
                    }
                }
                if ($r['status'] == 'online')
                {
                    $color = 'red';
                } elseif ($r['status'] == 'blocked')
                {
                    $color = 'green';
                } elseif ($r['status'] == 'break')
                {
                    $color = 'blue';
                } elseif ($r['status'] == 'away')
                {
                    $color = 'yellow';
                } else
                    $color = '#ccc';
                if ($r['username'] == $reader['username'])
                {
                    $background = "#33eeff";
                } else
                    $background = "#eeeeee";
                $list_html .= '<div class="contactlist-wrapper" style="background:' . $background . '"><div style="float:left;display:inline-block;">
                            		    <a href="#"><img src="' . $r["profile"] . '" class="profile" style="width:36px;"></a>
	                			   </div>
	                			   <div style="float:left;display:inline-block;margin:7px;">
	                					<a href="#">' . $r["username"] . '</a>
	                			   </div>
                					<div style="border-radius:50%; width:10px; height:10px; float:right;margin:10px; background-color:' . $color . '"></div></div>
                		<div class="clearfix"></div>';
            }
            $list_html .= '</div></div>';
            echo $list_html;
        }
    }*/
/*9-2*/

    ?>


    <!-- end contact list david_v3 -->
    <!-- contact admin modal david_v4 -->
    <?php if ($member_type == 'client') : ?>
        <div class="modal fade modal-lg modal-sm" id="contactAdminModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form role="form" id="contactAdminForm">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only"></span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">
                                Contact Admin
                            </h4>
                        </div>

                        <!-- Modal Body -->
                        <div class="modal-body">                		
                            <div class="form-group">
                                <label for="name">Your Name</label>
                                <input type="text" id="name" name="name" value="<?php echo $client['first_name'] . ' ' . $client['last_name'] ?>" class="form-control"  readonly/>
                            </div>
                            <div class="form-group">
                                <label for="username">Username</label>
                                <input type="text" value="<?php echo $client['username']; ?>" class="form-control" id="username" readonly />
                            </div>
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input type="text" name="email" id="email" class="form-control" value="<?php echo $client['email']; ?>" readonly/>
                            </div>
                            <div class="form-group">
                                <label for="phone">Phone number</label>
                                <input type="text" name="phone" class="form-control" id="phone" />
                            </div>
                            <div class="form-group">
                                <label for="subject">Subject</label>
                                <select name="subject" class="form-control">
                                    <option value=''>Subject</option>
                                    <option value="General Help"<?= set_select('subject', 'General Help') ?>>General Help</option>
                                    <option value="Report Abuse"<?= set_select('subject', 'Report Abuse') ?>>Report Abuse</option>
                                    <option value="Report Page Error"<?= set_select('subject', 'Report Page Error') ?>>Report Page Errors</option>
                                    <option value="Suggestions"<?= set_select('subject', 'Suggestions') ?>>Suggestions</option>
                                    <option value="Chat Question"<?= set_select('subject', 'Chat Question') ?>>Chat Question</option>
                                    <option value="Affiliate Question"<?= set_select('subject', 'Affiliate Question') ?>>Affiliate Question</option>
                                    <option value="Become An Expert"<?= set_select('subject', 'Become An Expert') ?>>Become A Expert Question</option>
                                    <option value="Other, No Listed"<?= set_select('subject', 'Other, No Listed') ?>>Other, Not Listed</option>
                                </select>                    			
                            </div>
                            <div class="form-group">
                                <label for="comments">Comments / Questions</label>
                                <textarea name='comments' rows='10' id="comments" class="form-control" cols='25'></textarea>
                            </div>                			

                        </div>

                        <!-- Modal Footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                Close
                            </button>
                            <button type="button" class="btn btn-default" id="sendInquryBtn">Send Inqury</button>
                        </div>
                    </form>
                </div><!--\ modal content -->
            </div><!--\ modal dialog -->
        </div><!--\ modal --> 
        <div class="modal fade" id="modalchatEnd">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Choose an action</h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            <ul class="list-group list-unstyled">
                            <?php if($client_id != 1) {?>
                                <li class="list-group-item">
                                    <a href="/psychics">Enter a new chat</a>
                                </li>
                            <?php }?>
                            <?php if($client_id != 1) {?>
                                <li class="list-group-item">
                                    <a href="/my_account">Your account dashboard</a>
                                </li>
                            <?php }?>
                            <?php if($client_id == 1) {?>
                                <li class="list-group-item">
                                    <a href="/admin/main/">Your account dashboard</a>
                                </li>
                            <?php }?>
                                <li class="list-group-item">
                                    <a href="#">Send nrr </a>
                                </li>
                            <?php if($client_id != 1) {?>
                                <li class="list-group-item">
                                    <a href="/contact">Contact admin</a>
                                </li>
                            <?php }?>
                                <li class="list-group-item">
                                    <a href="/my_account/main/testimonial/<?= $reader['id'] ;?>">Reader survey</a>
                                </li>
                            </ul>
                        </p>
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-default" href="/my_account/psychic_readers">Close</a>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <!-- end contact admin modal david_v4 --> 
    <!-- Footer -->
    <div id='footer' class="navbar navbar-inverse">
        <form action='' method='POST' id='chatForm' class="navbar-inner">
            <?php if ($detect->isMobile()) : ?>
                <div>
                    <div class='isTyping' id='whosTyping'></div>
                </div>
<!--                <div>-->
<!--                    <textarea name='input_message' id="input_message" placeholder="Enter text here and click Enter/Send" ></textarea>-->
<!--                </div>-->
                <div name='input_message' contenteditable="true" style="float:left;width:98.97% !important;max-width:100%;" id="input_message" placeholder="Enter text here and click Enter/Send"></div>
                <div>
                    <input id="chatForm_send" type='button' name='submit' value='Send' class='btn' style='margin:0;'>
                </div>
            <?php else: ?><!-- david_v4 -->
                <div class='form-group'>
                    <div class='col-md-9 col-sm-9 chat-area'>
                        <!--<textarea name='input_message' style="float:left;width:100% !important;max-width:100%;" id="input_message" placeholder="Enter text here and click Enter/Send"></textarea>-->
                        <div name='input_message' contenteditable="true" style="float:left;width:98.97% !important;max-width:100%;" id="input_message" placeholder="Enter text here and click Enter/Send"></div>
                        <div style="display: inline-flex;">
                            <div style=" padding-top: 5px; padding-right: 16px;">
                                <select class="font-size" style="width: auto;">
                                    <option class="dropdown-item" selected>Select Fontsize</option>
                                    <option class="dropdown-item" value="30">Large</option>
                                    <option class="dropdown-item" value="24">Medium</option>
                                    <option class="dropdown-item" value="18">Small</option>
                                </select>
                            </div>
                            <div class="btn-group btn-group-sm">
                                <button type="button" id="demo2"><span class="color-fill-icon dropdown-color-fill-icon" style="background-color:#000;"></span>&nbsp;<b class="caret"></b></button>
                            </div>
                        </div>
                        <div style="margin-top: 5px; padding-top: 20px;">
                            
                            
                            <img class="emo-img" src="//cdn.jsdelivr.net/emojione/assets/svg/263a.svg" data-src="263a" />
                            <img class="emo-img" src="//cdn.jsdelivr.net/emojione/assets/svg/1f60a.svg" data-src="1f60a" />
                            <img class="emo-img" src="//cdn.jsdelivr.net/emojione/assets/svg/2639.svg" data-src="2639" />
                            <img class="emo-img" src="//cdn.jsdelivr.net/emojione/assets/svg/1f60b.svg" data-src="1f60b" />
                            <img class="emo-img" src="//cdn.jsdelivr.net/emojione/assets/svg/1f60c.svg" data-src="1f60c" />
                            <img class="emo-img" src="//cdn.jsdelivr.net/emojione/assets/svg/1f60d.svg" data-src="1f60d" />
                            <img class="emo-img" src="//cdn.jsdelivr.net/emojione/assets/svg/1f60e.svg" data-src="1f60e" />
                            <img class="emo-img" src="//cdn.jsdelivr.net/emojione/assets/svg/1f60f.svg" data-src="1f60f" />
                            <img class="emo-img" src="//cdn.jsdelivr.net/emojione/assets/svg/1f61a.svg" data-src="1f61a" />
                            
                            <!-- <img src="/media/images/volume-up-4-512-white.gif" class="toggle-chat-sound" style="width: 20px; cursor:pointer"  /> -->
                            <!--
                            <div style="display: inline-block;position: relative">
                                <span class="fa fa-cog btn-settings" style="font-size: 18px;color: white;position: absolute;top: -12px;left: 7px;"></span>
                            </div>
                            -->
                            <div style="display: inline-flex;">
                                <img src="/media/images/camera.png" style="width:30px; height:30px; margin-right: 10px; margin-left: 20px;" />
                                <input type='file' id="imgInp" style="color: white;" data-toggle="tooltip" title="Please click this button to upload images."/>
                                <div id="input_message_preview" style=" margin-left:-50px;"></div>
                                <img class="blah" src="#" alt="your image" />
                            </div>
                            
                            <input type="hidden" id="chat_sound" value="1" />
                            
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 chat-send">
                        <input id="chatForm_send" type='button' name='submit' value='Send' class='btn' style='margin:0;'>
                        
                        <div style="color: white;padding-top: 29px;">
                            <a href="javascript:void(0)" class="btn-settings" style="color:white">
                            Change Ping Sound 
                            <span class="fa fa-cog" style="font-size: 16px;"></span>
                            </a>
                            <img src="/media/images/volume-up-4-512-white.gif" class="toggle-chat-sound" style="width: 20px; cursor:pointer"  />
                        </div>
                        
                    </div>
                </div>                
                <div class='isTyping' id='whosTyping'></div>
            <?php endif; ?>
        </form>
    </div>

</div>

<input type="hidden" id="choosen_sound" value="<?=$member["chat_notify_sound"] != "" ? $member["chat_notify_sound"] : "message_alert"; ?>" />

<div class="modal fade" id="md-settings">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Settings</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Sounds:
        <select class="form-control" id="tool-sound-setting">
            <option value="message_alert">Skype drop</option>
            <option value="clear_minimal">Clear Minimal Notification</option>
            <option value="chord">Chord</option>
            <option value="chat_alert">Chat Alert</option>
        </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btn-save-settings">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- 9-16 -->
<input type="hidden" value="0" id="gift_used">
<!--9-22-->
<input type="hidden" id="client-gift-size">

<!-- start chat server -->
<?php if ($this->session->userdata('member_logged')): ?>
    <script><!--david_v2-->
        var session_id = '<?php echo $chat_session_id; ?>';
        var chatId = '<?php echo $id; ?>';
        var vchat_url = '/chat/ChatInterface/video_chat/' + chatId + '/' + session_id;
    </script>
    <!--
    <script src="/chat/app/chat_load_config.js"></script>
    <script data-main="/chat/app/start_chat_room.js" src="/chat/app/require-min.js"></script>
    -->

    <script src="/chat/editor.js"></script>

    <script src="/chat/app/chat_all_room.js?ts=<?= $ts ?>" ></script>
    <script src="/media/video_chat/js/webrtc.io.js"></script>
    <script src="/media/video_chat/js/lobibox.min.js"></script>
    <!-- david_v2 -->

    <!-- color picker -->
    <script src="/media/dist/js/bootstrap-colorpicker.min.js"></script>
    <script src="/media/dist/js/bootstrap-colorpicker-plus.js"></script>
    <!-- color picker -->

    <script>
        
        function check_update_from_paypal()
        {
            // Check the database for new max length
            var interval = setInterval(function(){
                
                console.log("Interval is running...");
                
                var url = "/chat/chatInterface/check_max_length";
                var params = {
                    chat_id: '<?= $id; ?>',
                    max_chat_length: '<?php echo $max_chat_length; ?>'
                };
                
                $.post(url, params, function(data){
                    if (data.status == "OK")
                    {
                        $("#stopWatch").val('0');
                        clearInterval(interval);
                        Chat.manager.room.send.resume();
                    }
                }, "JSON");
            }, 1000);
        }
        
        function getCaret(obj) {
            var caretPos = 0;
            if($(window.getSelection().anchorNode).is(obj)){
                caretPos = 0;
            }else{
               caretPos = window.getSelection().anchorOffset;
            }
  
            return caretPos;
        };

        $(document).ready(function () {

            console.log('Free time balance ===>>> ', $('#freeTimeInput').val())

            var nVer = navigator.appVersion;
            var nAgt = navigator.userAgent;
            var browserName  = navigator.appName;
            var fullVersion  = ''+parseFloat(navigator.appVersion);
            var majorVersion = parseInt(navigator.appVersion,10);
            var nameOffset,verOffset,ix;

// In Opera, the true version is after "Opera" or after "Version"
            if ((verOffset=nAgt.indexOf("Opera"))!=-1) {
                browserName = "Opera";
                fullVersion = nAgt.substring(verOffset+6);
                if ((verOffset=nAgt.indexOf("Version"))!=-1)
                    fullVersion = nAgt.substring(verOffset+8);
            }
// In MSIE, the true version is after "MSIE" in userAgent
            else if ((verOffset=nAgt.indexOf("MSIE"))!=-1) {
                browserName = "Microsoft Internet Explorer";
                fullVersion = nAgt.substring(verOffset+5);
            }
// In Chrome, the true version is after "Chrome"
            else if ((verOffset=nAgt.indexOf("Chrome"))!=-1) {
                browserName = "Chrome";
                fullVersion = nAgt.substring(verOffset+7);
            }
// In Safari, the true version is after "Safari" or after "Version"
            else if ((verOffset=nAgt.indexOf("Safari"))!=-1) {
                browserName = "Safari";
                fullVersion = nAgt.substring(verOffset+7);
                if ((verOffset=nAgt.indexOf("Version"))!=-1)
                    fullVersion = nAgt.substring(verOffset+8);
            }
// In Firefox, the true version is after "Firefox"
            else if ((verOffset=nAgt.indexOf("Firefox"))!=-1) {
                browserName = "Firefox";
                fullVersion = nAgt.substring(verOffset+8);
            }
// In most other browsers, "name/version" is at the end of userAgent
            else if ( (nameOffset=nAgt.lastIndexOf(' ')+1) <
                (verOffset=nAgt.lastIndexOf('/')) )
            {
                browserName = nAgt.substring(nameOffset,verOffset);
                fullVersion = nAgt.substring(verOffset+1);
                if (browserName.toLowerCase()==browserName.toUpperCase()) {
                    browserName = navigator.appName;
                }
            }
// trim the fullVersion string at semicolon/space if present
            if ((ix=fullVersion.indexOf(";"))!=-1)
                fullVersion=fullVersion.substring(0,ix);
            if ((ix=fullVersion.indexOf(" "))!=-1)
                fullVersion=fullVersion.substring(0,ix);

            majorVersion = parseInt(''+fullVersion,10);
            if (isNaN(majorVersion)) {
                fullVersion  = ''+parseFloat(navigator.appVersion);
                majorVersion = parseInt(navigator.appVersion,10);
            }

            console.log(''
                +'Browser name  = '+browserName+'<br>'
                +'Full version  = '+fullVersion+'<br>'
                +'Major version = '+majorVersion+'<br>'
                +'navigator.appName = '+navigator.appName+'<br>'
                +'navigator.userAgent = '+navigator.userAgent+'<br>'
            )

            $.ajax({
                'type': "POST",
                'url': "/chat/chatInterface/save_browser_info",
                'data': { chat_session_id: '<?= $chat_session_id ?>', browser_info: browserName, user_agent: navigator.userAgent, member_type: $("#member_type").val()},
                success: function (data) {

                }
            });

            // blink warning messages
            function blinker_warning() {

                $('.warning-message p').fadeOut(2000);
                $('.warning-message p').fadeIn(2000);
            }

            setInterval(blinker_warning, 4000);

            $('[data-toggle="tooltip"]').tooltip(); // 9-18


            if($("#member_type").val() == 'reader') {

                $(".chat_client").css('display', 'none');

                /*9-23*/
                $("#btn-admin-reply").click(function () {

                    $.ajax({
                        'type': "POST",
                        'url': "/chat/chatInterface/save_admin_reply",
                        'data': { member_id : '<?=$mem_id;?>', message: $("#admin-reply-txt").val(), chat_session_id: '<?= $chat_session_id ?>'},
                        success: function (data) {
                            $("#admin-reply-txt").val('');
                        }
                    });
                });

                function blinker_paid() {
                    $('#timerSpan').fadeOut(1000);
                    $('#timerSpan').fadeIn(1000);
                }

                function blinker_free() {

                    $('#timerSpanFree').fadeOut(1000);
                    $('#timerSpanFree').fadeIn(1000);
                }

                var blink_paid; //Runs every second
                var blink_free;

                var free_min;
                var paid_min;

                var flag;
                
                setInterval(function(){

                    paid_min = parseInt($("#timerSpan").text().split(":")[1]);
                    free_min = parseInt($("#timerSpanFree").text().split(":")[1]);

                    console.log('paid_min====================>',paid_min);
                    console.log('free_min====================>',free_min);

                    if(paid_min >= 2) {

                        $("#timerSpan").css('color','lightgreen');
                        clearInterval(blink_paid);

                    } 
                    if(paid_min < 2) {

                        $("#timerSpan").css('color','red');
                        if(flag != 1 && blink_paid == null) {

                            blink_paid = setInterval(blinker_paid, 2000);
                            flag = 1;
                        }
                    }

                    if(free_min >= 2) {

                        $("#timerSpanFree").css('color','lightgreen');
                        clearInterval(blink_free);

                    } 
                    if(free_min < 2) {

                        $("#timerSpanFree").css('color','red');
                        
                        if(blink_free == null && $("#timerSpanFree").text() != '00:00:00') {

                            blink_free = setInterval(blinker_free, 2000);  
                        }
                        if($("#timerSpanFree").text() == '00:00:04') {

                            clearInterval(blink_free);
                        }
                        
                    }
                    

                },4000);
            }

            if($("#member_type").val() == 'client') {

                /*9-23*/
                $("#btn-admin-reply").click(function () {

                    $.ajax({
                        'type': "POST",
                        'url': "/chat/chatInterface/save_admin_reply",
                        'data': { member_id : '<?=$client_id;?>', message: $("#admin-reply-txt").val(), chat_session_id: '<?= $chat_session_id ?>'},
                        success: function (data) {
                            $("#admin-reply-txt").val('');
                        }
                    });
                });

                function blinker_paid() {
                    $('#timerSpan').fadeOut(1000);
                    $('#timerSpan').fadeIn(1000);
                }

                function blinker_free() {

                    $('#timerSpanFree').fadeOut(1000);
                    $('#timerSpanFree').fadeIn(1000);
                }

                var blink_paid; //Runs every second
                var blink_free; 

                var free_min;
                var paid_min;

                var flag;

                setInterval(function(){

                    paid_min = parseInt($("#timerSpan").text().split(":")[1]);
                    free_min = parseInt($("#timerSpanFree").text().split(":")[1]);

                    console.log('paid_min====================>',paid_min);
                    console.log('free_min====================>',free_min);

                    if(paid_min >= 2) {

                        $("#timerSpan").css('color','lightgreen');
                        clearInterval(blink_paid);

                    } 
                    if(paid_min < 2) {

                        $("#timerSpan").css('color','red'); 

                        if(flag != 1 && blink_paid == null) {

                            blink_paid = setInterval(blinker_paid, 2000);
                            flag = 1;
                        }
                        
                    }

                    if(free_min >= 2) {

                        $("#timerSpanFree").css('color','lightgreen');
                        clearInterval(blink_free);

                    } 
                    if(free_min < 2) {

                        $("#timerSpanFree").css('color','red');
                        
                        if(blink_free == null && $("#timerSpanFree").text() != '00:00:00') {
                        
                            blink_free = setInterval(blinker_free, 2000);   
                        } 
                        if($("#timerSpanFree").text() == '00:00:04') {
                        
                            clearInterval(blink_free);
                        }  
                        
                    }


                },4000);
            }

            /*9-2*/
            if($("#reader-note").val() == '') {

                $("#btn-save").css("display", "none");
            }

            $('#reader-note').bind('input propertychange', function() {

                if($("#reader-note").val() != ''){

                    $("#btn-save").css("display", "block");
                }
            });

            $("#btn-save").click(function() {

                $.ajax({
                    'type': "POST",
                    'url': "/chat/chatInterface/save_note",
                    'data': { reader_id : '<?=$mem_id;?>', client_id: '<?=$client_id;?>',client_username: '<?=$client['username'];?>',
                              client_firstname: '<?=$client['first_name'];?>', client_lastname: '<?=$client['last_name'];?>', note: $("#reader-note").val()},
                    success: function (data) {
                        obj = JSON.parse(data);
                    }
                });
            });

            $.ajax({
                'type': "POST",
                'url': "/chat/chatInterface/get_note",
                'data': { reader_id : '<?=$mem_id;?>', client_id: '<?=$client_id;?>'},
                success: function (data) {
                    obj = JSON.parse(data);
                    console.log('NOTE ==================>>>'+obj['note']+obj['reader_id']+obj['client_id']);
                    $("#reader-note").val(obj['note']);
                }
            });

            /*9-2 end*/

            $.ajax({
                'type': "POST",
                'url': "/chat/chatInterface/get_fontcolor",
                'data': { user_id : '<?=$mem_id;?>'},
                success: function (data) {
                    obj = JSON.parse(data);
                    console.log("font-color = "+obj.font_color['font_color']);
                    $('#input_message').css('color', obj.font_color['font_color']);
                    $('.chat_reader').css('color', obj.font_color['font_color']);
                    $('.chat_client').css('color', obj.font_color['font_color']);
                }
            });
            $.ajax({
                'type': "POST",
                'url': "/chat/chatInterface/get_fontsize",
                'data': { user_id : '<?=$mem_id;?>'},
                success: function (data) {

                    var obj = JSON.parse(data);

                    console.log("font-size = "+obj.font_size['font_size']);

                    var font_size = obj.font_size['font_size'];

                    if(parseInt(font_size) > 5) {

                        $("#input_message").css('font-size', font_size+"px");

                    } else {

                        $("#input_message").css('font-size', 24+"px");
                    }
                }
            });

            $("div.colorpickerplus").css('max-width', '205px');
            $("div.input-group-sm").css('display', 'none');
            $("div.colorpickerplus-custom-colors").css('display', 'none');
            $('select.font-size').on('change', function (e) {

                var optionSelected = $("option:selected", this);
                var valueSelected = this.value;
                $("#input_message").css('font-size', valueSelected+"px");
                $('.chat_reader span.text').css('font-size', valueSelected+"px");
                $('.chat_client span.text').css('font-size', valueSelected+"px");
                $('.chat_reader').css('font-size', valueSelected+"px");
                $('.chat_client').css('font-size', valueSelected+"px");
                $('div#font-size').html(valueSelected);
                $.ajax({
                    'type': "POST",
                    'url': "/chat/chatInterface/save_fontsize",
                    'data': { user_id : '<?=$mem_id;?>', 'data': valueSelected },
                    success: function (data) {
                        
                    }
                });
                
            });

            var demo2 = $('#demo2');
            demo2.colorpickerplus();
            demo2.on('changeColor', function(e,color){
                        
                if(color==null) {
                  //when select transparent color
                  $('.color-fill-icon', $(this)).addClass('colorpicker-color');
                } else {

                  $('.color-fill-icon', $(this)).removeClass('colorpicker-color');
                  $('.color-fill-icon', $(this)).css('background-color', color);
                  $('#input_message').css('color', color);
                  $('.chat_reader').css('color', color);
                  $('.chat_client').css('color', color);
                  $('#font-color').html(color);
                  console.log(color);
                  $.ajax({
                    'type': "POST",
                    'url': "/chat/chatInterface/save_fontcolor",
                    'data': { user_id : '<?=$mem_id;?>', 'data': color },
                    success: function (data) {

                    }
                });
                }
            });

            /*jquery image upload*/
            function readURL() {
                $('.blah').attr('src', '').hide();
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    $(reader).load(function(e) {
                        $('.blah').attr('src', e.target.result);
                        $("#input_message").append("<img src='"+ e.target.result +"' style='width: 50px; height: 50px; margin-left: 5px; margin-right: 5px;'>");
                        $("#input_message_preview").html("<img style='border-radius: 25px; width: 50px;height: 50px; margin-left: 70px; margin-top: -10px;' src='"+ e.target.result +"'>");
                    });
                    reader.readAsDataURL(this.files[0]);
                }
                $('#input_message').focus();
            }

            $('.blah').load(function(e) {
                    $(this).css('height', '0px').show(); 
                }).hide();    

            $("#imgInp").change(readURL);
            
            $("#btn-save-settings").click(function(){
                $("#choosen_sound").val( $("#tool-sound-setting").val() );
                
                // Save as default sound for user
                $.post("/chat/chatInterface/save_sound_settings", {user_id : '<?=$mem_id;?>', sound : $("#tool-sound-setting").val()}, function(data){
                    if (data.status == "OK")
                    {
                        $("#md-settings").modal("hide");
                    }
                }, "json");                
            });
            
            $(".btn-settings").click(function(){
                $("#md-settings").modal({
                    backdrop: 'static',
                    keyboard: false
                }, "show");
            });
            
            $(".toggle-chat-sound").click(function(){
                if ($("#chat_sound").val() == "1")
                {
                    $("#chat_sound").val(0);
                    $(this).attr("src", "/media/images/volume-up-4-512.gif");
                }
                else
                {
                    $("#chat_sound").val(1);
                    $(this).attr("src", "/media/images/volume-up-4-512-white.gif");
                }
            });
            
            $('img.emo-img').click(function () {
        		var org =  $('#input_message').html();
        		// var txt = '&#x' + $(this).data('src') + ';';
                var txt = '<img class="emo-img" style="float:none;width: 20px;margin-right:0px" src="//cdn.jsdelivr.net/emojione/assets/svg/'+$(this).data('src')+'.svg" data-src="'+$(this).data('src')+'" />';
        		var div = document.getElementById('input_message');
        		var caret = getCaret(div);
                // $('#input_message').focus();
                var caret_end = "";

                $('#input_message').html(org + txt).focus();
                
                // $('#input_message').html(org + txt);
                // $('#input_message').blur();
            });
            
            ion.sound({
                sounds: [
                    {name: "door_bell"},
                    {name: "message_alert"},
                    {name: "clear_minimal"},
                    {name: "chord"},
                    {name: "chat_alert"},
                    {name: "bell_ring"},
                    {name: "end_chat"},
                ],
                path: "/media/sounds/",
                preload: true,
                volume: 1
            });
            

            $('#addTimeHelp').tooltip({placement: 'right', trigger: 'hover'});

            Chat.run.room();
            //contactlist david_v3
            jQuery(".contactlist-formbox-head").click(function () {
                jQuery(".contactlist-formbox-body").slideToggle(300);
            });
            /*9-23*/
            jQuery(".contactlist-formbox-head-admin-reply").click(function () {
                jQuery(".contactlist-formbox-body-admin-reply").slideToggle(300);
            });

        });
        $("#sendInquryBtn").click(function () {
            var subject = $('select[name="subject"]').val();
            var comments = $('textarea[name="comments"]').val();
            if (!comments || !subject) {
                Lobibox.notify('error', {
                    title: 'Warning',
                    showClass: 'fadeInDown',
                    hideClass: 'fadeUpDown',
                    delay: 5000,
                    msg: 'You need to fill subject and comments.'
                });
                return false;
            }
            $.ajax({
                type: "POST",
                url: "/contact/send",
                data: $("#contactAdminForm").serialize(),
                success: function (data) {
                    obj = JSON.parse(data);
                    $('#contactAdminModal').modal('toggle');
                    Lobibox.alert('success', {
                        msg: obj.response
                    });
                }
            });

        });
    </script>
<?php endif; ?>