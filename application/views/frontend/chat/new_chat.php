<style type="text/css">
    .ui-dialog .ui-dialog-title{
        font-size: 12px !important; 
    }
    /*9-27*/
    .notifyjs-corner {
        display: inline-grid;
    }
</style>
<!-- 9-17 -->
<link href="/theme/template/styles/load-styles.css" rel="stylesheet" media="screen">
<link href="/theme/template/styles/main.css" rel="stylesheet" media="screen">
<?php
if (!isset($ts))
{
    $ts = time();
}
?>
<!-- 9-2 -->
<style>
    .img-preview {
        width: 100%;
    }
    .chat-info {
        width: 100%;
    }
    .chat-info span.info {
        font-size: 16px;
        font-weight: bold;
        color: dodgerblue;
    }
    /*9-17*/
    body {
        background: #126393;
        padding: 0px;
    }
    input#topic {
        height: 35px;
    }
    input.ChatRedBackground {
        color: white;
    }
    .ui-widget-content {
        color: #4c4b4b !important;
    }
    .ui-widget, .ui-widget button {
        font-family: "Avenir LT Std Light", Helvetica, Arial, sans-serif;
    }
    .ui-dialog .ui-dialog-title {
        font-size: 1.6rem !important;
    }
    .dialogContent {
        font-size: 14px !important;
    }

</style>
<link rel=stylesheet type="text/css" href="/media/css/chat.css?ts=<?php echo $ts ?>">
<!-- 9-16 -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/css/bootstrap-notify.css">


<?php
if ($detect->isMobile())
{
    // mobile CSS
    ?>
    <link rel="stylesheet" media="all and (orientation:portrait)" href="/media/css/iphone-portrait.css">
    <link rel="stylesheet" media="all and (orientation:landscape)" href="/media/css/iphone-landscape.css">
    <?php
    }  else {
    ?>
    <link rel="stylesheet"  href="/media/css/web.css">
    <?php
    }
    ?>

    <script src="<?php echo CHAT_URL ?>:<?php echo CHAT_PORT ?>/auth.js"></script>
    <script src="<?php echo CHAT_URL ?>:<?php echo CHAT_PORT ?>/socket.io/socket.io.js"></script>

   

    <div id="new_chat">
        <script>

            $(function () {
                document.title = '<?php echo $title ?>';
                $("#confirmationForm").submit(function (e) {

                    e.preventDefault();

                    $.post($(this).attr('action'), $(this).serialize(), function (object) {

                        if (object['error'] == '1') {

                            alert(object['message']);

                        } else {

                            window.location = object['redirect'];

                        }

                    }, 'json');

                });

            });

        </script>


        <!-- Navigation Bar -->
        <div id='navbar' class="navbar navbar-inverse">
            <div class="navbar-inner">
                <div class="container">
                    <?php 
                    if ($this->my_common->user_ban_type_by_reader($reader_data['id'])!=null) {?> 
                        <span class="brand chat_with_text"><?php echo strtoupper($username) ?> signaled a <?=$this->my_common->user_ban_type_by_reader($reader_data['id']);?> ban.</span>
                        <?php
                    }else{?>
                    <a class="brand" href="#"><span class="chat_with_text">Chat With <?php echo strtoupper($username) ?></span></a>
                        <?php
                    }
                    ?>

                    <ul class="nav pull-right">
                        <li><a href="Javascript:window.close();">Close Window</a></li>
                    </ul>

                </div>
            </div>
        </div>


        <?php 
        if ($this->my_common->user_ban_type_by_reader($reader_data['id'])==null) {
            ?> 
        <!-- Content -->
        <!-- 9-2 -->
        <div style="width: 100%; height: auto; display: inline-flex;">
            <div class='well' style="width: 75%;">

                <!-- <form id='confirmationForm' action='/chat/main/confirm/<?php echo $id ?>' method='POST'> </form> -->
                <form id='confirmationForm'>
                    <!-- 9-2 -->
                    <!-- <img src='<?php echo $profile ?>' class='img-polaroid head-image-setting' > -->

                    <legend class="image-legend">Please Confirm Your Chat Details</legend>

                    <p class="prechat-message">You currently have enough funds in your account for a chat lasting <strong><?php echo $this->system_vars->time_generator($time_balance) ?></strong>
                        <input type="hidden" id="full_mins" name="full_mins" value="<?php echo $time_balance/60; ?>" />
                        <?php if($this->member_funds->free_minute_balance() > 0): ?>
                        and a free minutes of <strong><?php echo $this->system_vars->time_generator($this->member_funds->free_minute_balance() * 60) ?></strong>
                        <input type="hidden" id="free_mins" name="free_mins" value="<?php echo $this->member_funds->free_minute_balance(); ?>" />
                        <?php endif; ?>
                        . 
                        To continue chatting with <?php echo $username ?>, enter your topic below and click continue. </p>

                    <hr />

                    <div style='padding:0 0 10px 0;'><b>How many minutes do you want to use?</b></div>

                    <div class="chat-minute-select">
                        <!--<input id='minutes' name='minutes' type='text' value="25" style="width:75px;" class="mobile-font form-control"/>-->
                        
                        <select id="minutes" name="minutes" style="width:172px;" class="mobile-font form-control">
                            <option value="4">4 mins</option>
                            <option value="5">5 mins</option>
                            <option value="7">7 mins</option>
                            <option value="10">10 mins</option>
                            <option value="15">15 mins</option>
                            <option value="25">25 mins</option>
                            <option value="35">35 mins</option>
                            <option value="45">45 mins</option>
                            <option value="75">75 mins</option>
                            <option value="<?= ($time_balance/60); ?>">ALL TIME STORED</option>
                        </select>
                        
                        <!--
                        <select style="width: 110px" id="tier">
                            <option value="1">Regular</option>
                            <option value="0">Free</option>
                        </select>
                        -->
                        <!--<span class="add-on mobile-font">Minutes</span>-->
                    </div>

                    <div style='padding:0 0 10px 0;'><b>Your Chat Topic:</b></div>
                    <div style='padding:0 0 10px 0;'><input id="topic" name='topic' type='text' class='chat-topic-input form-control' placeholder="Enter your chat topic here..."></div>
                    <div>
                        <input type='button' name='submit' value='Continue To Chat' id='chat-attempt' class='btn btn-large btn-primary ctn-btn' onclick="Chat.controller.lobby.actions.chat_attempt('<?php echo $id ?>')" />
                        <a name='cancel' value='Cancel' class='btn btn-large btn-danger ChatRedBackground' style="margin-left: 20px;" href="/my_account/psychic_readers">Cancel</a>

                    </div>

                </form>
                <!-- /. thom ho -->
                <input type="hidden" id="number" value="0" />
                <input type="hidden" id="manualabort" value="0" />
                <!-- Other dialogs-->
                <div id="dialog-wait" title="Waiting for response from " class="Hidden">
                    <p><span id="wait_seconds_left"><?php echo CHAT_MAX_WAIT ?></span> seconds left for chat attempt.  </p>
                </div>
                <div id="dialog-reject" title="Reader can not chat with you at this time" class="Hidden">
                    <div class="dialogContent">
                        <div>Please try the followings: </div>
                        <div><a href="/my_account">Go to your account</a></div>
                        <div><a href="/contact">Contact Admin</a></div>
                        <div><a href="/psychics">Try another Reader</a></div>
                    </div>
                </div>
                <div id="dialog-abort-retry" title="No response from <?php echo $username ?>" class="Hidden">
                    <div class="dialogContent">
                        <div>Please try again later or the followings: </div>
                        <div><a href="#" id="retryWithSameReader">Try again with same Reader</a></div>
                        <div><a href="/my_account">Go to your account</a></div>
                        <div><a href="/contact">Contact Admin</a></div>
                        <div><a href="/psychics">Try another Reader</a></div>
                    </div>
                </div>
                <div id="dialog-manual-abort-retry" title="Cancel this chat attempt." class="Hidden" style="width: 370px;">
                    <div class="dialogContent">
                        <div>Please choose from the following:</div>
                        <div><a href="#" id="retryWithSameReader">Retry to chat with this Reader again</a></div>
                        <div><a href="/psychics">Continue the request to cancel</a></div>
                        <div><a href="/my_account">Go to your account</a></div>
                        <div><a href="/contact">Contact Admin</a></div>
                    </div>
                </div>
                <div id="dialog-offline-retry" title="Reader is offline" class="Hidden">
                    <div class="dialogContent">
                        <div>Please try the followings: </div>
                        <div><a href="/my_account">Go to your account</a></div>
                        <div><a href="/contact">Contact Admin</a></div>
                        <div><a href="/psychics">Try another Reader</a></div>
                    </div>
                </div>
                <div id="dialog-refuse-byreader" title="Try another Reader who is available now" class="Hidden">
                    <div class="dialogContent">
                        <div>Please try the followings: </div>
                        <div><a href="/my_account">Go to your account</a></div>
                        <div><a href="/contact">Contact Admin</a></div>
                        <div><a href="/psychics">Try another Reader</a></div>
                    </div>
                </div>
                <div id="wait-for2min" title="Please wait for 2 min to retry" class="Hidden">
                    <div class="dialogContent">
                        <div>Please try the followings: </div>
                        <div><a href="/my_account">Go to your account</a></div>
                        <div><a href="/contact">Contact Admin</a></div>
                        <div><a href="/psychics">Try another Reader</a></div>
                    </div>
                </div>
                <div id="dialog-max-manual-quit" title="Unable to request new chat" class="Hidden">
                    <div class="dialogContent">
                        <div>You have exceed the limit of 3 chat attempts in 10 minutes.  Please try another reader or wait at least 10mins and try again.</div>
                        <div><a href="/my_account">Go to your account</a></div>
                        <div><a href="/contact">Contact Admin</a></div>
                        <div><a href="/psychics">Try another Reader</a></div>
                    </div>
                </div>


            </div>
            <!-- 9-2 -->
            <div class="well" style="width: 25%; float: right;">
                <div style="width: 50%;">
                    <img src='<?php echo $profile ?>' class='img-polaroid img-preview' >
                </div>
                <div class="chat-info">
                    <P><span>Last chat with <?php echo($reader_data['username']);?> : </span><span class="info" id="last-chat-time"></span><span class="info" style="font-size: 14px;"> (EST/SITE TIME)</span></P>
                    <P><span>Topic : </span><span class="info" id="topic"></span></P>
                    <P><span>Amount of Minutes used: </span><span class="info" id="time-used"></span></P>
                </div>
            </div>
        </div>
        <?php
        }
        ?>
    </div>

    <!-- lobby js -->

    <!-- Now, loading heavy JS scripts -->
    
    <?php if ($this->session->userdata('member_logged')): ?>
        <?php $socket_url = CHAT_URL; ?>
        <?php $socket_port = CHAT_PORT ?>
        <?php $mem_id = $this->member->data['id']; ?>
        <?php $member_username = $this->member->data['username']; ?>
        <?php $member_id_hash = $this->member->data['member_id_hash']; ?>
        <?php $member_type = (is_null($this->member->data['profile_id'])) ? 'client' : 'reader'; ?>

        <script>
            var chat_init_data = {
                'member_type': '<?php echo $member_type ?>',
                'member_id': '<?php echo $mem_id ?>',
                'member_id_hash': '<?php echo $member_id_hash ?>',
                'member_username': '<?php echo $member_username ?>',
                '_disconnect_url': '<?php echo $this->config->item('site_url') . "/main/disconnect_user/" . $mem_id ?>',
                'socket_url': '<?php echo CHAT_URL ?>',
                'socket_port': '<?php echo CHAT_PORT ?>',
                'reader_id': <?php echo $id ?>,
                'site_url': '<?php echo SITE_URL; ?>'
            };

        </script>
        <!--
        <script src="/chat/app/chat_load_config.js"></script>
        <script data-main="/chat/app/start_main_lobby.js" src="/chat/app/require-min.js"></script>
        -->
        
        <script src="/chat/app/chat_all_lobby.js?ts=<?php echo $ts ?>" ></script>

        <script src='/media/javascript/ion.sound.min.js'></script>
        <!-- 9-16 -->
        <script src="/theme/admin/js/notify.min.js"></script>
        <script>

            $(document).ready(function () {

                /*9-16*/
                function blinker() {

                    $('.bell').fadeOut(1000);
                    $('.bell').fadeIn(1000);
                }

                /*9-16*/
                function get_week_day(week_day) {

                    switch(week_day) {

                        case 1:
                            return "Monday";
                        break;
                        case 2:
                            return "Tuesday";
                        break;
                        case 3:
                            return "Wednesday";
                        break;
                        case 4:
                            return "Thursday";
                        break;
                        case 5:
                            return "Friday";
                        break;
                        case 6:
                            return "Saturday";
                        break;
                        case 7:
                            return "Any Day of Week";
                        case 0:
                            return "Sunday";
                        break;

                    }
                }
                /*9-16*/
                $.ajax({

                        url: '/chat/main/get_gift_info/'+'<?php echo $mem_id ?>',
                        type: 'GET',

                    }).done(function(response) {

                        var obj = JSON.parse(response);
                        var data = obj.data;

                        for(var i=0; i<data.length; i++){

                            if(data[i]['reader_name'] != undefined && data[i]['reader_name'] == '<?php echo($reader_data["username"])?>') {

                                var week_day = get_week_day(parseInt(data[i]['week_day']));

                                var message = 'Hi ' + '<?=$this->member->data['username']?>, ' + data[i]['reader_name'] + ' has given you a "Giveback Gift" of ' + data[i]['gift_size'] + ' mins ' + 'for ' + week_day;

                                $.notify( message,  {className: "info", hideDuration: 700, autoHideDelay: 60000, position: 'right top'});

                                setInterval(blinker, 2000);

                            }
                        }
                        /*9-23*/
                        for(var j=1; j<= $(".notifyjs-wrapper").length; j++) {
                            $(".notifyjs-corner .notifyjs-wrapper:nth-child("+j+")").prepend( "<span class='bell' style='color: white;font-size: 60px; margin-top: -2px;'><i class='fa  fa-long-arrow-right'></i></span>" ).css('display','inline-flex');
                        }

                    }).fail(function(error) {
                        console.error(error);
                    });
                // }, 5000);

                /*9-2*/
                $.ajax({
                    url: '/chat/main/get_latest_chat_info/<?php echo($this->session->userdata('member_logged')); ?>/<?php echo($reader_data['id']); ?>',
                    type: 'GET',
                })
                .done(function(chat_info) {
                    
                    if (chat_info) {
                        var obj = JSON.parse(chat_info);
                        var last_chat_date = obj['last_chat_time'];
                        last_chat_date = last_chat_date.split(" ")[0];
                        last_chat_date = last_chat_date.split("-");
                        last_chat_date = last_chat_date[0]+"/"+last_chat_date[1]+"/"+last_chat_date[2];
                        $("span#last-chat-time").text(last_chat_date);
                        $("span#topic").text(obj['topic']);
                        $("span#time-used").text((obj['time_used']/60).toFixed(2));

                    }
                })
                .fail(function(error) {
                    console.error(error);
                });

                var duration_session = 0;

                var timecounter = setInterval(function() {
                    duration_session++;
                    if(duration_session > 30) {

                        $.ajax({
                            url: '/my_account/main/setoonline/'+Chat.controller.lobby.reader_id,
                            type: 'GET',
                            dataType: 'json',

                        })
                        .done(function(setonline) {

                        })
                        .fail(function(error) {
                            console.error(error);
                        });

                        clearInterval(timecounter);

                        setTimeout(function() {

                             window.top.close();

                        }, 2000);
                       

                    }

                    console.log('session was started before '+ duration_session);

                }, 1000);

                $("#chat-attempt").on('click', function() {

                    clearInterval(timecounter);
                    
                    console.log('timecounter is removed');
                });
                
                /*thom ho*/
                function get_attempt_number() {
                    var num;
                    $.ajax({
                        'type': "GET",
                        'url': "/chat/Main/get_attempt_number",
                        'success': function(response) {
                            num = JSON.parse(response);
                            num = num.num;
                            console.log('sdagsdfgsdfg'+num);
                            $("#attempt_number").html(num);
                            if(num == null) {
                                num = 0;
                            }
                            $("#number").val(num);
                            
                        }
                    });
                }

                var data = get_attempt_number();

                /*thom ho end*/

                $(document).on('click', '#retryWithSameReader', function(event) {
                    event.preventDefault();

                    $.ajax({
                        url: '/chat/main/get_reader_status/<?php echo $id ?>/true',
                        type: 'GET',
                        dataType: 'json',
                        
                    })
                    .done(function(reader_status) {
                        
                        if (reader_status) {
                            window.location.reload();
                        }else{
                            $('#dialog-abort-retry').dialog("close");
                            $('#dialog-offline-retry').dialog("open");
                        }
                    })
                    .fail(function(error) {
                        console.error(error);
                    });
                    

                });

                // <!-- check idel time -->
                
                
                $("#topic").keypress(function(e) {
                    if(e.which == 13) {
                      Chat.controller.lobby.actions.chat_attempt('<?php echo $id ?>');
                    }
                });

                ion.sound({
                    sounds: [
                        {name: "door_bell"},
                        {name: "bell_ring"}
                    ],
                    path: "/media/sounds/",
                    preload: true,
                    volume: 1
                });

                Chat.run.lobby();
                <?php if (isset($idel_time)): ?>
                    <?php if ($idel_time <= 2 ): ?>
                    $('#wait-for2min').dialog("open");
                    // $('#dialog-offline-retry').dialog("open");
                    $.ajax({
                        url: '/my_account/main/setoonline/<?php echo $id ?>',
                        type: 'GET',
                        dataType: 'json',
                    })
                    .done(function(cb) {
                        console.log(cb);
                    })
                    .fail(function(error) {
                        console.error(error);
                    });
                    <?php endif ?>
                <?php endif ?>

            });

        </script>
    <?php endif; ?>
