<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<header class="page-header" id="top-page">

    <div class="page-header__top">

        <div class="logo">
            <img src="/theme/template/images/logo.png" alt="logo">
        </div>

        <div class="page-header__text">
            <span class="font-w-900">Questions aboutLove, Relationships, Money?</span>
            <span>
                <span>Call our Psychics 24/7</span><span class="font-w-900">1-888-573-9239</span>
            </span>
            <span>1st time callers</span><span class="font-w-900"> only 99¢ </span><span>minute!</span>
        </div>

        <!-- .burger-menu -->

        <div class="burger-menu">
            <div class="bar1"></div>
            <div class="bar2"></div>
            <div class="bar3"></div>
        </div><!-- .burger-menu -->

        <div class="page-header__authorization">

            <div class="page-header__social">
                <ul>
                    <li class="instagram-icon"><a href="#"></a></li>
                    <li class="twitter-icon"><a href="#"></a></li>
                    <li class="google-plus-icon"><a href="#"></a></li>
                    <li class="behance-icon"><a href="#"></a></li>
                    <li class="facebook-icon"><a href="#"></a></li>
                    <li class="pinterest-p-icon"><a href="#"></a></li>
                </ul>
            </div>
        </div><!-- .page-header__authorization -->

    </div><!-- .page-header__top -->

    <div class="clear"></div>

    <!-- check is promo set -->

    <div class="header-banner my-container">
        <h2>Christmas sale promotion</h2>
    </div><!-- .header-banner -->

    <div class="page-header__bot">

        <div class="my-container">

        </div><!-- .my-container -->

    </div><!-- .page-header__bot -->

</header><!-- .page-header -->