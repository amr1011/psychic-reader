<footer class="footer footer-default">
    <div class="container">
      <nav class="float-left">
        <ul>
          <li>
            <a href="#">
              About Us
            </a>
          </li>
          <li>
            <a href="https://www.psychic-contact.net">
              Blog
            </a>
          </li>
         
        </ul>
      </nav>
      <div class="copyright float-right">
        &copy;
        <script>
          document.write(new Date().getFullYear())
        </script>
        Jayson Lynn . Net
      </div>
    </div>
  </footer>