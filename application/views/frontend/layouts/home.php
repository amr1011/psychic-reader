<div class="content-block__header">
    <h2>why we are?</h2>
    <p>Welcome to Psychic Contact, where we have been providing truly genuine Live Online Psychic Readings through chat, email, and phone for <span class="font-w-900">more than 17 years.</span> We are one of the leading online websites where people from all walks of life come to us for caring Psychic advice.</p>
</div><!-- .content-block__header -->

<div class="content-block__article"><!-- 1 -->
    <h3 class="hide-title">common<br>areas</h3>
    <h3 class="show-title">common areas</h3>
    <div class="content-block__content">
        <p>One of the most common areas of concern is advice about finding love, or keeping
            love. Clients are interested in enriching their relationships. Often they need to be given
            the nudge to empower themselves to make the necessary changes in their lives,
            whether it's finding a new partner, career, starting a new business, or just simply how to
            move forward. Some people may merely wish to find happiness, or excel in their life's
            journey or purpose. <span class="font-w-900 article-span-color">If you're seeking spirituality,</span> or specific answers, our psychic
            readers can assist you through many areas of expertise including: clairvoyance,
            mediumship, astrology, angel card readings, spirit guides, tarot cards, numerology
            and so much more.</p>
    </div>
</div><!-- .content-block__article -->

<div class="content-block__article"><!-- 2 -->
    <h3 class="hide-title">what we<br>offer</h3>
    <h3 class="show-title">what we offer</h3>
    <div class="content-block__content">
        <p>We don't provide quick readings, we believe in providing a complete experience and
            developing an ongoing relationship with our clients. We offer a 7 minutes online chat
            reading for new clients for only <span class="font-w-900 article-span-color">$5.99, without any obligation,
            </span> so you can try us out and see if our Psychics are right for you. We guarantee that you'll receive open, honest and
            sincere guidance for those difficult questions that life can bring you.</p>
    </div>
</div><!-- .content-block__article -->

<div class="content-block__article"><!-- 3 -->
    <h3 class="hide-title">elite group<br>of psychic</h3>
    <h3 class="show-title">elite group of psychic</h3>
    <div class="content-block__content">
        <p>We don't accept just any psychic to our site. Our <span class="font-w-900 article-span-color">live online psychic readers</span> have
            been hand-picked and extensively tested for their accuracy, professionalism, and
            authenticity. You can rest assured that the readings are authentic, and not from some
            scripted text. We also ensure ourpsychics possess the training and knowledge required,
            for example, astrology, before offering their services to you. Our elite group of psychic
            professionals is our family, and we're proud to have them working with us. Our
            compassionate psychic readers utilize their metaphysical gifts in providing you with the
            tools to enrich your life.</p>
    </div>
</div><!-- .content-block__article -->

<div class="content-block__article"><!-- 4 -->
    <h3 class="hide-title">live online<br>psychics</h3>
    <h3 class="show-title">live online psychics</h3>
    <div class="content-block__content">
        <p>Our psychics can provide a caring, in-depth clairvoyant, astrology, medium or tarot card
            reading for every question in your life. Even if you don't have a question, a reading can
            be an informative way to discern if your life is on the right path.<span class="font-w-900 article-span-color"> Our live online psychic readers</span> can bring insights into problems that you may be facing.Our psychics possess empathy and understanding, and are non-judgemental.
            We provide a secure and safe environment where you can experience live online
            Psychic Readings by private chat, in email, or on the phone.</p>
    </div>
</div><!-- .content-block__article -->

<div class="wrapper-my-blogs">

    <div class="my-blogs">

        <div class="articles-blog"><!-- 1 -->
            <div class="articles-blog__header">
                <span>Articles / Blog</span>
            </div>

            <div class="articles-wrapper">

                <div class="articles-blog__nav">
                    <ul>
                        <li><a href="#">Spiritual articles</a></li>
                        <li><a href="#">Psychic blog</a></li>
                        <li><a href="#">New - Spirit Animals with Aurors</a></li>
                        <li><a href="#">Naw - Tarot Troughts with Jason</a></li>
                        <li><a href="#">Psychic Astrology with Summer</a></li>
                    </ul>
                </div><!-- .blog-nav -->

                <div class="articles-blog__picture">
                    <img src="/theme/template/images/blog-1.jpg" alt="blog picture">
                </div>

            </div><!-- .articles-wrapper -->
        </div><!-- .articles-blog --><!-- 1 -->


        <div class="articles-blog"><!-- 2 -->
            <div class="articles-blog__header">
                <span>Articles / Blog</span>
            </div>

            <div class="articles-wrapper">

                <div class="articles-blog__nav">
                    <ul>
                        <li><a href="#">Monthly Membership</a></li>
                        <li><a href="#">Specials</a></li>
                        <li><a href="#">Bookstore / Gift</a></li>
                        <li><a href="#">«Psychic ? Go fish!!»</a></li>
                        <li><a href="#">Avatar Quick Qestion</a></li>
                    </ul>
                </div><!-- .blog-nav -->

                <div class="articles-blog__picture">
                    <img src="/theme/template/images/blog-2.jpg" alt="blog picture">
                </div>

            </div><!-- .articles-wrapper -->
        </div><!-- .articles-blog --><!-- 2 -->

    </div><!-- .my-blogs -->


    <div class="fade slider-check">

        <div class="content-block__check"><!-- 1 -->
            <span>
                Check out our new <a href="#">Video.</a> Or try <a href="#">www.taroflash.com</a>
            </span>
            <span>Sign up to our Newsletter today to receive our exclusive email offers.</span>
            <button class="btn btn-sign-up" type="button">sign  up</button>
        </div>

        <div class="content-block__check"><!-- 2 -->
            <span>
                Check out our new <a href="#">Video.</a> Or try <a href="#">www.taroflash.com</a>
            </span>
            <span>Sign up to our Newsletter today to receive our exclusive email offers.</span>
            <button class="btn btn-sign-up" type="button">sign  up</button>
        </div>

        <div class="content-block__check"><!-- 3 -->
            <span>
                Check out our new <a href="#">Video.</a> Or try <a href="#">www.taroflash.com</a>
            </span>
            <span>Sign up to our Newsletter today to receive our exclusive email offers.</span>
            <button class="btn btn-sign-up" type="button">sign  up</button>
        </div>

    </div><!-- .fade .slider-check-->

</div><!-- .wrapper-my-blogs -->