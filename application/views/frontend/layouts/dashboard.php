<?php
$meta = $this->system_vars->meta_tags($this->uri->uri_string());
if(!isset($meta['title']))
{
    $meta = $this->system_vars->meta_tags();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    PSYCHIC CONTACT
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <link href="/theme/template/backend/css/material-dashboard.css?v=2.1.0" rel="stylesheet" />
  <link href="/theme/template/backend/css/app.css" rel="stylesheet" />

</head>



<body class="">
    <div class="wrapper ">


        {sidebar}
            <div class="main-panel">
                {header}


                    <div class="content">
                        <div class="container-fluid">
                                {content}
                        </div>
                    </div>

            </div>


    </div>

            <!-- start new js plugins -->

                <!--   Core JS Files   -->
              <script src="/theme/template/backend/js/core/jquery.min.js"></script>
              <script src="/theme/template/backend/js/core/popper.min.js"></script>
              <script src="/theme/template/backend/js/core/bootstrap-material-design.min.js"></script>
              <script src="/theme/template/backend/js/plugins/perfect-scrollbar.jquery.min.js"></script>
              <!-- Plugin for the momentJs  -->
              <script src="/theme/template/backend/js/plugins/moment.min.js"></script>
              <!--  Plugin for Sweet Alert -->
              <script src="/theme/template/backend/js/plugins/sweetalert2.js"></script>
              <!-- Forms Validations Plugin -->
              <script src="/theme/template/backend/js/plugins/jquery.validate.min.js"></script>
              <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
              <script src="/theme/template/backend/js/plugins/jquery.bootstrap-wizard.js"></script>
              <!--  Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
              <script src="/theme/template/backend/js/plugins/bootstrap-selectpicker.js"></script>
              <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
              <script src="/theme/template/backend/js/plugins/bootstrap-datetimepicker.min.js"></script>
              <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
              <script src="/theme/template/backend/js/plugins/jquery.dataTables.min.js"></script>

              <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>

              <!--  Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
              <script src="/theme/template/backend/js/material-dashboard.js?v=2.1.0" type="text/javascript"></script>
              <!-- Material Dashboard DEMO methods, don't include it in your project! -->
              <script src="/theme/template/backend/demo/demo.js"></script>


              <script>
                $(document).ready(function() {
                  $().ready(function() {
                    $sidebar = $('.sidebar');
                    $sidebar_img_container = $sidebar.find('.sidebar-background');
                    $full_page = $('.full-page');
                    $sidebar_responsive = $('body > .navbar-collapse');
                    window_width = $(window).width();
                    fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();
                    if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
                      if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
                        $('.fixed-plugin .dropdown').addClass('open');
                      }
                    }
                  });
                });
              </script>
             
            <!-- end new js plugins -->


            <audio id="audio" src="/media/sounds/there_alert.wav" autostart="false" ></audio>
            <script src="/theme/template/scripts/jquery-ui.min.js"></script>
            <script src="<?= CHAT_URL ?>:<?= CHAT_PORT ?>/auth.js"></script>
            <script src="<?= CHAT_URL ?>:<?= CHAT_PORT ?>/socket.io/socket.io.js"></script>

            <script src="/chat/app/chat_all_lobby.js?ts=<?=time(); ?>" ></script>        
            <script src='/media/javascript/ion.sound.min.js'></script>
            <!-- 9-4 -->
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>

            <!-- 9-8 -->
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.min.js"></script>

        <?php if ($this->session->userdata('member_logged')): ?>

            <?php $socket_url = CHAT_URL; ?>
            <?php $socket_port = CHAT_PORT ?>
            <?php $mem_id = $this->member->data['id']; ?>
            <?php $member_username = $this->member->data['username']; ?>
            <?php $member_id_hash = $this->member->data['member_id_hash']; ?>
            <?php $member_type = (is_null($this->member->data['profile_id'])) ? 'client' : 'reader'; ?>
            <?php $chat_try_sound = $this->member->data['chat_try_sound']; ?>

            <!-- 9-8 -->
            <?php $pause_timer_type = $this->member->data['pause_timer']; ?>
            <?php $pause_timer_size = $this->member->data['pause_timer_size']; ?>
            <?php $pause_timer_switch = $this->member->data['pause_timer_flag']; ?>

            <script>
                var chat_init_data = {
                    'member_type': '<?= $member_type ?>',
                    'member_id': '<?= $mem_id ?>',
                    'member_id_hash': '<?= $member_id_hash ?>',
                    'member_username': '<?= $member_username ?>',
                    '_disconnect_url': '<?= $this->config->item('site_url') . "/main/disconnect_user/" . $mem_id ?>',
                    'socket_url': '<?= CHAT_URL ?>',
                    'socket_port': '<?= CHAT_PORT ?>',
                    'site_url': '<?= SITE_URL ?>'
                };

                $(document).ready(function(){

                    setTimeout(function () {

                        $('select[name="to"]').multiselect({

                            numberDisplayed: 1,
                            disableIfEmpty: true,
                            includeSelectAllOption: true,
                            filterPlaceholder: 'Search',
                            enableFiltering: true,
                            includeFilterClearBtn: false,
                            selectAllNumber: false,
                        });
                    }, 500);

                    $('select[name="to"]').change(function () {
                        $('input[name="recipient"]').val($(this).val());
                    });

                    setTimeout(function(){
                        setInterval(function () {
                            $.ajax({
                                'type': 'POST',
                                'url': '/main/check_disconnected',
                                'data': {'id': '<?=$mem_id;?>'},
                                success: function(data) {
                                    let obj = JSON.parse(data);
                                    let disconnect = obj.disconnect;

                                    if(disconnect == 1) {
                                        window.location.href = 'main/logout';
                                    }
                                }
                            });
                        }, 3000);
                    }, 3000);

                    // ------10-18--------
                    var time = 0;
                    var status = "<?= $this->member->data['status']?>";
                    var countdown_timer;
                    var count_down;
                    var sound = document.getElementById("audio");

                    if(window.location.pathname == "/my_account") {
                        if (status == 'online' || status == 'break') {
                            var timer = setInterval(function () {
                                time ++;
                                if(time == 30) {
                                    sound.play();
                                    $(".check-there").css("display", "block");
                                    $(".popup-overlay, .popup-content").addClass("active");
                                    count_down = 0;
                                    countdown_timer = setInterval(function () {
                                        count_down ++;
                                        if(count_down == 60) {
                                            $.ajax({
                                                'type': "POST",
                                                'url' : "/chat/chatInterface/are_you_there_logout",
                                                'data': { user_id : '<?=$mem_id;?>' },
                                                success: function(data) {
                                                    let obj = JSON.parse(data);
                                                    status = obj.status;
                                                    if(status == "successful") {
                                                        location.href = 'main/logout';
                                                    }
                                                }
                                            });
                                        }
                                    }, 1000);
                                }
                            }, 1000*60);
                        }
                    }

                    $(".close-modal").on("click", function(){
                        $(".popup-overlay, .popup-content").removeClass("active");
                        time = 0;
                        clearInterval(countdown_timer);
                        count_down = 0;
                        sound.pause();
                    });

                    $("#reader-doorbell-sound-value").html("<?=($chat_try_sound)?>");
                    $("select#reader-doorbell-sound").val("<?=($chat_try_sound)?>");  

                    ion.sound({
                        sounds: [
                            {name: "door_bell"},
                            {name: "bell_ring"},
                            {name: "church_bell"},
                            {name: "railroad_bell"},
                            {name: "salamisound_bell"},
                            {name: "service_bell"},
                            {name: "siren_bell"},
                            {name: "warbling_bell"},
                        ],
                        path: "/media/sounds/",
                        preload: true,
                        volume: 1
                    });

                    Chat.run.lobby();

                    $("#save-doorbell-sound").click(function(){
                        $("#reader-doorbell-sound-value").html($("#reader-doorbell-sound").val());
                        $.post("/chat/chatInterface/save_doorbell_sound_settings", {user_id : '<?=$mem_id;?>', sound : $("#reader-doorbell-sound").val()}, 
                            function(data){
                                if (data.status == "OK")
                                {
                                    console.log('doorbell sound is saved.');
                                }
                        }, "json");              
                    });
                });

            </script>    
        <?php endif; ?>


        <!-- 9-5 -->
            <script>  

             //getBalance for Reader
                function getBalance(region){
                    //console.log(region);
                  
                     $.ajax({
                      type: "POST",
                      url: "/my_account/main/getBalance",
                      data : {"region":region},
                          success: function(oResponse){
                          
                           var uiPhoneContainer = $(".phone-reading"),
                               uiEmailContainer = $(".email-reading");

                               uiPhoneContainer.html("");
                               uiEmailContainer.html(" ");
                             var commission = 0;

                            for (var i in oResponse.data) {
                                var x = oResponse.data[i],
                                    type = oResponse.data[i].type;
                                   
                                    if (type == "reading") {
                                       commission = commission + x.commission;
                                        var phone = '<tr>'+
                                                 '<td>'+x.datetime+'</td>'+
                                                 '<td>'+x.region+'</td>'+
                                                 '<td>'+x.commission+'</td>'+
                                                 '</tr>';

                                    }

                                    if (type == "email") {
                                       var email = '<tr>'+
                                                 '<td>'+x.datetime+'</td>'+
                                                 '<td>'+x.region+'</td>'+
                                                 '<td>'+x.commission+'</td>'+
                                                 '</tr>';
                                    }   
                                  
                               
                            }
                               uiPhoneContainer.append(phone );
                               uiEmailContainer.append(email);
                                console.log(commission);
                                
                          }

                    });
                     
                }  
            $(document).ready(function () {

                getBalance();
                /*add to Giveback page if I am a new client (when a user registers as a new client)*/
                $.ajax({

                    url: '/chat/main/add_to_giveback/' + '<?=$this->member->data['id']?>',
                    type: 'GET',

                }).done(function (response) {


                }).fail(function (error) {
                    console.error(error);
                });



                $('.select-currency').on('change', function() {
                  var region =  this.value;

                     getBalance(region);
                });

               
                



                // --------------------  add to Giveback list  -------------------

                var declineTimer = setInterval(function () {

                    $.ajax({

                        url: '/chat/main/get_decline/' + '<?=$this->member->data['id']?>',
                        type: 'GET',

                    }).done(function (response) {

                        var obj = JSON.parse(response);
                        var data = obj['appointments'];

                        for (var i = 0; i < data.length; i++) {

                            var messageText = 'Hi ' + '<?=$this->member->data['username']?>, ' + data[i]['reader_username']
                                + ' declined the appointment for ' + data[i]['appointment_date'];

                            $.notify(messageText, {
                                className: "error",
                                hideDuration: 700,
                                autoHideDelay: 60000,
                                position: 'right buttom'
                            });
                        }
                        //console.log(obj);

                    }).fail(function (error) {
                        console.error(error);
                    });

                }, 3000);

                var acceptTimer = setInterval(function () {

                    $.ajax({

                        url: '/chat/main/get_accept/' + '<?=$this->member->data['id']?>',
                        type: 'GET',

                    }).done(function (response) {

                        var obj = JSON.parse(response);
                        var data = obj['appointments'];

                        for (var i = 0; i < data.length; i++) {

                            var messageText2 = 'Hi ' + '<?=$this->member->data['username']?>, ' + data[i]['reader_username']
                                + ' accepted the appointment for ' + data[i]['appointment_date'];

                            $.notify(messageText2, {
                                className: "info",
                                hideDuration: 700,
                                autoHideDelay: 60000,
                                position: 'right buttom'
                            });
                        }
                        // console.log(obj);

                    }).fail(function (error) {
                        console.error(error);
                    });

                }, 3000);

                /*9-16*/
                function blinker() {
                    $('.bell').fadeOut(1000);
                    $('.bell').fadeIn(1000);
                }

                /*9-14*/
                function get_week_day(week_day) {

                    switch (week_day) {

                        case 1:
                            return "Monday";
                            break;
                        case 2:
                            return "Tuesday";
                            break;
                        case 3:
                            return "Wednesday";
                            break;
                        case 4:
                            return "Thursday";
                            break;
                        case 5:
                            return "Friday";
                            break;
                        case 6:
                            return "Saturday";
                            break;
                        case 7:
                            return "Any Day of Week";
                            break;
                        case 0:
                            return "Sunday";
                            break;

                    }
                }

                setTimeout(function () {
                    $.ajax({

                        url: '/chat/main/get_gift_info/' + '<?=$this->member->data['id']?>',
                        type: 'GET',

                    }).done(function (response) {

                        var obj = JSON.parse(response);
                        var data = obj.data;

                        for (var i = 0; i < data.length; i++) {

                            if (data[i]['reader_name'] != undefined) {

                                var week_day = get_week_day(parseInt(data[i]['week_day']));

                                var message = 'Hi ' + '<?=$this->member->data['username']?>, ' + data[i]['reader_name'] + ' has given you a "Giveback Gift" of ' + data[i]['gift_size'] + ' mins ' + 'for ' + week_day;

                                $.notify(message, {
                                    className: "info",
                                    hideDuration: 700,
                                    autoHideDelay: 60000,
                                    position: 'left top'
                                });

                                setInterval(blinker, 2000);

                            }
                        }
                        /*9-23*/
                        for (var j = 1; j <= $(".notifyjs-wrapper").length; j++) {
                            $(".notifyjs-corner .notifyjs-wrapper:nth-child(" + j + ")").prepend("<span class='bell' style='color: white;font-size: 60px; margin-top: -2px;'><i class='fa  fa-long-arrow-right'></i></span>").css('display', 'inline-flex');
                        }

                    }).fail(function (error) {
                        console.error(error);
                    });
                }, 1000);

                if ($('p.record').html()) {
                    $.notify($('p.record').html(), {
                        className: "success",
                        align: "center",
                        verticalAlign: "top",
                        hideDuration: 700,
                        autoHideDelay: 9000,
                    });
                }

            });
        
        </script>

        <?php 
        if (isset($load_js))
            $this->load->view($load_js); 

        ?>

</body>
     


</html>
