
<?php
    $meta = $this->system_vars->meta_tags($this->uri->uri_string());
    $description ='';
    if (!isset($meta['title'])) {
        $meta = $this->system_vars->meta_tags();    
    }
    $description = preg_replace('/<[^>]*>/', '', $meta['description']);
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="./assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title><?= $meta['title'] ?></title>
        <meta name="keywords" content="<?php echo $meta['keywords'] ?>">
        <meta name="description" content="<?php echo $description?>">

        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
        <!-- CSS Files -->
        <link href="/theme/template/styles/material-kit.min.css?v=2.1.1" rel="stylesheet" />
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="/theme/template/styles/demo.css" rel="stylesheet" />
        <link href="/theme/template/styles/vertical-nav.css" rel="stylesheet" />
        <script> var SITE_URL = '<?php echo SITE_URL; ?>';</script>
    </head>    

    <body class="sections-page sidebar-collapse">


        {header}

        <div class="main">
            <div class="section-space"></div>
               {banner}

              <!--     *********    Featured Readers     *********      -->
              <div class="team-5" >
                <div class="container">
                  <div class="row">
                     {content}
                  </div>
                </div>
              </div>
              <!--     *********    featured readers     *********      -->

            </div>
</div>              
</body>     
         
{footer}
<script src="/theme/template/scripts/core/jquery.min.js" type="text/javascript"></script>
<script src="/theme/template/scripts/core/popper.min.js" type="text/javascript"></script>
<script src="/theme/template/scripts/core/bootstrap-material-design.min.js" type="text/javascript"></script>
<script src="/theme/template/scripts/plugins/moment.min.js" type="text/javascript"></script>  
<script src="/theme/template/scripts/modernizr.js" type="text/javascript"></script>  
<script src="/theme/template/scripts/vertical-nav.js" type="text/javascript"></script>
<script src="/theme/template/scripts/demo.js" type="text/javascript"></script>    
<script src="/theme/template/scripts/material-kit.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {

        setTimeout(function () {

            $('#favorite-option').multiselect({

                numberDisplayed: 1,
                disableIfEmpty: true,
                includeSelectAllOption: true,
                filterPlaceholder: 'Search',
                enableFiltering: true,
                includeFilterClearBtn: false,
                selectAllNumber: false,
            });
        }, 500);

        $('#save-favorite-option').click(function () {

            $.ajax({
                'type': "POST",
                'url': "/my_account/main/save_option",
                'data': {option_data: $('#favorite-option').val()},
                success: function (data) {
                    console.log(data);
                }
            });
        });

        // if(window.location.pathname == "/psychics") {
        //     setTimeout(function () {
        //         setInterval(function () {
        //             $.ajax({
        //
        //                 url: '/main/check_online_readers',
        //                 type: 'GET',
        //
        //             }).done(function (response) {
        //
        //                 let obj = JSON.parse(response);
        //                 let status = obj.status;
        //                 let message = obj.alert.message;
        //                 if(status == true) {
        //                     $.notify(message,  {className: "warning", hideDuration: 700, autoHideDelay: 60000, position: 'top left'});
        //                     setTimeout(function () {
        //                         window.location.href = 'https://www.taroflash.com';
        //                     }, 5000);
        //
        //                 } else {
        //
        //                 }
        //             }).fail(function (error) {
        //                 console.error(error);
        //             });
        //         }, 7000);
        //     }, 1000);
        //
        // }

        $('#rating').raty
        ({
            score: function () {
                return $(this).attr('data-rating');
            }
        });
    });
</script>
</body>
</html>
