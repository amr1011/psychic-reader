 <div class="cd-section" id="headers">

     <!-- Carousel Card -->
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
      </ol>
      <div class="carousel-inner">
          <div class="carousel-item active">
            <div class="page-header header-small header-filter" style="background-image: url('/theme/template/images/new_banner1.jpg');">
              <div class="container">
                <div class="row">
                  <div class="col-md-7  text-left" >                                  
                      <h4 style="text-shadow: 0px 3px 2px rgba(20,9,5,0.6);">Accurate, Compassionate, Professional & Ethical Psychic Readers.</h4>
                      <h4 style="text-shadow: 0px 3px 2px rgba(20,9,5,0.6);" >LIVE PSYCHIC READINGS 24/7</h4>
                      <h4 style="text-shadow: 0px 3px 2px rgba(20,9,5,0.6);">Satisfaction 100% Always Guaranteed!</h4>
                      <h4 style="text-shadow: 0px 3px 2px rgba(20,9,5,0.6);"> Call our Psychics  1-888-573-9239 </h4>
                    <br>
                    <div class="buttons">
                      <a href="/register" class="btn btn-success btn-lg">
                        Free Signup
                      </a>
                      <a href="/register/login" class="btn btn-warning btn-lg">
                        Login
                      </a>
                      
                    </div>
                  </div>
                </div>
              </div> <!--end container -->
            </div> <!--end page-header -->
          </div> <!-- end caroud-item -->

            <div class="carousel-item">
              <div class="page-header header-small header-filter" style="background-image: url('/theme/template/images/new_banner2.jpg');">
                <div class="container">
                  <div class="row">
                    <div class="col-md-7  text-left">
                      <h1 class="title" style="text-shadow: 0px 3px 2px rgba(20,9,5,0.6);">Call our Psychics</h1>
                        <h4 style="text-shadow: 0px 3px 2px rgba(20,9,5,0.6);">Accurate, Compassionate, Professional & Ethical Psychic Readers.</h4>
                        <h4 style="text-shadow: 0px 3px 2px rgba(20,9,5,0.6);">LIVE PSYCHIC READINGS 24/7</h4>
                        <h4 style="text-shadow: 0px 3px 2px rgba(20,9,5,0.6);">Satisfaction 100% Always Guaranteed!</h4>
                      
                    </div>
                  </div>
                </div>
              </div>
            </div> <!-- end carousel-item -->

            <div class="carousel-item">
              <div class="page-header header-small header-filter" style="background-image: url('/theme/template/images/new_banner3.jpg');">
                <div class="container">
                  <div class="row">
                    <div class="col-md-8 ml-auto mr-auto text-center">
                      <h1 class="title" style="text-shadow: 0px 3px 2px rgba(20,9,5,0.6);">Call our Psychics</h1>
                        <h4 style="text-shadow: 0px 3px 2px rgba(20,9,5,0.6);">Accurate, Compassionate, Professional & Ethical Psychic Readers.</h4>
                        <h4 style="text-shadow: 0px 3px 2px rgba(20,9,5,0.6);">LIVE PSYCHIC READINGS 24/7</h4>
                        <h4 style="text-shadow: 0px 3px 2px rgba(20,9,5,0.6);">Satisfaction 100% Always Guaranteed!</h4>
                        <h4 style="text-shadow: 0px 3px 2px rgba(20,9,5,0.6);"> Call our Psychics  1-888-573-9239 </h4>
         
                      
                    </div>
                  </div>
                </div>
              </div>
            </div> <!-- end carousel-item -->     


            <div class="carousel-item">
              <div class="page-header header-small header-filter" style="background-image: url('/theme/template/images/new_banner4.jpg');">
                <div class="container">
                  <div class="row">
                    <div class="col-md-8 ml-auto mr-auto text-center">
                      <h1 class="title" style="text-shadow: 0px 3px 2px rgba(20,9,5,0.6);">Call our Psychics</h1>
                        <h4 style="text-shadow: 0px 3px 2px rgba(20,9,5,0.6);">Accurate, Compassionate, Professional & Ethical Psychic Readers.</h4>
                        <h4 style="text-shadow: 0px 3px 2px rgba(20,9,5,0.6);">LIVE PSYCHIC READINGS 24/7</h4>
                        <h4 style="text-shadow: 0px 3px 2px rgba(20,9,5,0.6);">Satisfaction 100% Always Guaranteed!</h4>
                        <h4 style="text-shadow: 0px 3px 2px rgba(20,9,5,0.6);"> Call our Psychics  1-888-573-9239 </h4>                      
                    </div>
                  </div>
                </div>
              </div>
            </div> <!-- end carousel-item -->                      
        
      </div> <!-- end carousel-innter -->

      <!-- <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <i class="material-icons">keyboard_arrow_left</i>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <i class="material-icons">keyboard_arrow_right</i>
        <span class="sr-only">Next</span>
      </a> -->
      <!-- End Carousel Card -->
</div> <!-- cd-section -->

