<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="./assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>
        Psychic Contact 
        </title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
        <!-- CSS Files -->
        <link href="/theme/template/styles/material-kit.min.css?v=2.1.1" rel="stylesheet" />
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="/theme/template/styles/demo.css" rel="stylesheet" />
        <link href="/theme/template/styles/vertical-nav.css" rel="stylesheet" />
        <script> var SITE_URL = '<?php echo SITE_URL; ?>';</script>
    </head>    

    <body class="sections-page sidebar-collapse">


        {header}

        <div class="main">
            <div class="section-space"></div>
               {banner}
                <!--     *********    Featured Readers     *********      -->
                <div class="team-5" >
                  <div class="container">
                    <div class="row">
                      <div class="col-md-8 ml-auto mr-auto text-center">
                        <h2 class="title text-warning" >Featured Readers</h2>
                        </br>
                      </div>
                    </div>


                    <div class="row">
                    <?php $ctr = 0; ?>
                    <?php foreach($featured_readers as $value): ?>
                      <div class="col-md-4 d-flex align-items-stretch">
                        <div class="card card-profile">
                            <div class="card-header card-avatar">
                              <a href="#">
                                <img class="img" src="/media/assets/<?php echo $value->profile_image; ?>" />
                              </a>
                            </div>
                            <div class="card-body ">
                              <h4 class="card-title"><?php echo ucfirst($value->username); ?></h4>

                               <?php 
                                  switch ($value->status) {
                                      case 'online':
                                          $status = "I am available";
                                          $attrib ="text-success";
                                          break;
                                      case 'break';

                                          $status = "Be Right Back on ". $reader['break_time_amount'] ."  minutes";
                                          $attrib ="text-muted";
                                          break;                        
                                      default:
                                          $status = "I am offline";
                                          $attrib ="text-muted";
                                          break;
                                  }


                                ?>
                              <h6 class="card-category <?php echo $status ?>"><?php echo $status; ?></h6>
                              <p class="card-description">
                                <?php echo $value->area_of_expertise; ?>
                              </p>
                            </div>
                            <div class="card-footer justify-content-center">
                              <a href="/register/login" target="_self" class="btn btn-just-icon btn-link btn-phone"><i class="fa fa-phone"></i></a>
                              <a href="/register/login" target="_self" class="btn btn-just-icon btn-link btn-chat"><i class="fa fa-comments"></i></a>
                              <a href="/register/login" target="_self" class="btn btn-just-icon btn-link btn-appointment"><i class="fa fa-calendar"></i></a>

                              <a href="/profile/<?php echo $value->username; ?>" class="btn  btn-sm btn-round">Read biography</a>

                            </div>

                            
                            
                          </div>
                      </div> <!-- end col-md-4 -->
                      <?php endforeach; ?>
                     
                     
                    </div>
                  </div>
                </div>
                <!--     *********    featured readers     *********      -->
              <!--     *********    online readers     *********      -->
              <div class="testimonials-2">
                <div class="container">
                  <div class="row">
                    <div class="col-md-12">
                      <div id="carouselExampleIndicatorss" class="carousel slide" data-ride="carousel">
                        <div class="col-md-8 ml-auto mr-auto text-center">
                                <h2 class="title" >Meet our Professional Psychic Readers!</h2>
                            </div>
                        <div class="carousel-inner">
                            
                          <?php $ctr = 0; ?>
                          <?php foreach($readers as $value): ?>
                            <?php $active = ''; $ctr++; if ($ctr == 1) $active = "active"; ?>

                              <div class="carousel-item <?php echo $active ?> ">
                                <div class="card card-testimonial card-plain">
                                  <div class="card-header card-avatar">
                                    <a href="#">                                    
                                      <img class="img-raised rounded img-fluid" src="<?php echo $value['profile']; ?>" />
                                    </a>
                                  </div>
                                  <div class="card-body">
                                    <h5 class="card-description"><?php echo $value['area_of_expertise']; ?>
                                    </h5>
                                    <h4 class="card-title"><?php echo strtoupper($value['username']); ?></h4>
                                    <h6 class="card-category text-muted"></h6>
                                  </div>
                                  <div class="card-footer justify-content-center">
                                            <a href="/register/login" target="_self" class="btn btn-primary">Chat NOW</a>
                                            <a href="/register/login" target="_self" class="btn btn-primary ">Call Now</a>
                                  </div>
                                </div>
                              </div>
                         <?php endforeach; ?>

                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicatorss" role="button" data-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicatorss" role="button" data-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!--     *********    END TESTIMONIALS 2      *********      -->

               



            <!--     *********     BLOGS 3      *********      -->
            <div class="blogs-3">
              <div class="container">
                <div class="row">
                  <div class="col-md-10 ml-auto mr-auto">
                    <h2 class="title">Why we are?</h2>
                    <p>Welcome to Psychic Contact, where we have been providing truly genuine Live Online Psychic Readings through chat, email, and phone for more than 17 years. We are one of the leading online websites where people from all walks of life come to us for caring Psychic advice.</p>
                    <br>
                    <div class="card card-plain card-blog">
                      <div class="row">
                        <div class="col-md-4">
                          <div class="card-header card-header-image">
                            <img class="img img-raised" src="/theme/template/images/bg-check.jpg">
                          </div>
                        </div>
                        <div class="col-md-8">
                            <h3 class="card-title">
                            <a href="#pablo">Common Areas</a>
                          </h3>
                          <p class="card-description">
                           One of the most common areas of concern is advice about finding love, or keeping love. Clients are interested in enriching their relationships. Often they need to be given the nudge to empower themselves to make the necessary changes in their lives, whether it's finding a new partner, career, starting a new business, or just simply how to move forward. Some people may merely wish to find happiness, or excel in their life's journey or purpose. If you're seeking spirituality, or specific answers, our psychic readers can assist you through many areas of expertise including: clairvoyance, mediumship, astrology, angel card readings, spirit guides, tarot cards, numerology and so much more.
                          </p>
                          
                        </div>
                      </div>
                    </div>
                    <div class="card card-plain card-blog">
                      <div class="row">
                        <div class="col-md-4">
                          <div class="card-header card-header-image">
                            <a href="#pablo">
                              <img class="img img-raised" src="/theme/template/images/card-blog2.jpg">
                            </a>
                          </div>
                        </div>
                        <div class="col-md-8">
                          <h3 class="card-title">
                            <a href="#pablo">What we offer</a>
                          </h3>
                          <p class="card-description">
                           We don't provide quick readings, we believe in providing a complete experience and developing an ongoing relationship with our clients. We offer a 7 minutes online chat reading for new clients for only $5.99, without any obligation, so you can try us out and see if our Psychics are right for you. We guarantee that you'll receive open, honest and sincere guidance for those difficult questions that life can bring you.
                          </p>
                          
                        </div>
                      </div>
                    </div>
                    <div class="card card-plain card-blog">
                      <div class="row">
                        <div class="col-md-4">
                          <div class="card-header card-header-image">
                            <a href="#pablo">
                              <img class="img img-raised" src="/theme/template/images/valuation-block.jpg">
                            </a>
                            <div class="colored-shadow" style="background-image: url('./assets/img/examples/blog8.jpg')"></div>
                          </div>
                        </div>
                        <div class="col-md-8">
                          
                          <h3 class="card-title">
                            <a href="#pablo">Elite group of psychic</a>
                          </h3>
                          <p class="card-description">
                            We don't accept just any psychic to our site. Our live online psychic readers have been hand-picked and extensively tested for their accuracy, professionalism, and authenticity. You can rest assured that the readings are authentic, and not from some scripted text. We also ensure ourpsychics possess the training and knowledge required, for example, astrology, before offering their services to you. Our elite group of psychic professionals is our family, and we're proud to have them working with us. Our compassionate psychic readers utilize their metaphysical gifts in providing you with the tools to enrich your life.
                          </p>
                          
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--     *********    END BLOGS 4      *********      -->



    </div> <!-- end main -->

    {footer}

  <!--   Core JS Files   -->
    <script src="/theme/template/scripts/core/jquery.min.js" type="text/javascript"></script>
    <script src="/theme/template/scripts/core/popper.min.js" type="text/javascript"></script>
    <script src="/theme/template/scripts/core/bootstrap-material-design.min.js" type="text/javascript"></script>
    <script src="/theme/template/scripts/plugins/moment.min.js" type="text/javascript"></script>  
    <script src="/theme/template/scripts/modernizr.js" type="text/javascript"></script>  
    <script src="/theme/template/scripts/vertical-nav.js" type="text/javascript"></script>
    <script src="/theme/template/scripts/demo.js" type="text/javascript"></script>    
    <script src="/theme/template/scripts/material-kit.min.js" type="text/javascript"></script>

       
    </body>
</html>
