<?php
$meta = $this->system_vars->meta_tags($this->uri->uri_string());
if(!isset($meta['title']))
{
    $meta = $this->system_vars->meta_tags();
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?=$meta['title']?></title>
        <meta name="keywords" content="<?php echo $meta['keywords']?>">
        <meta name="description" content="<?php echo $meta['description']?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="/theme/template/styles/load-styles.css" rel="stylesheet" media="screen">
        <link href="/theme/template/styles/main.css" rel="stylesheet" media="screen">
        <!-- 9-8 -->
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css">
        <!-- <link href="images/favicon.ico" rel="icon" type="image/x-icon"> -->
    </head>
    <style>
        .check-there {

            width: 30%;
            height: 40%;
            position: absolute;
            left: 47%;
            z-index: 1;
        }
        .popup-overlay{
            /*Hides pop-up when there is no "active" class*/
            visibility:hidden;
            position:absolute;
            background:#ffffff;
            border:3px solid #666666;
            width:50%;
            height:50%;
            margin-top: 10%;
            left:15%;
            border-radius: 15px;
            background: lightgray;
        }
        .popup-overlay.active{
            /*displays pop-up when "active" class is present*/
            visibility:visible;
            text-align:center;
        }

        .popup-content {
            /*Hides pop-up content when there is no "active" class */
            visibility:hidden;
        }

        .popup-content.active {
            /*Shows pop-up content when "active" class is present */
            visibility:visible;
            padding-top: 10%;
        }

        .close-modal {
            display:inline-block;
            vertical-align:middle;
            border-radius:30px;
            margin:.20rem;
            font-size: 1.5rem;
            color:#666666;
            background:   #ffffff;
            border:1px solid #666666;
        }

        .close-modal:hover{
            border:1px solid #666666;
            background:#666666;
            color:#ffffff;
        }

    </style>

    <body>
        <div class="wrapper">

            <div class="page-container" data-chat-attemps="0" >

                {header}

                <div class="clear"></div>

                <div class="my-container form-container">
                    <div class="check-there" style="display:none;">
                        <div class="popup-overlay">
                            <!--Creates the popup content-->
                            <div class="popup-content">
                                <h3>Are You There?</h3>
                                <p>Please click this button... or you will be automatically log out after 1 min.</p>
                                <!--popup's close button-->
                                <button class="close-modal">I Am Here</button>
                            </div>
                        </div>
                    </div>

                    {content}

                </div><!-- .my-container -->

            </div><!-- .page-container -->

        </div><!-- .wrapper -->
        <audio id="audio" src="/media/sounds/there_alert.wav" autostart="false" ></audio>
        {footer}
        <script src="/theme/template/scripts/load-scripts.js"></script>
        <script src="/theme/template/scripts/main.js"></script>
        
        <script src='/media/javascript/jqui/jquery-ui-1.8.16.custom.min.js'></script>
        
        <!-- <link rel="stylesheet" href="/media/javascript/jqui/css/overcast/jquery-ui-1.8.16.custom.css" /> -->

        <script src="<?= CHAT_URL ?>:<?= CHAT_PORT ?>/auth.js"></script>
        <script src="<?= CHAT_URL ?>:<?= CHAT_PORT ?>/socket.io/socket.io.js"></script>

        <script src="/chat/app/chat_all_lobby.js?ts=<?=time(); ?>" ></script>        
        <script src='/media/javascript/ion.sound.min.js'></script>
        <!-- 9-4 -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>

        <!-- 9-8 -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.min.js"></script>

        <?php if ($this->session->userdata('member_logged')): ?>

            <?php $socket_url = CHAT_URL; ?>
            <?php $socket_port = CHAT_PORT ?>
            <?php $mem_id = $this->member->data['id']; ?>
            <?php $member_username = $this->member->data['username']; ?>
            <?php $member_id_hash = $this->member->data['member_id_hash']; ?>
            <?php $member_type = (is_null($this->member->data['profile_id'])) ? 'client' : 'reader'; ?>
            <?php $chat_try_sound = $this->member->data['chat_try_sound']; ?>

            <!-- 9-8 -->
            <?php $pause_timer_type = $this->member->data['pause_timer']; ?>
            <?php $pause_timer_size = $this->member->data['pause_timer_size']; ?>
            <?php $pause_timer_switch = $this->member->data['pause_timer_flag']; ?>

            <script>
                var chat_init_data = {
                    'member_type': '<?= $member_type ?>',
                    'member_id': '<?= $mem_id ?>',
                    'member_id_hash': '<?= $member_id_hash ?>',
                    'member_username': '<?= $member_username ?>',
                    '_disconnect_url': '<?= $this->config->item('site_url') . "/main/disconnect_user/" . $mem_id ?>',
                    'socket_url': '<?= CHAT_URL ?>',
                    'socket_port': '<?= CHAT_PORT ?>',
                    'site_url': '<?= SITE_URL ?>'
                };

                $(document).ready(function(){

                    setTimeout(function () {

                        $('select[name="to"]').multiselect({

                            numberDisplayed: 1,
                            disableIfEmpty: true,
                            includeSelectAllOption: true,
                            filterPlaceholder: 'Search',
                            enableFiltering: true,
                            includeFilterClearBtn: false,
                            selectAllNumber: false,
                        });
                    }, 500);

                    $('select[name="to"]').change(function () {
                        $('input[name="recipient"]').val($(this).val());
                    });

                    setTimeout(function(){
                        setInterval(function () {
                            $.ajax({
                                'type': 'POST',
                                'url': '/main/check_disconnected',
                                'data': {'id': '<?=$mem_id;?>'},
                                success: function(data) {
                                    let obj = JSON.parse(data);
                                    let disconnect = obj.disconnect;

                                    if(disconnect == 1) {
                                        window.location.href = 'main/logout';
                                    }
                                }
                            });
                        }, 3000);
                    }, 3000);

                    // ------10-18--------
                    var time = 0;
                    var status = "<?= $this->member->data['status']?>";
                    var countdown_timer;
                    var count_down;
                    var sound = document.getElementById("audio");

                    if(window.location.pathname == "/my_account") {
                        if (status == 'online' || status == 'break') {
                            var timer = setInterval(function () {
                                time ++;
                                if(time == 30) {
                                    sound.play();
                                    $(".check-there").css("display", "block");
                                    $(".popup-overlay, .popup-content").addClass("active");
                                    count_down = 0;
                                    countdown_timer = setInterval(function () {
                                        count_down ++;
                                        if(count_down == 60) {
                                            $.ajax({
                                                'type': "POST",
                                                'url' : "/chat/chatInterface/are_you_there_logout",
                                                'data': { user_id : '<?=$mem_id;?>' },
                                                success: function(data) {
                                                    let obj = JSON.parse(data);
                                                    status = obj.status;
                                                    console.log('send email =>' + status);
                                                    if(status == "successful") {
                                                        location.href = 'main/logout';
                                                    }
                                                }
                                            });
                                        }
                                    }, 1000);
                                }
                            }, 1000*60);
                        }
                    }

                    $(".close-modal").on("click", function(){
                        $(".popup-overlay, .popup-content").removeClass("active");
                        time = 0;
                        clearInterval(countdown_timer);
                        count_down = 0;
                        sound.pause();
                    });

                    $("#reader-doorbell-sound-value").html("<?=($chat_try_sound)?>");
                    $("select#reader-doorbell-sound").val("<?=($chat_try_sound)?>");  

                    ion.sound({
                        sounds: [
                            {name: "door_bell"},
                            {name: "bell_ring"},
                            {name: "church_bell"},
                            {name: "railroad_bell"},
                            {name: "salamisound_bell"},
                            {name: "service_bell"},
                            {name: "siren_bell"},
                            {name: "warbling_bell"},
                        ],
                        path: "/media/sounds/",
                        preload: true,
                        volume: 1
                    });

                    Chat.run.lobby();

                    $("#save-doorbell-sound").click(function(){
                        $("#reader-doorbell-sound-value").html($("#reader-doorbell-sound").val());

                        $.post("/chat/chatInterface/save_doorbell_sound_settings", {user_id : '<?=$mem_id;?>', sound : $("#reader-doorbell-sound").val()}, 
                            function(data){
                                if (data.status == "OK")
                                {
                                    console.log('doorbell sound is saved.');
                                }
                        }, "json");              
                    });
                });

            </script>    
        <?php endif; ?>

    </body>
</html>
