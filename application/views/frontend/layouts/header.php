
<nav class="navbar  bg-white  fixed-top  navbar-expand-lg " id="sectionsNav">
    <div class="container">
      <div class="navbar-translate">
        <a class="navbar-brand" href="/"><img src="/theme/template/images/psycon.png" alt="logo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse">
        <ul class="navbar-nav ml-auto">

           <?php if ($this->session->userdata('member_logged')) { ?>
                <li class="nav-item"><a  class="nav-link" href="/">home</a></li>
                <li class="nav-item"><a  class="nav-link" href="/my_account">my account</a></li>
                <li class="nav-item"><a  class="nav-link" href="/psychics">our psychics</a></li>
                <li class="nav-item"><a  class="nav-link" href="/phone">phone readings</a></li>
                <li class="nav-item"><a  class="nav-link" href="/my_account/email_readings/preview_question">email readings</a></li>
                <li class="nav-item"><a  class="nav-link" href="/package">packages</a></li>                
                <li class="nav-item"><a  class="nav-link" href="/articles">articles</a></li>
                <li class="nav-item"><a  class="nav-link" target="_blank" href="https://www.psychic-contact.net">blog</a></li>


            <?php } else {  ?>    

            <li class="nav-item"><a class="nav-link" href="/">home</a></li>
            <li class="nav-item"><a class="nav-link" href="/register/login">sign-in</a></li>
            <li class="nav-item"><a class="nav-link" href="/register">register</a></li>
            <li class="nav-item"><a class="nav-link" href="/psychics">our psychics</a></li>
            <li class="nav-item"><a class="nav-link" href="/phone">phone readings</a></li>
            <li class="nav-item"><a class="nav-link" href="/my_account/email_readings/preview_question">email readings</a></li>
            <li class="nav-item"><a class="nav-link" href="/package">packages</a></li>
            <li class="nav-item"><a class="nav-link" href="/articles">articles</a></li>
            <li class="nav-item"><a class="nav-link" target="_blank" href="https://www.psychic-contact.net">blog</a></li>

        <?php } ?>

          
        </ul>

      </div>
    </div>
</nav>