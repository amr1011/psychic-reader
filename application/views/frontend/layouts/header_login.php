<header class="page-header" id="top-page">

  <div class="page-header__top">

    <div class="logo">
      <a href="/" ><img src="/theme/template/images/logo.png" alt="logo"></a>
    </div>

    <div class="page-header__text">
      <span class="font-w-900">Questions aboutLove, Relationships, Money?</span>
      <span>
        <span>Call our Psychics 24/7</span><span class="font-w-900">1-888-573-9239</span>
      </span>
      <span>1st time callers</span><span class="font-w-900"> only 99¢ </span><span>minute!</span>
    </div>

	<!-- .burger-menu -->

    <div class="burger-menu">
      <div class="bar1"></div>
      <div class="bar2"></div>
      <div class="bar3"></div>
    </div><!-- .burger-menu -->

    <div class="page-header__authorization">
 
      <form action="#" method="#">
        <input type="search" class="search-top" placeholder="Search">
        <button type="submit" class="btn-search"><i class="fa fa-search" aria-hidden="true"></i></button>
      </form>

      <div class="page-header__social pull-right">
        <ul>
          <li class="instagram-icon"><a href="#"></a></li>
          <li class="twitter-icon"><a href="#"></a></li>
          <li class="google-plus-icon"><a href="#"></a></li>
          <li class="behance-icon"><a href="#"></a></li>
          <li class="facebook-icon"><a href="#"></a></li>
          <li class="pinterest-p-icon"><a href="#"></a></li>
        </ul>
      </div>
    </div><!-- .page-header__authorization -->

  </div><!-- .page-header__top -->

  <div class="clear"></div>

  <!-- check is promo set -->

  <div class="header-banner my-container">
    <h2>Christmas sale promotion</h2>
  </div><!-- .header-banner -->

  <div class="page-header__bot">

    <div class="my-container">

      <nav class="page-header__nav">
        <ul class="page-header__list">
          <!-- 9-15@pen -->

          <?php foreach($pages as $page) {?>
              <li class="nav-point"><a href="/<?php echo($page['url'])?>"><?php echo($page['title'])?></a></li>
          <?php }?>
            <li class="nav-point"><a href="/register">register</a></li>
            <li class="nav-point"><a href="/psychics">our psychics</a></li>
            <li class="nav-point"><a href="/phone">phone readings</a></li>
            <li class="nav-point"><a href="/my_account/email_readings/preview_question">email readings</a></li>
           <!-- <li class="nav-point"><a href="/">home</a></li>
           <li class="nav-point"><a href="/register">register</a></li>
           <li class="nav-point"><a href="/psychics">our psychics</a></li>
           <li class="nav-point"><a href="/phone">phone readings</a></li>
           <li class="nav-point"><a href="#">email readings</a></li>
           <li class="nav-point"><a href="/articles">articles</a></li>
           <li class="nav-point"><a href="/blog">blog</a></li>
           <li class="nav-point"><a href="/newsletter">newsletter</a></li>
           <li class="nav-point"><a href="/package">prices</a></li> -->
        </ul>

        <div class="burger-menu-bot">
          <div class="burger-menu-bot__btn">

            
            <button type="submit" class="burger-menu-bot__login">login</button>

           
            <span>/</span>
            <button type="submit" class="burger-menu-bot__signup">sign up</button>
          </div><!-- .burger-menu-bot__btn -->

          <div class="page-header__social">
            <ul>
              <li class="instagram-icon"><a href="#"></a></li>
              <li class="twitter-icon"><a href="#"></a></li>
              <li class="google-plus-icon"><a href="#"></a></li>
              <li class="behance-icon"><a href="#"></a></li>
              <li class="facebook-icon"><a href="#"></a></li>
              <li class="pinterest-p-icon"><a href="#"></a></li>
            </ul>
          </div><!-- .page-header__social -->

        </div><!-- .burger-menu-bot -->
      </nav>

    </div><!-- .my-container -->

  </div><!-- .page-header__bot -->

</header><!-- .page-header -->