

<div class="profile-content">
      <div class="container">
        <div class="row">
          <div class="col-md-6 ml-auto mr-auto">
            <div class="profile">
              <div class="avatar">
                <img src="<?= $this->reader->data['profile']?>" alt="Circle Image" class="img-raised rounded-circle img-fluid">
              </div>
              <div class="name">
                <h3 class="title"><?= $this->reader->data['username']?></h3>


                <h6><?php echo nl2br($this->reader->data['area_of_expertise']) ?></h6>
                
              </div>
            </div>
          </div>
        </div>
        <div class="description text-center">
          <p><?php echo nl2br($this->reader->data['biography']) ?></p>
        </div>
        <div class="row">
          <div class="col-md-12 ml-auto mr-auto">
              <h6>Review</h6>

              <?php if (count($testis) > 0) : ?>
                <?php foreach ($testis as $t) : ?>
                    <div class="testimonial-block">
                        <img class="quote-img" src="https://c5.patreon.com/external/explore/quote2.png"/>
                        <span style="word-break: break-word; padding: 5px;"><?php echo html_entity_decode(nl2br($t['review'])); ?></span>
                        <?php if($t['profile_image']) : ?>
                            <p><img src="/media/assets/<?php echo($t['profile_image'])?>" class="img-client" /> <span class="name"><?php echo($t['first_name'] .' '. $t['last_name'])?></span></p>
                        <?php else : ?>
                            <p><img src="/media/images/no_profile_image.jpg" class="img-client" /></p>
                        <?php endif;?>
                        <span class="date"><?php echo date("jS F Y", strtotime($t['datetime'])) ?></span>
                        <p>
                            <?php for($i=0; $i<$t['rating']; $i++) : ?>
                                <i class="fa fa-star rating-star"></i>
                            <?php endfor; ?>
                            <?php for($j=0; $j< 5 - $t['rating']; $j++) : ?>
                                <i class="fa fa-star-o rating-star"></i>
                            <?php endfor; ?>
                        </p>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>              
          </div>
        </div>

      </div>
    </div>


<script>
    $(document).ready(function () {
        var height = 0;
        $('div.testimonial-block').each(function(){
            height += parseInt($(this).height()) + 15;
            console.log(height)
        });

        height = height - 600;
        console.log(height)


        $('div').stop(false, false).animate({scrollTop: height},50000);
    })
</script>
