<link rel="stylesheet" href="/media/javascript/jqui/css/overcast/jquery-ui-1.8.16.custom.css" />
<link rel="stylesheet" href="/media/javascript/datetime/jquery-ui-timepicker-addon.css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<style>

    .ui-widget {

        font-size:0.9em;
    }

    .datetime {

        cursor: pointer; background:none !important; border:none; color:blue; text-decoration:underline;
    }
    form.pull-left {
        width: 75%;
    }

    .title {
        background: #f5f5f5;
        border: 1px solid #e3e3e3;
        border-radius: 5px;
        padding: 15px;
        margin-top: 40px;
        margin-bottom: 20px;
        color: #4c4b4b;
    }

    .content {

        border: 1px solid black;
        border-radius: 5px;
        padding: 30px;
        padding-left: 100px;
    }

    input, select, textarea {
        width: 350px;
        border-radius: 4px;
        background: #f5f5f5;
    }
    input#birth-time {
        width: 100px;
        margin-right: 40px;
    }
    input[name='birth-time-ap'] {
        width: 20px;
        margin-right: 10px;
    }
    input#name-numerology, #birth-date-numerology {
        width: 228px;
        margin-top: 5px;
    }

</style>

<div class='content_area'>

    <? /*$this->load->view('profile/badge');*/ ?>
    <div align='center' class="title">
        <h2>Email Reading Order Form</h2>
        <p style='padding:5px 0 0;'>Using this form, you will be able to place your order for an Email Reading with us.</p>
        <p style='padding:5px 0 0;'>All the payment information you enter on this form will be encrypted and can only be read by our secure & encrypted payment gateway system.</p>
        <p>Your privacy is assured.</p>
    </div>

    <div class="content">

        <form class='pull-left'>

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Your Name:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="name" value="<?=$this->member->data['first_name']?> <?=$this->member->data['last_name']?>" /></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>UserName:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="username" value="<?=$this->member->data['username']?>" /></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Street Address(1):</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="address-1" value="" /></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Street Address(2):</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="address-2" value="" /></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Town / City:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="town" value="" /></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>State / Province:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="state" value="" /></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Phone Number:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="number" value="" /></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Email Address(important!):</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="email" value="<?=$this->member->data['email']?>" /></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Gender:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><div style="display: inline-flex;">Male&nbsp;<input type="radio" name="gender" value="Male" style="display: block; width: 100px;"/>
                        Female&nbsp;<input type="radio" name="gender" value="Female" style="display: block; width: 100px;"/></div></div>
            </div>
            <br/>

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Your Date of Birth (Very Important! mm/dd/yy - overseas clients, please put the month first):</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="date-birth" id="birth-day" value="<?=date("m-d-Y", strtotime($this->member->data['dob']))?>" /></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>If ordering an Astrology Reading - Very Important! The time you were born(approx):</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><div style="display: inline-flex;"><input id="birth-time" type="text" name="time-birth"/>AM&nbsp;<input type="radio" name="birth-time-ap" value="AM" style="display: block;"/>
                        PM&nbsp;<input type="radio" name="birth-time-ap" value="PM" style="display: block;"/>
                        Unknown&nbsp;<input type="radio" name="birth-time-ap" value="UN" style="display: block;"/></div></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>If ordering an Astrology Reading - Very Important!: Your place of birth(City, State, Country):</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="place-birth" value="" /></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>If ordering a Numerology Reading Please Enter the name and/or
                        Date of Birth you wish the Reading done for (usually the name on birth certificate or the name being used now):</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <span style="margin-right: 70px;">Name:</span> <input type="text" name="name-numerology" id="name-numerology" value="" placeholder="Your Numerology Name"/><br/>
                    <span style="margin-right: 22px;">Date of Birth:</span> <input type="text" name="date-numerology" id="birth-date-numerology" />
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>In one or two words only, Please enter the topic that concerns you the most (optional)
                        (I.E.: Love, Relationship, Health, Career, $$$$, etc.):</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="topic-word" value="" /></div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>If you would like to choose your own personal Psychic, Reader, Astrologer,
                        or Numerologist from the list of our Readers, please check the button at the right of this window and type in
                        their name here: (If you leave this black, we will choose the Reader for you) * :</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <select name="reader">
                        <option selected disabled>--Please select--</option>
                    </select>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Special Instructions:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><textarea name="special-instruction"></textarea></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Please enter any additional information / questions here:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><textarea name="additional-info"></textarea></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Payment Information:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <select name="payment-info">
                        <option selected disabled>--please select--</option>
                    </select>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>First Name on Card:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="" name="f-name-card"/></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Last Name on Card:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="" name="l-name-card"/></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Billing Address:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="" name="billing-address"/></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Billing City:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="" name="billing-city"/></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Billing State:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="" name="billing-state"/></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Billing Zip code:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="" name="billing-zip-code"/></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Billing Country:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <select name="billing-country">
                        <option selected disabled>--please select--</option>
                        <?php foreach($countries as $country) { ?>
                            <option value="<?php echo$country['code']?>"><?= $country['name']?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Card Number:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="" name="card-number"/></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Card CVV Code:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6"><input type="" name="card-cvv-code"/></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"><b>Card Expiration:</b></div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <select name="card-expiration" style="margin-top: 5px;">
                        <option selected disabled>--please select--</option>
                    </select>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12"><b>* NOTICE: Any individual who chooses to submit ",Fraudulent",
                        Credit Card or Checking Account information will be prosecuted to the full extent allowable by law.
                        Please click the "SEND EMAIL READING ORDER", button below to submit your order.
                        Processing might take up to a minute or so.
                    </b></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12"><b style="color: red; font-size: 20px;">** DO NOT HIT YOUR BACK BUTTON OR THE SUBMIT BUTTON AGAIN
                        OR IT WILL DOUBLE CHARGE YOU! **
                    </b></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <input type="submit" value="SEND EMAIL READING ORDER" class="btn btn-success"/>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <button style="border-radius: 4px; font-size: 14px;" class="btn btn-primary">CLEAR ORDER FORM</button>
                </div>
            </div>
            <br/>

<!--            <div><b>Package:</b></div>-->
<!--            <div style='padding-bottom:25px;'>--><?//=$data['title']?><!-- @ $--><?//=number_format($data['price'], 2)?><!--</div>-->
<!---->
<!--            <div><b>Question(s):</b></div>-->
<!--            <div style='padding-bottom:25px;'>--><?//=nl2br($this->session->userdata('questions'))?><!--</div>-->
<!---->
<!--            <div><b>Special Instructions:</b></div>-->
<!--            <div style='padding-bottom:25px;'>--><?//=nl2br($this->session->userdata('instructions'))?><!--</div>-->
<!---->
<!--            <div><b>Additional Information:</b></div>-->
<!--            <div style='padding-bottom:25px;'>--><?//=nl2br($this->session->userdata('additional_info'))?><!--</div>-->

        </form>
<!--        <div class='pull-right' style='width:200px;text-align:center;'>-->
<!---->
<!--            <div class='well'>-->
<!--                <div style='padding-bottom:25px;'>-->
<!--                    <div><b>Order Total:</b></div>-->
<!--                    <div>$--><?//=number_format($data['price'], 2)?><!--</div>-->
<!--                </div>-->
<!---->
<!--                <div style='padding-bottom:25px;'>-->
<!--                    <div>-->
<!--                        <b>Estimated Completion:</b>-->
<!--                    </div>-->
<!--                    <div>-->
<!--                        --><?php
//
//                             $totalDays = number_format(($data['total_questions']*$this->reader->data['email_total_days']));
//
//                             if($totalDays > 3) $totalDays = 3;
//
//                             $dateOfCompletion = date("m/d/Y", strtotime("+{$totalDays} days"));
//
//                             echo $totalDays . " Days - {$dateOfCompletion}";
//                         ?>
<!--                    </div>-->
<!--                </div>-->
<!---->
<!--                <div style='padding-bottom:25px;'><a href='/profile/--><?//=$this->reader->data['username']?><!--/confirm_question' onClick="Javascript:return confirm('You are about to submit an email reading request. Your account will be charged immediately. Do you want to continue?');" class='btn btn-warning btn-large'>Confirm Order</a></div>-->
<!--                <div style='padding-bottom:25px;'><a href='/profile/--><?//=$this->reader->data['username']?><!--/submit_question' class='btn'>Edit</a></div>-->
<!--            </div>-->
<!--        </div>-->
        <div class='clearfix'></div>
    </div>
</div>

<script src='/media/javascript/jqui/jquery-ui-1.8.16.custom.min.js'></script>
<script src='/media/javascript/datetime/jquery-ui-timepicker-addon.js'></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

<script>
    $(document).ready(function()
    {
        $('#birth-day').datepicker ({
            ampm: true,
            separator: ' @ ',
            dateFormat: "mm-dd-yy"
        });

        $('#birth-date-numerology').datepicker({
            ampm: true,
            separator: ' @ ',
            dateFormat: "mm-dd-yy"
        })

        $("input[name='birth-time']").timepicker();

        $("input[name='deadline']").click(function() {

            if($(this).is(':checked')) {
                $('#div_deadline').show();
            }
            else {
                $('#div_deadline').hide();
            }
        });
    });
</script>