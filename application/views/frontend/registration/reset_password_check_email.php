
    <div class="row">

        <div class="col-md-8">

            <section class="page-form">

                <div class="page-form__header">
                    <h2>Forgot Password?</h2>
                </div>

                <div class='form-container'>

                    <div style="padding: 20px; min-height: 350px">


                        <h1>Forgot Password?</h1>

                        Please check your email account for an email from Psychic-Contact.com to allow you to reset your password.
                    </div>
                </div>

            </section>

        </div>


        <div class="col-md-4">

            <section style="margin-top: 5rem;">

                <div class="readers-block__online">

                    <div class="readers-header">
                        <span>Online Readers</span>
                    </div>

                    <div class="readers-wrapper">

                        <div id="div-online-readers">
                            <?php $this->load->view('frontend/pages/online_readers'); ?>
                        </div>

                    </div><!-- .readers-wrapper   -->

                </div><!-- .readers-block__online -->


            </section>

        </div>

    </div>