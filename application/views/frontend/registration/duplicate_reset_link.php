<div class="row">

    <div class="col-md-8">        

        <section class="page-form">

            <div class="page-form__header">
                <h2>Forgot Password?</h2>
            </div>

            <div class='form-container'>

                <div style="padding: 20px; min-height: 350px">

                    <div class="form-group">
                        You have used this link to reset your password. <a href="/register/forgot_password">You can try with another link</a>
                    </div>

                    <div class="form-group">

                    </div>

                </div>
            </div>

        </section>

    </div>


    <div class="col-md-4">    

        <section style="margin-top: 5rem;">

            <div class="readers-block__online">

                <div class="readers-header">
                    <span>Online Readers</span>
                </div>

                <div class="readers-wrapper">

                    <div id="div-online-readers">
                        <?php $this->load->view('frontend/pages/online_readers'); ?>
                    </div>

                </div><!-- .readers-wrapper   -->

            </div><!-- .readers-block__online -->

        </section>

    </div>

</div>

<script src="/theme/admin/js/jquery-3.1.1.min.js"></script>
<script>
    $(document).ready(function () {
        $('#confirmPin').click(function() {
            if($('#pinCode').val() != $('#original-pin-code').val()) {
                alert('Invalid Security Code. Please check your email.');
                return false;
            }
        })
    });
</script>