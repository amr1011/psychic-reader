<div class="row">

    <div class="col-md-8">        

        <section class="page-form">

            <div class="page-form__header">
                <h2>Forgot Password?</h2>
            </div>

            <div class='form-container'>

                <div style="padding: 20px; min-height: 350px">


                    <form action='/register/forgot_password_submit' method='POST'>

                        <div class="form-group">
                            <label for="emailAddressInput">Enter your email address below and we will email you a link to reset your password:</label>
                            <input name='email_address' type="email" class="form-control" id="emailAddressInput" placeholder="Your email address..." />
                        </div>

                        <div class="form-group">
                            <input name='submit' type="submit" class="btn btn-default btn-success" value='Reset My Password'>
                        </div>

                    </form>
                </div>
            </div>

        </section>

    </div>


   

</div>