
    <div class="row">

        <div class="col-md-8">

            <section class="page-form">

                <div class="page-form__header">
                    <h2>Forgot Password?</h2>
                </div>

                <div class='form-container'>

                    <div style="padding: 20px; min-height: 350px">


                        <h1>Reset Your Password</h1>

                        <form action='/register/reset_password_submit/<?=$id?>/<?=$pin_code?>' method='POST'>

                            <div class="form-group">
                                <label>Choose a new password:</label>
                                <input name='password1' type="password" class="form-control" value='<?=set_value('first_name')?>'>
                            </div>

                            <div class="form-group">
                                <label>Re-type new password:</label>
                                <input name='password2' type="password" class="form-control" value='<?=set_value('last_name')?>'>
                            </div>

                            <div class="form-group">
                                <input name='submit' type="submit" class="btn btn-default btn-success" value='Reset My Password'>
                            </div>

                        </form>
                    </div>
                </div>

            </section>

        </div>


        <div class="col-md-4">

            <section style="margin-top: 5rem;">

                <div class="readers-block__online">

                    <div class="readers-header">
                        <span>Online Readers</span>
                    </div>

                    <div class="readers-wrapper">

                        <div id="div-online-readers">
                            <?php $this->load->view('frontend/pages/online_readers'); ?>
                        </div>

                    </div><!-- .readers-wrapper   -->

                </div><!-- .readers-block__online -->


            </section>

        </div>

    </div>