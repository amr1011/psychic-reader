
<?php if (!empty(validation_errors())): ?>
    <div class="alert alert-warning  alert-dismissible " role="alert">
            <?php echo validation_errors(); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
    </div>
<?php endif; ?>   





<form id="form-data" name="contact-information" action="/register/submit" method="post" class="form-horizontal">
    <div class="form-group bmd-form-group">
        <label class="bmd-label-static">Email Address</label>
        <input type='text' name='email' class="form-control " required value='<?= set_value('email') ?>' >
    </div>
    <div class="form-group bmd-form-group">
        <label class="bmd-label-static">Username</label>
        <input type='text' name='username' class="form-control" required  value='<?= set_value('username') ?>' >
    </div>

    <div class="form-group bmd-form-group">
        <label class="bmd-label-static">Password</label>
        <input type="password" class="form-control" name="password" required id="password" value='<?= set_value('password') ?>'>
    </div>

    <div class="form-group bmd-form-group">
        <label class="bmd-label-static">Confirm Password</label>
        <input type="password" class="form-control" name="password2" required id="password2" value='<?= set_value('password2') ?>'>
    </div>        
    <div class="form-group bmd-form-group">
        <label class="bmd-label-static">Firstname</label>
        <input type='text' name='first_name' class="form-control" required value='<?= set_value('first_name') ?>' >
    </div>        
    <div class="form-group bmd-form-group">
        <label class="bmd-label-static">Lastname</label>
        <input type='text' name='last_name' class="form-control" required value='<?= set_value('last_name') ?>' >
    </div>        

    <label class="bmd-label-static">Gender</label>
    <div class="form-check form-check-radio form-check-inline">
      <label class="form-check-label">
        <input class="form-check-input" type="radio" name="gender" id="inlineRadio1" value="sex-female"> Female
        <span class="circle">
            <span class="check"></span>
        </span>
      </label>
    </div>
    <div class="form-check form-check-radio form-check-inline">
      <label class="form-check-label">
        <input class="form-check-input" type="radio" name="gender" id="inlineRadio2" value="sex-male"> Male
        <span class="circle">
            <span class="check"></span>
        </span>
      </label>
    </div>
    <div class="form-check form-check-radio form-check-inline ">
      <label class="form-check-label">
        <input class="form-check-input" type="radio" name="gender" id="inlineRadio3" value="sex-other"> Other
        <span class="circle">
            <span class="check"></span>
        </span>
      </label>
    </div>


    <div class="form-group bmd-form-group">
        <label class="bmd-label-static">Date of Birth</label>

    </div>
    <div class="form-row">
    <div class="form-group col-md-3">        
      <label for="inputCity">Month</label>
         <select name="dob_month" class="form-control" required >
                <option value="" selected disabled>Month </option>
                <option value="1">January</option>
                <option value="2">February</option>
                <option value="3">March</option>
                <option value="4">April</option>
                <option value="5">May</option>
                <option value="6">June</option>
                <option value="7">July</option>
                <option value="8">August</option>
                <option value="9">September</option>
                <option value="10">October</option>
                <option value="11">November</option>
                <option value="12">December</option>
            </select>      
    </div>
    <div class="form-group col-md-2">
      <label for="inputState">Day</label>
        <select name="dob_day" class="form-control" required style="width: 60px;">
            <option value="" selected disabled> Day </option>
            <?php echo $days; ?>
        </select>      
    </div>
    <div class="form-group col-md-2">
      <label for="inputZip">Year</label>
            <select name="dob_year" class="form-control" required style="width: 90px;">
                <option value="" selected disabled>Year</option>
                <?php echo $year; ?>
            </select>
    </div>
  </div>

     <div class="form-group bmd-form-group">
        <label class="bmd-label-static">Country</label>
         <select name="country" class="form-control" required >
                <option value="" selected disabled>Select Country</option>
                <?php foreach($countries as $country) {?>
                    <option value="<?php echo($country['code']);?>"><?php echo($country['name']);?></option>
                <?php } ?>
            </select>    
    </div>        


    <div class="form-check form-check-inline">
      <label class="form-check-label">
        <input type='checkbox'   class="form-check-input"  name='newsletter' value='' <?php echo set_checkbox('newsletter', '1', TRUE); ?> > Newsletter
        <span class="form-check-sign">
            <span class="check"></span>
        </span>
      </label>
    </div>    


    <div class="form-check form-check-inline">
      <label class="form-check-label">
        <input type='checkbox'   class="form-check-input"  name='terms' value='1' <?php echo set_checkbox('terms', '1', TRUE); ?>' > Terms and Privacy Policy
        <span class="form-check-sign">
            <span class="check"></span>
        </span>
      </label>
    </div>    

    <p>I have read and agreed to all the Member <a href='/terms' target='_blank'>Terms and Conditions</a></p>
    
    <div class="form-group bmd-form-group">
        <label class="col-sm-4 control-label"></label>
                <div class="col-sm-6">      
                    <div class="g-recaptcha" data-sitekey="6LfgyacUAAAAAC2sk_Uf8oVs3MlPhuGbPYZnkNX8"></div>
                </div>
    </div>


      <button type="submit" class="btn btn-primary">Register</button>
</form>
<script src='https://www.google.com/recaptcha/api.js'></script>
