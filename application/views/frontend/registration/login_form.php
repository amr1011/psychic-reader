
 

    



        <form method="post" action="/register/login_submit" id="loginform">
            <div class="card card-login card-hidden">
              <div class="card-header card-header-primary text-center">
                <h4 class="card-title">Login</h4>
                <div class="social-line">
                  <a href="#pablo" class="btn btn-just-icon btn-link btn-white">
                    <i class="fa fa-facebook-square"></i>
                  </a>
                  <a href="#pablo" class="btn btn-just-icon btn-link btn-white">
                    <i class="fa fa-twitter"></i>
                  </a>
                </div>
              </div>
              <div class="card-body ">
                <?php if (!empty($error)): ?>
                <p class="text-danger text-center">
                    <?php echo $error; ?>
                </p>                
                <?php endif; ?>   
                <?php if (!empty($notification)): ?>
                  <p class="text-success text-center"><?php echo $notification; ?></p>
                <?php endif; ?>   
                <span class="bmd-form-group">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">face</i>
                      </span>
                    </div>
                    <input type="text" class="form-control" placeholder="Username" name="username" id="username" value='<?=set_value('username')?>'>


                  </div>
                </span>
                </span>
                <span class="bmd-form-group">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">lock_outline</i>
                      </span>
                    </div>
                            <input type="password" class="form-control" placeholder="Password"  name="password" required id="password" value='<?= set_value('password') ?>'>
                  </div>
                </span>
              </div>
              <div class="card-footer justify-content-center">
                            <input type="submit" name="submit" value='Submit' class='btn btn-primary'>                      
              </div>
            <span class="bmd-form-group">

                            <ul>
                                <li>Need an account?<a href="/register/"> Register Here</a></li>
                                <li>Forget your password? <a href="/register/forgot_password">Click here</a></li>
                                <li>Having trouble in signing-in?    <a href="/contact">Contact Us</a> </li>
                            </ul>
                        </div>

            </span>
          </form>


