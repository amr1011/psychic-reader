
	<style>
	
		.large_anchor{ color:#489006 !important; font-size:22px !important; text-decoration:none !important; }
	
		#faq_list{ margin:0; padding:0; }
		#faq_list li{ list-style:none; margin-bottom:25px; }
		#faq_list li a{ display:block; color:#0037ff !important; font-size:14px !important; text-decoration:none !important; background-image:url(/media/images/plus.png); padding-left:20px; background-repeat: no-repeat; background-position:0 1px; }
		#faq_list li a.open{ background-image:url(/media/images/minus.png); }
		#faq_list li .answer{ display:none; padding:10px 0; padding-left: 25px;}
		
		#faq_list li{ width: 90%; background-color: #ec9d0b24; border: 1px solid #001fff; border-radius: 4px; }

		.card {margin-bottom:10px;background-color: #ec9d0b24;}
		.card-header {padding:8px;font-size:16px}
		.btn-link:hover, .btn-link:active, .btn-link:focus {text-decoration:none;}
		.btn-link:active, .btn-link:focus, .btn-link:hover{outline:none !important;border:1px solid #b0b0b0;}
		.btn-link{color:#0037ff;width:100%;text-align:left;border:1px solid #b0b0b0;}
		.collapse-inner{background-color:#fff;padding:10px 30px;border:1px solid #b0b0b0; }
		.ac-con{width:90%;}
		.card-header p{display:inline-block;}
	</style>

<section class="page-form">
    <div class="page-form__header">
        <h2>FAQs</h2>
    </div>

    <div style='padding:20px;'>
	<div class='content_area'>
		<!-- david_v4 -->
		<div class="col-md-6 col-sm-6">
		
			<h3>Client FAQs</h3>
			<div>&nbsp;</div>
			<div class="accordion ac-con" >

			<?php
			$count=1;
			foreach($clients as $q)
			{
			?>
			<div class="card">
				<button class="btn btn-link collapsed" type="button" onclick="check_fai('fai<?php echo $count;?>')" data-toggle="collapse" data-target="#collapse<?php echo $count;?>" aria-expanded="false" aria-controls="collapse<?php echo $count;?>">
				<div class="card-header">
					<span class="fai<?php echo $count;?>"><i class="fa fa-plus"></i></span>&nbsp;<?php echo $q['question'];?>
				</div>
				</button>
				<div id="collapse<?php echo $count;?>" class="collapse collapse-inner" aria-labelledby="headingTwo" data-parent="#accordionExample">
					<div class="card-body">
						<?php echo $q['answer'];?>
					</div>
				</div>
			</div>

			<?php
			$count++;
			} 
			?>
			</div>
		
		</div>
		
		<div class="col-md-6 col-sm-6">
		
			<h3>Readers FAQs</h3>
			<div>&nbsp;</div>

			<?php
			$count=1;
			foreach($readers as $q)
			{
			?>
			<div class="card">
				<button class="btn btn-link collapsed" type="button" onclick="check_fai('fais<?php echo $count;?>')" data-toggle="collapse" data-target="#collapses<?php echo $count;?>" aria-expanded="false" aria-controls="collapses<?php echo $count;?>">
				<div class="card-header">
					<span class="fais<?php echo $count;?>"><i class="fa fa-plus"></i></span>&nbsp;<?php echo $q['question'];?>
				</div>
				</button>
				<div id="collapses<?php echo $count;?>" class="collapse collapse-inner" aria-labelledby="headingTwo" data-parent="#accordionExample">
					<div class="card-body">
						<?php echo $q['answer'];?>
					</div>
				</div>
			</div>

			<?php
			$count++;
			} 
			?>
		</div>
		
		<div class='clear'></div>
		
	</div>
    </div>
</section>

<script>
function check_fai(params){
	var classs = $( "." + params ).find( "i" ).attr('class');
	console.log(params);
	if(classs == 'fa fa-plus'){
		$( "." + params ).find( "i" ).attr('class' , 'fa fa-minus');
	}else{
		$( "." + params ).find( "i" ).attr('class' , 'fa fa-plus');
	}

}
</script>





	