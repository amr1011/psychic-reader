<section class="page-form">
    <div class="page-form__header">
        <h2>Blogs</h2>
    </div>

    <div style='padding:20px;'>
        <?php foreach ($posts as $b) : ?>

            <?php $total_comments = $this->blog_model->totalComments($b['id']); ?>

            <div class='blog_entry'>
                <div class='col-md-4 col-sm-4'>
                    <?php echo ($b['image'] ? "<img src='/media/assets/" . $b['image'] . "' width='250' class=\"img-responsive img-polaroid\" />" : ""); ?>
                </div>
                <div class='col-md-8 col-sm-8'>
                    <div class='title'><a href='/blog/<?php echo $b['url'] ?>'><?php echo $b['title']; ?></a></div>
                    <div class='blog_caption'>Written By: <?php echo ($b['username'])?> on <?php echo date("F d, Y", strtotime($b['date'])); ?></div>
                    <div class='description'><?php echo html_entity_decode($b['content'])  ?></div>
                    <div class='read_more'><span class='icon-chevron-right'></span> <a href='/blog/<?php echo $b['url']; ?>'>Read More</a><?php echo ($total_comments > 0 ? " &nbsp; &nbsp; - {$total_comments} Comment(s)" : "") ?></div>
                </div>
                <div class='clearfix'></div>
                <hr />

            </div>
        <?php endforeach; ?>
    </div>
</section>




<div align='center'>
    <?php
    if (isset($pagination))
    {

        echo "<div align='center'>{$pagination}</div>";
    } else
    {

        //echo "<a href='/blog/archive' class='btn'>Archived Blog Entries</a>";
    }
    ?>
</div>