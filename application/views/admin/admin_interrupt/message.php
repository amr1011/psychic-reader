<style>
    #message-box {
        width: 100%;
        height: 311px;
        border: #0a568c 1px solid;
        border-radius: 4px;
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        resize: none;
        background-image: -webkit-linear-gradient(left, white 10px, transparent 10px), -webkit-linear-gradient(right, white 10px, transparent 10px), -webkit-linear-gradient(white 30px, #ffffff 30px, #3c3c3c 31px, white 31px);
        background-image: -moz-linear-gradient(left, white 10px, transparent 10px), -moz-linear-gradient(right, white 10px, transparent 10px), -moz-linear-gradient(white 30px, #ccc 30px, #ccc 31px, white 31px);
        background-image: -ms-linear-gradient(left, white 10px, transparent 10px), -ms-linear-gradient(right, white 10px, transparent 10px), -ms-linear-gradient(white 30px, #ccc 30px, #ccc 31px, white 31px);
        background-image: -o-linear-gradient(left, white 10px, transparent 10px), -o-linear-gradient(right, white 10px, transparent 10px), -o-linear-gradient(white 30px, #ccc 30px, #ccc 31px, white 31px);
        background-image: linear-gradient(left, white 10px, transparent 10px), linear-gradient(right, white 10px, transparent 10px), linear-gradient(white 30px, #ccc 30px, #ccc 31px, white 31px);
        background-size: 100% 100%, 100% 100%, 100% 31px;
        border-radius: 8px;
        box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
        line-height: 31px;
        font-family: Arial, Helvetica, Sans-serif;
        color: black;
        font-size: 22px;
        padding: 0 15px 0 15px;
    }

    #send {
        margin-left: 58%;
        margin-top: 20px;
    }

</style>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <legend>Message Send</legend>
                </div>
                <div class="ibox-content">
                    <div class="row" style="margin-left: 36%; color: red">
                        <?php if ($interrupt_type == 1) { ?><h2>Reader Only</h2><?php } ?>
                        <?php if ($interrupt_type == 2) { ?><h2>Client Only</h2><?php } ?>
                        <?php if ($interrupt_type == 3) { ?><h2>Reader and Client</h2><?php } ?>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <h2>Message Box</h2>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <textarea id="message-box"></textarea>
                        </div>
                    </div>
                    <button id="back" class="btn btn-danger btn-lg">Back</button>
                    <button id="send" class="btn btn-primary btn-lg">Send</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/theme/admin/js/jquery-3.1.1.min.js"></script>
<script src="/theme/admin/js/notify.min.js"></script>
<!--$.notify("Successfully Cancelled the Selected Clients",  {className: "info", hideDuration: 700, autoHideDelay: 60000, position: 'right buttom'});-->
<script>
    $(document).ready(function () {
        /*9-23*/
        //setInterval(function () {
        //    $.ajax({
        //        'type': "POST",
        //        'url': "/admin/main/get_interrupt_reply_message",
        //        'data': {
        //            chat_session_id: '<?//=$chat_session_id;?>//',
        //        },
        //        success: function (data) {
        //            var obj = JSON.parse(data);
        //            var messages = obj.messages;
        //            for (var i = 0; i < messages.length; i++) {
        //
        //                if ('<?//=$interrupt_type?>//' == 1 && messages[i]['member_type'] == 'READER') {
        //
        //                    var text = messages[i]['username'] + " : " + messages[i]['reply_text'];
        //
        //                    $.notify(text, {
        //                        className: "success",
        //                        hideDuration: 700,
        //                        autoHideDelay: 60000,
        //                        position: 'right buttom'
        //                    });
        //                    $.ajax({
        //                        'type': "POST",
        //                        'url': "/admin/main/admin_check_reply_message",
        //                        'data': {
        //                            id: messages[i]['id']
        //                        },
        //                        success: function (data) {
        //
        //                        }
        //                    });
        //                }
        //                if ('<?//=$interrupt_type?>//' == 2 && messages[i]['member_type'] != 'READER') {
        //
        //                    var text = messages[i]['username'] + " : " + messages[i]['reply_text'];
        //
        //                    $.notify(text, {
        //                        className: "info",
        //                        hideDuration: 700,
        //                        autoHideDelay: 60000,
        //                        position: 'right buttom'
        //                    });
        //                    $.ajax({
        //                        'type': "POST",
        //                        'url': "/admin/main/admin_check_reply_message",
        //                        'data': {
        //                            id: messages[i]['id']
        //                        },
        //                        success: function (data) {
        //
        //                        }
        //                    });
        //                }
        //                if ('<?//=$interrupt_type?>//' == 3) {
        //
        //                    var text = messages[i]['username'] + " : " + messages[i]['reply_text'];
        //
        //                    if (messages[i]['member_type'] == 'READER') {
        //
        //                        var message_type = "success";
        //
        //                    } else {
        //
        //                        var message_type = "info";
        //                    }
        //
        //                    $.notify(text, {
        //                        className: message_type,
        //                        hideDuration: 700,
        //                        autoHideDelay: 60000,
        //                        position: 'right buttom'
        //                    });
        //                    $.ajax({
        //                        'type': "POST",
        //                        'url': "/admin/main/admin_check_reply_message",
        //                        'data': {
        //                            id: messages[i]['id']
        //                        },
        //                        success: function (data) {
        //
        //                        }
        //                    });
        //                }
        //            }
        //        }
        //    });
        //}, 2000);

        $("#send").on('click', function () {

            $.ajax({
                'type': "POST",
                'url': "/admin/main/save_admin_interrupt_message",
                'data': {
                    chat_session_id: '<?=$chat_session_id;?>',
                    interrupt_type: '<?=$interrupt_type?>',
                    message: $("#message-box").val()
                },
                success: function (data) {
                    $("#message-box").val('');
                }
            });
        });

        $("#back").on('click', function () {

            history.go(-1);
        })
    });
</script>