<style>
    th, td {
        text-align: center;
        vertical-align: middle !important;
    }
</style>

<!--9-18-->
<script src="<?= CHAT_URL ?>:<?= CHAT_PORT ?>/auth.js"></script>
<script src="<?= CHAT_URL ?>:<?= CHAT_PORT ?>/socket.io/socket.io.js"></script>

<script src="/chat/app/chat_all_lobby.js?ts=<?= time(); ?>"></script>
<script src='/media/javascript/ion.sound.min.js'></script>
<!--9-18-->

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h2>List of Ongoing Chats</h2>
                </div>
                <div class="ibox-content">
                    <?php if($chats != null) {?>
                        <table class="table table-striped table-bordered table-hove">
                            <tr>
                                <th>ChatId</th>
                                <th>Reader</th>
                                <th>Client</th>
                                <th>Topic</th>
                                <th>Start Time</th>
                                <th>Set Length</th>
                                <th>Actions</th>
                            </tr>
                            <?php foreach ($chats as $chat) { ?>
                                <tr>
                                    <td><?php echo $chat['id']; ?></td>
                                    <td><?php echo $chat['reader_username']; ?></td>
                                    <td><?php echo $chat['client_username']; ?></td>
                                    <td><?php echo $chat['topic'] ?></td>
                                    <td><?php echo $chat['start_datetime'] ?></td>
                                    <td><?php echo $chat['set_length'] ?></td>
                                    <td>
                                        <a href="/admin/main/admin_interrupt_message/<?php echo $chat["chat_session_id"]; ?>/1"
                                           class="btn btn-primary">Reader</a>
                                        <a href="/admin/main/admin_interrupt_message/<?php echo $chat["chat_session_id"]; ?>/2"
                                           class="btn btn-warning">Client</a>
                                        <a href="/admin/main/admin_interrupt_message/<?php echo $chat["chat_session_id"]; ?>/3"
                                           class="btn btn-success">Reader & Client</a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    <?php } else {?>
                        <h3 style="color: red;">There is not ongoing chat now.</h3>
                    <?php }?>
                </div>
            </div>
        </div>
    </div>
</div>
