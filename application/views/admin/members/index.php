
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= (!$subnav ? $nav['title'] : $nav['title'] . " > " . $subnav['title']) ?></h5> 
                </div>
                <div class="ibox-content">
                    <?php if ($subnav["allow_add"]): ?>
                        <div class="ibox-content m-b-sm">
                            <div class="row">
                                <a href='<?php echo $add_link; ?>' class="btn btn-w-m btn-warning">
                                    <i class="fa fa-plus"> </i> 
                                    Add <?php echo singular($subnav['title']) ?>
                                </a>

                                <?php if ($this->uri->segment('5') == '21'): ?>
                                    <a href='/admin/pages/purge_old' onClick="Javascript:return confirm('Are you sure you want to delete ALL old members?');" class="btn btn-w-m  btn-danger">Purge ALL Old Members</a>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <div class="table-responsive">
                        <?php echo ($pagination ? "<span class='pagination'>{$pagination}</span>" : "") ?>

                        <?php if (!$data): ?>
                            <p style='padding:10px;'>There is no data to display in <?php echo (!$subnav ? $nav['title'] : $subnav['title']) ?> </p>
                        <?php else: ?>
                            <table class="table table-striped table-bordered table-hover  data-table" >
                                <thead>
                                    <tr>
                                        <?php foreach ($fields as $f): ?>
                                            <th style="text-align: center"><strong><?php echo strtoupper(str_replace("_", " ", $f)) ?></strong></th>
                                        <?php endforeach; ?>
                                        <?php if (!empty($nav['id']) && $nav['id'] == '13'): ?>
                                            <th>&nbsp;</th>                                
                                        <?php endif; ?>
                                        <?php if (!empty($transactionType) && $transactionType == 'pending') : ?>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                        <?php endif; ?>
                                        <th style="text-align: center; width: 25px"><strong>ACTION</strong></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($data as $table): ?>
                                        <tr class='special_td'>
                                            <?php foreach ($fields as $field): ?>
                                                <td class='std'><?php echo parseFields($table, $field, $specs); ?></td>                                        
                                            <?php endforeach; ?>
                                            <?php if (!empty($nav['id']) && $nav['id'] == '13'): ?>
                                                <td class='std'><?php echo getTransactionLevel($table, $nav); ?></td>
                                            <?php endif; ?>
                                            <?php if (!empty($transactionType) && $transactionType == 'pending') : ?>
                                                <td class='std' align='center' width='25'><a href='/admin/main/finalize_transaction/void/<?php echo $table['id'] ?>' class='btn btn-inverse btn-small' onClick="Javascript:return confirm('Are you sure you want to manually void this transaction?');">Void</a></td>
                                                <td class='std' align='center' width='25'><a href='/admin/main/finalize_transaction/settle/<?php echo $d['id'] ?>' class='btn btn-info btn-small' onClick="Javascript:return confirm('Are you sure you want to manually settle this transaction?');">Settle</a></td>                                            
                                            <?php endif; ?>
                                            <td style="text-align: center; white-space: nowrap">
                                                <a href='<?php echo $edit_link[$table["id"]]; ?>' class='btn btn-info btn-xs'> <span class="fa fa-pencil"></span> Edit</a>
                                                <a href='/admin/pages/delete/<?php echo $nav['id'] ?>/<?php echo ($subnav['id'] ? $subnav['id'] : '0') ?>/<?php echo $table['id']; ?>' class="btn btn-danger btn-xs" onClick="Javascript:return confirm('Are you sure you want to delete this record?');"><span class="fa fa-trash"></span> Delete</a>
                                                <?php if (in_array($subnav['id'], [19, 20])): ?>
                                                    <?php if (isBanned($table['id'])): ?>
                                                        <a href='/admin/pages/ban_member/<?php echo $nav['id'] ?>/<?php echo ($subnav['id'] ? $subnav['id'] : '0') ?>/<?php echo $table['id']; ?>/0' class='btn btn-danger btn-xs' style="width: 90px"> <span class="fa fa-warning"></span> Un-Banned</a>
                                                    <?php else: ?>
                                                        <a href='/admin/pages/ban_member/<?php echo $nav['id'] ?>/<?php echo ($subnav['id'] ? $subnav['id'] : '0') ?>/<?php echo $table['id']; ?>/1' class='btn btn-danger btn-xs' style="width: 90px"> <span class="fa fa-warning"></span> Banned</a>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        <?php endif; ?>
                        <?php echo ($pagination ? "<span class='pagination'>{$pagination}</span>" : "") ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>