<div style="padding:20px;background:#FFF;margin:20px;">

    <div class="btn-group pull-right">
        <a href="#newProfileModal" class="btn btn-primary" data-toggle="modal">New Reader</a>
        <a href="#downloadTransactionsModal" class="btn btn-default downloadAllTransactions" data-toggle="modal">Download All Transactions</a>
    </div>

    <legend>Reader Management</legend>

    <div class="clearfix" style="margin:15px 0;"></div>

    <table class="table table-bordered table-striped table-hover">

        <thead>

            <tr>
                <th>Username:</th>
                <th style="text-align: center;">US Balance:</th>
                <th style="text-align: center;">CAN Balance:</th>
                <th style="text-align:center;">Featured:</th>
                <th colSpan="2">&nbsp;</th>
            </tr>

        </thead>

        <tbody>

            <?php foreach ($readers as $reader): ?>

                <?php $this->reader->init($reader['id']); ?>

                <!-- US Balance -->
                <?php $us_balance = $this->reader->get_balance('us'); ?>
                <?php $USFormattedBalance = number_format($us_balance, 2); ?>

                <!-- CAN Balance -->
                <?php $ca_balance = $this->reader->get_balance('ca'); ?>
                <?php $CAFormattedBalance = number_format($ca_balance, 2); ?>

                <!-- Is Featured -->
                <?php $featuredClassImage = ($reader['featured'] ? "icon-star icon-white" : "icon-star-empty"); ?>
                <?php $featuredClassBTN = ($reader['featured'] ? "btn btn-primary" : "btn"); ?>

                <tr>
                    <td><a href="/admin/pages/edit/17/0/<?= $reader['id'] ?>"><?= $reader['username'] ?></a></td>
                    <td style="text-align: center;">$ <?= $USFormattedBalance ?></td>
                    <td style="text-align: center;">$ <?= $CAFormattedBalance ?></td>
                    <td style="text-align:center;"><a href="/admin/reader_management/toggleFeatured/<?= $reader['id'] ?>" class="<?= $featuredClassBTN ?>"><span class="<?= $featuredClassImage ?>"></span></a></td>
                    <td style="width:185px; text-align: center;"><a href='#downloadTransactionsModal' class="downloadPaymentsAnchor btn"  data-readerid="<?= $reader['id'] ?>" data-toggle="modal">Download Transactions</a></td>
                    <td style="width:175px; text-align: center;"><a href='#enterPaymentModal' class="newPaymentAnchor btn" data-toggle="modal" data-readerpaypal="<?= ($reader['paypal_email'] ? $reader['paypal_email'] : "") ?>" data-ustotal="<?= $USFormattedBalance ?>" data-catotal="<?= $CAFormattedBalance ?>" data-readerid="<?= $reader['id'] ?>">Enter New Payment</a></td>
                </tr>

            <?php endforeach; ?>

        </tbody>

    </table>

</div>

<div class="modal fade" id="newProfileModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="/admin/reader_management/new_reader" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Create New Reader</h4>
                </div>
                <div class="modal-body">
                    <p>To create a new reader, the reader must have a valid member account (username & password). Enter the member id of the user and click save. The user will now be able to login as a reader and update their profile accordingly.</p>

                    <table cellPadding="5">

                        <tr>
                            <td width="150"><b>Member ID:</b></td>
                            <td><input type="text" name="memberid" value="" class="form-control" /></td>
                        </tr>

                        <tr>
                            <td width="150">&nbsp;</td>
                            <td><input type="checkbox" name="legacy" value="1" /> Set as legacy member</td>
                        </tr>

                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save New Reader</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="enterPaymentModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form  action="/admin/reader_management/new_payment" method="post">
                <input type="hidden" name="readerid" value="" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Enter New Payment</h4>
                </div>
                <div class="modal-body">
                    <table class="table">

                        <tr>
                            <td><b>Region:</b></td>
                            <td>
                                <select name="region" class="form-control">
                                    <option value="us">US</option>
                                    <option value="ca">CA</option>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td><b>Amount:</b></td>
                            <td>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input name="amount" class="span2 form-control" id="prependedInput" type="text" />
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td><b>Notes:</b></td>
                            <td><textarea style="height:150px;" name="notes" class="form-control"></textarea></td>
                        </tr>

                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    <input type="submit" name="paypal" value="Pay with paypal" class="btn btn-success paypalAnchor" />
                    <input type="submit" name="submit" value="Save Payment" class="btn btn-primary" />
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="downloadTransactionsModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form  action="/admin/reader_management/download_transactions" method="post">
                <input type="hidden" name="readerid" value="" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Download Transactions</h4>
                </div>
                <div class="modal-body">
                    <table class="table">

                        <tr>
                            <td width="150"><b>Reader ID:</b></td>
                            <td><input type="text" name="readerid" value="" class="form-control" /></td>
                        </tr>

                        <tr>
                            <td><b>From:</b></td>
                            <td><input type="text" name="from" class="form-control datetime" value="<?= date("m/d/Y h:i A", strtotime("-7 days")) ?>" /></td>
                        </tr>

                        <tr>
                            <td><b>To:</b></td>
                            <td><input type="text" name="to" class="form-control datetime" value="<?= date("m/d/Y h:i A") ?>" /></td>
                        </tr>

                        <tr>
                            <td><b>Region:</b></td>
                            <td>
                                <select name="region" class="form-control">
                                    <option value="">All</option>
                                    <option value="us">US</option>
                                    <option value="ca">CA</option>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td><b>Service:</b></td>
                            <td>
                                <select name="service" class="form-control">
                                    <option value="">All</option>
                                    <option value="chat">Chat</option>
                                    <option value="email">Email</option>
                                </select>
                            </td>
                        </tr>

                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" name="submit" value="Download Transactions" class="btn btn-primary" />
                </div>
            </form>
        </div>
    </div>
</div>

