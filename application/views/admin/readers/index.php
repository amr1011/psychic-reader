
<form action="/admin/pages/submitreaders/12/58" method="POST" enctype="multipart/form-data">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= (!$subnav ? $nav['title'] : $nav['title'] . " > " . $subnav['title']) ?></h5> 
                </div>
                
                <div class="ibox-content">
                <?php if($this->session->flashdata('response')):?>
                    <p class='record' style="display: none;"> <?=$this->session->flashdata('response')?> </p>
                <?php endif?>

                   <div class="table-responsive">
                        <?php echo ($pagination ? "<span class='pagination'>{$pagination}</span>" : "") ?>

                        <?php if (!$data): ?>
                            <p style='padding:10px;'>There is no data to display in <?php echo (!$subnav ? $nav['title'] : $subnav['title']) ?> </p>
                        <?php else: ?>
                            <!-- pages tab table -->
                            <table class="table table-striped table-bordered table-hover  common-data-table" >
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>ID</th>
                                    <th>Reader</th>
                                    <th>Status</th>
                                    <th>1 Question</th>
                                    <th>2 Questions</th>
                                    <th>3 Questions</th>
                                    <th>4 Questions</th>
                                    <th>5 Questions</th>
                                    <th>Special</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $count = 0;
                                foreach ($data as $table): ?>
                                    <tr class='special_td tr_<?php echo $count;?>'>
                                        <td width="2%">
                                            <input class="r_chk" type="checkbox" <?php if($table['is_active'] == 1){?> checked <?php } ?>>
                                            <input type="hidden" class="is_active" value="<?php echo $table['is_active'];?>" name="is_active[]">
                                            <input type="hidden" value="<?php echo $table['id'];?>" name="id[]">
                                        </td>
                                        <td class='std'><?php echo $table['id'];?></td>
                                        <td class='std'><?php echo $table['username'];?></td>
                                        <td width="5%" class='std'>
                                            <span class="is_active_span">
                                            <?php if($table['is_active'] == 1){?>
                                                Active
                                            <?php } else {
                                                echo 'Inactive';
                                            }?>
                                            </span>
                                        </td>                                 
                                        <td class='std'>
                                            <input type="text" name="first_q[]" value="<?php echo $table['first_q'];?>" class="form-control num" autocomplete="off">
                                        </td>
                                        <td class='std'>
                                            <input type="text" name="second_q[]" value="<?php echo $table['second_q'];?>" class="form-control num" autocomplete="off">
                                        </td>
                                        <td class='std'>
                                            <input type="text" name="third_q[]" value="<?php echo $table['third_q'];?>" class="form-control num" autocomplete="off">
                                        </td>
                                        <td class='std'>
                                            <input type="text" name="fourth_q[]" value="<?php echo $table['fourth_q'];?>" class="form-control num" autocomplete="off">
                                        </td>
                                        <td class='std'>
                                            <input type="text" name="fifth_q[]" value="<?php echo $table['fifth_q'];?>" class="form-control num" autocomplete="off">
                                        </td>
                                        <td class='std'>
                                            <input type="text" name="special[]" value="<?php echo $table['special'];?>" class="form-control num" autocomplete="off">

                                        </td>
                                    </tr>
                                <?php 
                                $count++;
                                endforeach; ?>
                            </tbody>
                            </table>
                            
                            <input type='submit' value='Save changes' class='btn btn-primary'/>
                            
                        <?php endif; ?>
                        <?php echo ($pagination ? "<span class='pagination'>{$pagination}</span>" : "") ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<script>
    $(document).ready(function () {
        $('.num').keypress(function(event) {

            if((event.which >= 48 && event.which <= 57) ||  (event.which == 46 && this.value.indexOf(".") == -1 )){
                if (event.which == 46 && this.value.indexOf(".") > -1){
                event.preventDefault();
                }
                if (event.which == 101){
                    event.preventDefault();
                }
                if(this.value.indexOf(".")>-1 && (this.value.split('.')[1].length > 1)){
                    //alert('Two numbers only allowed after decimal point');
                    event.preventDefault();
                }
            }
            else{
                event.preventDefault();
            }

            
        });


        if($('p.record').html()) {
            $.notify($('p.record').html(), {className:"success",align:"center", verticalAlign:"top", hideDuration: 700, autoHideDelay: 5000,});
        }

        
        $('.r_chk').each(function () {
            var is_active = $(this).next("input[class=is_active]").val();
            var parent = $(this).parent();
            //var readonly = 

            if(is_active == 1){
                $(parent).siblings().find('input').attr('readonly', false);
            }else{
                $(parent).siblings().find('input').attr('readonly', true);
            }

        });

        $('input[type="checkbox"]').click(function(){
            var parent = $(this).parent();
            if($(this).prop("checked") == true){ 
                $(parent).siblings().find('input').attr('readonly', false);
                $(this).next("input[class=is_active]").val('1');
                $(parent).siblings().find('span.is_active_span').text('Active');
            }
            else if($(this).prop("checked") == false){
                $(parent).siblings().find('input').attr('readonly', true);
                $(this).next("input[class=is_active]").val('0');
                $(parent).siblings().find('span.is_active_span').text('Inactive');
            }
        });

        var common_table = $('.common-data-table').DataTable({
            "columnDefs": [
                { "orderable": false, "targets": 0 },
                { "orderable": true, "targets": 1 },
                { "orderable": true, "targets": 2 },
                { "orderable": true, "targets": 3 },
                { "orderable": false, "targets": 4 },
                { "orderable": false, "targets": 5 },
                { "orderable": false, "targets": 6 },
                { "orderable": false, "targets": 7 },
                { "orderable": false, "targets": 8 },
                { "orderable": false, "targets": 9 }
                
            ],
            "order": [[ 2, "asc" ]]
        });
    });


    
    
</script>