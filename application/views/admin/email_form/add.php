<style>
    .item-block {
        display: inline-flex;
        margin-bottom: 10px;
    }

    .item-title {
        height: 35px;
        margin-right: 10px;
        border: 1px solid #e5e6e7;
        border-radius: 1px;
        color: red;
        font-family: Monospace
    }

    .remove-btn {
        height: 30px;
        margin-left: 10px;
    }
</style>
<link rel="stylesheet" type="text/css"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css">

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add New Form</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <form action='/admin/email_form/add_new_form' method='POST' enctype="multipart/form-data">
                            <div class="box1 col-md-12">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td>Page</td>
                                            <td style="white-space: nowrap">
                                                <select class="form-control" name="page[]" id="select-page"
                                                        multiple="multiple" required>
                                                    <option value="price">price</option>
                                                    <option value="article">article</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Name</td>
                                            <td style="white-space: nowrap">
                                               <input type="text" class="form-control" name="name-page" id="name-page"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Content</td>
                                            <td style="white-space: nowrap">
                                                <div class="form-control row"
                                                     style="width: 100%; height: auto; min-height: 300px; margin: 0px;">
                                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                                        <a class="btn btn-success" id="add-input"><i class="fa fa-header"></i> Add Input</a>
                                                        <a class="btn btn-success" id="add-text"><i class="fa fa-text-width"></i> Add TextArea</a>
                                                        <a class="btn btn-success" id="add-list"><i class="fa fa-list"></i> Add List</a>
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-sm-8 content-preview"
                                                         style="display: inline-grid;">
                                                        <div class="item-block"><input class="item-title"
                                                                                       placeholder="Full Name" disabled><input
                                                                    class="form-control" disabled/></div>
                                                        <div class="item-block"><input class="item-title"
                                                                                       placeholder="Email"
                                                                                       disabled><input
                                                                    class="form-control" disabled/></div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Header</td>
                                            <td style="white-space: nowrap">
                                                <span>Yes</span> <input name="header" type="radio" value="1" required/>
                                                <span>No</span> <input name="header" type="radio" value="0" required/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Footer</td>
                                            <td style="white-space: nowrap">
                                                <span>Yes</span> <input name="footer" type="radio" value="1" required/>
                                                <span>No</span> <input name="footer" type="radio" value="0" required/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Popup</td>
                                            <td style="white-space: nowrap">
                                                <span>No </span> <input name="popup" type="radio" value="-1" required/>&nbsp;&nbsp;
                                                <span>Top Left</span> <input name="popup" type="radio" value="0" required/>&nbsp;&nbsp;
                                                <span>Top Center</span> <input name="popup" type="radio" value="1" required/>&nbsp;&nbsp;
                                                <span>Top Right</span> <input name="popup" type="radio" value="2" required/>&nbsp;&nbsp;
                                                <span>Middle Left</span> <input name="popup" type="radio" value="3" required/>&nbsp;&nbsp;
                                                <span>Middle Center</span> <input name="popup" type="radio" value="4" required/>&nbsp;&nbsp;
                                                <span>Middle Right</span> <input name="popup" type="radio" value="5" required/>&nbsp;&nbsp;
                                                <span>Bottom Left</span> <input name="popup" type="radio" value="6" required/>&nbsp;&nbsp;
                                                <span>Bottom Center</span> <input name="popup" type="radio" value="7" required/>&nbsp;&nbsp;
                                                <span>Bottom Right</span> <input name="popup" type="radio" value="8" required/>&nbsp;&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Responder Name</td>
                                            <td style="white-space: nowrap">
                                                <input type="text" class="form-control" name="name-responder" id="name-responder"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Responder Content</td>
                                            <td style="white-space: nowrap">
                                                <textarea name="responder-content"></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Responder Image</td>
                                            <td style="white-space: nowrap">
                                                <input name="responder-img" type="file"/>
                                                <img id="blah" src="#" alt="your image"
                                                     style="display: none; width: 300px; height: auto; margin-top: 10px;"/>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="box1 col-md-12">
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <a class="btn btn-white" href="/admin/email_form/index">Cancel</a>
                                        <input type='submit' value='Save changes' class='btn btn-primary'>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-latest.min.js"></script>
<script>
    $(document).ready(function () {
        $('#select-page').multiselect({
            numberDisplayed: 6,
            disableIfEmpty: true,
            includeSelectAllOption: true,
            filterPlaceholder: 'Search',
            enableFiltering: true,
            includeFilterClearBtn: false,
            selectAllNumber: true,
        });

        var preview = '';
        $('#add-input').click(function () {
            preview = '<div class="item-block"><input class="item-title" name="label[]" placeholder="please add label"><input type="hidden" value="input" name="type[]" /><input class="form-control" disabled/><button class="btn btn-danger remove-btn">remove</button></div>';
            $('.content-preview').append(preview);
        });

        $('#add-text').click(function () {
            preview = '<div class="item-block"><input class="item-title" name="label[]" placeholder="please add label"><input type="hidden" value="textarea" name="type[]" /><textarea class="form-control" disabled></textarea><button class="btn btn-danger remove-btn">remove</button></div>';
            $('.content-preview').append(preview);
        });

        $('#add-list').click(function () {
            preview = '<div class="item-block"><input class="item-title" name="label[]" placeholder="please add label"><input type="hidden" value="list" name="type[]" /><select name="list[]" class="form-control">' +
                '<option selected disabled>Select</option>' +
                '<option value="reader">Readers</option>' +
                '</select><button class="btn btn-danger remove-btn">remove</button></div>';
            $('.content-preview').append(preview);
        });

        $(document).on("click", ".remove-btn", function () {
            $(this).parent().remove();
            preview = $('.content-preview').html();
        });

        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                    $('#blah').css('display', 'block');
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $('input[name="responder-img"]').change(function () {
            readURL(this);
        });
    });
</script>
