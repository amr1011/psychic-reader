<div class="wrapper wrapper-content animated fadeInRight">
    <?php if ($this->session->flashdata('response_add')): ?>
        <p class='record-add' style="display: none;"> <?= $this->session->flashdata('response_add') ?> </p>
    <? endif ?>
    <?php if ($this->session->flashdata('response_delete')): ?>
        <p class='record-delete' style="display: none;"> <?= $this->session->flashdata('response_delete') ?> </p>
    <? endif ?>
    <?php if ($this->session->flashdata('response_update')): ?>
        <p class='record-update' style="display: none;"> <?= $this->session->flashdata('response_update') ?> </p>
    <? endif ?>
    <?php if ($this->session->flashdata('response_exist')): ?>
        <p class='record-exist' style="display: none;"> <?= $this->session->flashdata('response_exist') ?> </p>
    <? endif ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h2><?= $title; ?></h2>
                </div>
                <div class="ibox-content">
                    <?php if ($this->session->flashdata('response_end_chat')): ?>
                        <p class="record"
                           style="display: none;"> <?= $this->session->flashdata('response_end_chat') ?> </p>
                    <? endif ?>
                    <div class="table-responsive">
                        <a href="/admin/email_form/add" class="btn btn-danger">Add New Form</a>
                        <?php if (!$data): ?>
                            <p style="padding:10px; color: red;">There is no email form.</p>
                        <?php else: ?>
                            <table class="table table-striped table-bordered table-hover data-table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Page</th>
                                    <th>Form Name</th>
                                    <th>Header</th>
                                    <th>Footer</th>
                                    <th>Popup</th>
                                    <th>Created At</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($data as $form): ?>
                                    <tr class="special_td">
                                        <td class="std"><?php echo($form["id"]); ?></td>
                                        <td class="std"><?php echo($form["page"]); ?></td>
                                        <td class="std"><?php echo($form["page_name"]);?></td>
                                        <td class="std"><?php echo($form["header"] == 0 ? "No" : "Yes"); ?></td>
                                        <td class="std"><?php echo($form["footer"] == 0 ? "No" : "Yes"); ?></td>
                                        <td class="std"><?php echo($form["popup"] >= 0 ? "Yes" : "No"); ?></td>
                                        <td class="std"><?php echo($form["created_at"]); ?></td>
                                        <td class="std">
                                            <a href='/admin/email_form/edit/<?php echo $form["id"]; ?>'
                                               class='btn btn-info btn-xs'> <span class="fa fa-pencil"></span> Edit</a>
                                            <a href='/admin/email_form/delete/<?php echo $form["id"]; ?>'
                                               class='btn btn-danger btn-xs'
                                               onClick="Javascript:return confirm('Are you sure you want to delete this record?');">
                                                <span class="fa fa-trash"></span> Delete</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="/theme/admin/js/notify.min.js"></script>
<script>
    $(document).ready(function () {

        if ($('p.record-add').html()) {
            $.notify($('p.record-add').html(), {
                className: "success",
                align: "center",
                verticalAlign: "top",
                hideDuration: 700,
                autoHideDelay: 5000,
            });
        }
        if ($('p.record-delete').html()) {
            $.notify($('p.record-delete').html(), {
                className: "error",
                align: "center",
                verticalAlign: "top",
                hideDuration: 700,
                autoHideDelay: 5000,
            });
        }
        if ($('p.record-update').html()) {
            $.notify($('p.record-update').html(), {
                className: "success",
                align: "center",
                verticalAlign: "top",
                hideDuration: 700,
                autoHideDelay: 5000,
            });
        }
        if ($('p.record-exist').html()) {
            $.notify($('p.record-exist').html(), {
                className: "info",
                align: "center",
                verticalAlign: "top",
                hideDuration: 700,
                autoHideDelay: 5000,
            });
        }
    })
</script>
