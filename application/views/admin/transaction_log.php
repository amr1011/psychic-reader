<?php
$this->member->set_member_id($this->uri->segment('4'));
$minute_balance = $this->member_funds->minute_balance();
$free_minute_balance = $this->member_funds->free_minute_balance();
$email_balance = $this->member_funds->email_balance();
?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Transaction Log</h5>
                </div>

                <div class="ibox-content">

                    <div align="left">
                        <form class="well" action="/admin/billing/fund_account/<?php echo $this->uri->segment('4') ?>" method="POST">
                            <legend>Fund Account</legend>

                            <div><b>Minute Balance:</b> <?php echo $minute_balance; ?></div>
                            <div><b>Minute Balance (FREE):</b> <?php echo $free_minute_balance; ?></div>
                            <div><b>Email Balance:</b> <?php echo $email_balance; ?></div>

                            <hr />

                            <div align="left">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <span class="add-on">Type:</span>
                                        <select name='type' class='span2 form-control'>
                                            <option value='reading'>Reading / Chat</option>
                                            <option value='email'>Email</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-3">
                                        <span class="add-on">Tier:</span>
                                        <select name='tier' class='span2 form-control'>
                                            <option value='regular'>Regular</option>
                                            <option value='half'>Half</option>
                                            <option value='free'>Free</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-2">
                                        <span class="add-on">Region:</span>
                                        <select name='region' class='span2 form-control'>
                                            <option value='US'>US</option>
                                            <option value='CA'>CA</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-2">
                                        <span class="add-on">Mins/Credits:</span>
                                        <input type="text" name='amount' class="span2 form-control" id="appendedPrependedInput" value='0' />
                                    </div>
                                    <div class="col-lg-2">
                                        <br/>
                                        <input type='submit'  value='Fund Account' class='btn btn-primary' style='height:30px;' />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <legend>Balance Records</legend>
                    <p>A list of ALL minute balance records and email credits. Information changed here WILL affect a members balance.</p>



                    <?php if ($balance): ?>

                        <table class="table table-striped table-hover table-bordered">

                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Type</th>
                                    <th>Tier</th>
                                    <th>Region</th>
                                    <th>Total</th>
                                    <th>Used</th>
                                    <th>Balance</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>

                            <?php foreach ($balance as $b) : ?>

                                <tr>
                                    <td style='width:150px;'><?php echo date("m/d/Y @ h:i A", strtotime($b->datetime)) ?></td>
                                    <td style='width:100px;'><?php echo $b->type ?></td>
                                    <td><?php echo $b->tier ?></td>
                                    <td><?php echo $b->region ?></td>
                                    <td><?php echo $b->total ?></td>
                                    <td><?php echo $b->used ?></td>
                                    <td><?php echo $b->balance ?></td>
                                    <td style='width:75px;text-align:center;'>
                                        <a href='/admin/main/delete_balance_record/<?php echo $b->id ?>' onClick="Javascript:return confirm('Are you sure you want to delete this balance record? It WILL change the balance for this member.');">Delete</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                        </table>
                    <?php endif; ?>

                    <legend>Chat History</legend>

                    <p>ANY row highlighted in YELLOW is under 4 minutes in length and should be reviewed.</p>

                    <?php if ($transcripts) : ?>

                        <table class="table table-striped table-hover table-bordered">

                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Reader</th>
                                    <th>Topic</th>
                                    <th>Chat Length</th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>

                            <?php foreach ($transcripts as $t) : ?>

                                <?php
                                $className = "";
                                if ($t->length < 240)
                                {
                                    $className = " class='warning'";
                                }

                                $hasNRR = $this->nrr_model->check_nrr_for_chat($t->id);
                                ?>

                                <tr <?php echo $className ?>>
                                    <td style='width:150px;'><?php echo date("m/d/Y @ h:i A", strtotime($t->start_datetime)) ?></td>
                                    <td style='width:100px;'><?php echo $t->reader_username ?></td>
                                    <td><?php echo $t->topic ?></td>
                                    <td style='width:200px;'><?php echo $this->system_vars->time_generator($t->length) ?></td>
                                    <td style='width:75px;text-align:center;'><a href='/admin/main/transcripts/<?php echo $t->id ?>'>Transcripts</a></td>
                                    <td style='width:135px;text-align:center;'>

                                        <?php if (!$hasNRR): ?>
                                            <a class="btn" href='/admin/main/process_nrr/<?php echo $t->id ?>' onClick="Javascript:return confirm('This action will refund the client and remove paid funds from reader. Are you sure you want to continue?');">Process NRR</a>
                                        <?php else: ?>
                                            <span class="label">NRR Processed</span>
                                        <?php endif; ?>

                                    </td>
                                </tr>
                            <?php endforeach; ?>

                            </table>
                    <?php endif; ?>

                    <legend>Full Transaction Log</legend>

                    <p>Shows all transaction records: purchases, consumables (used time or email), refunds, etc. The information below is purely used for information purposes and if changed or deleted, does NOT affect member balance in ANY way. This section is only a transaction log.</p>

                    <?php if ($transactions) : ?>

                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Type</th>
                                    <th>Amount</th>
                                    <th>Summary</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <?php foreach ($transactions as $t) : ?>
                                <tr>
                                    <td><?php echo date("m/d/Y @ h:i A", strtotime($t->datetime)) ?></td>
                                    <td><?php echo $t->type ?></td>
                                    <td><?php echo $t->amount ?></td>
                                    <td><?php echo $t->summary ?></td>
                                    <td style='width:75px;text-align:center;white-space: nowrap'>
                                        <a href='/admin/pages/edit/13/23/<?php echo $t->id ?>'>Details</a> |
                                        <a href='/admin/pages/delete_transaction/<?php echo $t->id ?>' onClick="Javascript:return confirm('Are you sure you want to delete this transaction?');">Delete</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>







