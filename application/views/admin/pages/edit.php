<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Update Id # <?= $data['id'] ?> > <?= $subnav['title'] ?></h5>
                </div>
                <div class="ibox-title">
                    <?php if($nav['id'] == 20):?>
                        <h5>Order Number: #<?= $transaction_id;?></h5>
                    <?php endif;?>
                </div>

                <div class="ibox-content">
                    <div class="row">
                        <form class = 'edit_form' action = '<?php echo $form_action; ?>' method = 'POST' enctype = 'multipart/form-data'>
                            <div class ="box1 col-md-12">

                                <?php if($this->uri->segment('4') == '6' && $this->uri->segment('5') == '22') : ?>
                                    <?php if($data['banned'] == 0) : ?>
                                        <a href="/admin/pages/ban_user/<?php echo($this->uri->segment('6'));?>" class="btn btn-danger"><span class="fa fa-warning"> BAN THIS USER</a>
                                    <?php endif; ?>
                                    <?php if($data['banned'] == 1) : ?>
                                        <a href="/admin/pages/un_ban_user/<?php echo($this->uri->segment('6'));?>" class="btn btn-success"><span class="fa fa-repeat"> UNBAN THIS USER</a>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <?php if ($this->uri->segment('4') == '6' && ($this->uri->segment('5') == '19' || $this->uri->segment('5') == '20' || $this->uri->segment('5') == '21' || $this->uri->segment('5') == '22' || $this->uri->segment('5') == '66') ) : ?>

                                    <?php
                                    $this->member->set_member_id($this->uri->segment('6'));
                                    $minute_balance = $this->member_funds->minute_balance();
                                    $email_balance = $this->member_funds->email_balance();
                                    ?>

                                    <div style='padding:20px; background:#E0E0E0;' align='center'>

                                        <table class='table'>
                                            <tr>
                                                <td>Minute Balance</td>
                                                <td align='right'><?php echo $minute_balance; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Email Balance</td>
                                                <td align='right'>$<?php echo number_format($email_balance, 2); ?></td>
                                            </tr>
                                            <tr>
                                                <td colspan='2'>
                                                    <button class="btn btn-primary" type="button" id="btn-zero-out" data-user-id="<?= $this->uri->segment('6'); ?>">Zero Out</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align='center'>
                                                    <a href='/register/force_login/<?php echo $this->uri->segment('6') ?>/7856754232' target='_blank' class='btn btn-inverse'>Login As User</a>
                                                </td>
                                                <td align='center'>
                                                    <a href='/admin/main/transactions/<?php echo $data['id'] ?>' class='btn btn-inverse'>Transactions & Chat History</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                <?php endif ?>
                                <table class = 'table'>
                                    <?php foreach ($fields as $field) : ?>
                                        <?php if (!in_array($field, $hidden_fields)): ?>
                                            <?php $field_value = getFieldValue($field, $specs, $data); ?>
                                            <?php 
                                            if($this->uri->segment(4) == 6 && strtolower($field_value['formattedField']) == "type") 
                                            {
                                                continue;
                                            }
                                            ?>

                                            <tr>
                                                <td>
                                                    <?php echo $field_value['formattedField'] ?>
                                                </td>
                                                <td style="white-space: nowrap">
                                                    <?php echo $field_value['fieldView'] ?>
                                                    <span class="text-danger"><?php echo form_error('title'); ?></span>
                                                </td>
                                            </tr>
                                            <?php
                                            if($this->uri->segment(4)==7 && ($this->uri->segment(5)==17 || $this->uri->segment(5)==18 || $this->uri->segment(5)==53) && strtolower($field_value['formattedField']) == "content"):
                                                ?>
                                            <tr>
                                                <td>
                                                    <a class="btn btn-primary" style="margin-top: 20px;" id="check-copy">Check Copyright</a>
                                                    <a href="/admin/pages/disapprove_article/<?php echo $data['id']?>" class="btn btn-danger" style="margin-top: 20px;" id="reject-article">Reject Article</a>
                                                    <div id="total-result" style="width: 90%; height: 200px; border: 1px solid gray; border-radius: 3px;">
                                                        <p><strong>All View URL: </strong><a href="" id="all-view-url" target="_blank"></a></p>
                                                        <p><strong>Query Words: </strong><span id="query-words"></span></p>
                                                        <p><strong>Count: </strong><span id="count"></span></p>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div id="copycheck-result" style="width: 100%; height: 300px; border: 1px solid gray; border-radius: 3px; overflow-y: auto; padding: 20px;">
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </table>
                            </div>
                            <div class ="box1 col-md-12">
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <a class="btn btn-white" href="<?= $cancel_button ?>">Cancel</a>
                                        <a href='<?= $delete_button ?>'  class='btn btn-danger' onclick="Javascript:return confirm('Are you sure you want to delete this record?');">Delete</a> 
                                        <input type='submit' value='Save changes' class='btn btn-primary'/>
                                    </div>
                                </div>
                            </div>
                        </form>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script>
    $( document ).ready(function() {
        var reader_ids = $("td:contains('Reader Ids')").html('Uncheck the box to the reader <br>that would exclude him from the group').css('width', '25%');
        
        $('td').find('input').each(function(){
            if($(this).prop('required')){
              var val = $(this).parent().siblings().html();
              var required = String(val) + '<span class="red" style="color: red;">*</span>';
              var update = $(this).parent().siblings().html(required);
            } 
        });
    });
</script>