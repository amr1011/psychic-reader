<?php
$hidden_fields = array("date_updated");

if (isset($specs['hide_fields']))
{
   
    $hidden_fields = explode(',', $specs['hide_fields']['value']);
}

$cancel_button = "/admin/pages/index/{$nav['id']}/" . ($subnav['id'] ? $subnav['id'] : '0');
?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                <div class="ibox-title">
                    <h5>Add <?php echo $subnav['title'] ?></h5>                   
                </div>

                <div class="ibox-content">

                    <div class="row">
                        <form action='<?php echo $form_action; ?>' method='POST' enctype="multipart/form-data">
                            <div class ="box1 col-md-12">   
                                <div class="table-responsive">
                                    <table class="table">
                                        <?php foreach ($fields as $field) : ?>
                                            
                                            <?php if (!in_array($field, $hidden_fields)): ?>
                                                <?php 
                                                if ( $this->router->fetch_class() == "reader_management" )
                                                {
                                                    $data['type'] = 'READER';
                                                }
                                                ?>
                                                    <?php $field_value = getFieldValue($field, $specs, $data); ?>

                                                <?php 
                                                if($this->uri->segment(4) == 6 && strtolower($field_value['formattedField']) == "type") 
                                                {
                                                    continue;
                                                }
                                                ?>

                                                <tr>
                                                    <td><?php echo $field_value['formattedField'] ?></td>
                                                    <td style="white-space: nowrap">
                                                        <?php echo $field_value['fieldView'] ?>
                                                        <span class="text-danger"><?php echo form_error('title'); ?></span>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </table>
                                </div>
                            </div>

                            <div class ="box1 col-md-12">
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">                                        
                                        <a class="btn btn-white" href="<?= $cancel_button ?>">Cancel</a>
                                        <input type='submit' value='Save changes' class='btn btn-primary'>

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div> <!-- end row -->
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script>
    $( document ).ready(function() {
        var reader_ids = $("td:contains('Reader Ids')").html('Uncheck the box to the reader <br>that would exclude him from the group').css('width', '25%');
        
        $('td').find('input').each(function(){
            if($(this).prop('required')){
              var val = $(this).parent().siblings().html();
              var required = String(val) + '<span class="red" style="color: red;">*</span>';
              var update = $(this).parent().siblings().html(required);
            } 
        });


    });
</script>

