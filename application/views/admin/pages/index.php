
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <?php if($this->session->flashdata('response_delete')):?>
            <p class='record-delete' style="display: none;"> <?=$this->session->flashdata('response_delete')?> </p>
        <?php endif?>
        <?php if($this->session->flashdata('response_save')):?>
            <p class='record-save' style="display: none;"> <?=$this->session->flashdata('response_save')?> </p>
        <?php endif?>
        <?php if($this->session->flashdata('response')):?>
            <p class='record' style="display: none;"> <?=$this->session->flashdata('response')?> </p>
        <?php endif?>
        <?php if($this->session->has_userdata('response_delete')):?>
            <p class='record-delete' style="display: none;"> <?=$this->session->userdata('response_delete')?> </p>
        <?php endif?>
        <?php if($this->session->has_userdata('response')):?>
            <p class='record' style="display: none;"> <?=$this->session->userdata('response')?> </p>
        <?php endif?>
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= (!$subnav ? $nav['title'] : $nav['title'] . " > " . $subnav['title']) ?></h5> 
                </div>
                <div class="ibox-content">
                    <?php if ($subnav["allow_add"] || $nav['title'] == "Banning Tool" || $subnav['id'] == 83 || $nav['id'] == 38 ): ?>
                        <div class="ibox-content m-b-sm">
                            <div class="row">
                                <a href='<?php echo $add_link; ?>' class="btn btn-w-m btn-warning">
                                    <i class="fa fa-plus"> </i> 
                                    Add <?php 

                                    if(isset($subnav['new_title'])){
                                        echo singular($subnav['new_title']);
                                    }else{
                                        //retain
                                        echo singular($subnav['title']);
                                    }
                                   
                                    ?>
                                </a>

                                <?php if ($this->uri->segment('5') == '21'): ?>
                                    <a href='/admin/pages/purge_old' onClick="Javascript:return confirm('Are you sure you want to delete ALL old members?');" class="btn btn-w-m  btn-danger">Purge ALL Old Members</a>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <!--member search tool-->       
                    <?php if($nav['id'] == 6 && $subnav['id'] == 19) {?>

                        <select id="filter-option" class="form-control" style="position: absolute; width: 25%; left: 30%; top: 143px; z-index: 1;">
                            <option value="1" selected>User Name</option>
                            <option value="2">First Name</option>
                            <option value="3">Last Name</option>
                            <option value="4">DOB</option>
                            <option value="5">GENDER</option>
                            <option value="6">EMAIL ADDRESS</option>
                            <option value="7">C/C#  (last 4 digits)</option>
                            <option value="8">Client Acct ID#</option>
                            <option value="9">HOME ADDRESS (NUMERICAL PORTION ONLY)</option>
                            <option value="10">HOME ADDRESS (STREET NAME ONLY)</option>
                            <option value="11">COUNTRY</option>
                            <!--<option value="12">PASSWORD HINT</option>-->
                            <option value="13">SIGN UP DATE</option>
                        </select>
                        <select class="form-control" id="country-list" style="position: absolute; width: 10%; left: 60%; top: 143px; z-index: 1; display: none;">
                            <option value="" disabled selected>Select Country</option>
                            <?php foreach($countries as $country) {?>
                                <option value="<?php echo($country['code']);?>"><?php echo($country['name']);?></option>
                            <?php }?>
                        </select>

                        <input class="form-control" id="date-picker" placeholder="Input Date..." style="position: absolute; width: 10%; left: 60%; top: 143px; z-index: 1; display: none;"/>
                        <input class="form-control" id="date-picker-dob" placeholder="Input Date..." style="position: absolute; width: 10%; left: 60%; top: 143px; z-index: 1; display: none;"/>
                        <div id="gender" style="position: absolute; width: 15%; left: 60%; top: 148px; z-index: 1; font-size: 15px; display: none;">
                            <input type="radio" name="gender" style="width: 15px; height: 15px;" value="Male">Male
                            <input type="radio" name="gender" style="width: 15px; height: 15px;" value="Female">Female
                            <input type="radio" name="gender" style="width: 15px; height: 15px;" value="Other">Other
                        </div>

                    <?php }?>
                    <?php if($subnav['id'] == 55 || $subnav['id'] == 56) {?>
                        <select id="filter-message" class="form-control" style="position: absolute; width: 10%; left: 65%; top: 61px; z-index: 1;">
                            <option value="0" selected disabled>Select Status</option>
                            <option value="PENDING">PENDING</option>
                            <option value="APPROVED">APPROVED</option>
                            <option value="DENNY">DENNY</option>
                        </select>
                    <?php } ?>
                    <?php if($nav['id'] == 37 && $subnav['id'] == 46) {?>
                        <select id="filter-category" class="form-control" style="position: absolute; width: 15%; left: 50%; top: 61px; z-index: 1;">
                            <option value="0" selected disabled>Select Category</option>
                            <?php foreach($categories as $category) {?>
                                <option value="<?php echo($category['title'])?>"><?php echo($category['title'])?></option>
                            <?php }?>
                        </select>
                    <?php } ?>
                    <!--Ban Members-->
                    <?php if($nav['id'] == 6 && $subnav['id'] == 22) {?>

                        <input class="form-control" id="date-picker-dob" placeholder="Input DOB Here..." style="position: absolute; width: 10%; left: 43%; top: 61px; z-index: 1;"/>
                        <div id="gender-ban" style="position: absolute; width: 25%; left: 55%; top: 61px; z-index: 1; font-size: 15px;">
                            <b style="margin-right: 10px;">Select gender</b>
                            <input type="checkbox" name="gender" style="width: 15px; height: 15px;" value="Male">Male
                            <input type="checkbox" name="gender" style="width: 15px; height: 15px;" value="Female">Female
                            <input type="checkbox" name="gender" style="width: 15px; height: 15px;" value="Other">Other
                        </div>
                        <?php if($all_ban == 0) : ?>
                            <div id="gender" style="position: absolute; width: 10%; right:0%; top: 61px; z-index: 1; font-size: 15px;">
                                <a href="/admin/pages/ban_all" class="btn btn-danger" id="ban-all" onClick="Javascript:return confirm('Are you sure you want to delete this record?');"><span class="fa fa-warning"></span> BAN ALL</a>
                            </div>
                        <?php endif; ?>
                        <?php if($all_ban == 1) : ?>
                            <div id="gender" style="position: absolute; width: 10%; right:0%; top: 61px; z-index: 1; font-size: 15px;">
                                <a href="/admin/pages/un_ban_all" class="btn btn-success" id="un-ban-all" onClick="Javascript:return confirm('Are you sure you want to delete this record?');"><span class="fa fa-repeat"></span> UN BAN ALL</a>
                            </div>
                        <?php endif; ?>
                    <?php }?>
                    <?php if($nav['id'] == 6 && $subnav['id'] == 68 && $data) {?>
                        <p id="date_filter">
                            <span id="date-label-from" class="date-label">From: </span><input class="date_range_filter date" type="text" id="datepicker_from" />
                            <span id="date-label-to" class="date-label">To:<input class="date_range_filter date" type="text" id="datepicker_to" />
                        </p>
                    <?php }?>
                    <?php if($nav['id'] == 12 && $subnav['id'] == 87 && $data) {?>
                        <select class="form-control" id="reader-list" style="position: absolute; width: 10%; left: 20%; top: 120px; z-index: 1;">
                            <option value="" disabled selected>Select Reader</option>
                            <option value="">All Readers</option>
                            <?php foreach($readers_chat_room as $readers) {?>
                                <option value="<?php echo($readers['username']);?>"><?php echo($readers['username']);?></option>
                            <?php }?>
                        </select>
                        <p id="date_filter" style="position: absolute; width: 410px; left: 35%; top: 120px; z-index: 1;">
                            <span id="date-label-from" class="date-label">From: </span><input class="date_range_filter date" type="text" id="chat-logon-from" />
                            <span id="date-label-to" class="date-label">To:<input class="date_range_filter date" type="text" id="chat-logon-to" />
                        </p>
                    <?php }?>
                    <?php if($nav['id'] == 13 && $subnav['id'] == 60 && $data) {?>
                        <select class="form-control" id="reader-list" style="position: absolute; width: 10%; left: 35%; z-index: 1;">
                            <option value="" disabled selected>Select Member</option>
                            <?php foreach($members as $member) {?>
                                <option value="<?php echo($member['username']);?>"><?php echo($member['username']);?></option>
                            <?php }?>
                        </select>
                        <p id="date_filter" style="position: absolute; width: 30%; left: 50%; z-index: 1;">
                            <span id="date-label-from" class="date-label">From: </span><input class="date_range_filter date" type="text" id="transaction_date_from" />
                            <span id="date-label-to" class="date-label">To:<input class="date_range_filter date" type="text" id="transaction_date_to" />
                        </p>
                    <?php }?>
                    <div class="table-responsive">


                        <?php if (!$data): ?>
                            <p style='padding:10px; color: red;'>There is no data to display in <?php echo (!$subnav ? $nav['title'] : $subnav['title']) ?> </p>
                        <?php else: ?>
                            <?php echo ($pagination ? "<span class='pagination'>{$pagination}</span>" : "") ?>
                            <?php if($nav['id'] == 37 && $subnav['id'] == 84): ?>
                                <table class="table table-striped table-bordered table-hover  common-data-table" >
                                    <thead>
                                    <tr>

                                        <th style="text-align: center"><strong>ID</strong></th>
                                        <th style="text-align: center"><strong>YEAR</strong></th>
                                        <th style="text-align: center"><strong>MONTH</strong></th>
                                        <th style="text-align: center"><strong>DAY</strong></th>
                                        <th style="text-align: center"><strong>GENDER</strong></th>
                                        <th style="text-align: center; width: 25px"><strong>ACTION</strong></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($data as $table): $date = strtotime($table['day_of_birth']); ?>
                                        <tr class='special_td'>
                                            <td class="std"><?php echo $table['id']; ?></td>
                                            <td class="std"><?php echo date("Y", $date); ?></td>
                                            <td class="std"><?php echo date("F", $date); ?></td>
                                            <td class="std"><?php echo date("d", $date); ?></td>
                                            <td class="std"><?php echo $table['gender']; ?></td>
                                            <td style="text-align: center; white-space: nowrap">
                                                <a href='<?php echo $edit_link[$table["id"]]; ?>' class='btn btn-info btn-xs'> <span class="fa fa-pencil"></span> Edit</a>
                                                <a href='/admin/pages/delete/<?php echo $nav['id'] ?>/<?php echo ($subnav['id'] ? $subnav['id'] : '0') ?>/<?php echo $table['id']; ?>' class="btn btn-danger btn-xs" onClick="Javascript:return confirm('Are you sure you want to delete this record?');"><span class="fa fa-trash"></span> Delete</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            <?php elseif($nav['id'] == 6 && $subnav['id'] == 68): ?>
                                <table class="table table-striped table-bordered table-hover  canadian-data-table" >
                                    <thead>
                                    <tr>
                                        <th style="text-align: center"><strong>ID</strong></th>
                                        <th style="text-align: center"><strong>Username</strong></th>
                                        <th style="text-align: center"><strong>Country</strong></th>
                                        <th style="text-align: center"><strong>Datetime</strong></th>
                                        <th style="text-align: center"><strong>Payment type</strong></th>
                                        <th style="text-align: center"><strong>Amount</strong></th>
                                        <th style="text-align: center"><strong>Summary</strong></th>
                                        <th style="text-align: center; "><strong>Currency</strong></th>
                                        <th style="text-align: center; "><strong>Action</strong></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($data as $table): ?>
                                        <tr class='special_td'>
                                            <td class="std"><?php echo $table['id']; ?></td>
                                            <td class="std"><?php echo $table['username']; ?></td>
                                            <td class="std"><?php echo $table['country']; ?></td>
                                            <td class="std"><?php echo $table['datetime']; ?></td>
                                            <td class="std"><?php echo $table['payment_type']; ?></td>
                                            <td class="std"><?php echo $table['amount']; ?></td>
                                            <td class="std"><?php echo $table['summary']; ?></td>
                                            <td class="std"><?php echo $table['currency']; ?></td>
                                            
                                            <td style="text-align: center; white-space: nowrap">
                                                <a href='<?php echo $edit_link[$table["id"]]; ?>' class='btn btn-info btn-xs'> <span class="fa fa-pencil"></span> Edit</a>
                                                <a href='/admin/pages/delete/<?php echo $nav['id'] ?>/<?php echo ($subnav['id'] ? $subnav['id'] : '0') ?>/<?php echo $table['id']; ?>' class="btn btn-danger btn-xs" onClick="Javascript:return confirm('Are you sure you want to delete this record?');"><span class="fa fa-trash"></span> Delete</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            <?php elseif($nav['id'] == 36 && $subnav['id'] == 55): ?>

                                <table class="table table-striped table-bordered table-hover common-data-table" >
                                    <thead>
                                    <tr>

                                        <th style="text-align: center"><strong>ID</strong></th>
                                        <th style="text-align: center"><strong>FROM</strong></th>
                                        <th style="text-align: center"><strong>TO</strong></th>
                                        <th style="text-align: center"><strong>SUBJECT</strong></th>
                                        <th style="text-align: center"><strong>MESSAGE</strong></th>
                                        <th style="text-align: center; "><strong>REASON</strong></th>
                                        <th style="text-align: center; "><strong>CREATED AT</strong></th>
                                        <th style="text-align: center"><strong>STATUS</strong></th>
                                        <th style="text-align: center; width: 100px;"><strong>ACTION</strong></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($data1 as $table): ?>
                                        <tr class='special_td'>
                                            <td class="std"><?php echo $table['id']; ?></td>
                                            <td class="std"><?php echo $table['sender_name']; ?></td>
                                            <td class="std"><?php echo $table['recipient_name']; ?></td>
                                            <td class="std"><?php echo $table['subject']; ?></td>
                                            <td class="std"><?php echo $table['message']; ?></td>
                                            <td class="std"><?php echo $table['reason']; ?></td>
                                            <td class="std"><?php echo $table['created_at']; ?></td>
                                            <td class="std"><?php echo $table['message_status']; ?></td>
                                            <td style="text-align: center; white-space: nowrap">
                                                <a href='<?php echo $edit_link[$table["id"]]; ?>' class='btn btn-info btn-xs'> <span class="fa fa-pencil"></span> Edit</a>
                                                <a href='/admin/pages/delete/<?php echo $nav['id'] ?>/<?php echo ($subnav['id'] ? $subnav['id'] : '0') ?>/<?php echo $table['id']; ?>' class="btn btn-danger btn-xs" onClick="Javascript:return confirm('Are you sure you want to delete this record?');"><span class="fa fa-trash"></span> Delete</a>
                                                <?php if($table['message_status'] == 'PENDING') :?>
                                                    <a href='/admin/pages/approve_pending_message/<?php echo $table['id']; ?>' class='btn btn-success btn-xs approve-message' style="width: 90px"> <span class="fa fa-warning"></span> Approve</a>
                                                    <a href='/admin/pages/deny_pending_message/<?php echo $table['id']; ?>'  class='btn btn-danger btn-xs deny-message' style="width: 90px"> <span class="fa fa-warning"></span> Deny</a>
                                                <?php endif;?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            <?php elseif($nav['id'] == 36 && $subnav['id'] == 56): ?>

                                <table class="table table-striped table-bordered table-hover common-data-table" >
                                    <thead>
                                    <tr>

                                        <th style="text-align: center"><strong>ID</strong></th>
                                        <th style="text-align: center"><strong>FROM</strong></th>
                                        <th style="text-align: center"><strong>TO</strong></th>
                                        <th style="text-align: center"><strong>SUBJECT</strong></th>
                                        <th style="text-align: center"><strong>MESSAGE</strong></th>
                                        <th style="text-align: center; "><strong>REASON</strong></th>
                                        <th style="text-align: center; "><strong>CREATED AT</strong></th>
                                        <th style="text-align: center"><strong>STATUS</strong></th>
                                        <th style="text-align: center; width: 100px;"><strong>ACTION</strong></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($data1 as $table): ?>
                                        <tr class='special_td'>
                                            <td class="std"><?php echo $table['id']; ?></td>
                                            <td class="std"><?php echo $table['sender_name']; ?></td>
                                            <td class="std"><?php echo $table['recipient_name']; ?></td>
                                            <td class="std"><?php echo $table['subject']; ?></td>
                                            <td class="std"><?php echo $table['message']; ?></td>
                                            <td class="std"><?php echo $table['reason']; ?></td>
                                            <td class="std"><?php echo $table['created_at']; ?></td>
                                            <td class="std"><?php echo $table['message_status']; ?></td>
                                            <td style="text-align: center; white-space: nowrap">
                                                <a href='<?php echo $edit_link[$table["id"]]; ?>' class='btn btn-info btn-xs'> <span class="fa fa-pencil"></span> Edit</a>
                                                <a href='/admin/pages/delete/<?php echo $nav['id'] ?>/<?php echo ($subnav['id'] ? $subnav['id'] : '0') ?>/<?php echo $table['id']; ?>' class="btn btn-danger btn-xs" onClick="Javascript:return confirm('Are you sure you want to delete this record?');"><span class="fa fa-trash"></span> Delete</a>
                                                <a href='/admin/pages/approve_group_pending_message/<?php echo $table['id']; ?>' class='btn btn-success btn-xs approve-message' style="width: 90px"> <span class="fa fa-warning"></span> Approve</a>
                                                <a href='/admin/pages/deny_group_pending_message/<?php echo $table['id']; ?>'  class='btn btn-danger btn-xs deny-message' style="width: 90px"> <span class="fa fa-warning"></span> Deny</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            <?php elseif($nav['id'] == 12 && $subnav['id'] == 87): ?>

                                <table class="table table-striped table-bordered table-hover chat-logon-data-table" >
                                    <thead>
                                    <tr>

                                        <th style="text-align: center"><strong>ID</strong></th>
                                        <th style="text-align: center"><strong>Reader</strong></th>
                                        <th style="text-align: center"><strong>Client</strong></th>
                                        <th style="text-align: center"><strong>Topic</strong></th>
                                        <th style="text-align: center"><strong>Logon Time</strong></th>
                                        <th style="text-align: center"><strong>Logout Time</strong></th>
                                        <th style="text-align: center; width: 100px;"><strong>ACTION</strong></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($data as $table): ?>
                                        <tr class='special_td'>
                                            <td class="std"><?php echo $table['id']; ?></td>
                                            <td class="std"><?php echo $table['reader_name']; ?></td>
                                            <td class="std"><?php echo $table['client_name']; ?></td>
                                            <td class="std"><?php echo $table['topic']; ?></td>
                                            <td class="std"><?php echo $table['create_datetime']; ?></td>
                                            <td class="std"><?php echo $table['logout_time']; ?></td>
                                            <td style="text-align: center; white-space: nowrap">
                                                <a href='/admin/pages/delete/<?php echo $nav['id'] ?>/<?php echo ($subnav['id'] ? $subnav['id'] : '0') ?>/<?php echo $table['id']; ?>' class="btn btn-danger btn-xs" onClick="Javascript:return confirm('Are you sure you want to delete this record?');"><span class="fa fa-trash"></span> Delete</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            <?php elseif ($nav['id'] == 6 && $subnav['id'] == 69): ?>
                                <table class="table table-striped table-bordered table-hover  common-data-table" >
                                    <thead>
                                    <tr>

                                        <th style="text-align: center"><strong>ID</strong></th>
                                        <th style="text-align: center"><strong>Reader</strong></th>
                                        <th style="text-align: center"><strong>Client</strong></th>
                                        <th style="text-align: center"><strong>BackTime</strong></th>
                                        <th style="text-align: center"><strong>Week Day</strong></th>
                                        <th style="text-align: center;"><strong>Status</strong></th>
                                        <th style="text-align: center;"><strong>Create at</strong></th>
                                        <th style="text-align: center;"><strong>Action</strong></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($data as $table): ?>
                                        <tr class='special_td'>
                                            <td class="std"><?php echo $table['id']; ?></td>
                                            <td class="std"><?php echo $table['reader_username']; ?></td>
                                            <td class="std"><?php echo $table['client_username']; ?></td>
                                            <td class="std"><?php echo $table['gift_size']; ?></td>
                                            <td class="std week-day"><?php echo $table['day_of_week']; ?></td>
                                            <td class="std status-givetime"><?php echo $table['status']; ?></td>
                                            <td class="std"><?php echo $table['created_at']; ?></td>
                                            <td style="text-align: center; white-space: nowrap">

                                                <a href='/admin/pages/delete/<?php echo $nav['id'] ?>/<?php echo ($subnav['id'] ? $subnav['id'] : '0') ?>/<?php echo $table['id']; ?>' class="btn btn-danger btn-xs" onClick="Javascript:return confirm('Are you sure you want to delete this record?');"><span class="fa fa-trash"></span> Delete</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            <?php elseif ($nav['id'] == 6 && $subnav['id'] == 66): ?>
                                <table class="table table-striped table-bordered table-hover  common-data-table" >
                                    <thead>
                                    <tr>

                                        <th style="text-align: center"><strong>ID</strong></th>
                                        <th style="text-align: center"><strong>Client</strong></th>
                                        <th style="text-align: center"><strong>Sign up</strong></th>
                                        <th style="text-align: center"><strong>Last Sign in</strong></th>
                                        <th style="text-align: center"><strong>Action</strong></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($data as $table): ?>
                                        <tr class='special_td'>
                                            <td class="std"><?php echo $table['id']; ?></td>
                                            <td class="std"><?php echo $table['username']; ?></td>
                                            <td class="std"><?php echo $table['registration_date']; ?></td>
                                            <td class="std week-day"><?php echo $table['last_login_date']; ?></td>
                                            <td style="text-align: center; white-space: nowrap">

                                                <a href='/admin/pages/delete_old_account/<?php echo $nav['id'] ?>/<?php echo ($subnav['id'] ? $subnav['id'] : '0') ?>/<?php echo $table['id']; ?>' class="btn btn-danger btn-xs" onClick="Javascript:return confirm('Are you sure you want to delete this record?');"><span class="fa fa-trash"></span> Delete</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            <?php elseif ($nav['id'] == 6 && $subnav['id'] == 67): ?>
                                <table class="table table-striped table-bordered table-hover  common-data-table" >
                                    <thead>
                                    <tr>

                                        <th style="text-align: center"><strong>ID</strong></th>
                                        <th style="text-align: center"><strong>Username</strong></th>
                                        <th style="text-align: center"><strong>First Name</strong></th>
                                        <th style="text-align: center"><strong>Last Name</strong></th>
                                        <th style="text-align: center"><strong>Sign up</strong></th>
                                        <th style="text-align: center"><strong>Last Sign in</strong></th>
                                        <th style="text-align: center"><strong>Deleted Date</strong></th>
                                        <th style="text-align: center"><strong>Action</strong></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($data as $table): ?>
                                        <tr class='special_td'>
                                            <td class="std"><?php echo $table['id']; ?></td>
                                            <td class="std"><?php echo $table['username']; ?></td>
                                            <td class="std"><?php echo $table['first_name']; ?></td>
                                            <td class="std"><?php echo $table['last_name']; ?></td>
                                            <td class="std"><?php echo $table['registration_date']; ?></td>
                                            <td class="std week-day"><?php echo $table['last_login_date']; ?></td>
                                            <td class="std"><?php echo $table['deleted_date']; ?></td>
                                            <td style="text-align: center; white-space: nowrap">

                                                <a href='/admin/pages/delete_unused_log/<?php echo $nav['id'] ?>/<?php echo ($subnav['id'] ? $subnav['id'] : '0') ?>/<?php echo $table['id']; ?>' class="btn btn-danger btn-xs" onClick="Javascript:return confirm('Are you sure you want to delete this record?');"><span class="fa fa-trash"></span> Delete</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            <?php elseif ($nav['id'] == 37 && $subnav['id'] == 46): ?>
                                <table class="table table-striped table-bordered table-hover  common-data-table" >
                                    <thead>
                                    <tr>

                                        <th style="text-align: center"><strong>ID</strong></th>
                                        <th style="text-align: center"><strong>Username</strong></th>
                                        <th style="text-align: center"><strong>Category</strong></th>
                                        <th style="text-align: center"><strong>Action</strong></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($data as $table): ?>
                                        <tr class='special_td'>
                                            <td class="std"><?php echo $table['id']; ?></td>
                                            <td class="std"><?php echo $table['username']; ?></td>
                                            <td class="std"><?php echo $table['title']; ?></td>

                                            <td style="text-align: center; white-space: nowrap">
                                                <a href='/admin/pages/delete_category/<?php echo $table['id']; ?>' class="btn btn-danger btn-xs" onClick="Javascript:return confirm('Are you sure you want to delete this record?');"><span class="fa fa-trash"></span> Delete</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>

                            <!-- ----------------START OF ELSE------------------- -->
                            <?php else: ?>
                                <?php if($nav['id'] == 20 && $subnav['id'] == 77):?>

                                <?php endif;?>

                                <?php if($nav['id'] == 6):?>
                                    <table class="table table-striped table-bordered table-hover  custom-data-table" >
                                <?php elseif($nav['id'] == 13 && $subnav['id'] == 60):?>
                                    <table class="table table-striped table-bordered table-hover  cad-transaction-table" >
                                <?php else: ?>
                                    <table class="table table-striped table-bordered table-hover  common-data-table" >
                            <?php endif;?>
                                    <thead>
                                        <tr>
                                            <th style="text-align: center"><strong>ID</strong></th>
                                            <th style="text-align: center"><strong>DATE TIME</strong></th>
                                            <th style="text-align: center"><strong>USER NAME</strong></th>
                                            <th style="text-align: center"><strong>TYPE</strong></th>
                                            <th style="text-align: center"><strong>AMOUNT</strong></th>
                                            <th style="text-align: center"><strong>SUMMARY</strong></th>
                                            <th style="text-align: center"><strong>STATUS</strong></th>
                                            <th style="text-align: center; width: 25px"><strong>ACTION</strong></th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($data as $table): ?>
                                            <tr class='special_td'>
                                                <?php foreach ($fields as $field): ?>

                                                   
                                                    <td class='std'>
                                                        <?php
                                                            if($field == 'checked' || $field == 'admin_checked'||$field=='confirm'||$field=='decline'||$field=='decline_checked'||$field=='admin_decline_checked' || $field=='enable_email')
                                                            {
                                                                if(parseFields($table, $field, $specs) == 1)
                                                                {
                                                                    echo('YES');
                                                                } elseif (parseFields($table, $field, $specs) == 0)
                                                                {
                                                                    echo('NO');
                                                                }
                                                            } else if ($field == 'status')
                                                            {
                                                                if(parseFields($table, $field, $specs) == 0)
                                                                {
                                                                    echo('pending');
                                                                } elseif(parseFields($table, $field, $specs) == 1)
                                                                {
                                                                    echo('approved');
                                                                } elseif(parseFields($table, $field, $specs) == 2)
                                                                {
                                                                    echo('disapproved');
                                                                }
                                                            } else {
                                                                echo(parseFields($table, $field, $specs));
                                                            }
                                                        ?>
                                                    </td>
                                                <?php endforeach; ?>
                                                <?php if (!empty($nav['id']) && $nav['id'] == '13'): ?>
                                                    <td class='std'><?php echo getTransactionLevel($table, $nav); ?></td>
                                                <?php endif; ?>
                                                <?php if (!empty($transactionType) && $transactionType == 'pending') : ?>
                                                    <td class='std' align='center' width='25'><a href='/admin/main/finalize_transaction/void/<?php echo $table['id'] ?>' class='btn btn-inverse btn-small' onClick="Javascript:return confirm('Are you sure you want to manually void this transaction?');">Void</a></td>
                                                    <td class='std' align='center' width='25'><a href='/admin/main/finalize_transaction/settle/<?php echo $d['id'] ?>' class='btn btn-info btn-small' onClick="Javascript:return confirm('Are you sure you want to manually settle this transaction?');">Settle</a></td>


                                                <?php endif; ?>

                                                <!-- action starts here -->
                                                <td style="text-align: center; white-space: nowrap">
                                                    <?php if($subnav['id'] != 65) {?>
                                                        <a href='<?php echo $edit_link[$table["id"]]; ?>' class='btn btn-info btn-xs'> <span class="fa fa-pencil"></span> Edit</a>
                                                    <?php }?>
                                                    <?php if($nav['id'] == 12 && $subnav['id'] == 35) { ?>
                                                        <a href='<?php echo '/admin/pages/impersonate' . '/' . $table["id"]; ?>' class='btn btn-info btn-xs'> <span class="fa fa-key"></span> Impersonate</a>
                                                        <a href='<?php echo '/admin/pages/reset_password' . '/' . $table["id"] . '/' . $nav['id'] . '/' . $subnav['id'] ; ?>' class='btn btn-primary btn-xs'> <span class="fa fa-key"></span> Reset Password</a>
                                                        <?php if($table['block_reader'] == false) :?> <a href='<?php echo $block_link . '/' . $table["id"]; ?>' class='btn btn-warning btn-xs' onClick="Javascript:return confirm('Are you sure you want to block reader #<?php echo($table["id"])?>?');"> <span class="fa fa-warning"></span> Block</a> <?php endif; ?>
                                                        <?php if($table['block_reader'] == true) :?> <a href='<?php echo $un_block_link . '/' . $table["id"]; ?>' class='btn btn-primary btn-xs' onClick="Javascript:return confirm('Are you sure you want to unblock this reader #<?php echo($table["id"])?>?');"> <span class="fa fa-warning"></span> Un-Block</a> <?php endif; ?>

                                                    <?php } ?>
                                                    
                                                    <?php if($nav['id'] == 34 &&  $subnav['id'] == 47) : ?>
                                                        <a href="/admin/pages/approve_profile/<?php echo $table['id']?>" class='btn btn-success btn-xs' onClick="Javascript:return confirm('Are you sure you want to approve this record #<?php echo($table["id"])?>?');"><span>Approve</span></a>
                                                        <a href="/admin/pages/disapprove_profile/<?php echo $table['id']?>" class='btn btn-warning btn-xs' onClick="Javascript:return confirm('Are you sure you want to reject this record #<?php echo($table["id"])?>?');"><span>Disapprove</span></a>
                                                    <?php endif;?>
                                                    <?php if($nav['id'] == 35 && $subnav['id'] == 50 ) : ?>
                                                        <a href="/admin/pages/approve_promotion/<?php echo $table['id']?>" class='btn btn-success btn-xs' onClick="Javascript:return confirm('Are you sure you want to approve this record #<?php echo($table["id"])?>?');"><span>Approve</span></a>
                                                        <a href="/admin/pages/disapprove_promotion/<?php echo $table['id']?>" class='btn btn-warning btn-xs' onClick="Javascript:return confirm('Are you sure you want to reject this record #<?php echo($table["id"])?>?');"><span>Disapprove</span></a>
                                                    <?php endif;?>
                                             
                                                    <?php if($nav['id'] == 7 && ($subnav['id'] == 32 )) : ?>
                                                        <a href="/admin/pages/approve_blog/<?php echo $table['id']?>" class='btn btn-success btn-xs' onClick="Javascript:return confirm('Are you sure you want to approve this record #<?php echo($table["id"])?>?');"><span>Approve</span></a>
<!--                                                        <a href="/admin/pages/disapprove_blog/--><?php //echo $table['id']?><!--" class='btn btn-warning btn-xs'><span>Disapprove</span></a>-->
                                                    <?php endif;?>
                                                    <?php if($nav['id'] == 7 && $subnav['id'] == 53) : ?>
                                                        <a href="/admin/pages/approve_article/<?php echo $table['id']?>" class='btn btn-success btn-xs' onClick="Javascript:return confirm('Are you sure you want to approve this record #<?php echo($table["id"])?>?');"><span class="fa fa-thumbs-up"> Approve</span></a>
                                                        <a href="/admin/pages/disapprove_article/<?php echo $table['id']?>" class='btn btn-warning btn-xs' onClick="Javascript:return confirm('Are you sure you want to reject this record #<?php echo($table["id"])?>?');"><span class="fa fa-thumbs-down"> Reject</span></a>
                                                    <?php endif;?>
                                                    <?php if($nav['id'] == 6 && $subnav['id'] == 71) : ?>
                                                        <a href="/admin/pages/approve_testimonial/<?php echo $table['id']?>" class='btn btn-success btn-xs' onClick="Javascript:return confirm('Are you sure you want to approve this record #<?php echo($table["id"])?>?');"><span class="fa fa-thumbs-up"> Approve</span></a>
                                                        <a href="/admin/pages/reject_testimonial/<?php echo $table['id']?>" class='btn btn-danger btn-xs' onClick="Javascript:return confirm('Are you sure you want to reject this record #<?php echo($table["id"])?>?');"><span class="fa fa-thumbs-down"> Reject</span></a>
                                                    <?php endif;?>

                                                  <button type="button"  data-id="<?php echo $table['id'] ?>"class="btn btn-primary btn-xs view-details" data-toggle="modal" data-target="#exampleModalCenter">
                                                        <span class="fa fa-key"></span> View
                                                        </button>
                                                    

                                                    <a href='/admin/pages/delete/<?php echo $nav['id'] ?>/<?php echo ($subnav['id'] ? $subnav['id'] : '0') ?>/<?php echo $table['id']; ?>' class="btn btn-danger btn-xs" onClick="Javascript:return confirm('Are you sure you want to delete this record?');"><span class="fa fa-trash"></span> Delete</a>

                                                    

                                                    <?php if (in_array($subnav['id'], [19, 20])): ?>
                                                        <a href='<?php echo '/admin/pages/impersonate' . '/' . $table["id"]; ?>' class='btn btn-info btn-xs'> <span class="fa fa-key"></span> Impersonate</a>
                                                        <a href='<?php echo '/admin/pages/reset_password' . '/' . $table["id"] . '/' . '6' . '/' . $subnav['id']; ?>' class='btn btn-primary btn-xs'> <span class="fa fa-key"></span> Reset Password</a>
                                                        <?php if (isBanned($table['id'])): ?>
                                                            <a href='/admin/pages/ban_member/<?php echo $nav['id'] ?>/<?php echo ($subnav['id'] ? $subnav['id'] : '0') ?>/<?php echo $table['id']; ?>/0' class='btn btn-success btn-xs' style="width: 90px"> <span class="fa fa-warning"></span> Un-Banned</a>
                                                        <?php else: ?>
                                                            <a href='/admin/pages/ban_member/<?php echo $nav['id'] ?>/<?php echo ($subnav['id'] ? $subnav['id'] : '0') ?>/<?php echo $table['id']; ?>/1' class='btn btn-danger btn-xs' style="width: 90px"> <span class="fa fa-warning"></span> Banned</a>
                                                        <?php endif; ?>
                                                        <?php //if (isFeatured($table['id'])): ?>
                                                        <!--    <a href='/admin/reader_management/toggleFeatured/--><?php //echo $table['id']; ?><!--' class='btn btn-success btn-xs' style="width: 90px"> <span class="fa fa-asterisk"></span> Un-Featured</a>-->
                                                        <?php //else: ?>
                                                        <!--    <a href='/admin/reader_management/toggleFeatured/--><?php //echo $table['id']; ?><!--' class='btn btn-success btn-xs' style="width: 90px"> <span class="fa fa-asterisk"></span> Featured</a>-->
                                                        <?php //endif; ?>
                                                    <?php endif; ?>
                                                    <?php if (in_array($subnav['id'], [35, 36])): ?>
                                                        <?php if (isFeatured($table['id'])): ?>
                                                            <a href='/admin/reader_management/toggleFeatured/<?php echo $table['id']; ?>' class='btn btn-success btn-xs' style="width: 90px" onClick="Javascript:return confirm('Are you sure you want to feature this reader #<?php echo($table["id"])?>?');"> <span class="fa fa-asterisk"></span> Un-Featured</a>
                                                        <?php else: ?>
                                                            <a href='/admin/reader_management/toggleFeatured/<?php echo $table['id']; ?>' class='btn btn-success btn-xs' style="width: 90px" onClick="Javascript:return confirm('Are you sure you want to un-feature this reader #<?php echo($table["id"])?>?');"> <span class="fa fa-asterisk"></span> Featured</a>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                    <?php if (in_array($subnav['id'], [22])): ?>
                                                        <?php if (isBanned($table['id'])): ?>
                                                            <a href='/admin/pages/ban_member/<?php echo $nav['id'] ?>/<?php echo ($subnav['id'] ? $subnav['id'] : '0') ?>/<?php echo $table['id']; ?>/0' class='btn btn-success btn-xs' style="width: 90px"> <span class="fa fa-warning"></span> UN BAN</a>
                                                        <?php else: ?>
                                                            <a href='/admin/pages/ban_member/<?php echo $nav['id'] ?>/<?php echo ($subnav['id'] ? $subnav['id'] : '0') ?>/<?php echo $table['id']; ?>/1' class='btn btn-danger btn-xs' style="width: 90px"> <span class="fa fa-warning"></span> FULL BAN</a>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            <?php endif; ?>
                                <?php echo ($pagination ? "<span class='pagination'>{$pagination}</span>" : "") ?>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Transactions > View Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <form>
              <div class="form-group row">
                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm">User Name:</label>
                <div class="col-sm-9">
                  <label class="clear username"></label>
                </div>
              </div>

              <div class="form-group row">
                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm">Full Name:</label>
                <div class="col-sm-9">
                 <label class="clear fullname"></label>
                </div>
              </div>

              <div class="form-group row">
                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm">Address:</label>
                <div class="col-sm-9">
                   <label class="clear address"></label>
                </div>
              </div>

               <div class="form-group row">
                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm">DOB:</label>
                <div class="col-sm-9">
                  <label class="clear dob"></label>
                </div>
              </div>

               <div class="form-group row">
                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm">Chat Topic</label>
                <div class="col-sm-9">
                   <label class="clear chattopic"></label>
                </div>
              </div>

               <div class="form-group row">
                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm">Reader:</label>
                <div class="col-sm-9">
                  <label class="clear choosenreader"></label>
                </div>
              </div>

              <div class="form-group row">
                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm">Order Status:</label>
                <div class="col-sm-9">
                 <label class="clear orderstatus"></label>
                </div>
              </div>

              <div class="form-group row">
                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm">Amount:</label>
                <div class="col-sm-9">
                 <label class="clear amount"></label>
                </div>
              </div>

              <div class="form-group row">
                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm">Paid via:</label>
                <div class="col-sm-9">
                 <label class="clear paidvia"></label>
                </div>
              </div>
            
            </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>




<input type="hidden" value="<?php echo $nav['id'];?>" id="navid">
<input type="hidden" value="<?php echo $subnav['id'];?>" id="subnavid">
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<script>
    $(document).ready(function () {

        $('a.deny-message').on('click',function () {

            if (! /\S/.test($(this).parent().parent().find("td:nth-child(6)").text()))
            {
                alert('Please add reason to deny this message.')
                return false;
            }
        });

        function confirm_click()
        {
            return confirm("Are you sure ?");
        }

        var navid = parseInt($('#navid').val());
        var subnavid = parseInt($('#subnavid').val());

        var sort_desc_navid = [12, 35];
        var sort_desc_subnavid = [57, 82];

        var navid_ = sort_desc_navid.includes(navid);
        var subnavid_ = sort_desc_subnavid.includes(subnavid);

        if( navid_ == true && subnavid_ == true){
            var common_table = $('.common-data-table').DataTable({
                "order": [[ 0, "desc" ]]
                });
        }else{
            var common_table = $('.common-data-table').DataTable({
                "order": [[ 0, "desc" ]]
                });
        }

        $('#filter-message').change('on', function () {

            let filter = $(this).val();

            common_table
                .search( '' )
                .columns().search( '' )
                .draw();

            common_table.column(7)
                .search(filter)
                .draw();
        });

        $("#filter-category").change('on', function() {
            let filter = $(this).val();
            common_table
                .search( '' )
                .columns().search( '' )
                .draw();

            common_table.column(2)
                .search(filter)
                .draw();
        });

        var pathname = window.location.pathname;

        var table;
        var oTable;
        var cTable;

        $('.custom-data-table').dataTable({
            "order": [[ 0, "desc" ]]
        });

        if(pathname.split("/")[4] == 12 && pathname.split("/")[5] == 87) {

            $(function() {
                cTable = $('.chat-logon-data-table').dataTable({
                    "oLanguage": {
                        "sSearch": "Search all columns:"
                    }
                });

                $("#chat-logon-from").datepicker({
                    showOn: "button",
                    changeYear: true,
                    buttonImageOnly: false,
                    "onSelect": function(date) {
                        minDateFilter = new Date(date).getTime();
                        cTable.fnDraw();
                    }
                }).keyup(function() {
                    minDateFilter = new Date(this.value).getTime();
                    cTable.fnDraw();
                });

                $("#chat-logon-to").datepicker({
                    showOn: "button",
                    changeYear: true,
                    buttonImageOnly: false,
                    "onSelect": function(date) {
                        maxDateFilter = new Date(date).getTime();
                        cTable.fnDraw();
                    }
                }).keyup(function() {
                    maxDateFilter = new Date(this.value).getTime();
                    cTable.fnDraw();
                });

                $("#reader-list").change('on', function() {
                    let filter = $(this).val();

                    cTable.fnFilter( filter, 1 );
                });

            });

            // Date range filter
            minDateFilter = "";
            maxDateFilter = "";

            $.fn.dataTableExt.afnFiltering.push(
                function(oSettings, aData, iDataIndex) {

                    var myDate=aData[5].substring(0, 10).split("-");
                    var newDate=myDate[0]+"/"+myDate[1]+"/"+myDate[2];
                    newDate = new Date(newDate).getTime();
                    console.log(newDate);
                    console.log(minDateFilter);
                    console.log(maxDateFilter);
                    if (typeof newDate == 'undefined') {
                        newDate = new Date(aData[0]).getTime();
                    }

                    if (minDateFilter && !isNaN(minDateFilter)) {
                        if (newDate < minDateFilter) {
                            return false;
                        }
                    }

                    if (maxDateFilter && !isNaN(maxDateFilter)) {
                        if (newDate > maxDateFilter) {
                            return false;
                        }
                    }

                    return true;
                }
            );

        }

        if(pathname.split("/")[4] == 13 && pathname.split("/")[5] == 60) {

            $(function() {
                cTable = $('.cad-transaction-table').dataTable({
                    "oLanguage": {
                        "sSearch": "Search all columns:"
                    }
                });

                $("#transaction_date_from").datepicker({
                    showOn: "button",
                    changeYear: true,
                    buttonImageOnly: false,
                    "onSelect": function(date) {
                        minDateFilter = new Date(date).getTime();
                        cTable.fnDraw();
                    }
                }).keyup(function() {
                    minDateFilter = new Date(this.value).getTime();
                    cTable.fnDraw();
                });

                $("#transaction_date_to").datepicker({
                    showOn: "button",
                    changeYear: true,
                    buttonImageOnly: false,
                    "onSelect": function(date) {
                        maxDateFilter = new Date(date).getTime();
                        cTable.fnDraw();
                    }
                }).keyup(function() {
                    maxDateFilter = new Date(this.value).getTime();
                    cTable.fnDraw();
                });

                $("#reader-list").change('on', function() {
                    let filter = $(this).val();

                    cTable.fnFilter( filter, 2 );
                });

            });

            // Date range filter
            minDateFilter = "";
            maxDateFilter = "";

            $.fn.dataTableExt.afnFiltering.push(
                function(oSettings, aData, iDataIndex) {

                    var myDate=aData[1].substring(0, 10).split("/");
                    var newDate=myDate[0]+"/"+myDate[1]+"/"+myDate[2];
                    newDate = new Date(newDate).getTime();
                    console.log(newDate);
                    console.log(minDateFilter);
                    console.log(maxDateFilter);
                    if (typeof newDate == 'undefined') {
                        newDate = new Date(aData[0]).getTime();
                    }

                    if (minDateFilter && !isNaN(minDateFilter)) {
                        if (newDate < minDateFilter) {
                            return false;
                        }
                    }

                    if (maxDateFilter && !isNaN(maxDateFilter)) {
                        if (newDate > maxDateFilter) {
                            return false;
                        }
                    }

                    return true;
                }
            );
        }

        if(pathname.split("/")[4] == 6 && pathname.split("/")[5] == 68) {
            
            $(function() {
                oTable = $('.canadian-data-table').dataTable({
                    "order": [[ 0, "desc" ]]
                });

                $("#datepicker_from").datepicker({
                    showOn: "button",
                    changeYear: true,
                    buttonImageOnly: false,
                    "onSelect": function(date) {
                    minDateFilter = new Date(date).getTime();
                    oTable.fnDraw();
                    }
                }).keyup(function() {
                    minDateFilter = new Date(this.value).getTime();
                    oTable.fnDraw();
                });

                $("#datepicker_to").datepicker({
                    showOn: "button",
                    changeYear: true,
                    buttonImageOnly: false,
                    "onSelect": function(date) {
                    maxDateFilter = new Date(date).getTime();
                    oTable.fnDraw();
                    }
                }).keyup(function() {
                    maxDateFilter = new Date(this.value).getTime();
                    oTable.fnDraw();
                });

            });

            // Date range filter
            minDateFilter = "";
            maxDateFilter = "";

            $.fn.dataTableExt.afnFiltering.push(
                function(oSettings, aData, iDataIndex) {
                    
                    var myDate=aData[3].substring(0, 10).split("-");
                    var newDate=myDate[0]+"/"+myDate[1]+"/"+myDate[2];
                    newDate = new Date(newDate).getTime();
                    console.log(newDate);
                    console.log(minDateFilter);
                    console.log(maxDateFilter);
                    if (typeof newDate == 'undefined') {
                        newDate = new Date(aData[0]).getTime();
                    }

                    if (minDateFilter && !isNaN(minDateFilter)) {
                        if (newDate < minDateFilter) {
                            return false;
                        }
                    }

                    if (maxDateFilter && !isNaN(maxDateFilter)) {
                        if (newDate > maxDateFilter) {
                            return false;
                        }
                    }

                    return true;
                }
            );

        } else {

            if(pathname.split("/")[4] == 6 && pathname.split("/")[5] == 69) {

                $('.week-day').each(function () {
                    switch($(this).html()) {
                        case '1':
                            $(this).html('Monday')
                            break;
                        case '2':
                            $(this).html('Tuesday')
                            break;
                        case '3':
                            $(this).html('Wednesday')
                            break;
                        case '4':
                            $(this).html('Thursday')
                            break;
                        case '5':
                            $(this).html('Friday')
                            break;
                        case '6':
                            $(this).html('Saturday')
                            break;
                        case '7':
                            $(this).html('Sunday')
                            break;
                    }
                });

                $('.status-givetime').each(function () {

                    switch ($(this).html()) {
                        case '1':
                            $(this).html('Completed')
                            break;
                        case '0':
                            $(this).html('Pending')
                            break;
                    }
                })
            }

            if(pathname.split("/")[5] == 22) {


                table = $(".custom-data-table").DataTable(
                    {
                        "search": {
                            "caseInsensitive": false
                        }
                    }
                );
                $(".dataTables_filter").css("display", "none");
            } else {

                table = $(".custom-data-table").DataTable();
            }


            var search_filter_option = 1;

            $("#date-picker").datepicker({
                format: 'mm/dd/yyyy',
                changeYear: true,
                yearRange: "-30:+0",
                onSelect: function(dateText) {
                    table.column(search_filter_option)
                        .search(dateText)
                        .draw();
                }
            });

            $("#date-picker-dob").datepicker({
                dateFormat: 'yy-mm-dd',
                changeYear: true,
                yearRange: "-70:+0",
                onSelect: function(dateText) {
                    table.column(search_filter_option)
                        .search(dateText)
                        .draw();
                }
            });

            $('select#filter-option').on('change', function() {
                table
                    .search( '' )
                    .columns().search( '' )
                    .draw();

                if($(this).val() == 5) {

                    table.destroy()

                    table = $(".custom-data-table").DataTable(
                        {
                            "search": {
                                "caseInsensitive": false
                            }
                        }
                    );
                }

                $("#country-list").val($("#country-list option:first").val());

                $("#date-picker").datepicker('setDate', null);
                $("#date-picker-dob").datepicker('setDate', null);
                $("#gender input").prop('checked', false);

                search_filter_option = this.value;

                if(search_filter_option == 11) {
                    $("#date-picker").css("display", "none");
                    $("#date-picker-dob").css("display", "none");
                    $('#gender').css("display", "none");
                    $("#country-list").css("display", "block");
                    $(".input-sm").prop('disabled', true);

                } else if(search_filter_option == 13) {
                    $("#country-list").css("display", "none");
                    $("#date-picker").css("display", "block");
                    $("#date-picker-dob").css("display", "none");
                    $('#gender').css("display", "none");
                    $(".input-sm").prop('disabled', true);

                } else if(search_filter_option == 4) {

                    $("#country-list").css("display", "none");
                    $("#date-picker").css("display", "none");
                    $("#date-picker-dob").css("display", "block");
                    $('#gender').css("display", "none");
                    $(".input-sm").prop('disabled', true);

                } else if(search_filter_option == 5) {

                    $("#country-list").css("display", "none");
                    $("#date-picker").css("display", "none");
                    $("#date-picker-dob").css("display", "none");
                    $('#gender').css("display", "block");
                    $(".input-sm").prop('disabled', true);

                } else {

                    $("#country-list").css("display", "none");
                    $("#date-picker").css("display", "none");
                    $("#date-picker-dob").css("display", "none");
                    $('#gender').css("display", "none");
                    $(".input-sm").prop('disabled', false);
                }
            });


            $(".input-sm").on("keyup", function () {

                table.column(search_filter_option)
                    .search(this.value)
                    .draw();
                console.log(search_filter_option);
            });

            $("#date-picker").on("keyup", function () {

                if(this.value == "") {
                    table
                        .search( '' )
                        .columns().search( '' )
                        .draw();
                }

                table.column(search_filter_option)
                    .search(this.value)
                    .draw();
                console.log(search_filter_option);
            });

            $("#date-picker-dob").on("keyup", function () {

                if(this.value == "") {
                    table
                        .search( '' )
                        .columns().search( '' )
                        .draw();
                }

                table.column(4)
                    .search(this.value)
                    .draw();
                console.log(search_filter_option);
            });

            $("#country-list").change(function () {

                var country = $(this).val();

                table.column(search_filter_option)
                    .search(country)
                    .draw();
            });

            $("#gender input").change(

                function(){

                    table
                        .search( '' )
                        .columns().search( '' )
                        .draw();

                    let search_key = $(this).val();

                    // table.column(5)
                    //     .search(search_key, true, false)
                    //     .draw();

                    table.search(search_key,true, false).draw();
                }
            );

            // $("#ban-all").click(function () {
            //     confirm("Are You Sure?");
            // });
            //
            // $("#un-ban-all").click(function () {
            //     confirm("Are You Sure?");
            // });

            var gender_array = [];

            $("#gender-ban input").change(function () {
                if ($(this).is(':checked')) {

                    table
                        .search( '' )
                        .columns().search( '' )
                        .draw();

                    gender_array.push($(this).val());
                    let search_key = "";
                    for(let i=0; i<gender_array.length; i++) {
                        if(i==gender_array.length-1) {
                            search_key = search_key + gender_array[i];
                        } else {
                            search_key = search_key + gender_array[i] + "|";
                        }
                    }
                    search_key.slice(0, search_key.length - 1);
                    console.log(search_key);
                    table.search(search_key,true, false).draw();

                } else {

                    table
                        .search( '' )
                        .columns().search( '' )
                        .draw();

                    gender_array.splice( gender_array.indexOf($(this).val()), 1 );
                    let search_key = "";
                    for(let i=0; i<gender_array.length; i++) {
                        if(i==gender_array.length-1) {
                            search_key = search_key + gender_array[i];
                        } else {
                            search_key = search_key + gender_array[i] + "|";
                        }
                    }
                    search_key.slice(0, search_key.length - 1);
                    console.log(search_key);
                    table.search(search_key,true, false).draw();
                }
            });
        }

        if($('p.record-delete').html()) {
            $.notify($('p.record-delete').html(), {className:"error",align:"center", verticalAlign:"top", hideDuration: 700, autoHideDelay: 5000,});

            $.ajax({

                url: '/admin/pages/remove_session_data',
                type: 'GET',

            }).done(function () {
                console.log('removed session data');
            }).fail(function (error) {
                console.error(error);
            });
        }

        if($('p.record-save').html()) {
            $.notify($('p.record-save').html(), {className:"success",align:"center", verticalAlign:"top", hideDuration: 700, autoHideDelay: 5000,});
        }

        if($('p.record').html()) {
            $.notify($('p.record').html(), {className:"success",align:"center", verticalAlign:"top", hideDuration: 700, autoHideDelay: 5000,});

            $.ajax({

                url: '/admin/pages/remove_session_data',
                type: 'GET',

            }).done(function () {
                console.log('removed session data');
            }).fail(function (error) {
                console.error(error);
            });
        }
        


    });
</script>