<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add Item
                        <small>Create new item information</small>
                    </h5>
                <div class="hr-line-dashed"></div>
                </div>
                <div class="ibox-content">
                    <div class="row">         

					<?php echo form_open('admin/page/add',array("class"=>"form-horizontal")); ?>

						<div class="form-group">
							<label for="title" class="col-sm-2 control-label"><span class="text-danger">*</span>Title</label>
							<div class="col-md-8">
								<input type="text" name="title" value="<?php echo $this->input->post('title'); ?>" class="form-control" id="title" />
								<span class="text-danger"><?php echo form_error('title');?></span>
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-sm-2 control-label"><span class="text-danger">*</span>Content</label>
							<div class="col-md-8">
								<textarea name="content" class="form-control" id="content"><?php echo $this->input->post('content'); ?></textarea>
								<span class="text-danger"><?php echo form_error('content');?></span>
							</div>
						</div>

						<div class="form-group">
							<label for="url" class="col-sm-2 control-label"><span class="text-danger">*</span>Url</label>
							<div class="col-md-8">
								<input type="text" name="url" value="<?php echo $this->input->post('url'); ?>" class="form-control" id="url" />
								<span class="text-danger"><?php echo form_error('url');?></span>
							</div>
						</div>
						<div class="form-group">
							<label for="add_to_footer" class="col-sm-2 control-label">Add To Footer</label>
							<div class="col-md-8">
								<input type="text" name="add_to_footer" value="<?php echo $this->input->post('add_to_footer'); ?>" class="form-control" id="add_to_footer" />
							</div>
						</div>
						<div class="form-group">
							<label for="add_to_homepage" class="col-sm-2 control-label">Add To Homepage</label>
							<div class="col-md-8">
								<input type="text" name="add_to_homepage" value="<?php echo $this->input->post('add_to_homepage'); ?>" class="form-control" id="add_to_homepage" />
							</div>
						</div>
						<div class="form-group">
							<label for="sort" class="col-sm-2 control-label">Sort</label>
							<div class="col-md-2">
								<input type="text" name="sort" value="<?php echo $this->input->post('sort'); ?>" class="form-control" id="sort" />
							</div>
						</div>

						
						<div class="form-group">
							<div class="col-sm-offset-4 col-sm-8">
								<button type="submit" class="btn btn-success">Save</button>
					        </div>
						</div>

						<?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    





