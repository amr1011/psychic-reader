<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Page List</small></h5>              
          
                    </div>
                    <div class="ibox-content">
                    <div class="ibox-content m-b-sm">
                            <div class="row">                                                                                            
                                <a href="<?php echo site_url('admin/page/add'); ?>" class="btn btn-success"> <i class="fa fa-plus"></i> Add Page</a> 
                            </div>
                        </div>
                        <table class="table table-striped table-bordered">
					    <tr>
							<th>Id</th>
							<th>Page Title</th>
							<th>Page URL</th>
							<th>Actions</th>
					    </tr>
						<?php foreach($pages as $p){ ?>
					    <tr>
							<td><?php echo $p['id']; ?></td>
							<td><?php echo $p['title']; ?></td>
							<td><?php echo $p['url']; ?></td>
							<td>
					            <a href="<?php echo site_url('admin/page/edit/'.$p['id']); ?>" class="btn btn-info btn-xs">Edit</a> 
					            <a href="<?php echo site_url('admin/page/remove/'.$p['id']); ?>" class="btn btn-danger btn-xs">Delete</a>
					        </td>
					    </tr>
						<?php } ?>
					</table>
                    </div>
                </div>
            </div>
      </div>
</div>
