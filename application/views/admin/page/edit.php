<?php echo form_open('page/edit/'.$page['id'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="title" class="col-md-4 control-label"><span class="text-danger">*</span>Title</label>
		<div class="col-md-8">
			<input type="text" name="title" value="<?php echo ($this->input->post('title') ? $this->input->post('title') : $page['title']); ?>" class="form-control" id="title" />
			<span class="text-danger"><?php echo form_error('title');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="url" class="col-md-4 control-label"><span class="text-danger">*</span>Url</label>
		<div class="col-md-8">
			<input type="text" name="url" value="<?php echo ($this->input->post('url') ? $this->input->post('url') : $page['url']); ?>" class="form-control" id="url" />
			<span class="text-danger"><?php echo form_error('url');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="add_to_footer" class="col-md-4 control-label">Add To Footer</label>
		<div class="col-md-8">
			<input type="text" name="add_to_footer" value="<?php echo ($this->input->post('add_to_footer') ? $this->input->post('add_to_footer') : $page['add_to_footer']); ?>" class="form-control" id="add_to_footer" />
		</div>
	</div>
	<div class="form-group">
		<label for="add_to_homepage" class="col-md-4 control-label">Add To Homepage</label>
		<div class="col-md-8">
			<input type="text" name="add_to_homepage" value="<?php echo ($this->input->post('add_to_homepage') ? $this->input->post('add_to_homepage') : $page['add_to_homepage']); ?>" class="form-control" id="add_to_homepage" />
		</div>
	</div>
	<div class="form-group">
		<label for="sort" class="col-md-4 control-label">Sort</label>
		<div class="col-md-8">
			<input type="text" name="sort" value="<?php echo ($this->input->post('sort') ? $this->input->post('sort') : $page['sort']); ?>" class="form-control" id="sort" />
		</div>
	</div>
	<div class="form-group">
		<label for="content" class="col-md-4 control-label"><span class="text-danger">*</span>Content</label>
		<div class="col-md-8">
			<textarea name="content" class="form-control" id="content"><?php echo ($this->input->post('content') ? $this->input->post('content') : $page['content']); ?></textarea>
			<span class="text-danger"><?php echo form_error('content');?></span>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>