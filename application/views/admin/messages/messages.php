<style>
    div.content-notification-folder {
        padding-right: 20px;
        margin-top: 20px;
    }

    div.content-notification-messages {

    }

    span.folder-icon {
        font-size: 25px;
        color: #ece12c;
    }

    span.folder-name {
        font-size: 16px;
        font-family: Monospace;
        color: #1ab394;
    }

    div.notification-folder {
        cursor: pointer;
        padding-bottom: 5px;
    }

    .disabled {
        color: #cecbcb !important;
    }

    .disabled:hover {
        cursor: not-allowed;
    }

    tr.unread td {
        color: red;
    }

    tr.unread a {
        color: red;
    }

    span.unread {
        width: 20px;
        height: 20px;
        background: #e670c7;
        border-radius: 10px;
        padding: 3px 7px;
        color: white;
    }

    .ibox-content {
        height: 1000px;
    }

    ul.nav-tabs {
        font-size: 14px;
        border-bottom: none;
    }

    ul.nav-tabs li > a {
        color: black !important;
        font-family: Monospace;
    }

    ul.nav-tabs li.active > a {
        color: white !important;
        font-family: Monospace;
        background: black !important;
    }

</style>
<div class="wrapper wrapper-content animated fadeInRight" style="font-size: 18px;">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">

                    <h2><?= $title ?></h2>
                    <ul class="nav nav-tabs">
                        <li <?= ($this->uri->segment('3') == '' ? " class=\"active\"" : "") ?>><a
                                    href="/admin/message_management">Inbox</a></li>
                        <li <?= ($this->uri->segment('3') == 'outbox' ? " class=\"active\"" : "") ?>><a
                                    href="/admin/message_management/outbox">Outbox</a></li>
                        <li <?= ($this->uri->segment('3') == 'compose' ? " class=\"active\"" : "") ?>><a
                                    href="/admin/message_management/compose"><span class='icon icon-comment'></span>
                                Compose A New Message</a></li>
                    </ul>

                    <div class="content-block-60" style="display: inline-flex; width: 100%;">
                        <div class="content-notification-folder" style="width: 25%;">
                            <div class="main-folders">
                                <?php foreach ($senders as $sender) { ?>
                                    <div class="notification-folder message-folder">
                                        <?php if ($this->member->data['member_type'] == 'READER') { ?>
                                            <?php if ($sender['banned'] && $sender['banned'] == 1) { ?>
                                                <span class="fa fa-folder folder-icon disabled"></span>
                                                <span class="folder-name disabled sender-id"
                                                      style="display: none;"><?= $sender['sender_id'] ?></span>
                                                <span class="folder-name disabled show-folder-name"><?= $sender['username'] . ' (' . $sender['first_name'] . ')' ?></span>
                                            <?php } else { ?>
                                                <span class="fa fa-folder folder-icon enabled"><input type="hidden"
                                                                                                      class="folder-status"
                                                                                                      value="0"></span>
                                                <span class="folder-name enabled sender-id"
                                                      style="display: none;"><?= $sender['sender_id'] ?></span>
                                                <span class="folder-name enabled show-folder-name"><?= $sender['username'] . ' (' . $sender['first_name'] . ')' ?></span>
                                                <?php if ($sender['unread'] > 0) { ?>
                                                    <span class="unread enabled"><?= $sender['unread']; ?></span>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <span class="fa fa-folder folder-icon enabled"><input type="hidden"
                                                                                                  class="folder-status"
                                                                                                  value="0"></span>
                                            <span class="folder-name enabled sender-id"
                                                  style="display: none;"><?= $sender['sender_id'] ?></span>
                                            <span class="folder-name enabled show-folder-name"><?= $sender['username'] . ' (' . $sender['first_name'] . ')' ?></span>
                                            <?php if ($sender['unread'] > 0) { ?>
                                                <span class="unread enabled"><?= $sender['unread']; ?></span>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="content-notification-messages"
                             style="width: 75%;max-height: 840px;overflow-y: auto;">
                            <div class="box-message"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="/theme/admin/js/jquery-3.1.1.min.js"></script>
<script>
    function insertMessage(messages) {
        console.log('messages' + messages);
        let view = "<div class='table-responsive'><table width='80%' style='font-size: 15px; font-family: Monospace; color: black' cellPadding='0' cellSpacing='0' class='table table-striped table-hover'>";
        var member = null;
        for (let i = 0; i < messages.length; i++) {

            let class_name = 'read';
            if (messages[i]['read'] == 0) {
                class_name = 'unread';
            }

            view = view + "<tr class=" + class_name + ">" + "<td width='250'>" + messages[i]['datetime'] + " EST</td>"
                + "<td>" + messages[i]['subject'] + "</td>"
                + "<td width='50' align='center'><a href='/admin/message_management/view/" + messages[i]['id'] + "' class='btn btn-primary'>View</a></td>"
                + "<td width='50' align='center'><a href='/admin/message_management/delete/" + messages[i]['id'] + "' onClick=\"Javascript:return confirm('Are you sure you want to delete this message?');\" class='btn btn-danger'>Delete</a></td>"
                + "</tr>";
        }
        view = view + "</table></ div>";
        return view;
    }

    $(document).ready(function () {

        var messages;
        var admin_messages;
        var system_messages;

        $.ajax({
            'type': "POST",
            'url': "/my_account/messages/getSystemMessages",
            'data': {},
            success: function (data) {

                let obj = JSON.parse(data);
                system_messages = obj.messages;

                setTimeout(function () {

                    let view = insertMessage(system_messages);
                    $(".box-message").append(view);
                }, 500);

            }
        });

        function getData(sender_id) {
            $.ajax({
                'type': "POST",
                'url': "/my_account/messages/getMessages",
                'data': {user_id: sender_id},
                success: function (data) {

                    let obj = JSON.parse(data);
                    messages = obj.messages;
                    console.log(messages);
                }
            });
        }

        function getAdminData() {
            $.ajax({
                'type': "POST",
                'url': "/my_account/messages/getAdminMessages",
                'data': {},
                success: function (data) {

                    let obj = JSON.parse(data);
                    admin_messages = obj.messages;
                    console.log(admin_messages);
                }
            });
        }

        $(".message-folder").click(function () {

            let sender_id = $(this).find('.sender-id').html();
            getData(sender_id);

            $(".box-message").empty();
            let folder_status = $(this).find(".folder-status").val();
            if (folder_status == 0) {

            } else {
                setTimeout(function () {
                    let view = insertMessage(messages);
                    $(".box-message").append(view);
                }, 500);
            }
        });

        $(".admin-folder").click(function () {

            getAdminData();

            $(".box-message").empty();
            let folder_status = $(this).find(".folder-status").val();
            if (folder_status == 0) {

            } else {
                setTimeout(function () {
                    let view = insertMessage(admin_messages);
                    $(".box-message").append(view);
                }, 500);
            }
        });

        // folder open/close
        $(".notification-folder .enabled").click(function () {

            let folder_status = $(this).parent().find(".folder-status").val();
            if (folder_status == 0) {
                $(".fa-folder-open").removeClass("fa-folder-open").addClass("fa-folder");
                $(".folder-status").val(0);
                $(".folder-name").css('color', '#4c4b4b');
                $(this).parent().find(".fa-folder").removeClass("fa-folder").addClass("fa-folder-open");
                $(this).parent().find(".folder-name").css('color', '#337ab7');
                $(this).parent().find(".folder-status").val(1);

                // show contained messages
            } else {
                $(this).parent().find(".fa-folder-open").removeClass("fa-folder-open").addClass("fa-folder");
                $(this).parent().find(".folder-status").val(0);
                $(".folder-name").css('color', '#1ab394');
            }
        });
    });
</script>




