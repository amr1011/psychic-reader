<style>
    div.well {
        font-size: 16px;
        font-family: monospace;
        margin-top: 5px;
    }

    div.content {
        padding: 20px;
        font-size: 16px;
        line-height: 10px;
        font-family: monospace;
        border: 1px solid #95c7b2;
        border-radius: 4px;
    }

    ul.nav-tabs {
        font-size: 14px;
        border-bottom: none;
    }

    ul.nav-tabs li > a {
        color: black !important;
        font-family: Monospace;
    }

    ul.nav-tabs li.active > a {
        color: white !important;
        font-family: Monospace;
        background: black !important;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight" style="font-size: 18px;">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <h2>Messages(View)</h2>
                    <ul class="nav nav-tabs">
                        <li <?= ($this->uri->segment('3') == '' ? " class=\"active\"" : "") ?>><a
                                    href="/admin/message_management">Inbox</a></li>
                        <li <?= ($this->uri->segment('3') == 'outbox' ? " class=\"active\"" : "") ?>><a
                                    href="/admin/message_management/outbox">Outbox</a></li>
                        <li <?= ($this->uri->segment('3') == 'compose' ? " class=\"active\"" : "") ?>><a
                                    href="/admin/message_management/compose"><span class='icon icon-comment'></span>
                                Compose A New Message</a></li>
                    </ul>

                    <div class='well'>
                        From: <?= $from['first_name'] ?> <?= $from['last_name'] ?><br/>
                        To: <?= $to['first_name'] ?> <?= $to['last_name'] ?><br/>
                        Sent: <?= date("M d, Y @ h:i A", strtotime($datetime)) ?> EST
                    </div>
                    <div class="content">
                        <?= html_entity_decode($message) ?>
                    </div>

                    <?php if ($file) : ?>
                        <a href="/uploads/<?php echo($file); ?>" download>
                            <img src="/uploads/<?php echo($file); ?>" style="width:100px;"/>
                        </a>
                    <?php endif; ?>
                    <a href='/admin/message_management/compose/<?= $id ?>' class='btn btn-primary'
                       style="margin-top: 10px;">Send A Reply</a>
                </div>

            </div>

        </div><!-- .content-block-80 -->

    </div><!-- .my-container -->
</div>