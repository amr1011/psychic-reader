<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    .switch input {
        display: none;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #337ab7;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #337ab7;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }

    ul.nav-tabs {
        font-size: 14px;
        border-bottom: none;
    }

    ul.nav-tabs li > a {
        color: black !important;
        font-family: Monospace;
    }

    ul.nav-tabs li.active > a {
        color: white !important;
        font-family: Monospace;
        background: black !important;
    }

    label {
        font-size: 15px;
        color: #1ab394;
        font-family: cursive;
    }

    input[type="text"], select, textarea {
        border-radius: 4px !important;
        border: 1px #1ab394 solid !important;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight" style="font-size: 18px;">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content" style="height: 1000px;">
                    <h2>Compose A New Message</h2>

                    <ul class="nav nav-tabs">
                        <li <?= ($this->uri->segment('3') == '' ? " class=\"active\"" : "") ?>><a
                                    href="/admin/message_management">Inbox</a></li>
                        <li <?= ($this->uri->segment('3') == 'outbox' ? " class=\"active\"" : "") ?>><a
                                    href="/admin/message_management/outbox">Outbox</a></li>
                        <li <?= ($this->uri->segment('3') == 'compose' ? " class=\"active\"" : "") ?>><a
                                    href="/admin/message_management/compose"><span class='icon icon-comment'></span>
                                Compose A New Message</a></li>
                    </ul>


                    <form action='/admin/message_management/compose_submit' method='POST' class="form-horizontal"
                          enctype="multipart/form-data">

                        <div class='form-group compose-form-group' style="padding-top:15px;">
                            <label class='col-md-4 control-label' for="toggle-pause-timer">Send To Private Email</label>
                            <div class='col-md-8'>
                                <label class="switch" style="float: left;margin-right: 5px;">
                                    <input name="send-private" type="checkbox" id="send-private">
                                    <span class="slider round"></span>
                                </label>
                                <span id="switch-text"></span>
                            </div>
                        </div>

                        <div class='form-group compose-form-group' style="padding-top:15px;">

                            <label class='col-md-4 control-label'><b>Recipient</b></label>
                            <div class='col-md-8'>
                                <?php if ($this->uri->segment('4')) : ?>

                                    User: <?php echo $to_user['username'] . ' (' . $to_user['first_name'] . ')'; ?>
                                    <input type='hidden' name='to' value='<?php echo $to ?>' class='input-mini'>
                                <?php else: ?>


                                    <select name='to' class='form-control'>
                                        <option value=''></option>

                                        <?php foreach ($users as $u) : ?>

                                            <option value='<?php echo $u['id']; ?>' <?php echo set_select('to', $u['id'], ($to == $u['id'] ? TRUE : FALSE)) ?>><?php echo $u['username'] . '  (' . $u['first_name'] . ')'; ?></option>
                                        <?php endforeach; ?>


                                    </select>

                                <?php endif; ?>
                            </div>
                        </div>
                        <div class='form-group compose-form-group'>
                            <label class='col-md-4 control-label'><b>Subject</b></label>
                            <div class="col-md-8">
                                <input type='text' class='form-control' name='subject'
                                       value='<?= set_value('subject', $subject) ?>'>
                            </div>
                        </div>
                        <div class="form-group compose-form-group">
                            <label class='col-md-4 control-label'><b>Message</b></label>
                            <div class='col-md-8'>
                                <textarea name='message' class='form-control'
                                          style='width:100%;height:150px;'><?= set_value('message', $message) ?></textarea>
                            </div>
                        </div>

                        <div class="col-lg-12" style="text-align: center">
                            <input type="file" name="attach" id="attach" style="color: green; font-size: 15px;"/>
                            <input type='submit' name='submit' style="margin-left:15px;" value='Send Message'
                                   class='btn btn-large btn-primary'>
                        </div>

                    </form>
                </div>
            </div>
        </div><!-- .content-block-80 -->
    </div><!-- .my-container -->
</div>
<script src="/theme/admin/js/jquery-3.1.1.min.js"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=e465y2qbjbdi14qglxeq486et6jfhc6d0zy21wr3akkqps42"></script>
<script>tinymce.init({
        selector: 'textarea',
        height: 350,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor link image code",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste imagetools wordcount", 'image code textcolor colorpicker',
        ],
        relative_urls: false,
        remove_script_host: false,
        convert_urls: true,
        toolbar: "insertfile | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor",
        // imagetools_cors_hosts: ['www.tinymce.com', 'codepen.io'],
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ],
        automatic_uploads: true,
        imagetools_cors_hosts: ['mydomain.com', 'otherdomain.com'], paste_data_images: true,
        file_picker_callback: function (cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');

            // Note: In modern browsers input[type="file"] is functional without
            // even adding it to the DOM, but that might not be the case in some older
            // or quirky browsers like IE, so you might want to add it to the DOM
            // just in case, and visually hide it. And do not forget do remove it
            // once you do not need it anymore.

            input.onchange = function () {
                var file = this.files[0];

                var reader = new FileReader();
                reader.onload = function () {
                    // Note: Now we need to register the blob in TinyMCEs image blob
                    // registry. In the next release this part hopefully won't be
                    // necessary, as we are looking to handle it internally.
                    var id = 'blobid' + (new Date()).getTime();
                    var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                    var base64 = reader.result.split(',')[1];
                    var blobInfo = blobCache.create(id, file, base64);
                    blobCache.add(blobInfo);

                    // call the callback and populate the Title field with the file name
                    cb(blobInfo.blobUri(), {title: file.name});
                };
                reader.readAsDataURL(file);
            };

            input.click();
        }
    });</script>
<script>
    $(document).ready(function () {
        $('input[type=file]').change(function () {

            console.log(document.getElementById("attach").files[0].name);

        });

        $("#switch-text").text("Off").css('color', 'black');

        $('#send-private').change(function () {

            if (this.checked) {

                $("#switch-text").text("On").css('color', '#337ab7');
                $("#send-private").val(1);
            } else {

                $("#switch-text").text("Off").css('color', 'black');
                $("#send-private").val(0);
            }

        });
    })
</script>