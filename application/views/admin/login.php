<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Psychic Contact Dashboard</title>
    <link href="/theme/admin/css/bootstrap.min.css" rel="stylesheet">
	<link href="/theme/admin/font-awesome/css/font-awesome.css" rel="stylesheet">    
	<link href="/theme/admin/css/animate.css" rel="stylesheet">
	<link href="/theme/admin/css/style.css" rel="stylesheet">		
</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <h4 class="logo-name">Devpsy</h4>
            </div>
            <h3></h3>
            <p>Loging to access your account</p>
            <p>
            <?php
		
				//if(validation_errors()) echo validation_errors('<div class=\'errors\'>','</div>');
					//if($this->error) echo '<div class=\'errors\'>'.$this->error.'</div>';
		
			?>
			</p>
	
            <form class="m-t" role="form"   method='POST' action='/admin/login/submit'>
                <div class="form-group">
                    <input type="username" class="form-control" placeholder="Username" name="username" required="">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password"  name="password" required="">
                </div>
                <input type='submit' name='submit' value='Login ' class="btn btn-primary block full-width m-b"></div>

                <a href="#"><small>Forgot password?</small></a>
            </form>
            <p class="m-t"> <small>Psychic Contact &copy; 2014</small> </p>
        </div>
    </div>
    <!-- Mainly scripts -->
    <script src="/theme/admin/js/jquery-3.1.1.min.js"></script>
    <script src="/theme/admin/js/bootstrap.min.js"></script>

</body>

</html>



