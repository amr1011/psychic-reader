<style>
    ul.nav-tabs {
        font-size: 14px;
        border-bottom: none;
    }

    ul.nav-tabs li > a {
        color: black !important;
        font-family: Monospace;
    }

    ul.nav-tabs li.active > a {
        color: white !important;
        font-family: Monospace;
        background: black !important;
    }

    .well {
        font-size: 15px;
        font-family: Monospace;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight" style="font-size: 18px;">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div style='padding-bottom:25px;'>
                        <h2><?= $subject ?></h2>
                    </div>

                    <ul class="nav nav-tabs">
                        <li <?= ($this->uri->segment('3') == '' ? " class=\"active\"" : "") ?>><a
                                    href="/admin/system_email"><span class="fa fa-user"></span> NoBo</a></li>
                        <li <?= ($this->uri->segment('3') == 'email_setting' ? " class=\"active\"" : "") ?>><a
                                    href="/admin/system_email/email_setting"><span class='fa fa-gear'></span>
                                Setting</a></li>
                    </ul>

                    <div class='well'>
                        <div>From: <?= $v_from ?><br/></div>
                        <div>To:&nbsp;&nbsp;&nbsp;<?= $to['first_name'] ?> <?= $to['last_name'] ?><br/></div>
                        <div>Date: <?= date("M d, Y @ h:i A", strtotime($datetime)) ?> EST</div>
                        <?= nl2br($message) ?>

<!--                        <a href='/my_account/messages/compose/--><?//= $id ?><!--' class='btn btn-primary'>Send A Reply</a>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
	