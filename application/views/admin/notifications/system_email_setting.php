<style>
    .btn-box {
        margin-top: 10px;
        margin-bottom: 5px;
    }

    td, th {
        text-align: center;
    }

    #save-email-setting {
        position: relative;
        left: 88%;
        width: 100px;
    }

    /*spinner*/
    .sk-circle {
        margin: 100px auto;
        width: 40px;
        height: 40px;
        position: relative;
    }

    .sk-circle .sk-child {
        width: 100%;
        height: 100%;
        position: absolute;
        left: 0;
        top: 0;
    }

    .sk-circle .sk-child:before {
        content: '';
        display: block;
        margin: 0 auto;
        width: 15%;
        height: 15%;
        background-color: #333;
        border-radius: 100%;
        -webkit-animation: sk-circleBounceDelay 1.2s infinite ease-in-out both;
        animation: sk-circleBounceDelay 1.2s infinite ease-in-out both;
    }

    .sk-circle .sk-circle2 {
        -webkit-transform: rotate(30deg);
        -ms-transform: rotate(30deg);
        transform: rotate(30deg);
    }

    .sk-circle .sk-circle3 {
        -webkit-transform: rotate(60deg);
        -ms-transform: rotate(60deg);
        transform: rotate(60deg);
    }

    .sk-circle .sk-circle4 {
        -webkit-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        transform: rotate(90deg);
    }

    .sk-circle .sk-circle5 {
        -webkit-transform: rotate(120deg);
        -ms-transform: rotate(120deg);
        transform: rotate(120deg);
    }

    .sk-circle .sk-circle6 {
        -webkit-transform: rotate(150deg);
        -ms-transform: rotate(150deg);
        transform: rotate(150deg);
    }

    .sk-circle .sk-circle7 {
        -webkit-transform: rotate(180deg);
        -ms-transform: rotate(180deg);
        transform: rotate(180deg);
    }

    .sk-circle .sk-circle8 {
        -webkit-transform: rotate(210deg);
        -ms-transform: rotate(210deg);
        transform: rotate(210deg);
    }

    .sk-circle .sk-circle9 {
        -webkit-transform: rotate(240deg);
        -ms-transform: rotate(240deg);
        transform: rotate(240deg);
    }

    .sk-circle .sk-circle10 {
        -webkit-transform: rotate(270deg);
        -ms-transform: rotate(270deg);
        transform: rotate(270deg);
    }

    .sk-circle .sk-circle11 {
        -webkit-transform: rotate(300deg);
        -ms-transform: rotate(300deg);
        transform: rotate(300deg);
    }

    .sk-circle .sk-circle12 {
        -webkit-transform: rotate(330deg);
        -ms-transform: rotate(330deg);
        transform: rotate(330deg);
    }

    .sk-circle .sk-circle2:before {
        -webkit-animation-delay: -1.1s;
        animation-delay: -1.1s;
    }

    .sk-circle .sk-circle3:before {
        -webkit-animation-delay: -1s;
        animation-delay: -1s;
    }

    .sk-circle .sk-circle4:before {
        -webkit-animation-delay: -0.9s;
        animation-delay: -0.9s;
    }

    .sk-circle .sk-circle5:before {
        -webkit-animation-delay: -0.8s;
        animation-delay: -0.8s;
    }

    .sk-circle .sk-circle6:before {
        -webkit-animation-delay: -0.7s;
        animation-delay: -0.7s;
    }

    .sk-circle .sk-circle7:before {
        -webkit-animation-delay: -0.6s;
        animation-delay: -0.6s;
    }

    .sk-circle .sk-circle8:before {
        -webkit-animation-delay: -0.5s;
        animation-delay: -0.5s;
    }

    .sk-circle .sk-circle9:before {
        -webkit-animation-delay: -0.4s;
        animation-delay: -0.4s;
    }

    .sk-circle .sk-circle10:before {
        -webkit-animation-delay: -0.3s;
        animation-delay: -0.3s;
    }

    .sk-circle .sk-circle11:before {
        -webkit-animation-delay: -0.2s;
        animation-delay: -0.2s;
    }

    .sk-circle .sk-circle12:before {
        -webkit-animation-delay: -0.1s;
        animation-delay: -0.1s;
    }

    @-webkit-keyframes sk-circleBounceDelay {
        0%, 80%, 100% {
            -webkit-transform: scale(0);
            transform: scale(0);
        }
        40% {
            -webkit-transform: scale(1);
            transform: scale(1);
        }
    }

    @keyframes sk-circleBounceDelay {
        0%, 80%, 100% {
            -webkit-transform: scale(0);
            transform: scale(0);
        }
        40% {
            -webkit-transform: scale(1);
            transform: scale(1);
        }
    }

    input[type=checkbox] {
        width: 14px;
        height: 14px;
    }

    ul.nav-tabs {
        font-size: 14px;
        border-bottom: none;
    }

    ul.nav-tabs li > a {
        color: black !important;
        font-family: Monospace;
    }

    ul.nav-tabs li.active > a {
        color: white !important;
        font-family: Monospace;
        background: black !important;
    }

    .table-responsive {
        font-size: 14px;
        font-family: monospace;
        color: black;
        padding: 0 30px 0 30px;
    }

    thead {
        color: red;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight" style="font-size: 18px;">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <h2><?= $title ?></h2>

                    <ul class="nav nav-tabs">
                        <li <?= ($this->uri->segment('3') == '' ? " class=\"active\"" : "") ?>><a
                                    href="/admin/system_email"><span class="fa fa-user"></span> NoBo</a></li>
                        <li <?= ($this->uri->segment('3') == 'email_setting' ? " class=\"active\"" : "") ?>><a
                                    href="/admin/system_email/email_setting"><span class='fa fa-gear'></span>
                                Setting</a></li>
                    </ul>

                    <?php
                    if ($messages) {
                        echo "<div class='btn-box'>
                                    <div class=\"sk-circle\" style=\"width: 60px;height: 60px; position: fixed; left: 60%; display: none;\">
                                    <div class=\"sk-circle1 sk-child\"></div>
                                    <div class=\"sk-circle2 sk-child\"></div>
                                    <div class=\"sk-circle3 sk-child\"></div>
                                    <div class=\"sk-circle4 sk-child\"></div>
                                    <div class=\"sk-circle5 sk-child\"></div>
                                    <div class=\"sk-circle6 sk-child\"></div>
                                    <div class=\"sk-circle7 sk-child\"></div>
                                    <div class=\"sk-circle8 sk-child\"></div>
                                    <div class=\"sk-circle9 sk-child\"></div>
                                    <div class=\"sk-circle10 sk-child\"></div>
                                    <div class=\"sk-circle11 sk-child\"></div>
                                    <div class=\"sk-circle12 sk-child\"></div>
                                </div>
                                <button class='btn btn-primary' id='save-email-setting'>Save</button></div>";
                        echo "<div class='table-responsive'><table width='100%' cellPadding='0' cellSpacing='0' class='table table-striped table-hover'><thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Private/select all <input type='checkbox' class='select-all-private'></th>
                                    <th>Trash/select all <input type='checkbox' class='select-all-trash'></th>
                                    <th>Create Folder/select all <input type='checkbox' class='select-all-has-folder'></th>
                                    <th style='display: none; width: 0px;'></th>
                                </tr>
                                </thead>
                                <tbody>
                        ";
                        $i = 1;
                        foreach ($messages as $m) {
                            if (strtotime($m['created_at']) > strtotime(date("Y-m-d"))) {
                                echo "
                                    <tr>
                                        <td>{$i}</td>
                                        <td class='email-name' style='font-weight: bold; font-size: 18px; color: dodgerblue;'>{$m['name']}</td>
                                        <td><input type='checkbox' class='email-private'></td>
                                        <td><input type='checkbox' class='email-trash'></td>
                                        <td><input type='checkbox' class='email-has-folder'></td>
                                        <td style='display:none;'><input type='hidden' class='email-setting-type'/></td>
                                        <td style='display:none;'><input type='hidden' class='email-setting-has-folder'/></td>
                                    </tr>";
                            } else {
                                echo "
                                    <tr>
                                        <td>{$i}</td>
                                        <td class='email-name' style='display: none;'>{$m['name']}</td>
                                        <td class='email-name-show'></td>
                                        <td><input type='checkbox' class='email-private'></td>
                                        <td><input type='checkbox' class='email-trash'></td>
                                        <td><input type='checkbox' class='email-has-folder'></td>
                                        <td style='display:none;'><input type='hidden' class='email-setting-type'/></td>
                                        <td style='display:none;'><input type='hidden' class='email-setting-has-folder'/></td>
                                    </tr>";
                            }

                            $i++;
                        }
                        echo "</tbody></table></div>";
                    } else {
                        echo "<p>You do not have any messages</p>";
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/theme/admin/js/jquery-3.1.1.min.js"></script>
<script>
    $(document).ready(function () {

        // email-setting-type  1:private 2:trash 3:delete 4:both
        $(".email-private").change(function () {

            let private_checked = $(this).is(':checked');
            if (private_checked) {

                if ($(this).parent().parent().find(".email-trash").prop('checked') == true) {

                    $(this).parent().parent().find("input.email-setting-type").val(4); // both selected

                } else {

                    $(this).parent().parent().find("input.email-setting-type").val(1); // private selected
                }

            } else {

                if ($(this).parent().parent().find(".email-trash").prop('checked') == true) {

                    $(this).parent().parent().find("input.email-setting-type").val(2); // trash selected
                } else {

                    $(this).parent().parent().find("input.email-setting-type").val(3); // both unselected
                }

            }
        });

        $(".select-all-private").change(function () {

            let email_length = $("tbody tr").length;
            let private_checked = $(this).is(':checked');

            if (private_checked) {
                for (let i = 1; i <= email_length; i++) {
                    let trash_status = $("tbody tr:nth-child(" + i + ")").find(".email-trash").prop('checked');
                    if (trash_status == true) {
                        $("tbody tr:nth-child(" + i + ")").find("input.email-setting-type").val(4);
                    } else {
                        $("tbody tr:nth-child(" + i + ")").find("input.email-setting-type").val(1);
                    }
                }
                $(".email-private").prop('checked', true);
            } else {

                for (let i = 1; i <= email_length; i++) {
                    let trash_status = $("tbody tr:nth-child(" + i + ")").find(".email-trash").prop('checked');
                    if (trash_status == true) {
                        $("tbody tr:nth-child(" + i + ")").find("input.email-setting-type").val(2);
                    } else {
                        $("tbody tr:nth-child(" + i + ")").find("input.email-setting-type").val(3);
                    }
                }
                $(".email-private").prop('checked', false);
            }
        });

        $(".select-all-trash").change(function () {

            let email_length = $("tbody tr").length;
            let trash_checked = $(this).is(':checked');

            if (trash_checked) {
                for (let i = 1; i <= email_length; i++) {
                    let private_status = $("tbody tr:nth-child(" + i + ")").find(".email-private").prop('checked');
                    if (private_status == true) {
                        $("tbody tr:nth-child(" + i + ")").find("input.email-setting-type").val(4);
                    } else {
                        $("tbody tr:nth-child(" + i + ")").find("input.email-setting-type").val(2);
                    }
                }
                $(".email-trash").prop('checked', true);
            } else {
                for (let i = 1; i <= email_length; i++) {
                    let private_status = $("tbody tr:nth-child(" + i + ")").find(".email-private").prop('checked');
                    if (private_status == true) {
                        $("tbody tr:nth-child(" + i + ")").find("input.email-setting-type").val(1);
                    } else {
                        $("tbody tr:nth-child(" + i + ")").find("input.email-setting-type").val(3);
                    }
                }
                $(".email-trash").prop('checked', false);
            }
        });

        $(".email-trash").change(function () {

            let trash_checked = $(this).is(':checked');
            if (trash_checked) {
                if ($(this).parent().parent().find(".email-private").prop('checked') == true) {

                    $(this).parent().parent().find("input.email-setting-type").val(4);
                } else {
                    $(this).parent().parent().find("input.email-setting-type").val(2);
                }

            } else {
                if ($(this).parent().parent().find(".email-private").prop('checked') == true) {

                    $(this).parent().parent().find("input.email-setting-type").val(1);
                } else {
                    $(this).parent().parent().find("input.email-setting-type").val(3);
                }
            }
        });

        $(".email-has-folder").change(function () {

            let has_folder_checked = $(this).is(':checked');
            if (has_folder_checked) {
                $(this).parent().parent().find("input.email-setting-has-folder").val(1);
            } else {
                $(this).parent().parent().find("input.email-setting-has-folder").val(0);
            }
        });

        $(".select-all-has-folder").change(function () {

            let email_length = $("tbody tr").length;
            let has_folder_checked = $(this).is(':checked');

            if (has_folder_checked) {
                for (let i = 1; i <= email_length; i++) {
                    $("tbody tr:nth-child(" + i + ")").find("input.email-setting-has-folder").val(1);
                }
                $(".email-has-folder").prop('checked', true);
            } else {
                for (let i = 1; i <= email_length; i++) {
                    $("tbody tr:nth-child(" + i + ")").find("input.email-setting-has-folder").val(0);
                }
                $(".email-has-folder").prop('checked', false);
            }
        });

        $("#save-email-setting").on('click', function () {

            $(".sk-circle").css("display", "block");

            let email_length = $("tbody tr").length;
            let private_emails = [];
            let trash_emails = [];
            let canceled_emails = [];
            let has_folders = [];

            for (let i = 1; i <= email_length; i++) {
                let email_setting = $("tbody tr:nth-child(" + i + ")").find(".email-setting-type").val();
                let email_setting_has_folder = $("tbody tr:nth-child(" + i + ")").find(".email-setting-has-folder").val();
                if (email_setting == 1) {
                    private_emails.push($("tbody tr:nth-child(" + i + ")").find(".email-name").text());
                }
                if (email_setting == 2) {
                    trash_emails.push($("tbody tr:nth-child(" + i + ")").find(".email-name").text());
                }
                if (email_setting == 4) {
                    // canceled_emails.push($("tbody tr:nth-child(" + i + ")").find(".email-name").text());
                    private_emails.push($("tbody tr:nth-child(" + i + ")").find(".email-name").text());
                    trash_emails.push($("tbody tr:nth-child(" + i + ")").find(".email-name").text());
                }
                if (email_setting_has_folder == 1) {
                    has_folders.push($("tbody tr:nth-child(" + i + ")").find(".email-name").text());
                }
            }

            setTimeout(function () {
                $.ajax({
                    'type': "POST",
                    'url': "/my_account/system_email/save_email_setting",
                    'data': {
                        user_id: '<?=$this->member->data['id'];?>',
                        private: private_emails,
                        trash: trash_emails,
                        has_folders: has_folders
                    },
                    success: function (data) {
                        let obj = JSON.parse(data);
                        console.log(obj.private);
                        console.log(obj.trash);
                        $(".sk-circle").css("display", "none");
                    }
                });
            }, 1000);
        });

        $.ajax({
            'type': "POST",
            'url': "/my_account/system_email/get_email_setting",
            'data': {user_id: '<?=$this->member->data['id'];?>'},
            success: function (data) {
                let obj = JSON.parse(data);
                let private_emails = obj.private;
                let trash_emails = obj.trash;
                let has_folders = obj.has_folders;

                let count_private = 0;
                let count_trash = 0;
                let count_has_folders = 0;
                let email_length = $("tbody tr").length;

                for (let k = 1; k <= email_length; k++) {
                    let email_name = $("tbody tr:nth-child(" + k + ")").find(".email-name").text();
                    let array = email_name.split("_");
                    let email_name_show = "";
                    for (let n = 0; n < array.length; n++) {

                        email_name_show = email_name_show + array[n].charAt(0).toUpperCase() + array[n].slice(1) + " ";
                    }
                    $("tbody tr:nth-child(" + k + ")").find(".email-name-show").text(email_name_show);

                }

                for (let i = 0; i < private_emails.length; i++) {

                    for (let j = 1; j <= email_length; j++) {

                        if ($("tbody tr:nth-child(" + j + ")").find(".email-name").text() == private_emails[i]) {
                            $("tbody tr:nth-child(" + j + ")").find(".email-private").prop('checked', true);
                            count_private++;
                        }

                    }
                }
                for (let l = 0; l < trash_emails.length; l++) {

                    for (let k = 1; k <= email_length; k++) {

                        if ($("tbody tr:nth-child(" + k + ")").find(".email-name").text() == trash_emails[l]) {
                            $("tbody tr:nth-child(" + k + ")").find(".email-trash").prop('checked', true);
                            count_trash++;
                        }

                    }
                }
                for (let m = 0; m < has_folders.length; m++) {

                    for (let n = 1; n <= email_length; n++) {
                        if ($("tbody tr:nth-child(" + n + ")").find(".email-name").text() == has_folders[m]) {
                            $("tbody tr:nth-child(" + n + ")").find(".email-has-folder").prop('checked', true);
                            count_has_folders++;
                        }
                    }
                }
                if (count_private == email_length) {
                    $(".select-all-private").prop('checked', true);
                }
                if (count_trash == email_length) {
                    $(".select-all-trash").prop('checked', true);
                }
                if (count_has_folders == email_length) {
                    $(".select-all-has-folder").prop('checked', true);
                }
                setTimeout(function () {
                    for (let r = 1; r <= $("tbody tr").length; r++) {
                        let private_status = $("tbody tr:nth-child(" + r + ")").find(".email-private").prop('checked');
                        let trash_status = $("tbody tr:nth-child(" + r + ")").find(".email-trash").prop('checked');
                        let has_folder_status = $("tbody tr:nth-child(" + r + ")").find(".email-has-folder").prop('checked');
                        if (private_status && trash_status) {
                            $("tbody tr:nth-child(" + r + ")").find(".email-setting-type").val(4);
                        } else if (private_status && !trash_status) {
                            $("tbody tr:nth-child(" + r + ")").find(".email-setting-type").val(1);
                        } else if (!private_status && trash_status) {
                            $("tbody tr:nth-child(" + r + ")").find(".email-setting-type").val(2);
                        } else {
                            $("tbody tr:nth-child(" + r + ")").find(".email-setting-type").val(3);
                        }
                        if (has_folder_status) {
                            $("tbody tr:nth-child(" + r + ")").find(".email-setting-has-folder").val(1);
                        } else {
                            $("tbody tr:nth-child(" + r + ")").find(".email-setting-has-folder").val(0);
                        }
                    }
                }, 1000);
            }
        });
    })
</script>



