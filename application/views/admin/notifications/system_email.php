<style>
    div.content-notification-folder {
        padding-right: 20px;
        margin-top: 20px;
        border-right: solid 1px #cecbcb;
    }

    div.content-notification-messages {
    }

    span.folder-icon {
        font-size: 25px;
        color: #ece12c;
    }

    span.trash-icon {
        font-size: 25px;
        color: #e01e1e;
    }

    span.folder-name {
        font-size: 16px;
        font-family: Monospace;
        color: #1ab394;
    }

    div.notification-folder, .notification-trash-folder, .notification-inbox-folder {
        cursor: pointer;
        padding-bottom: 5px;
    }

    .disabled {
        color: #cecbcb !important;
        color: currentColor;
        cursor: not-allowed;
        text-decoration: none;
    }

    .trash-folder {
        font-size: 16px;
        font-family: Monospace;
        color: #1ab394;
    }

    tr.unread td {
        color: red;
    }

    tr.unread a {
        color: red;
    }

    .ibox-content {
        height: 1000px;
    }

    ul.nav-tabs {
        font-size: 14px;
        border-bottom: none;
    }

    ul.nav-tabs li > a {
        color: black !important;
        font-family: Monospace;
    }

    ul.nav-tabs li.active > a {
        color: white !important;
        font-family: Monospace;
        background: black !important;
    }

    .box-message {
        height: 800px !important;
        overflow: auto;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight" style="font-size: 18px;">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <h2><?= $title ?></h2>

                    <ul class="nav nav-tabs">
                        <li <?= ($this->uri->segment('3') == '' ? " class=\"active\"" : "") ?>><a
                                    href="/admin/system_email"><span class="fa fa-user"></span> NoBo</a></li>
                        <li <?= ($this->uri->segment('3') == 'email_setting' ? " class=\"active\"" : "") ?>><a
                                    href="/admin/system_email/email_setting"><span class='fa fa-gear'></span>
                                Setting</a></li>
                    </ul>

                    <div class="content-block-60" style="display: inline-flex; width: 100%;">
                        <div class="content-notification-folder" style="width: 25%;">
                            <div class="notification-folder inbox-folder">
                                <span class="fa fa-folder folder-icon enabled"><input type="hidden"
                                                                                      class="folder-status"
                                                                                      value="0"></span>
                                <span class="folder-name enabled">Inbox Folder</span>
                            </div>
                            <div class="main-folders" style="display: none;">
                                <?php foreach ($notifications as $notification) { ?>
                                    <?php if($notification['email_type'] == 2 || $notification['email_type'] == 4) : ?>
                                        <div class="notification-folder message-folder-disabled">
                                    <?php else: ?>
                                        <div class="notification-folder message-folder">
                                    <?php endif;?>
                                        <?php if ($notification['email_type'] == 2 || $notification['email_type'] == 4) { ?>
                                            <span class="fa fa-folder folder-icon disabled"></span>
                                            <span class="folder-name disabled email-name"
                                                  style="display: none;"><?= $notification['name'] ?></span>
                                            <span class="folder-name disabled show-folder-name"></span>
                                            <i class="fa fa-trash disabled"></i>
                                        <?php } else { ?>
                                            <span class="fa fa-folder folder-icon enabled"><input type="hidden"
                                                                                                  class="folder-status"
                                                                                                  value="0"></span>
                                            <span class="folder-name enabled email-name"
                                                  style="display: none;"><?= $notification['name'] ?></span>
                                            <span class="folder-name enabled show-folder-name"></span>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="notification-trash-folder">
                                <span class="fa fa-trash trash-icon enable"></span>
                                <span class="trash-folder enable">Trash</span>
                            </div>
                        </div>
                        <div class="content-notification-messages" style="width: 75%;">
                            <div class="box-message">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .content-block-80 -->
    </div><!-- .my-container -->
</div>
<script src="/theme/admin/js/jquery-3.1.1.min.js"></script>
<script>
    function insertMessage(messages) {
        let view = "<div class='table-responsive'><table width='80%' style='font-size: 15px; font-family: Monospace; color: black' cellPadding='0' cellSpacing='0' class='table table-striped table-hover'>";
        var member = null;
        for (let i = 0; i < messages.length; i++) {
            let from;
            let class_name = 'read';
            if (messages[i]['read'] == 0) {
                class_name = 'unread';
            }
            switch (messages[i]["type"]) {
                case "email":
                    from = "System";
                    break;

                case "admin":
                    from = "Administrator";
                    break;

                case "reader":
                    let sender_id = messages[i]['sender_id'];
                    $.ajax({
                        'type': "POST",
                        'url': "/my_account/system_email/getMember",
                        'data': {user_id: sender_id},
                        success: function (data) {
                            let obj = JSON.parse(data);
                            member = obj.member;
                        }
                    });
                    from = member['first_name'] + member['last_name'];
                    break;
            }
            view = view + "<tr class=" + class_name + ">" + "<td width='250'>" + messages[i]['datetime'] + " EST</td>"
                + "<td>" + from + "</td>" + "<td>" + messages[i]['subject'] + "</td>"
                + "<td width='50' align='center'><a href='/admin/system_email/view/" + messages[i]['id'] + "' class='btn btn-primary'>View</a></td>"
                + "<td width='50' align='center'><a href='/admin/system_email/delete/" + messages[i]['id'] + "' onClick=\"Javascript:return confirm('Are you sure you want to delete this message?');\" class='btn btn-danger'>Delete</a></td>"
                + "</tr>";
        }
        view = view + "</table></ div>";
        return view;
    }

    $(document).ready(function () {

        var messages;
        var notifications;
        var trash_notifications;

        function getData() {
            $.ajax({
                'type': "POST",
                'url': "/my_account/system_email/getMessages",
                'data': {user_id: '<?=$this->member->data['id'];?>'},
                success: function (data) {
                    let obj = JSON.parse(data);
                    messages = obj.messages;
                    notifications = obj.notifications;
                    trash_notifications = obj.notifications_trash;
                    console.log(notifications);
                }
            });
        }

        getData();

        setTimeout(function () {
            for (let i = 1; i <= notifications.length; i++) {
                let email_name = $(".main-folders .notification-folder:nth-child(" + i + ")").find(".folder-name").text();
                let array = email_name.split("_");
                let email_name_show = "";
                for (let n = 0; n < array.length; n++) {

                    email_name_show = email_name_show + array[n].charAt(0).toUpperCase() + array[n].slice(1) + " ";
                }
                $(".main-folders .notification-folder:nth-child(" + i + ")").find(".show-folder-name").text(email_name_show);
            }
            $(".main-folders").css("display", "block");
        }, 500);

        $(".notification-folder .enabled").click(function () {

            let folder_status = $(this).parent().find(".folder-status").val();
            console.log(folder_status);
            if (folder_status == 0) {
                $(".fa-folder-open").removeClass("fa-folder-open").addClass("fa-folder");
                $(".folder-status").val(0);
                $(".folder-name").css('color', '#1ab394');
                $(this).parent().find(".fa-folder").removeClass("fa-folder").addClass("fa-folder-open");
                $(this).parent().find(".folder-name").css('color', '#337ab7');
                $(this).parent().find(".folder-status").val(1);
                $('.trash-folder').css('color', '#1ab394');
                // show contained messages

            } else {
                $(this).parent().find(".fa-folder-open").removeClass("fa-folder-open").addClass("fa-folder");
                $(this).parent().find(".folder-status").val(0);
                $(".folder-name").css('color', '#1ab394');
                $('.trash-folder').css('color', '#1ab394');
            }
        });

        $(".notification-trash-folder").click(function () {
            $(".notification-folder").find(".fa-folder-open").removeClass("fa-folder-open").addClass("fa-folder");
            $(this).parent().find(".trash-folder").css('color', '#337ab7');
            $('.folder-name').css('color', '#1ab394');
            $(".folder-status").val(0);
        });

        $(".inbox-folder").click(function () {
            getData();
            $(".box-message").empty();
            let folder_status = $(this).parent().find(".folder-status").val();
            if (folder_status == 0) {

            } else {

                for (let i = 0; i < notifications.length; i++) {
                    for (let j = messages.length - 1; j >= 0; j--) {
                        if (messages[j]['name'] == notifications[i]['name']) {
                            messages.splice(j, 1);
                        }
                    }
                }
                let view = insertMessage(messages);
                $(".box-message").append(view);
            }
        });
        $(".message-folder").click(function () {

            getData();
            $(".box-message").empty();
            let folder_status = $(this).find(".folder-status").val();
            if (folder_status == 0) {

            } else {
                let folder_name = $(this).find(".email-name").text();
                console.log(folder_name);
                for (let j = messages.length - 1; j >= 0; j--) {
                    if (messages[j]['name'] != folder_name) {
                        messages.splice(j, 1);
                    }
                }

                let view = insertMessage(messages);
                $(".box-message").append(view);
            }
        });
        $(".notification-trash-folder").click(function () {

            // var trash_notifications = [];
            var trash_messages = [];
            getData();
            $(".box-message").empty();
            // for (let n = notifications.length - 1; n >= 0; n--) {
            //     if (notifications[n]['email_type'] == 2 || notifications[n]['email_type'] == 4) {
            //         trash_notifications.push(notifications[n]);
            //     }
            // }
            for (let i = 0; i < trash_notifications.length; i++) {
                for (let j = messages.length - 1; j >= 0; j--) {
                    if (messages[j]['name'] == trash_notifications[i]) {
                        trash_messages.push(messages[j]);
                    }
                }
            }
            let view = insertMessage(trash_messages);
            $(".box-message").append(view);
        });
    });
</script>


