<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Psychic Contact | Adminitrator </title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <link href="/theme/admin/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="/theme/admin/css/plugins/jqui/css/overcast/jquery-ui-1.8.16.custom.css" />
        <link rel="stylesheet" href="/theme/admin/css/plugins//datetime/jquery-ui-timepicker-addon.css" />
        <link href="/theme/admin/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="/theme/admin/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">
        <link href="/theme/admin/css/animate.css" rel="stylesheet">
        <link href="/theme/admin/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
        <link href="/theme/admin/css/style.css" rel="stylesheet">        
        <link href="/media/assets/css/jquery.spellchecker.css" />
        <script type="text/javascript"></script>

    </head>


<body class ="md-skin">


  <!-- Wrapper-->
    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="/theme/admin/images/profile_small.jpg" />
                             </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?= $this->admin['name'] ?></strong>
                             </span> <span class="text-muted text-xs block">Owner <b class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">                                
                                <li><a href='/admin/pages/edit/8/0/1'>Settings</a></li>
                                <li><a href='/admin/pages/index/1'>Administrators</a></li> 
                                <li class="divider"></li>
                                <li><a href="/admin/main/logout" onclick="Javascript:return confirm('Are you sure you want to logout?')";>Logout</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">                                
                        </div>
                    </li>

                                                

                <li class="active">
                    <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span></a>                        
                </li>
                <!--10-17-->
<!--                <li>-->
<!--                    <a href="/admin/main/chat_in_session"><i class="fa fa-comment"></i> <span class="nav-label"  style="color: #676a6c;">End Chat</span></a>-->
<!--                </li>-->
                <!-- 9-16 -->
<!--                <li>-->
<!--                    <a href="/admin/main/admin_chat"><i class="fa fa-comment"></i> <span class="nav-label"  style="color: #676a6c;">Admin Chat</span></a>-->
<!--                </li>-->
                <!--9-18-->
<!--                <li>-->
<!--                    <a href="/admin/main/admin_interrupt"><i class="fa fa-comment"></i> <span class="nav-label"  style="color: #676a6c;">Admin Interrupt</span></a>-->
<!--                </li>-->

<!--                <li>-->
<!--                    <a href="/admin/message_management"><i class="fa fa-comment"></i> <span class="nav-label"  style="color: #676a6c;">Message Board</span></a>-->
<!--                </li>-->
                <!--10-19-->
<!--                <li>-->
<!--                    <a href="/admin/main/urgent_message"><i class="fa fa-comment"></i> <span class="nav-label"  style="color: #676a6c;">Urgent Message</span></a>-->
<!--                </li>-->
<!---->
<!--                <li>-->
<!--                    <a href="/admin/main/page_disconnect_reader"><i class="fa fa-comment"></i> <span class="nav-label"  style="color: #676a6c;">Disconnect Readers</span></a>-->
<!--                </li>-->
<!---->
<!--                <li>-->
<!--                    <a href="/admin/main/reader_whitelist"><i class="fa fa-comment"></i> <span class="nav-label"  style="color: #676a6c;">Reader Whitelist</span></a>-->
<!--                </li>-->
<!--                <li>-->
<!--                    <a href="/admin/system_email"><i class="fa fa-comment"></i> <span class="nav-label"  style="color: #676a6c;">Notification Board</span></a>-->
<!--                </li>-->
<!--                <li>-->
<!--                    <a href="/admin/email_form"><i class="fa fa-comment"></i> <span class="nav-label"-->
<!--                                                                                    style="color: #676a6c;">Email Form</span></a>-->
<!--                </li>-->
                <?php echo $this->vadmin->getMenus(); ?>


                </ul>
            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <!-- top nav -->
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    </div>
                        <ul class="nav navbar-top-links navbar-right">

                        <li id="unread-message-link" style="display: none;">
                            <a class="m-r-sm text-muted welcome-message" href="/admin/message_management">You have unread messages : <span id="num-unread"></span></a>
                        </li>

                        <li>
                            <span class="m-r-sm text-muted welcome-message">Welcome</span>
                        </li>
                        <li>
                            <a href="/admin/main/logout">
                                <i class="fa fa-sign-out"></i> Log out
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- top nav -->

                <!-- main view -->                
                <?php                    
                    if(isset($view_template) && $view_template)
                    {
                        //$this->load->view($view_template);
                        echo $view_template;
                    }
                ?>                                    
                <!-- main view -->
                <div class="footer">
                    <div class="pull-right">
                        Jayson Lynn 
                    </div>
                    <div>
                        <strong>Copyright</strong>  2017
                    </div>
                </div>

        </div>
        <!-- End page wrapper-->
    </div>
    <!-- End wrapper-->
    
    <script src="/theme/admin/js/jquery-3.1.1.min.js"></script>
    <script src="/theme/admin/js/bootstrap.min.js"></script>
    <script src="/theme/admin/js/plugins/metisMenu/jquery.metisMenu.js"></script>    
    <script src="/theme/admin/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<!--    <script src="/theme/admin/js/plugins/tiny_mce/jq.tinymce.js"></script>-->
    <script src="/theme/admin/js/app.js"></script>
    <script src="/theme/admin/js/plugins/pace/pace.min.js"></script>
    <script src="/theme/admin/js/plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="/theme/admin/js/plugins/dataTables/datatables.min.js"></script>
    <script src='/theme/admin/js/plugins/datetime/jquery-ui-timepicker-addon.js'></script>
    <script src="/theme/admin/js/notify.min.js"></script>  <!-- thom ho8-5 -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.min.js"></script>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=e465y2qbjbdi14qglxeq486et6jfhc6d0zy21wr3akkqps42"></script>

    <!-- thom ho8-5 -->
    <script type="text/javascript">

        tinymce.init({
            selector: 'td textarea, #message',
            height: 350,
            plugins: [
                "advlist autolink lists link image charmap print preview anchor link image code",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste imagetools wordcount", 'image code textcolor colorpicker spellchecker',
            ],
            browser_spellcheck: true,
            image_dimensions: false,
            image_description: false,
            relative_urls : false,
            remove_script_host : false,
            convert_urls : true,
            toolbar: "insertfile | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor spellchecker",
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ],
            automatic_uploads: true,
            imagetools_cors_hosts: ['mydomain.com', 'otherdomain.com'],paste_data_images: true,

            file_picker_callback: function(cb, value, meta) {
                var input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');

                input.onchange = function() {
                    var file = this.files[0];

                    var reader = new FileReader();
                    reader.onload = function () {

                        var id = 'blobid' + (new Date()).getTime();
                        var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                        var base64 = reader.result.split(',')[1];
                        var blobInfo = blobCache.create(id, file, base64);
                        blobCache.add(blobInfo);
                        cb(blobInfo.blobUri(), { title: file.name });
                    };
                    reader.readAsDataURL(file);
                };

                input.click();
            }
        });

        $(document).ready(function(){

            $.ajax({

                url: '/admin/main/check_unread_message/',
                type: 'GET',

            }).done(function(response) {

                var obj = JSON.parse(response);
                var data = parseInt(obj['unread']);

                if(data > 0) {
                    $("li#unread-message-link").css("display", "inline-block");
                    $("#num-unread").text(data);
                }

            }).fail(function(error) {
                console.error(error);
            });

            var delete_chat_session_array = [];

            $(".delete-chat-checkbox").change(function () {

                var delete_session_checked = $(this).is(':checked');

                if (delete_session_checked) {
                    var chat_session_id = $(this).parent().parent().find(".chat-session-id").html();
                    delete_chat_session_array.push(chat_session_id);
                } else {
                    var chat_session_id = $(this).parent().parent().find(".chat-session-id").html();
                    delete_chat_session_array.splice(delete_chat_session_array.indexOf(chat_session_id), 1);
                }
            });

            $("#delete-chat-session").click(function () {
                $.ajax({
                    'type': "POST",
                    'url': "/admin/main/delete_chat_session",
                    'data': { chat_session: delete_chat_session_array },
                    success: function (data) {
                        location.reload();
                    }
                });
            });

            var index_array = (window.location.pathname).split("/");
            var index_position = $.inArray( "index", index_array );
            
            var index = index_array[index_position+1];
            
            if(index == 20) {
            $.notify("Chatting data has been updated !", {className:"success",align:"right", verticalAlign:"top", hideDuration: 700, autoHideDelay: 5000,});
            
                setInterval(function() {
                    location. reload(true);

                }, 60000);
            }

            /*9-5*/
            var appointmentTimer = setInterval(function(){

                $.ajax({

                    url: '/chat/main/get_admin_appointment/',
                    type: 'GET',

                }).done(function(response) {

                    var obj = JSON.parse(response);
                    var data = obj['appointments'];

                    for(var i=0; i<data.length; i++) {

                        var messageText = data[i]['client_username'] + ' ' + 'sent ' + data[i]['reader_username'] 
                                        + ' an appointment for ' + data[i]['appointment_date'];
                  
                        $.notify( messageText,  {className: "info", hideDuration: 700, autoHideDelay: 60000, position: 'right buttom'});
                    }
                   

                }).fail(function(error) {
                    console.error(error);
                });

            }, 3000);

            var declineTimer = setInterval(function(){

                $.ajax({

                    url: '/chat/main/get_admin_decline/'+'<?=$this->member->data['id']?>',
                    type: 'GET',

                }).done(function(response) {

                    var obj = JSON.parse(response);
                    var data = obj['appointments'];

                    for(var i=0; i<data.length; i++) {

                        var messageText = data[i]['reader_username'] + ' declined ' + data[i]['client_username'] + '`s appointment for ' + data[i]['appointment_date'];
                  
                        $.notify( messageText,  {className: "error", hideDuration: 700, autoHideDelay: 60000, position: 'right buttom'});
                    }
                    

                }).fail(function(error) {
                    console.error(error);
                });

            }, 3000);

            setInterval(function () {
                $.ajax({
                    'type': "POST",
                    'url': "/admin/main/get_interrupt_reply_message_all",

                    success: function (data) {
                        var obj = JSON.parse(data);
                        var messages = obj.messages;
                        for (var i = 0; i < messages.length; i++) {

                            var text = messages[i]['username'] + " : " + messages[i]['reply_text'];

                            $.notify(text, {
                                className: "success",
                                hideDuration: 700,
                                autoHideDelay: 60000,
                                position: 'right buttom'
                            });
                            $.ajax({
                                'type': "POST",
                                'url': "/admin/main/admin_check_reply_message",
                                'data': {
                                    id: messages[i]['id']
                                },
                                success: function (data) {

                                }
                            });

                        }
                    }
                });
            }, 2000);

            $(".btn-disconnect").click(function() {
                let username = $(this).parent().parent().find('.chat-session-id').html();
                var r = confirm("Are you sure you want to disconnect "+username+" ?");

                if (r == true) {
                    let reader_id = $(this).parent().parent().find(".id").html();
                    $.ajax({
                        'type': 'POST',
                        'url': '/admin/main/disconnect_reader',
                        'data': {id: reader_id},
                        success: function (data) {
                            window.location.reload();
                        }
                    })
                } else {

                }

            });

            $(".date-dob").datepicker({ dateFormat: 'mm-dd-yy', changeYear: true, yearRange: "-100:+0", });

            $("input[name='block_words']").prop('required',true);

            $('#check-copy').click(function () {
                $.ajax({
                    'type': "POST",
                    'url': "/admin/main/test_copy",
                    'data': { content : $('#content').val()},
                    success: function (data) {
                        var obj = JSON.parse(data);
                        var messages = obj.data;
                        var result_array='';

                        $('#all-view-url').text(messages['allviewurl']);
                        $('#all-view-url').attr('href', messages['allviewurl']);
                        $('#query-words').text(messages['querywords']);
                        $('#count').text(messages['count']);
                        if(messages['count'] == 1) {
                            result_array = result_array + '<p>\n' +
                                '<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample'+ 0 +'" aria-expanded="false" aria-controls="collapseExample'+ 0 +'">\n' +
                                '\n' + messages['result']['title']+
                                '</button>\n' +
                                '</p>\n' +
                                '<div class="collapse" style="width:600px;" id="collapseExample'+ 0 +'">\n' +
                                '<div class="card card-body">\n' +
                                '<p><strong>Title</strong>: '+ messages['result']['title'] +'</p>\n' +
                                '<p><strong>Text</strong>: '+ messages['result']['htmlsnippet'] +'</p>\n' +
                                '<p><strong>Min words matched</strong>: '+ messages['result']['minwordsmatched'] +'</p>\n' +
                                '<p><strong>Url</strong>: <a href="'+ messages['result']['url'] +'" target="_blank">'+ messages['result']['url'] +'</a></p>\n' +
                                '</div>\n' +
                                '</div>';
                        } else {

                            for(let i=0; i< messages['result'].length; i++) {

                                result_array = result_array + '<p>\n' +
                                    '<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample'+ i +'" aria-expanded="false" aria-controls="collapseExample'+ i +'">\n' +
                                    '\n' + messages['result'][i]['title']+
                                    '</button>\n' +
                                    '</p>\n' +
                                    '<div class="collapse" style="width:600px;" id="collapseExample'+ i +'">\n' +
                                    '<div class="card card-body">\n' +
                                    '<p><strong>Title</strong>: '+ messages['result'][i]['title'] +'</p>\n' +
                                    '<p><strong>Text</strong>: '+ messages['result'][i]['htmlsnippet'] +'</p>\n' +
                                    '<p><strong>Min words matched</strong>: '+ messages['result'][i]['minwordsmatched'] +'</p>\n' +
                                    '<p><strong>Url</strong>: <a href="'+ messages['result'][i]['url'] +'" target="_blank">'+ messages['result'][i]['url'] +'</a></p>\n' +
                                    '</div>\n' +
                                    '</div>';
                            }
                        }

                        console.log(result_array);
                        $('#copycheck-result').append(result_array);
                    }
                });
            });


            $('#reject-article').click(function () {

            });

            $("input[value='Save changes']").click(function(e) {
                var pathname = window.location.pathname;
                var word = $(this).parent().parent().parent().parent().find('input[name="block_words"]').val();
                if(pathname.split("/")[4] == 21 && pathname.split("/")[3] == 'add' && word) {

                    e.preventDefault(e);
                    $.ajax({
                        'type': 'POST',
                        'url': '/admin/main/check_duplicate_block_word',
                        'data': {block_word: word},
                        success: function (data) {
                            console.log(data)
                            let obj = JSON.parse(data);
                            let result = obj['status'];
                            if(result == 'exist') {
                                $.notify("This word already exists. Please try another one.", {className:"warning",align:"center", verticalAlign:"top", hideDuration: 700, autoHideDelay: 5000,});
                                $("input[value='Save changes']").parent().parent().parent().parent().find('input[name="block_words"]').val('');
                            } else {
                                $("form").submit()
                            }
                        }
                    })
                }

            })
            //get single transaction for modal
            var uiBtn = $('#wrapper').find('.view-details');

            uiBtn.off('click.viewData').on('click.viewData', function(){
             
                var id = $(this).attr("data-id");
                console.log(id);

                 $.ajax({
                  type: "POST",
                  url: "/admin/pages/getTransaction",
                  data : {"id":id},
                  success: function(data){
                    $('.clear').text(" ");
                    $('.username').text(data.data.username);
                    $('.fullname').text(data.data.client_fname + ' ' + data.data.client_lname);
                    $('.address').text(data.data.country);
                    $('.dob').text(data.data.dob);
                    $('.chattopic').text(data.data.username);
                    $('.choosenreader').text(data.data.reader_fname + ' ' + data.data.reader_lname);
                    $('.orderstatus').text(data.data.settled);
                    $('.amount').text(data.data.amount);
                    $('.paidvia').text(data.data.payment_type);
                  }
                });
            });

        });



    </script>
</body>
</html>
