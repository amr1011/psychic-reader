<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    End Chat
                </div>
                <div class="ibox-content">
                    <?php if ($this->session->flashdata('response_end_chat')): ?>
                        <p class="record"
                           style="display: none;"> <?= $this->session->flashdata('response_end_chat') ?> </p>
                    <? endif ?>
                    <div class="table-responsive">
                        <?php if (!$data): ?>
                            <p style="padding:10px; color: red;">There is no chat in session now.</p>
                        <?php else: ?>
                            <table class="table table-striped table-bordered table-hover data-table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Chat ID</th>
                                    <th>Topic</th>
                                    <th>Chat Info</th>
                                    <th style="text-align: center; width: 25px"><strong>End</strong></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($data as $chat): ?>
                                    <tr class="special_td">
                                        <td class="std"><?php echo($chat["id"]); ?></td>
                                        <td class="std chat-session-id"><?php echo($chat["chat_session_id"]); ?></td>
                                        <td class="std"><?php echo($chat["topic"]); ?></td>
                                        <td class="std"><?php echo($chat["reader_username"] . " is chatting with " . $chat["client_first_name"] . " " . $chat["client_last_name"]
                                                . " (ID: " . $chat["client_id"] . ", U: " . $chat["client_username"] . ")"); ?>
                                        </td>
                                        <td class="std"><input type="checkbox" class="delete-chat-checkbox"
                                                               style="width: 20px; height: 20px;"/></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div class="ibox-content m-b-sm">
                                <div class="row">
                                    <a href='#' id="delete-chat-session" class="btn btn-w-m btn-danger">End Chat</a>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script>
    $(document).ready(function () {

        if ($('p.record').html()) {
            $.notify($('p.record').html(), {
                className: "error",
                align: "center",
                verticalAlign: "top",
                hideDuration: 700,
                autoHideDelay: 5000,
            });
        }
    })
</script>
