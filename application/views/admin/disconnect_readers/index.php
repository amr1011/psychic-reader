<style>
    th, td {
        text-align: center;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight">
    <?php if ($this->session->flashdata('response_disconnect')): ?>
        <p class="record" style="display: none;"> <?= $this->session->flashdata('response_disconnect') ?> </p>
    <? endif ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    Disconnect Reader
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                        <?php if (!$data): ?>
                            <p style='padding:10px; color: red;'>There is no reader. </p>
                        <?php else: ?>
                            <table class="table table-striped table-bordered table-hover  data-table">
                                <thead>
                                <tr>
                                    <th>Reader ID</th>
                                    <th>User Name</th>
                                    <th>Status</th>
                                    <th style="width: 200px;">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($data as $reader): ?>
                                    <tr class="special_td">
                                        <td class="std id"><?php echo($reader["id"]); ?></td>
                                        <td class="std chat-session-id"><?php echo($reader["username"]); ?></td>
                                        <td class="std"><?php echo($reader["status"]); ?></td>
                                        <td class="std">
                                            <button class="btn btn-danger btn-disconnect">Disconnect</button>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script>
    $(document).ready(function () {

        if ($('p.record').html()) {
            $.notify($('p.record').html(), {
                className: "error",
                align: "center",
                verticalAlign: "top",
                hideDuration: 700,
                autoHideDelay: 5000,
            });
        }
    })
</script>