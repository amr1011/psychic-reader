<div style="padding:20px;background:#FFF;margin:20px;">

    <legend>Urgent Message</legend>
    <div class="clearfix" style="margin:15px 0;"></div>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css">
    <style>
        #message-box, #subject-box {
            width: 100%;
            height: 249px;
            border: #0a568c 1px solid;
            border-radius: 4px;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            resize: none;
            background-image: -webkit-linear-gradient(left, white 10px, transparent 10px), -webkit-linear-gradient(right, white 10px, transparent 10px), -webkit-linear-gradient(white 30px, #ffffff 30px, #3c3c3c 31px, white 31px);
            background-image: -moz-linear-gradient(left, white 10px, transparent 10px), -moz-linear-gradient(right, white 10px, transparent 10px), -moz-linear-gradient(white 30px, #ccc 30px, #ccc 31px, white 31px);
            background-image: -ms-linear-gradient(left, white 10px, transparent 10px), -ms-linear-gradient(right, white 10px, transparent 10px), -ms-linear-gradient(white 30px, #ccc 30px, #ccc 31px, white 31px);
            background-image: -o-linear-gradient(left, white 10px, transparent 10px), -o-linear-gradient(right, white 10px, transparent 10px), -o-linear-gradient(white 30px, #ccc 30px, #ccc 31px, white 31px);
            background-image: linear-gradient(left, white 10px, transparent 10px), linear-gradient(right, white 10px, transparent 10px), linear-gradient(white 30px, #ccc 30px, #ccc 31px, white 31px);
            background-size: 100% 100%, 100% 100%, 100% 31px;
            border-radius: 8px;
            box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
            line-height: 31px;
            font-family: Arial, Helvetica, Sans-serif;
            color: black;
            font-size: 22px;
            padding: 0 15px 0 15px;
        }

        #send {
            margin-left: 86%;
            margin-top: 20px;
        }

    </style>
    <div class="my-container templete-container">

        <div class="wrapper-content">
            <section class="content-block templete-block">
                <div class="row">
                    <div class="col-lg-10 col-md-10">
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <h2>Subject</h2>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9">
                                <textarea id="subject-box" style="height: 32px; overflow: hidden;"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <h2>Message Box</h2>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9">
                                <textarea id="message-box"></textarea>
                            </div>
                            <button id="send" class="btn btn-primary btn-lg">Send</button>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-2">
                        <h4 style="color: red" id="no-reader"></h4>
                        <h2>Select Readers</h2>
                        <div class="row" style="margin: 0px;">
                            <select name="repeat-reader-select" id="select-repeat-client" multiple="multiple" style="display:none;"></select>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

<!--    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-latest.min.js"></script>
    <script src="/theme/admin/js/notify.min.js"></script>
    <script>

        $(document).ready(function(){
            var readers = [];
            $.ajax({
                'type': "POST",
                'url': "/admin/main/get_online_readers",
                success: function (data) {

                    let obj = JSON.parse(data);
                    readers = obj.readers;
                    console.log(readers);
                }
            });

            setTimeout(function () {

                if (readers.length == 0) {
                    $('#no-reader').text('There is no online reader.');
                }

                $('#select-repeat-client').empty();
                for(var i=0; i<readers.length; i++) {

                    $('#select-repeat-client').append("<option value='"+ readers[i]["id"] +"'>"+ readers[i]["username"] +"</option>");
                }

                $('#select-repeat-client').multiselect({

                    numberDisplayed: 3,
                    disableIfEmpty: true,
                    includeSelectAllOption: true,
                    filterPlaceholder: 'Search',
                    enableFiltering: true,
                    includeFilterClearBtn: false,
                    selectAllNumber: false,
                });
            }, 500);

            $("#send").on('click', function () {
                $.ajax({
                    'type': "POST",
                    'url': "/admin/main/send_urgent_message",
                    'data': {readers: $("#select-repeat-client").val(), message: $("#message-box").val(), subject: $("#subject-box").val()},
                    success: function (data) {
                        obj = JSON.parse(data);
                        console.log(obj);
                        $.notify("Message has been sent successfully.", {
                            className: "success",
                            align: "center",
                            verticalAlign: "top",
                            hideDuration: 700,
                            autoHideDelay: 5000,
                        });
                    }
                });
            });

        });
    </script>


</div>