<style>
    th, td {
        text-align: center;
    }
    .reader-name {
        font-size: 18px;
        color: #6b0392;
    }
    .reader-list {
        width: 200px;
    }
    .reader-checkbox {
        float:right;
    }
    .checkbox {
        width: 18px;
        height: 18px;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <legend class="ibox-title">
                   Add New Client & Reader WhiteList <br/><br/>
                </legend>

                <div class="ibox-content">
                    <div>
                        <a class="btn btn-danger btn-cancel" href="/admin/main/reader_whitelist">Cancel</a>
                        <a class="btn btn-success btn-save">Save</a>
                    </div>
                    <br/>
                    <label for="client-list">Client List</label>
                    <select name="client-list" class="form-control client-list" style="width: 200px;">
                        <option disabled selected>Select a client</option>
                        <?php foreach($clients as $client) : ?>
                        <option value="<?= $client['id']?>"><?php echo($client['username']);?></option>
                        <?php endforeach;?>
                    </select>
                    <hr/>
                    <label for="client-list">Reader List</label>
                    <?php foreach($readers as $reader) : ?>
                    <div class="reader-list">
                        <span class="reader-name"><?= $reader['username']?></span><span class="reader-checkbox"><input value="<?= $reader['id']?>" class="checkbox" type="checkbox"/></span>
                    </div>
                    <?php endforeach; ?>

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<script>
    $(document).ready(function () {
        var client_id;
        var reader_list = [];
        $('select.client-list').change('on',function () {
            client_id = $(this).val();
        });

        $('.checkbox').change(function () {
            if(this.checked) {
                reader_list.push($(this).val());
                console.log(reader_list);
            }
            if(!this.checked) {
                let index = reader_list.indexOf($(this).val());
                reader_list.splice(index, 1);
            }
        });

        $('.btn-save').click(function () {
            if($('select.client-list').val()) {
                $.ajax({
                    'type': "POST",
                    'url': "/admin/main/add_new_whitelist",
                    'data': { client_id : client_id, readers: reader_list},
                    success: function (data) {
                        parent.history.back();
                    }
                });
            } else {
                alert("Please select Client")
            }

        })
    });
</script>
