<style>
    th, td {
        text-align: center;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight">
    <?php if($this->session->flashdata('response_delete')):?>
        <p class='record' style="display: none;"> <?=$this->session->flashdata('response_delete')?> </p>
    <?endif?>
    <?php if($this->session->flashdata('response_save')):?>
        <p class='record-save' style="display: none;"> <?=$this->session->flashdata('response_save')?> </p>
    <?endif?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <legend class="ibox-title">
                    Client & Reader WhiteList <br/><br/>
                    <a href="/admin/main/add_whitelist/" class="btn btn-danger"><i class="fa fa-plus"></i> Add New WhiteList</a>
                </legend>

                <div class="ibox-content">

                    <div class="table-responsive">
                        <?php if (!$data): ?>
                            <p style='padding:10px; color: red;'>There is no reader. </p>
                        <?php else: ?>
                            <table class="table table-striped table-bordered table-hover  data-table" >
                                <thead>
                                <tr>
                                    <th>Client</th>
                                    <th>White Listed Reader</th>
                                    <th>Created At</th>
                                    <th style="width: 200px;">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($data as $item): ?>
                                    <tr class='special_td'>
                                        <td class='std id'><?php echo($item["client"]);?></td>
                                        <td class="std chat-session-id"><?php echo($item["reader"]);?></td>
                                        <td class="std"><?php echo($item["created_at"]);?></td>
                                        <td class="std"><a href="/admin/main/delete_whitelist/<?= $item['id']?>" class="btn btn-danger" onClick="Javascript:return confirm('Are you sure you want to delete this record?');"><i class="fa fa-close"></i> Delete</a></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script>
    $(document).ready(function () {

        if($('p.record').html()) {
            $.notify($('p.record').html(), {className:"error",align:"center", verticalAlign:"top", hideDuration: 700, autoHideDelay: 5000,});
        }

        if($('p.record-save').html()) {
            $.notify($('p.record-save').html(), {className:"success",align:"center", verticalAlign:"top", hideDuration: 700, autoHideDelay: 5000,});
        }
    })
</script>