<div style="padding:20px;background:#FFF;margin:20px;">

    <legend>Admin Chat</legend>
<div class="clearfix" style="margin:15px 0;"></div>
<link href="/theme/template/styles/load-styles.css" rel="stylesheet" media="screen">
<link href="/theme/template/styles/main.css" rel="stylesheet" media="screen">

<style>
    .my-container {
    	max-width: unset;
    }
    a.navbar-minimalize {
        background-color: unset;
        border-color: transparent;
    }
    .md-skin .nav > li > a {
        font-weight: 400;
    }
    body {
        font-size: 13px;
        line-height: 1.42857143;
    }
</style>

<div class="my-container templete-container"> 
    
    <div class="wrapper-content">
        <section class="content-block templete-block">
            <div class="row">
                <div class="col-md-12">
                    <h2>Admin Chat With Readers</h2>
                </div>
                <?php $i=0; ?>
                <?php foreach ($readers as $reader): ?>
                	<?php if($reader['status'] != 'offline' && $reader['status'] != 'busy') {?>
	                    <div class="col-md-4">
	                        <div class="panel panel-default">
	                            <div class="panel-body">
	                           
	                                <div class="readers-content__img">                            
	                                    <?php if (file_exists(FCPATH . $reader["profile"])): ?>
	                                        <img src="<?php echo $reader["profile"]; ?>" alt="No Image" style="width: 130px; height: 105px">
	                                    <?php else: ?>
	                                        <img src="/media/assets/no-image-available.jpg" alt="No Image" style="width: 130px; height: 105px">
	                                    <?php endif; ?>                                
	                                </div>
	                                <div class="readers-content__text-top">
	                                    <span><?php echo ucfirst($reader['username']); ?> </span>
	                                    <span><?php echo truncate($reader['area_of_expertise'], 80); ?></span>
	                                </div>

	                                <div class="readers-content__text-bot">
	                                    <a href="/profile/<?php echo $reader["username"];?>">Read biography</a>
	                                </div>
	                               
	                                <div class="div-readers col-xs-12" id="div-reader-<?=$reader['id'];?>" data-username="<?=$reader['username']; ?>" data-reader-id="<?=$reader['id'];?>" style="padding: 0;" >
	                                    
	                                     <?php if ($reader['status'] == "online"): ?>
	                                        <button data-href="/chat/main/index/<?php echo $reader["username"]; ?>" class="btn-sidebar btn-chat" type="button">avaliable for chat</button>
	                                    <?php elseif ($reader['status'] == "break"): ?>
	                                        <button data-href="/chat/main/index/<?php echo $reader["username"]; ?>" class="btn-sidebar btn-break" type="button">Be Right Back: <?php echo $reader['break_time_amount'] ?> mins </button>
	                                        
	                                    <?php endif; ?>
	                           
	                                </div>
	                          	</div>
	                        </div>
	                    </div>
	                <?php }?>
                	<?php $i++; ?>
                <?php endforeach; ?>
            </div>
        </section>
    </div>
</div>      

<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<script>

    $(document).ready(function(){

        $(document).on("click", ".btn-chat", function(){
            window.open($(this).attr("data-href"));
        });

        $(document).on("click", ".btn-break", function(){
            window.open($(this).attr("data-href"));
        });

        function diff_minutes(dt2, dt1) 
        {

          var diff =(dt2.getTime() - dt1.getTime()) / 1000;
          diff /= 60;
          return Math.abs(Math.round(diff));

        }
        
        var check_interval = 50000;
        var check_interval_handler = setInterval(check_reader_status, check_interval);


        function check_reader_status() {
            
            $(".div-readers").each(function(){
                var reader_id = $(this).attr("data-reader-id");
                var username = $(this).attr("data-username");
                var client_id = <?php echo ($this->session->userdata('member_logged'))? $this->session->userdata('member_logged'):0 ?>;
                
                var url = "/ajax_functions.php?mode=checkReaderMyAccountStatus&reader_id="+reader_id+"&client_id="+client_id;
                $.ajax({    
                    url: url,  
                    cache: false,  
                    success: function(retValue){
                        // convert string to json object 
                        var d = jQuery.parseJSON(retValue);

                        if (d.logoff == true) {
                                window.location.href = "/main/logout";
                                return;
                        }
                        var online_reders = $('.readers-container .readers-content[data-status="online"]');
                        var break_readers = $('.readers-container .readers-content[data-status="break"]');
                        var busy_readers = $('.readers-container .readers-content[data-status="busy"]');
                        var offline_readers = $('.readers-container .readers-content[data-status="offline"]');

                        switch(d.status) {
                            case 'online':
                            if (d.ban_type!=null) {
                                $("#div-reader-" + reader_id).html('<span>Reader signaled a '+d.ban_type+' ban.</span>');
                            } else {
                                $("#div-reader-" + reader_id).html('<button data-href="/chat/main/index/'+username+'" class="btn-sidebar btn-chat" type="button">avaliable for chat</button>');
                            }
                            
                            if( $("#div-reader-" + reader_id).parents(".readers-content").data("status")!="online"){
                                $("#div-reader-" + reader_id).parents(".readers-content").data("status",d.status);
                                $("#div-reader-" + reader_id).parents(".readers-content").attr("data-status",d.status);
                                if (online_reders.length) {

                                    $(".div-reader-" + reader_id).insertBefore( $(online_reders[0])).fadeIn().css('display','');
                                } else {
                                    if (busy_readers.length) {
                                        $(".div-reader-" + reader_id).insertBefore( $(busy_readers[0])).fadeIn().css('display','');
                                    } else {
                                        if (break_readers.length) {
                                            $(".div-reader-" + reader_id).insertBefore( $(break_readers[0])).fadeIn().css('display','');
                                        } 

                                    }
                                }
                            }
                                break;
                            case 'busy':

                                var xdiff_minutes = " 0 ";
                                if (d.last_pending_time!=null) {
                                    var last_pending_time = new Date(d.last_pending_time);
                                    var now = new Date();

                                    xdiff_minutes = diff_minutes(last_pending_time, now);
                                } 
                                if (d.ban_type!=null) {
                                    $("#div-reader-" + reader_id).html('<span>Reader signaled a '+d.ban_type+' ban.</span>');
                                } else {
                                   $("#div-reader-" + reader_id).html('<button class="btn-sidebar btn-session" type="button"> in session: '+xdiff_minutes+' mins</button>');
                                }
                                //console.log(d.last_pending_time);
                                
                                if( $("#div-reader-" + reader_id).parents(".readers-content").data("status")!="busy"){
                                    $("#div-reader-" + reader_id).parents(".readers-content").data("status",d.status);
                                    $("#div-reader-" + reader_id).parents(".readers-content").attr("data-status",d.status);
                                    if (online_reders.length) {
                                        $(".div-reader-" + reader_id).insertBefore( $(online_reders[online_reders.length-1])).fadeIn().css('display','');
                                    } else {
                                        if (break_readers.length) {
                                            $(".div-reader-" + reader_id).insertBefore( $(break_readers[0])).fadeIn().css('display','');
                                        } 
                                    }
                                }

                                break;
                            case 'break':
                                if (d.ban_type!=null) {
                                    $("#div-reader-" + reader_id).html('<span>Reader signaled a '+d.ban_type+' ban.</span>');
                                } else {
                                   $("#div-reader-" + reader_id).html('<button data-href="/chat/main/index/'+username+'" class="btn-sidebar btn-break" type="button"> Be Right Back: '+ d.break_time_amount +' mins</button>');
                                }

                                if( $("#div-reader-" + reader_id).parents(".readers-content").data("status")!="break"){
                                    $("#div-reader-" + reader_id).parents(".readers-content").data("status",d.status);
                                    $("#div-reader-" + reader_id).parents(".readers-content").attr("data-status",d.status);

                                    if (offline_readers.length) {
                                        $(".div-reader-" + reader_id).insertBefore( $(offline_readers[0])).fadeIn().css('display','');
                                    } else {
                                        if (busy_readers.length) {
                                            $(".div-reader-" + reader_id).insertBefore( $(busy_readers[busy_readers.length-1])).fadeIn().css('display','');
                                        } 
                                    }
                                }

                                break;
                            
                            default:
                                console.log('undefined status');
                            break;

                        }
                    }  
                }); 
                              
            });
                    
        }
    });
</script>


</div>