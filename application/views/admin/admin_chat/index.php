<style>
    #first_col div{
        background:transparent !important;
        color: #000 !important;
    }
    .fc_container{
        border-bottom:1px solid #e7eaec;
    }
    #second_col table{
        width:100%;
    }

    .second_col_header{
        background: #e7eaec;
        width:100%;
        padding:10px;
    }

    .two_col{
        width:25%;
    }

    .bold, .second_col_header{
        font-weight:bold;
    }

    .form-css{
        width: 300px;
        position: absolute;
        left: 40%;
        z-index:997;
    }

    .center{
        text-align:center;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= (!$subnav ? $nav['title'] : $nav['title'] . " > " . $subnav['title']) ?></h5> 
                </div>
                
                <div class="ibox-content">
            
                   <div class="table-responsive">
                        <?php //echo ($pagination ? "<span class='pagination'>{$pagination}</span>" : "") ?>

                        <div class="form-inline form-css">
                        Filtered By:
                            <select class="form-control" id="filter_chat" onchange="filterFunction()">
                                <option value="ALL">-- All --</option>
                                <option value="USD">USD</option>
                                <option value="CAD">CAD</option>
                            </select>
                        </div>
                        <?php //echo print_r($data); ?>
                        <?php if (!$data): ?>
                            <p style='padding:10px;'>There is no data to display in <?php echo (!$subnav ? $nav['title'] : $subnav['title']) ?> </p>
                        <?php else: ?>
                            <!-- pages tab table -->
                            <table class="table table-striped table-bordered table-hover  common-data-table" >
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Date Recorded</th>
                                    <th>Subject</th>
                                    <th>Session #</th>
                                    <th>Reader</th>
                                    <th>Client</th>
                                    <th>Duration</th>
                                    <th>Paid Time</th>
                                    <th>Free Time</th>
                                    <th>Due to Reader</th>
                                    <th>Rate</th>
                                    <th>Affilliate</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $count = 0;
                                foreach ($data as $table):
                                ?>
                                
                                    <tr class='special_td tr_<?php echo $count;?>'>
                                        <td><?php echo $table['id'];?></td>
                                        <td><?php echo $table['start_datetime'];?></td>
                                        <td><a href="#" onclick="trigger_modal(<?php echo $table['id'];?>)"><?php echo $table['topic'];?></a></td>
                                        <td><?php echo $table['chat_session_id'];?></td>
                                        <td><?php echo $table['reader_name'];?></td>
                                        <td><?php echo $table['client_name'];?></td>
                                        <td><?php echo $table['length'];?></td>
                                        <td><?php echo $table['total_free_length'];?></td>
                                        <td><?php echo $table['free_minutes'];?></td>
                                        <td><?php echo $table['length'];?></td>
                                        <td><?php echo ($table['transaction_summary']['amount'] != '') ? $table['transaction_summary']['amount']. ' ('.$table['transaction_summary']['currency'].')' : ''   ;?></td>
                                        <td><?php ?></td>
                                        <td>
                                            <a href="<?php echo site_url('admin/pages/edit/20/77/'.$table['id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a>
					                        <a href="<?php echo site_url('admin/pages/delete/20/77/'.$table['id']); ?>" class="btn btn-danger btn-xs" onclick="Javascript:return confirm('Are you sure you want to delete this record?');"><span class="fa fa-trash"></span> Delete</a>
                                        </td>
                                    </tr>
                                <?php 
                                $count++;
                                endforeach; ?>
                            </tbody>
                            </table>
                            
                            <input type='submit' value='Save changes' class='btn btn-primary'/>
                            
                        <?php endif; ?>
                        <?php //echo ($pagination ? "<span class='pagination'>{$pagination}</span>" : "") ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- MODAL -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title center"></h4>
        </div>
        <div class="modal-body">
            <div class="alert alert-success data_success">
                <strong>Success!</strong> Data has been updated!
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-6 col-sm-12" id="first_col">

                </div>
                <div class="col-md-6 col-lg-6 col-sm-12" id="second_col">
                    <div class="second_col_header">
                        Common Session Info
                    </div>
                    <div class="second_col_content">
                        <table>
                            <tr>
                                <td>Session ID</td>
                                <td id="sc_session_id" colspan="3">.</td>
                            </tr>
                            <tr>
                                <td>Date</td>
                                <td id="sc_date" colspan="3">.</td>
                            </tr>
                            <tr>
                                <td class="two_col">Client</td>
                                <td class="two_col" id="sc_client">.</td>
                                <td class="two_col">Reader</td>
                                <td class="two_col" id="sc_reader">.</td>
                            </tr>
                            <tr>
                                <td class="two_col">Paid Time</td>
                                <td class="two_col" id="sc_paid_time">.</td>
                                <td class="two_col" >Free Time</td>
                                <td class="two_col" id="sc_free_time">.</td>
                            </tr>
                            <tr>
                                <td>BMT Time</td>
                                <td id="sc_btm_time">.</td>
                            </tr>
                        </table>
                    </div>
                    <div class="second_col_header">
                        Client Info
                    </div>
                    <div class="second_col_content">
                        <table>
                            <tr>
                                <td class="two_col">Balance Before</td>
                                <td class="two_col" id="sc_balance_before">.</td>
                                <td class="two_col" >Balance After</td>
                                <td class="two_col" id="sc_balance_after">.</td>
                            </tr>
                            <tr>
                                <td class="two_col">Free Time before</td>
                                <td class="two_col" id="sc_free_time_before">.</td>
                                <td class="two_col" >Free Time after</td>
                                <td class="two_col" id="sc_free_time_after">.</td>
                            </tr>
                        </table>
                    </div>
                    <div class="second_col_header">
                        Browser & Useragent Info
                    </div>
                    <div class="second_col_content">
                        <table>
                            <tr>
                                <td class="two_col bold">Client Browser/Client Useragent</td>
                                <td class="two_col bold">Reader Browser/Reader Useragent</td>
                                
                            </tr>
                            <tr>
                                <td class="two_col" id="sc_client_browser">.</td>
                                <td class="two_col" id="sc_reader_browser">.</td>
                            </tr>
                        </table>
                    </div>
                    <!-- <div class="second_col_header">
                        Flags
                    </div>
                    <div class="second_col_content">
                        <table>
                            <tr>
                                <td>NRR</td>
                                <td>ADDLST</td>
                                <td>SEr</td>
                                <td>SEc</td>
                                <td>NOTXT</td>
                            </tr>
                            <tr>
                                <td id="sc_nrr">.</td>
                                <td id="sc_addlst">.</td>
                                <td id="sc_ser">.</td>
                                <td id="sc_sec">.</td>
                                <td id="sc_notxt">.</td>
                            </tr>
                        </table>
                    </div>
                    <div class="second_col_header">
                        Payment Deposited
                    </div>
                    <div class="second_col_content">
                        .
                    </div>
                    <div class="second_col_header">
                        Service Info
                    </div>
                    <div class="second_col_content">
                        <table>
                            <tr>
                                <td class="two_col">Duration</td>
                                <td class="two_col" id="sc_duration">.</td>
                                <td class="two_col">Chat end initiator</td>
                                <td class="two_col" id="sc_chat_end">.</td>
                            </tr>
                            <tr>
                                <td class="two_col">Client finish chat normally</td>
                                <td class="two_col" id="sc_chat_finish_normally">.</td>
                                <td class="two_col">Time Returned Back</td>
                                <td class="two_col" id="sc_reader_useragent">.</td>
                            </tr>
                            <tr>
                                <td class="two_col">Reader finished chat normally</td>
                                <td class="two_col" id="sc_reader_chat_finish_normally">.</td>
                                <td class="two_col">Time returned back</td>
                                <td class="two_col" id="sc_reader_useragent">.</td>
                            </tr>
                            <tr>
                                <td class="two_col">Client Finish Chat Normally</td>
                                <td class="two_col" id="sc_client_chat_finish_normally">.</td>
                                <td class="two_col">Finished by cron script</td>
                                <td class="two_col" id="sc_cron_script">.</td>
                            </tr>
                            <tr>
                                <td class="two_col">Reader posts count</td>
                                <td class="two_col" id="sc_reader_posts_count">.</td>
                                <td class="two_col" >Client posts count</td>
                                <td class="two_col" id="sc_client_posts_count">.</td>
                            </tr>
                            <tr>
                                <td>Chat Type</td>
                                <td id="sc_chat_type"></td>
                            </tr>
                        </table>
                    </div> -->
                    <div class="second_col_header">
                        Add lost time
                    </div>
                    <div class="second_col_content">
                    <input type="hidden" id="hidden_id">
                        <table>
                            <tr>
                                <td width="50">Select added PAID time</td>
                                <td width="50"><input type="number" class="form-control-sm form-control" placeholder="0" name="add_paid_time" id="add_paid_time"></td>
                            </tr>
                        </table>
                        <input type="button" value="Add Time" class="btn btn-primary btn-sm" onclick="add_paid_time();">
                        <hr>
                        
                        <table>
                            <p>(FOR NEW FREEBIE CLIENT/SESSIONS ONLY)</p>
                            <tr>
                                <td width="50">Add free time</td>
                                <td width="50"><input type="number" class="form-control-sm form-control" placeholder="0" name="add_free_time" id="add_free_time"></td>
                            </tr>
                        </table>
                        <input type="button" value="Add Free time" class="btn btn-primary btn-sm" onclick="add_free_time();">
                    </div>     
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

<input type="hidden" id="site_url_edit" value="<?php  echo site_url('admin/pages/edit/20/77/');?>">
<input type="hidden" id="site_url_delete" value="<?php  echo site_url('admin/pages/delete/20/77/');?>">
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<script>
function add_free_time(){
    var update_data = $('#add_free_time').val();
    var hidden_id = $('#hidden_id').val()
    var pass_data ={data: update_data , id: hidden_id };
    $.ajax({
         type: "POST",
         url: "<?php echo base_url(); ?>" + "admin/pages/add_free_time",
         data: pass_data,
        // dataType: "json",  
         cache:false,
         success: 
              function(data){
                $(".data_success").show().delay(5000).fadeOut();
                $("#sc_free_time").html(data);
                filterFunction('ALL');
              },
              error: function(xhr, status, error){
         var errorMessage = xhr.status + ': ' + xhr.statusText
         alert('Error - ' + errorMessage);
     }
    });


}


function add_paid_time(){
    var update_data = $('#add_paid_time').val();
    var hidden_id = $('#hidden_id').val()
    var pass_data ={data: update_data , id: hidden_id };
    $.ajax({
         type: "POST",
         url: "<?php echo base_url(); ?>" + "admin/pages/add_paid_time",
         data: pass_data,
        // dataType: "json",  
         cache:false,
         success: 
              function(data){
                $(".data_success").show().delay(5000).fadeOut();
                $("#sc_paid_time").html(data);
                filterFunction('ALL');

              },
              error: function(xhr, status, error){
         var errorMessage = xhr.status + ': ' + xhr.statusText
         alert('Error - ' + errorMessage);
     }
    });
 
}
function trigger_modal($params){
    console.log($params);
    $('#myModal').modal('show');

    $.get( "<?php echo base_url(); ?>admin/pages/chat_transcripts/" + $params, function() {
    }).done(function(response) {
        //console.log(response);
        var data = JSON.parse(response);
        var string = '';
        for(var i=0;i<data.length;i++){
            string += '<div class="row fc_container">'
            ;
            string += '<div class="col-md-6">';
            string += data[i]['datetime'];
            string += '</div>';
            string += '<div class="col-md-6">';
            string += data[i]['message'];
            string += '</div>';
            string += '</div>';
        }

        $('#first_col').append(string);
    });


    $.get( "<?php echo base_url(); ?>admin/pages/chat_details/" + $params, function() {
        //console.log('asdasdasdasd');
    }).done(function(response) {
        //console.log('asdasdasd');
        //alert(response);
        var data = JSON.parse(response);
        $('#hidden_id').val(data['chat'][0]['id']);
        var modal_title = 'CLIENT: '+ data['chat'][0]["client_name"] + ' - SESSION #: ' + data['chat'][0]['chat_session_id'] + '- TIME/DATE: ' + data['chat'][0]["start_datetime"];
        $('.modal-title').html(modal_title);
        console.log(data);
        $('#sc_session_id').html(data['chat'][0]['chat_session_id']);
        $('#sc_date').html(data['chat'][0]["start_datetime"]);
        $('#sc_client').html(data['chat'][0]["client_name"]);
        $('#sc_reader').html(data['chat'][0]["reader_name"]);
        $('#sc_paid_time').html(data['chat'][0]["total_free_length"]);
        $('#sc_free_time').html(data['chat'][0]["free_length"]);
        $('#sc_btm_time').html(data['chat'][0]["free_time_used"]);

        var client_details = data['client_details'];
        var client_details_len = client_details.length;
        console.log('length ' + client_details_len);
        var sc_balance_before = 0;
        var sc_balance_after = 0;
        var sc_free_time_before = 0;
        var sc_free_time_after = 0;


        if(client_details_len > 0){
            sc_balance_after = client_details[0]['balance'] != undefined ? client_details[0]['balance'] : 0;
            sc_balance_before =  client_details[0]['total'] != undefined ? client_details[0]['total'] : 0;
        }
        $('#sc_balance_before').html(sc_balance_before);
        $('#sc_balance_after').html(sc_balance_after);
        $('#sc_free_time_before').html();
        $('#sc_free_time_after').html();

        $('#sc_client_browser').html(data['chat'][0]["client_agent"]);
        $('#sc_reader_browser').html(data['chat'][0]["reader_agent"]);



    }).fail( function(xhr, textStatus, errorThrown) {
        //alert.log(xhr.responseText);
    });

    
}

function filterFunction(){
    //var table = $('.common-data-table').DataTable({});
        var table = $('.common-data-table').DataTable();
        table.clear().draw();
        // var filter = $('#filter_chat option:selected').val();
        // $('.table tbody tr').remove();
        var filter = $('#filter_chat').val();
        $.get( "<?php echo base_url(); ?>admin/pages/chat_filter_data/" + filter, function() {
        //alert( "success" );
        })
        .done(function(response) {
            var string = '';
            var data_ = JSON.parse(response);
            var jdata = data_.datas;
           
            console.log(jdata);
            var t = $('.common-data-table').DataTable();
            for (var x = 0; x < jdata.length; x++) {
                var td1 = jdata[x].id;
                var td2 = jdata[x].start_datetime;
                var td3 = '<a href="#" onclick="trigger_modal('+jdata[x].id+')">'+jdata[x].topic+'</a>';
                var td4 = jdata[x].chat_session_id;
                var td5 = jdata[x].reader_name;
                var td6 = jdata[x].client_name;
                var td7 = jdata[x].length;
                var td8 = jdata[x].total_free_length;
                var td9 = jdata[x].free_minutes;
                var td10 = jdata[x].length;
                var td11 = (jdata[x].amount != undefined) ? jdata[x].amount + ' (' + jdata[x].currency + ') ': '';
                var td12 = '';
                var site_url_edit = $('#site_url_edit').val();
                var site_url_delete = $('#site_url_delete').val();
                var td13 = '<a href="' + site_url_edit + jdata[x].id + '" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span>Edit</a><a href="';
                    td13 += site_url_delete + '"class="btn btn-danger btn-xs" onclick="Javascript:return confirm("Are you sure you want to delete this record?");"><span class="fa fa-trash"></span>Delete</a>';
                t.row.add([td1, td2, td3, td4, td5, td6, td7, td8, td9, td10,td11,td12,td13]).draw(false);
            }
            console.log(string);
            $('.common-data-table tbody').append(string);
            //console.log( response );
        }); 

                                        
 


}



$(document).ready(function () {
        $('.data_success').hide();
        var common_table = $('.common-data-table').DataTable({
            "order": [[ 0, "desc" ]]
            // "columnDefs": [
            //     { "orderable": false, "targets": 0 },
            //     { "orderable": true, "targets": 1 },
            //     { "orderable": true, "targets": 2 },
            //     { "orderable": true, "targets": 3 },
            //     { "orderable": false, "targets": 4 },
            //     { "orderable": false, "targets": 5 },
            //     { "orderable": false, "targets": 6 },
            //     { "orderable": false, "targets": 7 },
            //     { "orderable": false, "targets": 8 },
            //     { "orderable": false, "targets": 9 }
                
            // ],
            // "order": [[ 2, "asc" ]]
        });
    });


    
    
</script>