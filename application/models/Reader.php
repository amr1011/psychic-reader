<?php

require_once( APPPATH . "models/ElephantIO/Client.php");

class reader extends CI_Model
{

    var $reader_id;
    var $data;

    function __construct()
    {

        parent::__construct();

        if ($this->session->userdata('member_logged'))
        {

            $this->init($this->session->userdata('member_logged'));
        }
    }

    function init($reader_id = null)
    {
        if ($reader_id)
        {

            if (is_numeric($reader_id))
            {
                $this->reader_id = $reader_id;

                $sql = "SELECT 
                                member_profiles.*,
                                members.username,
                                members.email,
                                CASE WHEN profile_image IS NULL 
                                        THEN '/media/images/no_profile_image.jpg'
                                        ELSE CONCAT('/media/assets/', profile_image)
                                END AS 'profile'
                        FROM 
                                member_profiles 
                        JOIN members ON members.id = member_profiles.id
                        WHERE 
                                member_profiles.id = {$this->reader_id} 
                        LIMIT 1";
                $this->data = $this->db->query($sql)->row_array();
            } else
            {

                $sql = "SELECT 
                            member_profiles.*,
                            members.username,
                            members.email,
                            CASE WHEN profile_image IS NULL 
                                    THEN '/media/images/no_profile_image.jpg'
                                    ELSE CONCAT('/media/assets/', profile_image)
                            END AS 'profile'
                        FROM 
                            member_profiles 
                        JOIN members ON members.id = member_profiles.member_id
                        WHERE 
                            members.username = '{$reader_id}'
                        LIMIT 1";
                $this->data = $this->db->query($sql)->row_array();
                $this->reader_id = $this->data['id'];
            }
        }

        return $this;
    }

    function unpay_reader_for_chat($chat_id = null)
    {

        $this->db->where('type', 'reading');
        $this->db->where('type_id', $chat_id);
        $this->db->update('profile_balance', array('unpay_reader' => 1));

        return true;
    }

    function get_email_package($id = null)
    {

        $packages = $this->get_email_packages();

        foreach ($packages as $p) {

            if ($p['id'] == $id)
            {

                return $p;
                break;
            }
        }
    }

    function set_status($status, $is_manual = false,$reader_id=null)
    {
        switch ($status)
        {
            case 'online':
                $update['status'] = 'online';
                break;

            case 'offline':
                $update['status'] = 'offline';
                break;

            case "busy":
                $update['status'] = 'busy';
                break;

            case "break":
                $update['status'] = 'break';
                break;

            case "away":
                $update['status'] = 'break';
                break;
        }


        if ($status == 'break' && $is_manual)
        {
            $this->db->where('id', $this->data['id']);
            $update['manual_break_time'] = date("Y-m-d H:i:s a", time() + MANUAL_BREAK_TIME);
            $this->db->update('member_profiles', $update);
        } else if ($status == 'break' && $is_manual == false){

            if (isset($reader_id) && !empty($reader_id) ) {
                $this->data['id'] = $reader_id;
            }

            $this->db->where('id', $this->data['id'] );
            $this->db->update('member_profiles', $update);


        } else if ($status == 'online' || $status == "offline")
        {
            if (isset($reader_id) && !empty($reader_id) ) {
                $this->data['id'] = $reader_id;
            }
            
            $this->db->where('id', $this->data['id']);
            $update['manual_break_time'] = "";
            $update['break_time'] = "";
            $update['last_pending_time'] = "";
            $update['last_chat_request'] = "";
            $this->db->update('member_profiles', $update);
        }
        log_message('debug', 'Update reader ' . $this->reader_id . ' status to : ' . $status);

        //--- Send RampNode A Status Update
        // re-encapsulate array
        /*
        $array = array();
        $array['status'] = $status;
        $array['member_id'] = $this->data['id'];
        $array['username'] = $this->data['username'];
        */
        /*
          $rampnode = new Client('http://psycon.rampnode.com', 'socket.io', 1, true, true, true);

          $rampnode->init();
          $rampnode->emit('subscribe', 'psycon.rampnode.com/readerStatuses');
          $rampnode->send(Client::TYPE_MESSAGE, null, null, json_encode($array));
          $rampnode->close();
         */
        return 1;
    }

    function set_break_time($ReaderBreakTime = null,$reader_id=null)
    {   
        if (isset($reader_id) && !empty($reader_id) ) {
            $this->data['id'] = $reader_id;
        }

        $this->db->where('id', $this->data['id']);
        $update['break_time_amount'] = $ReaderBreakTime;
        $this->db->update('member_profiles', $update);

        log_message('debug', 'Update reader ' . $this->reader_id . ' break time amount to : ' . $ReaderBreakTime . ' Minutes');
        
        //--- Send RampNode A Status Update
        // re-encapsulate array
        /*
        $array = array();
        $array['status'] = $status;
        $array['member_id'] = $this->data['id'];
        $array['username'] = $this->data['username'];
        */
        /*
          $rampnode = new Client('http://psycon.rampnode.com', 'socket.io', 1, true, true, true);

          $rampnode->init();
          $rampnode->emit('subscribe', 'psycon.rampnode.com/readerStatuses');
          $rampnode->send(Client::TYPE_MESSAGE, null, null, json_encode($array));
          $rampnode->close();
         */
        return 1;

    }

    function update_last_chat_request()
    {
        $d = date("Y-m-d H:i:s");
        $update['last_chat_request'] = $d;
        $this->db->where('member_id', $this->data['id']);
        $this->db->update('member_profiles', $update);
        return true;
    }

    function update_last_abort_time()
    {
        $d = date("Y-m-d H:i:s");
        $update['last_abort_time'] = $d;
        $this->db->where('member_id', $this->data['id']);
        $this->db->update('member_profiles', $update);
        return true;
    }

    function update_last_pending_request()
    {
        $d = date("Y-m-d H:i:s");
        $update['last_pending_time'] = $d;
        $this->db->where('member_id', $this->data['id']);
        $this->db->update('member_profiles', $update);
        return true;
    }
    function get_last_pending_request(){
        $this->db->selct('last_pending_time');
        $this->db->where(['member_id'=> $this->data['id']]);
        return $this->db->get('member_profiles')->result();
    }

    function update($paramaters = false)
    {
        if ($paramaters)
        {
            $this->db->where('member_id', $this->data['id']);
            $this->db->update('member_profiles', $paramaters);
            return $this->db->affected_rows();
        } else
        {

            return 0;
        }
    }
    function get_member_status($member_id = null,$is_online=false){
        if ($member_id) {
            $this->data['id'] = $member_id;
        }
        if ($is_online) {
            return ($this->db->select('status')->where('member_id', $this->data['id'])->get('member_profiles')->result()[0]->status=="online")? true:false;
        }else{            
            return $this->db->select('status')->where('member_id', $this->data['id'])->get('member_profiles')->result()[0];
        }
    }

    function get_email_packages()
    {

        $packages = $this->db->query("SELECT email_packages.* FROM email_packages ORDER BY email_packages.price")->result_array();
        $getSpecials = $this->db->query("SELECT email_specials.* FROM email_specials WHERE email_specials.profile_id = {$this->reader_id}");

        foreach ($getSpecials->result_array() as $p) {

            $packages[] = array
                (
                'id' => 's' . $p['id'],
                'title' => $p['title'],
                'total_questions' => $p['total_questions'],
                'price' => $p['price'],
                'special' => true
            );
        }

        return $packages;
    }

    function get_testimonials($tid = null, $display = 0)
    {
        if ($display == 1)
        {
            $wher = " and t.admin_approved = 1";
        } else
        {
            $wher = " and t.admin_approved = 1";
        }

        if ($tid)
        {
            $wher .= " and t.id = {$tid}";
        }
        $testi = $this->db->query("select m.*,
                                             t.*
                                      from   testimonials t,
                                             members m
                                      WHERE  t.member_id = m.id
                                             {$wher}
                                             and t.reader_id = {$this->data['id']}
                                      ORDER BY t.datetime DESC
                                             ");
        if ($tid)
        {
            return $testi->row_array();
        } else
        {
            return $testi->result_array();
        }
    }

    function get_clients($search_type = null, $query = null)
    {
        switch ($search_type)
        {
            case 'username':
                $search = " AND UPPER(m.username)  LIKE UPPER('%" . $query . "%') ";
                break;

            case 'session_date':
                $search = " AND DATE(c.start_datetime) = ' " . date("Y-m-d", strtotime($query)) . "' ";
                break;

            case 'client_first_name':
                $search = " AND UPPER(m.first_name)  LIKE UPPER('%" . $query . "%') ";
                break;

            default:
                $search = "";
                break;
        }
        /*$sql = "SELECT DISTINCT mb.*,(select m.first_name from members where id=mb.member_id) as first_name, m.last_name, m.username, m.id as mid
                FROM   members m
                LEFT JOIN member_bans mb ON mb.member_id = m.id
                WHERE   mb.reader_id = {$this->data['id']}
                           {$search}
                ORDER BY mb.date DESC";*/
        
        $sql = "SELECT DISTINCT m.first_name, m.last_name, m.username, m.id as mid, last_login_date
                FROM   members m,
                       chats c                 
                WHERE   c.reader_id = {$this->data['id']}
                        AND m.id = c.client_id
                        AND m.id <> 1
                           {$search}
                ORDER BY m.username";
        
        $clients = $this->db->query($sql);
        /*var_dump($this->db->last_query());
        var_dump($clients->result_array());
        die();*/
        return $clients->result_array();
    }
    function get_banned_clients($search_type = null, $query = null)
    {
        switch ($search_type)
        {
            case 'username':
                $search = " AND UPPER(m.username)  LIKE UPPER('%" . $query . "%') ";
                break;

            case 'session_date':
                $search = " AND DATE(c.start_datetime) = ' " . date("Y-m-d", strtotime($query)) . "' ";
                break;

            case 'client_first_name':
                $search = " AND UPPER(m.first_name)  LIKE UPPER('%" . $query . "%') ";
                break;

            default:
                $search = "";
                break;
        }
        $sql = "SELECT DISTINCT mb.*,(select m.first_name from members where id=mb.member_id) as first_name, m.last_name, m.username, m.id as mid
                FROM   members m
                LEFT JOIN member_bans mb ON mb.member_id = m.id
                WHERE   mb.reader_id = {$this->data['id']}
                           {$search}
                ORDER BY mb.date DESC";

        $clients = $this->db->query($sql);
        /*var_dump($this->db->last_query());
        var_dump($clients->result_array());
        die();*/
        return $clients->result_array();
    }
    /*9-2*/
    function get_note($reader_id)
    {
        $getNotes = $this->db->query("SELECT * FROM reader_note WHERE reader_id = '{$reader_id}' ORDER BY updated_at DESC" )->result_array();
        $notes = [];

        foreach ($getNotes as $item) {
            
            $client_id = $item["client_id"];
            $client_array = $this->db->query("SELECT * FROM members WHERE id = '{$client_id}'")->row_array();
            $user_array = array("client_username"=>$client_array["username"], "client_firstname"=>$client_array["first_name"], "client_lastname"=>$client_array["last_name"]);

            $new_array = [];

            foreach ($item as $key => $value) {
                
                $new_array[$key] = $value;
            }
            foreach ($user_array as $key => $value) {
                
                $new_array[$key] = $value;
            }

            array_push($notes, $new_array);
        }
        return $notes;
    }

    /*9-4*/
    function get_appointment($reader_id)
    {
        $getAppointments = $this->db->query("SELECT * FROM appointment WHERE reader_id = '{$reader_id}'")->result_array();
        $appointments = [];

        foreach($getAppointments as $item) 
        {

            $reader_id = $item["reader_id"];
            $client_id = $item["client_id"];

            $reader_array = $this->db->query("SELECT * FROM members WHERE id = '{$reader_id}'")->row_array();
            $client_array = $this->db->query("SELECT * FROM members WHERE id = '{$client_id}'")->row_array();

            $user_array = array("reader_username"=>$reader_array["username"], "reader_firstname"=>$reader_array["first_name"], "reader_lastname"=>$reader_array["last_name"], "client_username"=>$client_array["username"], "client_firstname"=>$client_array["first_name"], "client_lastname"=>$client_array["last_name"]);

            $new_array = [];

            foreach ($item as $key => $value) {
                
                $new_array[$key] = $value;
            }

            foreach ($user_array as $key => $value) {
                
                $new_array[$key] = $value;
            }
        
            array_push($appointments, $new_array);
        }
       
        return $appointments;
    }

    /*9-12*/
    function get_reader_info($reader_id) 
    {
         $getReaderInfo = $this->db->query("SELECT * FROM members WHERE id = '{$reader_id}'")->row_array();
         return $getReaderInfo;
    }

    /*9-22*/
    function get_gift_history($reader_id)
    {
        $getGifts = $this->db->query("SELECT * FROM reader_giveback WHERE reader_id = '{$reader_id}' ORDER BY id DESC")->result_array();
        $gifts = [];

        foreach($getGifts as $item)
        {
            $client_id = $item["client_id"];

            $client_array = $this->db->query("SELECT * FROM members WHERE id = '{$client_id}'")->row_array();

            $user_array = array("client_username"=>$client_array["username"], "client_firstname"=>$client_array["first_name"], "client_lastname"=>$client_array["last_name"]);

            $new_array = [];

            foreach ($item as $key => $value) {

                $new_array[$key] = $value;
            }

            foreach ($user_array as $key => $value) {

                $new_array[$key] = $value;
            }

            array_push($gifts, $new_array);
        }

        return $gifts;
    }

    function unbanUser($ban_id, $type = "personal")
    {
        $this->db->where("id", $ban_id);
        $this->db->where("type", $type);
        $this->db->delete("member_bans");
    }

    function get_balance($region = 'us')
    {

        $region = trim(strtolower($region));

        $t = $this->db->query
                        ("
                SELECT
	              IFNULL( (SELECT SUM(pb.commission) FROM profile_balance pb WHERE pb.member_id = {$this->reader_id} and pb.unpay_reader = 0 and pb.region = '{$region}'), 0) -
	              IFNULL( (SELECT SUM(t.amount) FROM transactions t WHERE t.member_id = {$this->reader_id} and t.region ='{$region}' and t.type = 'payment'), 0) as balance
			")->row_array();

        return (!$t['balance'] ? '0' : $t['balance']);
    }

    public function get_reader_balance($id,$region)
    {
        $sql = 'select * from profile_balance where member_id = '.$id.' and unpay_reader = 0 AND region = ' .'"'.$region.'"';

        $query = $this->db->query($sql);
        return $query->result_array();  
    }

    function remove_disconnect()
    {
        $this->db->where('member_id', $this->data["id"]);
        $this->db->update("member_profiles", ["disconnect" => 0]);
    }

    function get_read_clients()
    {
        $query = "SELECT DISTINCT chats.client_id, members.username FROM chats LEFT JOIN members ON chats.client_id=members.id WHERE reader_id={$this->data['id']} AND client_id <> 1";
        $all_clients = $this->db->query($query)->result_array();

        foreach($all_clients as $key=>$client)
        {
            $search_ban = $this->db->query("SELECT id FROM member_bans WHERE member_id={$client['client_id']} AND reader_id={$this->data['id']}")->result_array();
            if(count($search_ban) > 0)
            {
                // remove banned member from client array.
                unset($all_clients[$key]);
            }
        }

        return $all_clients;
    }


    function get_reader_name($reader_name) 
    {
            $sql = "SELECT 
                                member_profiles.*,
                                members.username,
                                members.email,
                                CASE WHEN profile_image IS NULL 
                                        THEN '/media/images/no_profile_image.jpg'
                                        ELSE CONCAT('/media/assets/', profile_image)
                                END AS 'profile'
                        FROM 
                                member_profiles 
                        JOIN members ON members.id = member_profiles.id
                        WHERE 
                                members.username = '{$reader_name}'
                        LIMIT 1";
                return $this->db->query($sql)->row_array();

    }
}
