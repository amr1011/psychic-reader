<?php

class readers extends CI_Model
{

    function getFeaturedReaders($total = 10)
    {

        $oneMinuteAgo = date("Y-m-d H:i:s", strtotime("-1 minute"));

        return $this->db->query
                        ("
			
				SELECT
					members.*,
					member_profiles.title,

					CASE WHEN (member_profiles.status IS NULL OR member_profiles.status = 'online') AND member_profiles.last_activity < '{$oneMinuteAgo}'
						THEN 1
						ELSE 0
					END AS online_status,

					CASE WHEN members.profile_image IS NULL
						THEN '/media/images/no_profile_image.jpg'
						ELSE CONCAT('/media/assets/', members.profile_image)
					END AS 'profile'

				FROM
					members

				JOIN member_profiles ON member_profiles.id = members.id

				WHERE
					member_profiles.active = 1 AND
					member_profiles.featured = 1

				ORDER BY
					online_status DESC,
					(SELECT SUM(reviews.rating)/COUNT(reviews.id) FROM reviews WHERE reviews.profile_id = member_profiles.id)
					
				LIMIT {$total}
			
			")->result_array();
    }

    // This function is not working because online_readers do not exist
    function getOnlineReaders($total = 10)
    {
        return $this->db->query
                        ("

				select *
				FROM   online_readers
				LIMIT {$total}

			")->result_array();
    }

    // Get all readers
    // Develop an algorithm to order them
    // ORDER BY: online first & rating second
    // We need to incorporate a pagination mechanism

    public function getAll()
    {
        $now = date("Y-m-d H:i:s");

        $is_full_ban = false;
        $personal_ban_list = array();

        // check if is logged in

        if ($this->session->userdata('member_logged'))
        {
            $query = $this->db->query("SELECT id, reader_id, type FROM member_bans where member_id=" . $this->session->userdata('member_logged'));
            if ($query->num_rows() !== 0)
            {
                $ban_results = $query->result_array();
                foreach ($ban_results as $ban_result) {
                    if ($ban_result['type'] == "full")
                    {
                       /* $is_full_ban = true;*/
                        break;
                    } else if ($ban_result['type'] == "personal")
                    {
                        array_push($personal_ban_list, $ban_result['reader_id']);
                    }
                }
            }
        }

        $sql = "SELECT
                        member_profiles.*,
                        members.username,
                        members.id as member_id,
                        members.paypal_email,
                        CASE WHEN profile_image IS NULL 
                                THEN '/media/images/no_profile_image.jpg'
                                ELSE CONCAT('/media/assets/', profile_image)
                        END AS 'profile'					
                FROM
                        members					
                LEFT JOIN member_profiles ON member_profiles.member_id = members.id

                WHERE
                        members.active = 1 AND members.type = 'READER' AND members.deleted = 0 
                ORDER BY FIELD (member_profiles.status,'online','busy','break','blocked','offline') ASC,members.username";
        $query = $this->db->query($sql);

        $all_results = $query->result_array();

        if ($is_full_ban == true || count($personal_ban_list) > 0)
        {
            
            foreach ($all_results as &$all_result) {
                if (in_array($all_result['member_id'], $personal_ban_list) )
                {
                    $all_result['status'] = "blocked";
                }

                if ($all_result['status'] == "online")
                {
                    if (strtotime($all_result['last_chat_request']) < strtotime($all_result['last_pending_time']))
                    {
                        // then just consider the pending time. 
                        if (strtotime($now) - strtotime($all_result['last_pending_time']) < CHAT_MAX_PENDING)
                        {
                            $all_result['status'] = "online";
                        }
                    } else
                    {
                        if (strtotime($now) - strtotime($all_result['last_chat_request']) < CHAT_MAX_WAIT)
                        {
                            $all_result['status'] = "online";
                        }
                    }

                    if (strtotime($now) < strtotime($all_result['break_time']))
                    {
                        $all_result['status'] = "break";
                    }
                }
            }
            foreach($all_results as $key=>$result)
            {
                $check_reader_block = $this->db->query("SELECT id FROM reader_block WHERE reader_id = {$result['id']}")->result_array();
                $check_reader_block = count($check_reader_block);
                if($check_reader_block > 0)
                {
                    unset($all_results[$key]);
                }
            }

            return $all_results;
        } else
        {
            $all_results = $query->result_array();
            foreach ($all_results as &$all_result) {

                if($all_result['status'] == 'offline') {
                        $all_result['status'] = 'offline';
                }
                
                if ($all_result['status'] == "online")
                {
                    if (strtotime($all_result['last_chat_request']) < strtotime($all_result['last_pending_time']))
                    {
                        // then just consider the pending time. 
                        if (strtotime($now) - strtotime($all_result['last_pending_time']) < CHAT_MAX_PENDING)
                        {
                            $all_result['status'] = "busy";
                        }
                    } else
                    {
                        if (strtotime($now) - strtotime($all_result['last_chat_request']) < CHAT_MAX_WAIT)
                        {
                            $all_result['status'] = "busy";
                        }
                    }

                    if (strtotime($now) < strtotime($all_result['break_time']))
                    {
                        $all_result['status'] = "break";
                    }
                }
            }
            foreach($all_results as $key=>$result)
            {
                $check_reader_block = $this->db->query("SELECT id FROM reader_block WHERE reader_id = {$result['id']}")->result_array();
                $check_reader_block = count($check_reader_block);
                if($check_reader_block > 0)
                {
                    unset($all_results[$key]);
                }
            }

            return $all_results;
        }
    }

    public function getWhiteList($client_id)
    {
        $now = date("Y-m-d H:i:s");

        $is_full_ban = false;
        $personal_ban_list = array();

        // check if is logged in

        if ($this->session->userdata('member_logged'))
        {
            $query = $this->db->query("SELECT id, reader_id, type FROM member_bans where member_id=" . $this->session->userdata('member_logged'));
            if ($query->num_rows() !== 0)
            {
                $ban_results = $query->result_array();
                foreach ($ban_results as $ban_result) {
                    if ($ban_result['type'] == "full")
                    {
                        /* $is_full_ban = true;*/
                        break;
                    } else if ($ban_result['type'] == "personal")
                    {
                        array_push($personal_ban_list, $ban_result['reader_id']);
                    }
                }
            }
        }

        $sql = "SELECT
                        member_profiles.*,
                        members.username,
                        members.id as member_id,
                        members.paypal_email,
                        CASE WHEN profile_image IS NULL 
                                THEN '/media/images/no_profile_image.jpg'
                                ELSE CONCAT('/media/assets/', profile_image)
                        END AS 'profile'					
                FROM
                        members					
                LEFT JOIN member_profiles ON member_profiles.member_id = members.id

                WHERE
                        members.active = 1 AND members.type = 'READER' AND members.deleted = 0 and member_profiles.status in ('online','busy','break','blocked','offline')
                ORDER BY FIELD (member_profiles.status,'online','busy','break','blocked','offline') ASC,members.username";
        $query = $this->db->query($sql);

        $all_results = $query->result_array();


        foreach($all_results as $key=>$result)
        {
            $new_array = [];
            $whitelist_readers = $this->db->query("SELECT reader_id FROM reader_whitelist WHERE client_id={$this->member->data['id']}")->result_array();

            foreach($whitelist_readers as $reader)
            {
                array_push($new_array, $reader['reader_id']);
            }

            if(count($new_array) > 0)
            {
                if(!in_array($result['id'], $new_array))
                {
                    unset($all_results[$key]);
                }
            }
        }


        if ($is_full_ban == true || count($personal_ban_list) > 0)
        {

            foreach ($all_results as &$all_result) {
                if (in_array($all_result['member_id'], $personal_ban_list) )
                {
                    $all_result['status'] = "blocked";
                }

                if ($all_result['status'] == "online")
                {
                    if (strtotime($all_result['last_chat_request']) < strtotime($all_result['last_pending_time']))
                    {
                        // then just consider the pending time.
                        if (strtotime($now) - strtotime($all_result['last_pending_time']) < CHAT_MAX_PENDING)
                        {
                            $all_result['status'] = "online";
                        }
                    } else
                    {
                        if (strtotime($now) - strtotime($all_result['last_chat_request']) < CHAT_MAX_WAIT)
                        {
                            $all_result['status'] = "online";
                        }
                    }

                    if (strtotime($now) < strtotime($all_result['break_time']))
                    {
                        $all_result['status'] = "break";
                    }
                }
            }
            foreach($all_results as $key=>$result)
            {
                $check_reader_block = $this->db->query("SELECT id FROM reader_block WHERE reader_id = {$result['id']}")->result_array();
                $check_reader_block = count($check_reader_block);

                if($check_reader_block > 0)
                {
                    unset($all_results[$key]);
                }
            }
            return $all_results;
        } else
        {

            foreach ($all_results as &$all_result) {

                if($all_result['status'] == 'offline') {
                    $all_result['status'] = 'offline';
                }

                if ($all_result['status'] == "online")
                {
                    if (strtotime($all_result['last_chat_request']) < strtotime($all_result['last_pending_time']))
                    {
                        // then just consider the pending time.
                        if (strtotime($now) - strtotime($all_result['last_pending_time']) < CHAT_MAX_PENDING)
                        {
                            $all_result['status'] = "busy";
                        }
                    } else
                    {
                        if (strtotime($now) - strtotime($all_result['last_chat_request']) < CHAT_MAX_WAIT)
                        {
                            $all_result['status'] = "busy";
                        }
                    }

                    if (strtotime($now) < strtotime($all_result['break_time']))
                    {
                        $all_result['status'] = "break";
                    }
                }
            }
            foreach($all_results as $key=>$result)
            {
                $check_reader_block = $this->db->query("SELECT id FROM reader_block WHERE reader_id = {$result['id']}")->result_array();
                $check_reader_block = count($check_reader_block);

                if($check_reader_block > 0)
                {
                    unset($all_results[$key]);
                }

            }
            return $all_results;
        }
    }

    // Perform a comprehensive search over readers
    // We need to incorporate a pagination mechanism

    function search($category_id = null, $search_query = null)
    {
        $now = date("Y-m-d H:i:s");

        $is_full_ban = false;
        $personal_ban_list = array();

        // check if is logged in

        if ($this->session->userdata('member_logged'))
        {
            $query = $this->db->query("SELECT id, reader_id, type FROM member_bans where member_id=" . $this->session->userdata('member_logged'));
            if ($query->num_rows() !== 0)
            {
                $ban_results = $query->result_array();
                foreach ($ban_results as $ban_result) {
                    if ($ban_result['type'] == "full")
                    {
                        $is_full_ban = true;
                        break;
                    } else if ($ban_result['type'] == "personal")
                    {
                        array_push($personal_ban_list, $ban_result['reader_id']);
                    }
                }
            }
        }

        // Search category AND query
        if ($category_id && $search_query)
        {

            $query = $this->db->query
                    ("
				
					SELECT
						member_profiles.*,
						members.id as member_id, 
						members.username,
						CASE WHEN profile_image IS NULL 
							THEN '/media/images/no_profile_image.jpg'
							ELSE CONCAT('/media/assets/', profile_image)
						END AS 'profile'

						
					FROM
						members
						
					JOIN member_profiles ON member_profiles.id = members.id
						
					WHERE
						member_profiles.active = 1 AND
						(
							members.first_name LIKE '%{$search_query}%' OR
							members.last_name LIKE '%{$search_query}%' OR
							members.username LIKE '%{$search_query}%' OR
							members.id LIKE '%{$search_query}%' OR
							members.email LIKE '%{$search_query}%'
						) AND
						
						EXISTS(SELECT * FROM profile_categories WHERE profile_categories.profile_id = member_profiles.id AND profile_categories.category_id = {$category_id} )
								
				");
        }

        // Search Just Category
        elseif ($category_id && !$search_query)
        {

            $query = $this->db->query
                    ("
				
					SELECT
						member_profiles.*,
						members.id as member_id, 
						members.username,
						CASE WHEN profile_image IS NULL 
							THEN '/media/images/no_profile_image.jpg'
							ELSE CONCAT('/media/assets/', profile_image)
						END AS 'profile'
						
					FROM
						members
						
					JOIN member_profiles ON member_profiles.id = members.id
						
					WHERE
					
						member_profiles.active = 1 AND
						EXISTS (SELECT * FROM profile_categories WHERE profile_categories.profile_id = member_profiles.id AND profile_categories.category_id = {$category_id} )
								
				");
        }

        // Search just query
        else
        {

            $query = $this->db->query
                    ("
				
					SELECT
						member_profiles.*,
						members.id as member_id, 
						members.username,
						CASE WHEN profile_image IS NULL 
							THEN '/media/images/no_profile_image.jpg'
							ELSE CONCAT('/media/assets/', profile_image)
						END AS 'profile'
						
					FROM
						members
						
					JOIN member_profiles ON profiles.id = members.id
						
					WHERE
						member_profiles.active = 1 AND
						(
							members.first_name LIKE (\"%{$search_query}%\") OR
							members.last_name LIKE (\"%{$search_query}%\") OR
							members.username LIKE (\"%{$search_query}%\") OR
							members.id LIKE (\"%{$search_query}%\") OR
							members.email LIKE (\"%{$search_query}%\")
						)
								
				");
        }

        if ($query->num_rows() == 0)
        {

            $array = array();
            $array['error'] = '1';
            $array['message'] = "There were no readers found matching your search criteria. Please try again.";
        } else
        {

            $array = array();
            $array['error'] = '0';

            if ($is_full_ban == true || count($personal_ban_list) > 0)
            {
                $all_results = $query->result_array();
                foreach ($all_results as &$all_result) {
                    if (in_array($all_result['member_id'], $personal_ban_list) || $is_full_ban)
                    {
                        $all_result['status'] = "blocked";
                    }

                    if ($all_result['status'] == "online")
                    {
                        if (strtotime($all_result['last_chat_request']) < strtotime($all_result['last_pending_time']))
                        {
                            // then just consider the pending time. 
                            if (strtotime($now) - strtotime($all_result['last_pending_time']) < CHAT_MAX_PENDING)
                            {
                                $all_result['status'] = "busy";
                            }
                        } else
                        {
                            if (strtotime($now) - strtotime($all_result['last_chat_request']) < CHAT_MAX_WAIT)
                            {
                                $all_result['status'] = "busy";
                            }
                        }

                        if (strtotime($now) < strtotime($all_result['break_time']))
                        {
                            $all_result['status'] = "break";
                        }
                    }
                }
                $array['readers'] = $all_results;
            } else
            {
                $all_results = $query->result_array();
                foreach ($all_results as &$all_result) {
                    if ($all_result['status'] == "online")
                    {
                        if (strtotime($all_result['last_chat_request']) < strtotime($all_result['last_pending_time']))
                        {
                            // then just consider the pending time. 
                            if (strtotime($now) - strtotime($all_result['last_pending_time']) < CHAT_MAX_PENDING)
                            {
                                $all_result['status'] = "busy";
                            }
                        } else
                        {
                            if (strtotime($now) - strtotime($all_result['last_chat_request']) < CHAT_MAX_WAIT)
                            {
                                $all_result['status'] = "busy";
                            }
                        }

                        if (strtotime($now) < strtotime($all_result['break_time']))
                        {
                            $all_result['status'] = "break";
                        }
                    }
                }
                $array['readers'] = $all_results;
            }
        }

        return $array;
    }

    public function get_online_readers()
    {
        $query = $this->db->query("
        
            SELECT members.*, member_profiles.status
            FROM members
            LEFT JOIN member_profiles ON members.`id` = member_profiles.`member_id`
            WHERE members.`type` = 'READER' AND (member_profiles.`status` = 'online' OR member_profiles.`status` = 'break') AND members.deleted = 0 AND members.active = 1
        ");
        $readers = $query->result_array();
        return $readers;
    }

    public function get_all_readers()
    {
        $query= $this->db->query("
            SELECT members.id, members.username, member_profiles.status
            FROM members
            LEFT JOIN member_profiles ON members.`id`=member_profiles.member_id
            WHERE members.`type`='READER' AND  members.deleted = 0 AND members.active = 1
        ");
        $all_readers = $query->result_array();
        return $all_readers;
    }

    public function get_whitelist_readers()
    {
        $query = "
            SELECT * FROM reader_whitelist    
        ";
        $all_white_list_array = $this->db->query($query)->result_array();
        $new_whitelist_array = [];

        foreach($all_white_list_array as $item)
        {
            $client_id = $item['client_id'];
            $reader_id = $item['reader_id'];
            $client = $this->db->query("SELECT username FROM members WHERE `id`={$client_id}")->row_array();
            $client_username = $client['username'];
            $reader = $this->db->query("SELECT username FROM members WHERE `id`={$reader_id}")->row_array();
            $reader_username = $reader['username'];

            $temp_array = [];
            $temp_array['id'] = $item['id'];
            $temp_array['client'] = $client_username;
            $temp_array['reader'] = $reader_username;
            $temp_array['created_at'] = $item['created_at'];

            array_push($new_whitelist_array, $temp_array);
        }

        return $new_whitelist_array;
    }

}
