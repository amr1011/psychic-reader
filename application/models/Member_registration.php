<?php

    class member_registration extends CI_Model
    {

        //--- Login From Form
        function loginForm()
        {

            $this->form_validation->set_rules('username',"Username","trim|required");
            $this->form_validation->set_rules('password',"Password","trim|required");
            $this->form_validation->set_rules('remember',"Remember Me","trim");
            if(!$this->form_validation->run())
            {

                $array = array();
                $array['error'] = 1;
                $array['message'] = validation_errors();
            }
            else
            {

                $array = $this->login(set_value('username'), set_value('password'), set_value('remember'));
            }

            return $array;

        }

        //--- Login Function
        function login($username = null, $password = null, $remember_member = true)
        {

            if($username&&$password)
            {

                $getUser = $this->db->query("SELECT id,banned,email,dob,gender, password FROM members WHERE username = '{$username}'")->result_array();

                foreach($getUser as $item)
                {
                    $member = $item;

                    if ($this->bcrypt->check_password($password, $member['password']))
                    {
                        set_cookie(array(
                            'name'   => $this->config->item('registration_cookie_name'),
                            'value'  => $this->encrypt->encode($member['email'] . "*" . $member['id']),
                            'expire' => time() + (10 * 365 * 24 * 60 * 60), // expire in 10 years
                            'domain' => '',
                            'path'   => '/',
                            'prefix' => '',
                            'secure' => FALSE
                        ));

                        $banned_dob_list = $this->db->query("SELECT * FROM banned_dob_list WHERE day_of_birth = '{$member['dob']}' AND (gender = '{$member['gender']}' OR gender = 'Both')")->result_array();

                        if(count($banned_dob_list) > 0)
                        {
                            $array = array();
                            $array['error'] = 1;
                            $array['message'] = "Your account has been banned.";
                        } else
                        {
                            if($member['banned'] != 1){

                                $this->db->where('id', $member['id']);
                                $this->db->update('members', array('last_login_date'=>date("Y-m-d H:i:s")));

                                $this->session->set_userdata('member_logged', $member['id']);

                                $array = array();
                                $array['error'] = 0;

                            }
                            else
                            {
                                $array = array();
                                $array['error'] = 1;
                                $array['message'] = "Your account has been banned.";
                            }

                        }
                        return $array;
                    }
                }

                // if($getUser->num_rows()==0)
                // {

                    $array = array();
                    $array['error'] = 1;
                    $array['message'] = "Invalid username and/or password";

                // }
                // else
                // {
                //
                //     $member = $getUser->row_array();
                //
                //     set_cookie(array(
                //         'name'   => $this->config->item('registration_cookie_name'),
                //         'value'  => $this->encrypt->encode($member['email'] . "*" . $member['id']),
                //         'expire' => time() + (10 * 365 * 24 * 60 * 60), // expire in 10 years
                //         'domain' => '',
                //         'path'   => '/',
                //         'prefix' => '',
                //         'secure' => FALSE
                //     ));
                //
                //     $banned_dob_list = $this->db->query("SELECT * FROM banned_dob_list WHERE day_of_birth = '{$member['dob']}' AND (gender = '{$member['gender']}' OR gender = 'Both')")->result_array();
                //
                //     if(count($banned_dob_list) > 0)
                //     {
                //         $array = array();
                //         $array['error'] = 1;
                //         $array['message'] = "Your account has been banned.";
                //     } else
                //     {
                //         if($member['banned'] != 1){
                //
                //             $this->db->where('id', $member['id']);
                //             $this->db->update('members', array('last_login_date'=>date("Y-m-d H:i:s")));
                //
                //             $this->session->set_userdata('member_logged', $member['id']);
                //
                //             $array = array();
                //             $array['error'] = 0;
                //
                //         }
                //         else
                //         {
                //             $array = array();
                //             $array['error'] = 1;
                //             $array['message'] = "Your account has been banned.";
                //         }
                //
                //     }
                //
                // }

            }
            else
            {

                $array = array();
                $array['error'] = 1;
                $array['message'] = "Missing username and/or password";

            }

            return $array;

        }

    }