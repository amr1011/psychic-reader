<?php

class sitepackage extends CI_Model
{

    public static $is_full_ban = null;
    var $data = false;

    function __construct()
    {

        if ($this->session->userdata('member_logged'))
        {
            $this->data = $this->get_member_data($this->session->userdata('member_logged'));
            // $this->data['member_id_hash'] = $this->generate_member_id_hash($this->data['id'], $this->data['registration_date']);
        }
    }

    function getPackages()
    {

        return  $this->db->query("SELECT * FROM packages ORDER BY packages.type")->result_array();


    }

}
