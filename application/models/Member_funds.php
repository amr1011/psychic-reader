<?php

class member_funds extends member
{
    /*
     * Define Class Constants
     */

    const TYPE_READING = 'reading';
    const TYPE_EMAIL = 'email';
    const ERROR_MSG_NOFUNDS = 'Insufficient funds, please purchase more.';

    /*
     * Pay Reader
     * An easy functiont to fund a readers account more efficiently :^)
     * 
     * type: email or chat
     * total: float that defines the total to pay the reader (do NOT include commissions, they will be calcualted here)
     * summary: Summarty of the charge
     * region: the client's region (either US or CA, the rest really don't matter)
     * 
     */

    /*
      public function pay_reader_chat($chat_id, $client_region)
      {

      // Get chat info
      $chat = $this->db->query("SELECT * FROM chats WHERE id = {$chat_id} LIMIT 1")->row_array();

      // Calcuate how much to pay the reader
      $length_in_seconds = $chat['length'];
      $reader_per_second = ($this->calculate_chat_commission($client_region)/60);
      $total_to_pay = round(($length_in_seconds*$reader_per_second), 2);

      // Pay the reader
      $this->pay_reader('chat', $total_to_pay, "Chat Session: {$chat['session_id']}", $client_region);

      }
     */

    /*
      public function pay_reader($type, $total, $summary = "", $region)
      {

      // Check service type
      switch($type)
      {

      case "email":

      // Get the percentage of funds the reader gets to keep
      $total_funds_to_deposit = $this->site->settings['reader_email_percentage'] * $total;

      // Fund Readers Account minus commission
      $this->fund_account('payment', 'regular', $total_funds_to_deposit, $region);

      // Insert transaction minus commission
      $this->insert_transaction('earning', $total_funds_to_deposit, $summary);

      break;

      case "chat":

      // Fund Readers Account minus commission
      $this->fund_account('payment', 'regular', $total, $region);

      // Insert transaction minus commission
      $this->insert_transaction('earning', $total, $summary);


      break;

      }

      }
     */

    /**/

    // region - client region
    function calculate_chat_commission($region, $tier = null)
    {
        // Determin what commission to subtract from the user
        if ($tier == 'promo')
        {
            return $this->site->settings['promo_minutes_price'];
        } else
        {
            if (trim(strtolower($region)) == 'ca')
            {
                // From Canada
                return $this->site->settings['canadian_readers_chat_price'];
            } else
            {
                // Not from Canada
                if ($this->member->data['legacy_member'])
                {
                    // Legacy members get treated differently
                    return $this->site->settings['legacy_readers_chat_price'];
                } else
                {
                    // Standard members
                    return $this->site->settings['standard_readers_chat_price'];
                }
            }
        }
    }

    /*
     * Insert Transaction
     * 8/14/2014 Rob testing code, PayPal IPN code not inserting into dep
     */

    public function insert_transaction($type, $amount, $region, $summary, $ptype = null, $currency = '$', $transaction_id=null, $chat_id=null, $reader_id=null) //last work here
    {
        // Build array to insert into transactions table
        $insert = array();
        $insert['member_id'] = $this->member->data['id'];
        $insert['datetime'] = date("Y-m-d H:i:s");
        $insert['type'] = $type; // earning, purchase, payment, consume
        $insert['amount'] = $amount;
        $insert['summary'] = $summary;
        $insert['region'] = $region;
        $insert['currency'] = $currency;
        $insert['payment_type'] = ($ptype ? $ptype : NULL);
        $insert['transaction_id'] = $transaction_id;
        $insert['chat_id'] = $chat_id;
        $insert['reader_id'] = $reader_id;

        $this->db->insert('transactions', $insert);
    }

    /*
     * Insert Deposit
     * 8/14/2014 Rob added function for using in paypal_ipn code, as payments are not auth'd
     */

    public function insert_deposit($amount, $order_numb, $notes, $currency, $chat_id='', $reader_id=null)
    {
        // Build array to insert into transactions table
        $insert = array();
        $insert['Client_id'] = $this->member->data['id'];
        $insert['Amount'] = $amount;
        $insert['Order_numb'] = $order_numb;
        $insert['Date'] = date("Y-m-d H:i:s");
        $insert['Notes'] = $notes;
        $insert['Currency'] = $currency;
        if($chat_id != '')
        {
            $insert['Sessions'] = $chat_id;
        }

        if($reader_id != null)
        {
            $insert['Reader_id'] = $reader_id;
            $reader = $this->db->query("SELECT username FROM members WHERE `id`={$reader_id}")->row_array();
            $insert['Bmt'] = $reader['username'];
        }

        $this->db->insert('deposits', $insert);
    }

    /*
     * Fund Account
     */

    public function fund_account($type, $tier, $total, $transaction_id = null, $region = null)
    {
        $insert = array();
        $insert['member_id'] = $this->member->data['id'];
        $insert['datetime'] = date("Y-m-d H:i:s");
        $insert['type'] = $type;
        $insert['tier'] = $tier;
        $insert['total'] = $total;
        $insert['balance'] = $total;
        $insert['transaction_id'] = $transaction_id;

        // Find out the region this fund is coming from
        // If not passed in, get the user's current region
        if ($region)
        {
            $insert['region'] = $region;
        } else
        {
            $insert['region'] = (trim(strtolower($this->member->data['country'])) == 'ca' ? "ca" : "us");
        }

        // Insert balance
        $this->db->insert('member_balance', $insert);


        // Calculate return array
        $array = array();
        $array['error'] = '0';
        $array['transaction_id'] = $this->db->insert_id();

        // Return calculated array :P
        return $array;
    }

    public function get_last_balance_currency($member_id = null)
    {
        if (!$member_id)
            $member_id = $this->member->data['id'];

        $t = $this->db->query
                        ("
            
                SELECT
                    region
                FROM
                    member_balance
                WHERE
                    member_id = {$member_id} 
                    
                ORDER BY id DESC
                LIMIT 1
                    
            ")->row();
        if (empty($t))
        {
            // default to US.
            return "us";
        } else
        {
            return $t->region;
        }
    }

    /*
     * Use Email Funds
     */

    public function use_email_funds($funds, $email_id)
    {
        if ($this->email_balance() < $funds)
        {

            $array = array();
            $array['error'] = '1';
            $array['message'] = self::ERROR_MSG_NOFUNDS;
        } else
        {

            $array = $this->use_funds($funds, self::TYPE_EMAIL, $email_id);
        }

        return $array;
    }

    /*
     * Use Reading Funds
     */

    public function use_reading_funds($funds, $chat_id, $reader_id, $region)
    {
        if ($this->minute_balance() < $funds)
        {
            $array = array();
            $array['error'] = '1';
            $array['message'] = self::ERROR_MSG_NOFUNDS;
        } else
        {

            $array = $this->use_funds($funds, self::TYPE_READING, $chat_id, $reader_id, $region);
        }

        return $array;
    }
    
    public function is_allow_freetime($client_id, $reader_id, $allow_max_readers, $allow_max_free_mins = 7)
    {
        $sql = "
            SELECT 
            (select b.free_time_used from chats b where b.reader_id = a.reader_id order by b.free_time_used desc limit 1) as max_time,
            a.* 
            FROM chats a 
            WHERE a.client_id = '".$client_id."'
            AND a.start_datetime BETWEEN '".date("Y-m-01 00:00:00")."' AND '".date("Y-m-t 00:00:00")."'
            GROUP BY a.reader_id";        
        
        $query = $this->db->query($sql);
        if ($query->num_rows() >= $allow_max_readers)
        {
            $not_allowed = true;
            foreach($query->result() as $row)
            {
                if ($row->max_time < (($allow_max_free_mins * 60) - 10) )
                {
                    $not_allowed = false;
                }
            }
            
            if ($not_allowed)
            {
                return 0;
            }
        }
        
        $sql = "
            SELECT * FROM chats a 
            WHERE a.client_id = '".$client_id."'
            AND a.reader_id = '".$reader_id."'
            AND start_datetime BETWEEN '".date("Y-m-01 00:00:00")."' AND '".date("Y-m-t 00:00:00")."'
            ORDER BY a.free_time_used DESC LIMIT 1";                
        
        $query = $this->db->query($sql);
        
        if ($query->num_rows() > 0)
        {
            $free_time_used = $query->row()->free_time_used;
            if ($free_time_used > (($allow_max_free_mins * 60) - 10) )
            {
                return 0;
            }
        }
        
        return 1;
    }

    /*
     * Get users minute balance
     */

    public function minute_balance($memberid = null)
    {

        if (!$memberid)
            $memberid = $this->member->data['id'];

        $t = $this->db->query
                        ("
				SELECT
					SUM(total)-SUM(used) as totalMinutes
					
				FROM
					member_balance
					
				WHERE
					member_id = {$memberid} AND
					`type` = 'reading' AND
                                        tier = 'regular'
				
					
				ORDER BY
					id
					
			")->row_array();

        return (!$t['totalMinutes'] ? '0' : $t['totalMinutes']);
    }
    
    public function free_minute_balance($memberid = null)
    {

        if (!$memberid)
            $memberid = $this->member->data['id'];

        $t = $this->db->query
                        ("
				SELECT
					SUM(total)-SUM(used) as totalMinutes
					
				FROM
					member_balance
					
				WHERE
					member_id = {$memberid} AND
					`type` = 'reading' AND
                                        (tier = 'free' OR tier = 'half') 
					
				ORDER BY
					id
					
			")->row_array();

        return (!$t['totalMinutes'] ? '0' : $t['totalMinutes']);
    }

    /*
     * Get users email balance
     */
    public function email_balance()
    {
        $t = $this->db->query
                        ("
				SELECT
					SUM(total)-SUM(used) as totalEmails
					
				FROM
					member_balance
					
				WHERE
					member_id = {$this->member->data['id']} AND
					type = 'email' 
					
				ORDER BY
					id
					
			")->row_array();

        return (!$t['totalEmails'] ? '0' : $t['totalEmails']);
    }


     /*
     * Get Phone balance
     */
    public function phone_balance()
    {
        $t = $this->db->query
                        ("
				SELECT
					SUM(total)-SUM(used) as totalPhone
					
				FROM
					member_balance
					
				WHERE
					member_id = {$this->member->data['id']} AND
					type = 'phone' 
					
				ORDER BY
					id
					
			")->row_array();

        return (!$t['totalPhone'] ? '0' : $t['totalPhone']);
    }

    /*
      function give_timeback_DEPRECATED($type,$timeback)
      {
      $bal =            $this->db->query("select *
      from   member_balance
      where  member_id = {$this->member->data['id']}
      and balance > 0
      order by tier = 'promo' desc limit 1")->row_array();

      if($type == 'free')
      {


      if($bal['tier'] == 'free' && $bal['used'] > $timeback)
      {
      $upd['balance'] = $bal['balance'] + $timeback;
      $upd['used'] = $bal['used'] - $timeback;

      $this->db->where('id',$bal['id']);
      $this->db->update('member_balance',$upd);
      }
      else
      {
      $this->fund_account('reading','free',$timeback);
      }
      }
      else
      {
      if($bal['used'] > $timeback)
      {
      $upd['balance'] = $bal['balance'] + $timeback;
      $upd['used'] = $bal['used'] - $timeback;

      $this->db->where('id',$bal['id']);
      $this->db->update('member_balance',$upd);
      }
      }



      }
     */

    function process_reading($funds,$free_funds, $type = null, $type_id = null, $reader_id = null,$client_id = null, $region = null)
    {
        // Build WHERE clause
        // $this->db->where(array
        // (
        //     'member_id' => $this->member->data['id'],
        //     'type' => $type,
        //     'balance >' => 0
        // ));
        //
        // // Build WHERE clause
        // $this->db->where(array
        //     (
        //     'member_id' => $this->member->data['id'],
        //     'type' => $type,
        //     'balance >' => 0
        // ));
        //
        // $this->db->update("member_balance", ["used"=>$funds, "balance" => $funds]);
        //
        // $this->db->order_by("tier = 'promo' desc");
        // $this->db->order_by("id");
        //
        // $this->db->update("member_balance", ["used"=>$funds, "balance" => $funds]);

        // $total = 0;

        // update free minutes
        $data_free_minute = $this->db->query("SELECT * FROM `member_balance` WHERE member_id = '{$client_id}' AND `type`='reading' AND (`tier`='half' OR `tier`='free') AND `balance` > 0 ORDER BY id ASC LIMIT 1")->row_array();
        $free_used = (float)$data_free_minute['used'] + $free_funds;
        $free_balance = (float)$data_free_minute['balance'] - $free_funds;

        $data = array(
            'used'=>$free_used,
            'balance' => $free_balance
        );

        $this->db->where('id', $data_free_minute['id']);
        $this->db->update('member_balance', $data);

        // update paid minutes
        $paid_minute_data = $this->db->query("SELECT * FROM `member_balance` WHERE member_id = '{$client_id}' AND `type`='reading' AND (`tier`='promo' OR `tier`='regular') AND balance > 0 ORDER BY id ASC LIMIT 1")->row_array();
        $paid_used_amount = (float)$paid_minute_data['used'] + $funds;
        $paid_balance_amount = (float)$paid_minute_data['balance'] - $funds;

        $this->db->where('id', $paid_minute_data['id']);
        $this->db->update('member_balance', array(
            'used' => $paid_used_amount,
            'balance' => $paid_balance_amount
        ));



        // $sum_paid_balance = (float)$this->minute_balance($client_id);
        // $sum_free_balance = (float)$this->free_minute_balance($client_id);
        //
        // if($sum_free_balance > 0) {
        //
        //     if($funds <= $sum_free_balance) {
        //         $data_free_minute = $this->db->query("SELECT * FROM `member_balance` WHERE member_id = '{$client_id}' AND `type`='reading' AND (`tier`='half' OR `tier`='free') AND `balance` > 0 ORDER BY id ASC LIMIT 1")->row_array();
        //         $free_used = (float)$data_free_minute['used'] + $funds;
        //         $free_balance = (float)$data_free_minute['balance'] - $funds;
        //
        //         $data = array(
        //             'used'=>$free_used,
        //             'balance' => $free_balance
        //         );
        //
        //         $this->db->where('id', $data_free_minute['id']);
        //         $this->db->update('member_balance', $data);
        //
        //     } else {
        //
        //         $left_free_minute_balance = (float)$this->free_minute_balance($client_id);
        //         $paid_minute_funds = $funds - $left_free_minute_balance;
        //
        //
        //         $left_free_minutes = $this->db->query("SELECT * FROM `member_balance` WHERE member_id = '{$client_id}' AND `type`='reading' AND (`tier`='half' OR `tier`='free')")->result_array();
        //
        //         foreach($left_free_minutes as $left_free_minute) {
        //
        //             $free_used_data = (float)$left_free_minute['total'];
        //             $free_balance_data = 0;
        //
        //             $this->db->where('id', $left_free_minute['id']);
        //             $this->db->update('member_balance', array(
        //                 'used' => $free_used_data,
        //                 'balance' => $free_balance_data
        //             ));
        //         }
        //
        //
        //         if($sum_paid_balance > 0) {
        //
        //             $data_paid_minute = $this->db->query("SELECT * FROM `member_balance` WHERE member_id = '{$client_id}' AND `type`='reading' AND (`tier`='promo' OR `tier`='regular') AND `balance` > 0 ORDER BY id ASC LIMIT 1")->row_array();
        //
        //             $paid_used = (float)$data_paid_minute['used'] + $paid_minute_funds;
        //             $paid_balance = (float)$data_paid_minute['balance'] - $paid_minute_funds;
        //
        //             $this->db->where('id', $data_paid_minute['id']);
        //             $this->db->update('member_balance', array(
        //                 'used' => $paid_used,
        //                 'balance' => $paid_balance
        //             ));
        //
        //         }
        //     }
        // } else if($sum_free_balance <= 0) {
        //
        //     if($sum_paid_balance > 0) {
        //
        //         if($funds > $sum_paid_balance) {
        //             $funds = $sum_paid_balance;
        //         }
        //
        //         $paid_minute_data = $this->db->query("SELECT * FROM `member_balance` WHERE member_id = '{$client_id}' AND `type`='reading' AND (`tier`='promo' OR `tier`='regular') AND balance > 0 ORDER BY id ASC LIMIT 1")->row_array();
        //         $paid_used_amount = (float)$paid_minute_data['used'] + $funds;
        //         $paid_balance_amount = (float)$paid_minute_data['balance'] - $funds;
        //
        //         $this->db->where('id', $paid_minute_data['id']);
        //         $this->db->update('member_balance', array(
        //             'used' => $paid_used_amount,
        //             'balance' => $paid_balance_amount
        //         ));
        //     }
        // }

        // $this->db->order_by("tier = 'promo' desc");
        // $this->db->order_by("id");

        $total = 0;


        // Get matching transactions
        if ($transactions = $this->db->get('member_balance')->result())
        {

            // Start transaction
            $this->db->trans_start();

            // Loop through transactions
            foreach ($transactions as $t) {

                //--- Update transaction when time has started being used
                //--- NOTE: Marks time as used wether paid or freebie minutes

                if ($t->transaction_id)
                {
                    $this->db->where('id', $t->transaction_id);
                    $this->db->update('transactions', array('time_used' => 1));
                }

                // Get lesser value of remaining balance and current credits
                $difference = $funds > $t->balance ? $t->balance : $funds;
                // $funds -= $difference;

                //$this->log_chat_transaction($funds,$type,$reader_id,$region);
                //Update reader table.
                $readerInsert['datetime'] = date("Y-m-d G:i:s");
                $readerInsert['reader_id'] = $reader_id;
                $readerInsert['type'] = $type;
                $readerInsert['type_id'] = $type_id;
                $readerInsert['amount'] = $difference;
                $readerInsert['tier'] = $t->tier;

                $readerInsert['region'] = $region;

                if ($type == 'reading')
                {

                    $reader_per_second = $this->calculate_chat_commission($region, $t->tier);
                    $readerInsert['commission'] = round(($difference * $reader_per_second), 2);
                    $readerInsert['commission_rate'] = $reader_per_second;
                } else
                {

                    $readerInsert['commission'] = $difference;
                }

                $total += $readerInsert['commission'];

                $this->db->insert('profile_balance', $readerInsert);
                //Create Update records


                // Are we done yet?
                if ($funds <= 0)
                    break;
            }

            // Complete transaction
            $this->db->trans_complete();
        }


        // Create a successful return
        $array = array();
        $array['error'] = '0';
        $array['total'] = $total;

        return $array;
    }
    /*pen 10/14*/
    function fund_amount_day($member_id)
    {
        $now = date("Y-m-d");
        $tomorrow = date("Y-m-d",strtotime("tomorrow"));

        $total_fund_today = 0;

        $funds_today = $this->db->query("SELECT * FROM deposits WHERE Client_id = '{$member_id}' AND `Date` >'{$now}' AND `Date`<'{$tomorrow}'")->result_array();
        foreach($funds_today as $fund)
        {
            $total_fund_today =  $total_fund_today + $fund['Amount'];
        }

        return $total_fund_today;
    }

    function fund_amount_month($member_id)
    {
        $now = date("Y-m-d");

        $month_start = date("Y-m-01", strtotime($now));
        $month_end = date("Y-m-t", strtotime($now));
        $total_fund_month = 0;

        $funds_month = $this->db->query("SELECT * FROM deposits WHERE Client_id = '{$member_id}' AND `Date` > '{$month_start}' AND `Date` < '{$month_end}'")->result_array();
        foreach($funds_month as $fund)
        {
            $total_fund_month = $total_fund_month + $fund["Amount"];
        }

        return $total_fund_month;
    }

    function get_fund_limit_day($member_id)
    {
        $member_data = $this->db->query("SELECT * FROM members WHERE id = '{$member_id}'")->row_array();
        $fund_amount_day = $member_data["fund_amount_day"];

        return $fund_amount_day;
    }

    function get_fund_limit_month($member_id)
    {
        $member_data = $this->db->query("SELECT * FROM members WHERE id = '{$member_id}'")->row_array();
        $fund_amount_month= $member_data["fund_amount_month"];

        return $fund_amount_month;
    }

}
