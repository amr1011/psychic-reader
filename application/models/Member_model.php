<?php

class Member_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get page by id
     */
    function get_member($id)
    {
        return $this->db->get_where('members',array('id'=>$id))->row_array();
    }
        
    /*
     * Get all members
     */
    function get_all_members()
    {
        $this->db->order_by('id', 'desc');
        return $this->db->get('members')->result_array();
    }
        
    
}