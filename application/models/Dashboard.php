<?php

class dashboard extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    

   public function get_single_transaction($id)
    {
        $sql = "SELECT c.username,c.dob,c.country,c.first_name as client_fname ,c.last_name as client_lname,r.first_name as reader_fname,r.last_name as reader_lname,t.settled,t.amount,t.payment_type FROM transactions t 
            LEFT JOIN members c 
            ON t.member_id = c.id
            LEFT JOIN members r  
            ON t.reader_id = r.id
            WHERE t.id = $id
            ";

        $query = $this->db->query($sql);
        return $query->row();  
    }
        
   
}