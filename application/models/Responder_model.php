<?php


class responder_model extends CI_Model
{

    function __construct()
    {

        if ($this->session->userdata('member_logged')) {
            $this->data = $this->member->get_member_data($this->session->userdata('member_logged'));
        }
    }

    function get_email_form($page)
    {
        $data = $this->db->query("SELECT * FROM email_forms WHERE page='{$page}'")->result_array();
        return $data;
    }

    function get_email_form_detail($form_id)
    {
        $data = $this->db->query("SELECT * FROM email_forms_detail WHERE form_id={$form_id}")->result_array();
        return $data;
    }

}
