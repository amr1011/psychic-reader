<?php

class messages_model extends CI_Model
{

    var $data = false;

    function __construct()
    {

        if($this->session->userdata('member_logged'))
        {

            $this->data = $this->member->get_member_data($this->session->userdata('member_logged'));

        }

    }

    function getMessage($message_id)
    {
        $getM = $this->db->query("select *
                                  from   messages m
                                  where  m.id = {$message_id}");
        return $getM -> row_array();
    }

    function markMessageRead($message_id)
    {
        if(isset($this->data['id']))
        {
            $m = $this->getMessage($message_id);

            //only recipient can mark message Read.
            if($m['ricipient_id'] == $this->data['id'])
            {
                if($m['name'] == 'no_reader')
                {
                    $update['read'] = 1;
                    $this->db->where("name",$m['name']);
                    $this->db->update("messages",$update);
                }
                $update['read'] = 1;
                $this->db->where("id",$message_id);
                $this->db->update("messages",$update);

                return $this->db->affected_rows();

            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    function notify($sender_id = null,
                    $recipient_id = null,
                    $subject = null,
                    $message = null,
                    $type = null,
                    $email_template ='member_notification')
    {

        // Get reader
        $sender = $this->member->get_member_data($sender_id);
        $recipient = $this->member->get_member_data($recipient_id);

        $params = $recipient;

        if($sender)
        {
            $params['sender_username'] = $sender['username'];
            $params['sender_id'] = $sender_id;
        }
        else
        {
            $params['sender_id'] = null;
        }


        $params['subject'] = $subject;
        $params['message'] = $message;
        $params['type'] = $type;

        $params['recipient_id'] = $recipient_id;

        $this->system_vars->m_omail($params['id'],$email_template,$params);

        return true;

    }

    function getAdminMessages()
    {
        $getMessages = $this->db->query("select   *
                                           from   messages m
                                           where  type = 'admin'
                                                  and ricipient_id = 1");
        return $getMessages -> result_array();
    }

    function sendAdminMessage($ricipient_id,$subject,$message)
    {
        $params = array();
        $params['type'] = "admin";
        $params['ricipient_id'] =  $ricipient_id;

        $m = $this->member->get_member_data($ricipient_id);
        $params['username'] = $m['username'];
        $params['subject'] = $subject;
        $params['message'] =  $message;
        $params['sender_id'] = 1;

        $this->system_vars->m_omail($ricipient_id,"admin_notification_template",$params);
    }

    function getUnreadMessages()
    {
        if(isset($this->data['id']))
        {
            $getUnread = $this->db->query("select count(id) as unread
                                           from   messages m
                                           where  m.ricipient_id = {$this->data['id']}
                                           and m.read != 1
                                           and sender_id is not null
                                           and (m.type<>'email' or m.type is null)");

            $total = $getUnread->row_array();
            return $total['unread'];
        }
        else
        {
            return 0;
        }
    }

    function getUnreadSystemMessages()
    {
        if(isset($this->data['id']))
        {
            $getUnread = $this->db->query("select count(id) as unread
                                           from   messages m
                                           where  m.ricipient_id = {$this->data['id']}
                                           and m.read != 1
                                           and m.type='email'");

            $total = $getUnread->row_array();
            return $total['unread'];
        }
        else
        {
            return 0;
        }
    }
}
?>