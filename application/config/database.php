<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$active_group = 'development';
$query_builder = TRUE;

if(!empty($_SERVER['ENVIRONMENT'])){
    $active_group = $_SERVER['ENVIRONMENT'];
}

//--- Production Credentials (CURRENTLY DOESN'T EXIST)
// $db['production']['hostname'] = 'localhost';
// $db['production']['username'] = 'devpsy';
// $db['production']['password'] = 'devpsy';
// $db['production']['database'] = 'devpsy';
// $db['production']['dbdriver'] = 'mysqli';
// $db['production']['dbprefix'] = '';
// $db['production']['pconnect'] = FALSE;
// $db['production']['db_debug'] = TRUE;
// $db['production']['cache_on'] = FALSE;
// $db['production']['cachedir'] = '';
// $db['production']['char_set'] = 'utf8';
// $db['production']['dbcollat'] = 'utf8_general_ci';
// $db['production']['swap_pre'] = '';
// $db['production']['autoinit'] = TRUE;
// $db['production']['stricton'] = FALSE;

//--- Staging Credentials (dev.psychic-contact.com)
$db['development']['hostname'] = 'localhost';
$db['development']['username'] = 'root';
$db['development']['password'] = '';
$db['development']['database'] = 'dev';
$db['development']['dbdriver'] = 'mysqli';
$db['development']['dbprefix'] = '';
$db['development']['pconnect'] = FALSE;
$db['development']['db_debug'] = TRUE;
$db['development']['cache_on'] = FALSE;
$db['development']['cachedir'] = '';
$db['development']['char_set'] = 'utf8';
$db['development']['dbcollat'] = 'utf8_general_ci';
$db['development']['swap_pre'] = '';
$db['development']['autoinit'] = TRUE;
$db['development']['stricton'] = FALSE;



/* End of file database.php */
/* Location: ./application/config/database.php */