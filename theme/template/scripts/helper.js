$(document).ready(function () {
    var mytimer = setInterval("get_online_readers()", 10000);    
});

function get_online_readers()
{
    $.post(SITE_URL + "main/display_online_readers", {}, function (data) {
        if (data.status == "OK")
        {
            $("#div-online-readers").html(data.html);
        }
    }, 'json');
}
