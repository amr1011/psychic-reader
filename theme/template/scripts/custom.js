$(document).ready(function() {
	$(".btn-call").on('click', function(event) {
    	event.preventDefault();
      var button = $(this);
      var telephone = "#telephone_" + $(this).attr('data-reader-id');
      var help = "#help-" + $(this).attr('data-reader-id') ;
      button.prop('disabled',true);
      $(help).text("Please wait while we connect your call.");
	     $.ajax({
           type:"POST",
           data:{
                "telephone": $(telephone).val(),
                "reader": $(this).attr('data-reader-id'),
                "member": $(this).attr('data-member-id')
            },
           url: '/phone/initiateCall',
           dataType: "json",
            success: function(data) {
                // Run the code here that needs
                // to access the data returned
                if (data.Response == "Error") 
                {
                  $(help).text("Failed to connect please input a valid mobile or telephone");
                }
                else 
                {
                  $(help).text("Your call is now connected");
                }
                button.prop('disabled',false);
            },
            error: function() {
                alert('Error occured');
                button.prop('disabled',false);
            }

        });
	});
});