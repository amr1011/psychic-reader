<?php
error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);

/*

dev.psychic-contact.com/modules/employment_form/logging/logcron.php?view=1

cron file for employment page, load logs from db table, generate email, and send to admin

clear table after

hardcoded because it's a cron task script

*/


// DB VARS
$host       = "localhost";
$dbusername = "devpsy_main";
$dbpassword = '7%ciRB$R';
$database   = "devpsy_main";


// set to 0 to use live emails
$testing = 0;

$link = mysql_connect("$host", "$dbusername", "$dbpassword") or die("Could not connect");
mysql_select_db("$database") or die("Could not select database");


main();

function main()
{

	$output = "Here is your employment page report for ".date("F d, Y");
	
	$query = "SELECT COUNT(*) FROM logging";
	$result = mysql_query($query);		
	$row  = mysql_fetch_array($result);
	$count = $row[0];
	
	$output .= "<p>Total unique visits by IP: $count</p><ul>";
	
	$query = "SELECT * FROM logging ORDER BY ip_address";
	$result = mysql_query($query);		
	while ($row  = mysql_fetch_array($result))
	{
		$output .= "<li>".$row['ip_address']."</li>";
	}// end while
	
	$output .= "</ul>";
	//debug("output",$output);

	sendEmail($output);
	
	if (!$_REQUEST['view'])
	{
		clearTable();
	}
	else
	{
		debug("output",$output);
	}

}// end


function sendEmail($message)
{

	global $testing;
	
	$subject      = "DevPsy :: Employment Page Stats";
	$site_name    = "Psychic-Contact";
	
	if ($testing)
	{
		$site_email   = "customcodinggroup@gmail.com";
	}
	else
	{
		$site_email   = "no-reply@psychic-contact.com";
	
	}
	
	$admin_name1  = "Jayson";
	
	if ($testing)
	{
		$admin_email1   = "customcodinggroup@gmail.com";
	}
	else
	{
		$admin_email1   = "rosie6807@gmail.com";
	}

	$admin_name2  = "Diana";
	
	if ($testing)
	{
		$admin_email2 = "robert.dunsmuir@gmail.com";
	}
	else
	{
		$admin_email2 = "dianahamster67@gmail.com";
	}	
	
	$headers  = "";
	$headers .= "MIME-Version: 1.0\n";
	$headers .= "Content-type: text/html; charset=iso-8859-1\n";
	$headers .= "From: $site_name <$site_email>\n";
	$headers .= "To: $admin_name1 <$admin_email1>\n";
	$headers .= "CC: $admin_name2 <$admin_email2>\n";
	$headers .= "Reply-To: $site_name <$site_email>\n";
	$headers .= "Return-Path: $site_name <$site_email>\n";
	$headers .= "X-Mailer: PHP/" . phpversion() ."\n";

	//debug("message",$message);

	if (!$_REQUEST['view'])
	{
		mail("", $subject, $message,$headers);
		echo "completed";
	}

}// end


function clearTable()
{

	$query = "TRUNCATE logging";
	$sql_query = mysql_query($query) or die (mysql_error());
	mysql_query("COMMIT");

}// end


function debug($name,$value)
{
	echo "<br>*$name*/*$value*";
}// end


?>