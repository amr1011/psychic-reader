<?php

/*
Quickly developed application form module to be included in the following pages

Could you make sure the following is changed on these pages:

http://dev.psychic-contact.com/advanced/site/employment

http://dev.psychic-contact.com/chat/info.php?page=employment

http://dev.psychic-contact.ca/chat/info.php?page=employment


// error rendering - for testing
*/

error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);

// log daily visitors
include "logging/logfile.php";

// set to 0 to make live addresses active
$testing = 0;

//Global Vars
$site_name       = "Psychic-Contact.com";
$site_email      = "no-reply@psychic-contact.com";

$admin_name1     = "Jayson";
if (!$testing)
{
	//$admin_email1    = "javachat@psychic-contact.com";
	$admin_email1    = "rosie6807@gmail.com";
	$admin_email3    = "customcodinggroup@gmail.com";
}
else
{
	$admin_email1    = "customcodinggroup@gmail.com";
}

$admin_name2     = "Diana";
if (!$testing)
{
	$admin_email2    = "dianahamster67@gmail.com";
}
else
{
	$admin_email2    = "robert.dunsmuir@gmail.com";
}

$subject         = "Psychic Reader Application";
$success_message = "Thank you for submitting your Psychic Contact Reader Application- We will review it shortly.  If we feel your application meets all our requirements, we will contact you within 3-5 days.";

$thanks_subject = "Thank You for Submitting a Psychic Reader Application";
$thanks_email_heading = "Thank you for submitting your application to www.psychic-contact.com.  Here is a copy of the information you submitted.";

$email_heading = "You have received a new employment application from www.psychic-contact.com:";

$table_start = "<table width='600' border='1' cellpadding='5' cellspacing='5'>";
$table_end = "</table>";

// if you update the form, add to this array for error checking
$required_fields = array("g-recaptcha-response","required_first-name","required_last-name","required_email-address","required_phone-number","required_date-of-birth","required_ssn-sin","required_reading-experience","required_blog-links");

$message_string = "&nbsp;";

/*  DO NOT EDIT BELOW THIS LINE UNLESS YOU KNOW WHAT YOU'RE DOING */

main();

/*
main function, either shows the content & form, or processes the submission, sending an email, displays a message to the user
*/
function main()
{
	
	global $success_message;

	if ($_REQUEST['action'] == "posted")
	{

		$errs = errorCheck();

		if ($errs)
		{
			// show errors
			echo  "<p><span style='color:red;'>The following fields were not filled in correctly: $errs</span>";
		}
		else
		{
			// email submission
			submitForm();

			// show success
			echo  "<p><span style='color:green;'>$success_message</span>";
		}

	}
	else
	{

		// content
		$content = readTemplate('content');

		$content = str_replace("<--YEAR-->",date("Y"),$content);
		$content = str_replace("<--MESSAGE-->",$message_string,$content);

		$form = readTemplate('form');

		echo $content;
		echo $form;

	}


}// end


/*
checks for errors, returns a error line to the user
may not run, as the form as javascript error checking as well
*/
function errorCheck()
{
	global $required_fields;

	$errs = "";

	foreach ($required_fields as $key)
	{
		
		if ($_REQUEST[$key] == "")
		{
			if ($key == "g-recaptcha-response")
			{
				$errmsg = "Captcha Verification Code, ";
				$errs .= $errmsg;
			}
		}

		if ($key == "required_email-address")
		{
			if (eregi('^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z.]{2,5}$', $_REQUEST[$key])) {
				// valid format provided
			} else {
				$errmsg = "Invalid email format  ";
				$errs .= $errmsg;
			}
		}
		

	}// end for
	
	$errs = substr($errs, 0, -2);

	return $errs;

}// end

/*
creates the email content and sends to admin(s)
*/
function submitForm()
{

	global $site_name,$site_email,$admin_name1,$admin_email1,$admin_name2,$admin_email2,$subject,$email_heading,$table_start,$table_end;
	
	$sender_name = $_REQUEST['required_first-name']." ".$_REQUEST['required_last-name'];
		
	// ignore these, add to the array is odd values show up in emails
	$ignore_array = array();
	$ignore_array['action'] = 1;
	$ignore_array['filter_month'] = 1;
	$ignore_array['filter_year'] = 1;
	$ignore_array['filter_period'] = 1;
	$ignore_array['PHPSESSID'] = 1;
	$ignore_array['g-recaptcha-response'] = 1;
	$ignore_array['Reader_id'] = 1;
	
	$headers  = "";
	$headers .= "MIME-Version: 1.0\n";
	$headers .= "Content-type: text/html; charset=iso-8859-1\n";
	$headers .= "From: $sender_name <".$_REQUEST['required_email-address'].">\n";
	$headers .= "To: $admin_name1 <$admin_email1>\n";
	$headers .= "CC: $admin_name2 <$admin_email2>\n";
	$headers .= "Reply-To: $site_name <$site_email>\n";
	$headers .= "Return-Path: $site_name <$site_email>\n";
	$headers .= "X-Mailer: PHP/" . phpversion() ."\n";

	$message = "";

/*	
	debug("captcha",$_REQUEST['g-recaptcha-response']);
	exit();
*/	
	foreach ($_REQUEST as $key => $value)
	{
		$t_key = $key;
		$t_key = str_replace("required_","",$t_key);
		$t_key = str_replace("-"," ",$t_key);
		$t_key = str_replace("_"," ",$t_key);
		
		$value = stripslashes($value);
	
		// create lists
		if ($key == "days-available")
			$value = implode("<br>",$_REQUEST['days-available']);

		if ($key == "areas-of-expertise")
			$value = implode("<br>",$_REQUEST['areas-of-expertise']);

		
		if (!$ignore_array[$key])
		$message .= "<tr><td><b>".ucfirst($t_key)."</b></td><td>$value</td></tr>";
	
	}

	$message = $table_start.$message.$table_end;
	
	$message = $email_heading."<br><br>".$message;

	mail("", $subject, $message,$headers);
	
	sendThanks();

}// end


function sendThanks()
{

	global $site_name,$site_email,$admin_name1,$admin_email1,$admin_name2,$admin_email2,$thanks_subject,$thanks_email_heading,$table_start,$table_end;
	
	$sender_name = $_REQUEST['required_first-name']." ".$_REQUEST['required_last-name'];
		
	// ignore these, add to the array is odd values show up in emails
	$ignore_array = array();
	$ignore_array['action'] = 1;
	$ignore_array['filter_month'] = 1;
	$ignore_array['filter_year'] = 1;
	$ignore_array['filter_period'] = 1;
	$ignore_array['PHPSESSID'] = 1;
	$ignore_array['g-recaptcha-response'] = 1;
	$ignore_array['Reader_id'] = 1;
	
	$headers  = "";
	$headers .= "MIME-Version: 1.0\n";
	$headers .= "Content-type: text/html; charset=iso-8859-1\n";
	$headers .= "To: $sender_name <".$_REQUEST['required_email-address'].">\n";
	$headers .= "From: $admin_name1 <$admin_email1>\n";
	$headers .= "Reply-To: $site_name <$site_email>\n";
	$headers .= "Return-Path: $site_name <$site_email>\n";
	$headers .= "X-Mailer: PHP/" . phpversion() ."\n";

	$message = "";

/*	
	debug("captcha",$_REQUEST['g-recaptcha-response']);
	exit();
*/	
	foreach ($_REQUEST as $key => $value)
	{
		$t_key = $key;
		$t_key = str_replace("required_","",$t_key);
		$t_key = str_replace("-"," ",$t_key);
		$t_key = str_replace("_"," ",$t_key);
		
		$value = stripslashes($value);
	
		// create lists
		if ($key == "days-available")
			$value = implode("<br>",$_REQUEST['days-available']);

		if ($key == "areas-of-expertise")
			$value = implode("<br>",$_REQUEST['areas-of-expertise']);

		
		if (!$ignore_array[$key])
		$message .= "<tr><td><b>".ucfirst($t_key)."</b></td><td>$value</td></tr>";
	
	}

	$message = $table_start.$message.$table_end;
	
	$message = $thanks_email_heading."<br><br>".$message;

	mail("", $thanks_subject, $message,$headers);

	// new
	logApplication($sender_name,$_REQUEST['required_email-address'],$message);
	
	sendLogNotice();
	
}// end


function logApplication($name, $email, $message)
{

	$host       = "localhost";
	$dbusername = "devpsy_main";
	$dbpassword = '7%ciRB$R';
	$database   = "devpsy_main";

	$link = mysql_connect("$host", "$dbusername", "$dbpassword") or die("Could not connect2");
	mysql_select_db("$database") or die("Could not select database");

	$query =  "INSERT INTO logging_employment (name,email,content,subDate) VALUES (\"$name\",\"$email\",\"".addslashes($message)."\",NOW())";
	//debug("query",$query);
	$sql_query = mysql_query($query) or die (mysql_error());
	mysql_query("COMMIT");
	
}// end


function sendLogNotice()
{

	global $site_name,$site_email,$admin_name1,$admin_email1,$admin_name2,$admin_email2,$admin_name3,$admin_email3;

	$headers  = "";
	$headers .= "MIME-Version: 1.0\n";
	$headers .= "Content-type: text/html; charset=iso-8859-1\n";
	$headers .= "To: $admin_name1 <".$admin_email1.">\n";
	$headers .= "CC: $admin_name2 <".$admin_email2.">\n";
	$headers .= "BCC: $admin_name3 <".$admin_email3.">\n";
	$headers .= "From: $site_name <$site_email>\n";
	$headers .= "Reply-To: $site_name <$site_email>\n";
	$headers .= "Return-Path: $site_name <$site_email>\n";
	$headers .= "X-Mailer: PHP/" . phpversion() ."\n";

	mail("", "Employment App Logged", "An employement application was logged<p><b>View Employment Visits Log</b><br><a href=\"http://dev.psychic-contact.com/modules/employment_form/logging/admin.php
\">http://dev.psychic-contact.com/modules/employment_form/logging/admin.php</a><p><b>View Employment Applicant Submissions</b><br><a href=\"http://dev.psychic-contact.com/modules/employment_form/admin.php
\">http://dev.psychic-contact.com/modules/employment_form/admin.php</a>",$headers);


}// end

/*
used for loading text files for display in the page
*/
function readTemplate($filename)
{


	$filename = getcwd()."/".$filename.".txt";
	$handle = fopen($filename, "r");

	$contents = fread($handle, filesize($filename));

	fclose($handle);
	return $contents;

}// end

/*
for testing
*/
function debug($name,$value)
{
	echo "<br>*$name*/*$value*";
}// end

function showArray($arr)
{
	echo "<pre>";
	print_r($arr);
	echo "</pre>";
}// end


?>