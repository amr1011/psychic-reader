// Run useing:  mocha test_userManager.js

var expect = require('chai').expect;
var util = require('util');
var userManager = require('../manager/userManager');
var roomManager = require('../manager/roomManager');

var input_data = {
    reader_id: 13,
    client_id: 14
}

var stored_data = {};
var room_info = {};

describe('UserManager', function(){

    describe('Chat Room', function(){
       it ('Client dummy chat room should have chat_session', function(done) {
           userManager.createDummyChatSession(input_data, function(err, data){
               //console.log("Data is: " + util.inspect(data));
               expect(data.chat_session_id).to.exist;
               stored_data = data;
               console.log("Stored Data: " + util.inspect(stored_data));
               done();
           })
       }),
       it ('Validate reader member id and member_id_hash', function(done) {
           userManager.validate_member_id({member_id:stored_data.reader_id, member_id_hash:stored_data.reader_id_hash}, function(err, data){
               //console.log("Data is: " + util.inspect(data));
               expect(data.status).to.be.true;
               done();
           })
       }),
       it ('Validate client member id and member_id_hash', function(done) {
           userManager.validate_member_id({member_id:stored_data.client_id, member_id_hash:stored_data.client_id_hash}, function(err, data){
               //console.log("Data is: " + util.inspect(data));
               expect(data.status).to.be.true;
               done();
           })
       }),
       
       it ('Validate the chat room ', function(done) {
            userManager.validate({chat_id:stored_data.id, chat_session_id:stored_data.chat_session_id, member_hash:stored_data.client_hash}, function(err, data) {
                expect(data.chat_session_id).to.equal(stored_data.chat_session_id);
                done();
            });
        }),
        it ('Client create should return status=true', function(done) {
            room_info = {
                room_name: stored_data.chat_session_id,
                reader_id: stored_data.reader_id,
                reader_hash: stored_data.reader_hash,
                client_id: stored_data.client_id,
                client_hash: stored_data.client_hash
                
            }
            roomManager.createOrJoinRoom({room_data:room_info, member_type:'client'}, function(err, data){
                expect(data.status).to.be.true;
                expect(roomManager.rooms).to.have.length(1);
                done();
            })
        }),
        it ('Reader join room should return status=true', function(done) {
            roomManager.createOrJoinRoom({room_data:room_info, member_type:'reader'}, function(err, data){
                expect(data.status).to.be.true;
                expect(roomManager.rooms).to.have.length(1);
                done();
            })
        }),
        it ('Start timing for room. status=true', function(done) {
           roomManager.startRoomTimer({room_name:room_info.room_name}, function(err, data){
               expect(data.status).to.be.true;
               expect(data.is_pause).to.be.false;
               expect(roomManager.rooms).to.have.length(1);
               done();
           })
        }),
        it ('Record the chat length after 2 seconds delay', function(done) {
            // let say after 2 s
            this.timeout(5000);
            setTimeout( function() {
                userManager.recordChat({chat_session_id:stored_data.chat_session_id}, function(err, data) {
                    expect(data.status).to.be.true;
                    done();
                });
            }, 2000);
        }),
        it ('Pause Billing after 1 seconds delay', function(done) {
            // let say after 2 s
            this.timeout(5000);
            setTimeout( function() {
                userManager.pauseBilling({chat_session_id:stored_data.chat_session_id}, function(err, data) {
                    expect(data.status).to.be.true;
                    done();
                });
            }, 1000);
        }),
        it ('Resume Billing after 1 seconds delay', function(done) {
            // let say after 2 s
            this.timeout(5000);
            setTimeout( function() {
                userManager.resumeBilling({chat_session_id:stored_data.chat_session_id}, function(err, data) {
                    expect(data.status).to.be.true;
                    done();
                });
            }, 1000);
        }),
        it ('Log Message', function(done) {
            // let say after 2 s
            this.timeout(5000);
            setTimeout( function() {
                userManager.logMessage({chat_session_id:stored_data.chat_session_id, member_id:stored_data.client_id,  message:"Message sent at :" + new Date()}, function(err, data) {
                    expect(data.status).to.be.true;
                    done();
                });
            }, 1000);
        }),
        it ('Leave Chat after 2 seconds', function(done) {
            // let say after 2 s
            this.timeout(5000);
            setTimeout( function() {
                userManager.leaveChat({chat_session_id:stored_data.chat_session_id, member_hash:stored_data.client_hash, member_type:"client"}, function(err, data) {
                    expect(data.status).to.be.true;
                    done();
                });
            }, 2000);
        })
    })
})