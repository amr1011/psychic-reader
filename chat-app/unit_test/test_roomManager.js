// Run useing:  mocha test_roomManager.js

var expect = require('chai').expect;
var util = require('util');
var roomManager = require('../manager/roomManager');

var room_info = {
    room_name: 'xxxxRoomName',
    reader_id: 1,
    reader_hash: '1asdfpjalsdf',
    client_id: 2,
    client_hash: '2woeworowere',
    
}
 
describe('RoomManager', function(){

    describe('CreateOrJoin', function(){
       it ('Client create should return status=true', function(done) {
           roomManager.createOrJoinRoom({room_data:room_info, member_type:'client'}, function(err, data){
               expect(data.status).to.be.true;
               expect(roomManager.rooms).to.have.length(1);
               done();
           })
       }),
       it ('Reader join should return status=true', function(done) {
           roomManager.createOrJoinRoom({room_data:room_info, member_type:'reader'}, function(err, data){
               //console.log("error is " + util.inspect(err));
               expect(data.status).to.be.true;
               expect(roomManager.rooms).to.have.length(1);
               done();
           })
       }),
       it ('Reader join again should return error', function(done) {
           roomManager.createOrJoinRoom({room_data:room_info, member_type:'reader'}, function(err, data){
               //console.log("error is " + util.inspect(err));
               expect(err).not.to.be.null;
               done();
           })
       })
    }),
    describe('look up room ', function(){
       it ('Look Up room status=true', function(done) {
           roomManager.lookupRoom({room_name:room_info.room_name}, function(err, data){
               expect(data.room_name).to.equal(room_info.room_name);
               expect(roomManager.rooms).to.have.length(1);
               done();
           })
       })
    }),
    describe('Time ', function(){
       it ('Start timing for room. status=true', function(done) {
           roomManager.startRoomTimer({room_name:room_info.room_name}, function(err, data){
               expect(data.status).to.be.true;
               expect(data.is_pause).to.be.false;
               console.log("Start time: " + data.start_time);
               expect(roomManager.rooms).to.have.length(1);
               done();
           })
       }),
       it ('Pause timing after 2 seconds for room. status=true', function(done) {
           this.timeout(5000);
           setTimeout( function() {
                   roomManager.pauseRoomTimer({room_name:room_info.room_name}, function(err, data){
                   expect(data.status).to.be.true;
                   expect(data.is_pause).to.be.true;
                   console.log("Last updated time: " + data.last_updated_time);
                   console.log("Diff time: " + data.diff_time);
                   expect(data.diff_time * 1000).to.be.within(2000, 2100);
                   done();
               })
               
           }, 2000);
           
       }),
       it ('Resume timing after 1 seconds for room. status=true', function(done) {
           this.timeout(5000);
           setTimeout( function() {
                   roomManager.resumeRoomTimer({room_name:room_info.room_name}, function(err, data){
                   expect(data.status).to.be.true;
                   expect(data.is_pause).to.be.false;
                   console.log("Last updated time: " + data.last_updated_time);
                   console.log("Pause time: " + data.pause_length);
                   expect(data.pause_length * 1000).to.be.within(1000, 1100);
                   done();
               })
               
           }, 1000);    // resume after 1 second
           
       }),
       it ('Update timing after 1.5 seconds for room. status=true', function(done) {
           this.timeout(5000);
           setTimeout( function() {
                   roomManager.updateRoomTimer({room_name:room_info.room_name}, function(err, data){
                   expect(data.status).to.be.true;
                   expect(data.is_pause).to.be.false;
                   console.log("Last updated time: " + data.last_updated_time);
                   console.log("Diff time: " + data.diff_time);
                   expect(data.diff_time * 1000).to.be.within(1500, 1600);
                   done();
               })
               
           }, 1500);    // check after 1.5 second
           
       }),
       it ('Pause timing again after 2 seconds for room. status=true', function(done) {
           this.timeout(5000);
           setTimeout( function() {
                   roomManager.pauseRoomTimer({room_name:room_info.room_name}, function(err, data){
                   expect(data.status).to.be.true;
                   expect(data.is_pause).to.be.true;
                   console.log("Last updated time: " + data.last_updated_time);
                   console.log("Diff time: " + data.diff_time);
                   expect(data.diff_time * 1000).to.be.within(2000, 2100);
                   done();
               })
               
           }, 2000);
           
       }),
       it ('Resume timing again after 1 seconds for room. status=true', function(done) {
           this.timeout(5000);
           setTimeout( function() {
                   roomManager.resumeRoomTimer({room_name:room_info.room_name}, function(err, data){
                   expect(data.status).to.be.true;
                   expect(data.is_pause).to.be.false;
                   console.log("Last updated time: " + data.last_updated_time);
                   console.log("Pause time: " + data.pause_length);
                   expect(data.pause_length * 1000).to.be.within(1000, 1100);
                   done();
               })
               
           }, 1000);    // resume after 1 second
           
       }),
       it ('Get Last updated time info. status=true', function(done) {

           roomManager.getLastUpdatedRoomTime({room_name:room_info.room_name}, function(err, data){
               expect(data.status).to.be.true;
               expect(data.is_pause).to.be.false;
               expect(data.total_elasped * 1000).to.be.within(5500, 5600);
               expect(data.total_pause_time * 1000).to.be.within(2000, 2100);
               //console.log("Data " + util.inspect(data));
               done();
           })
       })
    })
})