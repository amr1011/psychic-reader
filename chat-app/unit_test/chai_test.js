var expect = require('chai').expect;


describe('Array', function(){
  describe('length', function(){
    it('should return true for length be 2', function(){
        expect([1,456]).to.have.length(2);
    })
  }),
  describe('boolean()', function(){
    it('should true when the value true', function(){
       expect(true).to.be.true;
    })
  })
})