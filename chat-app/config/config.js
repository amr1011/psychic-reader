
if (process.env.NODE_ENV == 'production') {
    var config = require('./config_prod.js');
} else if  (process.env.NODE_ENV == 'testing') {
	var config = require('./config_test.js');
} else {
    var config = require('./config_dev.js');
    //var config = require('./config_dev2.js');
}
module.exports = config;

