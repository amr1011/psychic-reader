
// session
module.exports.session = {
    key:'devPsySKey'  
};


// socket
//module.exports.socket_url = "66.178.182.16";
module.exports.socket_url = "local.psychic-contact.com";
module.exports.socket_port = 80;

module.exports.isHttps = false;
/*
 * 
module.exports.httpsOptions = {
   key: fs.readFileSync('./key'), 
    cert: fs.readFileSync('./cert') 
};
 * 
 */

// request end point
module.exports.php_chat_service = {
    url: 'http://local.psychic-contact.com/', //'http://localhost/',
    api: {
        dummy_chat_session:"chat/chatInterface/create_dummy_chat_session",
        validate:"chat/chatInterface/validate",
        validate_member:"chat/chatInterface/validate_member",
        confirm_chat:"chat/main/confirm_json", 
        start_chat: "chat/chatInterface/start_chat",
        end_chat:"chat/chatInterface/end_chat",
        abort_chat:"chat/chatInterface/abort_chat",
        reject_chat:"chat/chatInterface/reject_chat",
        record_chat: "chat/chatInterface/record_chat",
        log_message: "chat/chatInterface/log_message",
        refund_chat: "chat/chatInterface/refund_chat",
        ban_user: "chat/chatInterface/banUser",
        purchase_more_time: "chat/chatInterface/purchaseMoreTime",
        get_stored_time: "chat/chatInterface/getStoredTime",
        add_stored_time: "chat/chatInterface/addStoredTime",
        add_free_time: "chat/chatInterface/addFreeTime",
        check_time_balance: "chat/chatInterface/check_time_balance"
    }
};

// other settings
module.exports.room = {
	chat_request_max_pending_time: 45000	// 45 seconds
};

