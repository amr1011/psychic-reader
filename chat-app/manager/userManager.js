/*
 * User Manager
 */
global.request = require('request');
global.util = require('util');
global.config = require('../config/config.js');
global.errorCode = require('../lib/errorCode');

userManager = {
    _explicitName:'userManager',
    
    createDummyChatSession: function (input, next) {
        //var request_url = config.php_chat_service.url + config.php_chat_service.api.dummy_chat_session;
        var request_url = input.site_url + config.php_chat_service.api.dummy_chat_session;
        
        console.log("request url " + request_url);
        var data = {
            client_id: input.client_id || 14,  
            reader_id: input.reader_id || 13,
            pass: "Brunch"
        };
        
        //console.log("data is: " + util.inspect(data));
        request.post(request_url, {form:data}, function(err, res, body){
            
            if (err) {
                //log
                //console.log("Error " + err);
                next(errorCode.gen('00109'));
            } else {
                //console.log("Body is " + body);
                var data = JSON.parse(body);
                console.log("Dummy Data is " + util.inspect(data));
                next(null, data);
            }
        });
    },
    
    // Data require:  chat_id, chat_session_id, member_hash
    validate: function(data, next) {
        //var request_url = config.php_chat_service.url + config.php_chat_service.api.validate;
        var request_url = data.site_url + config.php_chat_service.api.validate;
        request.post(request_url, {form:data}, function(err, res, body){
            if (err) {
                //log
                next(errorCode.gen('00101'));
            } else {
                //console.log("validate data is: " + util.inspect(body));
                var data = JSON.parse(body);
                console.log(" User Manager return data: "  + body);
                if (data.status) {
                    var result = {
                        reader_id       :   data.reader_id,
                        reader_hash     :   data.reader_hash,
                        reader_username :   data.reader_username,
                        client_id       :   data.client_id,
                        client_hash     :   data.client_hash,
                        client_username :   data.client_username,
                        chat_session_id :   data.chat_session_id,
                        member_id       :   data.member_id,
                        member_username :   data.member_username,
                        free_minutes :   data.total_free_length,
                    };
                    next(null, result);
                } else {
                    next(errorCode.gen('00101'));
                }
            }
        });
    },
    
    validate_member_id: function(data, next) {
        console.log("Validate Member ID " + util.inspect(data));
        //var request_url = config.php_chat_service.url + config.php_chat_service.api.validate_member;
        var request_url = data.site_url + config.php_chat_service.api.validate_member;        
        request.post(request_url, {form:data}, function(err, res, body){
            if (err) {
                //log
                next(errorCode.gen('00101'));
            } else {
                console.log("data is: " + body);
                //console.log("validate data is: " + util.inspect(body));
                var data = JSON.parse(body);
                
                if (data.status) {
                    
                    next(null, {status:true, member_type:data.member_type});
                } else {
                    next(errorCode.gen('00101'));
                }
            }
        });
    },
    
    confirm_chat: function(data, next) {
        //var request_url = config.php_chat_service.url + config.php_chat_service.api.confirm_chat;
        var request_url = data.site_url + config.php_chat_service.api.confirm_chat;
        request.post(request_url, {form:data}, function(err, res, body){
            if (err) {
                //log
                next(errorCode.gen('00101'));
            } else {
                console.log("confirm_chat data is: " + util.inspect(body));
                var data = JSON.parse(body);
                console.log(" User Manager return data: "  + body);
                if (data.status) {
      
                    next(null, {status:true, data:data});
                } else {
                    next(data);
                }
            }
        });
    },
    
    get_readers_list: function(data, next) {
        //var request_url = config.php_chat_service.url + config.php_chat_service.api.confirm_chat;
        var request_url = data.site_url + config.php_chat_service.api.get_readers_list;
        request.post(request_url, {form:data}, function(err, res, body){
            if (err) {
                //log
                next(errorCode.gen('00101'));
            } else {                
                var data = JSON.parse(body);                
                if (data.status) {
                    next(null, {status:true, data:data});
                } else {
                    next(data);
                }
            }
        });
    },
    
    check_register_client: function(data, next) {        
        var request_url = data.site_url + config.php_chat_service.api.check_register_client;        
        request.post(request_url, {form:data}, function(err, res, body){
            if (err) {                
                next(errorCode.gen('00101'));
            } else {                
                var data = JSON.parse(body);                
                if (data.status) {
                    next(null, {status:true, data:data});
                } else {
                    next(data);
                }
            }
        });
    },
    
    check_time_balance: function(data, next) {
        //var request_url = config.php_chat_service.url + config.php_chat_service.api.check_time_balance;
        var request_url = data.site_url + config.php_chat_service.api.check_time_balance;
        
        var chat_session_id = data.chat_session_id || data.room_name || null;
        var input_data = {chat_session_id:chat_session_id};
        // get the chat room
        if (room_name) {
            // update timer and find out the difference
            request.post(request_url, {form:input_data}, function(err, res, body){
                if (err) {
                    //log
                    next(errorCode.gen('00201'));
                } else {
                    var data = JSON.parse(body);
                    
                    if (data.status) {
                        var server_resp = {
                            status          : true,
                            start_time      : new Date(Date.parse(data.start_time)),
                            max_chat_length : data.max_chat_length,
                            chat_length     : data.chat_length,
                            time_balance    : data.time_balance
                        };
                    
                        next(null, server_resp);
                    } else {
                        next(errorCode.gen('00201'));
                    }
                }
            });  
        } else {
            next(errorCode.gen('00201'));
        }
        
    },
    
    abortChat: function (data, next) {
        //var request_url = config.php_chat_service.url + config.php_chat_service.api.abort_chat;
        var request_url = data.site_url + config.php_chat_service.api.abort_chat;
        // log request
        var room_name = data.chat_session_id || data.room_name || null;
        var member_hash = data.member_hash;
        var aborted_by = data.member_type || data.aborted_by;
        var type = data.type || "manual";
        var input_data = {room_name:room_name, member_hash:member_hash, aborted_by:aborted_by, type:type};
        
        if (room_name) {
            request.post(request_url, {form:input_data}, function(err, res, body){
                console.log("What is the body " + body);
                if (err) {
                    //log
                    next(errorCode.gen('00200'));
                } else {
                    var data = JSON.parse(body);
                    
                    if (data.status) {
                        
                        next(null, data);
                    } else {
                        console.log("Error: " + JSON.stringify(data));
                        next(errorCode.gen('00200'));
                    }
                }
            });  
            
        } else {
            console.log("Error: " + JSON.stringify(data));
            next(errorCode.gen('00200'));
        }        
    },
    
    rejectChat: function (data, next) {
        //var request_url = config.php_chat_service.url + config.php_chat_service.api.reject_chat;
        var request_url = data.site_url + config.php_chat_service.api.reject_chat;
        // log request
        var room_name = data.chat_session_id || data.room_name || null;
        var member_hash = data.member_hash;
        var member_type = data.member_type || data.rejected_by;
        var input_data = {room_name:room_name, member_hash:member_hash, rejected_by:member_type};
        console.log("Reject chat is called with input_data " + JSON.stringify(input_data));
        if (room_name) {
            roomManager.removeRoom({room_name:room_name}, function(err, data) {
                if (err) {
                	console.log(" reject chat error: " + util.inspect(err));
                    next(errorCode.gen('00201'));
                } else if (data && data.status) {
                    
                    console.log("Before sending " + util.inspect(input_data));
                    request.post(request_url, {form:input_data}, function(err, res, body){
                        console.log("What is the body " + body);
                        if (err) {
                            //log
                            next(errorCode.gen('00200'));
                        } else {
                            var data = JSON.parse(body);
                            
                            if (data.status) {
                                var server_resp = {
                                    status          : true
                                }
                                next(null, server_resp);
                            } else {
                                console.log("Error: " + JSON.stringify(data));
                                next(errorCode.gen('00200'));
                            }
                        }
                    });  
                } else{
                    console.log("Error: " + JSON.stringify(data));
                    next(errorCode.gen('00201'));
                }
                
            });
            
        } else {
            console.log("Error: " + JSON.stringify(data));
            next(errorCode.gen('00200'));
        }        
    },
    
    leaveChat: function (data, next) {
        //var request_url = config.php_chat_service.url + config.php_chat_service.api.end_chat;
        var request_url = data.site_url + config.php_chat_service.api.end_chat;
        // log request
        var room_name = data.chat_session_id || data.room_name || null;
        var member_hash = data.member_hash;
        var member_type = data.member_type || data.terminated_by;
        var input_data = {room_name:room_name, member_hash:member_hash, terminated_by:member_type};
        
        if (room_name) {
            roomManager.removeRoom({room_name:room_name}, function(err, data) {
                if (err) {
                    next(errorCode.gen('00201'));
                } else if (data && data.status) {
                    console.log("userManager leave chat - removeRoom done. and data is " + util.inspect(data));
                    if (data.is_pause) {
                        input_data.timer = 0;
                    } else {
                        input_data.timer = data.diff_time;
                    }
                    var chat_length = data.chat_length;
                    console.log("Before sending " + util.inspect(input_data));
                    request.post(request_url, {form:input_data}, function(err, res, body){
                        console.log("What is the body " + body);
                        if (err) {
                            //log
                            next(errorCode.gen('00200'));
                        } else {
                            var data = JSON.parse(body);
                            
                            if (data.status) {
                                var server_resp = {
                                    status          : true,
                                    start_time      : new Date(Date.parse(data.start_time)),
                                    max_chat_length : parseInt(data.max_chat_length,10),
                                    chat_length     : parseInt(data.chat_length, 10),
                                    time_balance    : parseInt(data.time_balance, 10)
                                }
                                next(null, server_resp);
                                console.log("-----------------------+-----------------------");
                            } else {
                                console.log("Error: " + JSON.stringify(data));
                                next(errorCode.gen('00200'));
                            }
                        }
                    });  
                } else{
                    console.log("Error: " + JSON.stringify(data));
                    next(errorCode.gen('00201'));
                }
                
            });
            
        } else {
            console.log("Error: " + JSON.stringify(data));
            next(errorCode.gen('00200'));
        }        
    },
    
    
    // record the chat time length 
    recordChat: function(data, next) {
        
        //var request_url = config.php_chat_service.url + config.php_chat_service.api.record_chat;
        var request_url = data.site_url + config.php_chat_service.api.record_chat;
        
        var room_name = data.chat_session_id || data.room_name || null;
        
        var update = {};
        if (_.has(data, "lost_time")) {
            console.log("userManager recordChat - Has lost time? ");
            update = {lost_time:data.lost_time};
        }
        
        // get the chat room
        if (room_name) {
            // update timer and find out the difference
            roomManager.updateRoomTimer({room_name:room_name},update, function(err, data) {
                if (err) {
                    console.log("userManager.recordChat: " + JSON.stringify(err));
                    next(errorCode.gen('00201'));
                } else if (data && data.status) {
                    
                    var server_resp = {
                        max_chat_length : data.max_chat_length,
                        chat_length     : data.chat_length,
                        time_balance    : data.time_balance
                    };
                    console.log("<----- Chat Length ------>" + data.chat_length)
                    console.log("<----- Chat Balance ------->" + data.time_balance);
                    var input_data = {chat_session_id:room_name, timer:data.diff_time};
                    request.post(request_url, {form:input_data}, function(err, res, body){
                        
                        if (err) {
                            //log
                            next(errorCode.gen('00201'));
                        } else {
                            var data = JSON.parse(body);
                            
                            if (data.status) {
                                
                                roomManager.update_max_length(room_name, data.chat_max_length);
                                
                                server_resp.status = true;
                                next(null, server_resp);
                            } else {
                                next(errorCode.gen('00201'));
                            }
                        }
                    });  
                    
                } else{
                    next(errorCode.gen('00201'));
                }
                
            });
        } else {
            next(errorCode.gen('00201'));
        }
    },
    updateFreeTimer: function(data, next) {
        
        //var request_url = config.php_chat_service.url + config.php_chat_service.api.record_chat;
        var request_url = data.site_url + config.php_chat_service.api.update_free_timer;        
        var room_name = data.chat_session_id || data.room_name || null;
        
        var update = {};
        if (_.has(data, "lost_time")) {
            console.log("userManager recordChat - Has lost time? ");
            update = {lost_time:data.lost_time};
        }
        
        // get the chat room
        if (room_name) {
            // update timer and find out the difference
            roomManager.updateRoomFreeTimer({room_name:room_name},update, function(err, data) {
                if (err) {
                    
                    next(errorCode.gen('00201'));
                    
                } else if (data && data.status) {
                    
                    var server_resp = {
                        total_free_length   : data.total_free_length,
                        free_length         : data.free_length,
                        free_time_balance   : data.free_time_balance,
                        time_balance        : data.time_balance
                    };
                    
                    var diff_time = data.diff_time;
                    
                    var input_data = {chat_session_id:room_name, timer:data.diff_time};
                    request.post(request_url, {form:input_data}, function(err, res, body){
                        if (err) {
                            //log
                            next(errorCode.gen('00201'));
                        } else {
                            var data = JSON.parse(body);
                            
                            console.log("Inside updateRoomFreeTimer -- Start");
                            console.log(data);
                            console.log("Inside updateRoomFreeTimer -- End");
                            
                            if (data.status) {
                                server_resp.total_free_length = data.total_free_length;
                                server_resp.free_length = data.free_length;
                                server_resp.free_time_balance = data.free_time_balance;
                                server_resp.time_balance = data.time_balance;
                                server_resp.status = true;
                                server_resp.timer = diff_time; 
                                next(null, server_resp);
                            } else {
                                next(errorCode.gen('00201'));
                            }
                        }
                    });                
                    
                } else{
                    next(errorCode.gen('00201'));
                }
                
            });
        } else {
            next(errorCode.gen('00201'));
        }
    },
    
    startBilling: function(data, next) {
        var request_url = data.site_url + config.php_chat_service.api.start_chat;
        
        var room_name = data.chat_session_id || data.room_name || null;
        var member_hash = data.member_hash ||  null;
        
        var server_resp = {};
        
        
        // get the chat room
        if (room_name) {
            
            var input_data = {chat_session_id:room_name};
            request.post(request_url, {form:input_data}, function(err, res,body){
                if (err) {
                    //log
                    next(errorCode.gen('00201'));
                } else {
                    var data = JSON.parse(body);
                    
                    if (data.status) {
                        server_resp = {
                            start_time  : new Date(Date.parse(data.start_time)),
                            started_already : data.started_already,
                            max_chat_length : parseInt(data.max_chat_length,10),
                            chat_length     : parseInt(data.chat_length,10),
                            time_balance    : parseInt(data.time_balance,10)
                        }
                        
                        // update timer 
                        roomManager.startRoomTimer({room_name:room_name}, server_resp, function(err, data){
                            if (data && data.status) {
                                next(null, data);
                            } else {
                                console.log("Error: " + JSON.stringify(data));
                                next(errorCode.gen('00201'));
                            }
                        });
                        
                    } else {
                        console.log("Error: " + JSON.stringify(data));
                        next(errorCode.gen('00201'));
                    }
                }
            });  
        } else {
            console.log("Error: room does not exist");
            next(errorCode.gen('00201'));
        }
    },
    
    // pause billing
    // record the chat length 
    pauseBilling: function(data, next) {
        
        //var request_url = config.php_chat_service.url + config.php_chat_service.api.record_chat;
        var request_url = data.site_url + config.php_chat_service.api.record_chat;
        
        var room_name = data.chat_session_id || data.room_name || null;
        
        var chat_length = '';
        // get the chat room
        if (room_name) {
            // update timer and find out the difference
            roomManager.pauseRoomTimer({room_name:room_name}, function(err, data) {
                if (data && data.status) {
                    console.log("Pause and Recording time length is:" + data.diff_time + " chat_length: " + data.chat_length);
                    
                    var server_resp = {
                        max_chat_length : data.max_chat_length,
                        chat_length     : data.chat_length,
                        time_balance    : data.time_balance,
                        diff_time       : data.diff_time,
                        is_already_paused : false
                    };
                    
                    var input_data = {chat_session_id:room_name, timer:data.diff_time};
                    request.post(request_url, {form:input_data}, function(err, res,body){
                        if (err) {
                            //log
                            next(errorCode.gen('00201'));
                        } else {
                            var data = JSON.parse(body);
                            
                            if (data.status) {
                                server_resp.status = true;
                                next(null, server_resp);
                            } else {
                                next(errorCode.gen('00201'));
                            }
                        }
                    });  
                    
                } else{
                    if (data.is_already_paused) {
                        next(null, {status:false, is_already_paused:true});
                    } else {
                        next(errorCode.gen('00201'));
                    }
                    
                }
                
            });
        } else {
            next(errorCode.gen('00201'));
        }
    },
    
    resumeBilling: function(data, next) {
        // just resume. no REST call.        
        
        var room_name = data.chat_session_id || data.room_name || null;
        // get the chat room
        if (room_name) {
            // update timer and find out the difference
            roomManager.resumeRoomTimer({room_name:room_name}, function(err, data) {
                if (data && data.status) {
                    console.log("userManager - Resume Room Timer: " + JSON.stringify(data));
                    var server_resp = {
                        status          : true,
                        max_chat_length : data.max_chat_length,
                        chat_length     : data.chat_length,
                        time_balance    : data.time_balance,
                        diff_time       : data.diff_time,
                        pause_length    : data.pause_length,
                        added_time      : data.added_time
                    };
                    
                    next(null, server_resp);
                    
                } else{
                    next(errorCode.gen('00201'));
                }
                
            });
        } else {
            next(errorCode.gen('00201'));
        }
    },
    
    refundChat: function(data, next) {
        //var request_url = config.php_chat_service.url + config.php_chat_service.api.refund_chat;
        var request_url = data.site_url + config.php_chat_service.api.refund_chat;
        
        var input_data = {chat_session_id:data.room_name};
        request.post(request_url, {form:input_data}, function(err, res,body){
            if (err) {
                //log
                next(errorCode.gen('00700'));
            } else {
                var data = JSON.parse(body);
                console.log("body is: " + body);
                if (data.status) {
                    if (data.refund) {
                        next(null, {status:true, refund:true});
                    } else {
                        next(null, {status:true, refund:false});
                    }
                    
                } else {
                    next(errorCode.gen('00700'));
                }
            }
        }); 
    },
    
    logMessage: function(data, next) {
        //var request_url = config.php_chat_service.url + config.php_chat_service.api.log_message;
        var request_url = data.site_url + config.php_chat_service.api.log_message;
        
        var room_name = data.chat_session_id || data.room_name || null;
        var input_data = {chat_session_id:room_name, message:data.message, member_id:data.member_id};
        console.log("Input data is " + util.inspect(input_data));
        request.post(request_url, {form:input_data}, function(err, res,body){
            if (err) {
                //log
                next(errorCode.gen('00600'));
            } else {
                var data = JSON.parse(body);
                
                if (data.status) {
                    next(null, {status:true});
                } else {
                    next(errorCode.gen('00600'));
                }
            }
        }); 
    },
    
    banUser: function(data, next) {
        //var request_url = config.php_chat_service.url + config.php_chat_service.api.ban_user;
        var request_url = data.site_url + config.php_chat_service.api.ban_user;
        
        // type is either "personal" or "full".
        var input_data = {reader_id:data.reader_id, member_id:data.client_id, type:data.type};
        request.post(request_url, {form:input_data}, function(err, res,body){
            if (err) {
                //log
                next(errorCode.gen('00800'));
            } else {
                var data = JSON.parse(body);
                
                if (data.status) {
                    next(null, {status:true});
                } else {
                    next(errorCode.gen('00800'));
                }
            }
        }); 
    },
    
    purchaseMoreTime: function(data, next) {
        //var request_url = config.php_chat_service.url + config.php_chat_service.api.purchase_more_time;
        var request_url = data.site_url + config.php_chat_service.api.purchase_more_time;
        
        var input_data = {chat_session_id:data.room_name};
        request.post(request_url, {form:input_data}, function(err, res,body){
            if (err) {
                //log
                next(errorCode.gen('00900'));
            } else {
                var data = JSON.parse(body);
                
                if (data.status) {
                    var redirect_url = data.data.redirect;
                    
                    var update = {
                        is_join_ok  : true,
                        rejoin_start_time   : new Date()
                    }
                    
                    roomManager.updateRoomTimer({room_name:room_name}, update, function(err, data) {
                        if (err) {
                            console.log("userManager.addStoredTime: " + JSON.stringify(err));
                            next(errorCode.gen('00201'));
                        } else if (data && data.status) {
                            
                            var server_resp = {
                                status: true,
                                max_chat_length : data.max_chat_length,
                                chat_length     : data.chat_length,
                                time_balance    : data.time_balance
                            }
                            
                            next(null, server_resp);
                        } else {
                            next(errorCode.gen('00201'));
                        }
                    });
                    // if success, data.data.redirect has the url to redirect
                    next(null, {status:true, data:data.data});
                } else {
                    next(errorCode.gen('00900'));
                }
            }
        }); 
    },
    
    getStoredTime: function(data, next) {
        //var request_url = config.php_chat_service.url + config.php_chat_service.api.get_stored_time;
        var request_url = data.site_url + config.php_chat_service.api.get_stored_time;
        
        var input_data = {chat_session_id:data.room_name};
        request.post(request_url, {form:input_data}, function(err, res,body){
            if (err) {
                //log
                next(errorCode.gen('00901'));
            } else {
                console.log(body);
                var data = JSON.parse(body);
                
                if (data.status) {
                    // if success, data.data.redirect has the url to redirect
                    next(null, {status:true, stored_time:data.stored_time});
                } else {
                    next(errorCode.gen('00901'));
                }
            }
        }); 
    },
    
    addStoredTime: function(data, next) {
        //var request_url = config.php_chat_service.url + config.php_chat_service.api.add_stored_time;
        var request_url = data.site_url + config.php_chat_service.api.add_stored_time;
        
        var minutes = data.minutes || data.added_time || 0; 
        var input_data = {chat_session_id:data.room_name, minutes:minutes};
        var room_name = data.room_name;
        
        console.log("Sending data to url: " + request_url);
        request.post(request_url, {form:input_data}, function(err, res,body){
            if (err) {
                //log
                next(errorCode.gen('00901'));
            } else {
                console.log("body: " + body);
                var data = JSON.parse(body);
                
                if (data.status) {
                    var update = {                       
                        max_chat_length : parseInt(data.max_chat_length,10),
                        chat_length     : parseInt(data.chat_length,10),
                        time_balance    : parseInt(data.time_balance,10),
                        add_time        : minutes
                    };
                    console.log("What to update? " + JSON.stringify(update));
                    // if success, data.data.redirect has the url to redirect
                    roomManager.updateRoomTimer({room_name:room_name}, update, function(err, data) {
                        if (err) {
                            console.log("userManager.addStoredTime: " + JSON.stringify(err));
                            next(errorCode.gen('00201'));
                        } else if (data && data.status) {
                            
                            var server_resp = {
                                status: true,
                                max_chat_length : data.max_chat_length,
                                chat_length     : data.chat_length,
                                time_balance    : data.time_balance,
                                added_time      : minutes
                            };
                            
                            next(null, server_resp);
                        } else {
                            next(errorCode.gen('00201'));
                        }
                    });
                        
                } else {
                    next(errorCode.gen('00901'));
                }
            }
        }); 
    },
    
    addFreeTime: function(data, next) {
        //var request_url = config.php_chat_service.url + config.php_chat_service.api.add_free_time;
        var request_url = data.site_url + config.php_chat_service.api.add_free_time;
        
        var minutes = data.minutes || data.free_time || 0; 
        var input_data = {chat_session_id:data.room_name, minutes:minutes};
        var room_name = data.room_name;
        
        var total_free_time; // in minutes
        
        request.post(request_url, {form:input_data}, function(err, res,body){
            if (err) {
                //log
                next(errorCode.gen('00901'));
            } else {
                console.log("body: " + body);
                var data = JSON.parse(body);
                
                if (data.status) {
                    
                    total_free_time = Math.floor(parseInt(data.total_free_length,10) / 60);
                    
                    var update = {                       
                        max_chat_length : parseInt(data.max_chat_length,10),
                        chat_length     : parseInt(data.chat_length,10),
                        time_balance    : parseInt(data.time_balance,10),
                        add_time        : minutes
                    }
                    console.log("What to update? " + JSON.stringify(update));
                    // if success, data.data.redirect has the url to redirect
                    roomManager.updateRoomTimer({room_name:room_name}, update, function(err, data) {
                        if (err) {
                            console.log("userManager.addStoredTime: " + JSON.stringify(err));
                            next(errorCode.gen('00201'));
                        } else if (data && data.status) {
                            
                            var server_resp = {
                                status: true,
                                max_chat_length : data.max_chat_length,
                                chat_length     : data.chat_length,
                                time_balance    : data.time_balance, 
                                total_free_time      : total_free_time,
                                free_time_added      : minutes
                            }
                            
                            next(null, server_resp);
                        } else {
                            next(errorCode.gen('00201'));
                        }
                    });
                        
                } else {
                    next(errorCode.gen('00901'));
                }
            }
        }); 
    }
    

};

module.exports = userManager;
