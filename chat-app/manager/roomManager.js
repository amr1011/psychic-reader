/*
 * 
 * Room Manager.. manage the room id. so it is acted like a cache
 */
global._ = require('underscore');
global.util = require('util');
global.errorCode = require('../lib/errorCode');
var domain = require('domain'); 

roomManager = {
    rooms: [], // array of rooms, use underscore to manage it 
    member_socket_map: {},
    
    setMemberSocket: function(member_id, socket_id) {
        this.member_socket_map[member_id] = socket_id;  
    },
    getMemberSocket: function(member_id) {
        return this.member_socket_map[member_id];  
    },
    cleanMemberSocket: function(socket_id) {
        var member_id = false;
        var invert_map = _.invert(this.member_socket_map);
        if (invert_map[socket_id]) {
            member_id = invert_map[socket_id];
            delete this.member_socket_map[invert_map[socket_id]];
        }
        return member_id;
    },
    
    confirmChatRoom: function(data, next) {    
    	
    	var client_id = data.client_id;
    	var client_id_hash = data.client_id_hash;
    	var reader_id = data.reader_id;
    	var topic = data.topic;
    	var minutes = data.minutes;
    	var free_minutes = data.free_minutes;
    	var client_lobby_socket_id = data.client_lobby_socket_id;
    	var that = this;
        var site_url = data.site_url;
        var client_url = data.client_url;
        var tier = data.tier;
    	
    	var input = {
        	client_id: client_id,
        	client_id_hash: client_id_hash,
        	reader_id: reader_id,
        	topic: topic,
        	minutes: minutes,
        	free_minutes: free_minutes,
                site_url: site_url,
                client_url: client_url,
                tier: tier
       };
        // pause billing
        userManager.confirm_chat(input, function(err, data) {
            if (err) {
                next(err);
            } else {            	
                var room_data = {
                    chat_id         : data.data.chat_id,
                    room_name       : data.data.chat_session_id, // use that as the room name
                    client_id       : data.data.client_id,
                    client_hash     : data.data.client_hash,
                    client_username : data.data.client_username,
                    client_socket_id: null,
                    client_lobby_socket_id: client_lobby_socket_id,
                    reader_id       : data.data.reader_id,
                    reader_hash     : data.data.reader_hash,
                    reader_username : data.data.reader_username,
                    reader_socket_id: null,
                    
                    reader_joined	: false,
                    client_joined	: false,
                    aborted			: false,
                    
                    chat_request_time: new Date(),
                    chat_request_counter: config.room.chat_request_max_pending_time / 1000, 
                    start_time		: null,
                    last_updated_time	: null,
                    is_pause		: true,	// hasn't started yet
                    paused_by		: '',
                    max_chat_length	: data.data.max_chat_length,
                    chat_length		: data.data.chat_length,		// in seconds
                    time_balance	: data.data.time_balance,
                    total_pause_time	: 0,	// in seconds
                    is_rejoin_ok	: false,
                    rejoin_start_time : null, // new Date object; set when purchase more time is called. reset to null when reconnect/rejoin
                    is_disconnected : false,
                    chat_attempt_accepted: false,
                    redirect_url: data.data.redirect_url,
                    site_url: data.data.site_url,
                    client_url: data.data.client_url,
                    free_minutes: data.data.total_free_length
                };
	   
	            that.rooms.push(room_data);
	            console.log("roomManager confirm room: " + JSON.stringify(this.rooms));
	            var output = room_data;
	           
	            next(null, {status:true, data:output});
            }
        });
    },
    
    createOrJoinRoom: function(data, next) {
        console.log("roomManager createOrJoinRoom ");
        // if already exist, then return error.
        var room_data = data.room_data;
        var member_type = data.member_type || 'client';    
        var member_socket_id = data.member_socket_id || '';    
        var site_url = data.site_url || '';    
        
        var room = _.findWhere(this.rooms, {room_name:room_data.room_name});
        if (room) {
        	room_data = room;
            console.log("roomManager createOrJoinRoom  - Room found " + util.inspect(room));
            if (member_type == 'reader') {
                
                room_data.is_rejoin_ok = false;
                room_data.rejoin_start_time = null;
                room_data.site_url = site_url;
                
                if (room.reader_joined) {
                    // join already
                    if (room.is_disconnected && room.reader_disconnected == true) {
                        room.reader_disconnected = false;
                        room.reader_disconnected_time = null;
                        
                        if (room.reader_disconnected == false && room.client_disconnected == false) {
                            room.is_disconnected = false;
                        }
                        if (room.is_disconnected == false) {
                            roomManager.job.remove_from_watch("disconnection", room.room_name);
                        }
                        // reset reader socket;
                        room.reader_socket_id = member_socket_id;

                        next(null, {status:true, message:"reader reconnect", is_reconnect:true, room:room_data});
                    } else {
                        next(errorCode.gen('00103'));
                    }
                    
                } else {
                    room.reader_joined = true;
                    room.reader_socket_id = member_socket_id;
                    next(null, {status:true, message:"reader joined", room:room_data});
                }
            } else if (member_type == 'client') {
                
                room_data.is_rejoin_ok = false;
                room_data.rejoin_start_time = null;
                room_data.site_url = site_url;
                
                if (room.client_joined) {
                    if (room.is_disconnected && room.client_disconnected == true) {
                        room.client_disconnected = false;
                        room.client_disconnected_time = null;
                        if (room.reader_disconnected == false && room.client_disconnected == false) {
                            room.is_disconnected = false;
                        }
                        if (room.is_disconnected == false) {
                            roomManager.job.remove_from_watch("disconnection", room.room_name);
                        }
                        // reset client_socket
                        room.client_socket_id = member_socket_id;
                        next(null, {status:true, message:"client reconnect", is_reconnect:true, room:room_data});
                    } else {
                        next(errorCode.gen('00104'));
                    }
                } else {
                    room.client_joined = true;
                    room.client_socket_id = member_socket_id;
                    next(null, {status:true, message:"client joined", room:room_data});
                }
            } else {
                next(errorCode.gen('00105'));
            }
        } else {     
            console.log("roomManager createOrJoinRoom  - Room not found");
            room_data.reader_joined = false;
            room_data.client_joined = false;
            room_data.aborted = false;
            if (member_type == 'reader')  {
                room_data.reader_joined = true;
                room_data.reader_socket_id = member_socket_id;
            } else if (member_type == 'client') {
                room_data.client_joined = true;
                room_data.client_socket_id = member_socket_id;
                room_data.chat_request_time = new Date();
            }
            room_data.start_time = null;
            room_data.last_updated_time = null;
            room_data.is_pause = true;  // haven't started
            room_data.max_chat_length = 0;
            room_data.chat_length = 0;    // in seconds
            room_data.time_balance = 0;
            room_data.total_pause_time = 0; // in seconds
            
            // special for room rejoin result from purchase more time
            room_data.is_rejoin_ok = false;
            room_data.rejoin_start_time = null; // new Date object; set when purchase more time is called. reset to null when reconnect/rejoin
            
            room_data.is_disconnected = false;
            room_data.redirect_url = "chat/chatInterface/index/" +room_data.chat_id + "/" + room_data.room_name;
            room_data.site_url = room_data.site_url;
   
            this.rooms.push(room_data);
            console.log("roomManager the rooms now is: " + JSON.stringify(this.rooms));
            next(null, {status:true, message:"created by "+ member_type, room:room_data});
        }
    },
    
    lookupRoom: function(data, next) {        
        var room = _.findWhere(this.rooms, data);
        if(room) {
            if (next) {
                next(null, room);
            } else {
                return room;
            }
        } else {
            if (next) {
                next(errorCode.gen('01101'));    
            } else {
                return false;
            }
        }
    },
    
    removeRoom: function(data, next) {
        if (data) {
            var room = _.findWhere(this.rooms, data);
            
            if (room) { 
                var curr_time = new Date();
                var diff_time = 0;
                if(room.last_updated_time) {
                    diff_time = Math.floor((curr_time.getTime() - room.last_updated_time.getTime()) / 1000); // from milliseconds to seconds
                } 
                room.last_updated_time = curr_time;
                if (room.is_pause) {
                    room.total_pause_time += diff_time;
                } else {
                    roomManager.add_chat_length(room, diff_time);
                }
                
                var standard_resp = roomManager.standardSuccessOutput(room);
                standard_resp.diff_time = diff_time;
                
                var pos = _.indexOf(this.rooms, room);
                this.rooms.splice(pos, 1);
                next(null, standard_resp);
            } else {
                next(null, {status:false, reason:'room not found'});
            }
        } else {
            next(null, {status:false, reason:'empty data, room not found'});
        }
    },
    
    startRoomTimer: function(data, input, next) {
        if (data) {
            room = _.findWhere(this.rooms, data);
            
            var curr_time = new Date();
            room.start_time = input.start_time || curr_time;
            //room.last_updated_time = input.start_time || curr_time;
            room.last_updated_time = curr_time;
            room.is_pause = false;
            room.chat_length = input.chat_length;
            room.total_pause_time = 0;
            room.is_disconnected = false;
            room.client_disconnected = false;
            room.reader_disconnected = false;
            room.time_balance = input.time_balance;
            room.max_chat_length = input.max_chat_length;
            room.started_already = input.started_already;
            
            var standard_resp = roomManager.standardSuccessOutput(room);
            
            next(null, standard_resp);
        } else {
            next(null, {status:false, reason:'room not found'});
        }
        
    },
    
    updateRoomTimer: function(data, input, next) {
        console.log("roomManager.updateRoomTimer: " + JSON.stringify(input) );
        if (data) {
            room = _.findWhere(this.rooms, data);
            
            // if room is pause, then no update. 
            var has_lost_or_add_time = false;
            if (_.has(input, "lost_time")) {
                has_lost_or_add_time = true;
            }
            if (_.has(input, "add_time")) {
                has_lost_or_add_time = true;
            }
            if (room.is_pause && has_lost_or_add_time == false) {
                console.log("Room is already pausedn and is not for lost time");
                next(null, {status:false, reason:'chat is already paused'});
            } else {
                // find the diff
                var diff_time = 0;
                var curr_time = new Date();
                if (_.has(input, 'lost_time') ) {
                    diff_time = input.lost_time * -1;
                    
                    room.last_updated_time = curr_time;
                    roomManager.add_chat_length(room, diff_time);
                } else if (_.has(input, 'add_time')) {
                    room.last_updated_time = curr_time;
                    room.added_time = input.add_time;
                    roomManager.set_new_chat_data(room, input);
                } else {
                    diff_time = Math.floor((curr_time.getTime() - room.last_updated_time.getTime()) / 1000); // from milliseconds to seconds
                    room.last_updated_time = curr_time;
                    roomManager.add_chat_length(room, diff_time);
                }

                var standard_resp = roomManager.standardSuccessOutput(room);
                standard_resp.diff_time = diff_time;
                
                next(null, standard_resp);
            }
            
        } else {
            next(null, {status:false, reason:'room not found'});
        }
    },
    updateRoomFreeTimer: function(data, input, next) {
        console.log("roomManager.updateRoomFreeTimer: " + JSON.stringify(data) );
        if (data) {
            room = _.findWhere(this.rooms, data);
            
            // if room is pause, then no update. 
            var has_lost_or_add_time = false;
            if (_.has(input, "lost_time")) {
                has_lost_or_add_time = true;
            }
            if (_.has(input, "add_time")) {
                has_lost_or_add_time = true;
            }
            if (room.is_pause && has_lost_or_add_time == false) {
                console.log("Room is already pausedn and is not for lost time");
                next(null, {status:false, reason:'chat is already paused'});
            } else {
                // find the diff
                var diff_time = 0;
                var curr_time = new Date();
                
                diff_time = Math.floor((curr_time.getTime() - room.last_updated_time.getTime()) / 1000); // from milliseconds to seconds
                console.log("updateRoomFreeTimer diff_time: " + diff_time);
                room.last_updated_time = curr_time;
                roomManager.add_chat_free_length(room, diff_time);

                var standard_resp = roomManager.standardSuccessOutput(room);                
                standard_resp.diff_time = diff_time;
                
                next(null, standard_resp);
            }
            
        } else {
            next(null, {status:false, reason:'room not found'});
        }
    },
    
    getLastUpdatedRoomTime: function(data, next) {
        if (data) {
            room = _.findWhere(this.rooms, data);
            
            var standard_resp = roomManager.standardSuccessOutput(room);
            
            next(null, standard_resp);
        } else {
            next(null, {status:false, reason:'room not found'});
        }
    },
    
    pauseRoomTimer: function(data, next) {
        if (data) {
            room = _.findWhere(this.rooms, data);

            if (!room) {
                next(null, {status:false, reason:'room not found'});
            }
            
            // if room is pause, then no update. 
            if (room.is_pause) {
                next(null, {status:false, reason:'chat is already paused', is_already_paused: true});
            } else {
                // when pause, set the is_pause = true, and update the last_updated_time, return the time diff. 
                room.is_pause = true;
                // find the diff
                var curr_time = new Date();
                var diff_time = Math.floor((curr_time.getTime() - room.last_updated_time.getTime()) / 1000); // from milliseconds to seconds
                room.last_updated_time = curr_time;

                roomManager.add_chat_length(room, diff_time);
                
                var standard_resp = roomManager.standardSuccessOutput(room);
                standard_resp.diff_time = diff_time;
                
                next(null, standard_resp);
            }
            
        } else {
            next(null, {status:false, reason:'room not found'});
        }
    }, 
    
    resumeRoomTimer: function(data, next) {
        if (data) {
            room = _.findWhere(this.rooms, data);
            
            console.log("roomManager - resume Room TImer: " + JSON.stringify(room));
            
            // if room is pause, then no update. 
            if (! room.is_pause) {
                next(null, {status:false, reason:'chat is not paused, cannot resume'});
            } else {
                // when resume, set the is_pause = false, and update the last_updated_time, return the time pause period   . 
                room.is_pause = false;
                // find the diff
                var curr_time = new Date();
                var diff_time = Math.floor((curr_time.getTime() - room.last_updated_time.getTime()) / 1000); // from milliseconds to seconds
                room.last_updated_time = curr_time;
                room.total_pause_time += diff_time;                
                
                var standard_resp = roomManager.standardSuccessOutput(room);
                standard_resp.pause_length = diff_time;
                standard_resp.added_time = room.added_time;                
                next(null, standard_resp);
            }
            
        } else {
            next(null, {status:false, reason:'room not found'});
        }
    },
    
    resetRoomTimer: function(data, next) {
        if (data) {
            room = _.findWhere(this.rooms, data);
            
            
            room.is_pause = false; // unpause it??
            
            // reset start time
            var curr_time = new Date();
            room.start_time = curr_time;
            room.last_updated_time = curr_time;
            room.max_chat_length = 0;
            room.chat_length = 0;
            room.time_balance = 0;
            room.total_pause_time = 0;
            room.is_disconnected = false;
            room.client_disconnected = false;
            room.reader_disconnected = false;
            
            var standard_resp = roomManager.standardSuccessOutput(room);
            
            next(null, standard_resp);
            
        } else {
            next(null, {status:false, reason:'room not found'});
        }
    },
    
    update_max_length: function(room_name, new_max_length) {
        var room = _.findWhere(this.rooms, {room_name:room_name});
        room.max_chat_length = new_max_length;
    },
    
    add_chat_length: function(room, diff_time) {
       room.chat_length += parseInt(diff_time, 0);
       room.time_balance = room.max_chat_length - room.chat_length;
       
       if (room.chat_length > room.max_chat_length) {
           room.chat_length = room.max_chat_length;
       }
       if (room.chat_length < 0) {
           room.chat_length = 0;
       }
       if (room.time_balance > room.max_chat_length) {
           room.time_balance = room.max_chat_length;
       }
       if (room.time_balance < 0) {
           room.time_balance = 0;
       }
    },
    
    add_chat_free_length: function(room, diff_time) {
       room.free_length += parseInt(diff_time, 0);
       room.free_time_balance = room.total_free_length - room.free_length;
       
       if (room.free_length > room.total_free_length) {
           room.free_length = room.total_free_length;
       }
       if (room.free_length < 0) {
           room.free_length = 0;
       }
       if (room.free_time_balance > room.total_free_length) {
           room.free_time_balance = room.total_free_length;
       }
       if (room.free_time_balance < 0) {
           room.free_time_balance = 0;
       }
    },
    
    set_new_chat_data: function(room, data) {
        room.max_chat_length = data.max_chat_length;
        room.chat_lenth = data.chat_length;
        room.time_balance = data.time_balance;
    },
     
    standardSuccessOutput: function(room_data) {
        var result = {
            status              : true,
            start_time          : room_data.start_time || '',
            max_chat_length     : room_data.max_chat_length || 0, 
            chat_length         : room_data.chat_length || 0,
            time_balance        : room_data.time_balance || 0,
            last_updated_time   : room_data.last_upadted_time || '',
            is_pause            : room_data.is_pause || true,
            total_pause_time    : room_data.total_pause_time || 0,
            total_free_length   : room_data.total_free_length || 0,
            free_length         : room_data.free_length || 0,
            free_time_balance   : room_data.free_time_balance || 0            
        };
        return result;
    },
    
    job: {
        job_types: [
            {   name: "disconnection",  interval:1000, handler:null, data:{max:5000}, domain:null },
            {   name: "lobby_retry_and_abort",  interval:1000, handler:null, domain:null }
        ],
        jobs: {},
        add_to_watch: function(job_type, key) {
            if (! _.findWhere(roomManager.job.job_types, {name:job_type})) {
                return false;
            }
            
            if (! roomManager.job.jobs[job_type]) {
                roomManager.job.jobs[job_type] = [];
            }
            
            if (roomManager.job.jobs[job_type].lastIndexOf(key) <0) {
                roomManager.job.jobs[job_type].push(key); 
            }
            return true;
        },
        
        remove_from_watch: function(job_type, key) {
            if (! _.findWhere(roomManager.job.job_types, {name:job_type})) {
                return false;
            }
            if (! roomManager.job.jobs[job_type]) {
                return false;
            }
            var lastIndex = roomManager.job.jobs[job_type].lastIndexOf(key);
            if (lastIndex >= 0) {
                roomManager.job.jobs[job_type].splice(lastIndex, 1); 
            }
            return true;
        },
        
        run_disconnection: function() {
            //console.log(" >>>> run disconneciton JOB routine");  
            var job_type = _.findWhere(roomManager.job.job_types, {name:'disconnection'});
            var max_time_diff = job_type.data.max;
            
            var remove_keys = [];
            if (roomManager.job.jobs['disconnection'] && roomManager.job.jobs['disconnection'].length > 0) {
                
                console.log("Disconnection job " + JSON.stringify(roomManager.job.jobs['disconnection']));
                for (var i=0; i<roomManager.job.jobs['disconnection'].length; i++) {
                    var room_name = roomManager.job.jobs['disconnection'][i];
                    var room = roomManager.lookupRoom({room_name:room_name});     
                    
                    var time_diff = 0;
                    var now = new Date();
                    var member_type = 'reader';
                    if (room.reader_disconnected) {
                        if (room.reader_disconnected_time) {
                            time_diff = now - room.reader_disconnected_time;
                        }
                        member_type = 'reader';
                    } else if (room.client_disconnected) {
                        if (room.client_disconnected_time) {
                            time_diff = now - room.client_disconnected_time;
                        }
                        member_type = 'client';
                    }
                    
                    if (time_diff >= max_time_diff) {
                        // do the disconnection. 
                        console.log(" >>> run disconnection - end_chat_by_disconnection");
                        roomManager.end_chat_by_disconnection(room, member_type);
                    }
                }
                  
            }
        },
        run_lobby_retry_and_abort: function() {
        	//console.log("---- run_lobby_retry_and_abort before check room ");
        	var curr_time = new Date();
            // loop through all the room, and see which one is not joined yet. 
            var rooms = _.where(roomManager.rooms, {is_disconnected:false, reader_joined:false, client_joined:false, aborted:false, chat_attempt_accepted: false});
            _.each(rooms, function(room, index){
            	console.log("abort room Key is "  + index);
            	var diff = curr_time - room.chat_request_time;
            	console.log("time diff for room request: " + room.chat_id + " is: " + diff);
            	if (room.chat_request_counter > 0) {
	            	room.chat_request_counter --;
	            	roomManager.sendPing(room);
            	}
            	if (config.room.chat_request_max_pending_time <= diff) {
            		// now, should abort chat 
            		roomManager.autoAbortChatAttempt(room);
            	}
            });
            
        },

        
        run: function() {
            _.each(roomManager.job.job_types, function(obj, index, list) {
            	console.log("set run job");
                if (! obj.handler) {
                	obj.domain = domain.create(); 
                	obj.domain.run_function_hanlder = null;
                	obj.domain.run_function = function() {
                		obj.handler = setInterval(roomManager.job["run_"+obj.name], obj.interval);
                		obj.domain.run_function_hanlder = obj.handler;
                	};
                	
                	obj.domain.run(function() {
                    	obj.domain.run_function();
                  	});
                  	obj.domain.on('error', function(err) {
                  		try {
                  			clearInterval(this.run_function_hanlder);
                  		} catch(e) {
                  			console.log(" Cannot clear interval " + e.message);
                  		}
                  		obj.domain.run_function();
                  	});
                }
            });
        }
    },
    
    end_chat_by_disconnection: function(room, member_type) {
        var room_name = room.room_name;
        var reader_socket_id = room.reader_socket_id;
        var client_socket_id = room.client_socket_id;
        
        if (member_type == 'reader') {
            var member_hash = room.reader_hash;
            var member_id = room.reader_id;
            
            room.reader_disconnected_time = new Date();
            room.reader_disconnected = true;
            
        } else {
            var member_hash = room.client_hash;
            var member_id = room.client_id;
            
            room.client_disconnected_time = new Date();
            room.client_disconnected = true;
        }
        console.log("end_chat_by_disconnection by " + member_type + " room_name: " + room_name);
        
        async.waterfall([
            function (callback) {
                
                // check if 
                console.log("end_chat_by_disconnection Leave Chat");
                console.log("_explicitName " + userManager._explicitName);
                // pause billing
                userManager.leaveChat({room_name:room_name, member_hash:member_hash, terminated_by:member_type }, function(err, data) {
                    console.log("Disconnection leave chat: err " + JSON.stringify(err) + " data: "  + JSON.stringify(data)); 
                    if (err) {
                        callback(null, {status:true, has_time: false});
                    } else {
                        if (data && data.status) {
                            data.has_time = true;
                        } else {
                            data.has_time = false;
                        }
                        callback(null, data);
                    }
                });
            },
            
            function (data, callback) {
                // find out other party's socket, and send pause
                if (data) {
                    
                    server_resp = data;
                    server_resp.member_type = member_type;
                    server_resp.is_by_disconnection = true;
                    
                    console.log(" prepare to send out to reader_socket_id " + reader_socket_id);
                    var reader_socket = app.io.sockets.sockets[reader_socket_id];
                    if (reader_socket) {
                        console.log("sending leave chat room to reader");
                        reader_socket.emit('leave_chat_room', server_resp);
                    }
                    
                    console.log(" prepare to send out to client_socket_id " + client_socket_id);
                    var client_socket = app.io.sockets.sockets[client_socket_id];
                    if (client_socket) {
                        console.log("sending leave chat room to client");
                        client_socket.emit('leave_chat_room', server_resp);
                    }
                    
                    
                } else {
                    console.log("Unable to pause or end");
                }
                roomManager.job.remove_from_watch("disconnection", room_name);
                callback(null, {status:true});
            }
        ], 
        function (err, result){
            console.log("end_chat_by_disconnection Err: " + util.inspect(err));
            console.log("end_chat_by_disconnection result: " + util.inspect(result));
            // do nothing. 
        });  
    },
    
    abortChatAttempt: function(room, next) {
    	
    	var server_resp = {status:true};

        room.aborted = true;
        
        userManager.abortChat({site_url: room.site_url, room_name:room.room_name, member_hash:room.client_hash, aborted_by:"client", type:"manual" }, function(err, data) {
        	if (err) {
        		next(errorCode.gen('01202'));
        	} else {
        		if (data && data.status) {
        			server_resp.data = data;
        			next(null, server_resp);
        		} else {
        			server_resp.status = false;
        			next(null, server_resp);
        		}
        	}
        });
    },
    
    autoAbortChatAttempt: function(room) {
    	
    	try {
	    	var client_socket = app.io.sockets.sockets[room.client_lobby_socket_id];
	        var reader_socket_id = roomManager.member_socket_map[room.reader_id];
	        var reader_socket = app.io.sockets.sockets[reader_socket_id];
	        
	        var server_resp = {status:true};
	
	        room.aborted = true;
	        
	        userManager.abortChat({room_name:room.room_name, member_hash:room.client_hash, aborted_by:"client", type:"auto" }, function(err, data) {
	        	if (err) {
	        		server_resp.status = false;
	        		console.log("Error: cannot abort chat " + JSON.stringify(err));
	        	} else {
	        		console.log("Auto abort data is " + util.inspect(data));
	        		if (! (data && data.status)) {
	        			console.log("Abort chat fail: " + JSON.stringify(data));
	        		}
	        		try {
		        		server_resp.data = data;
		        		
		        		if (client_socket) {
				            console.log("client socket is found");
				            if (data.abort_to_logout) {
				            	client_socket.emit('chat_attempt_offline', server_resp);
				            } else {
				            	client_socket.emit('chat_attempt_abort', server_resp);
				            }
				        } else {
				        	console.log("Client socket not found");
				        }
				    } catch(e) {
			        	console.log("Unable to send autoabort to client");
			        }
			        try {    
				        if (reader_socket) {
				            console.log("reader socket is found");
				            if (data.abort_to_logout) {
				            	reader_socket.emit('chat_attempt_offline', server_resp);
				            } else {
				            	reader_socket.emit('chat_attempt_abort', server_resp);
				            }
				            
				        }  else {
				        	console.log("Reader socket not found");
				        }
			        } catch(e) {
			        	console.log("Unable to send autoabort to reader");
			        }
	        	}
	        });
		} catch (e) {
			console.log("Exception for autoabort " + e.message);
		}
    },
    
    sendPing: function(room) {
    	try {
	    	var server_resp = {status:true};
	    	var client_socket = app.io.sockets.sockets[room.client_lobby_socket_id];
	    	server_resp.wait_time_left = room.chat_request_counter;
	    	
	    	client_socket.emit('chat_attempt_ping', server_resp);
    	} catch (e) {
    		console.log("Unable to send ping to client");
    	}
    }
};




module.exports = roomManager;