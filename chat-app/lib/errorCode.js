
var errorCode = {   
    /* GENERAL */
    '00000': {message:'General Error', action:{retry:false}},
    '00001': {message:'Restricted Call', action:{retry:false}},
    '00002': {message:'Invalid Input', action:{retry:false}},
    
    /* Join Chat Room*/
    '00100':{message:'Chat room ID and Session missing', action:{retry:false}},
    '00101':{message:'Chat Validation Fail', action:{retry:false}},
    '00102':{message:'Chat already exist', action:{retry:false}},
    '00103':{message:'Reader already joined', action:{retry:false}},
    '00104':{message:'Client already joined', action:{retry:false}},
    '00105':{message:'Member not found', action:{retry:false}},
    '00106':{message:'Member ID not found', action:{retry:false}},
    '00107':{message:'Member Type mismatch', action:{retry:false}},
    '00109':{message:'Problem with dummy call', action:{retry:false}},
    '00110':{message:'Only client can create chat', action:{retry:false}},
    
    /* leave chat room */
    '00200':{message:'Fail in leaving chat room', action:{retry:false}},
    '00201':{message:'Chat room not found', action:{retry:false}},
    '00202':{message:'Unauthorized Chat Room termination', action:{retry:false}},
    '00203':{message:'Reader and Client both Discconnected', action:{retry:false}},
    '00204':{message:'Socket not found', action:{retry:false}},
    '00205':{message:'Unauthorized Chat Room rejection 1', action:{retry:false}},
    '00206':{message:'Unauthorized Chat Room rejection 2', action:{retry:false}},
    
    /* start billing */
    '00300':{message:'Unable to start billing', action:{retry:false}},
    
    /* end billing */
    '00400':{message:'Unable to end billing', action:{retry:false}},
    
    /* pause or resume billing */
    '00500':{message:'Unable to pause billing', action:{retry:false}},
    '00501':{message:'Already paused', action:{retry:false}},
    '00502':{message:'Cannot resume chat disconnected by other party', action:{retry:false}},
    
    /* log message */
    '00600':{message:'Unable to log message', action:{retry:false}},
    '00601':{message:'Room is paused or have not started.  Cannot send message', action:{retry:false}},
    
    /* refund chat */
    '00700':{message:'Unable to refund chat', action:{retry:false}},

    /* ban user */
    '00800':{message:'Unable to ban user', action:{retry:false}},
    
    /* purchase & add time. lost time */
    '00900':{message:'Unable to purchase time', action:{retry:false}},
    '00901':{message:'Unable to add stored time', action:{retry:false}},
    '00902':{message:'Lost time must be numeric and greater than 0', action:{retry:false}},
    '00903':{message:'Cannot be called when not pause', action:{retry:false}},
    '00904':{message:'Cannot be called as reader ', action:{retry:false}},
    '00905':{message:'Added time must be numeric and greater than 0', action:{retry:false}},
    '00906':{message:'Cannot be called as client ', action:{retry:false}},
    
    /* RoomManager */
    '01101':{message:'Search key not found', action:{retry:false}},
    
    /* Chat Attempt */
   	'01201':{message:'Unable to create chat attempt', action:{retry:false}},
    '01202':{message:'Unable to abort chat attempt', action:{retry:false}},
 
    
    /* Security and privilege */
    '88001':{message:'No Privilege to perform action',action:{retry:false}},

    
    /* Server and network error */
    '99001':{message:'Database is disconnected', action:{retry:false}},
    '99002':{message:'Document error', action:{retry:false}},
    '99003':{message:'Document not found', action:{retry:false}},
    '99004':{message:'Duplicate Document', action:{retry:false}},
    '99005':{message:'Remote Connection Fail', action:{retry:false}},
    '99999':{message:'Other Server errors', action:{retry:false}}
};

exports.gen = function(code, extra) {
    code = code+ '';    // convert to string;
    if (extra && typeof extra == 'object') {
        extra = JSON.stringify(extra);
    } else {
        extra = extra || '';
    }
    if (errorCode[code]) {
        return {
        	status:false,
            code:code,
            message:errorCode[code].message,
            action:errorCode[code].action,
            extra:extra
        };
    } else {
        return {
        	status:false,
            code:'0000',
            message:errorCode['00000'].message,
            action:errorCode['00000'].action,
            extra:extra || ''
        };
    }
}