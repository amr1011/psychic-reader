_ = require('underscore');
util = require('util');
express = require('express.io')
app = express().http().io()

app.use(express.cookieParser())
app.use(express.session({secret: 'monkey'}))

var req_map= {};

var testObj = {
 
    myVar : 1,
    create: function() {
         var whatever = "";
         whatever = this.myVar;
         this.myVar++;
    },
    another: function() {
        var whatever = this.myVar;
    }
}

app.io.on('connection', function(socket){
    var socket_id = socket.id;
    console.log("Connect: " + socket_id);
    console.log("1 - ALL Sockets: " + (_.keys(app.io.sockets.sockets)).toString() );
    testObj.create();
    testObj.another();
    socket.on('disconnect', function(req){
        console.log("2 - ALL Sockets: " + (_.keys(app.io.sockets.sockets)).toString() );
        console.log("Disconnect: " + socket_id);
        
    });
})

// Setup the ready route, join room and broadcast to room.
app.io.route('ready', function(req) {
    console.log("Req data is: " + util.inspect(req.data));
    var room = req.data.room;
    var name = req.data.name;
    req.io.emit('server_message', {message: "1st EMIT server to everyone? from " + name });
    
    req.session.name = name;
    req.session.room = room;
    req_map[name] = req.io;
    req.io.join(room);
    req.io.room(room).broadcast('announce', {
        message: 'New client '+ name+ ' in the ' + room + ' room. '
    })
    
    req.io.emit('server_message', {message: name + " Global EMIT server got it from " + name + " to room: " + room });
    req.io.broadcast('server_message', {message: name + " Global BROADCAST server got it to room: " + room });
})

app.io.route('message', function(req) {
    console.log("Req data is::  " + req.data);
    console.log("Session room is: " + req.session.room)
    req.io.emit('server_message', {message: "emit . server got it " + req.data.text });
    if (req.session.room) {
        req.io.room(req.session.room).broadcast('server_message', {
            message:  req.session.name + " just boardcast " + req.data.text + " to the room: " + req.session.room
        })
    }
})

app.io.route('private_message', function(req){
    var message = req.data.text;
    var user_name = req.data.user_name;
    
    if (req_map[user_name]) {
        console.log("sending a private message to " + user_name);
        req_map[user_name].emit('server_message', {message: "private message to " + user_name + " from: " + req.session.name + " message: " + message})
    }
})


// Send the client html.
app.get('/', function(req, res) {
    res.sendfile(__dirname + '/client.html')
})

app.get('/test', function(req, res) {
    res.sendfile(__dirname + '/test.html')
})


app.listen(7076)
