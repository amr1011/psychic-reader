_ = require('underscore');
util = require('util');

map = [
   { 'aaa' : 12345 },
   { 'bbb' : 22222, 'bbc' : 22333 },
   { 'ccc' : 44444 },

]
var result = _.findWhere(map, {ccc:44444});
result.ccc = 45678;
console.log(" changed array result : " + util.inspect(result));
console.log(" changed map : " + util.inspect(map));

var result = _.findWhere(map, {bbb:22222});
console.log("array result : " + util.inspect(result));
var index = _.indexOf(map, result);
console.log("Array index: " + index);
map.splice(index, 1);
console.log("new array result : " + util.inspect(map));

map = {
    'aaa' : 12345,
    'bbb' : 22222,
    'ccc' : 44444
}

map_pairs = _.pairs(map);
console.log("pairs: result : " + util.inspect(map_pairs));

var result = _.findWhere(map_pairs, {"1": 44444});
console.log("pairs: result : " + util.inspect(result));

var invert_map = _.invert(map);
console.log("result : " + util.inspect(map));
console.log("result : " + util.inspect(invert_map));
console.log("result : " + invert_map['22222']);

map = {
    "aaa" : {reader_id:12345, reader_hash:'aaa12345'},
    "bbb" : {reader_id:22222, reader_hash:'bbb22222'},
    "ccc" : {reader_id:44444, reader_hash:'ccc44444'}
}

map_pairs = _.pairs(map);
console.log("pairs result : " + util.inspect(map_pairs));
var result = _.findWhere(map_pairs, {"1": {reader_id:22222}});
console.log("pairs: result : " + util.inspect(result));






