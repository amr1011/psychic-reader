_ = require('underscore');


job = {
    job_types: [
            {   name: "disconnection",  interval:1000, handler:null },
            {   name: "lobby_retry",  interval:5000, handler:null }
        ],
    run_disconnection: function() {
        console.log("run disconneciton ");  
    },
    run_lobby_retry: function() {
        console.log("run retry ");  
    },
    
    run: function() {
        _.each(job.job_types, function(obj, index, list) {
            console.log("job name: " + obj.name);
            if (! obj.handler) {
                console.log("Handler is not null, attach interval");
                obj.handler = setInterval(job["run_"+obj.name], obj.interval);
            }
        })
    }
}

job.run();


