/*
 * Chat Server
 */
global._ = require('underscore');
global.async = require('async');
global.fs = require('fs');
global.config = require('../config/config.js');
global.errorCode = require('../lib/errorCode');
global.util = require('../lib/util');
global.logger = require('../lib/logger');
global.cycle = require('../lib/cycle');

global.express = require('express.io');
//global.app = require('express.io')();   // create the express.ios
global.userManager = require('../manager/userManager');
global.roomManager = require('../manager/roomManager');

// non global lib

if (config.isHttps) {
    console.log("Set https");
    global.app = express().http(config.httpsOptions).io();
} else {
    console.log("Set http");
    global.app = express().http().io();
}

/* handle uncaught exception */
process.on('uncaughtException', function (ex) {
    console.error(ex + "\n" + ex.stack);
    console.log("UncaughtException: " + ex);
    console.log("Stack: " + ex.stack);
    process.nextTick(function () {
        console.log("After uncaughtException");
    });
});

// run roomManager Job
roomManager.job.run();


// set up session and others
app.use(express.cookieParser());
app.use(express.session({secret: config.session.key}));


// do the initialization and terminate all active chat session because of server restart. 

// all path
global.socket_map = {};
app.io.on('connection', function (socket) {
    console.log("--- CONNECTION ------- START ---------");
    var socket_id = socket.id;
    socket_map[socket_id] = socket;
    console.log("Connect: " + socket_id);
    console.log("Connect member_socket_map: " + util.inspect(roomManager.member_socket_map));
    console.log("1 - ALL Sockets: " + (_.keys(app.io.sockets.sockets)).toString());



    //console.log("1 - ALL Sockets: " + JSON.stringify(JSON.decycle(app.io.sockets.sockets)));
    //console.log("1 - ALL Sockets: " + (_.keys(app.io.sockets.sockets)).toString() );
    socket.on('disconnect', function () {
        //console.log("2 - ALL Sockets: " + (_.keys(app.io.sockets.sockets)).toString() );
        console.log("Disconnection --------START -------");
        console.log("Disconnect: socket_id " + socket_id);
        console.log("Disconnect: roomManager  member_socket_map" + util.inspect(roomManager.member_socket_map));
        var removed_member_id = roomManager.cleanMemberSocket(socket_id);
        // terminate any room and stop billing. 
        console.log("Disconnect from Lobby page?. removed_member_id " + removed_member_id);
        var member_type = null;
        if (removed_member_id === false) {
            //on_disconnection(removed_member_id);
            console.log("Disconnect may not from lobby");
            // there is a case if the server restart in the middle. 
            // check if client or reader socket id is in the room. 
            var room = roomManager.lookupRoom({reader_socket_id: socket_id});
            if (room !== false) {
                member_type = 'reader';
                console.log("It is reader chat room");
            } else {
                room = roomManager.lookupRoom({client_socket_id: socket_id});
                if (room !== false) {
                    member_type = 'client';
                    console.log("It is client chat room");
                }
            }

            if (room === false) {
                //console.log("No chat room ");
                console.log("Disconnection --------END -------");
            } else {
                console.log("Handle chat room disconnection - asyn operation ");

                on_chatroom_disconnection(room, member_type);
            }

        } else {
            console.log("Disconnection from Lobby");
            // do nothing. because it is in lobby
            var room = null;
            var socket_member_type = '';
            console.log("try to find out if the socket_id is used in any room");
            room = roomManager.lookupRoom({reader_socket_id: socket_id});
            if (room !== false) {
                console.log("It is reader socket, room: " + util.inspect(room));
                socket_member_type = 'reader';
                removed_member_id = room.reader_id;
            } else {
                roomManager.lookupRoom({client_socket_id: socket_id});
            }
            if (room !== false) {
                console.log("It is client socket, room: " + util.inspect(room));
                socket_member_type = 'client';
                removed_member_id = room.client_id;
            }
            console.log("Disconnection --------END -------");
        }
        //on_disconnection(removed_member_id);


    });
});


var on_chatroom_disconnection = function (room, member_type) {
    var server_resp = null;
    var room_name = room.room_name;
    var other_socket_id = null;
    if (member_type == 'reader') {
        var member_hash = room.reader_hash;
        var member_id = room.reader_id;
        room.reader_disconnected_time = new Date();
        room.reader_disconnected = true;
    } else {
        var member_hash = room.client_hash;
        var member_id = room.client_id;
        room.client_disconnected_time = new Date();
        room.client_disconnected = true;
    }
    console.log("on_chatroom_disconnection by " + member_type + " room_name: " + room_name);

    async.waterfall([
        function (callback) {

            // check if 
            console.log("on_disconnection Pause Billing");
            // pause billing
            if (room.is_disconnected) {
                console.log("Yes.. it is already discconnected");
                // it is already disconnected.. so now. both reader and client disconnect tgt,
                // throw an error and so it will end the chat right away.
                callback(errorCode.gen('00203'));
            } else {

                if (room.is_pause) {
                    callback(null, {status: true});
                } else {
                    console.log("Sending pauseBilling to userManager. ");
                    userManager.pauseBilling({room_name: room_name, member_hash: member_hash}, function (err, data) {
                        console.log("Pause billing data: " + JSON.stringify(data));
                        if (data && data.status) {
                            data.has_time = true;
                            callback(null, data);
                        } else {
                            console.log("Unable to pause");
                            userManager.leaveChat({room_name: room_name, member_hash: member_hash, terminated_by: member_type}, function (err, data) {
                                if (err) {
                                    callback(null, {status: true, has_time: false});
                                } else {
                                    if (data && data.status) {
                                        data.has_time = true;
                                    } else {
                                        data.has_time = false;
                                    }
                                    callback(null, data);
                                }
                            });
                        }
                    });
                }
            }
        },
        function (data, callback) {
            // find out other party's socket, and send pause
            console.log(" IN: data is " + JSON.stringify(data));
            if (data && data.status) {

                server_resp = data;
                server_resp.member_type = member_type;
                server_resp.is_by_disconnection = true;

                if (member_type == 'reader') {
                    other_socket_id = room.client_socket_id;

                } else {
                    other_socket_id = room.reader_socket_id;
                }
                console.log("other party's socket id: " + other_socket_id);

                var other_socket = app.io.sockets.sockets[other_socket_id];
                if (other_socket) {
                    console.log("Emitting pause message");
                    // emit pause message    
                    if (data.has_time) {
                        other_socket.emit('pause', server_resp);
                    } else {
                        server_resp.pause_no_time = true;
                    }
                    other_socket.emit('pause', server_resp);
                } else {
                    console.log(" ALL Sockets: " + (_.keys(app.io.sockets.sockets)).toString());
                    console.log("Socket not found for other_socket_id: " + other_socket_id);
                }
            } else {
                console.log("Unable to pause or end");
            }
            callback(null, {status: true});
        },
        function (data, callback) {
            // register in roomManager for countdown. 
            if (data && data.status) {

                roomManager.job.add_to_watch("disconnection", room_name);
                callback(null, {status: true});
            } else {
                console.log("Unable to pause or end");
                callback(errorCode.gen('00204'));
            }
        }
    ],
            function (err, result) {
                console.log("on_chatroom_disconnection Err: " + util.inspect(err));
                console.log("on_chatroom_disconnection result: " + util.inspect(result));
                // do nothing. 
                console.log("Disconnection --------END -------");
            });
};

// ------- chat attempt and abort BEGIN ----------

app.io.route('chat_attempt', function (req) {

    var socket_id = req.socket.id;
    var member_id = req.data.member_id;
    var member_id_hash = req.data.member_id_hash;
    var site_url = req.data.site_url;
    var client_url = req.data.client_url;

    var reader_id = req.data.reader_id || '';
    var tier = req.data.tier || '';
    var topic = req.data.topic || '';
    var minutes = req.data.minutes || '';
    var free_minutes = req.data.free_minutes || '';

    async.waterfall([
        function (callback) {
            var err = null;
            if (!reader_id) {
                err = errorCode.gen('00002');
                err.extra = "Reader Id is empty";
            }
            if (!topic) {
                err = errorCode.gen('00002');
                err.extra = "Topic is empty";
            }
            if (!parseInt(minutes, 10) > 0) {
                err = errorCode.gen('00002');
                err.extra = "Minutes is not a number";
            }
            minutes = parseInt(minutes, 10);

            if (err) {
                callback(err);
            } else {
                callback(null, {});
            }
            ;
        },
        function (data, callback) {
            // pass in chat room id, hash code, user id. 
            var input = {
                member_id: member_id,
                member_id_hash: member_id_hash,
                site_url: site_url
            };
            userManager.validate_member_id(input, callback);
        },
        function (data, callback) {
            // create a room
            if (data.member_type == 'client') {
                // good. only client can create chat.
                // make a call to roomManager to create a chat room in DB. 
                var input = {
                    client_id: member_id,
                    client_id_hash: member_id_hash,
                    client_lobby_socket_id: socket_id,
                    reader_id: reader_id,
                    topic: topic,
                    minutes: minutes,
                    free_minutes: free_minutes,
                    site_url: site_url,
                    client_url: client_url,
                    tier: tier
                };
                roomManager.confirmChatRoom(input, callback);
            } else {
                callback(errorCode.gen('00110'));
            }
        }
    ], function (err, result) {
        if (err) {
            req.io.respond(err);    // ack back to the client. 
        } else {
            console.log("Result -- start");
            console.log(result);
            console.log("Result -- end ");
            var output = {
                chat_id: result.data.chat_id,
                chat_session_id: result.data.room_name,
                redirect_url: result.data.redirect_url,
                member_hash: result.data.client_hash,
                client_username: result.data.client_username,
                reader_username: result.data.reader_username,
                disable_manual_quit: result.data.disable_manual_quit,
                free_minutes: result.data.total_free_length
            };

            req.io.respond({status: true, data: output});

            try {
                var reader_socket_id = roomManager.getMemberSocket(reader_id);
                var reader_socket = util.findSocket(req, reader_socket_id);
                if (reader_socket) {
                    // emit to reader's socket if user exist. 
                    reader_socket.emit('chat_attempt', {status: true, data: output});
                }
            } catch (ex) {
                console.log("chat_attempt Execption: " + ex.message);
            }
        }
    });
});
//  --------------------------test------------------------
app.io.route('test', function (req) {
    
    var socket_id = req.socket.id;
    var member_id = req.data.member_id;
    var member_id_hash = req.data.member_id_hash;
    var site_url = req.data.site_url;
    var client_url = req.data.client_url;

    var reader_id = req.data.reader_id || '';
    var tier = req.data.tier || '';
    var topic = req.data.topic || '';
    var minutes = req.data.minutes || '';
    var free_minutes = req.data.free_minutes || '';

    async.waterfall([
        function (callback) {
            var err = null;
            if (!reader_id) {
                err = errorCode.gen('00002');
                err.extra = "Reader Id is empty";
            }
            if (!topic) {
                err = errorCode.gen('00002');
                err.extra = "Topic is empty";
            }
            if (!parseInt(minutes, 10) > 0) {
                err = errorCode.gen('00002');
                err.extra = "Minutes is not a number";
            }
            minutes = parseInt(minutes, 10);

            if (err) {
                callback(err);
            } else {
                callback(null, {});
            }
            ;
        },
        function (data, callback) {
            // pass in chat room id, hash code, user id. 
            var input = {
                member_id: member_id,
                member_id_hash: member_id_hash,
                site_url: site_url
            };
            userManager.validate_member_id(input, callback);
        },
        function (data, callback) {
            // create a room
            if (data.member_type == 'client') {
                // good. only client can create chat.
                // make a call to roomManager to create a chat room in DB. 
                var input = {
                    client_id: member_id,
                    client_id_hash: member_id_hash,
                    client_lobby_socket_id: socket_id,
                    reader_id: reader_id,
                    topic: topic,
                    minutes: minutes,
                    free_minutes: free_minutes,
                    site_url: site_url,
                    client_url: client_url,
                    tier: tier
                };
                roomManager.confirmChatRoom(input, callback);
                // roomManager.lookupRoom({room_name: room_name}, callback);
            } else {
                callback(errorCode.gen('00110'));
            }
        }
    ], function (err, result) {
        if (err) {
            req.io.respond(err);    // ack back to the client. 
        } else {
            console.log("Result -- start");
            console.log(result);
            console.log("Result -- end ");
            var output = {
                chat_id: result.data.chat_id,
                chat_session_id: result.data.room_name,
                redirect_url: result.data.redirect_url,
                member_hash: result.data.client_hash,
                client_username: result.data.client_username,
                reader_username: result.data.reader_username,
                disable_manual_quit: result.data.disable_manual_quit,
                free_minutes: result.data.total_free_length
            };

            req.io.respond({status: true, data: output});

            try {
                var reader_socket_id = roomManager.getMemberSocket(reader_id);
                var reader_socket = util.findSocket(req, reader_socket_id);
                if (reader_socket) {
                    // emit to reader's socket if user exist. 
                    reader_socket.emit('test', {status: true, data: output});
                }
            } catch (ex) {
                console.log("chat_attempt Execption: " + ex.message);
            }
        }
    });
});
//  --------------------------test------------------------
app.io.route('chat_attempt_abort', function (req) {
    console.log("\n\n--------------- chat_attempt_abort ");
    console.log("chat_attempt " + util.inspect(req.data));
    var room_name = req.data.room_name; // only exist if client has joined the room
    var member_hash = req.data.member_hash; // it is either client or reader.  
    var site_url = req.data.site_url;
    var client_id = '';
    var reader_id = '';
    var redirect = '';
    var member_type = '';
    var room = {};
    var server_resp = {};

    // send to PHP server , and tell it to
    if (!room_name) {
        req.io.respond(errorCode.gen('00100'));
        return;// exist
    }

    async.waterfall([
        function (callback) {
            // check if room_name is found.
            roomManager.lookupRoom({room_name: room_name}, callback);
        },
        function (data, callback) {

            if (_.isEmpty(data)) {
                callback(errorCode.gen('00201'));
            } else {
                room = data;
                room.site_url = site_url;
                if (data.client_hash == member_hash) {
                    // terminated by reader
                    aborted_by = 'client';
                    member_id = data.client_id;
                    member_type = 'client';
                    reader_id = data.reader_id;
                    roomManager.abortChatAttempt(room, callback);
                } else {
                    callback(errorCode.gen('01202'));
                }
            }
        }
    ], function (err, result) {
        console.log("abort chat attempt: ERR: " + util.inspect(err));
        console.log("abort chat attempt: Result: " + util.inspect(result));

        if (err) {
            req.io.respond(err);    // ack back to the client. 
        } else {

            var client_socket = app.io.sockets.sockets[room.client_lobby_socket_id];
            var reader_socket_id = roomManager.member_socket_map[room.reader_id];
            var reader_socket = app.io.sockets.sockets[reader_socket_id];
            var server_resp = result;

            try {
                if (client_socket) {
                    console.log("client socket is found");
                    client_socket.emit('chat_attempt_abort', server_resp);
                } else {
                    console.log("Client socket not found");
                }
            } catch (ex) {
                console.log("chat_attempt-abort exception for client " + ex.message);
            }

            try {
                if (reader_socket) {
                    console.log("reader socket is found");
                    reader_socket.emit('chat_attempt_abort', server_resp);
                } else {
                    console.log("reader socket not found");
                }
            } catch (ex) {
                console.log("chat_attempt_abort exception for reader " + ex.message);
            }

            roomManager.removeRoom({room_name: room.room_name}, function (err, data) {
                if (err) {
                    console.log("problems with removing room: " + util.inspect(err));
                }
            });
        }
    });
});


// reject chat attempt by reader
app.io.route('reader_reject_chat_attempt', function (req) {
    console.log("\n\n--------------- reader_reject_chat_attempt ");
    var socket_id = req.socket.id;
    var chat_session_id = req.data.chat_session_id || req.data.room_name; // chat_session_id is the room name.
    var room_name = chat_session_id;
    var member_id = req.data.member_id;
    var member_id_hash = req.data.member_id_hash; // it is either client or reader.  
    var reader_username = '';
    var site_url = req.data.site_url;
    var should_register = true;
    var room = {};

    console.log("Send reject!!!, member_hash: " + member_id_hash);
    async.waterfall([
        function (callback) {
            // pass in chat room id, hash code, user id. 
            var input = {
                member_id: member_id,
                member_id_hash: member_id_hash,
                site_url: site_url
            };
            userManager.validate_member_id(input, callback);
        },
        function (data, callback) {
            console.log("what? data" + util.inspect(data));
            if (data && data.status) {
                if (data.member_type == 'reader') {
                    // check if teh room exist. 
                    roomManager.lookupRoom({room_name: chat_session_id}, callback);
                } else {
                    callback(errorCode.gen('00107'));
                }
            } else {
                console.log("Error " + util.inspect(data));
                callback(errorCode.gen('00106'));
            }
        },
        function (data, callback) {

            if (_.isEmpty(data)) {
                callback(errorCode.gen('00201'));
            } else {
                room = data;
                console.log("reject chat: DATA after lookup is : " + util.inspect(data));
                room.aborted = true; // reject.  so the routine will ignore it. 
                if (data.client_joined == true) {
                    callback(errorCode.gen('00205'));
                } else if (data.started_already == true || data.reader_joined == true) {
                    callback(errorCode.gen('00206'));
                } else if (data.reader_id == member_id) {
                    // rejected by reader
                    terminated_by = 'reader';
                    member_id = req.session.reader_id;
                    member_type = 'reader';
                    reader_username = data.reader_username;
                    console.log("Before calling reject chat");
                    userManager.rejectChat({room_name: room_name, member_hash: data.reader_hash, rejected_by: 'reader', site_url: site_url}, callback);
                } else {
                    callback(errorCode.gen('00202'));
                }
            }
        }
    ], function (err, result) {
        console.log("reject chat: ERR: " + util.inspect(err));
        console.log("reject chat: Result: " + util.inspect(result));

        if (err) {
            req.io.respond(err);    // ack back to the client. 
        } else {

            var client_socket = app.io.sockets.sockets[room.client_lobby_socket_id];

            var server_resp = result;
            try {
                if (client_socket) {
                    console.log("client socket is found");
                    client_socket.emit('reader_reject_chat_attempt', server_resp);
                } else {
                    console.log("Client socket not found");
                }
            } catch (ex) {
                console.log("reader_reject_chat_attempt exception for client " + ex.message);
            }

            req.io.respond(server_resp);

            roomManager.removeRoom({room_name: room.room_name}, function (err, data) {
                if (err) {
                    console.log("problems with removing room: " + util.inspect(err));
                }
            });
        }
        console.log("------------------\n\n");
    });
});
//8-18
app.io.route('reader_refuse_chat_attempt', function (req) {
    console.log("\n\n--------------- reader_refuse_chat_attempt ");
    var socket_id = req.socket.id;
    var chat_session_id = req.data.chat_session_id || req.data.room_name; // chat_session_id is the room name.
    var room_name = chat_session_id;
    var member_id = req.data.member_id;
    var member_id_hash = req.data.member_id_hash; // it is either client or reader.  
    var reader_username = '';
    var site_url = req.data.site_url;
    var should_register = true;
    var room = {};

    console.log("Send reject!!!, member_hash: " + member_id_hash);
    async.waterfall([
        function (callback) {
            // pass in chat room id, hash code, user id. 
            var input = {
                member_id: member_id,
                member_id_hash: member_id_hash,
                site_url: site_url
            };
            userManager.validate_member_id(input, callback);
        },
        function (data, callback) {
            console.log("what? data" + util.inspect(data));
            if (data && data.status) {
                if (data.member_type == 'reader') {
                    // check if teh room exist. 
                    roomManager.lookupRoom({room_name: chat_session_id}, callback);
                } else {
                    callback(errorCode.gen('00107'));
                }
            } else {
                console.log("Error " + util.inspect(data));
                callback(errorCode.gen('00106'));
            }
        },
        function (data, callback) {

            if (_.isEmpty(data)) {
                callback(errorCode.gen('00201'));
            } else {
                room = data;
                console.log("reject chat: DATA after lookup is : " + util.inspect(data));
                room.aborted = true; // reject.  so the routine will ignore it. 
                if (data.client_joined == true) {
                    callback(errorCode.gen('00205'));
                } else if (data.started_already == true || data.reader_joined == true) {
                    callback(errorCode.gen('00206'));
                } else if (data.reader_id == member_id) {
                    // rejected by reader
                    terminated_by = 'reader';
                    member_id = req.session.reader_id;
                    member_type = 'reader';
                    reader_username = data.reader_username;
                    console.log("Before calling reject chat");
                    userManager.rejectChat({room_name: room_name, member_hash: data.reader_hash, rejected_by: 'reader', site_url: site_url}, callback);
                } else {
                    callback(errorCode.gen('00202'));
                }
            }
        }
    ], function (err, result) {
        console.log("reject chat: ERR: " + util.inspect(err));
        console.log("reject chat: Result: " + util.inspect(result));

        if (err) {
            req.io.respond(err);    // ack back to the client. 
        } else {

            var client_socket = app.io.sockets.sockets[room.client_lobby_socket_id];

            var server_resp = result;
            
            try {
                if (client_socket) {
                    console.log("client socket is found");
                    client_socket.emit('reader_refuse_chat_attempt', server_resp);
                } else {
                    console.log("Client socket not found");
                }
            } catch (ex) {
                console.log("reader_reject_chat_attempt exception for client " + ex.message);
            }

            req.io.respond(server_resp);

            roomManager.removeRoom({room_name: room.room_name}, function (err, data) {
                if (err) {
                    console.log("problems with removing room: " + util.inspect(err));
                }
            });
        }
        console.log("------------------\n\n");
    });
});

app.io.route('reader_accept_chat_attempt', function (req) {
    console.log("\n\n--------------- reader_accept_chat_attempt ");
    var chat_session_id = req.data.chat_session_id || req.data.room_name; // chat_session_id is the room name.
    var room_name = chat_session_id;
    var member_id = req.data.member_id;
    var member_id_hash = req.data.member_id_hash; // it is either client or reader.  
    var site_url = req.data.site_url;
    var client_url = req.data.client_url;
    var reader_username = '';
    var should_register = true;
    var room = {};

    async.waterfall([
        function (callback) {
            // pass in chat room id, hash code, user id. 
            var input = {
                member_id: member_id,
                member_id_hash: member_id_hash,
                site_url: site_url
            };
            console.log("Send accept, validat input: " + util.inspect(input));
            userManager.validate_member_id(input, callback);
        },
        function (data, callback) {

            if (data && data.status) {
                if (data.member_type == 'reader') {
                    // check if teh room exist. 
                    roomManager.lookupRoom({room_name: chat_session_id}, callback);
                } else {
                    callback(errorCode.gen('00107'));
                }
            } else {
                console.log("Error " + util.inspect(data));
                callback(errorCode.gen('00106'));
            }
        },
        function (data, callback) {

            if (_.isEmpty(data)) {
                callback(errorCode.gen('00201'));
            } else {
                room = data;
                console.log("accept chat: DATA after lookup is : " + util.inspect(data));
                room.chat_attempt_accepted = true; // accepted.  so the routine will ignore it. 
                callback(null, {status: true});
            }
        }
    ], function (err, result) {
        console.log("ACCEPT chat: ERR: " + util.inspect(err));
        console.log("ACCEPT chat: Result: " + util.inspect(result));

        if (err) {
            req.io.respond(err);    // ack back to the client. 
        } else {

            var client_socket = app.io.sockets.sockets[room.client_lobby_socket_id];

            var server_resp = result;
            try {
                if (client_socket) {
                    console.log("client socket is found");
                    client_socket.emit('reader_accept_chat_attempt', server_resp);
                } else {
                    console.log("Client socket not found");
                }
            } catch (ex) {
                console.log("reader_accept_chat_attempt exception for client " + ex.message);
            }

            req.io.respond(server_resp);

        }
        console.log("------------------\n\n");
    });
});

// ------- chat attempt and abort END ----------

// register socket for global targetted message
app.io.route('register', function (req) {
    console.log("register is called ------------ ");
    var socket_id = req.socket.id;
    var member_id = req.data.member_id;
    var member_id_hash = req.data.member_id_hash;
    var site_url = req.data.site_url;
    var should_register = true;
    console.log("Send register, member_id: " + member_id + " member_id_hash: " + member_id_hash);
    async.waterfall([
        function (callback) {
            // check if the memberid and socket_id already exist and match
            var member_socket_id = roomManager.getMemberSocket(member_id);
            console.log("member socket id is " + member_socket_id + " and socket_id is: " + socket_id);
            if (member_socket_id && member_socket_id == socket_id) {
                console.log("member has socket stored already");
                should_register = false;
                callback(null, {});
            } else {
                console.log("member hasn't registered the socket. create one.");
                should_register = true;
                callback(null, {});
            }
        },
        function (data, callback) {
            if (should_register) {
                // pass in chat room id, hash code, user id. 
                var input = {
                    member_id: member_id,
                    member_id_hash: member_id_hash,
                    site_url: site_url
                };
                userManager.validate_member_id(input, callback);
            } else {
                callback(null, {status: true});
            }
        },
        function (data, callback) {
            if (should_register) {
                console.log("call back data: " + util.inspect(data));
                if (data && data.status) {
                    console.log("Setting member socket member_id" + member_id + " socket id: " + socket_id);
                    roomManager.setMemberSocket(member_id, socket_id);
                    callback(null, {status: true});
                } else {
                    callback(errorCode.gen('00106'));
                }
            } else {
                callback(null, {status: true});
            }
        }
    ], function (err, result) {
        console.log("Register Err: " + util.inspect(err));
        console.log("Register result: " + util.inspect(result));
        if (err) {
            req.io.respond(err);
        } else {
            req.io.respond({status: true});
        }

    });

});



app.io.route('readers_list', function (req) {
    var site_url = req.data.site_url;
    console.log("readers_list is called ------------ ");
    var socket_id = req.socket.id;
    userManager.get_readers_list({site_url: site_url}, function (data) {
        req.io.respond({status: true, json: data});
    });
});

app.io.route('check_register_client', function (req) {
    var site_url = req.data.site_url;
    var username = req.data.username;
    var email = req.data.email;

    console.log("check_register_client is called ------------ ");
    var socket_id = req.socket.id;
    var input = {
        site_url: site_url,
        username: username,
        email: email
    };
    userManager.check_register_client(input, function (data) {
        req.io.respond({status: true, json: data});
    });
});


/*
 * When Join, validate with chat-php-api, send up the session, and send it back a room if normal user, or a list of rooms for expert to choose for join. 
 * 
 */
app.io.route('join', function (req) {
    console.log("\n\n--------------- join ");
    console.log("received join message " + util.inspect(req.data));

    var socket_id = req.socket.id;
    var chat_id = req.data.chat_id;
    var chat_session_id = req.data.chat_session_id; // chat_session_id is the room name.
    var member_hash = req.data.member_hash; // it is either client or reader.  
    var transcript = '';
    var is_rejoin = false;
    var join_message = '';
    var site_url = req.data.site_url;

    if (!chat_id || !chat_session_id) {
        req.io.respond({error: errorCode.gen('00100')});
        return;// exist
    }
    var room_data = {};
    var member_type = '';

    async.waterfall([
        function (callback) {
            // pass in chat room id, hash code, user id. 
            var input = {
                chat_id: chat_id,
                chat_session_id: chat_session_id,
                member_hash: member_hash,
                site_url: site_url
            };
            userManager.validate(input, callback);
        },
        function (data, callback) {
            // join a room
            if (member_hash == data.reader_hash) {
                member_type = "reader";
            } else if (member_hash == data.client_hash) {
                member_type = "client";
            }

            if (member_type != "") {
                room_data = {
                    chat_id: chat_id,
                    room_name: data.chat_session_id, // use that as the room name
                    client_id: data.client_id,
                    client_hash: data.client_hash,
                    client_username: data.client_username,
                    reader_id: data.reader_id,
                    reader_hash: data.reader_hash,
                    reader_username: data.reader_username,
                    site_url: site_url
                };

                // a simple room manager.
                roomManager.createOrJoinRoom({
                    room_data: room_data,
                    member_type: member_type,
                    member_socket_id: socket_id,
                    site_url: site_url
                }, callback);
            } else {
                callback(errorCode.gen('00105'));
            }
        },
        function (data, callback) {
            // join the room.  but DO NOT Start
            console.log("Join return room name:" + data.room);
            room_data = data.room;
            req.io.join(room_data.room_name);
            // now also save in session
            req.session.chat_id = room_data.chat_id;
            req.session.room_name = room_data.room_name;
            req.session.client_hash = room_data.client_hash;
            req.session.client_id = room_data.client_id;
            req.session.client_username = room_data.client_username;
            req.session.reader_hash = room_data.reader_hash;
            req.session.reader_id = room_data.reader_id;
            req.session.reader_username = room_data.reader_username;
            req.session.member_hash = member_hash;

            join_message = data.message;

            callback(null);
        }
    ], function (err, result) {
        console.log("Join room Err: " + util.inspect(err));
        console.log("Join result: " + util.inspect(result));
        console.log("Room data is. " + util.inspect(room_data));
        if (err) {
            req.io.respond(err);    // ack back to the client. 
        } else {
            // success
            // if it is client creating the chat connection, then announce to the world that he is looking for reader_id to join
            // just expose the reader_id
            var res_message = '';
            if (member_type == 'client') {

                try {
                    // then send a message to reader in lobby. 
                    var reader_socket_id = roomManager.getMemberSocket(room_data.reader_id);
                    // get socket.
                    var reader_socket = util.findSocket(req, reader_socket_id);
                    if (reader_socket) {
                        console.log("reader socket is found, socket_id: " + reader_socket_id);
                        // emit to reader's socket if user exist. 
                        reader_socket.emit('join_new_chat_room', {
                            reader_id: room_data.reader_id,
                            client_id: room_data.client_id,
                            client_username: room_data.client_username,
                            chat_id: room_data.chat_id,
                            chat_session_id:
                                    room_data.room_name,
                            is_rejoin: is_rejoin,
                            join_message: join_message,
                            redirect_url: room_data.redirect_url});
                        res_message = "client create a new room and notify connected reader";
                    } else {
                        console.log('reader socket not found, broadcast');
                        // braodcast?? 
                        req.io.broadcast('join_new_chat_room', {reader_id: room_data.reader_id, join_message: join_message});  // or app.io
                        res_message = "client create a new room";
                    }
                } catch (ex) {
                    console.log("join exception for reader " + ex.message);
                }
            } else {
                // because member_type is checked earlier. assume if not client,then it has to be reader
                // reader join, tell client he has joined. 
                res_message = "reader " + room_data.reader_username + " join the room";
                req.io.room(room_data.room_name).broadcast('reader_join_new_chat_room', {reader_id: room_data.reader_id, message: res_message, is_rejoin: is_rejoin, join_message: join_message});

            }
            console.log("Join send response. " + res_message);
            req.io.respond({status: true, member_type: member_type, message: res_message, transcript: transcript, join_message: join_message});
        }
        console.log("------------------\n\n");
    });
});

app.io.route('join_notification', function (req) {
    console.log("Resend join notification to reader");
});

/*
 * Explicitly leaving 
 * 
 */
app.io.route('leave', function (req) {
    // leave
    var room_name = req.session.room_name; // only exist if client has joined the room
    var member_hash = req.session.member_hash; // it is either client or reader.  
    var site_url = req.data.site_url;
    var client_id = '';
    var reader_id = '';
    var client_hash = "";
    var reader_hash = "";

    if (!room_name) {
        req.io.respond(errorCode.gen('00100'));
        return;// exist
    }
    var member_type = '';
    var terminated_by = '';
    var member_id = '';

    async.waterfall([
        function (callback) {
            // check if room_name is found.
            roomManager.lookupRoom({room_name: room_name}, callback);
        },
        function (data, callback) {

            if (_.isEmpty(data)) {
                callback(errorCode.gen('00201'));
            } else {
                reader_id = data.reader_id;
                client_id = data.client_id;
                reader_hash = data.reader_hash;
                client_hash = data.client_hash;
                if (data.reader_hash == member_hash) {
                    // terminated by reader
                    terminated_by = 'reader';
                    member_id = req.session.reader_id;
                    member_type = 'reader';
                    userManager.leaveChat({room_name: room_name, member_hash: reader_hash, terminated_by: terminated_by, site_url: site_url}, callback);
                } else if (data.client_hash == member_hash) {
                    // terminate by client
                    terminated_by = 'client';
                    member_id = req.session.client_id;
                    member_type = 'client';
                    userManager.leaveChat({room_name: room_name, member_hash: client_hash, terminated_by: terminated_by, site_url: site_url}, callback);
                } else {
                    callback(errorCode.gen('00202'));
                }
            }
        }
    ], function (err, result) {
        console.log("leave chat: ERR: " + util.inspect(err));
        console.log("leave chat: Result: " + util.inspect(result));

        delete req.session.room_name;
        delete req.session.reader_id;
        delete req.session.reader_hash;
        delete req.session.reader_username;
        delete req.session.client_id;
        delete req.session.client_hash;
        delete req.session.client_username;
        delete req.session.member_hash;

        if (err) {
            req.io.respond(err);    // ack back to the client. 
        } else {
            var server_resp = result;
            // success
            // if it is client creating the chat connection, then announce to the world that he is looking for reader_id to leave
            // just expose the reader_id
            if (member_type == 'client') {
                server_resp.client_id = client_id;
                server_resp.member_id = member_id;
                server_resp.member_type = 'client';
                req.io.room(room_name).broadcast('leave_chat_room', server_resp);
                //req.io.room(room_name).broadcast('leave_chat_room', {status:true, client_id:client_id, duration:result.total_elasped, member_id:member_id, member_type:'client'});  // or app.io
            } else {
                // because member_type is checked earlier. assume if not client,then it has to be reader
                server_resp.reader_id = reader_id;
                server_resp.member_id = member_id;
                server_resp.member_type = 'reader';
                req.io.room(room_name).broadcast('leave_chat_room', server_resp);
                //req.io.room(room_name).broadcast('leave_chat_room', {status:true, reader_id:reader_id, duration:result.total_elasped, member_id:member_id, member_type:'reader'});
            }
            if (room_name) {
                req.io.leave(room_name);
            }
            server_resp.member_id = member_id;
            server_resp.member_type = terminated_by;
            req.io.respond(server_resp);
        }
    });
});

/*
 * Start the chat, and start billing. 
 */
app.io.route('start', function (req) {
    // start  assume already in the room.
    var room_name = req.session.room_name; // only exist if client has joined the room
    var member_hash = req.session.member_hash; // it is either client or reader.  
    var client_id = '';
    var reader_id = '';
    var start_time = '';
    var time_balance = 0;
    var max_chat_length = 0;
    var chat_length = 0;

    // send to PHP server , and tell it to start billing.
    // this action should be initiated by client only. 
    if (!room_name) {
        req.io.respond(errorCode.gen('00100'));
        return;// exist
    }
    var member_type = '';
    var terminated_by = '';

    async.waterfall([
        function (callback) {
            // check if room_name is found.
            roomManager.lookupRoom({room_name: room_name}, callback);
        },
        function (data, callback) {

            if (_.isEmpty(data)) {
                callback(errorCode.gen('00201'));
            } else {
                client_id = data.client_id;
                if (data.client_hash == member_hash) {
                    // good. initiated by client
                    userManager.startBilling({room_name: room_name, member_hash: member_hash, site_url: data.site_url}, callback);
                } else {
                    callback(errorCode.gen('00202'));
                }
            }
        },
        function (data, callback) {
            if (data && data.status) {
                console.log("after start billing: " + util.inspect(data));
                callback(null, data);
            } else {
                callback(errorCode.gen('00300'));
            }
        }
    ], function (err, result) {
        console.log("after start billing: Err " + util.inspect(err));
        console.log("after start billing: Result " + util.inspect(result));
        if (err) {
            req.io.respond(err);    // ack back to the client. 
        } else {
            var server_resp = result;
            server_resp.client_id = client_id;
            // success
            // if it is client creating the chat connection, then announce to the world that he is looking for reader_id to join
            // just expose the reader_id
            req.io.room(room_name).broadcast('start', server_resp);
            req.io.respond(server_resp);
        }
    });
});

// Pause timer.
app.io.route('pause', function (req) {
    var room_name = req.session.room_name; // only exist if client has joined the room
    var member_hash = req.session.member_hash; // it is either client or reader.      
    var site_url = req.data.site_url;
    var client_id = '';
    var reader_id = '';
    var diff_time = '';
    var room;

    // send to PHP server , and tell it to pause billing.
    // this action should be initiated by client only. 
    if (!room_name) {
        req.io.respond(errorCode.gen('00100'));
        return;// exist
    }
    var member_type = '';
    var terminated_by = '';

    async.waterfall([
        function (callback) {
            // check if room_name is found.
            roomManager.lookupRoom({room_name: room_name}, callback);
        },
        function (data, callback) {

            if (_.isEmpty(data)) {
                callback(errorCode.gen('00201'));
            } else {
                room = data;
                client_id = data.client_id;
                if (data.client_hash == member_hash) {
                    member_type = 'client';
                    room.paused_by = 'client';
                } else {
                    member_type = 'reader';
                    room.paused_by = 'reader';
                }

                // if pause already, no need to pause again
                if (room.is_pause) {

                } else {
                    userManager.pauseBilling({room_name: room_name, member_hash: member_hash, site_url: site_url}, callback);
                }

            }
        },
        function (data, callback) {
            if (data && data.status) {

                callback(null, data);
            } else {
                console.log("Error: " + util.inspect(data));
                callback(errorCode.gen('00400'));
            }
        }
    ], function (err, result) {

        if (err) {
            req.io.respond(err);    // ack back to the client. 
        } else {
            var server_resp = result;
            server_resp.client_id = client_id;
            server_resp.member_type = member_type;
            server_resp.paused_by = room.paused_by;
            req.io.room(room_name).broadcast('pause', server_resp);
            req.io.respond(server_resp);
        }
    });
});

//standby
app.io.route('standby', function (req) {
    var room_name = req.session.room_name; // only exist if client has joined the room
    var member_hash = req.session.member_hash; // it is either client or reader.      
    var site_url = req.data.site_url;
    var client_id = '';
    var reader_id = '';
    var diff_time = '';
    var room;

    // send to PHP server , and tell it to pause billing.
    // this action should be initiated by client only. 
    if (!room_name) {
        req.io.respond(errorCode.gen('00100'));
        return;// exist
    }
    var member_type = '';
    var terminated_by = '';

    async.waterfall([
        function (callback) {
            // check if room_name is found.
            roomManager.lookupRoom({room_name: room_name}, callback);
        },
        function (data, callback) {

            if (_.isEmpty(data)) {
                callback(errorCode.gen('00201'));
            } else {
                room = data;
                client_id = data.client_id;
                if (data.client_hash == member_hash) {
                    member_type = 'client';
                    room.paused_by = 'client';
                } else {
                    member_type = 'reader';
                    room.paused_by = 'reader';
                }

                // if pause already, no need to pause again
                if (room.is_pause) {

                } else {
                    userManager.pauseBilling({room_name: room_name, member_hash: member_hash, site_url: site_url}, callback);
                }

            }
        },
        function (data, callback) {
            if (data && data.status) {

                callback(null, data);
            } else {
                console.log("Error: " + util.inspect(data));
                callback(errorCode.gen('00400'));
            }
        }
    ], function (err, result) {

        if (err) {
            req.io.respond(err);    // ack back to the client. 
        } else {
            var server_resp = result;
            server_resp.client_id = client_id;
            server_resp.member_type = member_type;
            server_resp.paused_by = room.paused_by;
            req.io.room(room_name).broadcast('standby', server_resp);
            req.io.respond(server_resp);
        }
    });
});
// resume timer. 
app.io.route('resume', function (req) {
    var room_name = req.session.room_name; // only exist if client has joined the room
    var member_hash = req.session.member_hash; // it is either client or reader.  
    var client_id = '';
    var reader_id = '';
    var pause_length = '';
    var room;

    // send to PHP server , and tell it to start billing.
    // this action should be initiated by client only. 
    if (!room_name) {
        req.io.respond(errorCode.gen('00100'));
        return;// exist
    }
    var member_type = '';
    var terminated_by = '';

    async.waterfall([
        function (callback) {
            // check if room_name is found.
            roomManager.lookupRoom({room_name: room_name}, callback);
        },
        function (data, callback) {

            if (_.isEmpty(data)) {
                callback(errorCode.gen('00201'));
            } else {
                room = data;
                room.paused_by = '';
                client_id = data.client_id;
                if (data.client_hash == member_hash) {
                    member_type = 'client';
                } else {
                    member_type = 'reader';
                }

                // check if it is paused because of disconnection.  
                // Cannot resume if it is disconnected by other party.
                var cannot_resume = false;
                if (data.is_disconnected) {
                    if (member_type == 'client ' && data.reader_disconnected == true) {
                        console.log("Client try to resume disconnection by reader");
                        cannot_resume = true;
                    } else if (member_type == 'reader ' && data.client_disconnected == true) {
                        console.log("Reader try to resume disconnection by client");
                        cannot_resume = true;
                    }
                }

                if (cannot_resume) {
                    callback(errorCode.gen('00502'));
                } else {
                    userManager.resumeBilling({room_name: room_name, member_hash: member_hash}, callback);
                }
            }
        },
        function (data, callback) {
            if (data && data.status) {
                callback(null, data);
            } else {
                callback(errorCode.gen('00400'));
            }
        }
    ], function (err, result) {

        if (err) {
            req.io.respond(err);    // ack back to the client. 
        } else {
            var server_resp = result;
            server_resp.client_id = client_id;
            server_resp.member_type = member_type;
            req.io.room(room_name).broadcast('resume', server_resp);
            req.io.respond(server_resp);
        }
    });
});

// resume timer. MUST BE initiated by client
app.io.route('check_time_balance', function (req) {
    var room_name = req.session.room_name; // only exist if client has joined the room
    var member_hash = req.session.member_hash; // it is either client or reader.  


    // send to PHP server , and tell it to start billing.
    // this action should be initiated by client only. 
    if (!room_name) {
        req.io.respond(errorCode.gen('00100'));
        return;// exist
    }
    var time_balance = 0;
    var max_chat_length = 0;
    var chat_length = 0;

    async.waterfall([
        function (callback) {
            // check if room_name is found.
            roomManager.lookupRoom({room_name: room_name}, callback);
        },
        function (data, callback) {

            if (_.isEmpty(data)) {
                callback(errorCode.gen('00201'));
            } else {
                client_id = data.client_id;
                if (data.client_hash == member_hash || data.reader_hash == member_hash) {
                    // good. initiated by client
                    // IT MUST BE INITIATED BY CLIENT
                    userManager.check_time_balance({room_name: room_name}, callback);
                } else {
                    callback(errorCode.gen('00202'));
                }
            }
        },
        function (data, callback) {
            if (data && data.status) {

                callback(null, data);
            } else {
                callback(errorCode.gen('00400'));
            }
        }
    ], function (err, result) {

        if (err) {
            req.io.respond(err);    // ack back to the client. 
        } else {
            var server_resp = result;
            req.io.room(room_name).broadcast('check_time_balance', server_resp);
            req.io.respond(server_resp);
        }
    });
});

app.io.route('record_chat', function (req) {
    var room_name = req.session.room_name; // only exist if client has joined the room
    var member_hash = req.session.member_hash; // it is must be reader.  
    var site_url = req.data.site_url;
    var client_id = '';
    var reader_id = '';
    var time_balance = 0;
    var max_chat_length = 0;
    var chat_length = 0;

    // send to PHP server , and tell it to start billing.
    // this action should be initiated by client only. 
    if (!room_name) {
        req.io.respond(errorCode.gen('00100'));
        return;// exist
    }
    var member_type = '';
    console.log("record_chat req");
    async.waterfall([
        function (callback) {
            // check if room_name is found.
            roomManager.lookupRoom({room_name: room_name}, callback);
        },
        function (data, callback) {

            if (_.isEmpty(data)) {
                callback(errorCode.gen('00201'));
            } else {
                if (data.reader_hash == member_hash) {
                    // good. initiated by client
                    // IT MUST BE INITIATED BY READER
                    userManager.recordChat({room_name: room_name, member_hash: member_hash, site_url: site_url}, callback);
                } else {
                    callback(errorCode.gen('00202'));
                }
            }
        },
        function (data, callback) {
            if (data && data.status) {

                callback(null, data);
            } else {
                callback(errorCode.gen('00400'));
            }
        }
    ], function (err, result) {

        if (err) {
            req.io.respond(err);    // ack back to the client. 
        } else {
            var server_resp = result;
            server_resp.client_id = client_id;
            req.io.room(room_name).broadcast('record_chat', server_resp);
            req.io.respond(server_resp);
        }
    });
});

app.io.route('update_free_timer', function (req) {
    var room_name = req.session.room_name; // only exist if client has joined the room
    var member_hash = req.session.member_hash; // it is must be reader.  
    var site_url = req.data.site_url;
    var client_id = '';
    var reader_id = '';
    var time_balance = 0;
    var max_chat_length = 0;
    var chat_length = 0;

    // send to PHP server , and tell it to start billing.
    // this action should be initiated by client only. 
    if (!room_name) {
        req.io.respond(errorCode.gen('00100'));
        return;// exist
    }
    var member_type = '';
    console.log("update_free_timer req");
    async.waterfall([
        function (callback) {
            // check if room_name is found.
            roomManager.lookupRoom({room_name: room_name}, callback);
        },
        function (data, callback) {

            if (_.isEmpty(data)) {
                callback(errorCode.gen('00201'));
            } else {
                if (data.reader_hash == member_hash) {
                    // good. initiated by client
                    // IT MUST BE INITIATED BY READER
                    userManager.updateFreeTimer({room_name: room_name, member_hash: member_hash, site_url: site_url}, callback);
                } else {
                    callback(errorCode.gen('00202'));
                }
            }
        },
        function (data, callback) {
            if (data && data.status) {

                callback(null, data);
            } else {
                callback(errorCode.gen('00400'));
            }
        }
    ],
            function (err, result) {

                if (err) {
                    req.io.respond(err);    // ack back to the client. 
                } else {
                    var server_resp = result;
                    server_resp.client_id = client_id;
                    req.io.room(room_name).broadcast('record_free_chat', server_resp);
                    req.io.respond(server_resp);
                }
            });
});

app.io.route('timeout', function (req) {
    // timeout  
});

app.io.route('message', function (req) {
    console.log("on messsage " + util.inspect(req.session));
    // send, and log
    var room_name = req.session.room_name; // only exist if client has joined the room
    var member_hash = req.session.member_hash; // it is either client or reader.  
    var site_url = req.data.site_url;
    var client_id = '';
    var reader_id = '';
    var end_time = '';
    var message = req.data.message;
    var send_data = {};

    // send to PHP server , and tell it to start billing.
    // this action should be initiated by client only. 
    if (!room_name) {
        req.io.respond(errorCode.gen('00100'));
        return;// exist
    }

    async.waterfall([
        function (callback) {
            // check if room_name is found.
            roomManager.lookupRoom({room_name: room_name}, callback);
        },
        function (data, callback) {
            console.log(" Data is " + util.inspect(data));
            if (_.isEmpty(data)) {
                callback(errorCode.gen('00201'));
            } else {
                if (data.is_pause && data.paused_by == 'client') {
                    callback(errorCode.gen('00601'));
                } else {
                    reader_id = data.reader_id;
                    client_id = data.client_id;
                    var sender_id;
                    if (member_hash == data.reader_hash) {
                        send_data = {sender: 'reader', sender_id: reader_id, sender_name: data.reader_username, message: message};
                        sender_id = reader_id;

                        // only reader can send different message type
                        if (req.data.message_type) {
                            send_data.message_type = req.data.message_type;
                        }

                    } else {
                        send_data = {sender: 'client', sender_id: client_id, sender_name: data.client_username, message: message};
                        sender_id = client_id;
                    }

                    // if message type is sytem, then do not log message
                    if (req.data.message_type == 'system') {
                        callback(null);
                    } else {
                        userManager.logMessage({room_name: room_name, message: message, member_id: sender_id, site_url: site_url}, callback);
                    }

                }
            }
        }
    ],
            function (err, result) {
                console.log("Send data is " + util.inspect(send_data));
                if (err) {
                    req.io.respond(err);    // ack back to the client. 
                } else {
                    send_data.status = true;
                    req.io.room(room_name).broadcast('message', send_data);
                    req.io.respond(send_data);
                }
                // no ack. 
            });
});

// purcahse time . MUST BE initiated by client
app.io.route('purchase_time', function (req) {
    // client purchase time,  
    // client will be redirect to a url
    // reader will be notified.   

    var room_name = req.session.room_name; // only exist if client has joined the room
    var member_hash = req.session.member_hash; // it is either client or reader.  
    var client_id = '';
    var reader_id = '';
    var redirect = '';
    var server_resp = {};
    var room;

    // send to PHP server , and tell it to
    if (!room_name) {
        req.io.respond(errorCode.gen('00100'));
        return;// exist
    }
    var member_type = '';
    var terminated_by = '';

    async.waterfall([
        function (callback) {
            // check if room_name is found.
            roomManager.lookupRoom({room_name: room_name}, callback);
        },
        function (data, callback) {

            if (_.isEmpty(data)) {
                callback(errorCode.gen('00201'));
            } else if (!data.is_pause) {
                callback(errorCode.gen('00903'));
            } else {
                room = data;
                client_id = data.client_id;
                if (data.client_hash == member_hash) {
                    room.paused_by = 'client';
                    member_type = 'client';
                } else {
                    room.paused_by = 'reader';
                    member_type = 'reader';
                }
                userManager.pauseBilling({room_name: room_name, member_hash: member_hash}, callback);
            }
        },
        function (data, callback) {
            if (data && data.status) {
                server_resp = data;
            } else {
                server_resp.is_already_paused = true;
            }
            // after pause billing, now  make the purchase time call
            //userManager.purchaseMoreTime({room_name: room_name}, callback);
            callback(null, {status: true});
        },
        function (data, callback) {
            if (data && data.status) {
                server_resp.status = true;
                //server_resp.redirect = data.redirect;
                callback(null);
            } else {
                callback(errorCode.gen('00400'));
            }
        }
    ],
            function (err, result) {

                if (err) {
                    req.io.respond(err);    // ack back to the client. 
                } else {
                    server_resp.client_id = client_id;
                    server_resp.member_type = member_type;
                    server_resp.paused_by = room.paused_by;
                    req.io.room(room_name).broadcast('client_purchase_time', server_resp);
                    req.io.respond(server_resp); // get back the redirect url  that will be handled by client js. 
                }
            });
});

app.io.route('get_stored_time', function (req) {
    var room_name = req.session.room_name; // only exist if client has joined the room
    var member_hash = req.session.member_hash; // it is either client or reader.  
    var is_bypass_message = req.data.is_bypass_message || false;
    var site_url = req.data.site_url;
    var client_id = '';
    var reader_id = '';
    var redirect = '';

    // send to PHP server , and tell it to
    if (!room_name) {
        req.io.respond(errorCode.gen('00100'));
        return;// exist
    }
    var member_type = '';
    var terminated_by = '';
    var server_resp = {};

    async.waterfall([
        function (callback) {
            // check if room_name is found.
            roomManager.lookupRoom({room_name: room_name}, callback);
        },
        function (data, callback) {

            if (_.isEmpty(data)) {
                callback(errorCode.gen('00201'));
            } else if (!data.is_pause) {
                callback(errorCode.gen('00903'));
            } else {
                client_id = data.client_id;
                if (data.client_hash == member_hash) {
                    member_type = 'client'; // requestr sent by client
                    userManager.getStoredTime({room_name: room_name, site_url: site_url}, callback);
                } else {
                    callback(errorCode.gen('00904'));
                }
            }
        },
        function (data, callback) {
            console.log("Data is: " + JSON.stringify(data));
            if (data && data.status) {
                server_resp.status = true;
                server_resp.stored_time = data.stored_time;
                callback(null);
            } else {
                callback(errorCode.gen('00400'));
            }
        }
    ],
            function (err, result) {
                console.log("Server resp: " + JSON.stringify(server_resp));
                if (err) {
                    req.io.respond(err);    // ack back to the client. 
                } else {
                    server_resp.client_id = client_id;
                    server_resp.member_type = member_type;
                    server_resp.is_bypass_message = is_bypass_message;
                    req.io.room(room_name).broadcast('get_stored_time', server_resp);
                    req.io.respond(server_resp); // get back the redirect url  that will be handled by client js. 
                }
            });
});


// add stored time . MUST BE initiated by client
app.io.route('add_stored_time', function (req) {
    var room_name = req.session.room_name; // only exist if client has joined the room
    var member_hash = req.session.member_hash; // it is either client or reader.  
    var added_time = req.data.added_time || 0; // in second
    var client_id = '';
    var reader_id = '';
    var redirect = '';
    var site_url = req.data.site_url;

    // send to PHP server , and tell it to
    if (!room_name) {
        req.io.respond(errorCode.gen('00100'));
        return;// exist
    }
    added_time = parseInt(added_time, 10);
    if (!_.isNumber(added_time) || added_time <= 0) {
        req.io.respond(errorCode.gen('00902'));
        return;
    }

    var member_type = '';
    var terminated_by = '';
    var server_resp = {};

    async.waterfall([
        function (callback) {
            // check if room_name is found.
            roomManager.lookupRoom({room_name: room_name}, callback);
        },
        function (data, callback) {
            if (_.isEmpty(data)) {
                callback(errorCode.gen('00201'));
            } else if (!data.is_pause) {
                callback(errorCode.gen('00903'));
            } else {
                client_id = data.client_id;
                if (data.client_hash == member_hash) {
                    member_type = 'client'; // requestr sent by client
                    userManager.addStoredTime({room_name: room_name, added_time: added_time, site_url: site_url}, callback);
                } else {
                    callback(errorCode.gen('00904'));
                }
            }
        },
        function (data, callback) {
            console.log("Data is: " + JSON.stringify(data));
            if (data && data.status) {
                server_resp = data;
                callback(null);
            } else {
                callback(errorCode.gen('00400'));
            }
        }
    ],
            function (err, result) {

                if (err) {
                    req.io.respond(err);    // ack back to the client. 
                } else {
                    server_resp.client_id = client_id;
                    server_resp.member_type = member_type;
                    req.io.room(room_name).broadcast('client_add_stored_time', server_resp);
                    req.io.respond(server_resp); // get back the redirect url  that will be handled by client js. 
                }
            });
});

app.io.route('add_free_email', function (req) {
    // send, and log
    var room_name = req.session.room_name; // only exist if client has joined the room
    var member_hash = req.session.member_hash; // it is either client or reader.  
    var client_id = '';
    var reader_id = '';

    // send to PHP server , and tell it to start billing.
    // this action should be initiated by client only. 
    if (!room_name) {
        req.io.respond(errorCode.gen('00100'));
        return;// exist
    }

    async.waterfall([
        function (callback) {
            // check if room_name is found.
            roomManager.lookupRoom({room_name: room_name}, callback);
        },
        function (data, callback) {
            if (_.isEmpty(data)) {
                callback(errorCode.gen('00201'));
            } else {
                reader_id = data.reader_id;
                client_id = data.client_id;
                var send_data = {};
                if (member_hash == data.reader_hash) {
                    send_data = {status: true, sender: 'reader', sender_id: reader_id, sender_name: data.reader_username};
                } else {
                    send_data = {status: true, sender: 'client', sender_id: client_id, sender_name: data.client_username};
                }
                req.io.room(room_name).broadcast('reader_add_free_email', send_data);
                callback(null);
            }
        }
    ], function (err, result) {

        if (err) {
            req.io.respond(err);    // ack back to the client. 
        }
        // no ack. 
    });
});

// add free time . MUST BE initiated by client
app.io.route('add_free_time', function (req) {
    var room_name = req.session.room_name; // only exist if client has joined the room
    var member_hash = req.session.member_hash; // it is either client or reader.  
    var free_time = req.data.free_time || 0; // in second
    var site_url = req.data.site_url;
    var client_id = '';
    var reader_id = '';
    var redirect = '';
    console.log('Add Free Time ======>>>>', free_time);
    // send to PHP server , and tell it to
    if (!room_name) {
        req.io.respond(errorCode.gen('00100'));
        return;// exist
    }
    
    // free_time = parseInt(free_time, 10);
    if (free_time <= 0 || free_time > 1000) {
        console.log("Fail: " + free_time);
        req.io.respond(errorCode.gen('00902'));
        return;
    }

    var member_type = '';
    var terminated_by = '';
    var server_resp = {};

    async.waterfall([
        function (callback) {
            // check if room_name is found.
            roomManager.lookupRoom({room_name: room_name}, callback);
        },
        function (data, callback) {
            if (_.isEmpty(data)) {
                callback(errorCode.gen('00201'));
            } else {
                client_id = data.client_id;
                reader_id = data.reader_id;
                if (data.reader_hash == member_hash) {
                    member_type = 'reader'; // requestr sent by client
                    userManager.addFreeTime({room_name: room_name, free_time: free_time, site_url: site_url}, callback);
                } else {
                    // cannot be called as client
                    callback(errorCode.gen('00906'));
                }
            }
        },
        function (data, callback) {
            console.log("Data is: " + JSON.stringify(data));
            if (data && data.status) {
                server_resp = data;
                callback(null);
            } else {
                callback(errorCode.gen('00400'));
            }
        }
    ], function (err, result) {

        if (err) {
            req.io.respond(err);    // ack back to the client. 
        } else {
            server_resp.reader_id = reader_id;
            server_resp.member_type = member_type;
            req.io.room(room_name).broadcast('reader_add_free_time', server_resp);
            req.io.respond(server_resp); // get back the redirect url  that will be handled by client js. 
        }
    });
});

/*9-12*/
app.io.route('add_gift_time', function (req) {
    var room_name = req.session.room_name; // only exist if client has joined the room
    var member_hash = req.session.member_hash; // it is either client or reader.  
    var free_time = req.data.free_time || 0; // in second
    var site_url = req.data.site_url;
    var client_id = '';
    var reader_id = '';
    var redirect = '';

    // send to PHP server , and tell it to
    if (!room_name) {
        req.io.respond(errorCode.gen('00100'));
        return;// exist
    }
    
    free_time = parseInt(free_time, 10);
    if (!_.isNumber(free_time) || free_time <= 0 || free_time > 1000) {
        console.log("Fail: " + free_time);
        req.io.respond(errorCode.gen('00902'));
        return;
    }

    var member_type = '';
    var terminated_by = '';
    var server_resp = {};

    async.waterfall([
        function (callback) {
            // check if room_name is found.
            roomManager.lookupRoom({room_name: room_name}, callback);
        },
        function (data, callback) {
            if (_.isEmpty(data)) {
                callback(errorCode.gen('00201'));
            } else {
                client_id = data.client_id;
                reader_id = data.reader_id;
                if (data.reader_hash == member_hash) {
                    member_type = 'reader'; // requestr sent by client
                    userManager.addFreeTime({room_name: room_name, free_time: free_time, site_url: site_url}, callback);
                } else {
                    // cannot be called as client
                    callback(errorCode.gen('00906'));
                }
            }
        },
        function (data, callback) {
            console.log("Data is: " + JSON.stringify(data));
            if (data && data.status) {
                server_resp = data;
                callback(null);
            } else {
                callback(errorCode.gen('00400'));
            }
        }
    ], function (err, result) {

        if (err) {
            req.io.respond(err);    // ack back to the client. 
        } else {
            server_resp.reader_id = reader_id;
            server_resp.member_type = member_type;
            req.io.room(room_name).broadcast('reader_add_gift_time', server_resp);
            req.io.respond(server_resp); // get back the redirect url  that will be handled by client js. 
        }
    });
});
//8-9
app.io.route('refund', function (req) {
    
    var room_name = req.session.room_name; // only exist if client has joined the room
    var member_hash = req.session.member_hash; // it is either client or reader.  
    var client_id = '';
    var reader_id = '';
    var site_url = req.data.site_url;
    var last_updated_time;
    // send to PHP server , and tell it to
    if (!room_name) {
        
        req.io.respond(errorCode.gen('00100'));
        return;// exist
    }
    var member_type = '';
    var terminated_by = '';
    var server_resp = {};

    async.waterfall([
        function (callback) {
            // check if room_name is found.

            roomManager.lookupRoom({room_name: room_name}, callback);
        },
        function (data, callback) {

            if (_.isEmpty(data)) {
                
                callback(errorCode.gen('00201'));
            } else {
                
                client_id = data.client_id;
                reader_id = data.reader_id;
                if (data.reader_hash == member_hash) {
                    // good. initiated by client
                    // IT MUST BE INITIATED BY READER
                    console.log("*********userManager.refundChat***********");
                    userManager.refundChat({room_name: room_name, site_url: site_url}, callback);
                } else {

                    callback(errorCode.gen('00202'));
                }
            }
        },
        // function (data, callback) {
        //     // if refund from server is good, then reset timer
        //     // roomManager.resetRoomTimer({room_name: room_name}, callback);
        //     // roomManager.removeRoom({room_name: room_name}, callback);
        //     userManager.leaveChat({room_name: room_name, member_hash: member_hash, terminated_by: 'reader', site_url: site_url}, callback);
        // },
        function (data, callback) {
            if (data && data.status) {
                server_resp = data;
                callback(null);
            } else {
                callback(errorCode.gen('00400'));
            }
        }
    ], function (err, result) {

        if (err) {

            req.io.respond(err);    // ack back to the client. 
            console.log('********************error *****************');
        } else {
            // should End the session
            console.log('*********************result is okay***********');
            server_resp.client_id = client_id;
            req.io.room(room_name).broadcast('refund', server_resp);  // send from server to clients.
            req.io.respond(server_resp); // get back the redirect url  that will be handled by client js. 
        }
    });

});

// REFUND, must be initiated by READER
// app.io.route('refund', function (req) {
//     var room_name = req.session.room_name; // only exist if client has joined the room
//     var member_hash = req.session.member_hash; // it is either client or reader.  
//     var client_id = '';
//     var reader_id = '';
//     var last_updated_time;
//     // send to PHP server , and tell it to
//     if (!room_name) {
//         req.io.respond(errorCode.gen('00100'));
//         return;// exist
//     }
//     var member_type = '';
//     var terminated_by = '';
//     var server_resp = {};

//     async.waterfall([
//         function (callback) {
//             // check if room_name is found.
//             roomManager.lookupRoom({room_name: room_name}, callback);
//         },
//         function (data, callback) {

//             if (_.isEmpty(data)) {
//                 callback(errorCode.gen('00201'));
//             } else {
//                 client_id = data.client_id;
//                 reader_id = data.reader_id;
//                 if (data.reader_hash == member_hash) {
//                     // good. initiated by client
//                     // IT MUST BE INITIATED BY READER
//                     userManager.refundChat({room_name: room_name}, callback);
//                 } else {
//                     callback(errorCode.gen('00202'));
//                 }
//             }
//         },
//         function (data, callback) {
//             // if refund from server is good, then reset timer
//             //roomManager.resetRoomTimer({room_name: room_name}, callback);
//             //roomManager.removeRoom({room_name: room_name}, callback);
//             userManager.leaveChat({room_name: room_name, member_hash: member_hash, terminated_by: 'reader'}, callback);
//         },
//         function (data, callback) {
//             if (data && data.status) {
//                 server_resp = data;
//                 callback(null);
//             } else {
//                 callback(errorCode.gen('00400'));
//             }
//         }
//     ], function (err, result) {

//         if (err) {
//             req.io.respond(err);    // ack back to the client. 
//         } else {
//             // should End the session
//             server_resp.client_id = client_id;
//             req.io.room(room_name).broadcast('refund', server_resp);  // send from server to clients.
//             req.io.respond(server_resp); // get back the redirect url  that will be handled by client js. 
//         }
//     });
// });

app.io.route('contact_admin', function (req) {
    var room_name = req.session.room_name; // only exist if client has joined the room
    var member_hash = req.session.member_hash; // it is either client or reader.  
    var site_url = req.data.site_url;
    var client_id;
    // send to PHP server , and tell it to
    if (!room_name) {
        req.io.respond(errorCode.gen('00100'));
        return;// exist
    }
    var member_type = '';
    var server_resp = {};
    var room;

    // pause too
    async.waterfall([
        function (callback) {
            // check if room_name is found.
            roomManager.lookupRoom({room_name: room_name}, callback);
        },
        function (data, callback) {
            if (_.isEmpty(data)) {
                callback(errorCode.gen('00201'));
            } else {
                room = data;
                client_id = data.client_id;
                if (data.client_hash == member_hash) {
                    // good. initiated by client
                    // IT MUST BE INITIATED BY CLIENT
                    member_type = 'client';
                    room.paused_by = 'client';
                    userManager.pauseBilling({room_name: room_name, member_hash: member_hash, site_url: site_url}, callback);
                } else {
                    callback(errorCode.gen('00202'));
                }
            }
        },
        function (data, callback) {
            if (data && data.status) {
                server_resp = data;
            } else {
                server_resp.is_already_paused = true;
            }
            // after pause billing, now  make the purchase time call
            //userManager.purchaseMoreTime({room_name: room_name}, callback);
            callback(null, {status: true});
        },
        function (data, callback) {
            if (data && data.status) {
                server_resp.status = true;
                //server_resp.redirect = data.redirect;
                callback(null);
            } else {
                callback(errorCode.gen('00400'));
            }
        }
    ], function (err, result) {

        if (err) {
            req.io.respond(err);    // ack back to the client. 
        } else {
            server_resp.client_id = client_id;
            server_resp.member_type = member_type;
            server_resp.paused_by = room.paused_by;
            req.io.room(room_name).broadcast('contact_admin', server_resp);
            req.io.respond(server_resp); // get back the redirect url  that will be handled by client js. 
        }
    });
});

app.io.route('lost_time', function (req) {
    // start  
    var room_name = req.session.room_name; // only exist if client has joined the room
    var member_hash = req.session.member_hash; // it is either client or reader.  
    var lost_time = req.data.lost_time || 0; // in second
    var site_url = req.data.site_url;
    var client_id = '';
    var reader_id = '';
    var last_updated_time;
    // send to PHP server , and tell it to
    if (!room_name) {
        req.io.respond(errorCode.gen('00100'));
        return;// exist
    }

    lost_time = parseInt(lost_time, 10);
    if (!_.isNumber(lost_time) || lost_time <= 0) {
        req.io.respond(errorCode.gen('00902'));
        return;
    }

    var member_type = '';
    var terminated_by = '';
    var server_resp = {};

    async.waterfall([
        function (callback) {
            // check if room_name is found.
            roomManager.lookupRoom({room_name: room_name}, callback);
        },
        function (data, callback) {

            if (_.isEmpty(data)) {
                console.log("empty data? ");
                callback(errorCode.gen('00201'));
            } else {
                client_id = data.client_id;
                reader_id = data.reader_id;
                is_pause = data.is_pause;
                if (is_pause === false) {
                    callback(errorCode.gen('00903'));
                } else {
                    if (data.reader_hash == member_hash) {
                        // good. initiated by client
                        // IT MUST BE INITIATED BY CLIENT
                        terminated_by = 'reader';
                        userManager.recordChat({room_name: room_name, member_hash: member_hash, lost_time: lost_time, site_url: site_url}, callback);

                    } else {
                        callback(errorCode.gen('00202'));
                    }
                }
            }
        },
        function (data, callback) {
            if (data && data.status) {
                server_resp = data;
                callback(null);
            } else {
                callback(errorCode.gen('00400'));
            }
        }
    ], function (err, result) {

        if (err) {
            req.io.respond(err);    // ack back to the client. 
        } else {
            server_resp.client_id = client_id;
            req.io.room(room_name).broadcast('lost_time', server_resp);  // send from server to clients.
            req.io.respond(server_resp); // get back the redirect url  that will be handled by client js. 
        }
    });
});

// ban the server for a session.  initiated by READER only. 
app.io.route('pban', function (req) {
    var room_name = req.session.room_name; // only exist if client has joined the room
    var member_hash = req.session.member_hash; // it is either client or reader.  
    var site_url = req.data.site_url;
    var client_id = '';
    var reader_id = '';
    var last_updated_time;
    // send to PHP server , and tell it to
    if (!room_name) {
        req.io.respond(errorCode.gen('00100'));
        return;// exist
    }
    var member_type = '';
    var terminated_by = '';
    var server_resp = {};

    async.waterfall([
        function (callback) {
            // check if room_name is found.
            roomManager.lookupRoom({room_name: room_name}, callback);
        },
        function (data, callback) {

            if (_.isEmpty(data)) {
                callback(errorCode.gen('00201'));
            } else {
                client_id = data.client_id;
                reader_id = data.reader_id;
                if (data.reader_hash == member_hash) {
                    // good. initiated by client
                    // IT MUST BE INITIATED BY CLIENT
                    terminated_by = 'reader';
                    userManager.leaveChat({room_name: room_name, member_hash: member_hash, terminated_by: terminated_by, site_url: site_url}, callback);

                } else {
                    callback(errorCode.gen('00202'));
                }
            }
        },
        function (data, callback) {
            if (data && data.status) {
                server_resp = data;
            } else {
                server_resp.err = "unable to leave chat";
            }
            userManager.banUser({reader_id: reader_id, client_id: client_id, type: "personal", site_url: site_url}, callback);

        },
        function (data, callback) {
            if (data && data.status) {
                server_resp.status = true;
                callback(null);
            } else {
                callback(errorCode.gen('00400'));
            }
        }
    ], function (err, result) {
        delete req.session.room_name;
        delete req.session.reader_id;
        delete req.session.reader_hash;
        delete req.session.client_id;
        delete req.session.client_hash;
        delete req.session.member_hash;

        if (err) {
            req.io.respond(err);    // ack back to the client. 
        } else {
            server_resp.client_id = client_id;
            req.io.room(room_name).broadcast('pban', server_resp);  // send from server to clients.
            req.io.respond(server_resp); // get back the redirect url  that will be handled by client js. 
        }
    });
});

// ban the server forever.  initiated by READER only. 
app.io.route('fban', function (req) {

    var room_name = req.session.room_name; // only exist if client has joined the room
    var member_hash = req.session.member_hash; // it is either client or reader.  
    var site_url = req.data.site_url;
    var client_id = '';
    var reader_id = '';
    var last_updated_time;
    // send to PHP server , and tell it to
    if (!room_name) {
        req.io.respond(errorCode.gen('00100'));
        return;// exist
    }
    var member_type = '';
    var terminated_by = '';

    async.waterfall([
        function (callback) {
            // check if room_name is found.
            roomManager.lookupRoom({room_name: room_name}, callback);
        },
        function (data, callback) {

            if (_.isEmpty(data)) {
                callback(errorCode.gen('00201'));
            } else {
                client_id = data.client_id;
                reader_id = data.reader_id;
                if (data.reader_hash == member_hash) {
                    // good. initiated by client
                    // IT MUST BE INITIATED BY CLIENT
                    terminated_by = 'reader';
                    userManager.leaveChat({room_name: room_name, member_hash: member_hash, terminated_by: terminated_by, site_url: site_url}, callback);

                } else {
                    callback(errorCode.gen('00202'));
                }
            }
        },
        function (data, callback) {
            if (data && data.status) {
                server_resp = data;
            } else {
                server_resp.err = "unable to leave chat";
            }
            userManager.banUser({reader_id: reader_id, client_id: client_id, type: "full", site_url: site_url}, callback);

        },
        function (data, callback) {
            if (data && data.status) {
                server_resp.status = true;
                callback(null);
            } else {
                callback(errorCode.gen('00400'));
            }
        }
    ], function (err, result) {
        delete req.session.room_name;
        delete req.session.reader_id;
        delete req.session.reader_hash;
        delete req.session.client_id;
        delete req.session.client_hash;
        delete req.session.member_hash;

        if (err) {
            req.io.respond(err);    // ack back to the client. 
        } else {
            server_resp.client_id = client_id;
            req.io.room(room_name).broadcast('fban', server_resp);  // send from server to clients.
            req.io.respond(server_resp); // get back the redirect url  that will be handled by client js. 
        }
    });
});

app.io.route('typing', function (req) {
    console.log("Receive typing message ");
    // send, and log
    var room_name = req.session.room_name; // only exist if client has joined the room
    var member_hash = req.session.member_hash; // it is either client or reader.  
    var client_id = '';
    var reader_id = '';

    // send to PHP server , and tell it to start billing.
    // this action should be initiated by client only. 
    if (!room_name) {
        req.io.respond(errorCode.gen('00100'));
        return;// exist
    }

    async.waterfall([
        function (callback) {
            // check if room_name is found.
            roomManager.lookupRoom({room_name: room_name}, callback);
        },
        function (data, callback) {
            if (_.isEmpty(data)) {
                callback(errorCode.gen('00201'));
            } else {
                reader_id = data.reader_id;
                client_id = data.client_id;
                var send_data = {};
                if (member_hash == data.reader_hash) {
                    send_data = {status: true, sender: 'reader', sender_id: reader_id, sender_name: data.reader_username};
                } else {
                    send_data = {status: true, sender: 'client', sender_id: client_id, sender_name: data.client_username};
                }
                req.io.room(room_name).broadcast('typing', send_data);
                callback(null);
            }
        }
    ], function (err, result) {

        if (err) {
            req.io.respond(err);    // ack back to the client. 
        }
        // no ack. 
    });
});

app.io.route('pauseFreeTime', function (req) {
    console.log("Receive typing message ");
    // send, and log
    var room_name = req.session.room_name; // only exist if client has joined the room
    var member_hash = req.session.member_hash; // it is either client or reader.  
    var client_id = '';
    var reader_id = '';

    // send to PHP server , and tell it to start billing.
    // this action should be initiated by client only. 
    if (!room_name) {
        req.io.respond(errorCode.gen('00100'));
        return;// exist
    }

    async.waterfall([
        function (callback) {
            // check if room_name is found.
            roomManager.lookupRoom({room_name: room_name}, callback);
        },
        function (data, callback) {
            if (_.isEmpty(data)) {
                callback(errorCode.gen('00201'));
            } else {
                reader_id = data.reader_id;
                client_id = data.client_id;
                var send_data = {};
                if (member_hash == data.reader_hash) {
                    send_data = {status: true, sender: 'reader', sender_id: reader_id, sender_name: data.reader_username};
                } else {
                    send_data = {status: true, sender: 'client', sender_id: client_id, sender_name: data.client_username};
                }
                req.io.room(room_name).broadcast('pauseFreeTime', send_data);
                callback(null);
            }
        }
    ], function (err, result) {

        if (err) {
            req.io.respond(err);    // ack back to the client. 
        }
        // no ack. 
    });
});

app.io.route('continueFreeTime', function (req) {
    console.log("Receive typing message ");
    // send, and log
    var room_name = req.session.room_name; // only exist if client has joined the room
    var member_hash = req.session.member_hash; // it is either client or reader.
    var client_id = '';
    var reader_id = '';

    // send to PHP server , and tell it to start billing.
    // this action should be initiated by client only.
    if (!room_name) {
        req.io.respond(errorCode.gen('00100'));
        return;// exist
    }

    async.waterfall([
        function (callback) {
            // check if room_name is found.
            roomManager.lookupRoom({room_name: room_name}, callback);
        },
        function (data, callback) {
            if (_.isEmpty(data)) {
                callback(errorCode.gen('00201'));
            } else {
                reader_id = data.reader_id;
                client_id = data.client_id;
                var send_data = {};
                if (member_hash == data.reader_hash) {
                    send_data = {status: true, sender: 'reader', sender_id: reader_id, sender_name: data.reader_username};
                } else {
                    send_data = {status: true, sender: 'client', sender_id: client_id, sender_name: data.client_username};
                }
                req.io.room(room_name).broadcast('continueFreeTime', send_data);
                callback(null);
            }
        }
    ], function (err, result) {

        if (err) {
            req.io.respond(err);    // ack back to the client.
        }
        // no ack.
    });
});


app.get('/auth.js', function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.set('Content-Type', 'text/javascript');
    res.send('var auth=1;');
});

app.get('/send/:member_id', function (req, res) {
    var member_id = req.params.member_id;
    console.log("Send: member_id: " + member_id);

    var reader_socket_id = roomManager.getMemberSocket(member_id);
    // get socket.
    //var reader_socket = util.findSocket(req, reader_socket_id);
    console.log("reader_socket_id: " + reader_socket_id);
    console.log("socket_map: " + util.inspect(socket_map));
    var reader_socket = socket_map[reader_socket_id];
    if (reader_socket) {
        console.log("reader socket is found, socket_id: " + reader_socket_id);
        // emit to reader's socket if user exist. 
        reader_socket.emit('join_new_chat_room', {reader_id: member_id, client_id: 14});
        res_message = "client create a new room and notify connected reader";
    }
    res.send("done: " + res_message);
});

app.get('/test/:seconds', function (req, res) {
    var seconds = req.params.seconds || 0;

    setTimeout(function () {
        res.json({status: 'ok', time: seconds});
    }, seconds * 1000);
});

if (config.socket_url == "") {
    console.log("listening to port " + config.socket_port);
    app.listen(config.socket_port);
} else {
    console.log("listening to url & port " + config.socket_url + ":" + config.socket_port);
    app.listen(config.socket_port, config.socket_url);
}
