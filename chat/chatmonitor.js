
var member = reader;
var win = null;
var soundIV;

if(member.member_id)
{
//    var socket = new RampNode('http://psycon.rampnode.com/psycon', member);

	var port   = 3701;
	socket = io.connect('http://66.178.176.109:'+port);
	
    socket.on('message', function(object){

        if(object.type == 'new_chat' && member.member_id == object.message.reader_id){

            $('body').append("<div id='modal'><div class='cont'><b>\""+object.message.username+"\" wants to chat with you.<br />Do you want to accept?</b><div style='margin:25px 0 0;' align='center'><a href='/' id='startChat' class='blue-button'><span>Start Chatting</span></a> &nbsp; <a id='closeChat' href='/' class='blue-button'><span>Not At This Time</span></a></div></div></div><div id='modal_bg'></div>");

            // Center modal
            var leftPos = ($(window).width()/2)-($('#modal').width()/2);
            var topPos = ($(window).height()/2)-($('#modal').height()/2);

            $('#modal').css('left',leftPos);
            $('#modal').css('top',topPos);

            // Play sound every 4 seconds
            playsound();
            soundIV = setInterval(playsound, 2000);

            // If chat accepted

            $('#startChat').click(function(e){

                e.preventDefault();

                stopSound();
                remove_modal();
                open_window(object.message.chat_id);

            });

            // If Chat Denied
            $(document).on('click', '#closeChat',function(e){

                // Send a busy notification to the client
                e.preventDefault();

                stopSound();

                var sockObject ={
                    'type' : 'reader_decline',
                    'reader_id' : member.member_id,
                    'username'  : member.username,
                    'chat_id' : object.message.chat_id
                };

//                var socket2 = new RampNode('http://psycon.rampnode.com/psycon/' + object.message.chat_id, member);
				socket = io.connect('http://66.178.176.109:'+port);
                socket.send(sockObject);
                remove_modal();

            });
        }

        //Redirect request
        if(object.type == 'redirect' && member.member_id == object.message.member_id)
        {
            window.location.replace(object.message.redirect_url);
        }

        //Timeout
        if(object.type == 'timeout' && member.member_id ==object.message.member_id){
            window.location.replace("/chat/main/chat_options/reader/" + object.message.chat_id);
        }

        if(object.type == 'fullban' && member.member_id == object.message.member_id)
        {
            window.location.replace("/main/banned");
        }

        if(object.type == 'contact' && member.member_id == object.message.member_id)
        {
            window.location.replace("/contact");
        }



    });

    function open_window(chat_id){
        var w = 900; var h = 650;
        LeftPosition = (screen.width)?(screen.width-w)/2:100;TopPosition=(screen.height)?(screen.height-h)/2:100;
        settings='width='+w+',height='+h+',top='+TopPosition+',left='+LeftPosition+',scrollbars=yes,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=yes';
        win=window.open('/chat/chatInterface/index/' + chat_id,'PsyConChatWindow',settings);
    }

    function remove_modal(){
        $('#modal').remove();
        $('#modal_bg').remove();
    }

    function playsound(){
        $('.playSound').remove();
        $.playSound("/chat/new.mp3");
    }

    function stopSound(){
        $('.playSound').remove();
        clearInterval(soundIV);
    }

}