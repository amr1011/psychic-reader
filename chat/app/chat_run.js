Chat.run = {
    
    test: function() {
        // test if any missing module.d
    },
    
    room: function() {
        console.log("Done");
        // Chat init.
        Chat.init(chat_init_data);
        // room.  join & send notification to reader in lobby
        // now emit "join"
        console.log("Send join new chat room");
        Chat.manager.room.send.join_new_chat_room();
        Chat.views.popup.resize_chat();
        Chat.views.message.ui.adjust_input_message_textarea();
    },
    
    lobby: function() {
        console.log("Done");
        // Chat init.
        Chat.init(chat_init_data);
        if(Chat.models.member.member_type == "reader") {
        	Chat.controller.lobby.actions.register();
        }
    }
};
