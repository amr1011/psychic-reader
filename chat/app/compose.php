<?php

$lobby_only_file_names = array("preload_chat_lib", "chat", "models/member", "models/socket", "views/message", "views/error_log", "views/popup", "controller/lobby", "chat_run");

$room_file_names = array("preload_chat_lib", "chat", "models/member", "models/socket", "manager/room", "views/message", "views/error_log", "views/popup", "controller/room", "chat_run");

// read file. 
$lobby_content = '';
foreach ($lobby_only_file_names as $file_name) {
    $lobby_content .= file_get_contents($file_name . ".js");
}

// write to file
file_put_contents("chat_all_lobby.js", $lobby_content);

$room_content = '';
foreach ($room_file_names as $file_name) {
    $room_content .= file_get_contents($file_name . ".js");
}

// write to file
file_put_contents("chat_all_room.js", $room_content);

?>