var Chat = {
    
    // Chat main object. 
    // socket.io must be included before this class
    // must include chat_ui and chat_message after this class.
    title:'',
    is_mobile:false,
    
    // 
    controller: {},
    manager: {},
    models:{},
    views:{},
    browser:{
        name:'',
        version:''
    },
    
    set_browser: function() {
        try {
            Chat.browser.name = get_browser();
            Chat.browser.version = get_browser_version();
        } catch (e) {
            
        }
        
        if (Chat.browser.name == 'MSIE') {
            var html = '<embed src="/media/sounds/door_bell.mp3" autostart="false" width="0" height="0" id="beep" enablejavascript="true">';
            $('#ieDiv').append(html);
        }
    },
    
    init: function(data) {
        console.log(data);
        if (data.is_mobile) {
            Chat.is_mobile = true;
        }
        
        if (data.chat_title) {
            Chat.title = data.chat_title;
            document.title = Chat.title;
        }
        if (Chat.models.socket) {
            Chat.models.socket.init(data);
        }  
        if (Chat.models.member) {
            Chat.models.member.init(data);
        }
        if (Chat.manager.room) {
            Chat.manager.room.init(data);
        }
        if (Chat.controller.lobby) {
            Chat.controller.lobby.init();
            if (data.reader_id) {
            	Chat.controller.lobby.reader_id = data.reader_id;
            }
        }
        console.log("Step 1");
        if (Chat.controller.room) {
            console.log("Step 2");
            Chat.controller.room.init();
        }
        
        Chat.set_browser();
        
    }
    
};
