
/*thom ho 7.31*/
var duration_no_action = 0;
var duration_session = 0;
var interval;

var notify = 0;
var detector;
var end_session = 0;
var startTimer;
var myinterval;
/*9-27*/
var standbyinterval;
var stand_by_action_time = 0;

var block_words_array = [];
var info_data = [];
/*9-16*/
var start_giveback;
var message_used_2min = 0;
var message_used_5min = 0;
var used_freetime = 0;
var used_addfreetime = 0;
var confirmed_to_use_freetime = 0;

if (typeof console === "undefined" || typeof console.log === "undefined") {
    alert('it is undefined');
    console = {};
    console.data = [];
    console.log = function(enter) {
        console.data.push(enter);
    };
}


function get_browser(){
    var ua=navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || []; 
    if(/trident/i.test(M[1])){
        tem=/\brv[ :]+(\d+)/g.exec(ua) || []; 
        return 'IE '+(tem[1]||'');
    }   
    if(M[1]==='Chrome'){
        tem=ua.match(/\bOPR\/(\d+)/);
        if(tem!=null)   {return 'Opera '+tem[1];}
    }   
    M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem=ua.match(/version\/(\d+)/i))!=null) {M.splice(1,1,tem[1]);}
    return M[0];
}
            
function get_browser_version(){
    var ua=navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];                                                                                                                         
    if(/trident/i.test(M[1])){
        tem=/\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1]||'');
    }
    if(M[1]==='Chrome'){
        tem=ua.match(/\bOPR\/(\d+)/);
        if(tem!=null)   {return 'Opera '+tem[1];}
    }   
    M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem=ua.match(/version\/(\d+)/i))!=null) {M.splice(1,1,tem[1]);}
    return M[1];
 }
 
// ----  END OF PRELOAD --//
var new_win; //david_v3
var Chat = {
    
    // Chat main object. 
    // socket.io must be included before this class
    // must include chat_ui and chat_message after this class.
    title:'',
    is_mobile:false,
    
    // 
    controller: {},
    manager: {},
    models:{},
    views:{},
    browser:{
        name:'',
        version:''
    },
    
    set_browser: function() {
        try {
            Chat.browser.name = get_browser();
            Chat.browser.version = get_browser_version();
        } catch (e) {
            
        }
        
        if (Chat.browser.name == 'MSIE') {
            var html = '<embed src="/media/sounds/door_bell.mp3" autostart="false" width="0" height="0" id="beep" enablejavascript="true">';
            html += '<embed src="/media/sounds/message_alert.mp3" autostart="false" width="0" height="0" id="beep2" enablejavascript="true">';
            html += '<embed src="/media/sounds/clear_minimal.mp3" autostart="false" width="0" height="0" id="beep3" enablejavascript="true">';
            html += '<embed src="/media/sounds/chord.mp3" autostart="false" width="0" height="0" id="beep3" enablejavascript="true">';
            html += '<embed src="/media/sounds/chat_alert.mp3" autostart="false" width="0" height="0" id="beep3" enablejavascript="true">';
            html += '<embed src="/media/sounds/end_chat.mp3" autostart="false" width="0" height="0" id="beep7" enablejavascript="true">';
            $('#ieDiv').append(html);
        }
    },
    
    init: function(data) {        
        if (data.is_mobile) {
            Chat.is_mobile = true;
        }
        
        if (data.chat_title) {
            Chat.title = data.chat_title;
            document.title = Chat.title;
        }
        if (Chat.models.socket) {
            Chat.models.socket.init(data);
        }  
        if (Chat.models.member) {
            Chat.models.member.init(data);
        }
        if (Chat.manager.room) {
            Chat.manager.room.init(data);
        }
        if (Chat.controller.lobby) {
            Chat.controller.lobby.init();
            if (data.reader_id) {
                Chat.controller.lobby.reader_id = data.reader_id;
            }
        }
        console.log("Step 1");
        if (Chat.controller.room) {
            console.log("Step 2");
            Chat.controller.room.init();
        }
        
        Chat.set_browser();
        
    }
    
};
Chat.models.member = {
    
    member_id: '',      // member id
    member_id_hash: '', // member hash value for lobby
    member_hash: '',    // member hash value for chat room
    member_type: '',
    member_username: '',
    _disconnect_url: '',
    site_url: '',
    
    init: function(data) {
        this.member_id = data.member_id || '';
        this.member_id_hash = data.member_id_hash || '';
        this.member_hash = data.member_hash || '';
        this.member_type = data.member_type || '';
        this.member_username = data.member_username || '';
        this._disconnect_url = data._disconnect_url || '';
        this.site_url = data.site_url || '';
    }
};

Chat.models.socket = {
    
    io: null,   // socket io
    socket_port:'',
    socket_url:'',
    
    init: function(data) {
        this.socket_url = data.socket_url || '';       // e.g.  'http://66.178.176.109'
        this.socket_port = data.socket_port || ''; 
        if (this.socket_url) {
            console.log("Set socket: " + this.socket_url + ":"+this.socket_port);
            this.io = io.connect(this.socket_url + ":"+this.socket_port);
            console.log("Setted socket");
        }
    }
};
Chat.manager.room = {
    
    chat_id: null,
    chat_session_id: null,
    server_chat_time: 0,
    max_chat_length: 0,   // second   max time set. 
    chat_length: 0,     // current time elasped
    time_balance: 0,    // remaining time. 
    free_minutes: 0,    // remaining time. 
    free_time_added: 0,    // remaining time. 
    stored_time: -1,    // stored time. 
    
    free_time_balance: 0,    // free remaining time. 
    tier: '',    // free remaining time. 
    added_time: '',    // added mins from their stored time
    
    client_info:{},
    reader_info:{},
    
    init: function(data) {
        console.log("room manager init");
        info_data = data;  // get reader, client data
        this.chat_id = data.chat_id || '';
        this.chat_session_id = data.chat_session_id || '';
        
        this.max_chat_length = data.max_chat_length || 0;
        this.chat_length = data.chat_length || 0;
        this.time_balance = data.time_balance || 0;
        this.free_time_balance = Math.round(data.client_total_free_balance * 60) || 0;
        
        this.free_minutes = data.free_minutes || 0;
        
        this.client_info.username = data.client_username || '';
        this.client_info.first_name = data.client_first_name || '';
        this.client_info.last_name = data.client_last_name || '';
        this.client_info.dob = data.client_dob || '';
        
        this.reader_info.username = data.reader_username || '';
        this.tier = data.tier || '';
        this.added_time = data.added_time || '';
        
        // register dispatcher event.
        Chat.manager.room.dispatcher();
        // disable chat form frist, enable it when chat start.
        // 10-4
        // Chat.views.message.system.disable_chat_form();
        
        // adjust chat input message box UI
        Chat.views.message.ui.adjust_input_message_textarea();
    },
    
    timer: {
        
        ok_send_typing: true,
        send_typing_interval: 3000, // 3 seconds
        sending_typing_timer: function() {
            var ok_send_typing = Chat.manager.room.timer.ok_send_typing;
            if (Chat.manager.room.timer.ok_send_typing) {
                Chat.manager.room.timer.ok_send_typing = false;
                setTimeout(function() {
                    Chat.manager.room.timer.ok_send_typing = true;
                }, Chat.manager.room.timer.send_typing_interval);
            }
            return ok_send_typing;
        },
        
        ok_display_typing: true,
        receive_typing_interval: 5000,
        receive_typing_timer: function() {
            var ok_display_typing = Chat.manager.room.timer.ok_display_typing;
            if (Chat.manager.room.timer.ok_display_typing) {
                Chat.manager.room.timer.ok_display_typing = false;
                setTimeout(function() {
                    if (Chat.manager.room.timer.ok_display_typing == false) {
                        // remove typing text. 
                        Chat.views.message.system.clear_typing();
                        Chat.manager.room.timer.ok_display_typing = true;
                    }
                }, Chat.manager.room.timer.receive_typing_interval);
            }
            return ok_display_typing;
        },
        
        reset_ok_display_typing: function() {
            // clear text
            Chat.views.message.system.clear_typing();
            Chat.manager.room.timer.ok_display_typing = true;
        },
        
        chat_timer_interval: 1000, // 1 second
        chat_timer_handler:null,
        chat_timer_record_interval: 5, // in  seconds
        chat_timer_correction_threshold: 5, // allowable client-server time diff
        chat_timer_has_started_before: false,
        
        free_chat_timer_interval: 1000, // 1 second
        free_chat_timer_handler:null,
        
        init_chat_timer: function() {            
            Chat.views.message.system.set_timer(Chat.manager.room.max_chat_length);
        },
        
        // start or resume
        start_free_chat_timer: function() {
            $('#warning-one-minute').val(0);
            $('#warning-two-minute').val(0);
            Chat.manager.room.timer.free_chat_timer_handler = setInterval(Chat.manager.room.timer.run_free_chat_timer, Chat.manager.room.timer.free_chat_timer_interval );

        },
        // start or resume
        start_chat_timer: function() {
            $('.pauseChatAnchor').show();
            $('.resumeChatAnchor').hide();
            Chat.views.message.system.enable_chat_form();
            //. if he has started, call start means resume
            if(Chat.manager.room.timer.chat_timer_has_started_before) {
                //Chat.views.message.system.resume();
            } else {
                // if he hasn't started before, if chat_length <= 0 means it is a fresh start.. else chat_length > 0 means user has been disconnected and rejoin. 
                if(Chat.manager.room.chat_length <= 0) {
                    console.log("Chat length <0, start ");
                    Chat.views.message.system.start();
                } else {
                    //Chat.views.message.system.resume();
                }
            }
            Chat.manager.room.timer.chat_timer_has_started_before = true;

            /*9-16
            * run chat timer when chat with non-admin user, but not run with admin user.
            * */
            if (parseInt(info_data.client_id) != 1) {

                Chat.manager.room.timer.chat_timer_handler = setInterval(Chat.manager.room.timer.run_chat_timer, Chat.manager.room.timer.chat_timer_interval);
            } else {

                Chat.manager.room.timer.chat_timer_handler = Chat.manager.room.timer.run_chat_timer;
            }

        },
        pause_chat_timer: function(paused_by) {
            $('.pauseChatAnchor').hide();
            $('.resumeChatAnchor').show();
            if (paused_by && paused_by != 'reader') {
                Chat.views.message.system.clear_typing();
                Chat.views.message.system.disable_chat_form();
            }
            clearInterval(Chat.manager.room.timer.chat_timer_handler);
            Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
            if(new_win)
                new_win.close();//david_v3
        },
        resume_chat_timer: function() {
            Chat.manager.room.timer.start_chat_timer();
            Chat.manager.room.timer.unset_check_added_time();
        },
        end_chat_timer: function(is_soft_stop) {
            try {
                clearInterval(Chat.manager.room.timer.chat_timer_handler);
            } catch (e) {
                Chat.views.error_log.log(e);
            }
            
            
            $('.pauseChatAnchor').hide();
            $('.resumeChatAnchor').hide();
            Chat.views.message.system.clear_typing();
            Chat.views.message.system.disable_chat_form();
            Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
            
            if (is_soft_stop !== true) {
                if (Chat.models.member.member_type == 'client') {
                    Chat.controller.room.close_window(15000);
                } else {
                    setTimeout(function() {window.location.href = "/my_account";}, 15000);
                    
                }
            } 
            if(new_win)
                new_win.close();//david_v3
        },
        reset_chat_timer: function() {
            
        }, 
        
        correction: function() {

            return; // TODO
        },
        
        force_corretion: function(server_chat_time) {
            console.log("Force Timer Correction ");
            Chat.manager.room.server_chat_time = server_chat_time;
            var time_diff = Chat.manager.room.chat_length - Chat.manager.room.server_chat_time;
            
            if (Math.abs(time_diff) > 0) {
                Chat.manager.room.chat_length = Chat.manager.room.server_chat_time;
                // show a message for time correction as system 
                Chat.views.message.system.show_message({message_type:'system', message:'Chat time correction by system monitor'});
                
                Chat.manager.room.time_balance = Chat.manager.room.time_balance +  time_diff;
                Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
            }
        },
        
        run_chat_timer: function() {
            //--- Every set interval record chat length - !! ONLY IF READER
            console.log("MOD " + Chat.manager.room.chat_length + " > " + Chat.manager.room.chat_length%Chat.manager.room.timer.chat_timer_record_interval);
            if (Chat.manager.room.time_balance >= 0 &&
                Chat.manager.room.time_balance % Chat.manager.room.timer.chat_timer_record_interval == 0 && 
                Chat.models.member.member_type == 'reader'){
                // send record chat time
                console.log('######## ======> '+Chat.manager.room.time_balance);
                console.log("Send record chat");
                Chat.manager.room.send.record_chat();
            }

            if (Chat.manager.room.time_balance == 120){

                if(message_used_2min == 0) {
                    if (Chat.models.member.member_type == 'client'){
                        /*9-18*//*9-22*/
                        setTimeout(function () {
                            Chat.views.message.system.show_message({message_type:'system', message: "2 Minute Warning: There are only 2 minutes left in your current chat session.<br/><br /> <span style='margin-left: 160px;'>We recommend purchasing more time or add stored time using the settings menu at the top.</span> " });
                        },1000);
                    }
                    if(typeof(Lobibox) != 'undefined'){ //david_v3
                        Lobibox.notify('info', {
                            title:'Notification',
                            showClass: 'fadeInDown',
                            hideClass: 'fadeUpDown',
                            delay:10000,
                            msg: '2 Minute Warning: There are only 2 minutes left in your current chat session.'
                        });
                    }
                    message_used_2min = 1;
                }
            } else if (Chat.manager.room.time_balance == 60){

            } else if(Chat.manager.room.time_balance <= 0){
                if(confirmed_to_use_freetime == 0) {
                    Chat.manager.room.timer.pause_chat_timer(true);
                    if(Chat.models.member.member_type == 'client') {
                        if ($("#selectFreeTime").val() == 0)
                        {
                            $("#warningFreeTime").val(1);
                            $("#selectFreeTime").val(1);
                            //Chat.manager.room.send.pause();
                            Chat.views.message.setMessage("<div class='system'><span class='chat_system_title'>System Message: </span>&nbsp;&nbsp; Would you like to use your Free Time? <button class='btn btnYesFreeTime' type='button'>YES</button>&nbsp;<button class='btn btnNoFreeTime' type='button'>NO</button></div>");
                        }
                        Chat.views.message.setMessage("<div class='timer'>Do you want to use your " + "<span class='SpecialAlert'>FREE</span>" + " time to continue current chat session? Please check Yes/No button above." + "<br/> <span class='SpecialAlert'>Yes</span>" + " to continue, " + "<span class='SpecialAlert'>No</span>" + " to end.</div>");
                    }
                    } else {

                    Chat.manager.room.timer.end_chat_timer(true);   // just softly end chat for both reader and client.
                    // Timer Ends
                    if(Chat.models.member.member_type == 'reader'){
                        // just end it.
                        Chat.manager.room.send.leave_chat();
                    }
                }
            }
            
            if (Chat.manager.room.time_balance == 0) {
                Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
            } else if (Chat.manager.room.time_balance > 0) {
                Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
                Chat.manager.room.time_balance -= (Chat.manager.room.timer.chat_timer_interval/1000);      // Remaining TIME LEFT
                Chat.manager.room.chat_length += (Chat.manager.room.timer.chat_timer_interval/1000);    // Time also elapsed. 
            } else {
                //Chat.views.message.system.set_timer(Chat.manager.room.chat_time);
                Chat.manager.room.time_balance = 0;
            }
        },
        
        run_free_chat_timer: function() {
            
            if (Chat.manager.room.free_time_balance >= 0 &&
                Chat.manager.room.free_time_balance % Chat.manager.room.timer.chat_timer_record_interval == 0 && 
                Chat.models.member.member_type == 'reader'){                
            
                //Chat.manager.room.send.record_chat();
                Chat.manager.room.send.update_free_timer();
                
            }
            
            console.log("Run free chat timer: " + Chat.manager.room.free_time_balance);
            
            if (Chat.manager.room.free_time_balance == 0) {

                Chat.views.message.setMessage("<div class='system red' style='color: #3e903e;'>**Paid time has now started!**</div>");                    

                Chat.views.message.system.set_free_timer(Chat.manager.room.free_time_balance);                    
                Chat.manager.room.timer.resume_chat_timer();
                clearInterval(Chat.manager.room.timer.free_chat_timer_handler);
            } else if (Chat.manager.room.free_time_balance > 0) {
                Chat.views.message.system.set_free_timer(Chat.manager.room.free_time_balance);
                Chat.manager.room.free_time_balance -= (Chat.manager.room.timer.chat_timer_interval/1000);      // Remaining TIME LEFT
            } else {
                Chat.manager.room.free_time_balance = 0;
            }
        },
        
        update_time_balance: function(data) {
           // there is a socket message to update time balance 
            // ignore for now. 
        },
        
        disconnection_handler: null,
        default_grace_period: 60,
        is_disconnected_on: false,
        start_disconnection_countdown: function(grace_period) {
            if (!grace_period) {
                grace_period = Chat.manager.room.timer.default_grace_period;
            }
            grace_period *= 1000; // turn into milli second
            Chat.manager.room.timer.is_disconnected_on = true;
            
            setTimeout(Chat.manager.room.timer.run_disconnection_countdown, grace_period ); 
        },
        set_disconnection_off: function() {
            Chat.manager.room.timer.is_disconnected_on = false;
        },
        run_disconnection_countdown: function() {
            if (Chat.manager.room.timer.is_disconnected_on) {
                // fire end Chat.
                Chat.manager.room.send.leave_chat();
            }
        },
        
        check_added_time_timer_interval: 5000, // 5 seconds
        check_added_time_timer_handler:null,
        // check for added time timer
        set_check_added_time: function() {
            if (Chat.manager.room.timer.check_added_time_timer_handler) {
                // do nothing
            } else {
                Chat.manager.room.timer.check_added_time_timer_handler = setInterval( Chat.manager.room.timer.update_added_time, Chat.manager.room.timer.check_added_time_timer_interval);
            }
        },
        
        unset_check_added_time: function() {
            if (Chat.manager.room.timer.check_added_time_timer_handler) {
                clearInterval(Chat.manager.room.timer.check_added_time_timer_handler);
            }
        },
        update_added_time: function () {
            // call the get stored time. 
            Chat.manager.room.send.refresh_stored_time();
        }
        /* ,
        
        chat_pending_max_time: 45,
        chat_pending_timer_interval: 1000,
        chat_pending_timer_handler: null,
        set_chat_pending_timer: function() {
            if (! Chat.manager.room.timer.chat_pending_timer_handler) {
                Chat.manager.room.timer.chat_pending_timer_handler = setInterval(Chat.manager.room.timer.update_chat_pending_time, Chat.manager.room.timer.chat_pending_timer_interval);
            }
        },
        unset_chat_pending_timer: function() {
            if (Chat.manager.room.timer.chat_pending_timer_handler) {
                Chat.manager.room.timer.chat_pending_max_time = 0;
                clearInterval(Chat.manager.room.timer.chat_pending_timer_handler);
            }
        },
        
        update_chat_pending_time: function() {
            // update the pending time. 
            if (Chat.manager.room.timer.chat_pending_max_time > 0) {
                Chat.manager.room.timer.chat_pending_max_time --;
                
                Chat.views.message.client.update_wait_time(Chat.manager.room.timer.chat_pending_max_time);
            } else {
                Chat.manager.room.timer.chat_pending_max_time = 0;
                Chat.views.message.client.update_wait_time(Chat.manager.room.timer.chat_pending_max_time);
                Chat.manager.room.timer.unset_chat_pending_timer();
            }
        }
        */
    },
    
    send: {
        
        join_new_chat_room: function() {
            console.log('chat id' + Chat.manager.room.chat_id + " chat session id: " + Chat.manager.room.chat_session_id);
            var input = {
                chat_id: Chat.manager.room.chat_id,
                chat_session_id: Chat.manager.room.chat_session_id,
                member_hash: Chat.models.member.member_hash,
                site_url: Chat.models.member.site_url
            };
            console.log("Sending join new chat room ");            
            Chat.models.socket.io.emit('join', input, function(data) {
                Chat.views.error_log.log(data); // just log
                // show message
                if (data.status) {
                    // 
                    Chat.manager.room.timer.init_chat_timer();
                    Chat.views.message.client.join_new_chat_room();
    

                } else {
                    Chat.views.error_log.show(data);
                }
            });
        },
        
        leave_chat: function () {
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,
                site_url    : Chat.models.member.site_url
            };
            Chat.models.socket.io.emit('leave', input, function(data){
                if (data.status) {
                    // TODO: adjust time.
                    Chat.manager.room.timer.end_chat_timer();
                    Chat.views.message.system.leave_chat_room(data.member_type);
                } else {
                    Chat.views.error_log.show(data);
                }
            });
        },
        
        start: function() {
            console.log("start emit " );
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash
            };
            Chat.models.socket.io.emit('start', input, function(data){
                console.log("Received Start ");
                Chat.views.error_log.log(data);
                if (data.status) {
                    // UI
                    $('.endChatAnchor').show();
                    $('.addStoredTime').show();
                    
                    Chat.manager.room.max_chat_length = data.max_chat_length;
                    Chat.manager.room.chat_length = data.chat_length;
                    Chat.manager.room.time_balance = data.time_balance;
                    // 10-4
                    // Chat.manager.room.timer.start_chat_timer();
                } else {
                    Chat.views.error_log.show(data);
                }
            });
            
        },
        
        pauseFreeTime: function() {            
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,
                site_url    : Chat.models.member.site_url
            };      
            Chat.models.socket.io.emit('pauseFreeTime', input, function(data){
                if (data.status) {
                    
                } else {
                    Chat.views.error_log.show(data);
                }
            });
        },

        continueFreeTime: function() {
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,
                site_url    : Chat.models.member.site_url
            };
            Chat.models.socket.io.emit('continueFreeTime', input, function(data){
                if (data.status) {

                } else {
                    Chat.views.error_log.show(data);
                    console.log('continueFreeTime Error: ', data);
                }
            });
        },
        
        pause: function() {            
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,
                site_url    : Chat.models.member.site_url
            };      
            Chat.models.socket.io.emit('pause', input, function(data){
                if (data.status) {
                    Chat.manager.room.chat_length = data.chat_length;
                    Chat.manager.room.time_balance = data.time_balance;
                    
                    Chat.manager.room.timer.pause_chat_timer(data.paused_by);
                    
                    var username = '';
                    if (data.member_type == 'client') {
                        username = Chat.manager.room.client_info.username;
                    } else {
                        username = Chat.manager.room.reader_info.username;
                    }
                    Chat.views.message.system.pause(username);
                    
                    if ($("#warningFreeTime").val() == '0' && $("#stopWatch").val() == '0')
                    {
                        // Run the timer max of 3 mins
                        setTimeout(function(){
                            Chat.manager.room.send.resume();                    
                        }, 180000);                        
                    }
                    
                } else {
                    Chat.views.error_log.show(data);
                }
            });
        },

        standby: function() {            
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,
                site_url    : Chat.models.member.site_url
            };      
            Chat.models.socket.io.emit('standby', input, function(data){
                if (data.status) {
                    Chat.manager.room.chat_length = data.chat_length;
                    Chat.manager.room.time_balance = data.time_balance;
                    
                    Chat.manager.room.timer.pause_chat_timer(data.paused_by);
                    
                    var username = '';
                    if (data.member_type == 'client') {
                        username = Chat.manager.room.client_info.username;
                    } else {
                        username = Chat.manager.room.reader_info.username;
                    }
                    Chat.views.message.system.pause(username);
                    
                    if ($("#warningFreeTime").val() == '0' && $("#stopWatch").val() == '0')
                    {
                        // Run the timer max of 3 mins
                        setTimeout(function(){
                            Chat.manager.room.send.resume();                    
                        }, 180000);                        
                    }
                    
                } else {
                    Chat.views.error_log.show(data);
                }
            });
        },
        
        resume: function(data) {
            Chat.views.message.system.show_add_stored_time(false);
            // Chat.views.message.system.show_lost_time(false);            
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash
            };
            Chat.models.socket.io.emit('resume', input, function(data){
                if (data.status) {
                    Chat.manager.room.chat_length = data.chat_length;
                    Chat.manager.room.time_balance = data.time_balance;
                    
                    Chat.manager.room.timer.resume_chat_timer();
                     
                    var username = '';
                    if (data.member_type == 'client') {
                        username = Chat.manager.room.client_info.username;
                    } else {
                        username = Chat.manager.room.reader_info.username;
                    }
                    Chat.views.message.system.resume(username);
                    
                } else {
                    Chat.views.error_log.show(data);
                }
            });
            
        },
        
        message: function(data) {
            console.log("message emit " );
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,
                message     : data.message,
                site_url    : Chat.models.member.site_url
            };
            if (data.message_type) {
                input.message_type = data.message_type;
            }
            
            Chat.models.socket.io.emit('message', input, function(data){

                if (data.status) {

                    if (Chat.models.member.member_type == "reader") {
                        
                        Chat.views.message.system.show_message({member_type:'reader', message:input.message, sender_name:data.sender_name});
                    } else {
                        Chat.views.message.system.show_message({member_type:'client', message:input.message, sender_name:data.sender_name});
                    }
                    
                } else {
                    Chat.views.error_log.show(data);
                }
            });
        },
        //vcall request emit david_v2
        vcall_request: function() {
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,                
            };           
            
            Chat.models.socket.io.emit('vcall_request', input, function(data){
                if (data.status) {   console.log("vcall request emit " );
                        $('#vcall_wait_req').find('h2').html('Calling '+data.receiver_name);
                        $('#vcall_wait_req').css('display','block');

                        snd = new Audio('/media/video_chat/vCall.mp3');
                        snd.play();
                    
                } else {console.log('vcall error');
                    Chat.views.error_log.show(data);
                }
            });
        },
        //vcall accept emit david_v2
        vcall_accept: function() {
            who = 'me';
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,                
            };           
            
            Chat.models.socket.io.emit('vcall_accept', input, function(data){
                if (data.status) {
                        $('.vcall').css('display','none');
                        snd.pause();                        
                        var new_win = window.open(vchat_url,'_blank');
                        new_win.onbeforeunload = function(){
                            //document.getElementById('startVchat').style.pointerEvents = 'auto';
                        };
                } else {console.log('vcall error');
                    Chat.views.error_log.show(data);
                }
            });
        },
        
      //vcall accepted emit david_v2
        vcall_accepted: function() {console.log('accepted emit');
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,                
            };console.log('-----input-----');           
            console.log(input);
            Chat.models.socket.io.emit('vcall_accepted', input);
        },
      //vcall reject emit david_v2
        vcall_reject: function() {          
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,                
            };           
            
            Chat.models.socket.io.emit('vcall_reject', input, function(data){
                if (data.status) {
                        $('.vcall').css('display','none');
                        //document.getElementById('startVchat').style.pointerEvents = 'auto';
                        snd.pause();                        
                } else {console.log('vcall error');
                    Chat.views.error_log.show(data);
                }
            });
        },
        
        typing: function(data) {
            console.log("Typing emit " );
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash
            };
            
            if (Chat.manager.room.timer.sending_typing_timer() ) {
                // 
                Chat.models.socket.io.emit('typing', input, function(data){
                    if (data.status) {
                       // do not show anything in sender.  NOt even error
                    } else {
                        // just log error , not show
                        Chat.views.error_log.log(data);
                    }
                });
            }
            
        },
        
        record_chat: function(data) {
            console.log("Typing record_chat " );
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,
                site_url    : Chat.models.member.site_url
            };
            
            Chat.models.socket.io.emit('record_chat', input, function(data){
                if (data.status) {
                   Chat.manager.room.time_balance = data.time_balance;
                   Chat.manager.room.chat_length = data.chat_length;
                   Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
                   //Chat.manager.room.timer.correction();
                } else {
                    // just log error , not show
                    Chat.views.error_log.log(data);
                }
            });
        },
        update_free_timer: function(data) {
            console.log("Typing update_free_timer");
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,
                site_url    : Chat.models.member.site_url
            };
            
            Chat.models.socket.io.emit('update_free_timer', input, function(data){
                if (data.status) {
                   //Chat.manager.room.time_balance = data.time_balance;
                   Chat.manager.room.total_free_length = data.total_free_length;
                   Chat.manager.room.free_length = data.free_length;
                   Chat.manager.room.free_time_balance = data.free_time_balance;
                   Chat.views.message.system.set_free_timer(data.free_time_balance);
                   //Chat.manager.room.timer.correction();
                } else {
                    // just log error , not show
                    Chat.views.error_log.log(data);
                }
            });
        },
        
        contact_admin: function() {
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,
                site_url    : Chat.models.member.site_url
            };
            
            Chat.models.socket.io.emit('contact_admin', input, function(data){
                // do nothing, just notify the reader
                if (data.status) {
                
                    Chat.manager.room.chat_length = data.chat_length;
                    Chat.manager.room.time_balance = data.time_balance;
                    
                    Chat.manager.room.timer.pause_chat_timer(data.paused_by);
                    
                    var username = Chat.manager.room.client_info.username;
                    Chat.views.message.system.pause(username);
                    
                } else {
                    Chat.views.error_log.show(data);
                }
            });
            
            Chat.views.message.client.show_contact_admin();
        },
        
        purchase_time: function(data) {
            console.log("notifying purchase time " );
            
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash
            };
            // move to display the popup first. 
            //Chat.views.popup.open_purchase_more_time_window();
            
            Chat.models.socket.io.emit('purchase_time', input, function(data){
                if (data.status) {
                    if (Chat.models.member.member_type == 'client') {
                        Chat.views.message.client.purchase_more_time();
                        //Chat.views.popup.open_purchase_more_time_window();
                        Chat.manager.room.timer.set_check_added_time();
                        $('#refreshPurchasedTime').show();
                    }
                } else {
                    // just log error , not show
                    Chat.views.error_log.log(data);
                }
                
                // pause timer display
                if (data.status == true && data.is_already_paused !== false) {
                    // pause
                    if (data.chat_length && data.time_balance) {
                        Chat.manager.room.chat_length = data.chat_length;
                        Chat.manager.room.time_balance = data.time_balance;
                    }
                    Chat.manager.room.timer.pause_chat_timer(data.paused_by);
                } 
            });
        },
        
        get_stored_time: function() {
            Chat.manager.room.send.pause();
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,
                is_bypass_message : false,
                site_url: Chat.models.member.site_url
            };
            
            Chat.models.socket.io.emit('get_stored_time', input, function(data){
                if (data.status) {
                    var stored_minute = Math.floor(data.stored_time); 
                    Chat.manager.room.stored_time = stored_minute;
                        
                    Chat.views.message.system.show_add_stored_time(true);
                    $('#refreshPurchasedTime').hide();
                    Chat.views.message.system.set_stored_time(data.stored_time);
                } else {
                    // just log error , not show
                    Chat.views.error_log.log(data);
                }
            });
        },
        
        refresh_stored_time: function() {
            //Chat.manager.room.send.pause();
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,
                is_bypass_message : true
            };
            
            Chat.models.socket.io.emit('get_stored_time', input, function(data){
                if (data.status) {
                    var stored_minute = Math.floor(data.stored_time); 
                    if ( Chat.manager.room.stored_time != stored_minute ) {
                        Chat.manager.room.stored_time = stored_minute;
                        Chat.views.message.system.set_stored_time(data.stored_time);
                    } 
                } else {
                    // just log error , not show
                    Chat.views.error_log.log(data);
                }
            });
        },
        
        
        add_stored_time: function(data) {
            var added_time = $('#remainingStoredTimeInput').val();
            
            if (added_time <= 0) {
                Chat.views.error_log.show("You cannot add time less than one minute");
                return;
            }
            
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,
                added_time  : added_time,
                site_url    : Chat.models.member.site_url
            };
            
            Chat.models.socket.io.emit('add_stored_time', input, function(data){
                if (data.status) {
                    Chat.manager.room.max_chat_length = data.max_chat_length;
                    Chat.manager.room.chat_length = data.chat_length;
                    Chat.manager.room.time_balance = data.time_balance;
                    Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
                } else {
                    // just log error , not show
                    Chat.views.error_log.log(data);
                }        
                
                // resume
                Chat.views.message.system.show_add_stored_time(false);
                // if($('#watchStandbyoff').val() == 1) {
                console.log('Free Timer Stopped now.');
                clearInterval(Chat.manager.room.timer.free_chat_timer_handler);
                Chat.views.message.system.set_free_timer(Chat.manager.room.free_time_balance);

                Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
                Chat.manager.room.send.resume();
                // }
            });
        },
        
        add_free_email: function(){
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,
                site_url    : Chat.models.member.site_url                
            };
            
            Chat.models.socket.io.emit('add_free_email', input, function(data){                
                if (data.status) {
                    
                } else {
                    // just log error , not show
                    Chat.views.error_log.log(data);
                }

            });
        },
        
        add_free_time: function() {
            var free_time = $('#freeTimeInput').val();
            console.log('Freetimeinput value = ', free_time)
            
            // if (free_time <= 0) {
            //     Chat.views.error_log.show("You cannot add free time less than one minute");
            //     return;
            // }
            
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,
                site_url    : Chat.models.member.site_url,
                free_time  : free_time
            };
            
            Chat.models.socket.io.emit('add_free_time', input, function(data){                
                if (data.status) {
                    Chat.manager.room.max_chat_length = data.max_chat_length;
                    Chat.manager.room.chat_length = data.chat_length;
                    //Chat.manager.room.time_balance = data.time_balance;
                    
                    Chat.manager.room.free_time_added = data.free_time_added;
                    Chat.manager.room.free_time_balance = Math.round(data.free_time_added * 60);
                    console.log('ADDDDDDDDDDED FREE TIME ========>>>>>>', data.free_time_added);
                    //Chat.views.message.system.set_timer(Chat.manager.room.time_balance);



                    Chat.views.message.reader.add_free_time(data.total_free_time, data.free_time_added);
                    Chat.manager.room.timer.pause_chat_timer();
                    Chat.manager.room.timer.start_free_chat_timer();

                    $('#freeTimeInput').val(0);

                } else {
                    // just log error , not show
                    Chat.views.error_log.log(data);
                }

            });
        },

        /*9-12
        * if client has a gift, set free time with the gift size.
        * */
        add_gift_time: function() {
            var free_time = $('#freeTimeInput').val();
            
            if (free_time <= 0) {
                Chat.views.error_log.show("You cannot add free time less than one minute");
                return;
            }
            
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,
                site_url    : Chat.models.member.site_url,
                free_time  : free_time
            };
            
            Chat.models.socket.io.emit('add_gift_time', input, function(data){                
                if (data.status) {
              
                    Chat.manager.room.max_chat_length = data.max_chat_length;
                    Chat.manager.room.chat_length = data.chat_length;
                    //Chat.manager.room.time_balance = data.time_balance;
                    
                    Chat.manager.room.free_time_added = data.free_time_added;
                    Chat.manager.room.free_time_balance = data.free_time_added * 60;
                    
                    //Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
                    
                    Chat.views.message.reader.add_gift_time(data.total_free_time, data.free_time_added);
                    Chat.manager.room.timer.pause_chat_timer();
                    Chat.manager.room.timer.start_free_chat_timer();
                    $('#freeTimeInput').val(0);
                } else {
                    // just log error , not show
                    Chat.views.error_log.log(data);
                }

            });
        },
        
        pban: function(data) {

            console.log("pban " );

            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,
                site_url : Chat.models.member.site_url
            };
            
            Chat.models.socket.io.emit('pban', input, function(data){
                if (data.status) {
                    Chat.views.message.reader.pban();
                    Chat.manager.room.timer.end_chat_timer();
                } else {
                    // just log error , not show
                    Chat.views.error_log.log(data);
                }
            });
        },
        
        fban: function(data) {
            console.log("fban " );
           
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,
                site_url    : Chat.models.member.site_url
            };
            
            Chat.models.socket.io.emit('fban', input, function(data){
                if (data.status) {
                    Chat.views.message.reader.fban();
                    Chat.manager.room.timer.end_chat_timer();
                } else {
                    // just log error , not show
                    Chat.views.error_log.log(data);
                }
            });
        },

        // 8-9
        refund: function() {
            console.log("refund " );
           
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,
                site_url: Chat.models.member.site_url
            };
            
            Chat.models.socket.io.emit('refund', input, function(data){
                if (data.status) {
                    /*thom ho 7.31*/
                    console.log('refund action is working and data status is okay');
                    if(Chat.models.member.member_type == "reader") {
                        Chat.views.message.reader.refund();
                    } 

                } else {
                    // just log error , not show
                    console.log('refund action is working but data status is faild');
                    Chat.views.error_log.log(data);
                    /*thom ho 7.31*/
                    if(Chat.models.member.member_type == "client") {
                        // Chat.views.message.client.refund();
                    } 
                    if(Chat.models.member.member_type == "reader") {
                        Chat.views.message.reader.refund();
                    } 
                }
            });
        },
        
        lost_time: function(lost_time) {
            console.log("lost_time " );
           
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,
                site_url    : Chat.models.member.site_url,
                lost_time   : (lost_time * 60) /* in second */
            };
            
            Chat.manager.room.send.pause();
            Chat.models.socket.io.emit('lost_time', input, function(data){
                if (data.status) {
                    Chat.manager.room.chat_length = data.chat_length;
                    Chat.manager.room.time_balance = data.time_balance;
                    Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
                    Chat.views.message.system.show_lost_time(false);
                    
                    setTimeout(function(){
                        Chat.manager.room.send.leave_chat();
                    }, 500);
                    //Chat.manager.room.send.resume();
                } else {
                    // just log error , not show
                    Chat.views.error_log.log(data);
                }
            });
        },
        
        check_time_balance: function(callback) {
            console.log("Typing emit " );
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash
            };
            
            if (Chat.manager.room.timer.sending_typing_timer() ) {
                // 
                Chat.models.socket.io.emit('check_time_balance', input, function(data){
                    if (data.status) {
                       // do not show anything in sender.  NOt even error
                       if (callback) {
                           Chat.manager.room.max_chat_length = data.max_chat_length;
                           Chat.manager.room.chat_length = data.chat_length;
                           Chat.manager.room.chat_time = data.time_balance;
                           callback(null, data);
                       } else {
                           return data;
                       }
                    } else {
                        // just log error , not show
                        Chat.views.error_log.log(data);
                        if (callback) {
                            callback(data);
                        }
                    }
                });
            }
        }
        
    },
    
    // receiving message function
    receive: {
        // on receiving the call for join or create new chat room 
        
        reader_join_new_chat_room: function(data) {
            console.log(" raeder join new chat room");
            Chat.views.error_log.log(data);
            
            // then it should send start chat now. 
            Chat.manager.room.timer.init_chat_timer();
            Chat.manager.room.send.start();
        },
        
        reject_chat: function(data) {
            console.log(" reader reject the chat request");
            
            Chat.views.message.system.disable_chat_form();
            Chat.views.message.client.reject_chat(data.reader_username);
            Chat.controller.room.close_window();
        },
        
        abort_chat_room: function(data) {
            console.log(" client abort the chat request");
            if (Chat.models.member.member_type == "client") {
                Chat.views.message.system.disable_chat_form();
                Chat.views.message.client.abort_chat();
                Chat.controller.room.close_window();
            } else {
                Chat.views.message.reader.abort_chat();
            }

        },
        
        leave_chat_room: function(data) {
            if (data.status) {
                end_session = 1;
                Chat.manager.room.timer.end_chat_timer();
                Chat.views.message.system.leave_chat_room(data.member_type);
            } else {
                Chat.views.error_log.show(data);
            }
            
        },
        start: function(data) {
            console.log("start now " + data);
            
            $('.endChatAnchor').show();
            $('.addStoredTime').show();
            $('.pauseChatAnchor').show();
            $('.resumeChatAnchor').hide();
            
            Chat.manager.room.max_chat_length = data.max_chat_length;
            Chat.manager.room.chat_length = data.chat_length;
            Chat.manager.room.chat_time = data.time_balance;
            // 10-4
            // Chat.manager.room.timer.start_chat_timer();
        },
        
        pause: function(data) {
            console.log("pause now " + data);
            if (data.status) {
                if (! data.pause_no_time) {
                    Chat.manager.room.chat_length = data.chat_length;
                    Chat.manager.room.time_balance = data.time_balance;
                }
                Chat.manager.room.timer.pause_chat_timer(data.paused_by);
               
                clearInterval(myinterval);

                var username = '';
                if (data.member_type == 'client') {
                    username = Chat.manager.room.client_info.username;
                } else {
                    username = Chat.manager.room.reader_info.username;
                }
                
                if (data.is_by_disconnection) {
                    Chat.views.message.system.disconnect_pause(username);
                } else {
                    Chat.views.message.system.pause(username);
                }
                
            } else {
                Chat.views.error_log.show(data);
            }
        },
        /*8-26
        * stand by function, timer is not working.
        * */
        stand_by: function(data) {
            console.log("pause now " + data);
            if (data.status) {
                if (! data.pause_no_time) {
                    Chat.manager.room.chat_length = data.chat_length;
                    Chat.manager.room.time_balance = data.time_balance;
                }
                Chat.manager.room.timer.pause_chat_timer(data.paused_by);
                /*8-26*/
                Chat.views.message.setMessage("<div class='system red' style='color: #3e903e;'>**Stand by is ON (You do not pay for that period)**</div>");
                clearInterval(myinterval);

                var username = '';
                if (data.member_type == 'client') {
                    username = Chat.manager.room.client_info.username;
                    
                } else {
                    username = Chat.manager.room.reader_info.username;
                }
                
                if (data.is_by_disconnection) {
                    Chat.views.message.system.disconnect_pause(username);
                } else {
                    Chat.views.message.system.pause(username);
                }
                
            } else {
                Chat.views.error_log.show(data);
            }
        },
        
        resume: function(data) {
            if (data.status) {
                Chat.manager.room.chat_length = data.chat_length;
                Chat.manager.room.time_balance = data.time_balance;
                Chat.manager.room.added_time = data.added_time;
                
                Chat.manager.room.timer.resume_chat_timer();
                 
                var username = '';
                if (data.member_type == 'client') {
                    username = Chat.manager.room.client_info.username;
                } else {
                    username = Chat.manager.room.reader_info.username;
                }
                Chat.views.message.system.resume(username);
                
            } else {
                Chat.views.error_log.show(data);
            }
        },
        
        message: function(data) {
            duration_no_action = 0;

            if (data.status) {

                Chat.manager.room.timer.reset_ok_display_typing();
                if (data.message_type && data.message_type == 'system') {
                    Chat.views.message.system.show_message({member_type:'system', message:data.message});
                } else if (data.sender == "reader") {
                    
                    Chat.views.message.system.show_message({member_type:'reader', message:data.message, sender_name:data.sender_name});
                } else {
                    Chat.views.message.system.show_message({member_type:'client', message:data.message, sender_name:data.sender_name});
                }
                
            } else {
                Chat.views.error_log.show(data);
            }
        },
        //vcall request accept david_v2
        vcall_request: function(data) {
                if (data.status) {
                     $('#vcall_req').find('h2').html('Call from '+data.sender_name);
                     $('#vcall_req').css('display','block');                     

                     snd = new Audio('/media/video_chat/vCall.mp3');
                     snd.play();
                    
                } else {
                    Chat.views.error_log.show(data);
                }
        },
      //vcall reject david_v2
        vcall_reject: function(data) {console.log("vcall reject :" + JSON.stringify(data) );
                if (data.status) { 
                    $('.vcall').css('display','none');
                    //document.getElementById('startVchat').style.pointerEvents = 'auto';
                    snd.pause();
                    
                } else {console.log('vcall error');
                    Chat.views.error_log.show(data);
                }
        },
        //vcall accepted david_v2
        vcall_accepted: function(data) {
                if (data.status) { console.log('member :' + Chat.models.member.member_type);console.log('sender : '+data.sender);
                    if(Chat.models.member.member_type != data.sender){
                        $('.vcall').css('display','none');
                        snd.pause();                        
                        var new_win = window.open(vchat_url,'_blank');
                        new_win.onbeforeunload = function(){
                            //document.getElementById('startVchat').style.pointerEvents = 'auto';
                        };
                    }               
                } else {
                    Chat.views.error_log.show(data);
                }
        },
        //\ end
        typing: function(data) {            
            if (data.status) {
                if (data.sender_name) {
                    if (Chat.manager.room.timer.receive_typing_timer() ) {
                        Chat.views.message.system.show_typing(data.sender_name);
                    }
                }
            }
        },
        
        pauseFreeTime: function(data) {            
            if (data.status) {
                if (data.sender_name) {
                    // Chat.manager.room.send.resume();
                    Chat.manager.room.send.add_free_time();
                }
            }
        },

        continueFreeTime: function(data) {
            if (data.status) {
                if (data.sender_name) {
                    console.log('Continue Free TIme');
                    Chat.manager.room.timer.pause_chat_timer();
                    Chat.manager.room.timer.start_free_chat_timer();
                }
            }
        },
        
        record_chat: function(data) {
            //console.log("receive record_chat" + JSON.stringify(data));
            // record chat time  
            if (data.status) {
                Chat.manager.room.time_balance = data.time_balance;
                Chat.manager.room.chat_length = data.chat_length;

                if (data.time_balance < 120 && $("#freetimeadded").val() == 1 && $("#selectAddedFreeTime").val() == 0 && Chat.manager.room.free_time_balance > 0)
                {
                    $("#selectAddedFreeTime").val(1);
                    Chat.views.message.setMessage("<div class='system'><span class='chat_system_title'>System Message: </span>&nbsp;&nbsp; Would you like to use your Free Time? <button class='btn btnYesAddedFreeTime' type='button'>YES</button>&nbsp;<button class='btn btnNoAddedFreeTime' type='button'>NO</button></div>");
                }

                if (data.time_balance < 120 &&  $('#freetimeadded').val() == 0 && $("#btnYesPaused").val() == '0' && $("#allowFreeTime").val() == 1 && $("#selectFreeTime").val() == 0)
                {
                    $("#warningFreeTime").val(1);
                    $("#selectFreeTime").val(1);
                    //Chat.manager.room.send.pause();
                    Chat.views.message.setMessage("<div class='system'><span class='chat_system_title'>System Message: </span>&nbsp;&nbsp; Would you like to use your Free Time? <button class='btn btnYesFreeTime' type='button'>YES</button>&nbsp;<button class='btn btnNoFreeTime' type='button'>NO</button></div>");
                }

                
                Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
                // if (Chat.manager.room.tier !== 'free')
                // {
                //     $('#timerSpanFree').html(Chat.views.message.timerFormat(0));
                // }
            }
        },
        record_free_chat: function(data) {
            console.log("receive record_free_chat" + JSON.stringify(data));
            // record free chat time  
            if (data.status) {
                if(data.free_time_balance > 60 && data.free_time_balance <= 120 && $("#warning-two-minute").val() == 0) {
                    $("#warning-two-minute").val("1");
                    Chat.views.message.setMessage("<div class='system red'  style='color: #3e903e;'>**Your free time ends in 2 minute. **<br>** We recommend purchasing more time or add stored time using the settings menu at the top.**</div>");
                }
                
                if(data.free_time_balance <= 60 && $("#warning-one-minute").val() == 0) {
                    $("#warning-one-minute").val("1");
                    Chat.views.message.setMessage("<div class='system red'  style='color: #3e903e;'>**Your free time ends in 1 minute. **<br>** We recommend purchasing more time or add stored time using the settings menu at the top.**</div>");
                }
                if(data.free_time_balance < 5) {
                    setTimeout(function() {

                        Chat.views.message.setMessage("<div class='system red' style='color: #3e903e;'>**Paid time has now started!**</div>");                    
                    }, 4000);
                }
                Chat.manager.room.free_time_balance = data.free_time_balance;
                Chat.manager.room.free_length = data.free_length;
                Chat.manager.room.time_balance = data.time_balance;
                Chat.views.message.system.set_free_timer(Chat.manager.room.free_time_balance);     
                $('#timerSpan').html(Chat.views.message.timerFormat(Chat.manager.room.time_balance));
                Chat.manager.room.timer.pause_chat_timer();
            }
        },
        disconnect_chat_room: function(data) {
            console.log("disconnect_chat_room " + data);
            // pause Timer First
            Chat.manager.room.timer.pause_chat_timer();
            if (data.time_balance) {
                Chat.manager.room.timer.force_corretion(data.time_balance);
            }
            Chat.manager.room.timer.start_disconnection_countdown(data.grace_period);
        },
        
        client_purchase_time: function(data) {
            if (data) {
                if (data.status == true && data.is_already_paused !== false) {
                    // pause
                    if (data.chat_length && data.time_balance) {
                        Chat.manager.room.chat_length = data.chat_length;
                        Chat.manager.room.time_balance = data.time_balance;
                    }
                    Chat.manager.room.timer.pause_chat_timer(data.paused_by);
                } 
                
                if (Chat.models.member.member_type == 'reader') {
                    Chat.views.message.reader.purchase_more_time();
                }
            }
        },
        client_add_stored_time: function(data) {
            if (data.status) {
                Chat.manager.room.max_chat_length = data.max_chat_length;
                Chat.manager.room.chat_length = data.chat_length;
                Chat.manager.room.time_balance = data.time_balance;

                console.log('Free Timer Stopped now.');
                clearInterval(Chat.manager.room.timer.free_chat_timer_handler);
                Chat.views.message.system.set_free_timer(Chat.manager.room.free_time_balance);

                Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
            }
        },
        get_stored_time: function(data) {
            if (data.status && data.is_bypass_message === false) {
                Chat.views.message.system.get_stored_time(data.stored_time);  
            }
            
        },
        reader_add_free_time: function (data) {
            if (data.status) {
                Chat.manager.room.max_chat_length = data.max_chat_length;
                Chat.manager.room.chat_length = data.chat_length;
                Chat.manager.room.time_balance = data.time_balance;
                Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
                // Chat.views.message.client.add_free_time(data.total_free_time, data.free_time_added);
                /*9-16*/
                setTimeout(function(){

                    if($("#gift_used").val() == 1) {

                        Chat.views.message.client.add_gift_time(data.total_free_time, data.free_time_added);
                    } else {

                        Chat.views.message.client.add_free_time(data.total_free_time, data.free_time_added);
                    }
                },1000);
            }  
        },

        /*9-12
        * reader adds gift time.
        * */
        reader_add_gift_time: function (data) {
            if (data.status) {
                
                Chat.manager.room.max_chat_length = data.max_chat_length;
                Chat.manager.room.chat_length = data.chat_length;
                Chat.manager.room.time_balance = data.time_balance;
                Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
                Chat.views.message.client.add_gift_time(data.total_free_time, data.free_time_added);
            }  
        },

        reader_add_free_email: function (data) {
            if (data.status) {                
                Chat.views.message.client.add_free_email();
            }  
        },
        contact_admin: function(data) {
            if (data.status) {
                Chat.manager.room.chat_length = data.chat_length;
                Chat.manager.room.time_balance = data.time_balance;
                Chat.manager.room.timer.pause_chat_timer(data.paused_by);
                
                Chat.views.message.reader.show_contact_admin();
            }
        },
        pban: function(data) {
            Chat.views.message.client.pban();
            Chat.manager.room.timer.end_chat_timer();
        },
        fban: function(data) {
            Chat.views.message.client.fban();
            Chat.manager.room.timer.end_chat_timer();
        }, 

        //8-9
        refund: function(data) {
            if (data.status) {

                if (Chat.models.member.member_type == 'client') {
                    Chat.views.message.client.refund();
                }

            } else {
                Chat.views.error_log.show(data);
            }
        },
        lost_time: function(data) {
            if (data.status) {
                Chat.manager.room.chat_length = data.chat_length;
                Chat.manager.room.time_balance = data.time_balance;
                Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
                if (Chat.models.member.member_type == 'client') {

                    Chat.views.message.system.show_lost_time(false);
                }
                if (Chat.models.member.member_type == 'reader') {

                    Chat.views.message.system.show_lost_time(false);
                }
                // there will be resume call. wait for it. 
            } else {
                Chat.views.error_log.show(data);
            }
        }
    },
    
    
    
    // socket message dispatcher
    dispatcher: function() {
        console.log("Dispatcher ");
        var io = Chat.models.socket.io; // Chat.socket
        
        io.on('reader_join_new_chat_room', Chat.manager.room.receive.reader_join_new_chat_room);
        io.on('reject_chat', Chat.manager.room.receive.reject_chat);        // OUTDATED
       // io.on('abort_chat_room', Chat.manager.room.receive.abort_chat_room);
        io.on('leave_chat_room', Chat.manager.room.receive.leave_chat_room);
        io.on('start', Chat.manager.room.receive.start);
        io.on('pause', Chat.manager.room.receive.pause);
        io.on('resume', Chat.manager.room.receive.resume);
        io.on('message', Chat.manager.room.receive.message);
        io.on('vcall_request', Chat.manager.room.receive.vcall_request); //david_v2
        io.on('vcall_reject', Chat.manager.room.receive.vcall_reject); //david_v2
        io.on('vcall_accepted', Chat.manager.room.receive.vcall_accepted); //david_v2
        io.on('client_purchase_time', Chat.manager.room.receive.client_purchase_time);
        io.on('client_add_stored_time', Chat.manager.room.receive.client_add_stored_time);
        io.on('get_stored_time', Chat.manager.room.receive.get_stored_time);
        io.on('refund', Chat.manager.room.receive.refund);
        io.on('lost_time', Chat.manager.room.receive.lost_time);
        io.on('reader_add_free_time', Chat.manager.room.receive.reader_add_free_time);
        io.on('reader_add_free_email', Chat.manager.room.receive.reader_add_free_email);
        io.on('pban', Chat.manager.room.receive.pban);
        io.on('fban', Chat.manager.room.receive.fban);
        io.on('typing', Chat.manager.room.receive.typing);
        io.on('pauseFreeTime', Chat.manager.room.receive.pauseFreeTime);
        io.on('record_chat', Chat.manager.room.receive.record_chat);
        io.on('record_free_chat', Chat.manager.room.receive.record_free_chat);
        io.on('disconnect_chat_room', Chat.manager.room.receive.disconnect_chat_room);
        io.on('check_time_balance', Chat.manager.room.timer.update_time_balance);
        io.on('contact_admin', Chat.manager.room.receive.contact_admin);
        /*8-26
        * standby receiver
        * */
        io.on('standby', Chat.manager.room.receive.stand_by);
        /*9-12
        * reader adds gift time event receiver
        * */
        io.on('reader_add_gift_time', Chat.manager.room.receive.reader_add_gift_time);
        io.on('continueFreeTime', Chat.manager.room.receive.continueFreeTime);
    }
};
Chat.views.message = {
    
    
    // show on reader window
    reader: {
        chat_attempt_for_reader: function (data) {
            Chat.views.error_log.log(data);
            $('#messageContainer').append("<div class='system'>A client has sent your a request for chat.</div>");    
            //$('body').append("<div id='modal'><div class='cont'><b>\""+data.client_username+"\" wants to chat with you.<br />Do you want to accept?</b><div style='margin:25px 0 0;' align='center'><a id='startChat' class='blue-button' onclick='Chat.views.popup.open_window(\""+data.chat_id+"\", \""+data.chat_session_id+"\")' ><span>Start Chatting</span></a> &nbsp; <a id='closeChat' class='blue-button' onclick='Chat.controller.lobby.actions.reject_chat_room(\""+data.chat_session_id+"\")'><span>Not At This Time</span></a></div></div></div><div id='modal_bg'></div>");
            $('body').append("<div id='modal'><div class='cont'><b>\""+data.client_username+"\" wants to chat with you.<br />Do you want to accept?</b><div style='margin:25px 0 0;' align='center'><a id='startChat' class='blue-button' target='_blank' onclick='Chat.controller.lobby.actions.accept_chat_room(\""+data.chat_session_id+"\")' ><span>Start Chatting</span></a> &nbsp; <a id='closeChat' class='blue-button' onclick='Chat.controller.lobby.actions.reject_chat_room(\""+data.chat_session_id+"\")'><span>Not At This Time</span></a></div></div></div><div id='modal_bg'></div>");
        
        
            var leftPos = ($(window).width()/2)-($('#modal').width()/2);
            var topPos = ($(window).height()/2)-($('#modal').height()/2);

            $('#modal').css('left',leftPos);
            $('#modal').css('top',topPos);

            // Play sound every 4 seconds
            Chat.views.popup.sound.play_sound();
            //Chat.views.popup.sound.soundIV = setInterval(Chat.views.popup.sound.play_sound, 2000);

        },
        
        reject_chat_room: function() {
            Chat.views.popup.sound.stop_sound();
            Chat.views.popup.remove_modal(); 
        },
        
        abort_chat: function() {
            Chat.views.popup.sound.stop_sound();
            Chat.views.popup.remove_modal();
        },
        
        show_contact_admin: function() {
            Chat.views.message.setMessage("<div class='system'>Client is contacting admin. The chat is paused. </div>");
        },
        
        notify_purchase_more_time: function() {
            Chat.views.message.setMessage("<div class='timer'>Notifying client to purcahse more time</div>");
        },
        purchase_more_time: function() {
            //Chat.views.message.setMessage("<div class='timer'>The client is purchasing additional time and chat is still paused. Please wait until client back to chat. </div>");
            Chat.views.message.setMessage("<div class='timer'>Chat paused: Client BMT</div>");
        },
        
        add_stored_time: function() {
            Chat.views.message.setMessage("<div class='timer'>The client has paused the chat to add additional stored time. You may close this chat window. You will be notified when the user is ready to chat again.</div>");
        },
        
        add_free_time: function(total_free_time, free_time_added) {
            free_time_added = free_time_added * 60;
            let mins = Math.floor(free_time_added / 60);
            let seconds = free_time_added - mins * 60;
            seconds = seconds.toFixed();
            //Chat.views.message.setMessage("<div class='timer'> Added "+free_time_added +"minutes to this chat session for <span class='SpecialAlert'>FREE !</span>.  You have been rewarded <span class='SpecialAlert' >total: "+  total_free_time + " minutes </span> during this chat session.   </div>");
            Chat.views.message.setMessage("<div class='timer'>" + Chat.manager.room.client_info.username + " used "+ mins + "minutes and " + seconds + "seconds" +" of his/her <span class='SpecialAlert'>FREE</span> time</span>.");
        },

        /*9-12*/
        add_gift_time: function(total_free_time, free_time_added) {
            //Chat.views.message.setMessage("<div class='timer'> Added "+free_time_added +"minutes to this chat session for <span class='SpecialAlert'>FREE !</span>.  You have been rewarded <span class='SpecialAlert' >total: "+  total_free_time + " minutes </span> during this chat session.   </div>");
            // Chat.views.message.setMessage("<div class='timer'>" + Chat.manager.room.client_info.username + " used "+free_time_added +"minutes of his <span class='SpecialAlert'>FREE</span> time</span>.");
        },
        
        pban: function() {
            Chat.views.message.setMessage("<div class='timer'>"+ Chat.manager.room.client_info.username +" has been banned.</div>");
        },
        
        fban: function() {
            Chat.views.message.setMessage("<div class='timer'>"+ Chat.manager.room.client_info.username +" has been fully banned.</div>");
        },
        
        refund: function() {
            // Chat.views.message.setMessage("<div class='system'>Return " + Chat.manager.room.client_info.username + "'s chat time to client. </div>");
            Chat.views.message.setMessage("<div class='system'>System has returned " + Chat.manager.room.client_info.username + "'s chat time.</div>");
        }
    },
    
    client: {
        
        join_new_chat_room: function () {
            if (Chat.models.member.member_type == "client") {
                Chat.views.message.setMessage("<div class='system'>The reader has been contacted & no minutes will be used or taken until the timer starts at the top of this screen.</div>");    
                //Chat.views.message.setMessage("<div class='system'>You can <span id='chat_abort_room' class='SpecialAlert Pointer' onclick='Chat.manager.room.send.abort_chat'>abort this chat</span> before reader start</div>");
            }
        },
        
        reject_chat: function(username) {
            Chat.views.message.setMessage("<div class='timer'>"+username+" is busy as this time.</div>");
        },
        
        abort_chat: function() {
            Chat.views.message.setMessage("<div class='system'>Reader has no response.  The chat has been aborted.  Please try again later. </div>");
        },
        
        times_up_pause: function() {
            Chat.views.message.setMessage("<div class='system'>The chat session times up and is paused until additional time is purchased. </div>");
        },
        
        show_contact_admin: function() {
            Chat.views.message.setMessage("<div class='system'>Contact page is now open on new page.</div>");  
        },
        
        purchase_more_time: function() {
            Chat.views.message.setMessage("<div class='timer'>You can buy more time in the purchase page </div>");
        },
        
        add_stored_time: function() {
            Chat.views.message.setMessage("<div class='timer'>You will be redirected to the time add stored time page </div>");
        },
        
        add_free_time: function(total_free_time, free_time_added) {
            free_time_added = free_time_added * 60;
            let mins = Math.floor(free_time_added / 60);
            let seconds = free_time_added - mins * 60;
            seconds = seconds.toFixed();
            //Chat.views.message.setMessage("<div class='timer'>"+ Chat.manager.room.reader_info.username  +"has added "+free_time_added +"minutes to this chat session for <span class='SpecialAlert'>FREE !</span>.  </div>");
            Chat.views.message.setMessage("<div class='timer'>You started your "+ mins + "minutes and " + seconds + "seconds of " + "<span class='SpecialAlert'>FREE</span>" + " time to this chat session.</div>");
        },

        click_yes_free_time: function () {
            Chat.views.message.setMessage("<div class='timer'>Your <span class='SpecialAlert'>FREE</span> time session will start after the paid time consumed.</div>");
        },

        click_no_free_time: function () {
            Chat.views.message.setMessage("<div class='timer'>Your <span class='SpecialAlert'>FREE</span> time session won't start after the paid time consumed.</div>");
        },

        /*9-12*/
        add_gift_time: function(total_free_time, free_time_added) {
            //Chat.views.message.setMessage("<div class='timer'>"+ Chat.manager.room.reader_info.username  +"has added "+free_time_added +"minutes to this chat session for <span class='SpecialAlert'>FREE !</span>.  </div>");
            Chat.views.message.setMessage("<div class='system red' style='color: #3e903e;'>**"+ info_data.reader_username + " has given you a gift of FREE TIME: " + free_time_added +"mins**</div>");
        },
        
        add_free_email: function(){
            Chat.views.message.setMessage("<div class='timer'>Your Reader has given you a FREE Email Reading: Details will appear on your acct at end of this session.  </div>");
        },
        
        pban: function() {
            Chat.views.message.setMessage("<div class='timer'>You have been banned.</div>");
        },
        
        fban: function() {
            Chat.views.message.setMessage("<div class='timer'>You have been banned.</div>");
        },
        
        refund: function() {
            Chat.views.message.setMessage("<div class='system'> System has returned  your chat time due to no typing for 1mins or more. </div>");
            // Chat.views.message.setMessage("<div class='system'>" +Chat.manager.room.reader_info.username + " returned your time. </div>");
        },
        
        update_wait_time: function(remaining_time) {
            $("#wait_seconds_left").html(remaining_time);
        },
        show_pending_dialog: function(reader_username) {
            $( "#dialog-wait" ).dialog( "open" );
            /*
            if (disable_manual_quit) {
                $(".ui-dialog-buttonpane button:contains('Quit This Chat Attempt')").button("disable");
                $(".ui-dialog-buttonpane button:contains('Quit This Chat Attempt')").attr("disabled", true).addClass("ui-state-disabled");
            }
            */
            $( "#dialog-wait").dialog('option', 'title', "Waiting for response from " + reader_username);
        },
        
        hide_pending_dialog: function() {
            $( "#dialog-wait" ).dialog( "close" );
        },
        show_reject_dialog: function() {
            $('#dialog-reject').dialog("open");
        },
        show_abort_retry_dialog: function() {
            $('#dialog-abort-retry').dialog("open");
        },
        show_offline_retry_dialog: function() {
            $('#dialog-offline-retry').dialog("open");
        },
        show_unable_to_create_chat_max_manual_quit_dialog: function() {
            $('#dialog-max-manual-quit').dialog("open");
        }
    },
    
    system: {

        //thom ho8-6
        force_end_chat_room_client: function() {
            Chat.views.message.setMessage("<div class='system'>Chat has been ended by the Admin.</div>");
        },

        force_end_chat_room_reader: function() {
            Chat.views.message.setMessage("<div class='system'>Chat has been ended by the Admin.</div>");
            Chat.views.message.setMessage("<div class='system'>You will be redirect to your account page in 15 seconds.  You can also go here directly: <a href='/my_account'>My Account</a></div>");    
        },
        
        leave_chat_room: function (member_type) {
            Chat.views.popup.sound.play_end_sound();
            Chat.views.message.setMessage("<div class='system'>Chat has been terminated by the system"+ ".</div>");
            if (Chat.models.member.member_type == 'reader') {
                Chat.views.message.setMessage("<div class='system'>You will be redirect to your account page in 15 seconds.  You can also go here directly: <a href='/my_account'>My Account</a></div>");    
            }
            
        },
        
        show_typing: function(username) {
            $('#whosTyping').html("<span class='chat_username'>" + username  + "</span>&nbsp;&nbsp;is typing...");
        },
        
        clear_typing: function() {
            $('#whosTyping').html("");
        },
        
        show_message: function(data){
            Chat.views.message.system.clear_typing();
            var fontsize = $('#font-size').html();
            var fontcolor = $('#font-color').html();
            //clearTimeout(whosTypingTimeout);
            if (data.message_type == 'system') {
                var class_name = "chat_system";
                
                Chat.views.message.setInputMessage("<div class='"+class_name+"'><span class='chat_system_title'>System Message: </span>&nbsp;&nbsp;"+data.message+"</div>");
            } else {
                var class_name = "chat_" + data.member_type;
                
                // Chat.views.message.setMessage("<div class='"+class_name+"'><span class='chat_username'>"+data.sender_name+":</span>&nbsp;&nbsp;<span class='text' style='color: "+ fontcolor +"; font-size: "+ fontsize +"px;'>"+data.message+"</span></div>");
                Chat.views.message.setInputMessage("<div class='"+class_name+"' style='font-size:"+fontsize+"px;'><span class='chat_username'>"+data.sender_name+":</span>&nbsp;&nbsp;<span class='text' style='color: "+ fontcolor +"; '>"+data.message+"</span></div>");
                if ($("#chat_sound").val() == "1" && Chat.models.member.member_type != data.member_type)
                {
                    // Norman
                    Chat.views.popup.sound.play_message_sound();
                    setTimeout(function(){
                        Chat.views.popup.sound.stop_message_sound();
                    }, 700);
                }
            }
            
        },
        
        start: function() {
            
            Chat.views.message.setMessage("<div class='timer'>The timer is started</div>");
        },
        
        pause: function(username) {
            if (username) {  

            } else {

            }
        },
        
        disconnect_pause: function(username) {
            if (username) {
                Chat.views.message.setMessage("<div class='timer'>Connections to opponent lost.<br>"+username+" has paused because of disconnection</div>");

            } else {
                Chat.views.message.setMessage("<div class='timer'>Connections to opponent lost.<br>The timer is paused because of disconnection</div>");
            }
        },
        
        resume: function(username) {
            if (username) {
                if (Chat.manager.room.added_time > 0 && Chat.models.member.member_type != 'client')
                {
                    Chat.views.message.setMessage("<div class='system'>Client has added <span style='font-style: italic;'>"+ Chat.manager.room.added_time + "</span> minutes from their stored time</div>");
                    Chat.manager.room.added_time = 0;
                    startTimer();
                }
                else
                {
                    /*10-4*/
                    if(Chat.models.member.member_type == 'client') {
                        Chat.views.message.setMessage("<div class='timer'>"+username+" has resumed the timer</div>");
                    }

                    /*8-26*/
                    if($("#addtime_mode").val() == 0) {

                        Chat.views.message.setMessage("<div class='system red' style='color: #3e903e;'>**Stand by is OFF**</div>");
                        
                    }
                    $("#addtime_mode").val("0");
                    startTimer();
                }
            } else {
                Chat.views.message.setMessage("<div class='timer'>The timer is resumed</div>");
            }
        },
        
        refund: function() {
            Chat.views.message.setMessage("<div class='system'>Chat has been terminated & refunded by the reader</div> ");
        },
        
        close_window_message: function(timeout_var) {

            Chat.views.message.setMessage("<div class='system'>Your session is now over - thank you for choosing Psychic Contact.</div>");
            Chat.views.message.setMessage("<div class='system'>This chat window will close in " + (timeout_var/1000) + " seconds...</div>");    
        },
        
        set_timer: function(time_balance) {            
            if (Chat.manager.room.tier == 'free')
            {
                $('#timerSpanFree').html(Chat.views.message.timerFormat(time_balance));
            }
            else
            {
                $('#timerSpan').html(Chat.views.message.timerFormat(time_balance));
            }
        },
        set_free_timer: function(time_balance) {
            $('#timerSpanFree').html(Chat.views.message.timerFormat(time_balance));
        },
        
        times_up_pause: function() {
            
        },
        
        disable_chat_form: function(){
            $('#chatForm textarea').attr('disabled','disabled');
            $('#chatForm input').attr('disabled','disabled');
            $('#input_message').attr("disabled", "disabled");
        },
        enable_chat_form: function()
        {
            $('#chatForm textarea').removeAttr('disabled');
            $('#chatForm input').removeAttr('disabled');
            $('#input_message').removeAttr('disabled');
        },
        show_add_stored_time: function(toggle)
        {
            if(toggle == true) {
                $("#addStoredTime").show();
            }else{
                $("#addStoredTime").hide();
            }
        },
        set_stored_time: function(minute) {
            console.log("Minute "  + minute);
            var stored_minute = Math.floor(minute);
            $('#maxAddStoredTime').html(stored_minute);
            /*
            $('#remainingStoredTime').html(minute);

            $('.add_stored_time').val(0);
            */
           if (Chat.is_mobile) {
               $( "#remainingStoredTimeInput" ).val(0);
           } else {
               /*
               var slider = $( "#remaining_stored_time-slider-range-max" ).slider({
                    range: "max",
                    min: 0,
                    max: stored_minute,
                    value: 1,
                    slide: function( event, ui ) {
                        $( "#remainingStoredTimeInput" ).val( ui.value );
                    }
                });
                */
               $( "#sel_remaining_stored_time" ).change(function(){
                   $( "#remainingStoredTimeInput" ).val( $( "#sel_remaining_stored_time" ).val() );
               });
               
                $( "#remainingStoredTimeInput" ).val( $( "#sel_remaining_stored_time" ).val() );
                
                $('#remainingStoredTimeInput').change(function() {
                    
                    var value = $('#remainingStoredTimeInput').val();
                    console.log("value is " + value );
                    if (!isFinite(value)) {
                        value = 1;
                        $('#remainingStoredTimeInput').val(value);
                    }
                    if (value < 0) {
                        value = Math.abs(value);
                        $('#remainingStoredTimeInput').val(value);
                    }
                    
                    slider.slider( "value", value );
                });
           }
           
        },
        
        get_stored_time: function(minute) {      
            // norman
            //Chat.views.message.setMessage("<div class='system'>Client is going to add more time from the remaining <span style='font-style: italic;'>"+ minute+ "</span> minutes stored time</div>");
            clearInterval(myinterval);
            Chat.views.message.setMessage("<div class='system'>Client is adding additional time to this chat</div>");

        },
        show_lost_time: function(toggle)
        {
            Chat.views.message.setMessage("<div class='system red' style='color: #3e903e;'>**Reader used Add Lost Time**</div>");
            Chat.views.message.setMessage("<div class='system'>ADD LOST was used and that FULL time has been restored</div>");
            Chat.views.message.setMessage("<div class='system'>FULL TIME RESTORED/NO CHARGES</div>");   
            if(toggle == true)
            {
                $("#lostTime").show();
            }
            else
            {
                $("#lostTime").hide();
            }
        },
        
        show_add_free_time: function(toggle) {
            if(toggle == true) {
                $("#addFreeTimeSection").show();
            } else{
                $("#addFreeTimeSection").hide();
            }
        }

    },
    
    ui: {
        adjust_input_message_textarea: function () {
            console.log("Calling adjust_input_message_textarea");
            if (Chat.is_mobile) {
                setTimeout(function() {
                    console.log("Adjusting");
                    $('.chat_option').css({"float":"left"});
                    $('#chat_window').css({"height":"190px"});
                    $('#input_message').css({"width": "95%", "margin-bottom":"5px", "font-size":"10px"});
                }, 500);
            }
            
        }
    },
    
    hideMenu: function(){
        $('#chatMenu').hide();
    },
    
    scrollToTop: function() {
        $('#chat_window').scrollTop($('#messageContainer').height());
    },
    
    setMessage: function(message) {

        if(parseInt(info_data.client_id) != 1) {

            $('#messageContainer').append(message);

            if(message.indexOf('system') >=0 ) {

                Chat.controller.room.actions.logMessage({room_name: Chat.manager.room.chat_session_id, message: message, member_id: Chat.models.member.member_id, site_url: Chat.models.member.site_url});
            }

            Chat.views.message.scrollToTop();
        }
    },

    setInputMessage: function(message) {

        $('#messageContainer').append(message);

        if(message.indexOf('system') >=0 ) {

            Chat.controller.room.actions.logMessage({room_name: Chat.manager.room.chat_session_id, message: message, member_id: Chat.models.member.member_id, site_url: Chat.models.member.site_url});
        }

        Chat.views.message.scrollToTop();
    },
    
    timerFormat: function(time_balance) {
        var sec_num = parseInt(time_balance, 10); // don't forget the second parm
        var hours   = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {minutes = "0"+minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}
        var time    = hours+':'+minutes+':'+seconds;
        
        return time;
    }
    
};
Chat.views.error_log = {
    logs: [],
    
    show: function(err) {
        this.logs.push(err);
    },
    
    log: function(err) {
        this.logs.push(err);
    }
};
Chat.views.popup = {
    
    chat_interface_win: null,
    chat_interface_win_preview: false,
    purchase_more_win: false,
    
    sound: {
        sound_handler: null,
        sound_period: 1000,
        
        play_sound: function() {
            if (Chat.browser.name == "MSIE") {
                Chat.views.popup.sound.sound_handler = setInterval(function() {
                    var sound = document.getElementById('beep');
                    if (sound) {
                        sound.Play();
                    }
                    
                }, Chat.views.popup.sound.sound_period);
            } else {
                ion.sound.play("door_bell", {
                volume: 0.5,
                loop: 20
                });
            }
            
        },
        stop_sound: function(){
            if (Chat.browser.name == "MSIE") {
                clearInterval(Chat.views.popup.sound.sound_handler);
            } else {
                ion.sound.stop("door_bell");
            }
        },
        play_message_sound: function() {
            if (Chat.browser.name == "MSIE") {
                Chat.views.popup.sound.sound_handler = setInterval(function() {
                    
                    if ( $("#choosen_sound").val() == "message_alert" )
                    {                        
                        let sound = document.getElementById('beep2');
                    }
                    else if( $("#choosen_sound").val() == "clear_minimal" )
                    {
                        let sound = document.getElementById('beep3');
                    }
                    else if( $("#choosen_sound").val() == "chord" )
                    {
                        let sound = document.getElementById('beep4');
                    }
                    else if( $("#choosen_sound").val() == "chat_alert" )
                    {
                        let sound = document.getElementById('beep5');
                    }
                    
                    if (sound) {
                        sound.Play();
                    }
                    
                }, Chat.views.popup.sound.sound_period);
            } else {
                var sound_type = $("#choosen_sound").val();
                ion.sound.play(sound_type, {
                volume: 1,
                loop: 20
                });
            }
            
        },

        play_end_sound: function() {

                if (Chat.browser.name == "MSIE") {
                    Chat.views.popup.sound.sound_handler = setInterval(function() {
                                
                        var sound = document.getElementById('beep7');
                        
                        if (sound) {
                            sound.Play();
                        }
                        
                    }, Chat.views.popup.sound.sound_period);

            } else {

                ion.sound.play("end_chat", {
                volume: 1,
                loop: 1
                });

            }
            
        },
        stop_message_sound: function(){
            if (Chat.browser.name == "MSIE") {
                clearInterval(Chat.views.popup.sound.sound_handler);
            } else {
                var sound_type = $("#choosen_sound").val();
                ion.sound.stop(sound_type);
            }
        }
    },
    
    open_site_page: function(page_name) {
        var page_url = "/";
        if (page_name) {
            page_url += page_name;  
        } 
        window.open(page_url, "_blank");
    },
    
    preview_open_window: function() {
        var w = 900; 
        var h = 750;
        var LeftPosition = (screen.width)?(screen.width-w)/2:100;
        var TopPosition=(screen.height)?(screen.height-h)/2:100;
        var settings='width='+w+',height='+h+',top='+TopPosition+',left='+LeftPosition+',scrollbars=yes,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=yes';
        Chat.views.popup.chat_interface_win = window.open('/chat/loading.html','PsyConChatWindow',settings);
        if (Chat.views.popup.chat_interface_win) {
            Chat.views.popup.chat_interface_win_preview = true;
        } else {
            alert('Please allow popup from our website by changing the browser setting. ');
        }
    },
    
    after_open_window: function() {
        Chat.views.popup.sound.stop_sound();
        Chat.views.popup.remove_modal();
        
    },
    
    open_window: function(chat_id, chat_session_id){
        Chat.views.popup.after_open_window();
        
        var w = 900; 
        var h = 750;
        /*
        if ($.browser.safari || $.browser.mozilla) {
            h = 700;
        }
        */
        
        var LeftPosition = (screen.width)?(screen.width-w)/2:100;
        var TopPosition=(screen.height)?(screen.height-h)/2:100;
        var settings='width='+w+',height='+h+',top='+TopPosition+',left='+LeftPosition+',scrollbars=yes,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=yes';
        
        if (Chat.views.popup.chat_interface_win_preview && Chat.views.popup.chat_interface_win) {
            Chat.views.popup.chat_interface_win.location = '/chat/chatInterface/index/' + chat_id + "/" + chat_session_id;
        } else {
            Chat.views.popup.chat_interface_win = window.open('/chat/chatInterface/index/' + chat_id + "/" + chat_session_id,'PsyConChatWindow',settings);
            if (!Chat.views.popup.chat_interface_win) {
                alert('Please allow popup from our website by changing the browser setting. ');
            }
        } 
        Chat.views.popup.chat_interface_win_preview = false;
        // refresh the page after 2 seconds
        setTimeout(function() {
            location.reload();
        }, 2000);
        
    },
    
    preview_open_purchase_more_time_window: function() {
        var w = 900; 
        var h = 750;
        var LeftPosition = (screen.width)?(screen.width-w)/2:100;
        var TopPosition=(screen.height)?(screen.height-h)/2:100;
        var settings='width='+w+',height='+h+',top='+TopPosition+',left='+LeftPosition+',scrollbars=yes,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=yes';
        Chat.views.popup.purchase_more_win = window.open('/chat/loading.html','PurchaseMoreTimeWindow',settings);
        if (Chat.views.popup.purchase_more_win) {
            Chat.views.popup.purchase_more_win_preview = true;
        } else {
            alert('Please allow popup from our website by changing the browser setting. ');
        }
        
    },
    
    open_purchase_more_time_window: function() {
        var w = 900; 
        var h = 750;
        /*
        if ($.browser.safari) {
            h = 700;
        }
          */      
        var LeftPosition = (screen.width)?(screen.width-w)/2:100;
        var TopPosition=(screen.height)?(screen.height-h)/2:100;
        var settings='width='+w+',height='+h+',top='+TopPosition+',left='+LeftPosition+',scrollbars=yes,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=yes';
        
        if (Chat.views.popup.chat_interface_win_preview && Chat.views.popup.chat_interface_win) {
            Chat.views.popup.purchase_more_win.location = '/chat/main/purchase_time/' + Chat.manager.room.reader_info.username;
        } else {
            Chat.views.popup.purchase_more_win = window.open('/chat/main/purchase_time/' + Chat.manager.room.reader_info.username ,'PurchaseMoreTimeWindow',settings);
            if (!Chat.views.popup.purchase_more_win) {
                alert('Please allow popup from our website by changing the browser setting. ');
            }
        }
        Chat.views.popup.purchase_more_win_preview = false;
    },
    
    resize_chat: function()
    {
        var windowHeight = $(window).height() - $('#navbar').outerHeight() - $('#footer').outerHeight() - 115;
        $('#chat_window').height(windowHeight);
        $("#footer textarea").width($(window).width() - 200);
    },
    
    remove_modal: function() {
        $('#modal').remove();
        $('#modal_bg').remove();
    }
};
Chat.controller.room = {
    
    previous_char: '',
    
    start_timer: function() {
        
        
    },
    
    pause_timer: function() {
        
        
    },
    
    send_message: function() {
        
        
    },
    
    detect_typing: function() {
        
    },
    
    endChat: function(){
        Chat.views.message.hideMenu();
        // chat has a session
        if(Chat.manager.room.chat_session_id){
            if(Chat.member_type=='reader'){
                // do something 
            }
        }
    },

    showLostTime: function(toggle)
    {
        if(toggle == true)
        {
            $("#lostTime").show();
        }
        else
        {
            $("#lostTime").hide();
        }
    },


    //--- Close Chat Window
    close_window: function (timeout_var){
        timeout_var = timeout_var || 10000;

        Chat.views.message.system.clear_typing();
        Chat.views.message.system.disable_chat_form();
        Chat.controller.room.hide_all_user_actions();
        // just show a closing message
        Chat.views.message.system.close_window_message(timeout_var);
        //timeout_var = timeout_var || window_close_time;
        //objectWrite({ type : 'message', 'className' : 'system', 'message' : "This chat window will close in " + (timeout_var/1000) + " seconds..." });

        setTimeout(function(){
            //window.open('','_self').close();
            // norman
            if (window.location != window.parent.location)
            {
                window.top.location.href = 'https://www.psychic-journey.com/chat-psychics';
            }
            else
            {
                //window.open('','_self').close();
                $('#modalchatEnd').modal('show');
                $('#modalchatEnd').on('hidden.bs.modal', function () {
                    window.open('','_self').close();
                });
            }
            //window.close();
        }, timeout_var);

    },
    
    actions: {

        logMessage:   function (data) {

            console.log('data ===========================================>'+data.room_name+','+data.message+','+data.member_id+','+data.site_url);
            var room_name = data.room_name;
            var input_data = {chat_session_id:room_name, message:data.message, member_id:data.member_id};
            console.log("chat session id is" + input_data.chat_session_id);
            console.log("Input data is "+ data.message);
            
              var posting = $.post( "/chat/chatInterface/log_message", input_data );
            
              posting.done(function( data ) {
                
                console.log(data);
              });
        },
        
        on_message_send: function() {
            $("#chatForm_send").on('click touchstart',function()
            {
                // e.preventDefault();
                Chat.controller.room.actions.message_send();
            });
            
            $(document).on("keypress", "#input_message", function(e) {
                if(e.which == 13 && !e.shiftKey) {
                    
                    // enter
                    var message = $("#input_message");
                    //var send_message = $.trim(message.val());
                    var send_message = $.trim(message.html());

                    //if (send_message.length >= 1 ) {
                    if ($.trim(send_message) != '' ) {
                        Chat.controller.room.actions.message_send();
                    }
                    
                    setTimeout(function(){
                        message.html("");
                    }, 200);
                } else {
                    let message = $("#input_message");
                    //var send_message = $.trim(message.val());
                    send_message = $.trim(message.html());
                
                    if (send_message == '') {
                        Chat.controller.room.previous_char = '';
                        // nothing else happen
                    } else {
                        Chat.manager.room.send.typing();
                        /*if (send_message != Chat.controller.room.previous_char) {
                            // yes. typing
                            Chat.manager.room.send.typing();
                            Chat.controller.room.previous_char = send_message;
                        }*/
                    } 
                }
            });
        
        },
        
        message_send: function() {
            var message = $("#input_message");

            var send_message = $.trim(message.html());
            send_message = send_message.replace(/&nbsp;/gi,'');
            send_message = send_message.replace(/(<br>\s*)+$/, '');
            if(send_message && send_message.indexOf('<img') >= 0) {
                
                message.html("");
                Chat.manager.room.send.message({message:send_message});
                duration_no_action = 0;
            }
            /*
            * word block function.
            * */
            if(send_message && send_message.indexOf('<img') < 0)
            {
                send_message = $.trim(message.html());
                send_message = send_message.replace(/&nbsp;/gi,'');
                send_message = send_message.replace(/(<br>\s*)+$/, '');
                console.log('send message===>>>',send_message)

                $.each(block_words_array, function (index, item) {
                    block_words_array[index] = item.toLowerCase();
                });

                for(let j=0; j<block_words_array.length; j++) {

                    let split_send_message = send_message.split(' ');
                    let split_block_word = block_words_array[j].split(' ');

                    for(let k=0; k<split_send_message.length; k++) {

                        for(let n=0; n<split_block_word.length; n++) {

                            if (split_block_word[n] === split_send_message[k].toLowerCase().replace(/[^a-zA-Z ]/g, "")) {
                                // let replace_string = new Array(split_send_message[k].length + 1).join("*");
                                let replace_string = split_send_message[k].replace(/[a-zA-Z]/g, "*");
                                send_message = send_message.replace(split_send_message[k], replace_string);

                            } else {

                                let send_message_temp = split_send_message[k].toLowerCase().replace(/[^a-zA-Z ]/g, " ").split(" ");

                                for(let a=0; a<send_message_temp.length; a++) {

                                    if (split_block_word[n] === send_message_temp[a].toLowerCase()) {
                                        // let replace_string = new Array(split_send_message[k].length + 1).join("*");
                                        let replace_string = send_message_temp[a].replace(/[a-zA-Z]/g, "*");
                                        send_message = send_message.replace(send_message_temp[a], replace_string);
                                    }
                                }

                            }


                        }
                    }
                }

                var word_array = send_message.split(' ');
                var phoneno_1 = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
                var phoneno_2 = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;

                var email_1 = /(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;

                if(parseInt(info_data.client_id) != 1) {

                    for(var j=0; j<word_array.length; j++) {

                        if (word_array[j].match(phoneno_1) || word_array[j].match(phoneno_2)) {

                            let block_word = word_array[j];
                            let split_array = block_word.split(/[\s-.]+/);
                            for (var k = 0; k < split_array.length; k++) {

                                var replace_string = new Array(split_array[k].length + 1).join("*");
                                send_message = send_message.replace(split_array[k], replace_string);
                            }

                        }

                        if (word_array[j].match(email_1)) {

                            var block_word = word_array[j];
                            var split_array = block_word.split('@');
                            for (var a = 0; a < split_array.length; a++) {

                                let replace_string = new Array(split_array[a].length + 1).join("*");
                                send_message = send_message.replace(split_array[a], replace_string);
                            }

                        }

                        if (word_array[j].includes('net') == true) {

                            let block_word = word_array[j];
                            let split_array = block_word.split('net');

                            for (var ll = 0; ll < split_array.length; ll++) {

                                let replace_string = new Array(split_array[ll].length + 1).join("*");
                                send_message = send_message.replace(split_array[ll], replace_string);
                            }
                        }

                        if (word_array[j].includes('.net')) {

                            let block_word = word_array[j];
                            let split_array = block_word.split('.net');

                            for (var lll = 0; lll < split_array.length; lll++) {

                                let replace_string = new Array(split_array[lll].length + 1).join("*");
                                send_message = send_message.replace(split_array[lll], replace_string);
                            }
                        }

                        if (word_array[j].includes('com')) {

                            let block_word = word_array[j];
                            let split_array = block_word.split('com');

                            for (var n = 0; n < split_array.length; n++) {

                                let replace_string = new Array(split_array[n].length + 1).join("*");
                                send_message = send_message.replace(split_array[n], replace_string);
                            }
                        }

                        if (word_array[j].includes('.com')) {

                            let block_word = word_array[j];
                            let split_array = block_word.split('.com');

                            for (var b = 0; b < split_array.length; b++) {

                                let replace_string = new Array(split_array[b].length + 1).join("*");
                                send_message = send_message.replace(split_array[b], replace_string);
                            }
                        }
                    }
                }
                //message.val(""); 
                message.html("");
                Chat.manager.room.send.message({message:send_message});
                duration_no_action = 0;
            }

            setTimeout(function(){
                message.html("");
                $("#input_message_preview").html("");
                var file = document.getElementById("imgInp");
                file.value = file.defaultValue;
            }, 200);
        }
    },
    
    hide_all_user_actions: function() {
        $(".pauseChatAnchor").hide();
        $(".resumeChatAnchor").hide();        
        //$(".endChatAnchor").hide();
        
        if (Chat.models.member.member_type == 'client') {
            //$(".purchaseMoreTime").hide();
            $(".addStoredTime").hide();
            $(".contactAdmin").hide();
        } else {
            $(".personalBanUserAnchor").hide();
            $(".fullBanUserAnchor").hide();
            
            $(".refundChatAnchor").hide();
            $(".addLostTime").hide();
            $("#lostTimeBtn").hide();
        }
    },
    
    user_actions: function() {
        $(".pauseChatAnchor").click(function(){            
            /*8-26*/        
            // Chat.manager.room.send.pause();
            Chat.manager.room.send.standby();
            /*8-26*/
            Chat.views.message.setMessage("<div class='system red' style='color: #3e903e;'>**Stand by is ON (The client does not pay for that period)**</div>"); 
            clearInterval(myinterval);
        });
        
        $(".resumeChatAnchor").click(function(){
            $('#watchStandbyoff').val(1);
            Chat.manager.room.send.resume();
            /*9-27*/
            startTimer();
            if(Chat.models.member.member_type == 'reader') {
                clearInterval(standbyinterval);
            }
        });
        /*9-29
        * remove stand by mode at the beginning of chat
        * */
        $(document).on("click", ".btnYesStandBy", function(){
            Chat.manager.room.send.resume();
            /*9-27*/
            startTimer();
            if(Chat.models.member.member_type == 'reader') {
                clearInterval(standbyinterval);
            }
        });

        $(document).on('click', ".btnYesAddedFreeTime", function (e) {
            if(used_addfreetime == 0) {
                Chat.views.message.client.click_yes_free_time();
            }

            var watchTimebalance1 = setInterval(function(){
                console.log('Current time balance is ', Chat.manager.room.time_balance);
                if(Chat.manager.room.time_balance == 0) {
                    console.log('Continue Free Chat time Started');
                    clearInterval(watchTimebalance1);

                    $("#selectAddedFreeTime").val(0);

                    Chat.manager.room.send.continueFreeTime();
                    setTimeout(function () {
                        used_addfreetime = 1;
                    }, 1000);

                }
            }, 1000);
        });

        $(document).on("click", ".btnYesFreeTime", function(e){
            /*9-18
            * client confirm to use the gift from the current chatting reader.
            * */
            confirmed_to_use_freetime = 1;
            console.log('Left Time ===>> ',Chat.manager.room.time_balance);
            if(used_freetime == 0) {
                console.log('<-------------Use Free Time Button Click!----------->');

                used_freetime = 1;
                Chat.views.message.client.click_yes_free_time();
                var watchTimebalance = setInterval(function(){
                    console.log('Current time balance is ', Chat.manager.room.time_balance);
                    if(Chat.manager.room.time_balance == 0) {

                        setTimeout(function() {

                            $('#freetimeadded').val(1);

                            if($('#gift_used').val() > 0) {

                                start_giveback();
                            } else {

                                $("#btnYesPaused").val(1);
                                $("#warningFreeTime").val(0);
                                Chat.manager.room.send.pauseFreeTime();
                            }

                            clearInterval(watchTimebalance);
                        }, 1000);

                    }
                }, 1000);

            } else {

                e.preventDefault()
            }


        });
        
        $(document).on("click", ".btnNoFreeTime", function(){
            confirmed_to_use_freetime = 1;
            if(used_freetime == 0) {

                Chat.views.message.client.click_no_free_time();
            }
            var watchTimebalance2 = setInterval(function(){
                console.log('Current time balance is ', Chat.manager.room.time_balance);
                if(Chat.manager.room.time_balance == 0) {
                        // just end it.
                    clearInterval(watchTimebalance2);
                    clearInterval(detector);
                    Chat.manager.room.send.leave_chat();
                }
            }, 1000);
            // $("#btnYesPaused").val(1);
            // $("#warningFreeTime").val(0);
            // Chat.manager.room.send.resume();
        });

        $(document).on("click", ".btnNoAddedFreeTime", function(){
            if(used_addfreetime == 0) {

                Chat.views.message.client.click_no_free_time();
            }
            var watchTimebalance3 = setInterval(function(){
                console.log('Current time balance is ', Chat.manager.room.time_balance);
                if(Chat.manager.room.time_balance == 0) {
                    clearInterval(watchTimebalance3);
                    // just end it.
                    clearInterval(detector);
                    Chat.manager.room.send.leave_chat();


                }
            }, 1000);
        });

        $(".endChatAnchor").click(function() {
            if(confirm("Are you sure you want to end this session?")) {
                clearInterval(detector);
                Chat.manager.room.send.leave_chat();
            }
        });

        $(".purchaseMoreTime").click(function() {
            if(confirm("You are about to pause this chat to purchase more time. Are you sure you want to purchase more time?")){
                Chat.manager.room.send.purchase_time();
            }
        });
        
        $(".addStoredTime").click(function() {
            if(confirm("Doing this will pause the chat, do you want to continue?")){
                //Chat.manager.room.send.add_stored_time();
                $("#addtime_mode").val("1");
                clearInterval(myinterval);
                $("#stopWatch").val("1");
                Chat.manager.room.send.get_stored_time();
            }
        });
        
        $("#addStoredTimeBtn").click(function() {
            $("#stopWatch").val('0');
            var stored_time = $("#sel_remaining_stored_time").val();
            Chat.views.message.setMessage("<div class='system red' style='color: #3e903e;'>**You have added <span style='font-style: italic;'>"+ stored_time + "</span> minutes from their stored time**</div>");
            Chat.manager.room.send.add_stored_time();
        });
        
        $("#cancelAddStoredTimeBtn").click(function() {
            $("#stopWatch").val('0');
            Chat.views.message.system.show_add_stored_time(false);
            Chat.manager.room.send.resume();
        });
        
        $("#purchaseMoreTimeBtn").click(function() {
            if(confirm("You are about to purchase more time. Would you like to continue?")){
                $("#addStoredTime").hide();
                Chat.manager.room.send.purchase_time();
                // Check time balance
                check_update_from_paypal();
            }
        });
        
        $("#refreshPurchasedTimeBtn").click(function() {
            Chat.manager.room.send.refresh_stored_time();
        });
        
        $(".addFreeTime").click(function() {
            Chat.views.message.system.show_add_free_time(true);
        });
        
        $(".addFreeEmail").click(function(){
            if (confirm("Are you sure you want to give FREE email reading gift?"))
            {
                Chat.manager.room.send.add_free_email();
            }
        });
        
        $("#addFreeTimeBtn").click(function() {
            Chat.manager.room.send.add_free_time();
        });
        
        $("#cancelAddFreeTimeBtn").click(function() {
            $('#freeTimeInput').val(0);
            Chat.views.message.system.show_add_free_time(false);
        });
        

        $(".personalBanUserAnchor").click(function() {
            if(confirm('Ban this client? Are you sure?')){
                Chat.manager.room.send.pban();
            }
        });
        
        $(".fullBanUserAnchor").click(function() {
            if(confirm('Ban this client? Are you sure?')) {
                Chat.manager.room.send.fban();
            }
        });
        
        $(".refundChatAnchor").click(function() {
            if(confirm('Are you sure you want to return entire chat time back to the client and end this chat?')) {
                Chat.manager.room.send.refund();
            }
        });
        $(".addLostTime").click(function() {
            // just show the add lost time
            // set to pause first by reader
            //Chat.views.message.system.show_lost_time(true);
            //Chat.manager.room.send.pause();
            if (confirm('End the chat without Pay. Are you sure you want to end the session with add lost time?')) {
                Chat.manager.room.send.lost_time(Chat.manager.room.chat_length);
            }
            //Chat.manager.room.send.resume();
        });
        $(".removeLostTime").click(function() {
            // just show the add lost time
            // set to pause first by reader
            Chat.views.message.system.show_lost_time(false);
            Chat.manager.room.send.resume();
        });
        $("#lostTimeBtn").click(function(){
            if($.isNumeric($("input[name=losttime]").val()))
            {
                var lost_time = parseInt($("input[name=losttime]").val(), 10);
                
                if (lost_time < 0) {
                    alert('Lost time cannot be zero nor negative number');
                }
                
                if (lost_time * 60 >= Chat.manager.room.max_chat_length || lost_time*60 > Chat.manager.room.chat_length) {
                    alert('Lost time cannot exceed max chat time or chat time');
                } else {
                    Chat.manager.room.send.lost_time(lost_time);
                }           
            }
            else
            {
                alert("Must Be Valid Number");
            }
        });
        
        $(".contactAdmin").click(function() {
            Chat.manager.room.send.contact_admin(); 
        });
        //david_v2
        $('#startVchat').click(function(){
            who = 'me';         
            Chat.manager.room.send.vcall_request();
        });
        
        $('.acceptBtn').click(function(){
            Chat.manager.room.send.vcall_accept();
        });
        $('.rejectBtn').on('click', function(){         
            Chat.manager.room.send.vcall_reject();
        });
        //\ End david_v2
            
    },
    
    init: function() {

        if (performance.navigation.type == 1) {
            console.info( "This page is reloaded" );
            alert('page is reloaded')
        } else {
            console.info( "This page is not reloaded");

            // Chat.manager.room.timer.end_chat_timer();
            // Chat.views.message.system.leave_chat_room(data.member_type);
        }

        //detect chat delete by admin
        if(Chat.models.member.member_type == 'reader') {

            var detect_chat_session = setInterval(function () {
                $.ajax({
                    'type': "POST",
                    'url': "/chat/chatInterface/get_chat_ended",
                    'data': {chat_session_id: info_data.chat_session_id},
                    success: function (data) {
                        var obj = JSON.parse(data);
                        var ended = obj.data;
                         if(parseInt(ended) === 1) {
                            Chat.manager.room.send.leave_chat();
                            clearInterval(detect_chat_session);
                        }
                    }
                });
            }, 1000);
        }

        // get all warning messages 10-16
        var warning_key_array = [];
        var warning_array = [];

        $.ajax({
            'type': "POST",
            'url': "/chat/chatInterface/get_warning_messages",
            success: function (data) {

                var obj = JSON.parse(data);
                console.log('warning messages');
                warning_array = obj.data;
                for(var i=0; i<warning_array.length; i++) {
                    warning_key_array.push(warning_array[i]['key_word']);
                }
            }
        });

        /*9-17*/
        if(parseInt(info_data.client_id) == 1) {

            $("ul#chatMenu li").css('display', 'none');
            $("li.chat_end.lh-30").css({'display':'block', 'float':'right'});
        }

        /*9-13
        * if client used a gift, save the log in DB
        * */
        function save_applied_client(client_id, gift_id) {

            $.ajax({
                'type': "POST",
                'url': "/chat/chatInterface/save_applied_client",
                'data': {reader_id: info_data.reader_id,  client_id : client_id, gift_size: $("#client-gift-size").val(), gift_id: gift_id},
                success: function (data) {

                }
            });
        }
        /*
        * check if client has any gift from current chatting reader on this day - client side
        * */
        if(Chat.models.member.member_type == 'client') {

            let today = new Date();
            let week_day_today = today.getDay();
            var gift_id;

            $.ajax({
                'type': "POST",
                'url': "/chat/chatInterface/get_give_back_info",
                'data': { reader_id : info_data.reader_id, client_id: info_data.client_id, week_day: week_day_today},
                success: function (data) {

                    var obj = JSON.parse(data);
                    var give_back_info = obj.data;
                    var gift_size = give_back_info.gift_size;
                    gift_id = give_back_info.id;

                    $.ajax({
                        'type': "POST",
                        'url': "/chat/chatInterface/get_pause_timer_data",
                        'data': { 'id': info_data.reader_id },
                        success: function (response) {
                            var obj_2 = JSON.parse(response);
                            var data = obj_2.data;
                            let pause_timer_flag = data.pause_timer_flag;

                            if(pause_timer_flag == 1) {

                                $('#client-gift-size').val(gift_size);
                            }
                        }
                    });
                }
            });
        }
        /*9-16
        * check if client has any gift from current reader on this day - reader side
        * if yes, add gift's size into add free time input box.
        * */
        if(Chat.models.member.member_type == 'reader') {

            let today = new Date();
            let week_day_today = today.getDay();

            $.ajax({
                'type': "POST",
                'url': "/chat/chatInterface/get_give_back_info",
                'data': { reader_id : info_data.member_id, client_id: info_data.client_id, week_day: week_day_today},
                success: function (data) {

                    var obj = JSON.parse(data);
                    var give_back_info = obj.data;
                    var gift_size = give_back_info.gift_size;

                    $.ajax({
                        'type': "POST",
                        'url': "/chat/chatInterface/get_pause_timer_data",
                        'data': { 'id': info_data.member_id },
                        success: function (response) {
                            var obj_2 = JSON.parse(response);
                            var data = obj_2.data;
                            let pause_timer_flag = data.pause_timer_flag;

                            if(pause_timer_flag == 1) {

                                $('#freeTimeInput').val(gift_size);
                            }
                        }
                    });
                }
            });
        }

        /*9-18
        * admin interrupt message detector
        * */
        if (Chat.models.member.member_type == 'client') {
            setInterval(function () {

                $.ajax({
                    'type': "POST",
                    'url': "/chat/chatInterface/get_client_admin_interrupt_message",
                    'data': {chat_session_id: info_data.chat_session_id},
                    success: function (data) {

                        var obj = JSON.parse(data);
                        var messages = obj.admin_interrupt_messages;

                        for(var i=0; i<messages.length; i++) {

                            Lobibox.notify('warning', {
                                title:'Admin Message',
                                showClass: 'fadeInDown',
                                hideClass: 'fadeUpDown',
                                delay:30000,
                                msg: messages[i].message
                            });
                        }
                    }
                });
            }, 2000);
        }

        if (Chat.models.member.member_type == 'reader') {
            setInterval(function () {

                $.ajax({
                    'type': "POST",
                    'url': "/chat/chatInterface/get_reader_admin_interrupt_message",
                    'data': {chat_session_id: info_data.chat_session_id},
                    success: function (data) {

                        var obj = JSON.parse(data);
                        var messages = obj.admin_interrupt_messages;

                        for(var i=0; i<messages.length; i++) {

                            Lobibox.notify('warning', {
                                title:'Admin Message',
                                showClass: 'fadeInDown',
                                hideClass: 'fadeUpDown',
                                delay:30000,
                                msg: messages[i]['message']
                            });
                        }
                    }
                });
            }, 2000)
        }

        /*9-20
        * check if there are any clients in "Wait In Line" for this reader.
        * */
        if (Chat.models.member.member_type == 'reader') {
            var messageText;
            var number_wait_in_line_clients;
            var waitInLineTimer = setInterval(function () {

                $.ajax({

                    url: '/chat/main/get_wait_in_line_clients/' + info_data.member_id,
                    type: 'GET',

                }).done(function (response) {

                    var obj = JSON.parse(response);
                    var data = obj['wait_in_line_clients'];

                    number_wait_in_line_clients = data.length;
                    if(number_wait_in_line_clients > 0) {

                        messageText = data[0]['client_user_name'];
                        messageText = messageText + ' is waiting in line';
                        messageText = messageText + ' +';
                        messageText = messageText + data.length - 1;
                        messageText = messageText + ' other';
                    }

                }).fail(function (error) {
                    console.error(error);
                });

                setTimeout(function () {

                    if (number_wait_in_line_clients > 0) {

                        Lobibox.notify('success', {
                            title: 'Wait In Line',
                            showClass: 'fadeInDown',
                            hideClass: 'fadeUpDown',
                            delay: 7000,
                            msg: messageText
                        });
                        // clearInterval(waitInLineTimer);
                    }
                }, 2000);

            }, 10000);
        }

        /*9-9
        * get all favorite clients and apply for current chatting client - Favorite/Un-favorite button
        * */
        if(Chat.models.member.member_type == 'reader') {

            $.ajax({
                'type': "POST",
                'url': "/chat/chatInterface/get_favorite_info",
                'data': { reader_id: info_data.member_id, client_id: info_data.client_id},
                success: function (data) {

                    obj = JSON.parse(data);
                    var favorite_check = parseInt(obj.data);

                    if(favorite_check == 0) {

                        $(".addFavorite").css('display','block');
                    }
                    if(favorite_check == 1) {

                        $(".addUnfavorite").css('display','block');
                    }
                }
            });

            $(".addFavorite").click(function() {

                $.ajax({
                    'type': "POST",
                    'url': "/chat/chatInterface/set_favorite",
                    'data': { reader_id: info_data.member_id, client_id: info_data.client_id},
                    success: function (data) {

                        $(".addFavorite").css('display','none');
                        $(".addUnfavorite").css('display','block');
                    }
                });
            });

            $(".addUnfavorite").click(function() {

                $.ajax({
                    'type': "POST",
                    'url': "/chat/chatInterface/set_unfavorite",
                    'data': { reader_id: info_data.member_id, client_id: info_data.client_id},
                    success: function (data) {

                        obj = JSON.parse(data);

                        $(".addFavorite").css('display','block');
                        $(".addUnfavorite").css('display','none');
                    }
                });
            });
        }

        /*9-16
        * run this function if client agrees to use the gift today, 5 sec before the chat room ends
        * */
        start_giveback = function(){
 
            if(Chat.models.member.member_type == 'client') {

                $("#btnYesPaused").val(1);
                $("#warningFreeTime").val(0);
                Chat.manager.room.send.pauseFreeTime();

                save_applied_client(info_data.client_id, gift_id);
            }

        }

        /*get all blocked words*/
        $.ajax({
            'type': "POST",
            'url': "/chat/chatInterface/get_block_words",
            
            success: function (data) {
                obj = JSON.parse(data);

                var array = obj['block_words'];
                
                for(var i=0; i<array.length; i++) {
                    block_words_array.push(obj['block_words'][i]['block_words']);
                }
            }
        });

        console.log("Block Words ====>>>>", block_words_array);
        // register
        console.log("Registering controller room actions");
        Chat.controller.room.actions.on_message_send();
        // set up actions
        Chat.controller.room.user_actions();

        //thom ho 7.31
        
        setInterval(function() {
            duration_session++;
        }, 1000);
        /*
        * no action over 1 min, automatically end the chat room
        * */
        if(parseInt(info_data.client_id) != 1) {
            //thom8-9
            startTimer = function() {
                duration_no_action = 0;
                myinterval = setInterval(function() {
                    if (duration_no_action > 600) {
                        duration_no_action = 0;
                        if(duration_session < 240) {
                            Chat.manager.room.send.refund();
                        }

                        clearInterval(myinterval);
                        clearInterval(detector);

                        setTimeout(function() {

                            Chat.manager.room.send.leave_chat();

                        }, 3000);
                    }
                    duration_no_action++;

                }, 1000);
            }

            /*9-27*/
            if(Chat.models.member.member_type == 'reader') {

                setTimeout(function () {
                    $('.pauseChatAnchor').click();
                }, 1500);
            }
            /*
            * detect the standby mode at the beginning of chat and continue for over 1 min.
            * */
            standbyinterval = setInterval(function () {
                if (stand_by_action_time > 60) {

                    if(Chat.models.member.member_type == 'reader') {

                        Chat.views.message.setMessage("<div class='system' style='color: #000000;'>WARNING : YOU ARE STILL IN STANDBY MODE!  Turn Off? &nbsp;&nbsp;<button class='btn btnYesStandBy' type='button'>YES</button>&nbsp;<button class='btn btnNoStandBy' type='button'>NO</button></div>");
                        clearInterval(standbyinterval);
                    }
                }
                stand_by_action_time++;
            }, 1000);

            // warning messages for illegal users 10-16
            if(Chat.models.member.member_type == 'reader') {

                var checked_warning = [];
                setInterval(function () {
                    $.ajax({
                        'type': "POST",
                        'url': "/chat/chatInterface/check_client_warning",
                        'data': { client_id: info_data.client_id},
                        success: function (data) {

                            obj = JSON.parse(data);
                            var warning_info = obj;

                            for(var key in warning_info) {
                                if(warning_info[key] == true && checked_warning.indexOf(key) < 0) {

                                    checked_warning.push(key);
                                    var warning_message = warning_array[warning_key_array.indexOf(key)]['description'];
                                    if(Chat.models.member.member_type == 'reader') {
                                        warning_message = warning_message + "&nbsp;&nbsp;&nbsp;&nbsp;Please hit FULL BAN and end current session."
                                    }
                                    Chat.views.message.setMessage("<div class='system warning-message' style='color: red; background: black'><p>"+warning_message+"</p></div>");
                                }
                            }
                        }
                    });
                }, 6000);
            }

            //=========8-9============
            //thom ho8-6
            detector = setInterval(function() {

                if (notify >0) {

                    if(Chat.models.member.member_type == 'reader') {
                        if(end_session == 0) {

                            Chat.manager.room.timer.end_chat_timer();
                        }

                        clearInterval(detector);
                        clearInterval(myinterval);

                    }
                    if(Chat.models.member.member_type == 'client') {
                        if(end_session == 0) {

                            Chat.manager.room.timer.end_chat_timer();
                        }

                        clearInterval(detector);
                        clearInterval(myinterval);

                    }

                }
            }, 2000);

            //thom ho8-6
            var chat_Id = Chat.manager.room.chat_id;

            if(Chat.models.member.member_type == 'reader') {
                var timer1 = setInterval(function() {

                    $.ajax({
                        'type': "POST",
                        'url': "/Main/get_chat_ended",
                        'data': { 'data': chat_Id },
                        success: function (data) {
                            obj = JSON.parse(data);

                            if (obj.data.ended == 1) {
                                notify = 1;
                                clearInterval(timer1);
                            }
                        }
                    });
                    
                }, 1000);
            }

            if(Chat.models.member.member_type == 'client') {

                var timer2 = setInterval(function() {

                    $.ajax({
                        'type': "POST",
                        'url': "/Main/get_chat_ended2",
                        'data': { 'data': chat_Id },
                        success: function (data) {
                            obj = JSON.parse(data);

                            if (obj.data.ended == 1) {

                                clearInterval(timer2);
                                notify = 1;
                            }
                        }
                    });
                    // Chat.manager.room.send.leave_chat();
                }, 1000);
            }

        }

    }
    
};
Chat.run = {

    test: function() {
        // test if any missing module.d
    },
    
    room: function() {
        console.log("Done");
        // Chat init.
        Chat.init(chat_init_data);
        // room.  join & send notification to reader in lobby
        // now emit "join"
        console.log("Send join new chat room");
        Chat.manager.room.send.join_new_chat_room();
        Chat.views.popup.resize_chat();
        Chat.views.message.ui.adjust_input_message_textarea();
    },
    
    lobby: function() {
        console.log("Done");
        // Chat init.
        Chat.init(chat_init_data);
        if(Chat.models.member.member_type == "reader") {
            Chat.controller.lobby.actions.register();
        }
    }
};