
requirejs.config({
    //By default load any module IDs from js/lib
    baseUrl: '/chat/app',
    
    paths: {
        'step': '/chat/app/step'
    },
    config: {
        step: {
            steps: [
                ['chat'],
                ['models/member', 'models/socket'],
                ['manager/room'],
                ['views/message', 'views/error_log', 'views/popup'],
                ['controller/room'],
                ['chat_run']
            ]
        }
    }
});



requirejs(['step!chat_run'], function(){
    console.log("Done");
    // Chat init.
    Chat.init(chat_init_data);
    // room.  join & send notification to reader in lobby
    // now emit "join"
    console.log("Send join new chat room");
    Chat.manager.room.send.join_new_chat_room();
    Chat.views.popup.resize_chat();
})

/*
requirejs.config({
    //By default load any module IDs from js/lib
    baseUrl: '/chat/test',
    
    paths: {
        jquery: "//code.jquery.com/jquery-1.11.0.min" // Local
    }
});



requirejs(['chat', 'chat_views', 'chat_test', 'chat_manager', 'chat_manager_room'], function(){
    console.log("Done");
    Chat.init({a:"aaa", b:"bbb"});
    Chat.test.testing();
    Chat.manager.call();
    Chat.manager.room.call();
})

/*
 * 
 * requirejs(['simple-vp'], function(simple){
    console.log("Done: " + simple.color);
    
})

requirejs(['chat', 'chat_views', 'chat_manager', 'chat_manager_room', 'chat_test'], function(Chat){
    console.log("Done");
    //Chat.init({a:"aaa", b:"bbb"});
})
/*
// Start the main app logic.
requirejs(['jquery', 'underscore-min'],
function   ($,        underscore) {
    //jQuery, canvas and the app/sub module are all
    //loaded and can be used here now.
    console.log("Done");
//    Chat.init({a:"aaa", b:"bbb"});
});
*/