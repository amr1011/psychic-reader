Chat.models.member = {
    
    member_id: '',      // member id
    member_id_hash: '', // member hash value for lobby
    member_hash: '',    // member hash value for chat room
    member_type: '',
    member_username: '',
    _disconnect_url: '',
    
    init: function(data) {
        this.member_id = data.member_id || '';
        this.member_id_hash = data.member_id_hash || '';
        this.member_hash = data.member_hash || '';
        this.member_type = data.member_type || '';
        this.member_username = data.member_username || '';
        this._disconnect_url = data._disconnect_url || '';
    }
};
