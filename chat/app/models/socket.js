
Chat.models.socket = {
    
    io: null,   // socket io
    socket_port:'',
    socket_url:'',
    
    init: function(data) {
        this.socket_url = data.socket_url || '';       // e.g.  'http://66.178.176.109'
        this.socket_port = data.socket_port || ''; 
        if (this.socket_url && this.socket_port) {
            console.log("Set socket: " + this.socket_url + ":"+this.socket_port);
            this.io = io.connect(this.socket_url + ":"+this.socket_port);
            console.log("Setted socket");
        }
    }
};
