Chat.views.message = {
    
    
    // show on reader window
    reader: {
        chat_attempt_for_reader: function (data) {
            Chat.views.error_log.log(data);
            $('#messageContainer').append("<div class='system'>A client has sent your a request for chat.</div>");    
            //$('body').append("<div id='modal'><div class='cont'><b>\""+data.client_username+"\" wants to chat with you.<br />Do you want to accept?</b><div style='margin:25px 0 0;' align='center'><a id='startChat' class='blue-button' onclick='Chat.views.popup.open_window(\""+data.chat_id+"\", \""+data.chat_session_id+"\")' ><span>Start Chatting</span></a> &nbsp; <a id='closeChat' class='blue-button' onclick='Chat.controller.lobby.actions.reject_chat_room(\""+data.chat_session_id+"\")'><span>Not At This Time</span></a></div></div></div><div id='modal_bg'></div>");
            $('body').append("<div id='modal'><div class='cont'><b>\""+data.client_username+"\" wants to chat with you.<br />Do you want to accept?</b><div style='margin:25px 0 0;' align='center'><a id='startChat' class='blue-button' target='_blank' onclick='Chat.controller.lobby.actions.accept_chat_room(\""+data.chat_session_id+"\")' ><span>Start Chatting</span></a> &nbsp; <a id='closeChat' class='blue-button' onclick='Chat.controller.lobby.actions.reject_chat_room(\""+data.chat_session_id+"\")'><span>Not At This Time</span></a></div></div></div><div id='modal_bg'></div>");
        
        
            var leftPos = ($(window).width()/2)-($('#modal').width()/2);
            var topPos = ($(window).height()/2)-($('#modal').height()/2);

            $('#modal').css('left',leftPos);
            $('#modal').css('top',topPos);

            // Play sound every 4 seconds
            Chat.views.popup.sound.play_sound();
            //Chat.views.popup.sound.soundIV = setInterval(Chat.views.popup.sound.play_sound, 2000);

        },
        
        reject_chat_room: function() {
        	Chat.views.popup.sound.stop_sound();
           	Chat.views.popup.remove_modal(); 
        },
        
        abort_chat: function() {
        	Chat.views.popup.sound.stop_sound();
        	Chat.views.popup.remove_modal();
        },
        
        show_contact_admin: function() {
            Chat.views.message.setMessage("<div class='system'>Client is contacting admin. The chat is paused. </div>");
        },
        
        notify_purchase_more_time: function() {
            Chat.views.message.setMessage("<div class='timer'>Notifying client to purcahse more time</div>");
        },
        purchase_more_time: function() {
            Chat.views.message.setMessage("<div class='timer'>The client is purchasing additional time and chat is still paused. Please wait until client back to chat. </div>");
        },
        
        add_stored_time: function() {
            Chat.views.message.setMessage("<div class='timer'>The client has paused the chat to add additional stored time. You may close this chat window. You will be notified when the user is ready to chat again.</div>");
        },
        
        add_free_time: function(total_free_time, free_time_added) {
            Chat.views.message.setMessage("<div class='timer'> You have added "+free_time_added +"minutes to this chat session for <span class='SpecialAlert'>FREE !</span>.  You have been rewarded <span class='SpecialAlert' >total: "+  total_free_time + " minutes </span> during this chat session.   </div>");
        },
        
        pban: function() {
            Chat.views.message.setMessage("<div class='timer'>"+ Chat.manager.room.client_info.username +" has been banned.</div>");
        },
        
        fban: function() {
            Chat.views.message.setMessage("<div class='timer'>"+ Chat.manager.room.client_info.username +" has been fully banned.</div>");
        },
        
        refund: function() {
            Chat.views.message.setMessage("<div class='system'>Return " + Chat.manager.room.client_info.username + "'s chat time to client. </div>");
        }
    },
    
    client: {
        
        join_new_chat_room: function () {
        	if (Chat.models.member.member_type == "client") {
            	Chat.views.message.setMessage("<div class='system'>The reader has been contacted & no minutes will be used or taken until the timer starts at the top of this screen.</div>");    
        		//Chat.views.message.setMessage("<div class='system'>You can <span id='chat_abort_room' class='SpecialAlert Pointer' onclick='Chat.manager.room.send.abort_chat'>abort this chat</span> before reader start</div>");
        	}
        },
        
        reject_chat: function(username) {
            Chat.views.message.setMessage("<div class='timer'>"+username+" is busy as this time.</div>");
        },
        
        abort_chat: function() {
        	Chat.views.message.setMessage("<div class='system'>Reader has no response.  The chat has been aborted.  Please try again later. </div>");
        },
        
        times_up_pause: function() {
            Chat.views.message.setMessage("<div class='system'>The chat session times up and is paused until additional time is purchased. </div>");
        },
        
        show_contact_admin: function() {
            Chat.views.message.setMessage("<div class='system'>Contact page is now open on new page.</div>");  
        },
        
        purchase_more_time: function() {
            Chat.views.message.setMessage("<div class='timer'>You can buy more time in the purchase page </div>");
        },
        
        add_stored_time: function() {
            Chat.views.message.setMessage("<div class='timer'>You will be redirected to the time add stored time page </div>");
        },
        
        add_free_time: function(total_free_time, free_time_added) {
            Chat.views.message.setMessage("<div class='timer'>"+ Chat.manager.room.reader_info.username  +"has added "+free_time_added +"minutes to this chat session for <span class='SpecialAlert'>FREE !</span>.  </div>");
        },
        
        pban: function() {
            Chat.views.message.setMessage("<div class='timer'>You have been banned.</div>");
        },
        
        fban: function() {
            Chat.views.message.setMessage("<div class='timer'>You have been banned.</div>");
        },
        
        refund: function() {
            Chat.views.message.setMessage("<div class='system'>" +Chat.manager.room.reader_info.username + " returned your time. </div>");
        },
        
        update_wait_time: function(remaining_time) {
        	$("#wait_seconds_left").html(remaining_time);
        },
        show_pending_dialog: function(reader_username) {
        	$( "#dialog-wait" ).dialog( "open" );
        	/*
        	if (disable_manual_quit) {
        		$(".ui-dialog-buttonpane button:contains('Quit This Chat Attempt')").button("disable");
        		$(".ui-dialog-buttonpane button:contains('Quit This Chat Attempt')").attr("disabled", true).addClass("ui-state-disabled");
        	}
        	*/
        	$( "#dialog-wait").dialog('option', 'title', "Waiting for response from " + reader_username);
        },
        
        hide_pending_dialog: function() {
        	$( "#dialog-wait" ).dialog( "close" );
        },
        show_reject_dialog: function() {
        	$('#dialog-reject').dialog("open");
        },
        show_abort_retry_dialog: function() {
        	$('#dialog-abort-retry').dialog("open");
        },
        show_offline_retry_dialog: function() {
        	$('#dialog-offline-retry').dialog("open");
        },
        show_unable_to_create_chat_max_manual_quit_dialog: function() {
        	$('#dialog-max-manual-quit').dialog("open");
        }
    },
    
    system: {
        
        leave_chat_room: function (member_type) {
            Chat.views.message.setMessage("<div class='system'>Chat has been terminated by the "+ member_type+ ".</div>");    
            if (Chat.models.member.member_type == 'reader') {
            	Chat.views.message.setMessage("<div class='system'>You will be redirect to your account page in 15 seconds.  You can also go here directly: <a href='/my_account'>My Account</a></div>");    
            }
        },
        
        show_typing: function(username) {
            $('#whosTyping').html("<span class='chat_username'>" + username  + "</span>&nbsp;&nbsp;is typing...");
        },
        
        clear_typing: function() {
            $('#whosTyping').html("");
        },
        
        show_message: function(data){
            Chat.views.message.system.clear_typing();
            
            //clearTimeout(whosTypingTimeout);
            if (data.message_type == 'system') {
                class_name = "chat_system";
                
                Chat.views.message.setMessage("<div class='"+class_name+"'><span class='chat_system_title'>System Message: </span>&nbsp;&nbsp;"+data.message+"</div>");
            } else {
                var class_name = "chat_" + data.member_type;
                
                Chat.views.message.setMessage("<div class='"+class_name+"'><span class='chat_username'>"+data.sender_name+":</span>&nbsp;&nbsp;"+data.message+"</div>");
            }
            
        },
        
        start: function() {
            
            Chat.views.message.setMessage("<div class='timer'>The timer is started</div>");
        },
        
        pause: function(username) {
            if (username) {
                Chat.views.message.setMessage("<div class='timer'>"+username+" has paused the timer</div>");
            } else {
                Chat.views.message.setMessage("<div class='timer'>The timer is paused</div>");
            }
        },
        
        disconnect_pause: function(username) {
            if (username) {
                Chat.views.message.setMessage("<div class='timer'>"+username+" has paused becuase of disconnection</div>");
            } else {
                Chat.views.message.setMessage("<div class='timer'>The timer is pause because of disconnection</div>");
            }
        },
        
        resume: function(username) {
            if (username) {
                Chat.views.message.setMessage("<div class='timer'>"+username+" has resumed the timer</div>");
            } else {
                Chat.views.message.setMessage("<div class='timer'>The timer is resumed</div>");
            }
        },
        
        refund: function() {
            Chat.views.message.setMessage("<div class='system'>Chat has been terminated & refunded by the reader</div> ");
        },
        
        close_window_message: function(timeout_var) {
            Chat.views.message.setMessage("<div class='system'>This chat window will close in " + (timeout_var/1000) + " seconds...</div>");    
        },
        
        set_timer: function(time_balance) {
            console.log("Set chat time: " + time_balance);
            $('#timerSpan').html(Chat.views.message.timerFormat(time_balance));
        },
        
        times_up_pause: function() {
            
        },
        
        disable_chat_form: function(){
            $('#chatForm textarea').attr('disabled','disabled');
            $('#chatForm input').attr('disabled','disabled');
        },
        enable_chat_form: function()
        {
            $('#chatForm textarea').removeAttr('disabled');
            $('#chatForm input').removeAttr('disabled');
        },
        show_add_stored_time: function(toggle)
        {
            if(toggle == true) {
                $("#addStoredTime").show();
            }else{
                $("#addStoredTime").hide();
            }
        },
        set_stored_time: function(minute) {
            console.log("Minute "  + minute);
            var stored_minute = Math.floor(minute);
            $('#maxAddStoredTime').html(stored_minute);
            /*
            $('#remainingStoredTime').html(minute);
            
            $('.add_stored_time').val(0);
            */
           if (Chat.is_mobile) {
               $( "#remainingStoredTimeInput" ).val(0);
           } else {
               var slider = $( "#remaining_stored_time-slider-range-max" ).slider({
                    range: "max",
                    min: 0,
                    max: stored_minute,
                    value: 1,
                    slide: function( event, ui ) {
                        $( "#remainingStoredTimeInput" ).val( ui.value );
                    }
                });
                $( "#remainingStoredTimeInput" ).val( $( "#remaining_stored_time-slider-range-max" ).slider( "value" ) );
                
                $('#remainingStoredTimeInput').change(function() {
                    
                    var value = $('#remainingStoredTimeInput').val();
                    console.log("value is " + value );
                    if (!isFinite(value)) {
                        value = 1;
                        $('#remainingStoredTimeInput').val(value);
                    }
                    if (value < 0) {
                        value = Math.abs(value);
                        $('#remainingStoredTimeInput').val(value);
                    }
                    
                    slider.slider( "value", value );
                });
           }
           
        },
        
        get_stored_time: function(minute) {
            Chat.views.message.setMessage("<div class='system'>Client is going to add more time from the remaining <span style='font-style: italic;'>"+ minute+ "</span> minutes stored time</div>");
        },
        show_lost_time: function(toggle)
        {
            if(toggle == true)
            {
                $("#lostTime").show();
            }
            else
            {
                $("#lostTime").hide();
            }
        },
        
        show_add_free_time: function(toggle) {
            if(toggle == true) {
                $("#addFreeTimeSection").show();
            } else{
                $("#addFreeTimeSection").hide();
            }
        }

    },
    
    ui: {
        adjust_input_message_textarea: function () {
            console.log("Calling adjust_input_message_textarea");
            if (Chat.is_mobile) {
                setTimeout(function() {
                    console.log("Adjusting");
                    $('.chat_option').css({"float":"left"});
                    $('#chat_window').css({"height":"190px"});
                    $('#input_message').css({"width": "95%", "margin-bottom":"5px", "font-size":"10px"});
                }, 500);
            }
            
        }
    },
    
    hideMenu: function(){
        $('#chatMenu').hide();
    },
    
    scrollToTop: function() {
        $('#chat_window').scrollTop($('#messageContainer').height());
    },
    
    setMessage: function(message) {
        $('#messageContainer').append(message);
        Chat.views.message.scrollToTop();
    },
    
    timerFormat: function(time_balance) {
        var sec_num = parseInt(time_balance, 10); // don't forget the second parm
        var hours   = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {minutes = "0"+minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}
        var time    = hours+':'+minutes+':'+seconds;

        return time;
    }
    
};
