Chat.views.popup = {
    
    chat_interface_win: null,
    purchase_more_win: null,
    chat_interface_win_preview: false,
    purchase_more_win: false,
    
    sound: {
        sound_handler: null,
        sound_period: 1000,
        
        play_sound: function() {
            if (Chat.browser.name == "MSIE") {
                Chat.views.popup.sound.sound_handler = setInterval(function() {
                    var sound = document.getElementById('beep');
                    if (sound) {
                        sound.Play();
                    }
                    
                }, Chat.views.popup.sound.sound_period);
            } else {
                ion.sound.play("door_bell", {
                volume: 0.5,
                loop: 20
                });
            }
            
        },
        stop_sound: function(){
            if (Chat.browser.name == "MSIE") {
                clearInterval(Chat.views.popup.sound.sound_handler);
            } else {
                ion.sound.stop("door_bell");
            }
        }
    },
    
    open_site_page: function(page_name) {
        var page_url = "/";
        if (page_name) {
            page_url += page_name;  
        } 
        window.open(page_url, "_blank");
    },
    
    preview_open_window: function() {
        var w = 900; 
        var h = 750;
        var LeftPosition = (screen.width)?(screen.width-w)/2:100;
        var TopPosition=(screen.height)?(screen.height-h)/2:100;
        var settings='width='+w+',height='+h+',top='+TopPosition+',left='+LeftPosition+',scrollbars=yes,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=yes';
        Chat.views.popup.chat_interface_win = window.open('/chat/loading.html','PsyConChatWindow',settings);
        if (Chat.views.popup.chat_interface_win) {
            Chat.views.popup.chat_interface_win_preview = true;
        } else {
            alert('Please allow popup from our website by changing the browser setting. ');
        }
    },
    
    after_open_window: function() {
        Chat.views.popup.sound.stop_sound();
        Chat.views.popup.remove_modal();
        
    },
    
    open_window: function(chat_id, chat_session_id){
        Chat.views.popup.after_open_window();
        
        var w = 900; 
        var h = 750;
        /*
        if ($.browser.safari || $.browser.mozilla) {
            h = 700;
        }
        */
        
        var LeftPosition = (screen.width)?(screen.width-w)/2:100;
        var TopPosition=(screen.height)?(screen.height-h)/2:100;
        var settings='width='+w+',height='+h+',top='+TopPosition+',left='+LeftPosition+',scrollbars=yes,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=yes';
        
        if (Chat.views.popup.chat_interface_win_preview && Chat.views.popup.chat_interface_win) {
            Chat.views.popup.chat_interface_win.location = '/chat/chatInterface/index/' + chat_id + "/" + chat_session_id;
        } else {
            Chat.views.popup.chat_interface_win = window.open('/chat/chatInterface/index/' + chat_id + "/" + chat_session_id,'PsyConChatWindow',settings);
            if (!Chat.views.popup.chat_interface_win) {
                alert('Please allow popup from our website by changing the browser setting. ');
            }
        } 
        Chat.views.popup.chat_interface_win_preview = false;
        // refresh the page after 2 seconds
        setTimeout(function() {
            location.reload();
        }, 2000);
        
    },
    
    preview_open_purchase_more_time_window: function() {
        var w = 900; 
        var h = 750;
        var LeftPosition = (screen.width)?(screen.width-w)/2:100;
        var TopPosition=(screen.height)?(screen.height-h)/2:100;
        var settings='width='+w+',height='+h+',top='+TopPosition+',left='+LeftPosition+',scrollbars=yes,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=yes';
        Chat.views.popup.purchase_more_win = window.open('/chat/loading.html','PurchaseMoreTimeWindow',settings);
        if (Chat.views.popup.purchase_more_win) {
            Chat.views.popup.purchase_more_win_preview = true;
        } else {
            alert('Please allow popup from our website by changing the browser setting. ');
        }
        
    },
    
    open_purchase_more_time_window: function() {
        var w = 900; 
        var h = 750;
        /*
        if ($.browser.safari) {
            h = 700;
        }
          */      
        var LeftPosition = (screen.width)?(screen.width-w)/2:100;
        var TopPosition=(screen.height)?(screen.height-h)/2:100;
        var settings='width='+w+',height='+h+',top='+TopPosition+',left='+LeftPosition+',scrollbars=yes,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=yes';
        
        if (Chat.views.popup.chat_interface_win_preview && Chat.views.popup.chat_interface_win) {
            Chat.views.popup.purchase_more_win.location = '/chat/main/purchase_time/' + Chat.manager.room.reader_info.username;
        } else {
            Chat.views.popup.purchase_more_win = window.open('/chat/main/purchase_time/' + Chat.manager.room.reader_info.username ,'PurchaseMoreTimeWindow',settings);
            if (!Chat.views.popup.purchase_more_win) {
                alert('Please allow popup from our website by changing the browser setting. ');
            }
        }
        Chat.views.popup.purchase_more_win_preview = false;
    },
    
    resize_chat: function()
    {
        var windowHeight = $(window).height() - $('#navbar').outerHeight() - $('#footer').outerHeight() - 115;
        $('#chat_window').height(windowHeight);
        $("#footer textarea").width($(window).width() - 200);
    },
    
    remove_modal: function() {
        $('#modal').remove();
        $('#modal_bg').remove();
    }
};
