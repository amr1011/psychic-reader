Chat.controller.room = {
    
    previous_char: '',
    
    start_timer: function() {
        
        
    },
    
    pause_timer: function() {
        
        
    },
    
    send_message: function() {
        
        
    },
    
    detect_typing: function() {
        
    },
    
    endChat: function(){
        Chat.views.message.hideMenu();
        // chat has a session
        if(Chat.manager.room.chat_session_id){
            if(Chat.member_type=='reader'){
                // do something 
            }
        }
    },

    showLostTime: function(toggle)
    {
        if(toggle == true)
        {
            $("#lostTime").show();
        }
        else
        {
            $("#lostTime").hide();
        }
    },


    //--- Close Chat Window
    close_window: function (timeout_var){
        timeout_var = timeout_var || 10000;

        Chat.views.message.system.clear_typing();
        Chat.views.message.system.disable_chat_form();
        Chat.controller.room.hide_all_user_actions();
        // just show a closing message
        Chat.views.message.system.close_window_message(timeout_var);
        //timeout_var = timeout_var || window_close_time;
        //objectWrite({ type : 'message', 'className' : 'system', 'message' : "This chat window will close in " + (timeout_var/1000) + " seconds..." });

        setTimeout(function(){
            window.open('','_self').close();
            //window.close();
        }, timeout_var);

    },
    
    actions: {
        
        on_message_send: function() {
            console.log("register message send onclick");
            $("#chatForm_send").click(function(e)
            {
                console.log("On Click send");
                e.preventDefault();
    
                Chat.controller.room.actions.message_send();
            });
            
            $("#input_message").keypress(function(e) {
                
                    
                if(e.which == 13 ) {
                    // enter
                    var message = $("#input_message");
                    var send_message = $.trim(message.val());
                    
                    if (send_message.length >= 1 ) {
                        Chat.controller.room.actions.message_send();
                    }
                } else {
                    var message = $("#input_message");
                    var send_message = $.trim(message.val());
                
                    if (send_message == '') {
                        Chat.controller.room.previous_char = '';
                        // nothing else happen
                    } else {
                        if (send_message != Chat.controller.room.previous_char) {
                            // yes. typing
                            Chat.manager.room.send.typing();
                            Chat.controller.room.previous_char = send_message;
                        }
                    } 
                }
            });
        
        },
        
        message_send: function() {
            var message = $("#input_message");
                
            var send_message = $.trim(message.val());
            if(send_message)
            {
                //$.post('/chat/chatInterface/log_message/'+chatSessionId, { message : $.trim(message.val()) });
                //sendObject('message', (isReader ? "reader" : "client"), $.trim(message.val()));
                var send_message = $.trim(message.val());
                console.log("Send Message to server: " + send_message );
                message.val("");    
                
                Chat.manager.room.send.message({message:send_message});
            }
        }
    },
    
    hide_all_user_actions: function() {
        $(".pauseChatAnchor").hide();
        $(".resumeChatAnchor").hide();        
        //$(".endChatAnchor").hide();
        
        if (Chat.models.member.member_type == 'client') {
            //$(".purchaseMoreTime").hide();
            $(".addStoredTime").hide();
            $(".contactAdmin").hide();
        } else {
            $(".personalBanUserAnchor").hide();
            $(".fullBanUserAnchor").hide();
            
            $(".refundChatAnchor").hide();
            $(".addLostTime").hide();
            $("#lostTimeBtn").hide();
        }
    },
    
    user_actions: function() {
        $(".pauseChatAnchor").click(function(){
            Chat.manager.room.send.pause();
        });
        
        $(".resumeChatAnchor").click(function(){
            Chat.manager.room.send.resume();
        });
        
        $(".endChatAnchor").click(function() {
            if(confirm("Are you sure you want to end this chat?")) {
                Chat.manager.room.send.leave_chat();
            }
        });

        $(".purchaseMoreTime").click(function() {
            if(confirm("You are about to pause this chat to purchase more time. Would you like to continue?")){
                Chat.manager.room.send.purchase_time();
            }
        });
        
        $(".addStoredTime").click(function() {
            if(confirm("Doing this will pause the chat, do you want to continue?")){
                //Chat.manager.room.send.add_stored_time();
                Chat.manager.room.send.get_stored_time();
            }
        });
        
        $("#addStoredTimeBtn").click(function() {
            Chat.manager.room.send.add_stored_time();
        });
        
        $("#cancelAddStoredTimeBtn").click(function() {
            Chat.views.message.system.show_add_stored_time(false);
            Chat.manager.room.send.resume();
        });
        
        $("#purchaseMoreTimeBtn").click(function() {
            if(confirm("You are about to purchase more time. Would you like to continue?")){
                Chat.manager.room.send.purchase_time();
            }
        });
        
        $("#refreshPurchasedTimeBtn").click(function() {
            Chat.manager.room.send.refresh_stored_time();
        });
        
        $(".addFreeTime").click(function() {
            Chat.views.message.system.show_add_free_time(true);
        });
        
        $("#addFreeTimeBtn").click(function() {
            Chat.manager.room.send.add_free_time();
        });
        
        $("#cancelAddFreeTimeBtn").click(function() {
        	$('#freeTimeInput').val(0);
            Chat.views.message.system.show_add_free_time(false);
        });
        

        $(".personalBanUserAnchor").click(function() {
            if(confirm('Are you sure you want to ban this user & end this chat?')){
                Chat.manager.room.send.pban();
            }
        });
        
        $(".fullBanUserAnchor").click(function() {
            if(confirm('Are you sure you want to ban this user & end this chat?')) {
                Chat.manager.room.send.fban();
            }
        });
        
        $(".refundChatAnchor").click(function() {
            if(confirm('Are you sure you want to return entire chat time back to the client and end this chat?')) {
                Chat.manager.room.send.refund();
            }
        });
        $(".addLostTime").click(function() {
            // just show the add lost time
            // set to pause first by reader
            Chat.views.message.system.show_lost_time(true);
            Chat.manager.room.send.pause();
        });
        $(".removeLostTime").click(function() {
            // just show the add lost time
            // set to pause first by reader
            Chat.views.message.system.show_lost_time(false);
            Chat.manager.room.send.resume();
        });
        $("#lostTimeBtn").click(function(){
            if($.isNumeric($("input[name=losttime]").val()))
            {
                var lost_time = parseInt($("input[name=losttime]").val(), 10);
                
                if (lost_time < 0) {
                    alert('Lost time cannot be zero nor negative number');
                }
                
                if (lost_time * 60 >= Chat.manager.room.max_chat_length || lost_time*60 > Chat.manager.room.chat_length) {
                    alert('Lost time cannot exceed max chat time or chat time');
                } else {
                    Chat.manager.room.send.lost_time(lost_time);
                }           
            }
            else
            {
                alert("Must Be Valid Number");
            }
        });
        
        $(".contactAdmin").click(function() {
            Chat.manager.room.send.contact_admin(); 
        });
        	
    },
    
    init: function() {
        // register
        console.log("Registering controller room actions");
        Chat.controller.room.actions.on_message_send();
        // set up actions
        Chat.controller.room.user_actions();
        
    }
    
};
