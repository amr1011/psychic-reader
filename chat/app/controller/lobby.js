Chat.controller.lobby = {
    
    chat_session_id: null, // only for client when doing chat attempt
    chat_redirect_url: null, 
    reader_id: null,
    reader_chat_attempt_accepted: false,
    disable_manual_quit: false,
    
    abort_chat_room: function() {
    	Chat.views.message.reader.abort_chat();
    },
    
    actions: {
    	// register reader in the lobby for socket message
	    register: function() {
	        // SHOULD run at start.
	        var member = Chat.models.member;
	        console.log("Going to emit message");
	        Chat.models.socket.io.emit('register', {member_id:member.member_id, member_id_hash:member.member_id_hash}, function(data){
	            console.log("Emitted message");
	            // emit to socket server.
	            if (data && data.status) {
	                Chat.views.error_log.log("Success: register member " + member.member_id + " to the lobby");
	            } else {
	                // show error. 
	                Chat.views.error_log.show(data);
	            }
	        });
	    },
    	reject_chat_room: function(chat_session_id) {
    		if (Chat.models.member.member_type == 'reader' && !Chat.controller.lobby.reader_chat_attempt_accepted) {
		        console.log("Reject chat room: " + chat_session_id);
		        Chat.views.popup.sound.stop_sound();
		        Chat.views.message.reader.reject_chat_room();
		        
		        var input = {
		            room_name   : chat_session_id,
		            member_id : Chat.models.member.member_id,
		            member_id_hash : Chat.models.member.member_id_hash
		        };
		        
		        Chat.models.socket.io.emit('reader_reject_chat_attempt', input, function(data){
		            // emit to socket server.
		            if (data && data.status) {
		           
		            	Chat.controller.lobby.chat_id = null;
		            	Chat.controller.lobby.chat_session_id = null;
		            	Chat.controller.lobby.chat_redirect_url = null;
		                Chat.views.message.reader.reject_chat_room();
		            } else {
		                // show error. 
		                Chat.views.error_log.show(data);
		            }
		        });
	        }
	    },

    	accept_chat_room: function(chat_session_id) {
    		if (Chat.models.member.member_type == 'reader') {
    			console.log("Accept chat room: " + chat_session_id);
		        Chat.views.popup.sound.stop_sound();
		        
		        var input = {
		            room_name   : chat_session_id,
		            member_id : Chat.models.member.member_id,
		            member_id_hash : Chat.models.member.member_id_hash
		        };
		        
		        Chat.models.socket.io.emit('reader_accept_chat_attempt', input, function(data){
		            // emit to socket server.
		            if (data && data.status) {
		                //wait for the join_new_chat_room io.  so client join first. 
		                Chat.controller.lobby.reader_chat_attempt_accepted = true;
		            } else {
		                // show error. 
		                Chat.views.error_log.show(data);
		            }
		        });
    		}
    		
    		//href='/chat/chatInterface/index/"+data.chat_id+"/"+data.chat_session_id+"'
    	},
    	
    	// send out chat attempt from client
    	chat_attempt: function(reader_id) {
    		
    		var topic = $('#topic').val();
    		var minutes = $('#minutes').val();
    		
    		if (! topic) {
    			alert("Topic cannot be empty");
    			return;
    		}
    		if (! parseInt(minutes, 10) > 0) {
    			alert("Minutes must be greater than 0");
    			return;
    		}
    		
    		if (Chat.models.member.member_type == 'client') {
		        var input = {
		        	reader_id: reader_id,
		        	member_id: Chat.models.member.member_id,
		        	member_id_hash: Chat.models.member.member_id_hash,
		        	topic: topic,
		        	minutes: minutes
		        };
		        
		        Chat.models.socket.io.emit('chat_attempt', input, function(data){
		            console.log("Emitted reject chat message");
		            // emit to socket server.
		            if (data && data.status) {
		            	// save properties about the room. 
		            	Chat.controller.lobby.chat_id = data.data.chat_id; 
		            	Chat.controller.lobby.chat_session_id = data.data.chat_session_id; 
		            	Chat.controller.lobby.chat_redirect_url = data.data.redirect_url; 
		            	Chat.controller.lobby.disable_manual_quit = data.data.disable_manual_quit;
		            	Chat.models.member.member_hash = data.data.member_hash;
		                Chat.views.message.client.show_pending_dialog(data.data.reader_username);
		            } else {
		                // show error. 
		                if (data.is_max_manual_quit) {
		                	Chat.views.message.client.show_unable_to_create_chat_max_manual_quit_dialog();
		                } else {
			                alert(data.message);
			                Chat.views.error_log.show(data);
		                }
		            }
		        });
    		}
    	},
    	
    	chat_attempt_abort: function(reader_id) {
    		if (Chat.models.member.member_type == 'client' && !Chat.controller.lobby.reader_chat_attempt_accepted) {
    			
    			if (! Chat.models.member.member_hash) {
    				alert('You have not submit for a chat yet.');
    				return;
    			}
    			
		        var input = {
		            room_name   : Chat.controller.lobby.chat_session_id,
		            member_hash : Chat.models.member.member_hash
		        };
		        
		        Chat.models.socket.io.emit('chat_attempt_abort', input, function(data){
		            if (data && data.status) {
		               // do nothing.  let io received message deal with it. 
		            } else {
		                // show error. 
		                Chat.views.error_log.show(data);
		            }
		        });
    		}
    	}
    },
    
    user_actions: function() {
      	$( "#dialog-wait" ).dialog({
	      	autoOpen: false,
	      	show: {
	        	effect: "blind",
	        	duration: 100
	      	},
	      	hide: {
	        	effect: "blind",
	        	duration: 10
	      	},
	      	closeOnEscape: false,
		    dialogClass: "noclose",
	      	resizable: false,
	      	height:200,
	      	width:450,
	      	modal: true,
	      	buttons: {
	        	"Quit This Chat Attempt": function() {
	        		// call client abort function
	        		if (! Chat.controller.lobby.disable_manual_quit) {
	        			Chat.controller.lobby.actions.chat_attempt_abort();
	        		}
	        	}
	    	}
	    });
	    
	    $( "#dialog-reject" ).dialog({
	      	autoOpen: false,
	      	show: {
	        	effect: "blind",
	        	duration: 100
	      	},
	      	hide: {
	        	effect: "blind",
	        	duration: 10
	      	},
	      	closeOnEscape: false,
		    dialogClass: "noclose",
	      	resizable: false,
	      	height:200,
	      	modal: true
	    });
	    
	    $( "#dialog-abort-retry" ).dialog({
	      	autoOpen: false,
	      	show: {
	        	effect: "blind",
	        	duration: 100
	      	},
	      	hide: {
	        	effect: "blind",
	        	duration: 10
	      	},
	      	closeOnEscape: false,
		    dialogClass: "noclose",
	      	resizable: false,
	      	height:200,
	      	modal: true
	    });
	    
	    $( "#dialog-offline-retry" ).dialog({
	      	autoOpen: false,
	      	show: {
	        	effect: "blind",
	        	duration: 100
	      	},
	      	hide: {
	        	effect: "blind",
	        	duration: 10
	      	},
	      	closeOnEscape: false,
		    dialogClass: "noclose",
	      	resizable: false,
	      	height:200,
	      	modal: true
	    });
	    
	    $( "#dialog-max-manual-quit" ).dialog({
	      	autoOpen: false,
	      	show: {
	        	effect: "blind",
	        	duration: 100
	      	},
	      	hide: {
	        	effect: "blind",
	        	duration: 10
	      	},
	      	closeOnEscape: false,
		    dialogClass: "noclose",
	      	resizable: false,
	      	height:200,
	      	modal: true
	    });
	    
	    $( window ).unload(function() {
	    	if (Chat.controller.lobby.chat_session_id) {
	    		if (Chat.models.member.member_type == 'client') {
	    			Chat.controller.lobby.actions.chat_attempt_abort(Chat.controller.lobby.reader_id);	
	    		} else {
	    			Chat.controller.lobby.actions.reject_chat_room(Chat.controller.lobby.chat_session_id);
	    		}
	    	}
		});
    },
    
    receive: {
    	join_new_chat_room: function(data) {
	        //console.log(" join_new_chat_room: recieve socket message " + JSON.stringify(data));
	        Chat.views.error_log.log(data);
	        var reader_id = data.reader_id;
	        
	        if (reader_id == Chat.models.member.member_id) {
	            console.log('Now player can join, then check and launch the window to prompt reader to accept or reject the chat');
	            // message to readers. 
	            Chat.controller.lobby.chat_id = data.chat_id; 
            	Chat.controller.lobby.chat_session_id = data.chat_session_id; 
            	Chat.controller.lobby.chat_redirect_url = data.redirect_url; 
	            window.location.href = Chat.controller.lobby.chat_redirect_url; //"/chat/chatInterface/index/" +data.reader_id+ "/" + data.chat_session_id;
	        }
	    },
	    
	    chat_attempt: function(data) {
	    	// client handle that in return call
	    	if (Chat.models.member.member_type == 'reader') {
		    	if (data && data.status) {
		    		// show the popup. 
		    		Chat.controller.lobby.chat_id = data.data.chat_id; 
	            	Chat.controller.lobby.chat_session_id = data.data.chat_session_id; 
	            	Chat.controller.lobby.chat_redirect_url = data.data.redirect_url; 
		    		Chat.views.message.reader.chat_attempt_for_reader(data.data);
		    	}
	    	}
	    },
	    
	    chat_attempt_ping: function(data) {
	    	// just put the seconds left
	    	if (Chat.models.member.member_type == 'client') {
		    	if (data && data.status) {
			    	$('#wait_seconds_left').html(data.wait_time_left);
			    	if(data.wait_time_left==0){
                        $('#wait_seconds_left').parent('p').html("Quitting");
                        $( "#dialog-wait" ).dialog( "close" );
                        $('#dialog-abort-retry').dialog("open");
                    }
                }



	    	}
	    },
	    
	    chat_attempt_abort: function(data) {
	    	if(data && data.status) {
	    		Chat.controller.lobby.chat_id = null;
            	Chat.controller.lobby.chat_session_id = null;
            	Chat.controller.lobby.chat_redirect_url = null;
            	
	    		if (Chat.models.member.member_type == 'client') {
	    			
	    			Chat.views.message.client.hide_pending_dialog();
	    			Chat.views.message.client.show_abort_retry_dialog();
	    		} else {
	    			// just close the popup
	    			Chat.views.message.reader.abort_chat();
	    		}
	    	}
	    },
	    
	    chat_attempt_offline: function(data) {
	    	if(data && data.status) {
	    		Chat.controller.lobby.chat_id = null;
            	Chat.controller.lobby.chat_session_id = null;
            	Chat.controller.lobby.chat_redirect_url = null;
            	
	    		if (Chat.models.member.member_type == 'client') {
	    			Chat.views.message.client.hide_pending_dialog();
	    			Chat.views.message.client.show_offline_retry_dialog();
	    		} else {
	    			// logout
	    			Chat.views.message.reader.abort_chat();
	    			window.location.href = "/main/logout";
	    		}
	    	}
	    	
	    },
	    
	    reader_accept_chat_attempt: function(data) {
	    	if (Chat.models.member.member_type == 'client') {
	    		Chat.controller.lobby.reader_chat_attempt_accepted = true;
	    		window.location.href = Chat.controller.lobby.chat_redirect_url; //"/chat/chatInterface/index/" +data.reader_id+ "/" + data.chat_session_id;
	    	}
	    },
	    
	    reader_reject_chat_attempt: function(data) {
	    	if (Chat.models.member.member_type == 'client') {
	    		Chat.controller.lobby.chat_id = null;
            	Chat.controller.lobby.chat_session_id = null;
            	Chat.controller.lobby.chat_redirect_url = null;
	    		Chat.views.message.client.hide_pending_dialog();
    			Chat.views.message.client.show_reject_dialog();
	    	}
	    }
	    
    },
    
    
    // socket message dispatcher
    dispatcher: function() {
        console.log("Dispatcher ");
        var io = Chat.models.socket.io; // Chat.socket

        //io.on('reader_abort_chat_room', Chat.controller.lobby.abort_chat_room); // receive that message in lobby
        
        io.on('chat_attempt', Chat.controller.lobby.receive.chat_attempt); 							// Reader
        io.on('chat_attempt_ping', Chat.controller.lobby.receive.chat_attempt_ping); 				// Client only
        io.on('chat_attempt_abort', Chat.controller.lobby.receive.chat_attempt_abort); 				// Client & Reader
        io.on('chat_attempt_offline', Chat.controller.lobby.receive.chat_attempt_offline); 			// Client & Reader
        io.on('reader_accept_chat_attempt', Chat.controller.lobby.receive.reader_accept_chat_attempt); // Client only
        io.on('reader_reject_chat_attempt', Chat.controller.lobby.receive.reader_reject_chat_attempt); // Client only
        io.on('join_new_chat_room', Chat.controller.lobby.receive.join_new_chat_room); // Reader only
    },
    
    init: function() {
        Chat.controller.lobby.dispatcher();
        Chat.controller.lobby.user_actions();
    }
};
