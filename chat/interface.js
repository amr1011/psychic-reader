
    //--- Chat Form: chatForm
    //--- Chat Interface: messageContainer
    var socket;
    var isReader = 0;
    var whosTypingTimeout = false;
    var member;
    var timerInterval;
    var recordChatIntervalSeconds = 2;
    var chatLength = 0;
    var readerListening = 0;
    var reader_missing = true;
    var timeout_seconds = 5;
    //var window_close_time = 60000;
    var window_close_time = 300000; // dev

    //--- Initialize Chat Platform
    $(function(){

        if(chatSessionId){
            initChatInterface();
        }

    });

    //--- Init Chat Interface
    //--- Chat Interface
    function initChatInterface(){

        //--- Check if the user is the reader or not
        isReader = (member_id == reader_id ? 1 : 0);
        if(isReader) readerListening = 1;

        //--- If logged member IS NOT READER
        //--- Then we PING the reader
        if(!isReader){
            $('#timerSpan').html(timerFormat(chat_time));
        }

        //--- Set the object paramaters for the logged member
        if(isReader) member = reader;
        else member = client;

        //--- Customize Chat Interface Components
        customizeInterface();

        //--- pauseChatAnchor
        $(document).on('click', '.pauseChatAnchor, #timerSpan', function(e){
            e.preventDefault();

            if(isReader && typeof timerInterval != 'undefined'){
                sendObject('pause');
            }

        });

        //--- endChatAnchor
        $(document).on('click', '.endChatAnchor', function(e)
        {

            e.preventDefault();

            if(confirm("Are you sure you want to end this chat?"))
            {

                if(isReader)
                {

                    sendObject('end', 'system', "Chat has been terminated by the reader.");

                }
                else
                {

                    sendObject('end', 'system', "Chat has been terminated by the client.");

                }

            }

        });

        //--- purchaseMoreTime
        if(isReader) $('.purchaseMoreTime').hide();
        $(document).on('click', '.purchaseMoreTime', function(e){

            e.preventDefault();

            if(confirm("You are about to pause this chat to purchase more time. Would you like to continue?")){
                sendObject('purchase_time');
            }

        });

        //--- addStoredTime
        if(isReader) $('.addStoredTime').hide();
        $(document).on('click', '.addStoredTime', function(e){

            e.preventDefault();

            if(confirm("Doing this will pause the chat, do you want to continue?")){
                sendObject('add_stored_time');
            }

        });

        //--- resumeChatAnchor
        $(document).on('click', '.resumeChatAnchor', function(e){

            e.preventDefault();

            if(isReader){
                sendObject('start');
            }

        });

        //--- refundChatAnchor (show for readers only)
        if(isReader) $('.refundChatAnchor').show();
        $(document).on('click', '.refundChatAnchor', function(e)
        {

            e.preventDefault();

            if(isReader)
            {

                if(confirm('Are you sure you want to refund & end this chat?'))
                {

                     sendObject('refund', 'system', "Chat has been terminated & refunded by the reader.");

                }

            }

        });
        //--- addLostTime
        if(isReader) $('.addLostTime').show();
        $(document).on('click', '.addLostTime', function(e)
        {

            e.preventDefault();

            if(isReader)
            {
               sendObject("pause");
               showLostTime(true);
            }

        });


        //--- addLostTime
        $('.contactAdmin').show();
        $(document).on('click', '.contactAdmin', function(e)
        {

            e.preventDefault();

            var admin_obj =
            {
                'member_id' : member_id
            }
            sendObject('contact',false, admin_obj);

        });

        $(document).on('click', '#lostTimeBtn', function(e)
        {
            if($.isNumeric($("input[name=losttime]").val()))
            {
                var lt_arr = {
                    'losttime' : $("input[name=losttime]").val()
                }
                sendObject('losttime',false,lt_arr);
            }
            else
            {
                alert("Must Be Valid Number");
            }

        });

        //--- banUserAnchor (show for readers only)
        if(isReader) $('.personalBanUserAnchor').show();
        $(document).on('click', '.personalBanUserAnchor', function(e)
        {

            e.preventDefault();

            if(isReader)
            {

                if(confirm('Are you sure you want to ban this user & end this chat?'))
                {

                    sendObject('pban');

                }

            }

        });

        if(isReader) $('.fullBanUserAnchor').show();
        $(document).on('click', '.fullBanUserAnchor', function(e)
        {

            e.preventDefault();

            if(isReader)
            {

                if(confirm('Are you sure you want to ban this user & end this chat?'))
                {

                    sendObject('fban');

                }

            }

        });

        //--- Initialize Chat Platform

	    var port   = 3701;
    	socket = io.connect('http://66.178.176.109:'+port);

        socket.on('message', objectWrite);
        socket.on('join', memberJoined);
        socket.on('leave', memberLeft);


        //---- Send chat request to reader.
        var new_chat = {
            'reader_id' : reader_id,
            'chat_id'   : chatSessionId,
            'username'  : client.username
        }
        if(!isReader) sendObject('new_chat',false, new_chat);
        //-------------------

        //--- Start Timer
        //--- When the reader joins ONLY
        var delay_in_seconds = 10;

        if(isReader)
        {

            setTimeout(function()
            {

                sendObject('start', 'system');
                memberJoined(client);

            }, (delay_in_seconds * 1000));

        }
        else
        {

            setTimeout(function()
            {

                sendObject('start', 'system');           
                memberJoined(reader);

            }, (delay_in_seconds * 1000));
        }


        setTimeout(function ()
        {
            clearInterval(intervalID);

            if(reader_missing == true)
            {
                var timeout_obj =
                {
                    'member_id' : member_id,
                    'chat_id'   : chatSessionId,
                    'username'  : client.username
                }
                sendObject('timeout',false, timeout_obj);
            }
        }, timeout_seconds);



        //--- If the chat form is submitted, capture the request and
        //--- Convert it into a new chat object
        $("#chatForm").submit(function(e)
        {

            e.preventDefault();

            var message = $("#chatForm textarea[name='message']");

            if(message.val())
            {
                $.post('/chat/chatInterface/log_message/'+chatSessionId, { message : $.trim(message.val()) });
                sendObject('message', (isReader ? "reader" : "client"), $.trim(message.val()));
                setTimeout(function()
                {

                    message.val("");

                }, 250);
            }

        });

        //--- When a user clicks ENTER from within the textarea
        //--- Submit the form
        $("#chatForm textarea[name='message']").keypress(function(e)
        {

            var stringLength = $.trim( $(this).val()).length;

            if(e.which == 13 && stringLength >= 1)
            {

                $("#chatForm").submit();

            }
            else
            {

                //--- If ENTER is not pressed
                //--- Then find which user is typgin and send object

                var characterValue = parseInt($(this).val().length.toFixed());
                var checkCharacters = (characterValue % 3);

                if(checkCharacters == 0)
                {

                    if(isReader) sendObject('typing', 'reader');
                    else sendObject('typing', 'client');

                }

            }

        });

    }

    //--- Send Object
    function sendObject(type, className, message)
    {
        console.log("+++++++++++"+type);

        //--- Find out who's sending this object
        //--- If system, then nobody is sending this object
        var senderName = false;
        if(className != 'system')
        {

            if(isReader) senderName = 'reader';
            else senderName = 'client';

        }

        //--- Build the parameters for the object
        var object =
        {
            type : type, // message, end, pause, resume, refund, ban
            className : className,  // type: message (system, reader, client)
            message : message,
            sender : senderName
        };

        //--- Send the object
//		JSON.stringify(object);
 	    socket.emit('send', object);

//alert("object:"+JSON.stringify(message));

    }

    //--- When an object has been receieved
    function objectWrite(object)
    {
//alert("received object:"+JSON.stringify(object)+";type:"+object.type);

        switch(object.type){

            //--- Ban a user.
            case "pban":

                pause();
                disableChatForm();

                if(isReader){
                    banUser("personal");
                }

                record_chat(function(){

                    endChat();

                    if(isReader){
                        $('#messageContainer').append("<div class='timer'>"+ client.username +" has been banned.</div>");
                    }else{
                        $('#messageContainer').append("<div class='timer'>You have been banned.</div>");
                    }

                    closeWindow(5000);

                });

            break;



            case "losttime":
                var lt = object.message.losttime * 60 + chat_time;
                //alert("lt-" + lt + " " + "ct-" + chat_time + " " + "mct-" + max_chat_time);

                if(lt > max_chat_time)
                {
                    chat_time = max_chat_time;
                }
                else
                {
                    chat_time = lt;
                }

                showLostTime(false);
                startResumeChat();
            break;

            case "fban":

                pause();
                disableChatForm();

                if(isReader){
                    banUser("full");
                }

                var banObj = {
                    'member_id' : member_id,
                    'chat_id'   : chatSessionId,
                    'username'  : client.username
                }
                if(!isReader) sendObject('fullban',false, banObj);

                record_chat(function(){

                    endChat();

                    if(isReader){
                        $('#messageContainer').append("<div class='timer'>"+ client.username +" has been fully banned.</div>");
                    }else{
                        $('#messageContainer').append("<div class='timer'>You have been banned.</div>");
                    }

                    closeWindow(5000);

                });

                break;

            //--- Ban a user.
            case "refund":

                pause();
                disableChatForm();

                if(isReader){
                    refundChat();
                    $('#messageContainer').append("<div class='"+object.className+"'>Refunded " + client.username + "'s time. </div>");
                }else{
                    $('#messageContainer').append("<div class='"+object.className+"'>" +reader.username + " refunded your time. </div>");
                }

                closeWindow();

            break;

            //--- Post message to chat area
            case "message":
                $('#whosTyping').html("");
                clearTimeout(whosTypingTimeout);

                $('#messageContainer').append("<div class='"+object.className+"'>"+object.message+"</div>");
                $('#chat_window').scrollTop($('#messageContainer').height());

                break;

            //--- Update who's typing
            case "typing":

                if(object.sender == 'reader'){

                    if(!isReader){

                        $('#whosTyping').html(reader.username  + " is typing...");
                        if(whosTypingTimeout) clearTimeout(whosTypingTimeout);

                        whosTypingTimeout = setTimeout(function()
                        {

                            $('#whosTyping').html("");

                        }, 1000);

                    }

                }else{

                    if(isReader)
                    {

                        $('#whosTyping').html(client.username  + " is typing...");
                        if(whosTypingTimeout) clearTimeout(whosTypingTimeout);

                        whosTypingTimeout = setTimeout(function()
                        {

                            $('#whosTyping').html("");

                        }, 1000);

                    }

                }

                break;

            //--- Start Timer
            case "start":
                reader_missing = false;
                startResumeChat();
                enableChatForm();

                $('#messageContainer').append("<div class='timer'>The timer has started</div>");

                break;

            //---   + End Chat +
            //---   Clear chat timer
            //---   Post message notifying connected users of chat termination
            //---   Disable form fields so users can no longer communicate off chat record
            //---   Remove settings menu so users cannot click anything else

            case "end":

                    pause();
                    disableChatForm();

                    record_chat(function(){

                        endChat();

                        $('#messageContainer').append("<div class='timer'>"+object.message+"</div>");
                        closeWindow();

                    });

                    /*JHW 05/04/2014 */
                    //Send redirect request.
                    var redirect_url = "";
                    if(isReader)
                    {
                        redirect_url = "/my_account/chats/transcript/" + chatSessionId;
                    }
                    else
                    {
                        redirect_url = "/chat/main/chat_ended/" + chatSessionId;
                    }

                    var redirect_obj =
                    {
                        'member_id' : member_id,
                        'chat_id'   : chatSessionId,
                        'redirect'  : client.username
                    }
                    sendObject('redirect',false, redirect_obj);
                    // end
                break;

            case "pause":

                    pause();
                    $('#messageContainer').append("<div class='timer'>The timer has been paused. Any messages sent will be saved to the correspondence history but no funds are being charged to the client's account.</div>");

                break;

            case "purchase_time":

                if(isReader) $('#messageContainer').append("<div class='timer'>The client has paused the chat to purchase additional time. You may close this chat window. You will be notified when the user is ready to chat again.</div>");

                pause();
                hideMenu();

                record_chat(function(){
                    closeWindow();
                    if(!isReader) window.location = '/chat/chatInterface/purchaseMoreTime/'+chatSessionId;
                });

                break;

            case "add_stored_time":

                if(isReader) $('#messageContainer').append("<div class='timer'>The client has paused the chat to add additional stored time. You may close this chat window. You will be notified when the user is ready to chat again.</div>");

                pause();
                hideMenu();

                record_chat(function(){
                    closeWindow();
                    if(!isReader) window.location = '/chat/chatInterface/addStoredTime/'+chatSessionId;
                });

                break;

            case "new_chat":
                $('#messageContainer').append("<div class='system'>The reader has been contacted & no minutes will be used or taken until the timer starts at the top of this screen.</div>");
                break;

            case "reader_decline":
                if(object.chat_id == chatSessionId){
                    pause();
                    disableChatForm();
                    $('#messageContainer').append("<div class='timer'>"+object.username+" is busy as this time.</div>");
                    closeWindow();
                }
                break;

            case "timeout":
                disableChatForm();
                pause();

                $('#messageContainer').append("<div class='system'>No response from psychic.</div>");
                closeWindow();
                break;

            //--- Notify of invalid message
            case "contact":
                $('#messageContainer').append("<div class='system'>Contact page is now open on launch page.</div>");
                break;
                
            default:
                $('#messageContainer').append("<div class='system'>Invalid request received...</div>");
                break;

        }

    }

    //--- When a user has joined the chat session
    function memberJoined(memberObject){
        objectWrite({ type : 'message', 'className' : 'system', 'message' : (memberObject.username).toUpperCase() + " has joined the chat room." });
    }

    //--- When a user has left the chat session
    function memberLeft(memberObject){

        //--- If the reader left
        //--- Then mark him as no longer listening
        if(!isReader){
            readerListening = 0;
        }

        objectWrite({ type : 'message', 'className' : 'system', 'message' : (memberObject.username).toUpperCase() + " has left the chat." });
        sendObject('end', 'system', "Chat has been terminated. " + (memberObject.username).toUpperCase() + " has left the chat room");

    }

    //--- Customize user's interface
    function customizeInterface()
    {

        if(isReader)
        {

            $('.client_username').html(client.username);
            $('.client_first_name').html(client.firstName);
            $('.client_last_name').html(client.lastName);
            $('.client_dob').html(client.dob);

        }
        else
        {

            $('.client_first_name').html(reader.username);
            $('#clientInfo').hide();

        }

    }

    //--- Timer
    function timer(){

        timerInterval = setInterval(function(){

            $('#timerSpan').html(timerFormat(chat_time));

            //--- Every set interval record chat length - !! ONLY IF READER
            if(chatLength > 0 && chat_time % recordChatIntervalSeconds == 0 && isReader){
                record_chat();
            }

            // 5 minutes warning
            if(chat_time == 300){
                objectWrite({ type : 'message', 'className' : 'system', 'message' : "5 Minute Warning: There are only 5 minutes left in your current chat session.<br />We recommend purchasing more time using the settings menu at the top. " });
            }

            // 2 minutes warning
            else if(chat_time == 120){
                objectWrite({ type : 'message', 'className' : 'system', 'message' : "2 Minute Warning: There are only 2 minutes left in your current chat session.<br />We recommend purchasing more time using the settings menu at the top. " });
            }

            // 1 minute warning
            else if(chat_time == 60){
                objectWrite({ type : 'message', 'className' : 'system', 'message' : "1 Minute Warning: There is only 1 minute left in your current chat session.<br />We recommend purchasing more time using the settings menu at the top. " });
            }

            // Timer Ends
            else if(chat_time == 1){

                if(isReader){
                    sendObject('purchase_time');
                }

            }

            chat_time -= 1;
            chatLength += 1;

        }, 1000);

    }

    //-- Format time in HH:MM:SS based on number of seconds
    function timerFormat(seconds)
    {

        var sec_num = parseInt(seconds, 10); // don't forget the second parm
        var hours   = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {minutes = "0"+minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}
        var time    = hours+':'+minutes+':'+seconds;

        return time;

    }

    //--- Pause Timer
    //---   Clear the timer interval
    function pause()
    {

        if(typeof timerInterval != 'undefined')
        {

            $('#timerSpan').addClass('pauseClass');
            clearInterval(timerInterval);

            if(isReader)
            {
                $('.pauseChatAnchor').hide();
                $('.resumeChatAnchor').show();
            }

        }

    }

    //--- Reusume Chat After Pause
    //--- Takes care of some UI elements that starding START ( timer() ) does not
    function startResumeChat()
    {

        $('#timerSpan').removeClass('pauseClass');
        timer();

        if(isReader)
        {


            $('.pauseChatAnchor').show();
            $('.resumeChatAnchor').hide();

        }
        else
        {

            $('.pauseChatAnchor').hide();
            $('.resumeChatAnchor').hide();

        }

    }

    //--- Disable form fields
    function disableChatForm()
    {

        $('#chatForm textarea').attr('disabled','disabled');
        $('#chatForm input').attr('disabled','disabled');

    }

    //--- Enable form fields
    function enableChatForm()
    {

        $('#chatForm textarea').removeAttr('disabled');
        $('#chatForm input').removeAttr('disabled');

    }

    //--- Record Chat Length
    function record_chat(callback)
    {
        
        if(readerListening)
        {

            if(isReader)
            {

                $.get('/chat/chatInterface/record_chat/' + chatSessionId + '/' + recordChatIntervalSeconds, function()
                {

                    if(typeof callback != 'undefined') callback();

                });

            }

        }
        else
        {

            if(!isReader)
            {

                $.get('/chat/chatInterface/record_chat/' + chatSessionId + '/' + recordChatIntervalSeconds, function()
                {

                    if(typeof callback != 'undefined') callback();

                });

            }

        }

    }

    function banUser(ban_type){
        $.get('/chat/chatInterface/banUser/' + reader_id + '/' + client_id + '/' + ban_type);
    }

    function refundChat(){

        if(isReader){
            $.get('/chat/chatInterface/refund_chat/' + chatSessionId);
        }

    }

    //--- Charge for chat
    //--- Determine what user is still connected
    //--- First check if expert is still listening
    //--- Else use the client

    function endChat(){

        hideMenu();

        if(readerListening){

            if(isReader){
                $.get('/chat/chatInterface/end_chat/' + chatSessionId);
            }

        }

    }

    function showLostTime(toggle)
    {
        if(toggle == true)
        {
            $("#lostTime").show();
        }
        else
        {
            $("#lostTime").hide();
        }
    }

    function hideMenu(){
        $('#chatMenu').hide();
    }

    //--- Close Chat Window
    function closeWindow(timeout_var){
        timeout_var = timeout_var || window_close_time;
        objectWrite({ type : 'message', 'className' : 'system', 'message' : "This chat window will close in " + (timeout_var/1000) + " seconds..." });

        setTimeout(function(){
            window.close();
        }, timeout_var);

    }

    window.onbeforeunload = function(){

        if(!isReader && reader_missing){
            opener.location.replace("/chat/main/chat_options/client/" + chatSessionId);
        }

    }