
    //--- Listen for reader statuses
    var onlineMembers = {};
    var status_socket = new RampNode('http://psycon.rampnode.com/readerStatuses');

    status_socket.on('message', function(stringObject){

        var object = JSON.parse(stringObject);
        var status = object.status;
        var username = object.username;

        if(status == 'offline'){

            push_offline(username);

        }else{

            pull_online(username);

        }

    });

    //--- Manual first call to check online statuses
    $(function(){

        $.get('/main/check_all_reader_statuses', function(object){

            $.each(object, function(key, value){

                if(value == 'offline'){

                    push_offline(key);

                }else{

                    pull_online(key);

                }

            });

        }, 'json');

    });

    //--- Pull a member online
    function pull_online(username){

        $('#online_container').append("<div class='reader_box' data-reader-username='"+username+"'>"+username+"</div>");

    }

    //--- Push a member offline
    function push_offline(username){
        $("#online_container .reader_box[data-reader-username='"+username+"']").slideUp('fast', function(){
            $(this).remove();
        })
    }