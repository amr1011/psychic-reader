
requirejs.config({
    //By default load any module IDs from js/lib
    baseUrl: '/chat/test',
    
    paths: {
        jquery: "//code.jquery.com/jquery-1.11.0.min", // Local
        'step': '/chat/test/step'
    },
    config: {
        step: {
            steps: [
                ['chat'],
                ['chat_views'],
                ['chat_manager'],
                ['chat_manager_room'],
                ['folder1/chat_folder1_test', 'folder1/chat_folder1_test2',],
                ['chat_test']
               
                
                
            ]
        }
    }
});



requirejs(['step!chat_test'], function(){
    console.log("Done");
    Chat.init({a:"aaa", b:"bbb"});
    Chat.test.testing();
    Chat.manager.call();
    Chat.manager.room.call();
    Chat.folder1.test.testing();
})

/*
requirejs.config({
    //By default load any module IDs from js/lib
    baseUrl: '/chat/test',
    
    paths: {
        jquery: "//code.jquery.com/jquery-1.11.0.min" // Local
    }
});



requirejs(['chat', 'chat_views', 'chat_test', 'chat_manager', 'chat_manager_room'], function(){
    console.log("Done");
    Chat.init({a:"aaa", b:"bbb"});
    Chat.test.testing();
    Chat.manager.call();
    Chat.manager.room.call();
})

/*
 * 
 * requirejs(['simple-vp'], function(simple){
    console.log("Done: " + simple.color);
    
})

requirejs(['chat', 'chat_views', 'chat_manager', 'chat_manager_room', 'chat_test'], function(Chat){
    console.log("Done");
    //Chat.init({a:"aaa", b:"bbb"});
})
/*
// Start the main app logic.
requirejs(['jquery', 'underscore-min'],
function   ($,        underscore) {
    //jQuery, canvas and the app/sub module are all
    //loaded and can be used here now.
    console.log("Done");
//    Chat.init({a:"aaa", b:"bbb"});
});
*/