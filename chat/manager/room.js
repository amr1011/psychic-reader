Chat.manager.room = {
    
    chat_id: null,
    chat_session_id: null,
    server_chat_time: 0,
    max_chat_length: 0,   // second   max time set. 
    chat_length: 0,     // current time elasped
    time_balance: 0,    // remaining time. 
    stored_time: -1,    // stored time. 
    
    client_info:{},
    reader_info:{},
    
    init: function(data) {
        console.log("room manager init");
        this.chat_id = data.chat_id || '';
        this.chat_session_id = data.chat_session_id || '';
        
        this.max_chat_length = data.max_chat_length || 0;
        this.chat_length = data.chat_length || 0;
        this.time_balance = data.time_balance || 0;
        
        this.client_info.username = data.client_username || '';
        this.client_info.first_name = data.client_first_name || '';
        this.client_info.last_name = data.client_last_name || '';
        this.client_info.dob = data.client_dob || '';
        
        this.reader_info.username = data.reader_username || '';
        
        // register dispatcher event.
        Chat.manager.room.dispatcher();
        // disable chat form frist, enable it when chat start. 
        Chat.views.message.system.disable_chat_form();
        
        // adjust chat input message box UI
        Chat.views.message.ui.adjust_input_message_textarea();
    },
    
    timer: {
        
        ok_send_typing: true,
        send_typing_interval: 3000, // 3 seconds
        sending_typing_timer: function() {
            var ok_send_typing = Chat.manager.room.timer.ok_send_typing;
            if (Chat.manager.room.timer.ok_send_typing) {
                Chat.manager.room.timer.ok_send_typing = false;
                setTimeout(function() {
                    Chat.manager.room.timer.ok_send_typing = true;
                }, Chat.manager.room.timer.send_typing_interval);
            }
            return ok_send_typing;
        },
        
        ok_display_typing: true,
        receive_typing_interval: 5000,
        receive_typing_timer: function() {
            var ok_display_typing = Chat.manager.room.timer.ok_display_typing;
            if (Chat.manager.room.timer.ok_display_typing) {
                Chat.manager.room.timer.ok_display_typing = false;
                setTimeout(function() {
                    if (Chat.manager.room.timer.ok_display_typing == false) {
                        // remove typing text. 
                        Chat.views.message.system.clear_typing();
                        Chat.manager.room.timer.ok_display_typing = true;
                    }
                }, Chat.manager.room.timer.receive_typing_interval);
            }
            return ok_display_typing;
        },
        
        reset_ok_display_typing: function() {
            // clear text
            Chat.views.message.system.clear_typing();
            Chat.manager.room.timer.ok_display_typing = true;
        },
        
        chat_timer_interval: 1000, // 1 second
        chat_timer_handler:null,
        chat_timer_record_interval: 5, // in  seconds
        chat_timer_correction_threshold: 5, // allowable client-server time diff
        chat_timer_has_started_before: false,
        
        init_chat_timer: function() {
            Chat.views.message.system.set_timer(Chat.manager.room.max_chat_length);
        },
        
        // start or resume
        start_chat_timer: function() {
            $('.pauseChatAnchor').show();
            $('.resumeChatAnchor').hide();
            Chat.views.message.system.enable_chat_form();
            //. if he has started, call start means resume
            if(Chat.manager.room.timer.chat_timer_has_started_before) {
                //Chat.views.message.system.resume();
            } else {
                // if he hasn't started before, if chat_length <= 0 means it is a fresh start.. else chat_length > 0 means user has been disconnected and rejoin. 
                if(Chat.manager.room.chat_length <= 0) {
                    console.log("Chat length <0, start ");
                    Chat.views.message.system.start();
                } else {
                    //Chat.views.message.system.resume();
                }
            }
            Chat.manager.room.timer.chat_timer_has_started_before = true;
            
            Chat.manager.room.timer.chat_timer_handler = setInterval(Chat.manager.room.timer.run_chat_timer, Chat.manager.room.timer.chat_timer_interval ); 
        },
        pause_chat_timer: function(paused_by) {
            $('.pauseChatAnchor').hide();
            $('.resumeChatAnchor').show();
            if (paused_by && paused_by != 'reader') {
	            Chat.views.message.system.clear_typing();
	            Chat.views.message.system.disable_chat_form();
            }
            clearInterval(Chat.manager.room.timer.chat_timer_handler);
            Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
        },
        resume_chat_timer: function() {
            Chat.manager.room.timer.start_chat_timer();
            Chat.manager.room.timer.unset_check_added_time();
        },
        end_chat_timer: function(is_soft_stop) {
            try {
                clearInterval(Chat.manager.room.timer.chat_timer_handler);
            } catch (e) {
                Chat.views.error_log.log(e);
            }
            
            
            $('.pauseChatAnchor').hide();
            $('.resumeChatAnchor').hide();
            Chat.views.message.system.clear_typing();
            Chat.views.message.system.disable_chat_form();
            Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
            
            if (is_soft_stop !== true) {
            	if (Chat.models.member.member_type == 'client') {
                	Chat.controller.room.close_window(15000);
              	} else {
              		setTimeout(function() {window.location.href = "/my_account";}, 15000);
              		
              	}
            } 
            
        },
        reset_chat_timer: function() {
            
        }, 
        
        correction: function() {
            //
            return; // TODO
            var time_diff = Chat.manager.room.chat_length - Chat.manager.room.server_chat_time;
            console.log("Check Timer Correction, times: " + Chat.manager.room.chat_length + " - " + Chat.manager.room.server_chat_time );
            if (Math.abs(time_diff) >= Chat.manager.room.timer.chat_timer_correction_threshold) {
                // correct to server time
                Chat.manager.room.chat_length = Chat.manager.room.server_chat_time;
                // show a message for time correction as system 
                Chat.views.message.system.show_message({message_type:'system', message:'Chat time correction by system monitor'});
                
                Chat.manager.room.time_balance = Chat.manager.room.time_balance +  time_diff;
                console.log("Proceed  Timer Correction , chat time: " + Chat.manager.room.chat_time);
                Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
            }
        },
        
        force_corretion: function(server_chat_time) {
            console.log("Force Timer Correction ");
            Chat.manager.room.server_chat_time = server_chat_time;
            var time_diff = Chat.manager.room.chat_length - Chat.manager.room.server_chat_time;
            
            if (Math.abs(time_diff) > 0) {
                Chat.manager.room.chat_length = Chat.manager.room.server_chat_time;
                // show a message for time correction as system 
                Chat.views.message.system.show_message({message_type:'system', message:'Chat time correction by system monitor'});
                
                Chat.manager.room.time_balance = Chat.manager.room.time_balance +  time_diff;
                Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
            }
        },
        
        run_chat_timer: function() {
            //--- Every set interval record chat length - !! ONLY IF READER
            console.log("MOD " + Chat.manager.room.chat_length + " > " + Chat.manager.room.chat_length%Chat.manager.room.timer.chat_timer_record_interval);
            if (Chat.manager.room.time_balance > 0 && 
                Chat.manager.room.time_balance % Chat.manager.room.timer.chat_timer_record_interval == 0 && 
                Chat.models.member.member_type == 'reader'){
                // send record chat time
                console.log("Send record chat");
                Chat.manager.room.send.record_chat();
            }

            // 5 minutes warning
            if (Chat.manager.room.time_balance == 300){
                //objectWrite({ type : 'message', 'className' : 'system', 'message' : "5 Minute Warning: There are only 5 minutes left in your current chat session.<br />We recommend purchasing more time using the settings menu at the top. " });
                //Chat.manager.room.send.message({message_type:'system', message: "5 Minute Warning: There are only 5 minutes left in your current chat session.<br />We recommend purchasing more time using the settings menu at the top. " });
                if (Chat.models.member.member_type == 'client'){
                    Chat.views.message.system.show_message({message_type:'system', message: "5 Minute Warning: There are only 5 minutes left in your current chat session.<br />We recommend purchasing more time or add stored time using the settings menu at the top. " });
                }
            } else if (Chat.manager.room.time_balance == 120){
                // 2 minutes warning
                //objectWrite({ type : 'message', 'className' : 'system', 'message' : "2 Minute Warning: There are only 2 minutes left in your current chat session.<br />We recommend purchasing more time using the settings menu at the top. " });
                //Chat.manager.room.send.message({message_type:'system', message: "2 Minute Warning: There are only 2 minutes left in your current chat session.<br />We recommend purchasing more time using the settings menu at the top. " });
                if (Chat.models.member.member_type == 'client'){
                    Chat.views.message.system.show_message({message_type:'system', message: "2 Minute Warning: There are only 2 minutes left in your current chat session.<br />We recommend purchasing more time or add stored time using the settings menu at the top. " });
                }
            } else if (Chat.manager.room.time_balance == 60){
                // 1 minute warning
                //objectWrite({ type : 'message', 'className' : 'system', 'message' : "1 Minute Warning: There is only 1 minute left in your current chat session.<br />We recommend purchasing more time using the settings menu at the top. " });
                //Chat.manager.room.send.message({message_type:'system', message: "1 Minute Warning: There is only 1 minute left in your current chat session.<br />We recommend purchasing more time using the settings menu at the top. " });
                if (Chat.models.member.member_type == 'client'){
                    Chat.views.message.system.show_message({message_type:'system', message: "1 Minute Warning: There is only 1 minute left in your current chat session.<br />We recommend purchasing more time or add stored time using the settings menu at the top. " });
                } else {
                    Chat.views.message.system.show_message({message_type:'system', message: "Client is running out of time in one minute. "});
                }
            } else if(Chat.manager.room.time_balance <= 0){
                Chat.manager.room.timer.end_chat_timer(true);   // just softly end chat for both reader and client. 
                // Timer Ends
                if(Chat.models.member.member_type == 'reader'){
                   // just end it. 
                    Chat.manager.room.send.leave_chat();
                } 
            }
            
            if (Chat.manager.room.time_balance == 0) {
                Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
            } else if (Chat.manager.room.time_balance > 0) {
                Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
                Chat.manager.room.time_balance -= (Chat.manager.room.timer.chat_timer_interval/1000);      // Remaining TIME LEFT
                Chat.manager.room.chat_length += (Chat.manager.room.timer.chat_timer_interval/1000);    // Time also elapsed. 
            } else {
                //Chat.views.message.system.set_timer(Chat.manager.room.chat_time);
                Chat.manager.room.time_balance = 0;
            }
        },
        update_time_balance: function(data) {
           // there is a socket message to update time balance 
            // ignore for now. 
        },
        
        disconnection_handler: null,
        default_grace_period: 60,
        is_disconnected_on: false,
        start_disconnection_countdown: function(grace_period) {
            if (!grace_period) {
                grace_period = Chat.manager.room.timer.default_grace_period;
            }
            grace_period *= 1000; // turn into milli second
            Chat.manager.room.timer.is_disconnected_on = true;
            
            setTimeout(Chat.manager.room.timer.run_disconnection_countdown, grace_period ); 
        },
        set_disconnection_off: function() {
            Chat.manager.room.timer.is_disconnected_on = false;
        },
        run_disconnection_countdown: function() {
            if (Chat.manager.room.timer.is_disconnected_on) {
                // fire end Chat.
                Chat.manager.room.send.leave_chat();
            }
        },
        
        check_added_time_timer_interval: 5000, // 5 seconds
        check_added_time_timer_handler:null,
        // check for added time timer
        set_check_added_time: function() {
            if (Chat.manager.room.timer.check_added_time_timer_handler) {
                // do nothing
            } else {
                Chat.manager.room.timer.check_added_time_timer_handler = setInterval( Chat.manager.room.timer.update_added_time, Chat.manager.room.timer.check_added_time_timer_interval);
            }
        },
        
        unset_check_added_time: function() {
            if (Chat.manager.room.timer.check_added_time_timer_handler) {
                clearInterval(Chat.manager.room.timer.check_added_time_timer_handler);
            }
        },
        update_added_time: function () {
            // call the get stored time. 
            Chat.manager.room.send.refresh_stored_time();
        }
        /* ,
        
        chat_pending_max_time: 45,
        chat_pending_timer_interval: 1000,
        chat_pending_timer_handler: null,
        set_chat_pending_timer: function() {
        	if (! Chat.manager.room.timer.chat_pending_timer_handler) {
        		Chat.manager.room.timer.chat_pending_timer_handler = setInterval(Chat.manager.room.timer.update_chat_pending_time, Chat.manager.room.timer.chat_pending_timer_interval);
        	}
        },
        unset_chat_pending_timer: function() {
        	if (Chat.manager.room.timer.chat_pending_timer_handler) {
        		Chat.manager.room.timer.chat_pending_max_time = 0;
                clearInterval(Chat.manager.room.timer.chat_pending_timer_handler);
            }
        },
        
        update_chat_pending_time: function() {
        	// update the pending time. 
        	if (Chat.manager.room.timer.chat_pending_max_time > 0) {
	        	Chat.manager.room.timer.chat_pending_max_time --;
	        	
	        	Chat.views.message.client.update_wait_time(Chat.manager.room.timer.chat_pending_max_time);
        	} else {
        		Chat.manager.room.timer.chat_pending_max_time = 0;
        		Chat.views.message.client.update_wait_time(Chat.manager.room.timer.chat_pending_max_time);
        		Chat.manager.room.timer.unset_chat_pending_timer();
        	}
        }
        */
    },
    
    send: {
        
        join_new_chat_room: function() {
            console.log('chat id' + Chat.manager.room.chat_id + " chat session id: " + Chat.manager.room.chat_session_id);
            var input = {
                chat_id: Chat.manager.room.chat_id,
                chat_session_id: Chat.manager.room.chat_session_id,
                member_hash: Chat.models.member.member_hash
            };
            console.log("Sending join new chat room ");
            Chat.models.socket.io.emit('join', input, function(data) {
                Chat.views.error_log.log(data); // just log
                // show message
                if (data.status) {
                    // 
                    Chat.manager.room.timer.init_chat_timer();
                    Chat.views.message.client.join_new_chat_room();
    

                } else {
                    Chat.views.error_log.show(data);
                }
            });
        }, 
        
        leave_chat: function () {
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash
            };
            Chat.models.socket.io.emit('leave', input, function(data){
                if (data.status) {
                    // TODO: adjust time.
                    Chat.manager.room.timer.end_chat_timer();
                    Chat.views.message.system.leave_chat_room(data.member_type);
                } else {
                    Chat.views.error_log.show(data);
                }
            });
        },
        
        start: function() {
            console.log("start emit " );
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash
            };
            Chat.models.socket.io.emit('start', input, function(data){
                console.log("Received Start ");
                Chat.views.error_log.log(data);
                if (data.status) {
                    // UI
                    $('.endChatAnchor').show();
                    $('.addStoredTime').show();
                    var start_time = data.start_time; // in seconds
                    
                    Chat.manager.room.max_chat_length = data.max_chat_length;
                    Chat.manager.room.chat_length = data.chat_length;
                    Chat.manager.room.time_balance = data.time_balance;

                    Chat.manager.room.timer.start_chat_timer();
                } else {
                    Chat.views.error_log.show(data);
                }
            });
            
        },
        
        pause: function() {
            console.log("pause emit " );
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash
            };      
            Chat.models.socket.io.emit('pause', input, function(data){
                if (data.status) {
                    Chat.manager.room.chat_length = data.chat_length;
                    Chat.manager.room.time_balance = data.time_balance;
                    
                    Chat.manager.room.timer.pause_chat_timer(data.paused_by);
                    
                    var username = '';
                    if (data.member_type == 'client') {
                        username = Chat.manager.room.client_info.username;
                    } else {
                        username = Chat.manager.room.reader_info.username;
                    }
                    Chat.views.message.system.pause(username);
                } else {
                    Chat.views.error_log.show(data);
                }
            });
            
        },
        
        resume: function(data) {
            Chat.views.message.system.show_add_stored_time(false);
            Chat.views.message.system.show_lost_time(false);
            console.log("resume emit " );
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash
            };
            Chat.models.socket.io.emit('resume', input, function(data){
                if (data.status) {
                    Chat.manager.room.chat_length = data.chat_length;
                    Chat.manager.room.time_balance = data.time_balance;
                    
                    Chat.manager.room.timer.resume_chat_timer();
                     
                    var username = '';
                    if (data.member_type == 'client') {
                        username = Chat.manager.room.client_info.username;
                    } else {
                        username = Chat.manager.room.reader_info.username;
                    }
                    Chat.views.message.system.resume(username);
                    
                } else {
                    Chat.views.error_log.show(data);
                }
            });
            
        },
        
        message: function(data) {
            console.log("message emit " );
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,
                message     : data.message
            };
            if (data.message_type) {
                input.message_type = data.message_type;
            }
            
            Chat.models.socket.io.emit('message', input, function(data){
                //console.log("Message is emitted. response is "+ JSON.stringify(data) );
                if (data.status) {
                    //var time_balance = data.time_balance; // in seconds
                    //Chat.Message.client.leave_chat(time_balance);
                    if (Chat.models.member.member_type == "reader") {
                        
                        Chat.views.message.system.show_message({member_type:'reader', message:input.message, sender_name:data.sender_name});
                    } else {
                        Chat.views.message.system.show_message({member_type:'client', message:input.message, sender_name:data.sender_name});
                    }
                    
                } else {
                    Chat.views.error_log.show(data);
                }
            });
        },
        
        typing: function(data) {
            console.log("Typing emit " );
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash
            };
            
            if (Chat.manager.room.timer.sending_typing_timer() ) {
                // 
                Chat.models.socket.io.emit('typing', input, function(data){
                    if (data.status) {
                       // do not show anything in sender.  NOt even error
                    } else {
                        // just log error , not show
                        Chat.views.error_log.log(data);
                    }
                });
            }
            
        },
        
        record_chat: function(data) {
            console.log("Typing record_chat " );
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash
            };
            
            Chat.models.socket.io.emit('record_chat', input, function(data){
                if (data.status) {
                   Chat.manager.room.time_balance = data.time_balance;
                   Chat.manager.room.chat_length = data.chat_length;
                   Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
                   //Chat.manager.room.timer.correction();
                } else {
                    // just log error , not show
                    Chat.views.error_log.log(data);
                }
            });
        },
        
        contact_admin: function() {
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash
            };
            
            Chat.models.socket.io.emit('contact_admin', input, function(data){
                // do nothing, just notify the reader
                if (data.status) {
                
                    Chat.manager.room.chat_length = data.chat_length;
                    Chat.manager.room.time_balance = data.time_balance;
                    
                    Chat.manager.room.timer.pause_chat_timer(data.paused_by);
                    
                    var username = Chat.manager.room.client_info.username;
                    Chat.views.message.system.pause(username);
                    
                } else {
                    Chat.views.error_log.show(data);
                }
            });
            
            Chat.views.message.client.show_contact_admin();
            //Chat.views.popup.open_site_page('contact');
        },
        
        purchase_time: function(data) {
            console.log("notifying purchase time " );
            
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash
            };
            // move to display the popup first. 
            //Chat.views.popup.open_purchase_more_time_window();
            
            Chat.models.socket.io.emit('purchase_time', input, function(data){
                if (data.status) {
                    if (Chat.models.member.member_type == 'client') {
                        Chat.views.message.client.purchase_more_time();
                        //Chat.views.popup.open_purchase_more_time_window();
                        Chat.manager.room.timer.set_check_added_time();
                        $('#refreshPurchasedTime').show();
                    }
                } else {
                    // just log error , not show
                    Chat.views.error_log.log(data);
                }
                
                // pause timer display
                if (data.status == true && data.is_already_paused !== false) {
                    // pause
                    if (data.chat_length && data.time_balance) {
                        Chat.manager.room.chat_length = data.chat_length;
                        Chat.manager.room.time_balance = data.time_balance;
                    }
                    Chat.manager.room.timer.pause_chat_timer(data.paused_by);
                } 
            });
        },
        
        get_stored_time: function() {
            Chat.manager.room.send.pause();
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,
                is_bypass_message : false
            };
            
            Chat.models.socket.io.emit('get_stored_time', input, function(data){
                if (data.status) {
                    var stored_minute = Math.floor(data.stored_time); 
                    Chat.manager.room.stored_time = stored_minute;
                        
                    Chat.views.message.system.show_add_stored_time(true);
                    $('#refreshPurchasedTime').hide();
                    Chat.views.message.system.set_stored_time(data.stored_time);
                } else {
                    // just log error , not show
                    Chat.views.error_log.log(data);
                }
            });
        },
        
        refresh_stored_time: function() {
            //Chat.manager.room.send.pause();
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,
                is_bypass_message : true
            };
            
            Chat.models.socket.io.emit('get_stored_time', input, function(data){
                if (data.status) {
                    var stored_minute = Math.floor(data.stored_time); 
                    if ( Chat.manager.room.stored_time != stored_minute ) {
                        Chat.manager.room.stored_time = stored_minute;
                        Chat.views.message.system.set_stored_time(data.stored_time);
                    } 
                } else {
                    // just log error , not show
                    Chat.views.error_log.log(data);
                }
            });
        },
        
        
        add_stored_time: function(data) {
            console.log("notifying add stored time " );
            
            var added_time = $('#remainingStoredTimeInput').val();
            
            if (added_time <= 0) {
                Chat.views.error_log.show("You cannot add time less than one minute");
                return;
            }
            
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,
                added_time  : added_time
            };
            
            Chat.models.socket.io.emit('add_stored_time', input, function(data){
                if (data.status) {
                    Chat.manager.room.max_chat_length = data.max_chat_length;
                    Chat.manager.room.chat_length = data.chat_length;
                    Chat.manager.room.time_balance = data.time_balance;
                    Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
                } else {
                    // just log error , not show
                    Chat.views.error_log.log(data);
                }
                // resume
                    Chat.manager.room.send.resume();
            });
        },
        
        add_free_time: function() {
            var free_time = $('#freeTimeInput').val();
            
            if (free_time <= 0) {
                Chat.views.error_log.show("You cannot add free time less than one minute");
                return;
            }
            
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,
                free_time  : free_time
            };
            
            Chat.models.socket.io.emit('add_free_time', input, function(data){
                if (data.status) {
                    Chat.manager.room.max_chat_length = data.max_chat_length;
                    Chat.manager.room.chat_length = data.chat_length;
                    Chat.manager.room.time_balance = data.time_balance;
                    Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
                    Chat.views.message.reader.add_free_time(data.total_free_time, data.free_time_added);
                    $('#freeTimeInput').val(0);
                    Chat.views.message.system.show_add_free_time(false);
                } else {
                    // just log error , not show
                    Chat.views.error_log.log(data);
                }

            });
        },
        
        pban: function(data) {
            console.log("pban " );
            console.log("caht " );
           
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash
            };
            
            Chat.models.socket.io.emit('pban', input, function(data){
                if (data.status) {
                    Chat.views.message.reader.pban();
                    Chat.manager.room.timer.end_chat_timer();
                } else {
                    // just log error , not show
                    Chat.views.error_log.log(data);
                }
            });
        },
        
        fban: function(data) {
            console.log("fban " );
           
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash
            };
            
            Chat.models.socket.io.emit('fban', input, function(data){
                if (data.status) {
                    Chat.views.message.reader.fban();
                    Chat.manager.room.timer.end_chat_timer();
                } else {
                    // just log error , not show
                    Chat.views.error_log.log(data);
                }
            });
        },
        
        refund: function() {
            console.log("refund " );
           
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash
            };
            
            Chat.models.socket.io.emit('refund', input, function(data){
                if (data.status) {
                    Chat.views.message.reader.refund();
                    Chat.manager.room.timer.end_chat_timer();
                } else {
                    // just log error , not show
                    Chat.views.error_log.log(data);
                }
            });
        },
        
        lost_time: function(lost_time) {
            console.log("lost_time " );
           
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash,
                lost_time   : (lost_time * 60) /* in second */
            };
            
            Chat.models.socket.io.emit('lost_time', input, function(data){
                if (data.status) {
                    Chat.manager.room.chat_length = data.chat_length;
                    Chat.manager.room.time_balance = data.time_balance;
                    Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
                    Chat.views.message.system.show_lost_time(false);
                    Chat.manager.room.send.resume();
                } else {
                    // just log error , not show
                    Chat.views.error_log.log(data);
                }
            });
        },
        
        check_time_balance: function(callback) {
            console.log("Typing emit " );
            var input = {
                room_name   : Chat.manager.room.chat_session_id,
                member_hash : Chat.models.member.member_hash
            };
            
            if (Chat.manager.room.timer.sending_typing_timer() ) {
                // 
                Chat.models.socket.io.emit('check_time_balance', input, function(data){
                    if (data.status) {
                       // do not show anything in sender.  NOt even error
                       if (callback) {
                           Chat.manager.room.max_chat_length = data.max_chat_length;
                           Chat.manager.room.chat_length = data.chat_length;
                           Chat.manager.room.chat_time = data.time_balance;
                           callback(null, data);
                       } else {
                           return data;
                       }
                    } else {
                        // just log error , not show
                        Chat.views.error_log.log(data);
                        if (callback) {
                            callback(data);
                        }
                    }
                });
            }
        }
        
    },
    
    // receiving message function
    receive: {
        // on receiving the call for join or create new chat room 
        
        reader_join_new_chat_room: function(data) {
            console.log(" raeder join new chat room");
            Chat.views.error_log.log(data);
            var reader_id = data.reader_id;
            
            // then it should send start chat now. 
            Chat.manager.room.timer.init_chat_timer();
            Chat.manager.room.send.start();
        },
        
        reject_chat: function(data) {
            console.log(" reader reject the chat request");
            
            Chat.views.message.system.disable_chat_form();
            Chat.views.message.client.reject_chat(data.reader_username);
            Chat.controller.room.close_window();
            //if(object.chat_id == chatSessionId){
        },
        
        abort_chat_room: function(data) {
            console.log(" client abort the chat request");
            if (Chat.models.member.member_type == "client") {
            	Chat.views.message.system.disable_chat_form();
	            Chat.views.message.client.abort_chat();
	            Chat.controller.room.close_window();
            } else {
            	Chat.views.message.reader.abort_chat();
            }
            //if(object.chat_id == chatSessionId){
        },
        
        leave_chat_room: function(data) {
            if (data.status) {
                Chat.manager.room.timer.end_chat_timer();
                Chat.views.message.system.leave_chat_room(data.member_type);
            } else {
                Chat.views.error_log.show(data);
            }
            
        },
        start: function(data) {
            console.log("start now " + data);
            
            $('.endChatAnchor').show();
            $('.addStoredTime').show();
            $('.pauseChatAnchor').show();
            $('.resumeChatAnchor').hide();
            
            Chat.manager.room.max_chat_length = data.max_chat_length;
            Chat.manager.room.chat_length = data.chat_length;
            Chat.manager.room.chat_time = data.time_balance;
            Chat.manager.room.timer.start_chat_timer();
        },
        
        pause: function(data) {
            console.log("pause now " + data);
            if (data.status) {
                if (! data.pause_no_time) {
                    Chat.manager.room.chat_length = data.chat_length;
                    Chat.manager.room.time_balance = data.time_balance;
                }
                Chat.manager.room.timer.pause_chat_timer(data.paused_by);
                
                var username = '';
                if (data.member_type == 'client') {
                    username = Chat.manager.room.client_info.username;
                } else {
                    username = Chat.manager.room.reader_info.username;
                }
                
                if (data.is_by_disconnection) {
                    Chat.views.message.system.disconnect_pause(username);
                } else {
                    Chat.views.message.system.pause(username);
                }
                
            } else {
                Chat.views.error_log.show(data);
            }
        },
        
        resume: function(data) {
            console.log("resume now " + data);
            if (data.status) {
                Chat.manager.room.chat_length = data.chat_length;
                Chat.manager.room.time_balance = data.time_balance;
                
                Chat.manager.room.timer.resume_chat_timer();
                 
                var username = '';
                if (data.member_type == 'client') {
                    username = Chat.manager.room.client_info.username;
                } else {
                    username = Chat.manager.room.reader_info.username;
                }
                Chat.views.message.system.resume(username);
                
            } else {
                Chat.views.error_log.show(data);
            }
        },
        
        message: function(data) {
            //{sender:'client', sender_id:cleint_id, message:message}
            //console.log("receive message data: " + JSON.stringify(data));
            if (data.status) {
                //var time_balance = data.time_balance; // in seconds
                //Chat.Message.client.leave_chat(time_balance);
                Chat.manager.room.timer.reset_ok_display_typing();
                if (data.message_type && data.message_type == 'system') {
                    Chat.views.message.system.show_message({member_type:'system', message:data.message});
                } else if (data.sender == "reader") {
                    
                    Chat.views.message.system.show_message({member_type:'reader', message:data.message, sender_name:data.sender_name});
                } else {
                    Chat.views.message.system.show_message({member_type:'client', message:data.message, sender_name:data.sender_name});
                }
                
            } else {
                Chat.views.error_log.show(data);
            }
        },
        
        typing: function(data) {
            //console.log("receive typing data" + JSON.stringify(data));
            if (data.status) {
                if (data.sender_name) {
                    if (Chat.manager.room.timer.receive_typing_timer() ) {
                        Chat.views.message.system.show_typing(data.sender_name);
                    }
                }
            }
        },
        
        record_chat: function(data) {
            //console.log("receive record_chat" + JSON.stringify(data));
            // record chat time  
            if (data.status) {
                Chat.manager.room.time_balance = data.time_balance;
                Chat.manager.room.chat_length = data.chat_length;
                Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
            }
        },
        
        
        
        disconnect_chat_room: function(data) {
            console.log("disconnect_chat_room " + data);
            // pause Timer First
            Chat.manager.room.timer.pause_chat_timer();
            if (data.time_balance) {
                Chat.manager.room.timer.force_corretion(data.time_balance);
            }
            Chat.manager.room.timer.start_disconnection_countdown(data.grace_period);
        },
        
        client_purchase_time: function(data) {
            if (data) {
                if (data.status == true && data.is_already_paused !== false) {
                    // pause
                    if (data.chat_length && data.time_balance) {
                        Chat.manager.room.chat_length = data.chat_length;
                        Chat.manager.room.time_balance = data.time_balance;
                    }
                    Chat.manager.room.timer.pause_chat_timer(data.paused_by);
                } 
                
                if (Chat.models.member.member_type == 'reader') {
                    Chat.views.message.reader.purchase_more_time();
                }
            }
        },
        client_add_stored_time: function(data) {
            if (data.status) {
                Chat.manager.room.max_chat_length = data.max_chat_length;
                Chat.manager.room.chat_length = data.chat_length;
                Chat.manager.room.time_balance = data.time_balance;
                Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
            }
        },
        get_stored_time: function(data) {
            if (data.status && data.is_bypass_message === false) {
                Chat.views.message.system.get_stored_time(data.stored_time);  
            }
            
        },
        reader_add_free_time: function (data) {
            if (data.status) {
                Chat.manager.room.max_chat_length = data.max_chat_length;
                Chat.manager.room.chat_length = data.chat_length;
                Chat.manager.room.time_balance = data.time_balance;
                Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
                Chat.views.message.client.add_free_time(data.total_free_time, data.free_time_added);
            }  
        },
        contact_admin: function(data) {
            if (data.status) {
                Chat.manager.room.chat_length = data.chat_length;
                Chat.manager.room.time_balance = data.time_balance;
                Chat.manager.room.timer.pause_chat_timer(data.paused_by);
                
                Chat.views.message.reader.show_contact_admin();
            }
        },
        pban: function(data) {
            Chat.views.message.client.pban();
            Chat.manager.room.timer.end_chat_timer();
        },
        fban: function(data) {
            Chat.views.message.client.fban();
            Chat.manager.room.timer.end_chat_timer();
        }, 
        refund: function(data) {
            if (data.status) {
                Chat.views.message.client.refund();
                Chat.manager.room.timer.end_chat_timer();
            } else {
                Chat.views.error_log.show(data);
            }
        },
        lost_time: function(data) {
            if (data.status) {
                Chat.manager.room.chat_length = data.chat_length;
                Chat.manager.room.time_balance = data.time_balance;
                Chat.views.message.system.set_timer(Chat.manager.room.time_balance);
                Chat.views.message.system.show_lost_time(false);
                // there will be resume call. wait for it. 
            } else {
                Chat.views.error_log.show(data);
            }
        }
    },
    
    
    
    // socket message dispatcher
    dispatcher: function() {
        console.log("Dispatcher ");
        var io = Chat.models.socket.io; // Chat.socket
        
        io.on('reader_join_new_chat_room', Chat.manager.room.receive.reader_join_new_chat_room);
        io.on('reject_chat', Chat.manager.room.receive.reject_chat);		// OUTDATED
       // io.on('abort_chat_room', Chat.manager.room.receive.abort_chat_room);
        io.on('leave_chat_room', Chat.manager.room.receive.leave_chat_room);
        io.on('start', Chat.manager.room.receive.start);
        io.on('pause', Chat.manager.room.receive.pause);
        io.on('resume', Chat.manager.room.receive.resume);
        io.on('message', Chat.manager.room.receive.message);
        io.on('client_purchase_time', Chat.manager.room.receive.client_purchase_time);
        io.on('client_add_stored_time', Chat.manager.room.receive.client_add_stored_time);
        io.on('get_stored_time', Chat.manager.room.receive.get_stored_time);
        io.on('refund', Chat.manager.room.receive.refund);
        io.on('lost_time', Chat.manager.room.receive.lost_time);
        io.on('reader_add_free_time', Chat.manager.room.receive.reader_add_free_time);
        io.on('pban', Chat.manager.room.receive.pban);
        io.on('fban', Chat.manager.room.receive.fban);
        io.on('typing', Chat.manager.room.receive.typing);
        io.on('record_chat', Chat.manager.room.receive.record_chat);
        io.on('disconnect_chat_room', Chat.manager.room.receive.disconnect_chat_room);
        io.on('check_time_balance', Chat.manager.room.timer.update_time_balance);
        io.on('contact_admin', Chat.manager.room.receive.contact_admin);
    }
};
