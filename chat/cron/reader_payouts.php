<?php
require_once '../../main.config.php';
 $app_path = PROJECT_ROOT."/chat/";
/// $app_path = '../';
require_once ($app_path."common.php");
require_once ($app_path."inc/functions_payouts_new.php");
require_once ($app_path."inc/functions_user.php");
require_once ($app_path."inc/functions_common.php");

$conn = mysql_connect($dbhost, $dbusername, $dbuserpassword);
mysql_select_db($default_dbname);

$readerList = getReaderList();
/*
$temp = strtotime('-1 month');
#getPeriod();

//echo $temp."<br>";

$monthPart = (date("d") > 15)? "1" : "2";
$daysInMonth = date("t");

//echo "<br>monthPart =".$monthPart." date = ".date("d");

	
$curYear  = date("Y");
if ($monthPart == 1) 
{
	$dayStart = "01"; $dayEnd ="15";
	$curMonth = date("m");
}
else  
{
	$dayStart = "16";
	$curMonth = date("m",$temp);
	$daysInMonth = date("t",$temp);
	$dayEnd = $daysInMonth;
	if($curMonth==12)
		$curYear--;
}
*/
$period = getPeriod();    
$curYear=$period['curYear'];
$curMonth=$period['curMonth'];
$dayEnd=$period['dayEnd'];
$dayStart=$period['dayStart'];

$firstDate = $curYear."-".$curMonth."-".$dayStart;
$secDate = $curYear."-".$curMonth."-".$dayEnd;
//var_export($.readerList);
$adm_email_text = "Pay period ending: $curMonth/$dayEnd/$curYear"."\r\n";
$email_text = '';

$total_mail_text = 'Readers  total by '.(date('F',strtotime('-1 month')))."\r\n";

if($_GET['force'])
{
	$subject = 'Adjusted payroll totals';   
	$email_text = "Readers, below is your final total for the pay period ending: $curMonth/$dayEnd/$curYear"."\r\n\r\n";
}
else 
{
	$subject = 'Auto-Invoicing: proposed totals';
	$email_text = "Pay period ending: $curMonth/$dayEnd/$curYear"."\r\n".
		"NOTE: This is only current proposed totals: SUBJECT TO CHANGE WHEN ADMIN MAKES ANY NECESSARY ADJUSTMENTS"."\r\n\r\n";
	
}

$cur_period = date('j');

foreach ($readerList as $reader)
{
/// if($reader['rr_record_id'] == '19310') {	
			$readerPayoutData = findReaderPayouts($reader['rr_record_id'], $firstDate, $secDate);
		
			$readerPayoutData['Reader_id']	= $reader['rr_record_id'];
			$readerPayoutData['Month']		= $curMonth;
			$readerPayoutData['Period']		= $monthPart;
			$readerPayoutData['Year']		= $curYear;
			$readerPayoutData['Month_name'] = $month_name[$readerPayoutData['Month']+0];
			if ($readerPayoutData['duration'] > 1)
			{
				$stat_id = saveReaderStat($readerPayoutData);
				
				saveReaderInvoice($readerPayoutData, $stat_id);
				//email functionality
				//get all pending invoices and send it to email 
				
				$e_text = formReaderEmail($readerPayoutData['Reader_id']);
				
				$text2send = $email_text."You earned ".$e_text."\r\n";
				
				$reader_email = $reader['emailaddress'];
				
				PsyMailer::send($reader_email, $subject, nl2br($text2send));
				
				$adm_email_text .= "\r\nReader ".$reader['login']."(".$reader['emailaddress'].") has earned: \r\n".$e_text."\r\n";
				
				if($cur_period == 1) 
				{
					$total = getReaderMonthTotal($reader['rr_record_id']);
					$total_mail_text .= $reader['login'].'('.$reader['emailaddress'].") : \r\nUSD: ".$total['USD']."\r\nCAD: ".$total['CAD']."\r\n";
				}
			}
/// }	
}

PsyMailer::send(Config::adminMail(), $subject, nl2br($adm_email_text)); 
PsyMailer::send('karpovsoft@yandex.ru', $subject."to ".Config::adminMail(), nl2br($adm_email_text)); 

if($cur_period == 1) PsyMailer::send(Config::adminMail(),$subject, nl2br($total_mail_text));  
?>