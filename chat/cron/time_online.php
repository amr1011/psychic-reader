<?php

require_once '../../psy_config.php';
require_once '_cron_functions.php';

$adm_email = "rob@custom-coding.com";

$conn = mysql_connect($db_host, $db_user, $db_pass);
mysql_select_db($db_name);

$all_readers_info = '';
$found_records    = 0;

// Delete previous week time
$mysql = "DELETE FROM week_online_time WHERE 1";
mysql_query($mysql, $conn);

$strsql  = "
SELECT 
DISTINCT 
total_online_time.reader_id, 
total_online_time.time_online, 
total_online_time.last_reset, 
T1_1.name, 
T1_1.emailaddress 
FROM 
total_online_time left JOIN T1_1 on total_online_time.reader_id = T1_1.rr_record_id 
WHERE (T1_1.rr_record_id > 0) AND (DATE_ADD(total_online_time.last_reset, interval 4 day) < now()) 
ORDER BY total_online_time.time_online DESC";

psy_debug("strsql",$strsql);

$rs = mysql_query($strsql, $conn) or die(mysql_error());
while ($row = mysql_fetch_assoc($rs))
{

	$reader_id         = $row[reader_id];
	$time_online       = $row[time_online];
	$last_reset        = $row[last_reset];
	$name_reader       = $row[name];
	$emailaddress      = $row[emailaddress];

	$minutes_online    = $time_online/60;
	$hours_online      = ($minutes_online > 59) ? floor($minutes_online/60): 0;
	$minutes_online    =  $minutes_online - ($hours_online * 60);
	$minutes_online    = round($minutes_online);

	$since_time        = date("M j, g:i a (l)", strtotime($last_reset));
	$reader_info       = "$name_reader: $hours_online hours $minutes_online minutes online since $since_time<br>";
	$all_readers_info .= $reader_info;
	$found_records = 1;

	$subject = "DEAR $name_reader From JAYSON";
	
	$reader_note = 'Your total log on hours since '.$since_time.' are: <b>'.$hours_online.'</b> hours '.$minutes_online.' minutes<br>';
	
	if($hours_online < 10)
	{
		$reader_note .= 'YOU ARE NOT MEETING THE MINIMUM LOGON REQUIREMENT-<br>PLEASE CONTACT JAYSON IMMEDIATELY!';
	}
	else if($hours_online < 12)
	{
		$reader_note .= 'THANK YOU! But if possible - please log on more often.';
	}
	else
	{
		$reader_note .= 'THANK YOU! Keep up the good work!';
	}
	
	//mail($emailaddress, $subject, $reader_note, $headers);
	sendCronMail($name_reader,$emailaddress,$subject,$reader_note,1);
	
	//echo"$reader_note<br>";

	$mysql = "INSERT INTO week_online_time SET time_online = '".$time_online."', reader_id=".$reader_id;

psy_debug("mysql",$mysql);

	mysql_query($mysql, $conn);
	echo mysql_error();
	
}

mysql_free_result($rs);

if($found_records > 0)
{
// testing - uncomment after
/*
	$mysql = "UPDATE total_online_time set time_online = '0', last_reset = now()";
psy_debug("mysql",$mysql);
	mysql_query($mysql, $conn);
	echo mysql_error();
*/

	echo $all_readers_info;
	$text_2_send = $report;
	$subject = "PSYCHAT - Total log on hours report";
//	mail($adm_email, $subject, $all_readers_info, $headers);
	sendCronMail($adm_email,$adm_email,$subject,$all_readers_info,1);
	echo"<br>ok";
}

mysql_close($conn);
?>
