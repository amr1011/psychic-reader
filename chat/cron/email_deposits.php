<?php

/*

http://dev.psychic-contact.com/chat/cron/email_deposits.php

*/

require_once '../../psy_config.php';
require_once '_cron_functions.php';

$_DEBUG = 1;

// db connection
$conn = mysql_connect($db_host, $db_user, $db_pass);
mysql_select_db($db_name);


// select all deposit times
$strsql = "SELECT now() from deposits where 1 limit 0,1";
//psy_debug("strsql",$strsql);
$rs     = mysql_query($strsql, $conn) or die(mysql_error());
$row    = mysql_fetch_array($rs);
mysql_free_result($rs);

$all_info ='<br /> Now is : '.$row[0].'<br />';

$totalAmountByCurrency['USD'] = 0;
$totalAmountByCurrency['CAD'] = 0;

$tbps = array(); //totals by payment system
$tbps["Auth"] = 0;
$tbps["Paypal US"] = 0;
$tbps["Paypal CA"] = 0;
$tbps["IS"] = 0;
//$tbps[""] = 0;

$totals = array();
$totalAffiliates = '';

// select all daily deposits
$strsql = "SELECT * FROM deposits where DATE_ADD(Date, interval 24 hour) >= now() ORDER BY Order_numb ASC";
//psy_debug("strsql",$strsql);
$rs = mysql_query($strsql, $conn) or die(mysql_error());
while ($row = mysql_fetch_assoc($rs))
{

	$ID_tr      = $row["ID_tr"];
	$Deposit    = $row["Deposit"];
	$Client_id  = $row["Client_id"];
	$Amount     = $row["Amount"];
	$Order_numb = $row["Order_numb"];
	$Date       = $row["Date"];
	$Reader_id  = $row["Reader_id"];
	$Bmt        = $row["Bmt"];
	$Notes      = $row["Notes"];
	$Currency   = $row["Currency"];

	$totalAmountByCurrency[$Currency] += $Amount;
	$tbps[getPs($row)] += $Amount;

	$Date = strtotime($Date);
	$Date = date("M j, Y h:i a", $Date);

	$Bmt = (strlen($Bmt) > 0)?$Bmt:'no';
	$client_login ='';
	
	// if webphone exists in the Notes
	if(preg_match("/webphone/i", $Notes))
	{
		$strsql2 = "SELECT Client_username, Affiliate FROM wp_clients where Client_id = '$Client_id'";
//psy_debug("strsql2",$strsql2);
		$rs2 = mysql_query($strsql2, $conn) or die(mysql_error());
		while ($row2 = mysql_fetch_assoc($rs2))
		{
			$client_login     = $row2["Client_username"];
			$client_affiliate = $row2["Affiliate"];
		}
		mysql_free_result($rs2);
	}
	else
	{
		$strsql2 = "SELECT login, affiliate FROM T1_1 where rr_record_id = '$Client_id'";
//psy_debug("strsql2",$strsql2);
		$rs2 = mysql_query($strsql2, $conn) or die(mysql_error());
		while ($row2 = mysql_fetch_assoc($rs2))
		{
			$client_login     = $row2["login"];
			$client_affiliate = $row2["affiliate"];
		}
		mysql_free_result($rs2);
	}

	if(!empty($client_affiliate))
	{
		$totals['affiliate'][$client_affiliate]++;
	}
	else {
	    $client_affiliate = "GSW";
	}
	$totals['type'][$Notes]++;

	$all_info .= "<br>Client: $client_login<br>Amount: $Amount $Currency<br>Payment method: $Order_numb".(!empty($row['ds_order_numb'])?"  DS Order#: ".$row['ds_order_numb']:"")."<br>Order#".$Deposit."-".$client_affiliate." <br/>$Date<br>\nBMT: $Bmt<br>Notes: $Notes<br>Affiliate: $client_affiliate<br>--------------------<br>\n";

}// end while

//psy_debug("all_info",$all_info);

mysql_free_result($rs);
mysql_close($conn);


if(is_array($totals['affiliate']))
{
	foreach($totals['affiliate'] as $key=>$val)
	{
		$totalAffiliates .= $key.': '.$val.'<br>';
	}
}

if(is_array($totals['type']))
{
	foreach($totals['type'] as $key=>$val)
	{
		$totalTypes .= $key.': '.$val.'<br>';
	}
}

$totalPS = "";
foreach($tbps as $key=>$val)
{
	$totalPS .= '<b>'.$key.': '.$val.'</b><br>';
}

//echo"$all_info";

$text_2_send = "Total by currency:<br><b>".$totalAmountByCurrency['USD']."</b> USD<br><b>".$totalAmountByCurrency['CAD']."</b> CAD<br><br>
Total by payment system: <br /> 
$totalPS
$all_info<br>$totalAffiliates<br>--------------------<br>$totalTypes";

psy_debug("text_2_send",$text_2_send);

$subject = "DEVPSYCHAT - Deposit for the last 24 hours";

if ($_DEBUG)
{
psy_debug("debug_mode",1);
	$adm_email = 'rob@custom-coding.com';
	sendCronMail("Admin Test",$adm_email, $subject, $text_2_send);
}
else
{
psy_debug("debug_mode",0);
//	sendCronMail("Diana Lynn","mysticladydi@yahoo.com", $subject, $text_2_send);
//	sendCronMail("Jayson Lynn","psychic-contact@jayson-lynn.net", $subject, $text_2_send);
}

echo"<br>Finished";


/* FUNCTIONS */

function getPs($row) {
	$orderNumber = $row["Order_numb"];
	if(strstr($orderNumber, "InternetSecure:")!==false)
		return "IS";	
	if(strstr($orderNumber, "Echo:")!==false)
		return "Echo";	
	if(strstr($orderNumber, "Auth:")!==false)
		return "Auth";	
	if(strstr($orderNumber, "PayPal:")!==false) {
		if($row["Currency"] == 'USD')
			return "Paypal US";	
		else
			return "Paypal CA";	
	}
}// end

?>
